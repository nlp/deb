<?xml version='1.0' encoding='utf-8'?>
<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'><xsl:template match='/entry'><html><head><meta content='text/html[-semicolon-] charset=utf-8' http-equiv='Content-Type'/><title>Odborné PLIN termíny: <xsl:value-of select="hs"/> (DEBWrite)</title><style type="text/css">.hs{color: blue}
.type_container {border: 1px solid #000}
.type_container {background-color:#eee}
</style>
</head>
<body><h1><xsl:value-of select="hs"/></h1>
<xsl:apply-templates/></body></html></xsl:template>
<xsl:template name="file"><xsl:param name="file_element"/><br/><xsl:if test="starts-with($file_element/@mime, 'image')"><img src="/files/deb_PLIN/{.}" style="max-width:400px"/></xsl:if><xsl:if test="starts-with($file_element/@mime, 'audio')"><audio src="/files/deb_PLIN/{.}" controls="true"/></xsl:if><xsl:if test="starts-with($file_element/@mime, 'video')"><video src="/files/deb_PLIN/{.}" controls="true" style="max-width:400px"/></xsl:if><br/></xsl:template>

<xsl:template match="hs"><span class="hs type_text">heslové slovo: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="gram"><span class="gram type_text">gramatické informace: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="sd"><span class="sd type_select">slovní druh: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="rod"><span class="rod type_select">rod: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="deleni"><span class="deleni type_text">dělení slova: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="vysl"><span class="vysl type_text">výslovnost: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="vyzn"><div class="vyzn type_container">význam: <xsl:apply-templates/></div><br/></xsl:template>
<xsl:template match="cislo"><span class="cislo type_number">číslo: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="def"><span class="def type_textarea">definice: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="pr"><span class="pr type_textarea">příklad: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="syn"><span class="syn type_text">synonymum: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="ant"><span class="ant type_text">antonymum: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="reference"><span class="reference type_textarea">reference: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="koment"><span class="koment type_textarea">komentář: <xsl:apply-templates/></span><br/></xsl:template>
</xsl:stylesheet>
