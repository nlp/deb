<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output encoding="utf-8" method="xml" indent="yes" />
    <xsl:key name="lemma" match="LITERAL" use="."/>

  <xsl:template match="WN">
    <LexicalResource>
      <GlobalInformation label="{INFO/@globallabel}"/>
      <Lexicon languageCoding="{INFO/@languageCoding}" label="{INFO/@label}" language="{INFO/@language}" owner="{INFO/@owner}" version="{INFO/@version}">
        <xsl:call-template name="lexical_entry"/>
        <xsl:call-template name="synset"/>
      </Lexicon>
      <xsl:call-template name="sense_axes"/>
    </LexicalResource>
  </xsl:template>

  <xsl:template name="lexical_entry">
    <xsl:for-each select="//LITERAL[generate-id() = generate-id(key('lemma',text())[1])]">
      <xsl:variable name="lemma"><xsl:value-of select="."/></xsl:variable>
      <LexicalEntry>
        <Lemma writtenForm="{$lemma}">
          <xsl:attribute name="partOfSpeech"><xsl:value-of select="//SYNSET[SYNONYM/LITERAL/text()=$lemma]/POS"/></xsl:attribute>
        </Lemma>
        <xsl:for-each select="//LITERAL[.=$lemma]">
          <Sense id="{@id}" synset="{../../ID}"/>
        </xsl:for-each>
      </LexicalEntry>
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="synset">
    <xsl:for-each select="SYNSET">
      <Synset id="{ID}">
        <xsl:if test="BCS!=''">
          <xsl:attribute name="baseConcept"><xsl:value-of select="BCS"/></xsl:attribute>
        </xsl:if>
        <Definition gloss="{DEF}">
          <xsl:for-each select="USAGE">
            <Statement example="{.}"/>
          </xsl:for-each>
        </Definition>
        
        <SynsetRelations>
          <xsl:for-each select="ILR">
            <SynsetRelation target="{.}">
              <xsl:attribute name="relType">
                <xsl:choose>
                  <xsl:when test="@type='hypernym'">has_hyperonym</xsl:when>
                  <xsl:otherwise><xsl:value-of select="@type"/></xsl:otherwise>
                </xsl:choose>
              </xsl:attribute>

              <Meta author="{@author}" date="{@date}" source="{@source}" confidenceScore="{@confidenceScore}"/>
            </SynsetRelation>
          </xsl:for-each>
        </SynsetRelations>

        <MonolingualExternalRefs>
          <xsl:for-each select="ELR[not(@sa_id)]">
            <MonolingualExternalRef externalSystem="{@system}" externalReference="{.}" relType="{@type}"/>
          </xsl:for-each>

          <xsl:for-each select="SUMO">
            <MonolingualExternalRef externalSystem="SUMO" externalReference="{.}" relType="{@type}"/>
          </xsl:for-each>
          <xsl:for-each select="DOMAIN">
            <MonolingualExternalRef externalSystem="Domain" externalReference="{.}" />
          </xsl:for-each>
        </MonolingualExternalRefs>
      </Synset>
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="sense_axes">
    <SenseAxes>
      <xsl:for-each select="//ELR[@sa_id]">
        <SenseAxis id="{@sa_id}" relType="{@type}">
          <Meta author="{@author}" date="{@date}" source="{@source}" confidenceScore="{@confidenceScore}"/>
          <Target ID="{../ID}"/>
          <Target ID="{.}"/>
        </SenseAxis>
      </xsl:for-each>
    </SenseAxes>
  </xsl:template>

</xsl:stylesheet>
