<?xml version='1.0' encoding='utf-8'?>
<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'><xsl:template match='/entry'><html><head><meta content='text/html[-semicolon-] charset=utf-8' http-equiv='Content-Type'/><title>wer: <xsl:value-of select="et"/> (DEBWrite)</title></head>
<body><h1><xsl:value-of select="et"/></h1>
<xsl:apply-templates/></body></html></xsl:template>
<xsl:template match="hw"><span class="hw">headword: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="pos"><span class="pos">part of speech: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="gram"><span class="gram">grammar info: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="pron"><span class="pron">pronunciation: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="hyph"><span class="hyph">hyphenation: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="meaning"><div class="meaning" style="border: 1px solid #000[-semicolon-] background-color:#eee[-semicolon-]">meaning: <xsl:apply-templates/></div><br/></xsl:template>
<xsl:template match="nr"><span class="nr">meaning nr: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="domain"><span class="domain">domain: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="def"><span class="def">definition: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="usage"><span class="usage">usage example: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="synonym"><span class="synonym">synonym: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="antonym"><span class="antonym">antonym: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="et"><div class="et" style="border: 1px solid #000[-semicolon-] background-color:#eee[-semicolon-]">er: <xsl:apply-templates/></div><br/></xsl:template>
<xsl:template match="trrtret"><div class="trrtret" style="border: 1px solid #000[-semicolon-] background-color:#eee[-semicolon-]">trrtretwer: <xsl:apply-templates/></div><br/></xsl:template>
<xsl:template match="wrwre"><span class="wrwre">werwer: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="tr"><span class="tr">tretert: <xsl:apply-templates/></span><br/></xsl:template>
</xsl:stylesheet>
