<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="utf-8" method="text"/>

<xsl:param name="slovnik"/>

<xsl:template match="SYNSET">
  <xsl:for-each select="SYNONYM/LITERAL">&lt;https://abulafia.fi.muni.cz:9001/wnen30?action=linked&amp;query=<xsl:value-of select="/SYNSET/ID"/>&gt; &lt;http://deb.fi.muni.cz/wn-types#LITERAL&gt; "<xsl:value-of select="."/>" .
</xsl:for-each>&lt;https://abulafia.fi.muni.cz:9001/wnen30?action=linked&amp;query=<xsl:value-of select="ID"/>&gt; &lt;http://deb.fi.muni.cz/wn-types#POS&gt; "<xsl:choose>
<xsl:when test="POS='n'">Noun</xsl:when>
<xsl:when test="POS='v'">Verb</xsl:when>
<xsl:when test="POS='a'">Adjective</xsl:when>
<xsl:when test="POS='b'">Adverb</xsl:when>
</xsl:choose>" .
&lt;https://abulafia.fi.muni.cz:9001/wnen30?action=linked&amp;query=<xsl:value-of select="ID"/>&gt; &lt;http://deb.fi.muni.cz/wn-types#DEF&gt; "<xsl:value-of select="DEF"/>" .
<xsl:for-each select="ILR">&lt;https://abulafia.fi.muni.cz:9001/wnen30?action=linked&amp;query=<xsl:value-of select="/SYNSET/ID"/>&gt; &lt;http://deb.fi.muni.cz/wn-types#<xsl:value-of select="@type"/>&gt; &lt;https://abulafia.fi.muni.cz:9001/wnen30?action=linked&amp;query=<xsl:value-of select="."/>&gt; .
</xsl:for-each>
<xsl:for-each select="RILR">&lt;https://abulafia.fi.muni.cz:9001/wnen30?action=linked&amp;query=<xsl:value-of select="."/>&gt; &lt;http://deb.fi.muni.cz/wn-types#<xsl:value-of select="@type"/>&gt; &lt;https://abulafia.fi.muni.cz:9001/wnen30?action=linked&amp;query=<xsl:value-of select="/SYNSET/ID"/>&gt; .
</xsl:for-each>
</xsl:template>
</xsl:stylesheet>

