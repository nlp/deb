<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="utf-8" method="xml" />

  <xsl:template match="fanuk">
<xsl:processing-instruction name="xml-stylesheet">href="fanuk-preview.xslt" type="text/xsl"</xsl:processing-instruction>
<fanuk>
<xsl:apply-templates select="entry">
<xsl:sort select="@sorting"/>
</xsl:apply-templates>
</fanuk>
</xsl:template>

<xsl:template match="@*|node()">
  <xsl:copy>
    <xsl:apply-templates select="@*|node()"/>
  </xsl:copy>
</xsl:template>

<xsl:template match="entry">
<entry entry_id="{@entry_id}" xref="{@xref}" me="{@me}">
<xsl:apply-templates/>
</entry>
</xsl:template>

<xsl:template match="sense">
<sense nr="{s}" language_culture="{@language}">
    <xsl:apply-templates select="expl"/>
    <xsl:apply-templates select="eb"/>
</sense>
</xsl:template>

<xsl:template match="entry/me">
  <var><xsl:value-of select="."/></var>
</xsl:template>

<xsl:template match="heartland_gb">
  <gb_distrib><xsl:value-of select="."/></gb_distrib>
</xsl:template>
<xsl:template match="heartland_ir">
  <ire_distrib><xsl:value-of select="."/></ire_distrib>
</xsl:template>
<xsl:template match="freq_gb_2011">
  <freq_gb><xsl:value-of select="."/></freq_gb>
</xsl:template>
<xsl:template match="freq_uk_1881">
  <freq_gb_1881><xsl:value-of select="."/></freq_gb_1881>
</xsl:template>
<xsl:template match="freq_ir_2008">
  <freq_ire><xsl:value-of select="."/></freq_ire>
</xsl:template>

<xsl:template match="dafn|heartland_gb_comment|comment|editors|musings|meta|med|other|igi|source|freq_uk|freq_roi">
</xsl:template>

<xsl:template match="eb">
  <xsl:if test="count(arch)>0">
  <ebs>
  <xsl:apply-templates/>
  </ebs>
  </xsl:if>
</xsl:template>
<xsl:template match="expl">
  <xsl:if test="count(ppkk)>0">
  <expls>
  <xsl:apply-templates/>
  </expls>
  </xsl:if>
</xsl:template>

<xsl:template match="arch/i">
<sn><xsl:apply-templates/></sn>
</xsl:template>
<xsl:template match="arch">
  <eb type="{@type}">
  <xsl:apply-templates/>
  </eb>
</xsl:template>
<xsl:template match="ppkk">
  <expl category="{@category}">
  <xsl:if test="count(../ppkk)>1">(<xsl:number format="i"/>) </xsl:if>
  <xsl:apply-templates/>
  </expl>
</xsl:template>

</xsl:stylesheet>

