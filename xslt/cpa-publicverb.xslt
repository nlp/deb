<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:RDF="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
  xmlns:d="http://nlp.fi.muni.cz/cpa/rdf#">

  <xsl:output encoding="utf-8" type="xml" method="html"/>
  <xsl:param name="entry_id"/>

  <xsl:template match="RDF:RDF">
    <html>
      <head>
        <base href="http://deb.fi.muni.cz/pdev/"/>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <link rel="stylesheet" href="files.php?table.css" type="text/css"/>
        <title>Patterns for <xsl:value-of select="$entry_id"/></title>
      </head>
      <body>
        <a href="">verb list</a>
        <xsl:if test="samplesize or samplesize_i">
          <p>Sample size: <b><xsl:value-of select="samplesize_i"/> / <xsl:value-of select="samplesize"/></b></p>
        </xsl:if>
        <xsl:apply-templates select="RDF:Seq"/>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="RDF:Seq">
    <!--<xsl:value-of select="@about"/>-->
    <xsl:variable name="res0" select="@about"/>
    <xsl:apply-templates select='//RDF:Description[@about=$res0]'/>
    <xsl:if test="count(RDF:li)>0">
      <table>
        <tr>
          <td class="cell-patnum"><h5 class="colhdr">No.</h5></td>
      <xsl:if test="//d:perc">
        <td class="cell-patperc"><h5 class="colhdr">%</h5></td>
      </xsl:if>
      <td class="cell-patstring"><h5 class="colhdr">Pattern / Implicature</h5></td>
      <td class="cell-link"><h5 class="colhdr">&#160;</h5></td>      
      </tr>
      <xsl:for-each select="RDF:li">
        <!--<xsl:sort select="@resource"/>-->
        <xsl:if test="@resource">
            <!--<xsl:value-of select="@resource"/>-->
            <xsl:variable name="res" select="@resource"/>
            <xsl:apply-templates select='//RDF:Description[@about=$res]'/>
        </xsl:if>
        <xsl:if test="not(@resource)">
            <xsl:apply-templates select="RDF:Seq"/>
        </xsl:if>
      </xsl:for-each>
    </table>
  </xsl:if>
  </xsl:template>

  <xsl:template match="RDF:Description">
    <tr>
      <td class="cell-patnum">
        <a href="?action=pattern&amp;id={$entry_id}&amp;pat={d:patid}" target="_blank"><xsl:value-of select="d:num"/></a>
      </td>
      <xsl:if test="d:perc">
        <td class="cell-patperc">
          <xsl:value-of select="d:perc"/>
        </td>        
      </xsl:if>
      <td class="cell-patstring">
        <xsl:if test="d:pvidiom!=''">
          <span class="idiom"><xsl:value-of select="d:pvidiom"/></span>&#160;
        </xsl:if>
        <b><xsl:value-of select="d:string"/></b>
      </td>
      <td class="cell-link">
        <a target="_blank" href="doc.php?action=corpora&amp;id={$entry_id}&amp;num={d:num}">conc.</a>
      </td>
    </tr>
    <tr class="bottom">
      <td>&#160;</td>
      <xsl:if test="d:perc">
        <td></td>
      </xsl:if>
      <td class="cell-implic">
        <xsl:if test="d:domain!=''"><i><xsl:value-of select="d:domain"/></i></xsl:if>
        <xsl:if test="d:domain!='' and d:register!=''">, </xsl:if>
        <xsl:if test="d:register!=''"><i><xsl:value-of select="d:register"/></i></xsl:if>
        <xsl:if test="d:domain!='' or d:register!=''">. </xsl:if>
        <xsl:value-of select="d:primary"/>
        <xsl:if test="d:secimp!=''">
        <br/><xsl:value-of select="d:secimp"/>
        </xsl:if>
      </td>
      <td class="cell-link">
        <a target="_blank" href="edoc.php?action=corpora&amp;id={$entry_id}&amp;num={d:num}">exploit.</a>
      </td>
      
    </tr>
  </xsl:template>
</xsl:stylesheet>

