<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="utf-8" method="xml"
  doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
  doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
  />

<xsl:template match="entry">
  <html>
     <head>
      <title>DAFN2: <xsl:value-of select="name"/></title>
      <style type="text/css">
      body {
      background: #ffffff;
      color: #000000;
      }
      .red {
      color: #ff0000;
      }
      .green {
      color: #007700;
      }
      .darkred {
      color: #770000;
      }
      .blue {
      color: #0000ff;
      }
      .darkblue {
      color: #000088;
      }
      .head {
      font-size: 150%;
      }
      .sans {
        font-family: sans-serif;
      }
      .caps {
        font-variant: small-caps;
      }
      .indent {
        margin-left: 1em;
      }
      .normal {
        font-weight: normal;
      }
      </style>
     </head>
     <body>
       <h1 class="red head"><xsl:value-of select="name"/></h1>
       <p>1997 TD: <span class="green"><xsl:value-of select="count"/></span>;
         2000 freq.: <span class="green"><xsl:value-of select="freq_2000"/></span>
         2007 freq.: <span class="green"><xsl:value-of select="freq_2007"/></span>
         2010 freq.: <span class="green"><xsl:value-of select="freq_2010"/></span>
         
       </p>
       <xsl:apply-templates select="defblock"/>
       <xsl:apply-templates select="history"/>
       <xsl:apply-templates select="forenames"/>
     </body>
  </html>
</xsl:template>

<xsl:template match="defblock">
  <p><xsl:apply-templates select="prelim"/><xsl:apply-templates select="sense"/></p>
</xsl:template>

<xsl:template match="prelim">
  <xsl:apply-templates/><xsl:text> </xsl:text>
</xsl:template>

<xsl:template match="sense">
   <span class="blue"><xsl:if test="count(//sense)>1"><b><xsl:if test="@number!=''"><xsl:value-of select="@number"/></xsl:if><xsl:if test="not(@number) or @number=''"><xsl:value-of select="position()"/></xsl:if><xsl:text> </xsl:text> </b></xsl:if><xsl:apply-templates/></span><br/>
</xsl:template>
<xsl:template match="history">
  <p><i>History:</i> <xsl:apply-templates/></p>
</xsl:template>
<xsl:template match="forenames">
  <p><i>Forenames: </i> <xsl:apply-templates/></p>
</xsl:template>
<xsl:template match="fn">
<xsl:value-of select="."/>
</xsl:template>
<xsl:template match="lang">
<xsl:value-of select="."/><xsl:text> </xsl:text>
</xsl:template>
<xsl:template match="prob">
<xsl:value-of select="."/><xsl:text> </xsl:text>
</xsl:template>
<xsl:template match="forename">
  <i><xsl:apply-templates/></i>
</xsl:template>
<xsl:template match="regionalStats">
  <span class="green"><xsl:apply-templates/></span>
</xsl:template>

<xsl:template match="i">
  <i><xsl:apply-templates/></i>
</xsl:template>

<xsl:template match="su">
  <sup><xsl:apply-templates/></sup>
</xsl:template>

<xsl:template match="form">
  <i><xsl:apply-templates/></i>
</xsl:template>

<xsl:template match="xref|xr">
  <a href="/dafn2?action=preview&amp;id={.}"><b><i><xsl:apply-templates/></i></b></a>
</xsl:template>

<xsl:template match="b">
  <b><xsl:value-of select="text()"/></b>
</xsl:template>

<xsl:template match="vz">
  <i><xsl:value-of select="text()"/></i>
</xsl:template>

<xsl:template match="r">
  <xsl:value-of select="text()"/>
</xsl:template>

<xsl:template match="annotation">
</xsl:template>

<xsl:template match="render">
  <img src="/files/dafnimages/gb_{@img}.JPG"/>
</xsl:template>

</xsl:stylesheet>

