<?xml version='1.0' encoding='utf-8'?>
<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>
<xsl:template match='/entry'>
<html><head><meta content='text/html; charset=utf-8' http-equiv='Content-Type'/><title>test5-predloga-polona: <xsl:value-of select="i"/> (DEBWrite)</title></head><body>
<h1><xsl:value-of select="i"/></h1>
<xsl:apply-templates/></body></html></xsl:template>

<xsl:template match="i"><span class="i">iztočnica: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="lk"><span class="lk">leksikalna kategorija: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="po"><span class="po">podkategorija: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="var"><span class="var">variantni zapis: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="pom"><span class="pom">pomen: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="slo"><span class="slo">slovenski del: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="opis"><span class="opis">pomenski opis: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="kol"><span class="kol">kolokacije: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="prev"><span class="prev">prevodni del: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="zg"><span class="zg">zgled: <xsl:apply-templates/></span><br/></xsl:template>
</xsl:stylesheet>
