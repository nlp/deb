<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="utf-8" method="xml"
  doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
  doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
  />

<xsl:template match="root">
  <html>
    <head>
      <title>SČFI neslovesné: <xsl:value-of select="h"/></title>
      <style type="text/css">
      body {
      background: #ffffff;
      color: #000000;
      }
      .red {
      color: #ff0000;
      }
      .green {
      color: #007700;
      }
      .darkred {
      color: #770000;
      }
      .darkblue {
      color: #000088;
      }
      .head {
      font-size: 150%;
      }
      </style>
    </head>
    <body>
      <p><xsl:apply-templates/></p>
      <hr/>
      <p><small>Slovník české frazeologie a idiomatiky: výrazy neslovesné</small></p>
    </body>
  </html>
</xsl:template>

<xsl:template match="h">
      <b class="head red"><xsl:value-of select="." disable-output-escaping='yes'/></b><br/><br/>
</xsl:template>

<xsl:template match="b">
      <b><xsl:value-of select="." disable-output-escaping='yes'/></b>
</xsl:template>

<xsl:template match="i">
      <i><xsl:value-of select="." disable-output-escaping='yes'/></i>
</xsl:template>

<xsl:template match="n">
      <xsl:value-of select="." disable-output-escaping='yes'/>
</xsl:template>

</xsl:stylesheet>

