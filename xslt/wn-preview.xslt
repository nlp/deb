<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="utf-8"/>

<xsl:param name="dict"/>
<xsl:param name="servername"/>
<xsl:param name="default"/>
<xsl:param name="package"/>

<xsl:template match="SYNSET">
    <body style="background-color: white;" id="body">
      <span class="BLUE">POS: </span>
      <span class="RED"><xsl:value-of select="POS"/></span>
      <xsl:text> </xsl:text>
      <span class="BLUE">ID: </span>
      <span class="RED"><xsl:value-of select="ID"/></span>
      <xsl:text> </xsl:text>
      <xsl:apply-templates select="BCS"/>
      <xsl:apply-templates select="NL"/>
      <xsl:if test="ILI">
        <br/>
        <span class="BLUE">ILI: </span>
        <a href="http://compling.hss.ntu.edu.sg/iliomw/ili/concepts/{substring(ILI,2)}" target="cili"><xsl:value-of select="ILI"/></a>
      </xsl:if>
      <br/>
      <span class="BLUE">Synonyms: </span>
      <xsl:for-each select="SYNONYM/LITERAL">
        <xsl:value-of select="text()"/>:<xsl:value-of select="@sense"/>
        <xsl:if test="@lnote!=''"> (<xsl:value-of select="@lnote"/>)</xsl:if>
	<xsl:if test="position()!=last()"><xsl:text>, </xsl:text></xsl:if>
      </xsl:for-each>
      <br/>
      <xsl:apply-templates select="DEF"/>
      <xsl:for-each select="USAGE">
        <span class="BLUE">Usage: </span>
        <span class="DARK_RED"><xsl:value-of select="."/></span><br/>
      </xsl:for-each>
      <xsl:apply-templates select="DOMAIN"/>
      <xsl:for-each select="SUMO">
        <span class="BLUE">SUMO/MILO: </span>
        <span class="GREEN"><xsl:value-of select="TYPE"/><xsl:text> </xsl:text>
          <xsl:if test="$default='1'">
            <a href="{$servername}/sumo?action=runQuery&amp;query={.}&amp;outtype=html&amp;nojson=1&amp;default=1"><xsl:value-of select="text()"/></a>
          </xsl:if>
          <xsl:if test="$default!='1'">
            <a href="javascript:Model.show_entry('sumo','{.}');"><xsl:value-of select="text()"/></a>
          </xsl:if>
        </span><br/>
      </xsl:for-each>
      <xsl:for-each select="ILR">
        <span class="BLUE">--&gt;&gt;</span><xsl:text> </xsl:text>
	<xsl:choose>
	<xsl:when test="@type='hypernym'">
        <span class="GREEN">[<xsl:value-of select="@type"/>]</span>
	</xsl:when>
	<xsl:when test="@type='holo_member' or @type='holo_portion' or @type='holo_part'">
        <span class="BLUE">[<xsl:value-of select="@type"/>]</span>
	</xsl:when>
	<xsl:when test="@type='derived'">
        <span class="GREEN2">[<xsl:value-of select="@type"/>]</span>
	</xsl:when>
	<xsl:otherwise>
        <span class="DARK_RED">[<xsl:value-of select="@type"/>]</span>
	</xsl:otherwise>
	</xsl:choose>
	<xsl:text> </xsl:text>
  <xsl:if test="@literal!=''">
    <span class="RED">(<xsl:value-of select="@literal"/>)</span><xsl:text> </xsl:text>
  </xsl:if>
  <xsl:if test="$default='1'">
    <a href="{$servername}/{$dict}?action=runQuery&amp;query={@link}&amp;outtype=html&amp;nojson=1&amp;default=1"><xsl:value-of select="text()"/></a>
  </xsl:if>
  <xsl:if test="$default!='1'">
    <a href="#" onclick="Model.show_entry('{$dict}','{@link}');"><xsl:value-of select="text()"/></a>
  </xsl:if>
	<br/>
      </xsl:for-each>
      <xsl:for-each select="RILR">
        <span class="BLUE">&lt;&lt;--</span><xsl:text> </xsl:text>
	<xsl:choose>
	<xsl:when test="@type='hypernym'">
        <span class="GREEN">[hyponym]</span>
	</xsl:when>
	<xsl:when test="@type='holo_member'">
        <span class="BLUE">[mero_member]</span>
	</xsl:when>
	<xsl:when test="@type='holo_portion'">
        <span class="BLUE">[mero_portion]</span>
	</xsl:when>
	<xsl:when test="@type='holo_part'">
        <span class="BLUE">[mero_part]</span>
	</xsl:when>
	<xsl:when test="@type='derived'">
        <span class="GREEN2">[<xsl:value-of select="@type"/>]</span>
	</xsl:when>
	<xsl:when test="@type='category_domain'">
        <span class="DARK_RED">[category_member]</span>
	</xsl:when>
	<xsl:when test="@type='region_domain'">
        <span class="DARK_RED">[region_member]</span>
	</xsl:when>
	<xsl:when test="@type='usage_domain'">
        <span class="DARK_RED">[usage_member]</span>
	</xsl:when>
	<xsl:otherwise>
        <span class="DARK_RED">[<xsl:value-of select="@type"/>]</span>
	</xsl:otherwise>
	</xsl:choose>
	<xsl:text> </xsl:text>
  <xsl:if test="@literal!=''">
    <span class="RED">(<xsl:value-of select="@literal"/>)</span><xsl:text> </xsl:text>
  </xsl:if>
  <xsl:if test="$default='1'">
    <a href="{$servername}/{$dict}?action=runQuery&amp;query={@link}&amp;outtype=html&amp;nojson=1&amp;default=1"><xsl:value-of select="text()"/></a>
  </xsl:if>
  <xsl:if test="$default!='1'">
    <a href="#" onclick="Model.show_entry('{$dict}','{@link}', this);"><xsl:value-of select="text()"/></a>
  </xsl:if>
	<br/>
      </xsl:for-each>

      <br/>
      <xsl:for-each select="VALENCY">
        <span class="BLUE">VALENCY: </span>
        <ul>
        <xsl:for-each select="FRAME">
          <li><span class="DARK_RED"><xsl:value-of select="."/></span></li>
        </xsl:for-each>
        </ul>
      </xsl:for-each>

      <br/>
      <span class="BLUE">STAMP: </span>
      <span class="RED"><xsl:value-of select="STAMP"/></span><xsl:text> / </xsl:text>
      <span class="DARK_RED"><xsl:value-of select="CREATED"/></span>
      <xsl:if test="SNOTE">
        <br/>
        <span class="BLUE">Notes: </span>
        <xsl:apply-templates select="SNOTE"/> 
      </xsl:if>
    </body>
</xsl:template>

<xsl:template match="NL">
  <xsl:if test="normalize-space(text())='true'">
    <span class="BLUE">Not lexicalized</span>
  </xsl:if>
</xsl:template>

<xsl:template match="BCS">
      <span class="BLUE">BCS: </span>
      <span class="RED"><xsl:value-of select="."/></span>
</xsl:template>

<xsl:template match="DEF">
      <span class="BLUE">Definition: </span>
      <span class="RED"><xsl:value-of select="."/></span>
      <br/>
    </xsl:template>

<xsl:template match="DOMAIN">
      <span class="BLUE">Domain: </span>
      <span class="GREEN"><xsl:value-of select="."/></span><br/>
    </xsl:template>

<xsl:template match="SNOTE">
  <xsl:value-of select="."/>
  <xsl:if test="position()!=last()"><br/></xsl:if>
</xsl:template>

</xsl:stylesheet>

