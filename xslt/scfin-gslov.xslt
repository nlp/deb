<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="utf-8" method="xml" omit-xml-declaration="yes"/>

<xsl:template match="root">
      <p><xsl:apply-templates/></p>
</xsl:template>

<xsl:template match="h">
      <b class="head red"><xsl:value-of select="." disable-output-escaping='yes'/></b><br/><br/>
</xsl:template>

<xsl:template match="b">
      <b><xsl:value-of select="." disable-output-escaping='yes'/></b>
</xsl:template>

<xsl:template match="i">
      <i><xsl:value-of select="." disable-output-escaping='yes'/></i>
</xsl:template>

<xsl:template match="n">
      <xsl:value-of select="." disable-output-escaping='yes'/>
</xsl:template>

</xsl:stylesheet>

