<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="utf-8" method="xml"
  doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
  doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
  />

<xsl:template match="root">
  <html>
    <head>
      <title>SCS: <xsl:value-of select="h"/></title>
      <style type="text/css">
      body {
      background: #ffffff;
      color: #000000;
      }
      .red {
      color: #ff0000;
      }
      .green {
      color: #007700;
      }
      .darkred {
      color: #770000;
      }
      .darkblue {
      color: #000088;
      }
      .head {
      font-size: 150%;
      }
      </style>
    </head>
    <body>
      <p><xsl:apply-templates/></p>
      <hr />
      <p><small>Slovník cizích slov</small></p>
    </body>
  </html>
</xsl:template>

<xsl:template match="h">
      <b class="red head"><xsl:value-of select="."/></b><br/><br/>
</xsl:template>

<xsl:template match="ref">
  viz <a class="blue" href="scs?action=getdoc&amp;id={text()}&amp;tr=scs"><xsl:value-of select="."/></a>
</xsl:template>

<xsl:template match="t">
      <xsl:value-of select="."/>
</xsl:template>

</xsl:stylesheet>

