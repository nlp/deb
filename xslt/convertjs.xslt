<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output encoding="utf-8" method="text"/>
  <xsl:include href="convert.xslt"/>

  <xsl:param name="dictcode">dict</xsl:param>
  <xsl:param name="index_path">/entry</xsl:param>
  <xsl:param name="index_id">_entry</xsl:param>

  <xsl:template match="/element">
    var id = new Array();
    <xsl:for-each select="oneOrMore/*/text|zeroOrMore/*/text">
      id['<xsl:call-template name="get_id"/>'] = 1;
    </xsl:for-each>
var url_base = 'https://didone.fi.muni.cz:8001/<xsl:value-of select="$dictcode"/>';

function edit_onload() {
  try {
    var xml = new XML();
  } catch(e) {
    alert(e);
  }
}

function open_list() {
  location.href = url_base + '?action=list&amp;unlock=' + encodeURIComponent(document.getElementById('entry_id').value);
  return true;
}
function save_xml() {
  if (document.getElementById('<xsl:value-of select="$index_id"/>').value == '') {
    alert('<xsl:value-of select="$index_path"/> not filled');
    return false;
  }
  //new entry
  //var entry_id = document.getElementById('entry_id').value;
  var entry_id = document.getElementById('<xsl:value-of select="$index_id"/>').value;
  if (entry_id == '') {
    entry_id = '_new_id_';
  }

  //build XML
  var entry = &lt;<xsl:value-of select="@name"/>&gt;&lt;/<xsl:value-of select="@name"/>&gt;
  <xsl:apply-templates select="//text|//choice" mode="build"/>
  var url = url_base + '?action=save&amp;data=' + encodeURIComponent(entry) + '&amp;id=' + encodeURIComponent(entry_id);
  var r = new XMLHttpRequest ();
  r.open ('GET', url, true);
  r.onreadystatechange = function () {
    if (r.readyState == 4) {
      if (r.responseText == 'unauthorized') {
        alert('not authorized to save entry');
      } else {
        alert('entry saved');
        //document.getElementById('entry_id').value = r.responseText;
        open_list();
      }
    }
  }
  r.send ('');
  return true;
  }

function get_array(idStart, tagName) {
  var res_ar = new Array();
  if (tagName == null || tagName == undefined) tagName = 'textbox';
  var ar = document.getElementsByTagName(tagName);
  for (var i = 0; i &lt; ar.length; i++) {
    re = new RegExp(idStart + '[0-9]+')
    if (re.test(ar[i].id)) {
      res_ar.push(ar[i]);
    }
  }
  return res_ar;
}

function add(box, idStart, beforeElement) {
  var box_variant = document.getElementById(box);
  if (id[idStart] == null) id[idStart] = 0;
  var nid = ++id[idStart];
  
  var hb = document.createElement('hbox');
  hb.setAttribute('id', "hb"+idStart + nid);
  var but = document.createElement('button');
  but.setAttribute('label', '-');
  but.setAttribute('oncommand', "remove('hb" + idStart + nid + "')");
  
  var tb = document.createElement('textbox');
  tb.setAttribute('id', idStart + nid);
  tb.setAttribute('class', idStart);
  hb.appendChild(tb);
  hb.appendChild(but);
  box_variant.insertBefore(hb, document.getElementById(beforeElement));
}

function addlist(box, idStart, beforeElement, refElement) {
  var box_variant = document.getElementById(box);
  if (id[idStart] == null) id[idStart] = 0;
  var nid = ++id[idStart];
  
  var hb = document.createElement('hbox');
  hb.setAttribute('id', "hb"+idStart + nid);
  var but = document.createElement('button');
  but.setAttribute('label', '-');
  but.setAttribute('oncommand', "remove('hb" + idStart + nid + "')");
  
  var ml = document.createElement('menulist');
  ml.setAttribute('id', idStart + nid);
  ml.setAttribute('class', idStart);
  ml.setAttribute('flex', '1');
  ml.setAttribute('editable', 'true');
  ml.setAttribute('onkeypress', "if (event.target.nodeName == 'menulist' &amp;&amp; event.keyCode == event.DOM_VK_RETURN) find_entry(this, '"+refElement+"')");
  hb.appendChild(ml);
  hb.appendChild(but);
  box_variant.insertBefore(hb, document.getElementById(beforeElement));
}

function remove(element) {
  document.getElementById(element).parentNode.removeChild( document.getElementById(element) );
}

function find_entry(list, element) {
  if (list.value != '') {
    url = url_base + '?action=list_starts_with&amp;element=' + encodeURIComponent(element) + '&amp;id=' + encodeURIComponent(list.value);
    var r = new XMLHttpRequest ();
    r.open ('GET', url, true);
    r.onreadystatechange = function () {
      if (r.readyState != 4 || r.status != 200 || r.responseText == null) return;
      
      var a = r.responseText.split (/\r?\n/);
      list.selectedIndex = -1;
      list.removeAllItems();
      for (var i = 0; i &lt; a.length; i++) {
        if (a[i] != '') {
          list.insertItemAt(i, a[i], a[i]);
        }
      }
      list.open = true;
    }
    r.send('');
  }
  return true;
}

  </xsl:template>

  <xsl:template match="text" mode="build">
    <!--only one field-->
    <xsl:if test="name(../../.)!='oneOrMore' and name(../../.)!='zeroOrMore'">
      <xsl:call-template name="get_e4x"/> =  document.getElementById('<xsl:call-template name="get_id"/>').value;
    </xsl:if>

    <!--multiple fields-->
    <xsl:if test="name(../../.)='oneOrMore' or name(../../.)='zeroOrMore'">
      <xsl:if test="../@ref">
        <!--is reference to other entry = menulist -->
        var va = get_array('<xsl:call-template name="get_id"/>', 'menulist');
      </xsl:if>
      <xsl:if test="not(../@ref)">
        var va = get_array('<xsl:call-template name="get_id"/>');
      </xsl:if>
      //<xsl:call-template name="get_e4x"/> = '';
      var vi = 1;
      for (var i = 0; i &lt; va.length; i++) {
      if (va[i].value != '') {
        <xsl:call-template name="get_e4x"/>[vi] = va[i].value;
        vi++;
        }
      }
    </xsl:if>
  </xsl:template>
  <xsl:template match="choice" mode="build">
    <!--only one field-->
    <xsl:if test="name(../../.)!='oneOrMore' and name(../../.)!='zeroOrMore'">
      <xsl:call-template name="get_e4x"/> =  document.getElementById('<xsl:call-template name="get_id"/>').selectedItem.value;
    </xsl:if>

    <!--multiple fields-->
    <xsl:if test="name(../../.)='oneOrMore' or name(../../.)='zeroOrMore'">
      var va = get_array('<xsl:call-template name="get_id"/>');
      <xsl:call-template name="get_e4x"/> = '';
      var vi = 1;
      for (var i = 0; i &lt; va.length; i++) {
      if (va[i].value != '') {
        <xsl:call-template name="get_e4x"/>[vi] = va[i].selectedItem.value;
        vi++;
        }
      }
    </xsl:if>
  </xsl:template>
</xsl:stylesheet>
