<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="utf-8" method="xml" indent="yes"/>

<xsl:param name="hw"/>
<xsl:param name="where_hw"/>
<xsl:param name="path"/>
<xsl:template match="entry">
<html>
  <head>
    <title>delete <xsl:value-of select="name"/>?</title>
    <meta content='text/html; charset=utf-8' http-equiv='Content-Type'/>
    <link href='files/surnames.css' rel='stylesheet' type='text/css'/>
  </head>
  <body>
    <p><b>Really delete <i><xsl:value-of select="name"/></i>?</b></p>
    <p><a>
        <xsl:attribute name="href"><xsl:value-of select="$path"/>?action=dodelete&amp;hw=<xsl:value-of select="$hw"/>&amp;where_hw=<xsl:value-of select="$where_hw"/>&amp;id=<xsl:value-of select="@sortkey"/></xsl:attribute>
      Yes, delete</a> | <a>
      <xsl:attribute name="href"><xsl:value-of select="$path"/>?action=list&amp;hw=<xsl:value-of select="$hw"/>&amp;where_hw=<xsl:value-of select="$where_hw"/></xsl:attribute>
    No, return</a></p>
  </body>
</html>
</xsl:template>


</xsl:stylesheet>

