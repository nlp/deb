<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="utf-8" method="xml" indent="yes"/>

<xsl:template match="entry">
<html>
  <head>
    <title>smazat <xsl:value-of select="head/lemma"/>?</title>
    <meta content='text/html; charset=utf-8' http-equiv='Content-Type'/>
  </head>
  <body>
    <p><b>Opravdu smazat <i><xsl:value-of select="head/lemma"/></i>?</b></p>
    <p><a>
        <xsl:attribute name="href">/edna?action=dodelete&amp;id=<xsl:value-of select="@id"/></xsl:attribute>
      Ano, smazat</a> | <a>
      <xsl:attribute name="href">/edna?action=list</xsl:attribute>
    Ne, zpět</a></p>
  </body>
</html>
</xsl:template>


</xsl:stylesheet>

