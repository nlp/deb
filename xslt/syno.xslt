<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="utf-8" method="xml"
  doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
  doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
  />

<xsl:template match="ENTRY">
  <html>
    <head>
      <title>Synonyma: <xsl:value-of select="HEAD"/></title>
      <style type="text/css">
      body {
      background: #ffffff;
      color: #000000;
      }
      .red {
      color: #ff0000;
      }
      .green {
      color: #007700;
      }
      .darkred {
      color: #770000;
      }
      .darkblue {
      color: #000088;
      }
      .head {
      font-size: 150%;
      }
      </style>
    </head>
    <body>
      <p><xsl:apply-templates/></p>
      <hr/>
      <p><small>Slovník českých synonym</small></p>
    </body>
  </html>
</xsl:template>

<xsl:template match="HEAD">
      <b class="red head"><xsl:value-of select="."/></b>
<xsl:if test="$freq!=''">
  <xsl:text> (frekvence v korpusu: </xsl:text><xsl:value-of select="$freq"/>)
</xsl:if>
<br/>
</xsl:template>

<xsl:template match="LINE">
      <xsl:value-of select="."/><br/>
</xsl:template>

</xsl:stylesheet>

