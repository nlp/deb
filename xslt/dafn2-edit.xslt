<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns="http://www.w3c.org/1999/xhtml">
	<xsl:output encoding="utf-8" method="html" indent="yes" version="4.0"/>

<xsl:param name="path"/>
<xsl:param name="user"/>
<xsl:template match="entry">
  <script type="text/javascript">
    entries.push("<xsl:value-of select="@sortkey"/>");
    id["<xsl:value-of select="@sortkey"/>_sense"] =  <xsl:value-of select="count(sense)+2"/>;
    //ta_i = <xsl:value-of select="count(//ex|//cit)"/>;
    complete_time["<xsl:value-of select="@sortkey"/>"] = '<xsl:value-of select="@complete_time"/>';
    /*subModal setting*/
    /*var selchar_object = null;
    var selsrc_object = null;*/
    setPopUpLoadingPage('/files/popload.html');
    /*ikony z famfamfam.com*/
  </script>

  <div>
          <span style="right:20px;position:absolute">
            <span id="info_move_entry_{//@sortkey}" style="display:none">Please wait...</span>
            <span style="right:10px;position:relative">
              <xsl:if test="$path='dafn2'">
                <button onclick="if (save_before_move()) move_entry('dafn2','dafn2res','{//@sortkey}')">-&gt; reserve</button>
                <button onclick="if (save_before_move()) move_entry('dafn2','dafn2quar','{//@sortkey}')">-&gt; quarantine</button>
              </xsl:if>
              <xsl:if test="$path='dafn2quar'">
                <button onclick="if (save_before_move()) move_entry('dafn2quar','dafn2','{//@sortkey}')">-&gt; main list</button>
                <button onclick="if (save_before_move()) move_entry('dafn2quar','dafn2res','{//@sortkey}')">-&gt; reserve</button>
              </xsl:if>
              <xsl:if test="$path='dafn2res'">
                <button onclick="if (save_before_move()) move_entry('dafn2res','dafn2','{//@sortkey}')">-&gt; main list</button>
                <button onclick="if (save_before_move()) move_entry('dafn2res','dafn2quar','{//@sortkey}')">-&gt; quarantine</button>
              </xsl:if>
            </span>
          </span>
  <table border="0">
<tr><td>
<span style="display:none">ID: <input type="text" disabled="true" id="entry_id" value="{@sortkey}"/></span>
<span style="display:none">hw: <input type="text" disabled="true" id="{@sortkey}_cur_hw" value="{hw}"/></span>
        <label>Status <select id="{@sortkey}_status">
            <option value=""></option>
            <option value="Active">
              <xsl:if test="@status='Active'"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
              Active
            </option>
            <option value="Suppressed">
              <xsl:if test="@status='Suppressed'"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
              Suppressed
            </option>
            <option value="Deleted">
              <xsl:if test="@status='Deleted'"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
              Deleted
            </option>
            <option value="New">
              <xsl:if test="@status='New'"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
              New
            </option>
          </select>
        </label>
        <label>MM checked
        <input type="checkbox" id="{@sortkey}_mmcheck">
        <xsl:if test="@mmcheck='true'"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
        <xsl:if test="$user!='patrick' and $user!='munro' and $user!='deb'"><xsl:attribute name="disabled">disabled</xsl:attribute></xsl:if>
        </input>
        </label>
        <label>PWH approved
        <input type="checkbox" id="{@sortkey}_approved">
        <xsl:if test="@approved='true'"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
        <xsl:if test="$user!='patrick'"><xsl:attribute name="disabled">disabled</xsl:attribute></xsl:if>
        </input>
        </label>
        <input type="button" value="XRs from here"  onclick="xr_from('{@sortkey}')"/>
        <td>
        <span id="{@sortkey}_xr_from_show"></span>
        </td>
        <td>
        <input type="button" value="XRs to here"  onclick="xr_to('{@sortkey}')"/>
        </td>
        <td>
        <span id="{@sortkey}_xr_to_show"></span>
        </td>
</td>
<td>
	<span id="{@sortkey}_modified" style="display:none;font-style:italic;font-weight:bold"> *modified*</span>
</td>
</tr></table>
          <input type="hidden" id="is_new" value="{@new}"/>

<table id="data_table">
<tr><td colspan="2">
<table>
<tr><th>headword</th>
<td>
<input type="text" id="{@sortkey}_name" class="word" value="{name}"  onchange="update('{@sortkey}');check_hw(this, '{@sortkey}');"><xsl:if test="name!=''"><xsl:attribute name="disabled">disabled</xsl:attribute></xsl:if></input><xsl:text> </xsl:text>
          </td>
          <td  style="min-width:50%;vertical-align:top;text-align:right">
          TD 1997
          <input type="text" id="{@sortkey}_count" class="freq" value="{count}" onchange="update('{@sortkey}')"/><xsl:text> </xsl:text>
          Freq 2000
<input type="text" id="{@sortkey}_freq_2000" class="freq" value="{freq_2000}" onchange="update('{@sortkey}')"/><xsl:text> </xsl:text>
          Freq 2007
<input type="text" id="{@sortkey}_freq_2007" class="freq" value="{freq_2007}" onchange="update('{@sortkey}')"/><xsl:text> </xsl:text>
          Freq 2010
<input type="text" id="{@sortkey}_freq_2010" class="freq" value="{freq_2010}" onchange="update('{@sortkey}')"/><xsl:text> </xsl:text>
      </td></tr>
      </table>
      </td></tr>
      <tr><td>preamble</td><td>
          <input type="text" id="{@sortkey}_prelim" class="prelim" value="{defblock/prelim}" onchange="update('{@sortkey}')"/><xsl:text> </xsl:text><img src="files/icon_bold.png" title="Bold" class="button_img" onclick="insert_b('{@sortkey}_prelim')"/>
      </td></tr>
        <tr id="{@sortkey}_before_sense"><td><xsl:comment></xsl:comment></td></tr>
      <xsl:apply-templates select="defblock/sense">
        <xsl:sort select="@number" data-type="number"/>
      </xsl:apply-templates>
        <tr id="{@sortkey}_after_sense"><td><xsl:comment></xsl:comment></td></tr>
        <tr id="{@sortkey}_add_sense_box">
          <xsl:if test="count(defblock/sense)>0">
            <xsl:attribute name="style">visibility:hidden</xsl:attribute>
          </xsl:if>
          <td><xsl:comment></xsl:comment></td><td> <button onclick="add_data(document.getElementById('{@sortkey}_before_sense'), 'sense', '', '', '{//@sortkey}')">+ sense</button></td>
        </tr>
        <tr id="{@sortkey}_before_common"><td><xsl:comment></xsl:comment></td></tr>

        <tr><th>history</th>
          <td>
    <div class="textarea_updiv">
    <div class="textarea_div">
            <textarea id="{@sortkey}_history" onkeyup="resize(this)" onchange="update('{@sortkey}');" class="comment"><xsl:value-of select="history"/><xsl:comment></xsl:comment></textarea>
            </div>
            </div>
  <script type="text/javascript">
    make_textedit(document.getElementById('<xsl:value-of select="//@sortkey"/>_history'));
    ta_i++;
  </script>
          </td>
        </tr>
        <tr><th>forenames</th>
          <td>
            <textarea id="{@sortkey}_forenames" onkeyup="resize(this)" onchange="update('{@sortkey}');" class="comment"><xsl:value-of select="forenames"/><xsl:comment></xsl:comment></textarea>
          </td>
        </tr>

      <tr>
        <th colspan="2" style="text-align:left">Notes (not for publication)</th>
      </tr>
      <tr>
        <table>
          <tr>
            <th style="text-align:left">Editors</th>
            <th style="text-align:left"><xsl:comment></xsl:comment></th>
          </tr>
          <tr>
            <td><textarea id="{@sortkey}_editors" onchange="update('{@sortkey}');" class=""><xsl:value-of select="editors"/><xsl:comment></xsl:comment></textarea></td>
            <td><b>Requests</b> <textarea id="{@sortkey}_comment" onkeyup="resize(this)" onchange="update('{@sortkey}');" class="comment"><xsl:value-of select="comment"/><xsl:comment></xsl:comment></textarea>
            <input type="button" value="show requests" id="{@sortkey}_comment_show" style="display:none" onclick="document.getElementById('{@sortkey}_comment').style.display='inline';document.getElementById('{@sortkey}_comment_show').style.display='none'"/>
            <br/><b>Musings</b> <textarea id="{@sortkey}_musings" onkeyup="resize(this)" onchange="update('{@sortkey}');" class="comment"><xsl:value-of select="musings"/><xsl:comment></xsl:comment></textarea>
            <input type="button" value="share my thoughts" id="{@sortkey}_musings_show" style="display:none" onclick="document.getElementById('{@sortkey}_musings').style.display='inline';document.getElementById('{@sortkey}_musings_show').style.display='none'"/>
            </td>
          </tr>
        </table>
      </tr>
      <tr>
        <td colspan="2" style="vertical-align:top">Contributors, first edition
        <textarea id="{@sortkey}_annot" onchange="update('{@sortkey}');" class=""><xsl:value-of select="annot"/><xsl:comment></xsl:comment></textarea>
        </td>
      </tr>
      <tr class="{@sortkey}_dataend"/>
    </table>

  </div>

<hr class="divider"/>
</xsl:template>



<xsl:template match="sense">
<xsl:text>
</xsl:text>
  <tr class="sense" id="{//@sortkey}_citexrow{position()}" data="{//@sortkey}_citex" element="sense">
  <th>sense</th>
  <td >
  <table>
  <tr><th>nr.</th><td>
  <xsl:if test="@number and not(@number='')">
    <input type="text" data="{//@sortkey}_nr" class="sense_nr" value="{@number}" />
  </xsl:if>
  <xsl:if test="@number='' or not(@number)">
    <input type="text" data="{//@sortkey}_nr" class="sense_nr" value="{position()}" />
  </xsl:if>
    <label>Status <select id="{@sortkey}_sense{position()}_status" data="{//@sortkey}_status">
            <option value=""></option>
            <option value="Active">
              <xsl:if test="@status='Active'"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
              Active
            </option>
            <option value="Suppressed">
              <xsl:if test="@status='Suppressed'"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
              Suppressed
            </option>
            <option value="Deleted">
              <xsl:if test="@status='Deleted'"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
              Deleted
            </option>
            <option value="New">
              <xsl:if test="@status='New'"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
              New
            </option>
          </select>
        </label>

            <button onclick="add_data(document.getElementById('{//@sortkey}_citexrow{position()}'), 'sense', '', '', '{//@sortkey}')">+ sense</button>
            <button onclick="remove_sense('{//@sortkey}_citexrow{position()}', '{//@sortkey}')">remove sense</button>
        </td></tr>
        <tr><td><b>expl.</b></td>
          <td>
    <div class="textarea_updiv">
    <div class="textarea_div">
      <textarea class="ppkk" rows="4" cols="60" onkeyup="resize(this)" element="ppkk" id="{//@sortkey}_sense{position()}_text" data="citex" onchange="checkxmltext(this, '{//@sortkey}');update('{//@sortkey}');"><xsl:value-of select="."/><xsl:comment></xsl:comment></textarea>
    </div>
    </div>
  <script type="text/javascript">
    make_textedit(document.getElementById('<xsl:value-of select="//@sortkey"/>_sense<xsl:value-of select="position()"/>_text'));
    show_chinese(document.getElementById('<xsl:value-of select="//@sortkey"/>_sense<xsl:value-of select="position()"/>_text'));
    ta_i++;
  </script>

          </td>
          <td valign="top">
            <select onfocus="populate_link_select(this, document.getElementById('{//@sortkey}_sense{position()}_text'))" onchange="open_link(this)"><option selected="selected">open links</option></select>
          </td>
        </tr>


        <tr class="dataend"/>
    </table>
    </td>
  </tr>
</xsl:template>

</xsl:stylesheet>

