<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="utf-8" method="xml"
  doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
  doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
  />

<xsl:template match="prose">
  <html>
     <head>
       <title>MAS: <xsl:value-of select="H4/@title"/></title>
      <style type="text/css">
      body {
      background: #ffffff;
      color: #000000;
      }
      .red {
      color: #ff0000;
      }
      .green {
      color: #007700;
      }
      .darkred {
      color: #770000;
      }
      .darkblue {
      color: #000088;
      }
      .head {
      font-size: 150%;
      }
      </style>
     </head>
     <body>
       <p><xsl:apply-templates/></p>
       <hr />
       <p><small>MAS</small></p>
     </body>
  </html>
</xsl:template>

<xsl:template match="H4"><b class="red head"><xsl:value-of select="@title"/></b>
<br/></xsl:template>
<xsl:template match="pos"><small class="green"><xsl:value-of select="text()"/></small></xsl:template>
<xsl:template match="em"><i class="darkred"><xsl:value-of select="text()"/></i></xsl:template>
<xsl:template match="p"><xsl:apply-templates/></xsl:template>
<xsl:template match="p[@class='etym']"><p class="green"><xsl:apply-templates/></p></xsl:template>
<xsl:template match="b"><br/><b class="red"><xsl:value-of select="text()"/></b></xsl:template>
<xsl:template match="span[@class='page']"></xsl:template>

</xsl:stylesheet>

