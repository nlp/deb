<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output encoding="utf-8" method="xml"
  />

<xsl:template match="notes">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="cs" lang="cs">
  <head>
    <title>TeZK</title>
    <meta charset="UTF-8"/>
    <link rel="stylesheet" type="text/css" href="/editor/style.css" />
    <link rel="stylesheet" type="text/css" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/smoothness/jquery-ui.css" />

    <script src="https://code.jquery.com/jquery-1.11.3.min.js"><![CDATA[//]]></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js"><![CDATA[//]]></script>
    <script type="text/javascript" src="/editor/search.js">//</script>
    <script>
      <![CDATA[
  $(document).ready(function () {
    $('#search-field').keyup(function () {
      $(this).autocomplete({
        minLength: 3,
        source: function (request, response) {
          response(get_results(request.term, $('#search-status').val()));
        },
        select: function (event, ui) {
          window.location.href = url_start+'/editor/tezaurus.html?id=' + ui.item.id;
        },
      })
    });
  });
  ]]>
  </script>
  </head>
  <body>
	<div class="menu-nav">
		<ul class="menu">
      <li><a href="/editor/tezaurus.html"><div id="logo"></div></a></li>
			<li><a href="/editor/tezaurus.html">Tezaurus</a></li>
			<li><a href="/editor/informace.html">Informace</a></li>
      <li><a href="/tecu?action=history">Změny</a></li>
      <li><a href="/editor/kontakt.html">Kontakt</a></li>
		</ul>
	<a href="#" id="pull">Menu</a>  
	</div>
	<div class="search">
          <div class="search-bar">
              <form>
                  <label>Vyhledávání:</label>
                  <input id="search-field" type="text" name="search" />
                  <select id="search-status" name="search-status">
                    <option value="">všechny</option>
                    <option value="1">terminologický</option>
                    <option value="2">používaný</option>
                    <option value="3">používaný nezpracovaný</option>
                    <option value="4">odmítnutný</option>                  
                  </select>                  
              </form>
          </div>
	</div>
	<div class="clear"></div>
	<div id="index-wrap">
    <div id="index-content">
      <h1>Poslední změny</h1>
      <ul>
        <xsl:apply-templates/>
      </ul>
      <br/>
    </div>
  </div>
	<div id="footer">
		<div id="footer-content">
			<p>Tezaurus pro obor zeměměřictví a katastru nemovitostí</p>
			<p>Provozuje <a href="http://nlp.fi.muni.cz">Centrum zpracování přirozeného jazyka</a></p>
		</div>
	</div>
  </body>
</html>
</xsl:template>

<xsl:template match="last">
  <li><xsl:value-of select="note/@time"/>, <xsl:value-of select="note/@author"/>: <a href="/editor/tezaurus.html?id={@id}"><xsl:value-of select="@term"/></a>
    <xsl:if test="note/@xpath!='' or note/text()!=''">
      (<xsl:value-of select="note/@xpath"/><xsl:text> </xsl:text> 
      <xsl:value-of select="note/text()"/>)
    </xsl:if>
  </li>
</xsl:template>

</xsl:stylesheet>
