<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:RDF="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
  xmlns:d="http://nlp.fi.muni.cz/cpa/rdf#">

  <xsl:output encoding="utf-8" type="xml" method="html"/>

  <xsl:template match="RDF:RDF">
    <html>
      <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <style type="text/css">
          .entry_label {color:#ff0000; background-color: #FFFFCC;}
          .entry_label:after {content: ", "; background-color: #fff; color: #000;}
          .group_label {color:#000000; font-weight: bold;}
          .patterns {}
          .patterns:after {content: ", "}
          .oec_freq {}
          .oec_freq:after {content: ", "}
          .bnc_freq {}
          .bnc_freq:after {content: ", "}
          .create {}
          .create:after {content: ", "}
          .update{}
          .update:after {content: ", "}
          .label {font-size: 80%}
          .value {font-style: italic}
        </style>
      </head>
      <body onload="print();">
        <xsl:apply-templates select="RDF:Seq">

        </xsl:apply-templates>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="RDF:Seq">
    <!--<xsl:value-of select="@about"/>-->
    <xsl:variable name="res0" select="@about"/>
    <xsl:apply-templates select='//RDF:Description[@about=$res0]'/>
    <xsl:if test="count(RDF:li)>0">
    <ul>
      <xsl:for-each select="RDF:li">
        <xsl:sort select="RDF:Seq/@about"/>
        <!--<xsl:if test="@resource">
          <li>
            <xsl:variable name="res" select="@resource"/>
            <xsl:apply-templates select='//RDF:Description[@about=$res]'/>
          </li>
        </xsl:if>-->
        <xsl:if test="not(@resource)">
          <li>
            <xsl:apply-templates select="RDF:Seq"/> 
          </li>
        </xsl:if>
      </xsl:for-each>
    </ul>
  </xsl:if>
  </xsl:template>

  <xsl:template match="RDF:Description[@group='true']">
    <div class="group">
      <span class="group_label">
        <xsl:value-of select="d:entry_label"/> -

        <xsl:variable name="res0" select="@about"/>
        <xsl:value-of select="count(//RDF:Seq[@about=$res0]/RDF:li)"/>
      </span>
    </div>
  </xsl:template>

  <xsl:template match="RDF:Description[not(@group)]">
    <div class="entry">
      <span class="entry_label">
        <xsl:value-of select="d:entry_label"/>
      </span>
      <span class="patterns">
        <span class="label"><xsl:text>No. of patterns: </xsl:text></span>
        <span class="value"><xsl:value-of select="d:patterns"/></span>
      </span>
      <span class="oec_freq">
        <span class="label"><xsl:text>OEC: </xsl:text></span>
        <span class="value"><xsl:value-of select="d:oec_freq"/></span>
      </span>
      <span class="bnc_freq">
        <span class="label"><xsl:text>BNC: </xsl:text></span>
        <span class="value"><xsl:value-of select="d:bnc_freq"/></span>
      </span>
      <span class="create">
        <span class="label"><xsl:text>Created: </xsl:text></span>
        <span class="value"><xsl:value-of select="d:create_by"/><xsl:text>, </xsl:text><xsl:value-of select="d:create"/></span>
      </span>
      <span class="update">
        <span class="label"><xsl:text>Last edit: </xsl:text></span>
        <span class="value"><xsl:value-of select="d:edit_by"/><xsl:text>, </xsl:text><xsl:value-of select="d:last_edit"/></span>
      </span>
    </div>
  </xsl:template>
</xsl:stylesheet>

