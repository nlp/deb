<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="utf-8" method="xml"
  doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
  doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
  />

<xsl:variable name="smallcase" select="'abcdefghijklmnopqrstuvwxyz'" />
<xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'" />


<xsl:template match="entry">
  <html>
       <h1 class="darkblue"><xsl:value-of select="head/lemma"/><xsl:text> </xsl:text><em><xsl:value-of select="head/gram"/></em></h1>
       <p class="trans">
         <xsl:for-each select="trans">
           <xsl:value-of select="."/>
           <xsl:if test="@lang!=''">
             <xsl:choose>
               <xsl:when test="@lang='br'"><xsl:text> </xsl:text><em class="small">BrE</em></xsl:when>
               <xsl:when test="@lang='am'"><xsl:text> </xsl:text><em class="small">AmE</em></xsl:when>
               <xsl:when test="@lang='archaic'"><xsl:text> </xsl:text><em class="small">archaic</em></xsl:when>
             </xsl:choose>
           </xsl:if>
           <xsl:if test="position()!=last()"><xsl:text>; </xsl:text></xsl:if>
         </xsl:for-each>
       </p>
       <xsl:for-each select="ency/sense">
         <p><span class="sense"><xsl:value-of select="def"/></span>
           <xsl:if test="example!=''"><br/>[<xsl:value-of select="example"/>]</xsl:if>
         </p>
       </xsl:for-each>
       <xsl:if test="ency/example!=''"><p>[<xsl:value-of select="ency/example"/>]</p></xsl:if>
       <xsl:if test="count(semlink)>0">
         <ul class="semlink">
           <xsl:for-each select="semlink">
               <xsl:sort select="@type" lang="cs"/>
               <xsl:sort select="." lang="cs"/>
             <li><xsl:value-of select="translate(@type, $smallcase, $uppercase)"/><xsl:text> </xsl:text><xsl:value-of select="."/></li>
           </xsl:for-each>
         </ul>
       </xsl:if>
       <xsl:if test="count(kolok)>0">
         <p>
           <xsl:for-each select="kolok">
            <xsl:sort select="kolok" lang="cs"/>
             <b><xsl:value-of select="kolok"/></b><xsl:text> </xsl:text>
             <xsl:value-of select="trans"/><xsl:text> </xsl:text>
             <xsl:if test="ref!=''">
               -&gt; <a href="/edna?action=preview&amp;id={ref}"><xsl:value-of select="ref"/></a>
             </xsl:if>
             <br/>
           </xsl:for-each>
         </p>
       </xsl:if>
       <xsl:if test="count(deriv)>0">
         <p>
           <xsl:for-each select="deriv">
             <b><xsl:value-of select="deriv"/></b><xsl:text> </xsl:text>
             <em><xsl:value-of select="full"/></em>
           </xsl:for-each>
         </p>
       </xsl:if>
       <xsl:for-each select="refer">
         <p><b>→ <a href="/edna?action=preview&amp;id={.}"><xsl:value-of select="."/></a></b></p>
       </xsl:for-each>

       <p>Kategorie: </p>

       <hr />
  </html>
</xsl:template>

<xsl:template match="h"><b class="red head"><xsl:value-of select="text()"/></b>
<xsl:if test="$freq!=''">
  <xsl:text> (frekvence v korpusu: </xsl:text><xsl:value-of select="$freq"/>)
</xsl:if>
<br/></xsl:template>
<xsl:template match="small"><small class="green"><xsl:value-of select="text()"/></small></xsl:template>
<xsl:template match="ital"><i class="darkred"><xsl:value-of select="text()"/></i></xsl:template>
<xsl:template match="norm"><xsl:value-of select="text()"/></xsl:template>
<xsl:template match="bold"><br/><b class="red"><xsl:value-of select="text()"/></b></xsl:template>
<xsl:template match="it_sm"><small><i class="darkblue"><xsl:value-of select="text()"/></i></small></xsl:template>
<xsl:template match="arial"><b class="green"><xsl:value-of select="text()"/></b></xsl:template>

</xsl:stylesheet>

