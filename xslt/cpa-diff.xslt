<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:RDF="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
  xmlns:d="http://nlp.fi.muni.cz/cpa/rdf#">

  <xsl:output encoding="utf-8" type="xml" method="html"/>

  <xsl:template match="RDF:RDF">
    <html>
      <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <style type="text/css">
          .entry_label {}
          .comment {font-size: 80%}
          .comment:before {content: ", "}
        </style>
      </head>
      <body onload="print();">
        <xsl:apply-templates select="RDF:Seq"/>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="RDF:Seq">
    <!--<xsl:value-of select="@about"/>-->
    <xsl:variable name="res0" select="@about"/>
    <xsl:apply-templates select='//RDF:Description[@about=$res0]'/>
    <xsl:if test="count(RDF:li)>0">
    <table>
      <xsl:for-each select="RDF:li">
        <xsl:if test="@resource">
          <tr>
            <!--<xsl:value-of select="@resource"/>-->
            <xsl:variable name="res" select="@resource"/>
            <xsl:apply-templates select='//RDF:Description[@about=$res]'/>
          </tr>
        </xsl:if>
        <xsl:if test="not(@resource)">
          <tr>
            <xsl:apply-templates select="RDF:Seq"/>
          </tr>
        </xsl:if>
      </xsl:for-each>
    </table>
  </xsl:if>
  </xsl:template>

  <xsl:template match="RDF:Description">
      <td><xsl:value-of select="d:entry_label"/></td>
      <td><xsl:value-of select="d:number"/></td>
      <td><xsl:value-of select="d:type"/></td>
  </xsl:template>
</xsl:stylesheet>

