<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="utf-8" method="xml"
  doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
  doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
  />

  <xsl:param name="freq"/>
<xsl:template match="h">
       <p><xsl:apply-templates/></p>
</xsl:template>

<xsl:template match="Cil"></xsl:template>
<xsl:template match="Odkaz"><span class="{name()}"><a href="psjc?action=getdoc&amp;id={text()}&amp;tr=psjc"><xsl:value-of select="."/></a></span></xsl:template>
<xsl:template match="Vyzn"><span class="Vyzn"><xsl:value-of select="."/></span></xsl:template>
<xsl:template match="Heslo"><span class="Heslo"><xsl:value-of select="text()"/></span>
<xsl:if test="$freq!=''">
  <xsl:text> (frekvence v korpusu: </xsl:text><xsl:value-of select="$freq"/>)
</xsl:if>
</xsl:template>
<xsl:template match="*"><span class="{name()}"><xsl:value-of select="."/></span></xsl:template>

</xsl:stylesheet>

