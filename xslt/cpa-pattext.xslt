<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output encoding="utf-8" method="text" />
  
  <xsl:template match="entry">
    <xsl:apply-templates select="pattern">
      <xsl:sort select="@pattern_order" data-type="number"/>
    </xsl:apply-templates>
  </xsl:template>

  <xsl:template name="create_subject">
    <xsl:if test="subject[@semantic_type_defined='yes'] and subject[@lexical_set_defined='yes']">{</xsl:if>
    <xsl:apply-templates select="subject[@semantic_type_defined='yes']"/>
    <xsl:if test="subject[@semantic_type_defined='yes'] and subject[@lexical_set_defined='yes']"> | </xsl:if>
    <xsl:apply-templates select="subject[@lexical_set_defined='yes']"/>
    <xsl:if test="subject[@semantic_type_defined='yes'] and subject[@lexical_set_defined='yes']">}</xsl:if>
  </xsl:template>

  <xsl:template name="create_object_ind">
      <xsl:if test="object[@indirect='yes' and @optional='yes']">(</xsl:if>
      <xsl:if test="object[@indirect='yes' and @semantic_type_defined='yes'] and object[@lexical_set_defined='yes']">{</xsl:if>
      <xsl:apply-templates select="object[@indirect='yes' and @semantic_type_defined='yes']"/>
      <xsl:if test="object[@indirect='yes' and @semantic_type_defined='yes'] and object[@lexical_set_defined='yes']"> | </xsl:if>
      <xsl:apply-templates select="object[@indirect='yes' and @lexical_set_defined='yes']"/>
      <xsl:if test="object[@indirect='yes' and @semantic_type_defined='yes'] and object[@lexical_set_defined='yes']">}</xsl:if>
      <xsl:if test="object[@indirect='yes' and @optional='yes']">)</xsl:if>
    </xsl:template>

  <xsl:template name="create_object">
      <xsl:if test="object[not(@indirect='yes') and @optional='yes']">(</xsl:if>
      <xsl:if test="object[not(@indirect='yes') and @semantic_type_defined='yes'] and object[@lexical_set_defined='yes']">{</xsl:if>
      <xsl:apply-templates select="object[not(@indirect='yes') and @semantic_type_defined='yes']"/>
      <xsl:if test="object[not(@indirect='yes') and @semantic_type_defined='yes'] and object[@lexical_set_defined='yes']"> | </xsl:if>
      <xsl:apply-templates select="object[not(@indirect='yes') and @lexical_set_defined='yes']"/>
      <xsl:if test="object[not(@indirect='yes') and @semantic_type_defined='yes'] and object[@lexical_set_defined='yes']">}</xsl:if>
      <xsl:if test="object[not(@indirect='yes') and @optional='yes']">)</xsl:if>
  </xsl:template>

  <xsl:template match="pattern">
    <xsl:value-of select="@pattern_order"/><xsl:text>: </xsl:text>
    <xsl:call-template name="create_subject"/>
    <xsl:text> </xsl:text>
    <xsl:value-of select="../lemma"/><xsl:text> </xsl:text>
    <xsl:if test="@no_object='yes'">[NO OBJ]</xsl:if>
    <xsl:if test="not(@no_object='yes')">
      <xsl:call-template name="create_object_ind"/>
      <xsl:call-template name="create_object"/>
    </xsl:if>
    <xsl:if test="@no_adverbial='yes'">[NO ADVL]</xsl:if>
    <xsl:if test="not(@no_adverbial='yes')">
    </xsl:if>
    <xsl:if test="clausal[@optional='yes']">(</xsl:if>
    <xsl:apply-templates select="clausal"/>
    <xsl:if test="clausal[@optional='yes']">)</xsl:if>
    <xsl:apply-templates select="complement"/>
    <xsl:text>
    </xsl:text>
  </xsl:template>

  <xsl:template match="subject|object|adverbial_alternation|clausal">
    <xsl:if test="position() = 1 and @semantic_type_defined='yes'">[[</xsl:if>
    <xsl:if test="position() = 1 and @lexical_set_defined='yes'">{</xsl:if>
    <xsl:if test="@determiner_defined='yes'"><xsl:value-of select="determiner"/></xsl:if>
    <xsl:if test="@semantic_type_defined='yes'">
      <xsl:if test="semantic_role!=''">{</xsl:if>
      <xsl:value-of select="semantic_type"/>
      <xsl:if test="semantic_role!=''"> = <xsl:value-of select="semantic_role"/>}</xsl:if>
    </xsl:if>
    <xsl:if test="@lexical_set_defined='yes'"><xsl:value-of select="lexical_set"/></xsl:if>
    <xsl:if test="position() &lt; last()"> | </xsl:if>
    <xsl:if test="position() = last() and @semantic_type_defined='yes'">]]</xsl:if>
    <xsl:if test="position() = last() and @lexical_set_defined='yes'">}</xsl:if>
  </xsl:template>

  <xsl:template match="complement">
    {<xsl:value-of select="."/>}
    <xsl:if test="position() &lt; last()"> | </xsl:if>
  </xsl:template>

</xsl:stylesheet>

