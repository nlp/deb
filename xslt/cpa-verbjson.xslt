<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:RDF="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
  xmlns:d="http://nlp.fi.muni.cz/cpa/rdf#">

  <xsl:output encoding="utf-8" method="text" omit-xml-declaration="yes" indent="no"/>
  <xsl:param name="entry_id"/>

  <xsl:template match="RDF:RDF">[
    <xsl:apply-templates select="RDF:Description"/>
    ]
  </xsl:template>


<xsl:template match="RDF:Description">
  {"prop":"<xsl:value-of select="d:prop"/>",
  "pattern_id":"<xsl:value-of select="d:patid"/>",
  "number":"<xsl:value-of select="d:num"/>",
  "string":"<xsl:value-of select="d:string"/>",
  "primary":"<xsl:value-of select="d:primary"/>",
  "secondary":"<xsl:value-of select="d:secimp"/>",
  "pvidiom":"<xsl:value-of select="d:pvidiom"/>",
  "domain":"<xsl:value-of select="d:domain"/>",
  "register":"<xsl:value-of select="d:register"/>",
  "comment":"<xsl:value-of select="d:comment"/>",
  "corpus":"<xsl:value-of select="d:corpus"/>"
}
<xsl:if test="position()!=last()">,</xsl:if>
  </xsl:template>
</xsl:stylesheet>

