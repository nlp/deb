<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="utf-8"/>

<xsl:param name="dictionary"/>
<xsl:param name="servername"/>
<xsl:param name="default"/>

<xsl:template match="cid">
  <html>
    <head>
    <style type="text/css"></style> <!--this tag has to be on the first place, even empty-->
                <style type="text/css">
                a {
                        cursor: pointer;
                        text-decoration: underline;
                }
                </style>

    <xsl:if test="$default='1'">
      <link rel="stylesheet" type="text/css" href="{$servername}/files/default.css"/>
    </xsl:if>
    <style type="text/css">
    <![CDATA[
    .link {
      cursor: pointer;
    }
    ]]>
    </style>
    <script>
    <![CDATA[
      function show_entry(real, id) {
        var ev = document.createEvent('Events');
        ev.initEvent('MyEventHtml', false, true); 
                                var obj = document.getElementById('body');
                                obj.actual_item_id = id;
                                obj.actual_id = real;
        dispatchEvent(ev);
                                return (true);
      }
    ]]>
    </script>
    </head>
    <body style="background-color: white;" id="body">
      <span class="BLUE">CID_ID: </span>
      <span class="RED"><xsl:value-of select="@cid_id"/></span>
      <xsl:text> </xsl:text>
      <span class="BLUE">SEQ_NR: </span>
      <span class="RED"><xsl:value-of select="@seq_nr"/></span>
      <br/><xsl:text>
</xsl:text>
      <span class="BLUE">Form: </span><span class="GREEN"><xsl:value-of select="@form"/></span><br/>
      <span class="BLUE">PoS: </span><span class="GREEN"><xsl:value-of select="@pos"/></span><br/>
      <br/><xsl:text>
</xsl:text>
      <span class="BLUE">Score: </span><span class="GREEN"><xsl:value-of select="@score"/></span><br/>
      <span class="BLUE">Status: </span><span class="GREEN"><xsl:value-of select="@status"/></span><br/>
      <xsl:if test="$default='1'">
        <span class="BLUE">LU ID: </span><span class="GREEN"><a href="{$servername}/cdb_lu?action=runQuery&amp;query={@c_lu_id}&amp;outtype=html&amp;nojson=1&amp;default=1"><xsl:value-of select="@c_lu_id"/></a></span><br/>
      </xsl:if>
      <xsl:if test="$default!='1'">
        <span class="BLUE">LU ID: </span><span class="GREEN link" onclick="show_entry('cdblu1','{@c_lu_id}');"><xsl:value-of select="@c_lu_id"/></span><br/><xsl:text>
</xsl:text>
      </xsl:if>
      <xsl:if test="$default='1'">
        <span class="BLUE">Synset ID: </span><span class="GREEN"><a href="{$servername}/cdb_syn?action=runQuery&amp;query={@d_sy_id}&amp;outtype=html&amp;nojson=1&amp;default=1"><xsl:value-of select="@d_sy_id"/></a></span><br/>
      </xsl:if>
      <xsl:if test="$default!='1'">
        <span class="BLUE">Synset ID: </span><span class="GREEN link" onclick="show_entry('cdbsyn1','{@d_sy_id}');"><xsl:value-of select="@d_sy_id"/></span><br/>
      </xsl:if>

      
    </body>
  </html>
</xsl:template>

</xsl:stylesheet>

