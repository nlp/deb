<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">	
  <xsl:import href="czj-locale.xslt"/>

  <xsl:output encoding="utf-8" method="xml"   doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"   doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"   />
  <xsl:param name="perm"/>
  <xsl:param name="lang"/>
  <xsl:param name="dictcode"/>
  <xsl:param name="empty"/>
  <xsl:param name="public">false</xsl:param>
  <xsl:variable name="skupina_test">
    <!--ma uzivatel stejnou skupinu jako heslo? nebo bez omezeni-->
    <xsl:if test="contains(/entry/@user_skupina,'all') or contains($perm, 'admin')">true</xsl:if>
    <xsl:if test="$public!='true' and not(contains(/entry/@user_skupina,'all')) and not(contains($perm, 'admin'))"><xsl:value-of select="contains(/entry/@user_skupina, /entry/lemma/pracskupina)"/></xsl:if>
    <xsl:if test="$public='true'">false</xsl:if>
  </xsl:variable>


  <!--<xsl:key name="label" match="/entry/media/file/label/text()" use="." />-->
  <xsl:template match="entry">
    <html>
      <head>
        <title><xsl:choose>
            <xsl:when test="$dictcode='czj'">CZJ</xsl:when>
            <xsl:when test="$dictcode='asl'">ASL</xsl:when>
            <xsl:when test="$dictcode='is'">IS</xsl:when>
            <xsl:when test="$dictcode='spj'">SPJ</xsl:when>
          </xsl:choose>:
          <xsl:value-of select="@id"/>
        </title>
        <style type="text/css">
        </style>
        <link rel="stylesheet" href="/editor/css/simplebar.css" type="text/css" media="screen"/>
        <script type="text/javascript" src="/editor/css/prettify.js">//</script>
        <script type="text/javascript" src="/editor/css/jquery.js">//</script>
        <script type="text/javascript" src="/editor/css/jquery.scrollbar.js">//</script>
        <script type="text/javascript" src="/editor/css/wrapper.js">//</script>
        <link rel="stylesheet" type="text/css" media="screen" href="/editor/css-6.2/czj-common2.css" ></link>
        <link rel="stylesheet" type="text/css" media="screen" href="/editor/css/empty.css" ></link>
        <link rel="stylesheet" type="text/css" media="screen" href="/editor/css-6.2/czj-preview7-min2.css" ></link>
        <link href="/editor/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="/editor/lib/skin/functional.css" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Titillium+Web:400,200,300,600,700,900&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css" />       
         <link rel="stylesheet" href="/editor/lib/jquery-ui.css"></link>
        <script type="text/javascript" src="/editor/lib/jquery.min.js"></script>
        <script src="/editor/lib/flowplayer.min.js"></script>
        <script src="/editor/lib/jquery-ui.js">//</script>
        <link rel="stylesheet" href="/editor/lib/minimalist.css"/>
        <script type="text/javascript" src="/media/fancybox/jquery.fancybox.min.js"></script>
        <link rel="stylesheet" href="/media/fancybox/jquery.fancybox.min.css" type="text/css" media="screen" />
        <script type="text/javascript" src="/editor/imagesloaded.pkgd.min.js">//</script>
        <script type="text/javascript" src="/editor/czjmain.js">//</script>
        <script><![CDATA[
          function change_trans(type) {
            if ($('.trans'+type).css('display')=='none') {
              $('.trans'+type).css('display', 'inline-block');
            } else {
              $('.trans'+type).css('display', 'none');
            }
          }
          function change_video(type) {
            switch(type) {
            case 'front':
              $('.topvideosign_front').show();
              $('.topvideosign_side').hide();
              $('.videoselect-front').addClass('videoselect-active');
              $('.videoselect-side').removeClass('videoselect-active');
              break;
            case 'side':
              $('.topvideosign_front').hide();
              $('.topvideosign_side').show();
              $('.videoselect-front').removeClass('videoselect-active');
              $('.videoselect-side').addClass('videoselect-active');
              break;
            }
          }
          function change_view(eid) {
            if (document.getElementById(eid).style.display=='none') {
              if (eid == 'lemma_parts_box') {
                if (document.getElementById(eid).getAttribute('data-type') == 'coll') {
                  document.getElementById('lemma_parts_info').innerHTML = 'skrýt části slovního spojení';
                } else {
                  document.getElementById('lemma_parts_info').innerHTML = 'skrýt podrobnosti';
                }
              }
              document.getElementById(eid).style.display='block';
              user_resize_sw();
            } else {
              if (eid == 'lemma_parts_box') {
                if (document.getElementById(eid).getAttribute('data-type') == 'coll') {
                  document.getElementById('lemma_parts_info').innerHTML = 'zobrazit části slovního spojení';
                } else {
                  document.getElementById('lemma_parts_info').innerHTML = 'zobrazit podrobnosti';
                }
              }
              document.getElementById(eid).style.display='none';
            }
          }
          function player_change(pid) {
            if (document.getElementById(pid).style.display=='none') {
              document.getElementById(pid).style.display='block';
            } else {
              document.getElementById(pid).style.display='none';
            }
          }
          $(document).ready(function() {
            $(".flowmouse").bind("mouseenter mouseleave", function(e) {
              flowplayer(this)[/ee/.test(e.type) ? "resume":"pause"]();
            });
          });
          $(document).ready(function() {
            $('#navselect').change(function() {
              var option = $(this.options[this.selectedIndex]).val();
              $('#sidevideo').css('display', 'none');
              $('#sidesw').css('display', 'none');
              $('#sidehamnosys').css('display', 'none');
              $('#side'+option).css('display', 'block');
            });
            $('.topvideosign_side').hide();
            $("a.fancybox").fancybox({
              'autoScale'      : false,
              'transitionIn'   : 'elastic',
              'transitionOut'  : 'elastic',
              'titlePosition'  : 'inside',
              'hideOnContentClick' : true,
              'speedIn'        : 100,
              'speedOut'     : 100,
              'changeSpeed'    : 100,
              'centerOnScroll' : false,
              padding: 0,
              closeClick: true
            });
            $("a.hand").fancybox({
              'autoScale'      : false,
              'transitionIn'   : 'elastic',
              'transitionOut'  : 'elastic',
              'titlePosition'  : 'inside',
              'hideOnContentClick' : true,
              'speedIn'        : 100,
              'speedOut'     : 100,
              'changeSpeed'    : 100,
              'centerOnScroll' : false,
              padding: 0,
              closeClick: true
            });
          });
          $("a.fancybox_avatar").fancybox({
            'autoScale'      : false,
            'width'          : 612,
            'height'         : 640,
            'transitionIn'  : 'elastic',
            'transitionOut'   : 'elastic',
            'titlePosition'  : 'inside',
            'hideOnContentClick' : false,
            'speedIn'        : 100,
            'speedOut'    : 100,
            'changeSpeed'    : 100,
            'centerOnScroll' : false
          });
          $('#sw_search_link').fancybox({
            'autoScale'      : false,
            'transitionIn'   : 'elastic',
            'transitionOut'  : 'elastic',
            'titlePosition'  : 'inside',
            'hideOnContentClick' : true,
            'speedIn'        : 100,
            'speedOut'     : 100,
            'changeSpeed'    : 100,
            'centerOnScroll' : false,
            'height': 500,
            autoSize: false,
            padding: 0,
            closeClick: true,
            afterLoad: function(cur, prev) {
              cur.content[0].contentWindow.onload(prepare_swe());
            }
          });
          function open_iframe(id, dict) {
            var iframe = document.getElementById('colloc'+id);
            if (iframe.style.display=='none') {
              iframe.style.display='inline';
            } else {
              iframe.style.display='none';
            }
            if (iframe.src == 'about:blank') {
              iframe.src = '/'+dict+'?action=getdoc&amp;id='+id+'&amp;tr=inline';
            }
          }
          function iframe_loaded(id) {
            var iframe = document.getElementById('colloc'+id);
            if (iframe.src != 'about:blank') {
              //alert(iframe.contentWindow.document.body.scrollHeight);
              iframe.height = iframe.contentWindow.document.body.scrollHeight + 'px;'
            }
          }
          $(function() {
            $("#search").tabs();
            $("#search-cs").tabs();
          });
          $(document).ready(function() {
            // set drag drop
            $('.swlink').draggable({
              overflow:'visible',containment:'document',appendTo: 'body', helper:'clone',
              start: function(event, ui) {
                $('#search').show();
              }
            });
            $('#search').droppable({
              activeClass: 'ui-state-hover',
              drop:function(event,ui){
                console.log(ui.draggable.attr('data-sw'));
                $('#swpreview').attr('src', 'http://znaky.zcu.cz/proxy/tts/signwriting.png?generator[sw]='+ui.draggable.attr('data-sw')+'&generator[align]=top_left&generator[set]=sw10');
                $('#searchsw').val(ui.draggable.attr('data-sw'));
                $("#select-tab-sw").click();
              }
            });
          });
          ]]></script>

        <style type="text/css">
        body {width: 760px;}
          /* override jquery ui tabs*/
          #search, #search-cs {
          font-family: 'Titillium Web', sans-serif;
          font-size: 100%;
          border: 0;
          }
          .ui-tabs-nav {         border: 0;         background: white;       }
          .ui-tabs .ui-tabs-nav li a {
          padding: 0.2em 0.5em;
          color: white;
          }
          .ui-tabs .ui-tabs-nav li {
          background: #a3caaa;
          margin: 0;
          }
          .ui-tabs-nav li.ui-tabs-active  {
          background: #a3ca49;
          }
          .ui-tabs-nav li.ui-tabs-active a  {
          font-weight: bold;
          }
          .ui-tabs .ui-tabs-panel {
          padding: 0.2em;
          }
          .flowplayer .fp-buffer{background-color: white; }
          .flowplayer .fp-progress{background-color: #03C9A9; }
          a.none {color: black; text-decoration: none;}
        </style>
      </head>
      <body>
        <!---<xsl:apply-templates />-->
        <div class="czj-container">
          <div class=""> <!-- czj-inner-wrapper zrušeno pro iframe-->
            
            <div class="czj-inner-right">
              <div>
                <div>
                  <div class="czj-mainlemma TOP{$dictcode}">
                    <xsl:if test="/entry/lemma/completeness='1'">
                      <xsl:attribute name="style">border:solid 8px firebrick; background-color: dimgray;</xsl:attribute>
                    </xsl:if>
                    <!--zdroj čelní video-->
                    <xsl:if test="//lemma/video_front!=''">
                      <xsl:variable name="vf"><xsl:value-of select="//lemma/video_front"/>
                      </xsl:variable>
                      <xsl:if test="//media/file[location=$vf]/id_meta_source!='' or //media/file[location=$vf]/id_meta_author!=''">
                        <div style="float: left; width: 648px;" class="source">
                          <xsl:choose>
                            <xsl:when test="//media/file[location=$vf][1]/id_meta_source!=''"><xsl:value-of select="$loc-zdroj"/>: <xsl:value-of select="//media/file[location=$vf][1]/id_meta_source"/></xsl:when>
                            <xsl:when test="//media/file[location=$vf][1]/id_meta_source='' and //media/file[location=$vf][1]/id_meta_copyright='UP Olomouc'"><xsl:value-of select="$loc-zdroj"/>: LANGER, J. a kol. Znaková zásoba českého znakového jazyka ..</xsl:when>
                            <xsl:otherwise><xsl:value-of select="$loc-autor"/>: <a href="/{$dictcode}?lang={$lang}&amp;action=page&amp;page=about#skupiny"><xsl:value-of select="//media/file[location=$vf][1]/id_meta_author"/></a></xsl:otherwise>
                          </xsl:choose>
                        </div>
                      </xsl:if>
                      <div class="author-info" style="display:none;">
                        <br />
                        <xsl:if test="//media/file[location=$vf]/id_meta_source!=''">
                          <span class="author-author"><xsl:value-of select="$loc-autor"/>:
                            <a href="/{$dictcode}?lang={$lang}&amp;action=page&amp;page=about#skupiny"><xsl:value-of select="//media/file[location=$vf]/id_meta_author"/></a>
                            ; </span>
                        </xsl:if>
                        <xsl:if test="//media/file[location=$vf][1]/id_meta_copyright!=''">
                          <span class="author-copyright"><xsl:value-of select="$loc-autorvideo"/>: <xsl:value-of select="//media/file[location=$vf][1]/id_meta_copyright"/></span>
                        </xsl:if>
                      </div>
                    </xsl:if>
                    <!--zdroj čelní video-->
              
                    <xsl:if test="$perm!='ro' or $skupina_test='true'">
                      <span style="float: right;">
                        <xsl:if test="$perm!='ro'">
                          <xsl:if test="$dictcode='czj'"><a href="/editor/?id={@id}&amp;lang={$lang}&amp;empty={$empty}"><xsl:value-of select="$loc-edit"/><img src="/editor/img/edit.png" /></a></xsl:if>
                          <xsl:if test="$dictcode!='czj'"><a href="/editor{$dictcode}/?id={@id}&amp;lang={$lang}&amp;empty={$empty}"><xsl:value-of select="$loc-edit"/><img src="/editor/img/edit.png" /></a></xsl:if>
                        </xsl:if>
                        <xsl:choose>
                          <xsl:when test="lemma/@auto_complete='1' and lemma/completeness!='1'"><a style="color:blue;cursor:pointer" onclick="window.location = 'http://www.dictio.info'+window.location.pathname+window.location.search"><img src="/editor/publikovano.png" border="0" title="přepnout na veřejné zobrazení (heslo viditelné v překladové i výkl. části)"/></a></xsl:when>
                          
                          <xsl:otherwise><img src="/editor/nepublikovano.png" title="heslo ve výkladové části nezveřejněné"/> </xsl:otherwise>
                        </xsl:choose>
                      </span>
                      <xsl:if test="$perm!='ro'">
                        <span class="publish">
                          <xsl:if test="/entry/lemma/completeness='1'">
                            <xsl:attribute name="style">color: yellow;</xsl:attribute>
                          </xsl:if>
                          <xsl:value-of select="$loc-publikovani"/>:
                          <xsl:choose>
                            <xsl:when test="/entry/lemma/completeness='0'"><xsl:value-of select="$loc-automaticky"/> </xsl:when>
                            <xsl:when test="/entry/lemma/completeness='1'"><xsl:value-of select="$loc-nepublikovat"/> </xsl:when>
                            <xsl:when test="/entry/lemma/completeness='2'"><xsl:value-of select="$loc-vyplnene"/> </xsl:when>
                            <xsl:when test="/entry/lemma/completeness='100'"><xsl:value-of select="$loc-schvalene"/> </xsl:when>
                        </xsl:choose></span>
                        <span class="author-info" style="margin-left: 5px;"><strong><xsl:value-of select="$loc-primy"/>: </strong> <span id="dirURL">http://www.dictio.info/<xsl:value-of select="$dictcode"/>?lang=<xsl:value-of select="$lang"/>&amp;action=search&amp;getdoc=<xsl:value-of select="@id"/></span><button class="button-small" style="background-color: #D7E1E4; font-size: 10px; color: gray;" id="clipBoard" onclick="myFunction()"><xsl:value-of select="$loc-kopirovat"/></button></span>
                        <br />

                        <span class="set-status">             ID:
                          <xsl:value-of select="@id"/> | label:
                         
                          <xsl:value-of select="/entry/media/file[location=/entry/lemma/video_front][1]/label/text()"/>
                          |VIDEO=<xsl:value-of select="$video"/>
                    </span>
                    <br />
                  </xsl:if>
                </xsl:if>
                <xsl:if test="$perm='ro' and $skupina_test!='true'">
                  <span style="float: right; position: absolute; right: 200px;">
                  </span>
                </xsl:if>
                <xsl:if test="lemma/lemma_type='collocation' and (($perm!='ro' or $skupina_test='true') or collocations/@status!='hidden')">
                  <span id="lemma_parts">
                    <img src="/editor/img/spojeni.png" title="ustálené slovní spojení" style="padding-right: 5px;"/> <!-- <strong>Slovní spojení. </strong>-->
                    <xsl:if test="collocations/colloc"><a href="#" onclick="change_view('lemma_parts_box')" id="lemma_parts_info"><xsl:value-of select="$loc-zobrazitcasti"/></a></xsl:if>
                    <br />
                    <span id="lemma_parts_box" data-type="coll" style="display:none;">
                      <xsl:apply-templates select="collocations/colloc"/>
                    </span>
                  </span>
                  <div style="height: 1px; width: 100%; display: inline-block; background-color: white; margin-top: 5px; margin-bottom: 10px;"></div>
                </xsl:if>
                <div class="czj-mainlemma-inner">

                  <div class="czj-mainlemma-video">
                    <span class="videoblock">
                      <div class="czj-mainlemma-videoblock">
                        <!--<xsl:apply-templates select="media/file[type='sign_front' or type='sign_side']" mode="inlinetop"/>-->
                        <xsl:if test="//lemma/video_front!=''">
                          <xsl:apply-templates select="media/file[location=//lemma/video_front][1]" mode="inlinetop2"> <!-- původní podmínka and ($perm!='ro' or $skupina_test='true' or status!='hidden')][1]-->
                            <xsl:with-param name="video_type">sign_front</xsl:with-param>
                          </xsl:apply-templates>
                        </xsl:if>
                        <xsl:if test="@auto_complete!='1' and $perm='ro' and $skupina_test!='true'"><div style="position: absolute; background-color: yellow; top: 440px; margin-left: 30px; padding: 2px 50px 2px 50px;">CHYBA: nemáte dostatečná práva pro zobrazení všech částí tohoto hesla</div></xsl:if>
                        <xsl:if test="//lemma/video_front='' or not(//lemma/video_front)">
                          <span class="topvideosign_front">
                            <img src="/media/img/emptyvideo.jpg"/>
                          </span>
                        </xsl:if>
                        <xsl:if test="//lemma/video_side!=''">
                          <xsl:apply-templates select="media/file[location=//lemma/video_side][1]" mode="inlinetop2">
                            <xsl:with-param name="video_type">sign_side</xsl:with-param>
                          </xsl:apply-templates>
                        </xsl:if>
                        <xsl:if test="//lemma/video_side='' or not(//lemma/video_side)">
                          <span class="topvideosign_side">
                            <img src="/media/img/emptyvideo.jpg"/>
                          </span>
                        </xsl:if>
                      </div>
                      <div class="czj-mainlemma-videoblock-buttons">
                        <a href="#" style="text-decoration: none; color: black;">
                          <span onclick="change_video('front');" class="videoselect videoselect-front videoselect-active"><xsl:value-of select="$loc-celni"/></span>
                        </a>
                        <a href="#" style="text-decoration: none; color: black;">
                          <span class="videoselect videoselect-side" onclick="change_video('side')"><xsl:value-of select="$loc-bocni"/></span>
                        </a>
 	                 <!-- <a href="/{$dict}?lang={$lang}&amp;action=search&amp;getdoc={../@lemma_id}&amp;empty={$empty}">webm video</a>-->
                       </div>
                      <!--dominance -->
                      <xsl:if test="//lemma/video_front!=''">
                        <xsl:variable name="vf">
                          <xsl:value-of select="//lemma/video_front"/>
                        </xsl:variable>
                        <xsl:choose>
                          <xsl:when test="//media/file[location=$vf]/orient='pr'">
                            <span style="color: #03C9A9;; position: relative;  top: -235px; left: +45px; z-index: 10;"> R
                            </span>
                          </xsl:when>
                          <xsl:when test="//media/file[location=$vf]/orient='lr'">
                            <span style="color: #03C9A9;; position: relative;  top: -235px; left: -210px; z-index: 10;"> L
                            </span>
                          </xsl:when>
                        </xsl:choose>
                      </xsl:if>
                      <div class="cleaner">
                      </div>
                    </span>
                    <!-- videoblock -->
                  </div>
                  <xsl:if test="lemma/swmix/sw and ($perm!='ro' or $skupina_test='true' or lemma/@swstatus!='hidden' or (lemma/lemma_type!='single'))">
                    <div class="czj-mainlemma-sw">
                      <!-- <div class="czj-mainlemma-sw-text">SignWriting</div> -->
                      <div class="czj-mainlemma-sw-images">
                        <div class="czj-mainlemma-sw-images-inner">
                          <xsl:apply-templates select="lemma/swmix/sw"/>
                        </div>
                      </div>
                    </div>
                  </xsl:if>
                  <xsl:if test="lemma/hamnosys[$perm='ro' and @status!='hidden' and $skupina_test='true'] and lemma/hamnosys!=''">
                    <div id="hamnava">
                      <span id="avatar" class="transavatar">
                        <!--3D avatar:<br/>-->
                        <a data-fancybox="" data-type="iframe" class="fancybox_avatar iframe" data-src="http://znaky.zcu.cz/avatar/show?unicode_hamnosys={lemma/hamnosys}">
                          <img src="/media/avatar.png" alt="click to play avatar"/>
                        </a>
                      </span>
                    </div>
                    <div style="clear:both"></div>
                  </xsl:if>
                  <!--<xsl:if test="lemma/hamnosys=''"><div style="width: 145px; height: 284px; background-color: silver;"></div></xsl:if>-->
                  <!--schvalovani-->
                  <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_lemmaczj')">
                    <span class="set-status">
                      <br /><br /><xsl:value-of select="$loc-typhesla"/>:
                      <xsl:choose>
                        <xsl:when test="/entry/lemma/lemma_type='single'"><xsl:value-of select="$loc-jednoduche"/>; </xsl:when>
                        <xsl:when test="/entry/lemma/lemma_type='derivat'"><xsl:value-of select="$loc-derivat"/>; </xsl:when>
                        <xsl:when test="/entry/lemma/lemma_type='kompozitum'"><xsl:value-of select="$loc-kompozitum"/>; </xsl:when>
                        <xsl:when test="/entry/lemma/lemma_type='fingerspell'"><xsl:value-of select="$loc-spelovany"/>; </xsl:when>
                        <xsl:when test="/entry/lemma/lemma_type='collocation'"><xsl:value-of select="$loc-spojeni"/>; </xsl:when>
                      </xsl:choose>
                      <xsl:call-template name="status_publish">
                        <xsl:with-param name="status" select="/entry/lemma/status"/>
                        <xsl:with-param name="type">entry</xsl:with-param>
                      </xsl:call-template>
                    </span>
                    <br/><br/>
                  </xsl:if>
                  <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_video')">
                    <span class="set-status"><xsl:value-of select="$loc-celnivideo"/>
                      <xsl:if test="//file[location=//lemma/video_front and status='published']"><img src="/editor/img/checked.png" title="schváleno" /></xsl:if>
                      <xsl:if test="not(//file[location=//lemma/video_front and status='published'])">
                        <xsl:call-template name="status_publish">
                          <xsl:with-param name="status" select="//file[location=//lemma/video_front]/status"/>
                          <xsl:with-param name="type">video_front</xsl:with-param>
                        </xsl:call-template>
                      </xsl:if>
                    </span>
                    <br/>
                  </xsl:if>
                  <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_video')">
                    <span class="set-status"><xsl:value-of select="$loc-bocnivideo"/>
                      <xsl:if test="//file[location=//lemma/video_side and status='published']"><img src="/editor/img/checked.png" title="schváleno" /></xsl:if>
                      <xsl:if test="not(//file[location=//lemma/video_side and status='published'])">
                        <xsl:call-template name="status_publish">
                          <xsl:with-param name="status" select="//file[location=//lemma/video_side]/status"/>
                          <xsl:with-param name="type">video_side</xsl:with-param>
                        </xsl:call-template>
                      </xsl:if>
                    </span>
                    <br/>
                  </xsl:if>
                  <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_transkripce')">
                    <span class="set-status">SW
                      <xsl:call-template name="status_publish">
                        <xsl:with-param name="status" select="/entry/lemma/@swstatus"/>
                        <xsl:with-param name="type">sw</xsl:with-param>
                      </xsl:call-template>
                    </span>
                    <br/>
                  </xsl:if>
                  <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_transkripce')">
                    <span class="set-status">HNS
                      <xsl:call-template name="status_publish">
                        <xsl:with-param name="status" select="/entry/lemma/hamnosys/@status"/>
                        <xsl:with-param name="type">hns</xsl:with-param>
                      </xsl:call-template>
                    </span>
                    <br/>
                  </xsl:if>
                  <!--schvalovani-->
                  <xsl:if test="lemma/hamnosys[($perm!='ro' or $skupina_test='true') or @status!='hidden'] and lemma/hamnosys!=''">
                    <div id="hamnosys" class="transhamn" style="width: 660px; height: 35px; float: left; background-color: white; margin-top: 20px; padding: 10px 10px 0px 10px;">
                      <xsl:if test="count(../sw)>1"></xsl:if>
                      <a data-fancybox="images" class="fancybox" href="http://znaky.zcu.cz/proxy/tts/tex2img.png?generator[template]=hamnosys&amp;generator[dpi]=400&amp;generator[engine]=x&amp;generator[tex]={lemma/hamnosys}">
                        <img src="http://znaky.zcu.cz/proxy/tts/tex2img.png?generator[template]=hamnosys&amp;generator[dpi]=200&amp;generator[engine]=x&amp;generator[tex]={lemma/hamnosys}"/>                     </a>
                    </div>
                    <xsl:if test="count(lemma/sw)>1"><br/></xsl:if>
                  </xsl:if>
                </div>
                <!-- czj-mainlemma-inner -->
              </div>
              <!-- czj-mainlemma -->
              <xsl:if test="(lemma/grammar_note and (lemma/grammar_note/@status!='hidden' or ($perm!='ro' or $skupina_test='true'))) or ((lemma/lemma_type='derivat' or lemma/lemma_type='kompozitum') and (($perm!='ro' or $skupina_test='true') or collocations/@status!='hidden'))">
                <div class="czj-gram-popis">
                  <img src="/editor/img/grey-corner.png" style="position: relative; top: 0px; left: -45px;" valign="top"/>
                  <!--<div class="czj-lemma-h1">Gramatický popis</div>-->
                  <xsl:if test="lemma/grammar_note/@source!=''">
                    <span style="float: right;" class="source"><xsl:value-of select="$loc-zdroj"/>: <xsl:value-of select="lemma/grammar_note/@source"/>;
                    </span>
                  </xsl:if>
                  <div class="author-info" style="display:none">
                    <xsl:if test="lemma/grammar_note/@author!=''">
                      <span class="author-author"><xsl:value-of select="$loc-autor"/>:
                        <a href="/{$dictcode}?lang={$lang}&amp;action=page&amp;page=about#skupiny"><xsl:value-of select="lemma/grammar_note/@author"/></a>,
                      </span>
                    </xsl:if>
                    <!--<span class="author-copyright"><xsl:value-of select="$loc-autorvideo"/>:
                      <xsl:value-of select="lemma/grammar_note/@copyright"/>
                    </span>-->
                  </div>
                  <!--schvalování-->
                  <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_lingvist')">
                    <span class="set-status"><xsl:value-of select="$loc-grampopis"/>
                      <xsl:call-template name="status_publish">
                        <xsl:with-param name="status" select="/entry/lemma/grammar_note[1]/@status"/>
                        <xsl:with-param name="type">gram</xsl:with-param>
                      </xsl:call-template>
                    </span>
                  </xsl:if>
                  <!--schvalování konec-->
                  <br />
                  <xsl:if test="($perm!='ro' or $skupina_test='true') or lemma/grammar_note[1]/@status!='hidden'">
                    <xsl:if test="lemma/lemma_type!='collocation' and lemma/grammar_note/@slovni_druh!=''">
                      <xsl:choose>
                        <xsl:when test="lemma/grammar_note/@slovni_druh='klf' or lemma/grammar_note/@slovni_druh='spc'"></xsl:when>
                        <xsl:otherwise><strong><xsl:value-of select="$loc-lexkategorie"/>: </strong></xsl:otherwise>
                      </xsl:choose>
                      <xsl:for-each select="lemma/grammar_note">
                        <xsl:if test="@slovni_druh!=''">
                          <xsl:if test="count(//lemma/grammar_note)&gt;1"><strong><xsl:text> </xsl:text><xsl:number/>) </strong></xsl:if>
                          <xsl:choose>
                            <xsl:when test="@slovni_druh='subst'"><xsl:value-of select="$loc-lexsubst"/></xsl:when>
                            <xsl:when test="@slovni_druh='verb'"><xsl:value-of select="$loc-lexverb"/></xsl:when>
                            <xsl:when test="@slovni_druh='modif'"><xsl:value-of select="$loc-lexmodif"/></xsl:when>
                            <xsl:when test="@slovni_druh='pron'"><xsl:value-of select="$loc-lexpron"/></xsl:when>
                            <xsl:when test="@slovni_druh='num'"><xsl:value-of select="$loc-lexnum"/></xsl:when>
                            <xsl:when test="@slovni_druh='konj'"><xsl:value-of select="$loc-lexkonj"/></xsl:when>
                            <xsl:when test="@slovni_druh='part'"><xsl:value-of select="$loc-lexpart"/></xsl:when>
                            <xsl:when test="@slovni_druh='taz'"><xsl:value-of select="$loc-lextaz"/></xsl:when>
                            <xsl:when test="@slovni_druh='kat'"><xsl:value-of select="$loc-lexkat"/></xsl:when>
                            <xsl:when test="@slovni_druh='klf'"><xsl:value-of select="$loc-lexklf"/></xsl:when>
                            <xsl:when test="@slovni_druh='spc'"><xsl:value-of select="$loc-lexspc"/></xsl:when>
                          </xsl:choose>
                          <xsl:if test="@skupina!=''">
                            <xsl:text> <!--, typ:--></xsl:text>
                            <xsl:choose>
                              <!-- <xsl:when test="@skupina='konk'">konkrétní</xsl:when> -->
                              <!-- <xsl:when test="@skupina='abs'">abstraktní</xsl:when> -->
                              <xsl:when test="@skupina='jmen'"><xsl:value-of select="$loc-jmen"/></xsl:when>
                              <xsl:when test="@skupina='proste'"><xsl:value-of select="$loc-proste"/></xsl:when>
                              <xsl:when test="@skupina='prostor' and @slovni_druh='verb'"><xsl:value-of select="$loc-prostorv"/></xsl:when>
                              <xsl:when test="@skupina='shodove'"><xsl:value-of select="$loc-shodove"/></xsl:when>
                              <xsl:when test="@skupina='modal'"><xsl:value-of select="$loc-modal"/></xsl:when>
                              <xsl:when test="@skupina='ukaz'"><xsl:value-of select="$loc-ukaz"/></xsl:when>
                              <xsl:when test="@skupina='priv'"><xsl:value-of select="$loc-priv"/></xsl:when>
                              <xsl:when test="@skupina='klas'"><xsl:value-of select="$loc-klas"/></xsl:when>
                              <xsl:when test="@skupina='kval'"><xsl:value-of select="$loc-kval"/></xsl:when>
                              <xsl:when test="@skupina='cas'"><xsl:value-of select="$loc-cas"/></xsl:when>
                              <xsl:when test="@skupina='dej'"><xsl:value-of select="$loc-dej"/></xsl:when>
                              <xsl:when test="@skupina='prostor' and @slovni_druh='modif'"><xsl:value-of select="$loc-prostorm"/></xsl:when>
                              <xsl:when test="@skupina='zakl'"><xsl:value-of select="$loc-zakl"/></xsl:when>
                              <xsl:when test="@skupina='rad'"><xsl:value-of select="$loc-rad"/></xsl:when>
                              <xsl:when test="@skupina='nas'"><xsl:value-of select="$loc-nas"/></xsl:when>
                              <xsl:when test="@skupina='ikon'"><xsl:value-of select="$loc-ikon"/></xsl:when>
                              <xsl:when test="@skupina='neur'"><xsl:value-of select="$loc-neur"/></xsl:when>
                              <xsl:when test="@skupina='partneg'"><xsl:value-of select="$loc-partneg"/></xsl:when>
                              <xsl:when test="@skupina='partcont'"><xsl:value-of select="$loc-partcont"/></xsl:when>
                            </xsl:choose>
                          </xsl:if>
                          <xsl:if test="@skupina2!=''">
                            <xsl:text> - <!--, podtyp:--></xsl:text>
                            <xsl:call-template name="split_skupina2"><xsl:with-param name="text" select="@skupina2"/></xsl:call-template>
                          </xsl:if>
                          <xsl:if test="@skupina3!=''">
                            <xsl:text>; </xsl:text>
                            <xsl:choose>
                              <!-- <xsl:when test="@slovni_druh='verb'">podtyp II: </xsl:when>-->
                              <xsl:when test="@slovni_druh='subst' and @skupina3!='netvori'"><xsl:value-of select="$loc-pluralu"/>: </xsl:when>
                              <xsl:when test="@slovni_druh='cas'"><xsl:value-of select="$loc-pluralu"/>: </xsl:when>
                            </xsl:choose>
                            <xsl:call-template name="split_skupina3"><xsl:with-param name="text" select="@skupina3"/></xsl:call-template>
                          </xsl:if>;
                        </xsl:if>
                      </xsl:for-each>
                    </xsl:if>
                    <xsl:if test="lemma/grammar_note/@slovni_druh!=''"><br /></xsl:if>
                    <xsl:if test="lemma/grammar_note/@oral_komp!=''"><b><xsl:value-of select="$loc-oralni"/>: </b>
                      [<xsl:value-of select="lemma/grammar_note/@oral_komp"/>]
                      <xsl:choose>
                        <!-- <xsl:when test="lemma/grammar_note/@oral_komp_sel='nezadano'"> <em>nezadáno</em></xsl:when>-->
                        <xsl:when test="lemma/grammar_note/@oral_komp_sel='povinny'"> - <em><xsl:value-of select="$loc-povinny"/></em></xsl:when>
                        <xsl:when test="lemma/grammar_note/@oral_komp_sel='nepovinny'"> - <em><xsl:value-of select="$loc-nepovinny"/></em></xsl:when>
                      </xsl:choose>
                      <xsl:if test="lemma/grammar_note/@mluv_komp!=''">, </xsl:if>
                    </xsl:if>
                    <xsl:if test="lemma/grammar_note/@mluv_komp!=''"><b><xsl:value-of select="$loc-mluvni"/>: </b>
                      [<xsl:value-of select="lemma/grammar_note/@mluv_komp"/>]
                      <xsl:choose>
                        <!--<xsl:when test="lemma/grammar_note/@mluv_komp_sel='nezadano'"> - <em>nezadáno</em></xsl:when>-->
                        <xsl:when test="lemma/grammar_note/@mluv_komp_sel='povinny'"> - <em><xsl:value-of select="$loc-povinny"/></em></xsl:when>
                        <xsl:when test="lemma/grammar_note/@mluv_komp_sel='nepovinny'"> - <em><xsl:value-of select="$loc-nepovinny"/></em></xsl:when>
                      </xsl:choose>
                    </xsl:if>
                    <xsl:if test="lemma/grammar_note/@mluv_komp!='' or lemma/grammar_note/@oral_komp!=''"><br /></xsl:if>
                    <xsl:if test="lemma/grammar_note!=''">
                      <xsl:apply-templates mode="grammar_note" select="lemma/grammar_note"/>
                    </xsl:if>

                  </xsl:if>
                  <xsl:if test="lemma/grammar_note/file"><br /></xsl:if>
                  <xsl:for-each select="lemma/grammar_note/file">
                    <xsl:variable name="mid">
                      <xsl:value-of select="@media_id"/>
                    </xsl:variable>
                    <xsl:apply-templates select="//media/file[(@id=$mid or id=$mid) and main_for_entry and main_for_entry/@completeness!='1']" mode="var">
                      <xsl:with-param name="variant_sw" select="swmix"/>
                      <xsl:with-param name="in_grammar">true</xsl:with-param>
                      <xsl:with-param name="in_note">G<xsl:value-of select="position()"/></xsl:with-param>
                    </xsl:apply-templates>
                    <xsl:apply-templates select="//media/file[(@id=$mid or id=$mid) and (not(main_for_entry) or main_for_entry/@completeness='1')]">
                      <xsl:with-param name="in_grammar">true</xsl:with-param>
                      <xsl:with-param name="in_note">G<xsl:value-of select="position()"/></xsl:with-param>
                    </xsl:apply-templates>
                  </xsl:for-each>
                  <div class="cleaner"></div>
                  <xsl:if test="lemma/grammar_note/variant">
                    <div class="czj-lemma-h2"><xsl:value-of select="$loc-gramvar"/></div>
                    <div class="czj-relation-wrapper">
                      <xsl:apply-templates select="lemma/grammar_note/variant" mode="file">
                        <xsl:sort data-type="number" order="descending" select="((@lemma_region='cr')*3) + ((@lemma_region='cechy')*2) +((@lemma_region='morava')*1)"/>
                        <xsl:with-param name="gram_var_count">0</xsl:with-param>
                      </xsl:apply-templates>
                    </div>
                  </xsl:if>
                  <xsl:if test="(lemma/lemma_type='derivat' or lemma/lemma_type='kompozitum') and (($perm!='ro' or $skupina_test='true') or collocations/@status!='hidden')">   
                    <div id="lemma_parts">
                      <xsl:if test="lemma/grammar_note"><br /></xsl:if>                                         
                      <strong>
                      <xsl:if test="lemma/lemma_type='kompozitum'"><xsl:value-of select="$loc-slozenim"/></xsl:if>
                      <xsl:if test="lemma/lemma_type='derivat'"><xsl:value-of select="$loc-odvozenim"/></xsl:if>: 
                      </strong><a style="cursor:pointer;color:blue"  onclick="change_view('lemma_parts_box')" id="lemma_parts_info"><xsl:value-of select="$loc-podrobnosti"/></a><br />
                      <span id="lemma_parts_box" style="display:none">
                        <div class="czj-relation-wrapper">
                          <xsl:apply-templates select="collocations/colloc"/>
                        </div>
                      </span>                      
                    </div>
                  </xsl:if>
                </div>
                <!-- czj-gram-popis -->
              </xsl:if>
              <xsl:if test="(lemma/style_note/file or lemma/style_note/variant or lemma/grammar_note/@region!='' or lemma/style_note/@kategorie!='' or lemma/style_note/@generace!='' or lemma/style_note/text()!='' or lemma/style_note/@gender!='' or lemma/puvod!='') and (lemma/style_note/@status!='hidden' or ($perm!='ro' or $skupina_test='true'))">
                <div class="czj-styl-popis">
                  <!--<img src="/editor/img/grey-corner.png" style="position: relative; top: 0px; left: -45px;" valign="top"/>-->
                  <xsl:if test="lemma/style_note/@source!=''">
                    <span style="float: right;" class="source"><xsl:value-of select="$loc-zdroj"/>: <xsl:value-of select="lemma/style_note/@source"/>;
                    </span>
                  </xsl:if>
                  <!--<div class="czj-lemma-h1">Stylistický popis</div>-->
                  <div class="author-info" style="display:none">
                    <xsl:if test="lemma/style_note/@author!=''">
                      <span class="author-author"><xsl:value-of select="$loc-autor"/>: <a href="/{$dictcode}?lang={$lang}&amp;action=page&amp;page=about#skupiny"><xsl:value-of select="lemma/style_note/@author"/></a>;</span>
                    </xsl:if>
                    <!--<span class="author-copyright"><xsl:value-of select="$loc-autorvideo"/>: <xsl:value-of select="lemma/style_note/@copyright"/></span>-->
                  </div>
                  <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_lingvist')">
                    <span class="set-status"><xsl:value-of select="$loc-stylpopis"/>
                      <xsl:call-template name="status_publish">
                        <xsl:with-param name="status" select="/entry/lemma/style_note[1]/@status"/>
                        <xsl:with-param name="type">style</xsl:with-param>
                      </xsl:call-template>
                    </span>
                  </xsl:if>
                  <!--<xsl:if test="lemma/style_note/file or lemma/style_note/variant or lemma/grammar_note/@region!='' or lemma/style_note/@kategorie!='' or lemma/style_note/@generace!='' or lemma/style_note/text()!='' or lemma/puvod!='' or lemma/style_note/@gender!=''">-->
                    <br/>
                    <div id="styletop">
                      <div class="grammar">
                        <xsl:if test="($perm!='ro' or $skupina_test='true') or lemma/style_note/@status!='hidden'">
                          <xsl:if test="lemma/style_note/@kategorie != ''">
                            <div>
                              <xsl:choose>
                                <xsl:when test="lemma/style_note/@kategorie='neo'"><xsl:value-of select="$loc-neo"/></xsl:when>
                                <xsl:when test="lemma/style_note/@kategorie='arch'"><xsl:value-of select="$loc-arch"/></xsl:when>
                                <xsl:otherwise><xsl:value-of select="lemma/puvod"/></xsl:otherwise>
                              </xsl:choose>
                            </div>
                          </xsl:if>
                          <xsl:if test="lemma/grammar_note/@region != ''">
                            <div><b><xsl:value-of select="$loc-oblast"/>: </b>
                              <img src="/media/img/region/cr.png" class="oblast"/>
                              <xsl:call-template name="split_region">
                                <xsl:with-param name="text" select="lemma/grammar_note/@region"/>
                              </xsl:call-template>
                            </div>
                            </xsl:if>
                            <xsl:if test="lemma/puvod !=''">
                              <div>
                                <b><xsl:value-of select="$loc-prejato"/>: </b>
                                <xsl:choose>
                                  <xsl:when test="lemma/puvod='asl'"><xsl:value-of select="$loc-asl"/></xsl:when>
                                  <xsl:when test="lemma/puvod='bsl'"><xsl:value-of select="$loc-bsl"/></xsl:when>
                                  <xsl:when test="lemma/puvod='spj'"><xsl:value-of select="$loc-spj"/></xsl:when>
                                  <xsl:when test="lemma/puvod='ogs'"><xsl:value-of select="$loc-ogs"/></xsl:when>
                                  <xsl:when test="lemma/puvod='pjm'"><xsl:value-of select="$loc-pjm"/></xsl:when>
                                  <xsl:when test="lemma/puvod='dgs'"><xsl:value-of select="$loc-dgs"/></xsl:when>
                                  <xsl:when test="lemma/puvod='is'"><xsl:value-of select="$loc-is"/></xsl:when>
                                  <xsl:when test="lemma/puvod='loc'"><xsl:value-of select="$loc-loc"/></xsl:when>
                                  <xsl:otherwise><xsl:value-of select="lemma/puvod"/></xsl:otherwise>
                              </xsl:choose>  </div>
                            </xsl:if>
                          
                          <xsl:if test="lemma/style_note/@generace != ''">
                            <div><b><xsl:value-of select="$loc-vekova"/>: </b>
                              <xsl:call-template name="split_generace">
                                <xsl:with-param name="text" select="lemma/style_note/@generace"/>
                              </xsl:call-template>
                            </div>
                          </xsl:if>
                          <xsl:if test="lemma/style_note/@gender != ''">
                            <div>
                              <xsl:choose>
                                <xsl:when test="lemma/style_note/@gender='m'"><xsl:value-of select="$loc-muzsky"/></xsl:when>
                                <xsl:when test="lemma/style_note/@gender='f'"><xsl:value-of select="$loc-zensky"/></xsl:when>
                                <xsl:otherwise><xsl:value-of select="lemma/puvod"/></xsl:otherwise>
                              </xsl:choose>
                            </div>
                          </xsl:if>
                        </xsl:if>
                        <xsl:apply-templates mode="style_note" select="lemma/style_note"/>
                        <xsl:if test="lemma/style_note/file">
                          <br/>
                        </xsl:if>
                        <xsl:for-each select="lemma/style_note/file">
                          <xsl:variable name="mid">
                            <xsl:value-of select="@media_id"/>
                          </xsl:variable>
                          <xsl:apply-templates select="//media/file[(@id=$mid or id=$mid) and main_for_entry and main_for_entry/@completeness!='1']" mode="var">
                            <xsl:with-param name="variant_sw" select="swmix"/>
                            <xsl:with-param name="in_grammar">true</xsl:with-param>
                            <xsl:with-param name="in_note">S<xsl:value-of select="position()"/>
                            </xsl:with-param>
                          </xsl:apply-templates>
                          <xsl:apply-templates select="//media/file[(@id=$mid or id=$mid) and (not(main_for_entry) or main_for_entry/@completeness='1')]">
                            <xsl:with-param name="in_grammar">true</xsl:with-param>
                            <xsl:with-param name="in_note">S<xsl:value-of select="position()"/>
                            </xsl:with-param>
                          </xsl:apply-templates>
                        </xsl:for-each>
                        <div class="cleaner">
                        </div>
                        <xsl:if test="lemma/style_note/variant">
                          <div class="czj-lemma-h2"><xsl:value-of select="$loc-stylvar"/></div>
                          <div class="czj-relation-wrapper">
                            <xsl:apply-templates select="lemma/style_note/variant" mode="file">
                              <xsl:sort data-type="number" order="descending" select="((@lemma_region='cr')*3) + ((@lemma_region='cechy')*2) +((@lemma_region='morava')*1)"/>
                              <xsl:with-param name="gram_var_count" select="count(lemma/grammar_note/variant)"/>
                            </xsl:apply-templates>
                          </div>
                        </xsl:if>
                      </div>
                    </div>
                    <!--</xsl:if>-->
                </div>
                <!-- czj-styl-popis -->
              </xsl:if>

              <div class="czj-vyznam">
                <!-- <img src="/editor/img/grey-corner.png" style="position: relative; top: 0px; left: -45px;" valign="top"/> -->
                <div id="meanings">
                  <xsl:apply-templates select="meanings/meaning[($perm!='ro' or $skupina_test='true') or (status!='hidden' and (//lemma/completeness='100' or //lemma/completeness='2' or //lemma/@auto_complete='1'))]">
                    <xsl:sort select="@number" data-type="number"/>
                  </xsl:apply-templates>
                  <div style="clear:both">
                  </div>
                </div>
              </div>
              <!-- czj-vyznam -->
              <div id="collocations"><!--slovni spojeni-->
                <xsl:if test="count(collocations/revcolloc[@lemma_type='collocation' and (($perm!='ro' or $skupina_test='true') or @auto_complete='1')])>0">
                  <!--<span class="divtitle">Slovní spojení s tímto heslem</span>-->
                  <img src="/editor/img/grey-corner.png" style="position: relative; top: 0px; left: -45px;" valign="top"/>
                  <div class="czj-lemma-h1"><xsl:value-of select="$loc-sheslem"/></div>
                  <div class="collocations">
                    <xsl:apply-templates select="collocations/revcolloc[@lemma_type='collocation' and (($perm!='ro' or $skupina_test='true') or @auto_complete='1') and @completeness!='1']"/>
                  </div>
                  <div style="clear:both">
                  </div>
                </xsl:if>
              </div>
              <input type="button" class="button-zdroje" value="zobrazit/skrýt autory a zdroje" id="show-author" onclick="$('.author-info').toggle();"/>
        </div></div></div>  <!-- czj-right -->
      </div>   <!-- czj-wrapper -->
      <div class="cleaner"></div>
    </div> <!-- czj-container -->
    <div id="outer">
      <div id="side">
        <div id="sidevideo" style="">
          <!--<xsl:apply-templates select="media/file[type='sign_front' or type='sign_side']" mode="side"/>-->
        </div>
        <div id="sidesw" style="display:none">
          <xsl:apply-templates select="lemma/swmix/sw" mode="rel"/>
        </div>
        <div id="sidehamnosys" style="display:none">
          <xsl:if test="lemma/hamnosys and lemma/hamnosys!=''">
            <img width="200" src="http://znaky.zcu.cz/proxy/tts/tex2img.png?generator[template]=hamnosys&amp;generator[dpi]=200&amp;generator[engine]=x&amp;generator[tex]={lemma/hamnosys}"/>
          </xsl:if>
        </div>
      </div>
    </div>
  </body>
</html>
  </xsl:template>
  <xsl:template match="file" mode="grammar_note">
    <em><xsl:value-of select="$loc-viz"/> G<xsl:value-of select="count(preceding-sibling::file)+1"/>
    </em>
  </xsl:template>
  <xsl:template match="file" mode="style_note">
    <em><xsl:value-of select="$loc-viz"/> S<xsl:value-of select="count(preceding-sibling::file)+1"/>
    </em>
  </xsl:template>

  <xsl:template match="sw">
    <xsl:if test=".!=''">
      <span class="sw" style="vertical-align:top; display:inline-block;">
        <div class="author-info" style="display:none"><!-- autorské položky -->
          <xsl:if test="@source!=''"><span class="author-source"><xsl:value-of select="$loc-zdroj"/>: <xsl:value-of select="@source"/>; </span></xsl:if>
          <span class="author-author"><xsl:value-of select="$loc-autor"/>: <a href="/{$dictcode}?lang={$lang}&amp;action=page&amp;page=about#skupiny"><xsl:value-of select="@author"/></a>; </span>
          <!--<span class="author-copyright"><xsl:value-of select="$loc-autorvideo"/>: <xsl:value-of select="@copyright"/></span>-->
        </div>
        <a href="http://znaky.zcu.cz/proxy/tts/signwriting.png?generator[sw]={.}&amp;generator[align]=top_left&amp;generator[set]=sw10" class="fancybox swlink ui-draggable" data-fancybox="images" data-sw="{.}">
          <img src="http://znaky.zcu.cz/proxy/tts/signwriting.png?generator[sw]={.}&amp;generator[align]=top_left&amp;generator[set]=sw10" alt="signwriting" class="signwriting-full" ratio="0.8"/>
        </a>
      </span>
    </xsl:if>
  </xsl:template>

  <xsl:template match="sw" mode="rel">
    <xsl:if test=".!=''">
      <a href="http://znaky.zcu.cz/proxy/tts/signwriting.png?generator[sw]={.}&amp;generator[align]=top_left&amp;generator[set]=sw10" data-fancybox="images" class="fancybox swlink ui-draggable" data-sw="{.}">
        <img src="http://znaky.zcu.cz/proxy/tts/signwriting.png?generator[sw]={.}&amp;generator[align]=top_left&amp;generator[set]=sw10" alt="signwriting" class="signwriting" ratio="0.5" style=""/>
      </a>
    </xsl:if>
  </xsl:template>

  <xsl:template match="variant" mode="style_note">
  </xsl:template>

  <xsl:template match="variant" mode="grammar_note">
  </xsl:template>

  <xsl:template match="variant" mode="file">
    <xsl:param name="gram_var_count"/>
    <xsl:variable name="f">
      <xsl:value-of select="./text()"/>
    </xsl:variable>
    <xsl:if test="($perm!='ro' or $skupina_test='true')">
      <xsl:apply-templates select="//media/file[(@id=$f) and (main_for_entry)]" mode="var">
        <xsl:with-param name="variant_sw" select="swmix"/>
        <xsl:with-param name="variant_pos" select="position()+$gram_var_count"/>
      </xsl:apply-templates>
      <xsl:apply-templates select="//media/file[(@id=$f) and (not(main_for_entry))]">
        <xsl:with-param name="parent_id" select="position()"/>
        <xsl:with-param name="variant_sw" select="swmix"/>
        <xsl:with-param name="variant_pos" select="position()+$gram_var_count"/>
      </xsl:apply-templates>
    </xsl:if>
    <xsl:if test="not($perm!='ro' or $skupina_test='true')">
      <xsl:apply-templates select="//media/file[(@id=$f) and (main_for_entry and main_for_entry/@completeness!='1')]" mode="var">
        <xsl:with-param name="variant_sw" select="swmix"/>
        <xsl:with-param name="variant_pos" select="position()+$gram_var_count"/>
      </xsl:apply-templates>
      <xsl:apply-templates select="//media/file[(@id=$f) and (not(main_for_entry) or main_for_entry/@completeness='1')]">
        <xsl:with-param name="parent_id" select="position()"/>
        <xsl:with-param name="variant_sw" select="swmix"/>
        <xsl:with-param name="variant_pos" select="position()+$gram_var_count"/>
      </xsl:apply-templates>
    </xsl:if>
    <xsl:if test="@desc!=''"><span class="author-info">pozn.: <xsl:value-of select="@desc"/></span><!-- desc varianta --></xsl:if>
  </xsl:template>

  <xsl:template match="file" mode="inline"><!--video význam -->
    <xsl:param name="media_id"/>
    <xsl:if test="(id=$media_id or @id=$media_id) and $media_id!=''">
      <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_lemmaczj') or contains($perm, 'revizor_video')"><!-- test="$perm!='ro' or $skupina_test='true'">-->
        <span class="publish"><xsl:value-of select="location"/></span>
        <br />
      </xsl:if>
      <xsl:if test="substring(location, string-length(location)-3, 4) = '.flv'">
        <span id="flowvideoin{@id}" data-ratio="0.8" class="flowplayer meaningfile" style="width:285px; background:#777 url(/media/video{$dictcode}/thumb/{location}/thumb.jpg) no-repeat; background-size: 285px 228px">
          <video>
            <source type="video/flash" src="/media/video{$dictcode}/{location}"/>
          </video>
        </span>
      </xsl:if>
      <xsl:if test="substring(location, string-length(location)-3, 4) = '.mp4'">
        <span id="flowvideoin{@id}" data-ratio="0.8" class="meaningfile" style="width:285px;">
          <video preload="none" controls="" width="285px" height="228px" poster="/media/video{$dictcode}/thumb/{location}/thumb.jpg">
          <xsl:if test="$video='webm'"> 
            <source type="video/webm" src="/media/video{$dictcode}/{location}.webm"/>
          </xsl:if>  
            <source type="video/mp4" src="/media/video{$dictcode}/{location}"/>
          </video>
        </span>
      </xsl:if>

      <xsl:choose>
        <xsl:when test="location='Synonymum-odkaz.mp4'"><span id="video-odkaz" style="color: white; position: relative;  top: -20px; left: -190px; z-index: 10; "><xsl:value-of select="$loc-vizsyn"/></span></xsl:when>
        <xsl:when test="location='Varianty-odkaz.mp4'"><span id="video-odkaz" style="color: white; position: relative;  top: -20px; left: -190px; z-index: 10; "><xsl:value-of select="$loc-vizvar"/></span></xsl:when>
      </xsl:choose>
      <br />
      <div style="float: left; width: 286px;" class="source">
        <xsl:if test="id_meta_source!='' or id_meta_author!=''">
          <span class="source">
            <xsl:choose>
              <xsl:when test="id_meta_source!=''"><xsl:value-of select="$loc-zdroj"/>: <xsl:value-of select="id_meta_source"/></xsl:when>
              <xsl:otherwise><xsl:value-of select="$loc-autor"/>: <a href="/{$dictcode}?lang={$lang}&amp;action=page&amp;page=about#skupiny"><xsl:value-of select="id_meta_author"/></a>;</xsl:otherwise>
            </xsl:choose>
            <xsl:text> </xsl:text></span>
        </xsl:if>

        <div class="author-info" style="display:none">
          <xsl:if test="id_meta_source!=''">
            <span class="author-author"><xsl:value-of select="$loc-autor"/>: <a href="/{$dictcode}?lang={$lang}&amp;action=page&amp;page=about#skupiny"><xsl:value-of select="id_meta_author"/></a>; </span>
          </xsl:if>
          <xsl:if test="id_meta_copyright!=''">
            <span class="author-copyright"><xsl:value-of select="$loc-autorvideo"/>: <xsl:value-of select="id_meta_copyright"/></span>
          </xsl:if>
        </div>
      </div>
    </xsl:if>
  </xsl:template>
  <!--<xsl:template match="file" mode="inlinetop">
    <span id="flowvideoin{@id}" data-ratio="0.8" class="flowplayer meaningfile topvideo{type}" style="width:285px; background:#777 url(/media/video{$dictcode}/thumb/{location}/thumb.jpg) no-repeat; background-size: 285px 228px">
      <video>
        <xsl:if test="substring(location, string-length(location)-3, 4) = '.flv'">
          <source type="video/flash" src="/media/video{$dictcode}/{location}"/>
        </xsl:if>
        <xsl:if test="substring(location, string-length(location)-3, 4) = '.mp4'">
          <source type="video/mp4" src="/media/video{$dictcode}/{location}"/>
        </xsl:if>
      </video>
    </span>
  </xsl:template>-->
  <!-- čelní / boční video -->
  <xsl:template match="file" mode="inlinetop2">
    <xsl:param name="video_type"/>
    <xsl:if test="substring(location, string-length(location)-3, 4) = '.flv'">
      <span id="flowvideoin{@id}" data-ratio="0.8" class="flowplayer meaningfile topvideo{$video_type}" style="width:285px; background:#777 url(/media/video{$dictcode}/thumb/{location}/thumb.jpg) no-repeat; background-size: 285px 228px">
        <video>
          <source type="video/flash" src="/media/video{$dictcode}/{location}"/>
        </video>
      </span>
    </xsl:if>
    <xsl:if test="substring(location, string-length(location)-3, 4) = '.mp4'">
      <span id="flowvideoin{@id}" data-ratio="0.8" class="meaningfile topvideo{$video_type}" style="width:285px;">
        <video preload="none" controls="" width="285px" height="228px" poster="/media/video{$dictcode}/thumb/{location}/thumb.jpg">
          <xsl:if test="$video='webm'"> 
            <source type="video/webm" src="/media/video{$dictcode}/{location}.webm"/>
          </xsl:if>
          <source type="video/mp4" src="/media/video{$dictcode}/{location}"/>
        </video>
      </span>
    </xsl:if>
    <xsl:if test="not(substring(location, string-length(location)-3, 4) = '.flv' or substring(location, string-length(location)-3, 4) = '.mp4')">
      <span  class="meaningfile topvideo{$video_type}" style="width:285px; background:#000 url(/media/video{$dictcode}/{location}) no-repeat; background-size: 285px 228px">
        <img style="width:285px;height:228px" src="/media/video{$dictcode}/{location}"/>
      </span>
    </xsl:if>
  </xsl:template>

  <xsl:template match="file" mode="side">
    <xsl:if test="substring(location, string-length(location)-3, 4) = '.flv'">
      <div id="flowvideo{id}" data-ratio="0.8" class="flowplayer flowmouse" style="float: left; width:120px; background:#777 url(/media/video{$dictcode}/thumb/{location}/thumb.jpg) no-repeat; background-size: 120px 96px">
        <video loop="loop">
          <source type="video/flash" src="/media/video{$dictcode}/{location}"/>
        </video>
      </div>
    </xsl:if>
    <xsl:if test="substring(location, string-length(location)-3, 4) = '.mp4'">
      <div id="flowvideo{id}" data-ratio="0.8" class="flowmouse" style="float: left; width:120px; background:#000 url(/media/video{$dictcode}/thumb/{location}/thumb.jpg) no-repeat; background-size: 120px 96px">
        <video preload="none" loop="loop" width="120px" height="96px" poster="/media/video{$dictcode}/thumb/{location}/thumb.jpg" onmouseover="this.play()" onmouseout="this.pause()">
          <source type="video/mp4" src="/media/video{$dictcode}/{location}"/>
        </video>
      </div>
    </xsl:if>

  </xsl:template>

  <xsl:template match="file" mode="rel">
    <xsl:param name="target"><xsl:value-of select="$dictcode"/></xsl:param>
    <xsl:if test="substring(location, string-length(location)-3, 4) = '.flv'">
      <div id="flowvideorel{id}" data-ratio="0.8" class="flowplayer flowmouse" style="width:120px; background:#777 url(/media/video{$target}/thumb/{location}/thumb.jpg) no-repeat; background-size: 120px 96px">
        <video loop="loop">
          <source type="video/flash" src="/media/video{$target}/{location}"/>
        </video>
      </div>
    </xsl:if>
    <xsl:if test="substring(location, string-length(location)-3, 4) = '.mp4'">
      <div id="flowvideorel{id}" data-ratio="0.8" class="flowmouse" style="width:120px; background: url(/media/video{$target}/thumb/{location}/thumb.jpg) no-repeat; background-size: 120px 96px; float:left;">
        <xsl:attribute name="style">width:120px; background: url(/media/video<xsl:value-of select="$target"/>/thumb/<xsl:value-of select="location"/>/thumb.jpg) no-repeat; background-size: 120px 96px; float:left;
          <xsl:if test="$perm!='ro' or @auto_complete='1'">cursor: pointer;</xsl:if>
        </xsl:attribute>
        <xsl:if test="$perm!='ro' or ../@auto_complete='1'">
          <xsl:attribute name="data-entryid"><xsl:value-of select="../@lemma_id"/></xsl:attribute>
          <xsl:attribute name="data-dict"><xsl:value-of select="$target"/></xsl:attribute>
        </xsl:if>
        <video preload="none" loop="loop" width="120px" height="96px" poster="/media/video{$target}/{location}/thumb.jpg" onmouseover="this.play()" onmouseout="this.pause()">
        <xsl:if test="$video='webm'">  
          <source type="video/webm" src="/media/video{$target}/{location}.webm"/>
        </xsl:if>
          <source type="video/mp4" src="/media/video{$target}/{location}"/>
        </video>
      </div>
    </xsl:if>

  </xsl:template>
  <xsl:template match="file" mode="var">
    <xsl:param name="var_id"/>
    <xsl:param name="in_grammar"/>
    <xsl:param name="in_note"/>
    <xsl:param name="variant_sw"/>
    <xsl:param name="variant_pos"/>
    <div class="czj-relation-item">
      <div class="czj-relation relationvideo">
        <div class="czj-lemma-preview-sm">
          <div class="czj-lemma-preview-sm-header">
            <xsl:if test="$in_note!=''">
              <div class="alignleft">
                <xsl:value-of select="$in_note"/>
              </div>
            </xsl:if>
            <xsl:if test="$in_note=''">
              <div class="alignleft">V<xsl:value-of select="$variant_pos"/>
                <xsl:if test="$perm!='ro' or $skupina_test='true'">
                  <span class="set-status">
                    <xsl:value-of select="main_for_entry/@lemma_id"/>
                  </span>
                </xsl:if>
              </div>
            </xsl:if>
            <div class="alignright">
              <xsl:if test="@auto_complete='1' or main_for_entry/@auto_complete='1' or main_for_entry/@skupina_test='true'">  <!--implementovat do dalších jazyků true-->
                <a href="/{$dictcode}?lang={$lang}&amp;action=search&amp;getdoc={main_for_entry/@lemma_id}&amp;empty={$empty}">
                  <img src="/media/img/slovnik-open_w.png" height="20"/></a>
              </xsl:if>
            </div>
          </div>
        </div>
        <div style="clear:both">
        </div>
        <xsl:if test="substring(location, string-length(location)-3, 4) = '.flv'">
          <div id="flowvideovar{@id}" data-ratio="0.8" class="flowplayer flowmouse" style="width:120px; background:#000 url(/media/video{$dictcode}/thumb/{location}/thumb.jpg) no-repeat; background-size: 120px 96px">
            <video loop="loop">
              <source type="video/flash" src="/media/video{$dictcode}/{location}"/>
            </video>
          </div>
        </xsl:if>
        <xsl:if test="substring(location, string-length(location)-3, 4) = '.mp4'">
          <div id="flowvideovar{@id}" data-ratio="0.8" class="flowmouse" style="width:120px; height:96px background: url(/media/video{$dictcode}/thumb/{location}/thumb.jpg) no-repeat; background-size: 120px 96px; float: left; background-color: black;">
            <video preload="none" width="120px" height="96px" loop="loop" poster="/media/video{$dictcode}/thumb/{location}/thumb.jpg" onmouseover="this.play()" onmouseout="this.pause()">
            <xsl:if test="$video='webm'">  
              <source type="video/webm" src="/media/video{$dictcode}/{location}.webm"/>
            </xsl:if>  
              <source type="video/mp4" src="/media/video{$dictcode}/{location}"/>
            </video>
          </div>
        </xsl:if>

        <div class="czj-lemma-preview-sm-sw">
          <span class="sw transsw" style="vertical-align: top; display: inline-block; height: 96px;">
            <xsl:if test="($variant_sw/sw)">
              <xsl:apply-templates select="$variant_sw/sw" mode="rel"/>
            </xsl:if>
            <xsl:if test="not($variant_sw/sw)">
              <xsl:if test="main_for_entry[1]/@lemma_type='single' or not(main_for_entry[1]/@lemma_type)">
                <xsl:if test="main_for_entry[1]/swmix/sw[@primary='true']">
                  <xsl:apply-templates select="main_for_entry[1]/swmix/sw[@primary='true']" mode="rel"/>
                </xsl:if>
                <xsl:if test="not(main_for_entry[1]/swmix/sw[@primary='true'])">
                  <xsl:apply-templates select="main_for_entry[1]/swmix/sw[1]" mode="rel"/>
                </xsl:if>
              </xsl:if>
              <xsl:if test="main_for_entry[1]/@lemma_type!='single'">
                <xsl:apply-templates select="main_for_entry[1]/swmix/sw" mode="rel"/>
              </xsl:if>
            </xsl:if>
          </span>
        </div>
        <xsl:if test="@auto_complete='1' or main_for_entry/@auto_complete='1' or contains($perm, 'admin')">
          <script type="text/javascript">
            $("#flowvideovar<xsl:value-of select="@id"/>").on("click", function(e) {
            window.location='/<xsl:value-of select="$dictcode"/>?lang=<xsl:value-of select="$lang"/>&amp;action=search&amp;getdoc=<xsl:value-of select="main_for_entry/@lemma_id"/>&amp;empty=<xsl:value-of select="$empty"/>';
            });
          </script>
        </xsl:if>
      </div>
    </div>
  </xsl:template>
  <xsl:template match="file" mode="colloc">
    <xsl:if test="substring(location, string-length(location)-3, 4) = '.flv'">
      <div id="flowvideocol{id}" data-ratio="0.8" class="flowplayer flowmouse" style="width:120px; background:#000 url(/media/video{$dictcode}/thumb/{location}/thumb.jpg) no-repeat; background-size: 120px 96px">
        <video loop="loop">
          <source type="video/flash" src="/media/video{$dictcode}/{location}"/>
        </video>
      </div>
    </xsl:if>
    <xsl:if test="substring(location, string-length(location)-3, 4) = '.mp4'">
      <div id="flowvideocol{id}" data-ratio="0.8" class="flowmouse" style="width:120px; background:#000 url(/media/video{$dictcode}/thumb/{location}/thumb.jpg) no-repeat; background-size: 120px 96px; float: left;">
        <video preload="none" loop="loop" width="120px" height="96px" poster="/media/video{$dictcode}/thumb/{location}/thumb.jpg" onmouseover="this.play()" onmouseout="this.pause()">
        <xsl:if test="$video='webm'">  
          <source type="video/webm" src="/media/video{$dictcode}/{location}.webm"/>
        </xsl:if>  
          <source type="video/mp4" src="/media/video{$dictcode}/{location}"/>
        </video>
      </div>
    </xsl:if>
    <script type="text/javascript">
      $("#flowvideocol<xsl:value-of select="id"/>").on("click", function(e) {
      open_iframe('<xsl:value-of select="../@lemma_id"/>', 'czj');
      });
    </script>
  </xsl:template>
  <xsl:template match="file">
    <xsl:param name="parent_id"/>
    <xsl:param name="variant_sw"/>
    <xsl:param name="variant_pos"/>
    <xsl:param name="in_grammar"/>
    <xsl:param name="in_note"/>
    <div class="czj-relation-item">
      <xsl:if test="not($variant_sw)">
        <xsl:attribute name="style">background-color: grey;>
        </xsl:attribute>
      </xsl:if>
      <div class="czj-relation relationvideo">
        <div class="czj-lemma-preview-sm">
          <div class="czj-lemma-preview-sm-header" style="background-color: none;">
            <xsl:if test="$in_note!=''">
              <div class="alignleft">
                <xsl:value-of select="$in_note"/>
              </div>
            </xsl:if>
            <xsl:if test="$in_note=''">
              <div class="alignleft">V<xsl:value-of select="$variant_pos"/>
              </div>
            </xsl:if>
            <div class="alignright">
            </div>
          </div>
        </div>
        <xsl:if test="substring(location, string-length(location)-3, 4) = '.flv'">
          <div id="flowvideo{@id}-{$parent_id}" data-ratio="0.8" class="flowplayer flowmouse" style="width:120px; background:#777 url(/media/video{$dictcode}/thumb/{location}/thumb.jpg) no-repeat; background-size: 120px 96px">
            <video loop="loop">
              <source type="video/flash" src="/media/video{$dictcode}/{location}"/>
            </video>
          </div>
        </xsl:if>

        <xsl:if test="substring(location, string-length(location)-3, 4) = '.mp4'">
          <div id="flowvideovar{@id}" data-ratio="0.8" class="flowmouse" style="width:120px; height:96px background: url(/media/video{$dictcode}/thumb/{location}/thumb.jpg) no-repeat; background-size: 120px 96px; float: left; background-color: black; cursor: zoom-in;">
            <video preload="none" width="120px" height="96px" loop="loop" poster="/media/video{$dictcode}/thumb/{location}/thumb.jpg" onmouseover="this.play()" onmouseout="this.pause()">
            <xsl:if test="$video='webm'">
              <source type="video/webm" src="/media/video{$dictcode}/{location}.webm"/>
            </xsl:if>  
              <source type="video/mp4" src="/media/video{$dictcode}/{location}"/>
            </video>
          </div>
        </xsl:if>

        <xsl:if test="$variant_sw">
          <div class="czj-lemma-preview-sm-sw">
            <span class="sw transsw" style="vertical-align: top; display: inline-block; height: 96px;">
              <xsl:apply-templates select="$variant_sw/sw" mode="rel"/>
            </span>
          </div>
        </xsl:if>
        <xsl:if test="main_for_entry/sw and not($variant_sw)">
          <div class="czj-lemma-preview-sm-sw">
            <span class="sw transsw" style="vertical-align: top; display: inline-block; height: 96px;">
              <!--<span class="sw transsw" style="vertical-align:top; display:inline-block;">-->
                <xsl:if test="main_for_entry/sw[@primary='true']">
                  <xsl:apply-templates select="main_for_entry/sw[@primary='true']" mode="rel"/>
                </xsl:if>
                <xsl:if test="not(main_for_entry/sw[@primary='true'])">
                  <xsl:apply-templates select="main_for_entry/sw[1]" mode="rel"/>
                </xsl:if>
              </span>
            </div>
          </xsl:if>
        </div>
      </div>
      <xsl:if test="substring(location, string-length(location)-3, 4) = '.mp4'">
        <script type="text/javascript">
          $("#flowvideovar<xsl:value-of select="@id"/>").on("click", function(e) {
          e.target.pause();
          var container = $('<div data-ratio="0.8" style="width:335px;">
            <video preload="none" controls="" width="285px" height="228px" poster="/media/video{$dictcode}/thumb/{location}/thumb.jpg" autoplay="">
            <xsl:if test="$video='webm'">
              <source type="video/webm" src="/media/video{$dictcode}/{location}.webm"/>
            </xsl:if>  
              <source type="video/mp4" src="/media/video{$dictcode}/{location}"/>
            </video>
          </div>');
          $.fancybox.open({
          src: container,
          type: 'html',
          scrolling: 'no',
          });
          });
        </script>
      </xsl:if>
      <xsl:if test="substring(location, string-length(location)-3, 4) = '.flv'">
        <script type="text/javascript">
          $("#flowvideovar<xsl:value-of select="@id"/>").on("click", function(e) {
          flowplayer(this).pause();
          var container = $('<div/>');
          console.log(container);
          container.css('background', '#777 url(/media/video<xsl:value-of select="$dictcode"/>/thumb/<xsl:value-of select="location"/>/thumb.jpg) no-repeat');
          container.css('background-size', '285px 228px');
          var video_type = ('/media/video<xsl:value-of select="$dictcode"/>/<xsl:value-of select="location"/>'.substr(-3, 3) == 'mp4')? 'mp4':'flash';
          container.flowplayer({
          autoPlay: true,
          autoBuffering: true,
          width: 285,
          height: 228,
          ratio: 0.8,
          playlist: [
          [{'>': '/media/video<xsl:value-of select="$dictcode"/>/<xsl:value-of select="location"/>'}]
          ],
          });
          $.fancybox({
          content: container,
          width: 285,
          height: 228,
          scrolling: 'no',
          autoSize: false
          });
          flowplayer(container[0]).load([{'flash': '/media/video<xsl:value-of select="$dictcode"/>/<xsl:value-of select="location"/>'}]);
          flowplayer(container[0]).resume();
          });
        </script>
      </xsl:if>
    </xsl:template>

    <xsl:template match="file" mode="usage"><!-- příklady užití -->
      <xsl:param name="parent_id"/>
      <xsl:param name="variant_sw"/>
      <div class="usage">
        <xsl:if test="substring(location, string-length(location)-3, 4) = '.flv'">
          <div id="flowvideo{@id}-{$parent_id}" data-ratio="0.8" class="flowplayer flowmouse usage" style="width:120px; background:#777 url(/media/video{$dictcode}/thumb/{location}/thumb.jpg) no-repeat; background-size: 120px 96px;">
            <video preload="none" loop="loop">
              <source type="video/flash" src="/media/video{$dictcode}/{location}"/>
            </video>
          </div><br />
        </xsl:if>
        <xsl:if test="substring(location, string-length(location)-3, 4) = '.mp4'">
          <div id="flowvideo{@id}-{$parent_id}" data-ratio="0.8" class="usage" style="width:120px; cursor: zoom-in;">
            <video preload="none" width="120px" height="96px" poster="/media/video{$dictcode}/thumb/{location}/thumb.jpg" onmouseover="this.play()" onmouseout="this.pause()">
            <xsl:if test="$video='webm'">
              <source type="video/webm" src="/media/video{$dictcode}/{location}.webm"/>
            </xsl:if>  
              <source type="video/mp4" src="/media/video{$dictcode}/{location}"/>
            </video>
          </div>
        </xsl:if>
        <xsl:choose>
          <xsl:when test="location='Synonymum-odkaz.mp4'"><span id="video-odkaz" style="color: white; position: relative;  top: -30px; left: 5px; z-index: 10; "><xsl:value-of select="$loc-vizsyn"/></span></xsl:when>
          <xsl:when test="location='Varianty-odkaz.mp4'"><span id="video-odkaz" style="color: white; position: relative;  top: -30px; left: 5px; z-index: 10; "><xsl:value-of select="$loc-vizvar"/></span></xsl:when>
        </xsl:choose>
        <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_lemmaczj') or contains($perm, 'revizor_video')"><div class="publish"><xsl:value-of select="location"/></div></xsl:if>

        <xsl:if test="id_meta_source!='' or id_meta_author!=''">
          <span style="float: left;" class="source">
            <xsl:choose>
              <xsl:when test="id_meta_source!=''"><xsl:value-of select="$loc-zdroj"/>: <xsl:value-of select="id_meta_source"/></xsl:when>
              <xsl:otherwise><xsl:value-of select="$loc-autor"/>: <a href="/{$dictcode}?lang={$lang}&amp;action=page&amp;page=about#skupiny"><xsl:value-of select="id_meta_author"/></a></xsl:otherwise>
            </xsl:choose>
          </span>
        </xsl:if>
        <div class="author-info" style="display:none">
          <xsl:if test="id_meta_source!=''">
            <span class="author-author"><xsl:value-of select="$loc-autor"/>:
              <a href="/{$dictcode}?lang={$lang}&amp;action=page&amp;page=about#skupiny"><xsl:value-of select="id_meta_author"/></a>;
            </span>
          </xsl:if>
          <xsl:if test="id_meta_copyright!=''">
            <span class="author-copyright"><xsl:value-of select="$loc-autorvideo"/>: <br /><xsl:value-of select="id_meta_copyright"/></span>
          </xsl:if>
        </div>
      </div>
      <xsl:if test="substring(location, string-length(location)-3, 4) = '.mp4'">
        <script type="text/javascript">
          $("#flowvideo<xsl:value-of select="@id"/>-<xsl:value-of select="$parent_id"/>").on("click", function(e) {
          var container = $('<div data-ratio="0.8" style="width:335px;">
            <video preload="none" controls="" width="285px" height="228px" poster="/media/video{$dictcode}/thumb/{location}/thumb.jpg" autoplay="">
            <xsl:if test="$video='webm'">  
              <source type="video/webm" src="/media/video{$dictcode}/{location}.webm"/>
            </xsl:if>  
              <source type="video/mp4" src="/media/video{$dictcode}/{location}"/>
            </video>
          </div>');
          $.fancybox.open({
          src: container,
          type: 'html',
          scrolling: 'no',
          });
          });
        </script>
      </xsl:if>
      <xsl:if test="substring(location, string-length(location)-3, 4) = '.flv'">
        <script type="text/javascript">
          $("#flowvideo<xsl:value-of select="@id"/>-<xsl:value-of select="$parent_id"/>").on("click", function(e) {
          flowplayer(this).pause();
          var container = $('<div/>');
          console.log(container);
          container.css('background', '#777 url(/media/video<xsl:value-of select="$dictcode"/>/thumb/<xsl:value-of select="location"/>/thumb.jpg) no-repeat');
          container.css('background-size', '285px 228px');
          container.flowplayer({
          autoPlay: true,
          autoBuffering: true,
          width: 285,
          height: 228,
          ratio: 0.8,
          playlist: [
          [{'flash': '/media/video<xsl:value-of select="$dictcode"/>/<xsl:value-of select="location"/>'}]
          ],
          });
          $.fancybox({
          content: container,
          width: 285,
          height: 228,
          scrolling: 'no',
          autoSize: false
          });
          flowplayer(container[0]).load([{'flash': '/media/video<xsl:value-of select="$dictcode"/>/<xsl:value-of select="location"/>'}]);
          flowplayer(container[0]).resume();
          });
        </script>
      </xsl:if>
    </xsl:template>

    <xsl:template match="meaning">
      <img src="/editor/img/grey-corner.png" style="position: relative; top: 0px; left: -45px;" valign="top"/>
      <div class="meaningtop">
        <div class="czj-lemma-h1"><xsl:value-of select="$loc-vyznam"/><xsl:text> </xsl:text>
          <xsl:if test="count(//meanings/meaning[status='published'])>1">
            <xsl:value-of select="position()"/>
          </xsl:if>
          <xsl:if test="count(//meanings/meaning[status='published'])&lt;=1 and ($perm!='ro' or $skupina_test='true')">
            <xsl:value-of select="@number"/>
          </xsl:if>
          <xsl:if test="($perm!='ro' or $skupina_test='true') and @number!=substring-after(@id,'-')"> <span class="source">[! id:<xsl:value-of select="@id"/>]</span></xsl:if>
          <xsl:if test="@source!='' or @copyright!=''"><span style="float: right;" class="source"><xsl:value-of select="$loc-zdroj"/>: <xsl:value-of select="@source"/>
              <xsl:if test="@source='' and @copyright!=''"><xsl:value-of select="@copyright"/></xsl:if>
            </span><br />
          </xsl:if>
          <div class="source" style="display:none; float: right;">
            <xsl:if test="@author!=''"><span class="author-author"><xsl:value-of select="$loc-autor"/>: <a href="/{$dictcode}?lang={$lang}&amp;action=page&amp;page=about#skupiny"><xsl:value-of select="@author"/></a>; </span></xsl:if>
            <xsl:if test="@copyright!=''"><span class="author-copyright"><xsl:value-of select="$loc-autorvideo"/>: <xsl:value-of select="@copyright"/></span></xsl:if>
          </div>
        </div>
        <div id="meaning{@id}" class="meaning">
          <xsl:if test="category!=''">
            <span class="category"><b><xsl:value-of select="$loc-semoblast"/>: </b>
              <xsl:for-each select="category">
                <xsl:choose>
                  <xsl:when test="text()='6'"><xsl:value-of select="$loc-cat6"/></xsl:when>
                  <xsl:when test="text()='14'"><xsl:value-of select="$loc-cat14"/></xsl:when>
                  <xsl:when test="text()='28'"><xsl:value-of select="$loc-cat28"/></xsl:when>
                  <xsl:when test="text()='27'"><xsl:value-of select="$loc-cat27"/></xsl:when>
                  <xsl:when test="text()='anat'"><xsl:value-of select="$loc-catanat"/></xsl:when>
                  <xsl:when test="text()='antr'"><xsl:value-of select="$loc-catantr"/></xsl:when>
                  <xsl:when test="text()='archeol'"><xsl:value-of select="$loc-catarcheol"/></xsl:when>
                  <xsl:when test="text()='archit'"><xsl:value-of select="$loc-catarchit"/></xsl:when>
                  <xsl:when test="text()='biol'"><xsl:value-of select="$loc-catbiol"/></xsl:when>
                  <xsl:when test="text()='bot'"><xsl:value-of select="$loc-catbot"/></xsl:when>
                  <xsl:when test="text()='dipl'"><xsl:value-of select="$loc-catdipl"/></xsl:when>
                  <xsl:when test="text()='div'"><xsl:value-of select="$loc-catdiv"/></xsl:when>
                  <xsl:when test="text()='dopr'"><xsl:value-of select="$loc-catdopr"/></xsl:when>
                  <xsl:when test="text()='ekol'"><xsl:value-of select="$loc-catekol"/></xsl:when>
                  <xsl:when test="text()='ekon'"><xsl:value-of select="$loc-catekon"/></xsl:when>
                  <xsl:when test="text()='eltech'"><xsl:value-of select="$loc-cateltech"/></xsl:when>
                  <xsl:when test="text()='etn'"><xsl:value-of select="$loc-catetn"/></xsl:when>
                  <xsl:when test="text()='feud'"><xsl:value-of select="$loc-catfeud"/></xsl:when>
                  <xsl:when test="text()='filat'"><xsl:value-of select="$loc-catfilat"/></xsl:when>
                  <xsl:when test="text()='film'"><xsl:value-of select="$loc-catfilm"/></xsl:when>
                  <xsl:when test="text()='filoz'"><xsl:value-of select="$loc-catfiloz"/></xsl:when>
                  <xsl:when test="text()='fot'"><xsl:value-of select="$loc-catfot"/></xsl:when>
                  <xsl:when test="text()='fyz'"><xsl:value-of select="$loc-catfyz"/></xsl:when>
                  <xsl:when test="text()='fyziol'"><xsl:value-of select="$loc-catfyziol"/></xsl:when>
                  <xsl:when test="text()='geol'"><xsl:value-of select="$loc-catgeol"/></xsl:when>
                  <xsl:when test="text()='geom'"><xsl:value-of select="$loc-catgeom"/></xsl:when>
                  <xsl:when test="text()='gnoz'"><xsl:value-of select="$loc-catgnoz"/></xsl:when>
                  <xsl:when test="text()='hist'"><xsl:value-of select="$loc-cathist"/></xsl:when>
                  <xsl:when test="text()='horn'"><xsl:value-of select="$loc-cathorn"/></xsl:when>
                  <xsl:when test="text()='horol'"><xsl:value-of select="$loc-cathorol"/></xsl:when>
                  <xsl:when test="text()='hosp'"><xsl:value-of select="$loc-cathosp"/></xsl:when>
                  <xsl:when test="text()='hud'"><xsl:value-of select="$loc-cathud"/></xsl:when>
                  <xsl:when test="text()='hut'"><xsl:value-of select="$loc-cathut"/></xsl:when>
                  <xsl:when test="text()='hvězd'"><xsl:value-of select="$loc-cathvězd"/></xsl:when>
                  <xsl:when test="text()='chem'"><xsl:value-of select="$loc-catchem"/></xsl:when>
                  <xsl:when test="text()='ideal'"><xsl:value-of select="$loc-catideal"/></xsl:when>
                  <xsl:when test="text()='jad'"><xsl:value-of select="$loc-catjad"/></xsl:when>
                  <xsl:when test="text()='jaz'"><xsl:value-of select="$loc-catjaz"/></xsl:when>
                  <xsl:when test="text()='kapit'"><xsl:value-of select="$loc-catkapit"/></xsl:when>
                  <xsl:when test="text()='karet'"><xsl:value-of select="$loc-catkaret"/></xsl:when>
                  <xsl:when test="text()='katol církvi'"><xsl:value-of select="$loc-catkatolcírkvi"/></xsl:when>
                  <xsl:when test="text()='krim'"><xsl:value-of select="$loc-catkrim"/></xsl:when>
                  <xsl:when test="text()='kuch'"><xsl:value-of select="$loc-catkuch"/></xsl:when>
                  <xsl:when test="text()='kult'"><xsl:value-of select="$loc-catkult"/></xsl:when>
                  <xsl:when test="text()='kyb'"><xsl:value-of select="$loc-catkyb"/></xsl:when>
                  <xsl:when test="text()='lék'"><xsl:value-of select="$loc-catlék"/></xsl:when>
                  <xsl:when test="text()='lékár'"><xsl:value-of select="$loc-catlékár"/></xsl:when>
                  <xsl:when test="text()='let'"><xsl:value-of select="$loc-catlet"/></xsl:when>
                  <xsl:when test="text()='liter'"><xsl:value-of select="$loc-catliter"/></xsl:when>
                  <xsl:when test="text()='log'"><xsl:value-of select="$loc-catlog"/></xsl:when>
                  <xsl:when test="text()='marx'"><xsl:value-of select="$loc-catmarx"/></xsl:when>
                  <xsl:when test="text()='mat'"><xsl:value-of select="$loc-catmat"/></xsl:when>
                  <xsl:when test="text()='meteor'"><xsl:value-of select="$loc-catmeteor"/></xsl:when>
                  <xsl:when test="text()='miner'"><xsl:value-of select="$loc-catminer"/></xsl:when>
                  <xsl:when test="text()='motor'"><xsl:value-of select="$loc-catmotor"/></xsl:when>
                  <xsl:when test="text()='mysl'"><xsl:value-of select="$loc-catmysl"/></xsl:when>
                  <xsl:when test="text()='mytol'"><xsl:value-of select="$loc-catmytol"/></xsl:when>
                  <xsl:when test="text()='náb'"><xsl:value-of select="$loc-catnáb"/></xsl:when>
                  <xsl:when test="text()='nár'"><xsl:value-of select="$loc-catnár"/></xsl:when>
                  <xsl:when test="text()='obch'"><xsl:value-of select="$loc-catobch"/></xsl:when>
                  <xsl:when test="text()='pedag'"><xsl:value-of select="$loc-catpedag"/></xsl:when>
                  <xsl:when test="text()='peněž'"><xsl:value-of select="$loc-catpeněž"/></xsl:when>
                  <xsl:when test="text()='polit'"><xsl:value-of select="$loc-catpolit"/></xsl:when>
                  <xsl:when test="text()='polygr'"><xsl:value-of select="$loc-catpolygr"/></xsl:when>
                  <xsl:when test="text()='pošt'"><xsl:value-of select="$loc-catpošt"/></xsl:when>
                  <xsl:when test="text()='potrav'"><xsl:value-of select="$loc-catpotrav"/></xsl:when>
                  <xsl:when test="text()='práv'"><xsl:value-of select="$loc-catpráv"/></xsl:when>
                  <xsl:when test="text()='prům'"><xsl:value-of select="$loc-catprům"/></xsl:when>
                  <xsl:when test="text()='přír'"><xsl:value-of select="$loc-catpřír"/></xsl:when>
                  <xsl:when test="text()='psych'"><xsl:value-of select="$loc-catpsych"/></xsl:when>
                  <xsl:when test="text()='rybn'"><xsl:value-of select="$loc-catrybn"/></xsl:when>
                  <xsl:when test="text()='řem'"><xsl:value-of select="$loc-catřem"/></xsl:when>
                  <xsl:when test="text()='sklář'"><xsl:value-of select="$loc-catsklář"/></xsl:when>
                  <xsl:when test="text()='soc'"><xsl:value-of select="$loc-catsoc"/></xsl:when>
                  <xsl:when test="text()='sociol'"><xsl:value-of select="$loc-catsociol"/></xsl:when>
                  <xsl:when test="text()='stat'"><xsl:value-of select="$loc-catstat"/></xsl:when>
                  <xsl:when test="text()='stav'"><xsl:value-of select="$loc-catstav"/></xsl:when>
                  <xsl:when test="text()='škol'"><xsl:value-of select="$loc-catškol"/></xsl:when>
                  <xsl:when test="text()='tech'"><xsl:value-of select="$loc-cattech"/></xsl:when>
                  <xsl:when test="text()='těl'"><xsl:value-of select="$loc-cattěl"/></xsl:when>
                  <xsl:when test="text()='text'"><xsl:value-of select="$loc-cattext"/></xsl:when>
                  <xsl:when test="text()='úč'"><xsl:value-of select="$loc-catúč"/></xsl:when>
                  <xsl:when test="text()='úř'"><xsl:value-of select="$loc-catúř"/></xsl:when>
                  <xsl:when test="text()='veř spr'"><xsl:value-of select="$loc-catveřspr"/></xsl:when>
                  <xsl:when test="text()='vet'"><xsl:value-of select="$loc-catvet"/></xsl:when>
                  <xsl:when test="text()='voj'"><xsl:value-of select="$loc-catvoj"/></xsl:when>
                  <xsl:when test="text()='výp tech'"><xsl:value-of select="$loc-catvýptech"/></xsl:when>
                  <xsl:when test="text()='výr'"><xsl:value-of select="$loc-catvýr"/></xsl:when>
                  <xsl:when test="text()='výtv'"><xsl:value-of select="$loc-catvýtv"/></xsl:when>
                  <xsl:when test="text()='zahr'"><xsl:value-of select="$loc-catzahr"/></xsl:when>
                  <xsl:when test="text()='zbož'"><xsl:value-of select="$loc-catzbož"/></xsl:when>
                  <xsl:when test="text()='zeměd'"><xsl:value-of select="$loc-catzeměd"/></xsl:when>
                  <xsl:when test="text()='zeměp'"><xsl:value-of select="$loc-catzeměp"/></xsl:when>
                  <xsl:when test="text()='zool'"><xsl:value-of select="$loc-catzool"/></xsl:when>
                  <xsl:when test="text()='cirkev'"><xsl:value-of select="$loc-catcirkev"/></xsl:when>
                </xsl:choose>
                <xsl:if test="position()!=last()">, </xsl:if>
              </xsl:for-each>
            </span><br />
          </xsl:if>
          <xsl:if test="@style_region != '' or @style_kategorie != '' or @style_generace != ''">
            <br/>
          </xsl:if>
          <xsl:if test="@style_region != ''">
            <xsl:call-template name="split_region">
              <xsl:with-param name="text" select="@style_region"/>
            </xsl:call-template>
          </xsl:if>
          <xsl:if test="@style_kategorie != ''">       ,
            <xsl:value-of select="@style_kategorie"/>
          </xsl:if>
          <xsl:if test="@style_generace != ''">       ,
            <xsl:call-template name="split_generace">
              <xsl:with-param name="text" select="@style_generace"/>
            </xsl:call-template>
          </xsl:if>

          <!-- <xsl:if test="status!='hidden' or $perm!='ro' or $skupina_test='true'"> skrytí nechválených definic a příkladů pro veřejné zobrazení -->
          
          <div class="vyklad">
            <span class="videoblock">
              <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_lingvist') or contains($perm, 'revizor_video')">
                <span class="set-status">
                  <xsl:call-template name="status_publish">
                    <xsl:with-param name="status" select="status"/>
                    <xsl:with-param name="type">meaning</xsl:with-param>
                    <xsl:with-param name="element" select="."/>
                  </xsl:call-template>
                </span>
              </xsl:if>
              <xsl:if test="not(text//file)">
                <br/>
                <img src="/media/img/emptyvideo.jpg" width="285px"/>
              </xsl:if>
              <xsl:for-each select="text//file[@media_id!='']">
                <xsl:variable name="mid" select="@media_id"/>
                <xsl:apply-templates select="//media/file[id=$mid or @id=$mid][1]" mode="inline">
                  <xsl:with-param name="media_id">
                    <xsl:value-of select="$mid"/>
                  </xsl:with-param>
                </xsl:apply-templates>
              </xsl:for-each>
              <xsl:if test="text!=''">
                <xsl:value-of select="text"/>
              </xsl:if>
            </span>
          </div>
          <div style="clear:both"/>
          <xsl:if test="usages/usage[@id]!=''">
            <div class="usages">
              <div class="czj-lemma-h2"><xsl:value-of select="$loc-priklady"/></div>
              <xsl:apply-templates select="usages/usage[(status!='hidden' or ($perm!='ro' or $skupina_test='true'))]"/>
            </div>
          </xsl:if>          

          <xsl:if test="relation[@type!='translation' and (($perm!='ro' or $skupina_test='true') or (@status!='hidden' and  (@auto_complete='1' or @completeness='2' or @completeness='100' or @front_video='published')))] ">
            <div class="synanto">
              <div class="cleaner"></div>
              <div class="czj-lemma-h2"><xsl:value-of select="$loc-vztahy"/>
              </div>
              <div class="czj-relation-wrapper">
                <xsl:apply-templates select="relation[(@type='hyperonym' or @type='hyponym' or @type='synonym' or @type='antonym' or @type='synonym_strategie') and (($perm!='ro' or $skupina_test='true') or (@status!='hidden' and  (@auto_complete='1' or @completeness='2' or @completeness='100' or @front_video='published')))]">
                  <xsl:sort data-type="number" order="ascending" select="((@type='synonym')*1) + ((@type='synonym_strategie')*2) +((@type='antonym')*3)"/>
                  <xsl:sort data-type="number" order="descending" select="((@lemma_region='cr')*3) + ((@lemma_region='cechy')*2) +((@lemma_region='morava')*1)"/>
                </xsl:apply-templates>
              </div>
            </div>
          </xsl:if>
          <div style="clear:both"/>
          <xsl:if test="relation[@type='translation'] and not(is_translation_unknown='1')"> 
          <!-- <xsl:if test="relation[@type='translation' and ((@status!='hidden') or ($perm!='ro' or $skupina_test='true'))] and not(is_translation_unknown='1')"> -->
            <div class="relations">
              <div class="cleaner">
              </div>      <!-- upravit výpis překladů -->
              <div class="czj-lemma-h2"><xsl:value-of select="$loc-preklad"/>
              </div>
              <div class="czj-relation-wrapper">
                <p><xsl:apply-templates select="relation[@type='translation' and (@target='cs' or @target='en' or @target='sj')]">
                <!--<p><xsl:apply-templates select="relation[@type='translation' and ((@status!='hidden') or ($perm!='ro' or $skupina_test='true')) and (@target='cs' or @target='en' or @target='sj')]"> zobrazení jen schválených překladů -->
                    <xsl:sort data-type="number" order="ascending" select="((@target='cs')*1) + ((@target='en')*2) + ((@target='sj')*3)"/>
                </xsl:apply-templates></p>
                <xsl:apply-templates select="relation[@type='translation' and not(@target='cs' or @target='en' or @target='sj')]">
                <!--<xsl:apply-templates select="relation[@type='translation' and ((@status!='hidden') or ($perm!='ro' or $skupina_test='true')) and not(@target='cs' or @target='en' or @target='sj')]">-->
                  <xsl:sort data-type="number" order="ascending" select="((@target='czj')*1) + ((@target='is')*2) + ((@target='asl')*3)"/>
                  <xsl:sort data-type="number" order="descending" select="((@lemma_region='cr')*3) + ((@lemma_region='cechy')*2) +((@lemma_region='morava')*1)"/>
                </xsl:apply-templates>
              </div>
            </div>
          </xsl:if> 
          <br />
          <div style="clear:both"></div>
        </div>
      </div>
    </xsl:template>

    <xsl:template match="relation[@type='translation' and not(@target='cs' or @target='en' or @target='sj')]">       
      <!--<xsl:if test="((@status!='hidden' or name(..)='usage' and ../status='published') and @auto_complete='1') or $perm!='ro'">-->
        <xsl:variable name="lemma"><xsl:value-of select="@lemma_id"/></xsl:variable>
        <xsl:variable name="rtype"><xsl:value-of select="@type"/></xsl:variable>
        <xsl:if test="count(preceding-sibling::relation[@lemma_id=$lemma and @type=$rtype])=0">

          <div class="czj-relation-item dict-{@target}">
            <div class="czj-relation relationvideo">
              <div class="czj-lemma-preview-sm">
                <div class="czj-lemma-preview-sm-header dict-{@target}">

                  <div class="alignleft" style="white-space: nowrap; overflow: hidden; width: 160px;">
                    <xsl:choose>
                      <xsl:when test="@target='czj'">ČZJ </xsl:when>
                      <xsl:when test="@target='is'">IS </xsl:when>
                      <xsl:when test="@target='asl'">ASL </xsl:when>
                      <xsl:when test="@target='spj'">SPJ </xsl:when>
                      <xsl:otherwise>?:</xsl:otherwise>
                    </xsl:choose>

                    <xsl:choose>
                    <xsl:when test="contains($perm, 'admin') or contains($perm, 'revizor_cjlingvist') or contains($perm, 'revizor_preklad')">
                      <xsl:if test="@meaning_nr='' or @meaning_count = 1">
                        <span class="set-status">
                          <xsl:call-template name="status_publish">
                            <xsl:with-param name="status" select="@status"/>
                            <xsl:with-param name="type">relation</xsl:with-param>
                            <xsl:with-param name="element" select="."/>
                          </xsl:call-template>
                        </span>
                      </xsl:if>
                      <!--<br/>-->
                      </xsl:when>
                      <xsl:otherwise><xsl:if test="@status='hidden'"><img src="/media/img/neovereno.png" title="{$loc-neovereno}"/></xsl:if>
              </xsl:otherwise>
              </xsl:choose>
                      
                      <xsl:if test="@meaning_nr!='' and @meaning_count > 1"><xsl:value-of select="$loc-vevyznamu"/> <xsl:value-of select="@meaning_nr"/> 
                      <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_cjlingvist') or contains($perm, 'revizor_preklad')">
                        <span class="set-status">
                          <xsl:call-template name="status_publish">
                            <xsl:with-param name="status" select="@status"/>
                            <xsl:with-param name="type">relation</xsl:with-param>
                            <xsl:with-param name="element" select="."/>
                          </xsl:call-template>
                        </span>
                      </xsl:if>
                      <xsl:if test="count(following-sibling::relation[@lemma_id=$lemma and @type=$rtype]) &gt; 0">
                        <xsl:for-each select="following-sibling::relation[@lemma_id=$lemma and @type=$rtype]">
                          <xsl:sort select="@meaning_nr"/>
                          <xsl:text>, </xsl:text><xsl:value-of select="@meaning_nr"/>
                          <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_cjlingvist') or contains($perm, 'revizor_preklad')">
                            <span class="set-status">
                              <xsl:call-template name="status_publish">
                                <xsl:with-param name="status" select="@status"/>
                                <xsl:with-param name="type">relation</xsl:with-param>
                                <xsl:with-param name="element" select="."/>
                              </xsl:call-template>
                            </span>
                          </xsl:if>
                        </xsl:for-each>
                      </xsl:if>
                  </xsl:if></div>
                  <div class="alignright">
                  <xsl:if test="((@status!='hidden' or name(..)='usage' and ../status='published') and @auto_complete='1') or $perm!='ro'">
                    <a href="/{@target}?lang={$lang}&amp;action=search&amp;getdoc={@lemma_id}&amp;empty={$empty}">
                      <!--<xsl:value-of select="@lemma_id"/>-->
                      <img src="/media/img/slovnik-open_w.png" height="20"/>                      
                    </a></xsl:if>
                    <xsl:if test="@type='translation_colloc'">
                        <xsl:value-of select="translation"/>
                      </xsl:if>            
                  </div>
                </div>
                <xsl:apply-templates select="file" mode="rel">
                  <xsl:with-param name="target"><xsl:value-of select="@target"/></xsl:with-param>
                </xsl:apply-templates>
                <div class="czj-lemma-preview-sm-sw">
                  <span class="sw transsw" style="vertical-align: top; display: inline-block; height: 96px;">
                    <xsl:apply-templates select="swmix/sw" mode="rel"/>
                  </span>
                </div>
              </div>
            </div>
          </div>
          <xsl:if test="@type='translation_colloc'">
            <br/>
          </xsl:if>
        </xsl:if>
      <!--</xsl:if>-->
    </xsl:template>

    <xsl:template match="relation[@type='translation' and (@target='cs' or @target='en' or @target='sj')]">
      <xsl:variable name="lemma"><xsl:value-of select="@lemma_id"/></xsl:variable>
      <xsl:variable name="rtype"><xsl:value-of select="@type"/></xsl:variable>

      <span class="relation-entry dict-{@target}">
        <xsl:if test="@title_only!='true'">
          <xsl:if test="count(preceding-sibling::relation[@lemma_id=$lemma and @type='translation' and not(@title_only='true')])=0">
            <xsl:if test="@auto_complete='1' or ($perm!='ro' or $skupina_test='true')">
              <span style="color: gray">
                <xsl:choose>
                  <xsl:when test="@target='cs'">CZ: </xsl:when>
                  <xsl:when test="@target='en'">EN: </xsl:when>
                  <xsl:when test="@target='sj'">SJ: </xsl:when>
                  <xsl:otherwise>?</xsl:otherwise>
                </xsl:choose>
              </span>
              <a href="/{@target}?lang={$lang}&amp;action=search&amp;getdoc={@lemma_id}&amp;empty={$empty}">
                <xsl:value-of select="title"/></a>
            </xsl:if>
            <xsl:if test="@auto_complete!='1' and $perm='ro' and $skupina_test!='true'">
              <span style="color: gray">
                <xsl:choose>
                  <xsl:when test="@target='cs'">CZ: </xsl:when>
                  <xsl:when test="@target='en'">EN: </xsl:when>
                  <xsl:when test="@target='sj'">SJ: </xsl:when>
                  <xsl:otherwise>?</xsl:otherwise>
                </xsl:choose>
              </span>
              <xsl:value-of select="title"/>
            </xsl:if>
            <xsl:if test="(@meaning_nr!='' and @meaning_count > 1) and (@auto_complete='1' or ($perm!='ro' or $skupina_test='true'))"> (<xsl:value-of select="$loc-vevyznamu"/><xsl:text> </xsl:text>
              <xsl:value-of select="@meaning_nr"/>
              <!--schvalování-->
              <xsl:choose>
              <xsl:when test="contains($perm, 'admin') or contains($perm, 'revizor_lingvist') or contains($perm, 'revizor_preklad')">  
                <span class="set-status">
                  <xsl:call-template name="status_publish">
                    <xsl:with-param name="status" select="@status"/>
                    <xsl:with-param name="type">relation</xsl:with-param>
                    <xsl:with-param name="element" select="."/>
                  </xsl:call-template>
                </span>
              </xsl:when><!--konec schvalování-->
              <!--výpis upozornění -->
              <xsl:otherwise> <xsl:if test="@status='hidden'"><img src="/media/img/neovereno.png" title="{$loc-neovereno}"/></xsl:if>
              </xsl:otherwise>
              </xsl:choose>
              <xsl:if test="count(following-sibling::relation[@lemma_id=$lemma and @type='translation']) &gt; 0">
                <xsl:for-each select="following-sibling::relation[@lemma_id=$lemma and @type='translation']">
                  <xsl:sort select="@meaning_nr" data-type="number"/>
                  <xsl:text>, </xsl:text>
                  <xsl:value-of select="@meaning_nr"/>
                  <!--schvalování-->
                  <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_lingvist') or contains($perm, 'revizor_preklad')">
                    <span class="set-status">
                      <xsl:call-template name="status_publish">
                        <xsl:with-param name="status" select="@status"/>
                        <xsl:with-param name="type">relation</xsl:with-param>
                        <xsl:with-param name="element" select="."/>
                      </xsl:call-template>
                    </span>
                  </xsl:if>
                  <!--konec schvalování-->
                </xsl:for-each>
              </xsl:if>)
            </xsl:if>
            <xsl:if test="@meaning_nr='' or @meaning_count = 1">
              <!--schvalování-->
              <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_lingvist') or contains($perm, 'revizor_preklad')">
                <span class="set-status">
                  <xsl:call-template name="status_publish">
                    <xsl:with-param name="status" select="@status"/>
                    <xsl:with-param name="type">relation</xsl:with-param>
                    <xsl:with-param name="element" select="."/>
                  </xsl:call-template>
                </span>
              </xsl:if><!--konec schvalování-->
              <xsl:if test="position()!=last()">, </xsl:if>
            </xsl:if>

            <xsl:if test="(count(following-sibling::relation[@type=$rtype and @lemma_id!=$lemma])-count(following-sibling::relation[@type=$rtype and @lemma_id!=$lemma and @lemma_id=preceding-sibling::relation[@type=$rtype]/@lemma_id]))!=0"><xsl:text>, </xsl:text></xsl:if>
          </xsl:if>
        </xsl:if>
        <xsl:if test="@title_only='true'">
          <span style="color: gray">
            <xsl:choose>
              <xsl:when test="@target='cs'">CZ: </xsl:when>
              <xsl:when test="@target='en'">EN: </xsl:when>
              <xsl:when test="@target='sj'">SJ: </xsl:when>
              <xsl:otherwise>?</xsl:otherwise>
            </xsl:choose>
          </span>
          <xsl:value-of select="title"/>
          <!--schvalování-->
          <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_lingvist') or contains($perm, 'revizor_preklad')">
            <span class="set-status">
              <xsl:call-template name="status_publish">
                <xsl:with-param name="status" select="@status"/>
                <xsl:with-param name="type">relation</xsl:with-param>
                <xsl:with-param name="element" select="."/>
              </xsl:call-template>
            </span>
          </xsl:if><!--konec schvalování-->

          <xsl:if test="position()!=last()">, </xsl:if>
        </xsl:if>
      </span>
    </xsl:template>

    <xsl:template match="relation[@type!='translation']">
      <div class="czj-relation-item">
        <div class="czj-relation relationvideo">
          <div class="czj-lemma-preview-sm">
            <div class="czj-lemma-preview-sm-header">
              <div class="alignleft" style="white-space: nowrap; overflow: hidden; width: 165px;">
                <xsl:choose>
                  <xsl:when test="@type='synonym'"><xsl:value-of select="$loc-synonym"/></xsl:when>
                  <xsl:when test="@type='synonym_strategie'"><xsl:value-of select="$loc-synonym_strategie"/></xsl:when>
                  <xsl:when test="@type='antonym'"><xsl:value-of select="$loc-antonym"/></xsl:when>
                  <xsl:when test="@type='hyponym'"><xsl:value-of select="$loc-hyponym"/></xsl:when>
                  <xsl:when test="@type='hyperonym'"><xsl:value-of select="$loc-hyperonym"/></xsl:when>
                  <xsl:when test="@type='revcolloc'">
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="@type"/>
                  </xsl:otherwise>
                </xsl:choose>
                <xsl:if test="@meaning_nr!='' and @meaning_count > 1"> (<xsl:value-of select="$loc-vevyz"/><xsl:text> </xsl:text>
                  <xsl:value-of select="@meaning_nr"/>)
                </xsl:if>
                <!--schvalování-->
                <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_lingvist')">
                  <span class="set-status">
                    <xsl:call-template name="status_publish">
                      <xsl:with-param name="status" select="@status"/>
                      <xsl:with-param name="type">relation</xsl:with-param>
                      <xsl:with-param name="element" select="."/>
                    </xsl:call-template>
                  </span>
                </xsl:if>
                <!--schvalování konec-->
              </div>
              <xsl:if test="$perm!='ro' or @auto_complete='1'">
                <div class="alignright">
                  <a href="/{$dictcode}?lang={$lang}&amp;action=search&amp;getdoc={@lemma_id}&amp;empty={$empty}" >
                    <img src="/media/img/slovnik-open_w.png" height="20"/>
                    <!--<xsl:value-of select="@lemma_id"/>-->            </a>
                </div>
              </xsl:if>
            </div>

          </div>
          <div style="clear:both">
          </div>
          <xsl:if test="not(file)">
            <img src="/media/img/emptyvideo.jpg" height="96" width="120"/>
          </xsl:if>
          <xsl:apply-templates select="file" mode="rel"/>
          <div class="czj-lemma-preview-sm-sw">
            <span class="sw transsw" style="vertical-align: top; display: inline-block; height: 96px;">
              <xsl:if test="$perm!='ro' or @auto_complete='1'"></xsl:if>
              <!-- <span class="sw transsw" style="vertical-align:top; display:inline-block;"> -->
                <xsl:apply-templates select="swmix/sw" mode="rel"/>
              </span>
            </div>
            <br/>
            <span class="" style="vertical-align:top; display:none;">
              <xsl:value-of select="translation"/>
            </span>
          </div>
          <!-- czj-relation relationvideo -->
        </div>
      </xsl:template>
      <xsl:template match="usage"><!--příklady užití objekt-->
        <xsl:variable name="ppp" select="current()"/>
        <div class="relation-entry" style="float: left">
          <!--schvalování-->
          <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_lingvist') or contains($perm, 'revizor_video')">
            <span class="set-status">
              <xsl:call-template name="status_publish">
                <xsl:with-param name="status" select="status"/>
                <xsl:with-param name="type">usage</xsl:with-param>
                <xsl:with-param name="element" select="."/>
              </xsl:call-template>
            </span>
            <br/>
          </xsl:if><!--schvalování konec-->
          <xsl:if test="not(text//file)">
            <img src="/media/img/emptyvideo.jpg" height="96" width="120"/>
          </xsl:if>
          <xsl:for-each select="text//file">
            <xsl:variable name="mid">
              <xsl:value-of select="@media_id"/>
            </xsl:variable>
            <xsl:apply-templates select="//media/file[id=$mid or @id=$mid]" mode="usage">
              <xsl:with-param name="parent_id" select="generate-id($ppp)"/>
            </xsl:apply-templates>
          </xsl:for-each>
          <xsl:if test="text!=''">
            <xsl:value-of select="text"/>
          </xsl:if>
          <xsl:if test="relation">
            <em style="font-size:80%; cursor: pointer;" onclick="$(this).next().toggle();$(this).find('.usagetrans-toggle').toggle();"> <span class="usagetrans-toggle">(<xsl:value-of select="$loc-prekladyzobr"/><img src="/media/img/slovnik-expand.png" height="10" />)</span><span class="usagetrans-toggle" style="display:none;">(<xsl:value-of select="$loc-prekladyskryt"/><img src="/media/img/slovnik-collapse.png" height="10" />)</span></em>
            <div class="usage-translation" style="display:none; padding-left: 20px; font-style: italic; font-size:90%">
              <div><strong><xsl:value-of select="$loc-preklady"/>: </strong></div>
              <xsl:for-each select="relation[@target='en' or @target='cs' or @target='sj']">
                <div class="dict-@target"><xsl:choose><xsl:when test="@target='en'"><span style="color: grey">EN:</span></xsl:when><xsl:when test="@target='cs'"><span style="color: grey">ČJ:</span></xsl:when></xsl:choose>
                  <xsl:value-of select="title"/></div>
              </xsl:for-each>
              <xsl:apply-templates select="relation[@target!='en' and @target!='cs' and @target!='sj']" mode="usage"/>
              <div style="clear:both"/>
            </div>
          </xsl:if>
        </div>
      </xsl:template>
      <xsl:template match="relation" mode="usage">
        <xsl:param name="variant_pos"/>
        <xsl:apply-templates select="file" mode="usage_translation">
          <xsl:with-param name="variant_sw" select="swmix"/>
          <!--<xsl:with-param name="variant_pos" select="$variant_pos"/> -->
          <xsl:with-param name="variant_pos" select="position()"/>
          <xsl:with-param name="usage_target" select="@target"/>
          <xsl:with-param name="relation_link"><xsl:if test="(@auto_complete='1' or $perm!='ro') and not(contains(@meaning_id,'_us'))">true</xsl:if></xsl:with-param>
        </xsl:apply-templates>
      </xsl:template>
      <xsl:template match="file" mode="usage_translation">
        <xsl:param name="variant_sw"/>
        <xsl:param name="variant_pos"/>
        <xsl:param name="usage_target"/>
        <xsl:param name="relation_link"/>
        <div class="czj-relation-item dict-czj">
          <div class="czj-relation relationvideo">
            <div class="czj-lemma-preview-sm">
              <div class="czj-lemma-preview-sm-header dict-{$usage_target}">
                <div class="alignleft"><xsl:choose><xsl:when test="$usage_target='czj'">ČZJ</xsl:when>
                    <xsl:when test="$usage_target='is'">IS</xsl:when>
                    <xsl:when test="$usage_target='asl'">ASL</xsl:when>
                    <xsl:when test="$usage_target='spj'">SPJ</xsl:when></xsl:choose>
                </div>
                <xsl:if test="$relation_link='true'">
                  <div class="alignright">
                    <a href="/{$usage_target}?lang={$lang}&amp;action=search&amp;getdoc={../@lemma_id}&amp;empty={$empty}">
                      <img src="/media/img/slovnik-open_w.png" height="20"/></a>
                  </div>
                </xsl:if>
              </div>

            </div>
            <div style="clear:both">
            </div>

            <xsl:if test="substring(location, string-length(location)-3, 4) = '.flv'">
              <div id="flowvideousg{@id}-{../@meaning_id}" data-ratio="0.8" class="flowplayer flowmouse usage-video{$relation_link}" style="background:#777 url(/media/video{$usage_target}/thumb/{location}/thumb.jpg) no-repeat;">
                <video preload="none" loop="loop">
                  <source type="video/flash" src="/media/video{$usage_target}/{location}"/>
                </video>
              </div><br />
            </xsl:if>
            <xsl:if test="substring(location, string-length(location)-3, 4) = '.mp4'">
              <div id="flowvideousg{@id}-{../@meaning_id}" data-ratio="0.8" class="flowmouse usage-video{$relation_link}" style="background: url(/media/video{$usage_target}/thumb/{location}/thumb.jpg) no-repeat;">
                <video preload="none" width="120px" height="96px" poster="/media/video{$usage_target}/thumb/{location}/thumb.jpg" onmouseover="this.play()" onmouseout="this.pause()">
                <xsl:if test="$video='webm'">
                  <source type="video/webm" src="/media/video{$usage_target}/{location}.webm"/>
                </xsl:if>  
                  <source type="video/mp4" src="/media/video{$usage_target}/{location}"/>
                </video>
              </div>
            </xsl:if>

            <div class="czj-lemma-preview-sm-sw">
              <xsl:if test="($variant_sw/sw)">
                <xsl:apply-templates select="$variant_sw/sw" mode="rel"/>
              </xsl:if>
            </div>

            <xsl:if test="$relation_link='true'">
              <script type="text/javascript">
                $("#flowvideousg<xsl:value-of select="@id"/>-<xsl:value-of select="../@meaning_id"/>").on("click", function(e) {
                window.location='/<xsl:value-of select="$usage_target"/>?lang=<xsl:value-of select="$lang"/>&amp;action=search&amp;getdoc=<xsl:value-of select="../@lemma_id"/>';
                });
              </script>
            </xsl:if>
            <xsl:if test="not($relation_link='true')">
              <script type="text/javascript">
                $("#flowvideousg<xsl:value-of select="@id"/>-<xsl:value-of select="../@meaning_id"/>").on("click", function(e) {
                var container = $('<div data-ratio="0.8" style="width:285px;">
                  <video preload="none" controls="" width="285px" height="228px" poster="/media/video{$usage_target}/thumb/{location}/thumb.jpg" autoplay="">
                  <xsl:if test="$video='webm'">
                    <source type="video/webm" src="/media/video{$usage_target}/{location}.webm"/>
                  </xsl:if>  
                    <source type="video/mp4" src="/media/video{$usage_target}/{location}"/>
                  </video>
                </div>');
                $.fancybox.open({
                src: container,
                width: 285,
                height: 228,
                scrolling: 'no',
                autoSize: false
                });
                });
              </script>
            </xsl:if>
          </div>
        </div>
      </xsl:template>
      <xsl:template match="colloc">
        <xsl:if test="($perm!='ro' or $skupina_test='true') or @auto_complete='1'">
          <div class="czj-relation-item">
            <span class="czj-relation relationvideo">
              <div class="czj-lemma-preview-sm">
                <div class="czj-lemma-preview-sm-header">
                  <div class="alignleft">
                    <xsl:if test="$perm!='ro' or $skupina_test='true'">
                      <span class="set-status">
                        <xsl:value-of select="@lemma_id"/>
                      </span>
                    </xsl:if>
                  </div>
                  <div class="alignright">
                    <a href="/{$dictcode}?lang={$lang}&amp;action=search&amp;getdoc={@lemma_id}&amp;empty={$empty}">
                      <img src="/media/img/slovnik-open_w.png" height="20"/></a>
                  </div>
                </div>
              </div>
              <xsl:if test="not(file)">
                <img src="/media/img/emptyvideo.jpg" height="96" width="120"/>
              </xsl:if>
              <xsl:apply-templates select="file" mode="rel"/>
              <div class="czj-lemma-preview-sm-sw">
                <span class="sw transsw" style="vertical-align: top; display: inline-block; height: 96px;">
                  <!--<span class="sw transsw" style="vertical-align:top; display:inline-block;">-->
                    <xsl:apply-templates select="swmix/sw" mode="rel"/>
                  </span>
                </div>
                <br/>
                <span class="" style="vertical-align:top; display:none;">
                  <xsl:value-of select="translation"/>
                </span>
              </span>
            </div>
          </xsl:if>
        </xsl:template>
        <xsl:template match="revcolloc">
          <xsl:if test="@auto_complete='1' or $perm!='ro' or $skupina_test='true'">
            <br/>
            <div style="background-color: white;">
              <span class="relation-entry" >
                <xsl:apply-templates select="file" mode="colloc"/>
                <span class="sw transsw" style="width:auto; height: 96px">
                  <!--<span class="sw transsw" style="vertical-align:top; display:inline-block;">-->
                    <xsl:apply-templates select="swmix/sw" mode="rel"/>
                  </span>
                  <span class="" style="vertical-align:top; display:inline-block;">
                    <!-- <xsl:value-of select="translation"/> -->
                  </span>
                  <span style="vertical-align:top; display:inline-block; float: right;">
                    <span onclick="open_iframe('{@lemma_id}','czj')" style="cursor:pointer;color:blue;text-decoration:underline">
                      <!--<xsl:value-of select="@lemma_id"/>-->
                      <img src="/editor/img/spojeni_w.png" style="position: relative; left: -623px;"/>
                      <img src="/media/img/slovnik-expand.png" height="20" style="position: relative; top: 78px; left: -280px"/>
                    </span>
                  </span>
                  <div style="clear:both; margin-bottom: 5px;">
                  </div>
                </span>
                <iframe class="colloc-iframe" id="colloc{@lemma_id}" src="about:blank" style="display:none" onload="iframe_loaded('{@lemma_id}')"></iframe>
              </div>
            </xsl:if>
          </xsl:template>

          <xsl:template name="split_misto">
            <xsl:param name="text"/>
            <xsl:variable name="first" select='substring-before($text,";")'/>
            <xsl:variable name='rest' select='substring-after($text,";")'/>
            <xsl:if test='$first'>
              <xsl:choose>
                <xsl:when test="$first='neutral'"><xsl:value-of select="$loc-mistoneutral"/></xsl:when>
                <xsl:when test="$first='hlava'"><xsl:value-of select="$loc-mistohlava"/></xsl:when>
                <xsl:when test="$first='oblicej'"><xsl:value-of select="$loc-mistooblicej"/></xsl:when>
                <xsl:when test="$first='temeno'"><xsl:value-of select="$loc-mistotemeno"/></xsl:when>
                <xsl:when test="$first='celo'"><xsl:value-of select="$loc-mistocelo"/></xsl:when>
                <xsl:when test="$first='oci'"><xsl:value-of select="$loc-mistooci"/></xsl:when>
                <xsl:when test="$first='nos'"><xsl:value-of select="$loc-mistonos"/></xsl:when>
                <xsl:when test="$first='usi'"><xsl:value-of select="$loc-mistousi"/></xsl:when>
                <xsl:when test="$first='tvare'"><xsl:value-of select="$loc-mistotvare"/></xsl:when>
                <xsl:when test="$first='usta'"><xsl:value-of select="$loc-mistousta"/></xsl:when>
                <xsl:when test="$first='brada'"><xsl:value-of select="$loc-mistobrada"/></xsl:when>
                <xsl:when test="$first='krk'"><xsl:value-of select="$loc-mistokrk"/></xsl:when>
                <xsl:when test="$first='hrud'"><xsl:value-of select="$loc-mistohrud"/></xsl:when>
                <xsl:when test="$first='paze'"><xsl:value-of select="$loc-mistopaze"/></xsl:when>
                <xsl:when test="$first='ruka'"><xsl:value-of select="$loc-mistoruka"/></xsl:when>
                <xsl:when test="$first='pas'"><xsl:value-of select="$loc-mistopas"/></xsl:when>
                <xsl:when test="$first='dolni'"><xsl:value-of select="$loc-mistodolni"/></xsl:when>
              </xsl:choose>
              <xsl:if test="$rest">, </xsl:if>
            </xsl:if>

            <xsl:if test='$rest'>
              <xsl:call-template name='split_misto'>
                <xsl:with-param name='text' select='$rest'/>
              </xsl:call-template>
            </xsl:if>
            <xsl:if test='not($rest)'>
              <xsl:choose>
                <xsl:when test="$text='neutral'"><xsl:value-of select="$loc-mistoneutral"/></xsl:when>
                <xsl:when test="$text='hlava'"><xsl:value-of select="$loc-mistohlava"/></xsl:when>
                <xsl:when test="$text='oblicej'"><xsl:value-of select="$loc-mistooblicej"/></xsl:when>
                <xsl:when test="$text='temeno'"><xsl:value-of select="$loc-mistotemeno"/></xsl:when>
                <xsl:when test="$text='celo'"><xsl:value-of select="$loc-mistocelo"/></xsl:when>
                <xsl:when test="$text='oci'"><xsl:value-of select="$loc-mistooci"/></xsl:when>
                <xsl:when test="$text='nos'"><xsl:value-of select="$loc-mistonos"/></xsl:when>
                <xsl:when test="$text='usi'"><xsl:value-of select="$loc-mistousi"/></xsl:when>
                <xsl:when test="$text='tvare'"><xsl:value-of select="$loc-mistotvare"/></xsl:when>
                <xsl:when test="$text='usta'"><xsl:value-of select="$loc-mistousta"/></xsl:when>
                <xsl:when test="$text='brada'"><xsl:value-of select="$loc-mistobrada"/></xsl:when>
                <xsl:when test="$text='krk'"><xsl:value-of select="$loc-mistokrk"/></xsl:when>
                <xsl:when test="$text='hrud'"><xsl:value-of select="$loc-mistohrud"/></xsl:when>
                <xsl:when test="$text='paze'"><xsl:value-of select="$loc-mistopaze"/></xsl:when>
                <xsl:when test="$text='ruka'"><xsl:value-of select="$loc-mistoruka"/></xsl:when>
                <xsl:when test="$text='pas'"><xsl:value-of select="$loc-mistopas"/></xsl:when>
                <xsl:when test="$text='dolni'"><xsl:value-of select="$loc-mistodolni"/></xsl:when>
              </xsl:choose>
            </xsl:if>
          </xsl:template>
          <xsl:template name="split_region">
            <xsl:param name="text"/>
            <xsl:variable name="first" select='substring-before($text,";")'/>
            <xsl:variable name='rest' select='substring-after($text,";")'/>
            <xsl:if test='$first'>
              <xsl:choose>
                <xsl:when test="$first='cr'"><xsl:value-of select="$loc-regcr"/>
                  <img src="/media/img/region/celaCR.png" class="oblast"/>
                </xsl:when>
                <xsl:when test="$first='praha'"><xsl:value-of select="$loc-regpraha"/>
                  <img src="/media/img/region/pha.png" class="oblast"/>
                </xsl:when>
                <xsl:when test="$first='plzen'"><xsl:value-of select="$loc-regplzen"/>
                  <img src="/media/img/region/plzen.png" class="oblast"/>
                </xsl:when>
                <xsl:when test="$first='brno'"><xsl:value-of select="$loc-regbrno"/>
                  <img src="/media/img/region/brno.png" class="oblast"/>
                </xsl:when>
                <xsl:when test="$first='vm'"><xsl:value-of select="$loc-regvm"/>
                  <img src="/media/img/region/valmez.png" class="oblast"/>
                </xsl:when>
                <xsl:when test="$first='hk'"><xsl:value-of select="$loc-reghk"/>
                  <img src="/media/img/region/hk.png" class="oblast"/>
                </xsl:when>
                <xsl:when test="$first='kr'"><xsl:value-of select="$loc-regkr"/>
                  <img src="/media/img/region/krom.png" class="oblast"/>
                </xsl:when>
                <xsl:when test="$first='cechy'"><xsl:value-of select="$loc-regcechy"/>
                  <img src="/media/img/region/cechy.png" class="oblast"/>
                </xsl:when>
                <xsl:when test="$first='morava'"><xsl:value-of select="$loc-regmorava"/>
                  <img src="/media/img/region/morava.png" class="oblast"/>
                </xsl:when>
                <xsl:when test="$first='jih'"><xsl:value-of select="$loc-regjih"/>
                  <img src="/media/img/region/jihlava.png" class="oblast"/>
                </xsl:when>
                <xsl:when test="$first='zl'"><xsl:value-of select="$loc-regzl"/>
                  <img src="/media/img/region/zlin.png" class="oblast"/>
                </xsl:when>
                <xsl:when test="$first='cb'"><xsl:value-of select="$loc-regcb"/>
                  <img src="/media/img/region/cb.png" class="oblast"/>
                </xsl:when>
                <xsl:when test="$first='ot'"><xsl:value-of select="$loc-regot"/>
                  <img src="/media/img/region/ostrava.png" class="oblast"/>
                </xsl:when>
                <xsl:when test="$first='ol'"><xsl:value-of select="$loc-regol"/>
                  <img src="/media/img/region/ol.png" class="oblast"/>
                </xsl:when>
                <xsl:when test="$first='ul'"><xsl:value-of select="$loc-regul"/>
                  <img src="/media/img/region/usti.png" class="oblast"/>
                </xsl:when>
                <xsl:when test="$first='lib'"><xsl:value-of select="$loc-reglib"/>
                  <img src="/media/img/region/lbc.png" class="oblast"/>
                </xsl:when>
                <xsl:when test="$first='slovensko'"><xsl:value-of select="$loc-regslovensko"/>
                  <img src="/media/img/region/sk.png" class="oblast"/>
                </xsl:when>

              </xsl:choose>
              <xsl:if test="$rest">,
              </xsl:if>
            </xsl:if>
            <xsl:if test='$rest'>
              <xsl:call-template name='split_region'>
                <xsl:with-param name='text' select='$rest'/>
              </xsl:call-template>
            </xsl:if>
            <xsl:if test='not($rest)'>
              <xsl:choose>
                <xsl:when test="$text='cr'"><xsl:value-of select="$loc-regcr"/>
                  <img src="/media/img/region/celaCR.png" class="oblast"/>
                </xsl:when>
                <xsl:when test="$text='praha'"><xsl:value-of select="$loc-regpraha"/>
                  <img src="/media/img/region/pha.png" class="oblast"/>
                </xsl:when>
                <xsl:when test="$text='plzen'"><xsl:value-of select="$loc-regplzen"/>
                  <img src="/media/img/region/plzen.png" class="oblast"/>
                </xsl:when>
                <xsl:when test="$text='brno'"><xsl:value-of select="$loc-regbrno"/>
                  <img src="/media/img/region/brno.png" class="oblast"/>
                </xsl:when>
                <xsl:when test="$text='vm'"><xsl:value-of select="$loc-regvm"/>
                  <img src="/media/img/region/valmez.png" class="oblast"/>
                </xsl:when>
                <xsl:when test="$text='hk'"><xsl:value-of select="$loc-reghk"/>
                  <img src="/media/img/region/hk.png" class="oblast"/>
                </xsl:when>
                <xsl:when test="$text='kr'"><xsl:value-of select="$loc-regkr"/>
                  <img src="/media/img/region/krom.png" class="oblast"/>
                </xsl:when>
                <xsl:when test="$text='cechy'"><xsl:value-of select="$loc-regcechy"/>
                  <img src="/media/img/region/cechy.png" class="oblast"/>
                </xsl:when>
                <xsl:when test="$text='morava'"><xsl:value-of select="$loc-regmorava"/>
                  <img src="/media/img/region/morava.png" class="oblast"/>
                </xsl:when>
                <xsl:when test="$text='jih'"><xsl:value-of select="$loc-regjih"/>
                  <img src="/media/img/region/jihlava.png" class="oblast"/>
                </xsl:when>
                <xsl:when test="$text='zl'"><xsl:value-of select="$loc-regzl"/>
                  <img src="/media/img/region/zlin.png" class="oblast"/>
                </xsl:when>
                <xsl:when test="$text='cb'"><xsl:value-of select="$loc-regcb"/>
                  <img src="/media/img/region/cb.png" class="oblast"/>
                </xsl:when>
                <xsl:when test="$text='ot'"><xsl:value-of select="$loc-regot"/>
                  <img src="/media/img/region/ostrava.png" class="oblast"/>
                </xsl:when>
                <xsl:when test="$text='ol'"><xsl:value-of select="$loc-regol"/>
                  <img src="/media/img/region/ol.png" class="oblast"/>
                </xsl:when>
                <xsl:when test="$text='ul'"><xsl:value-of select="$loc-regul"/>
                  <img src="/media/img/region/usti.png" class="oblast"/>
                </xsl:when>
                <xsl:when test="$text='lib'"><xsl:value-of select="$loc-reglib"/>
                  <img src="/media/img/region/lbc.png" class="oblast"/>
                </xsl:when>
                <xsl:when test="$text='slovensko'"><xsl:value-of select="$loc-regslovensko"/>
                  <img src="/media/img/region/sk.png" class="oblast"/>
                </xsl:when>
              </xsl:choose>
            </xsl:if>
          </xsl:template>
          <xsl:template name="split_generace">
            <xsl:param name="text"/>
            <xsl:variable name="first" select='substring-before($text,";")'/>
            <xsl:variable name='rest' select='substring-after($text,";")'/>
            <xsl:if test='$first'>
              <xsl:choose>
                <xsl:when test="$first='mlada'"><xsl:value-of select="$loc-genmlada"/></xsl:when>
                <xsl:when test="$first='stredni'"><xsl:value-of select="$loc-genstredni"/></xsl:when>
                <xsl:when test="$first='starsi'"><xsl:value-of select="$loc-genstarsi"/></xsl:when>
                <xsl:when test="$first='deti'"><xsl:value-of select="$loc-gendeti"/></xsl:when>
              </xsl:choose>
              <xsl:if test="$rest">, </xsl:if>
            </xsl:if>

            <xsl:if test='$rest'>
              <xsl:call-template name='split_generace'>
                <xsl:with-param name='text' select='$rest'/>
              </xsl:call-template>
            </xsl:if>
            <xsl:if test='not($rest)'>
              <xsl:choose>
                <xsl:when test="$text='mlada'"><xsl:value-of select="$loc-genmlada"/></xsl:when>
                <xsl:when test="$text='stredni'"><xsl:value-of select="$loc-genstredni"/></xsl:when>
                <xsl:when test="$text='starsi'"><xsl:value-of select="$loc-genstarsi"/></xsl:when>
                <xsl:when test="$text='deti'"><xsl:value-of select="$loc-gendeti"/></xsl:when>
              </xsl:choose>
            </xsl:if>
          </xsl:template>
          <xsl:template name="split_skupina2">
            <xsl:param name="text"/>
            <xsl:variable name="first" select='substring-before($text,";")'/>
            <xsl:variable name='rest' select='substring-after($text,";")'/>
            <xsl:if test='$first'>
              <xsl:choose>
                <xsl:when test="$first='intr'"><xsl:value-of select="$loc-skupintr"/></xsl:when>
                <xsl:when test="$first='tran'"><xsl:value-of select="$loc-skuptran"/></xsl:when>
                <xsl:when test="$first='pohyb'"><xsl:value-of select="$loc-skuppohyb"/></xsl:when>
                <xsl:when test="$first='misto'"><xsl:value-of select="$loc-skupmisto"/></xsl:when>
                <xsl:when test="$first='prost'"><xsl:value-of select="$loc-skupprost"/></xsl:when>
                <xsl:when test="$first='subj'"><xsl:value-of select="$loc-skupsubj"/></xsl:when>
                <xsl:when test="$first='obj'"><xsl:value-of select="$loc-skupobj"/></xsl:when>
                <xsl:when test="$first='reci'"><xsl:value-of select="$loc-skupreci"/></xsl:when>
                <xsl:when test="$first='polo'"><xsl:value-of select="$loc-skuppolo"/></xsl:when>
                <xsl:when test="$first='lok'"><xsl:value-of select="$loc-skuplok"/></xsl:when>
                <xsl:when test="$first='pok'"><xsl:value-of select="$loc-skuppok"/></xsl:when>
                <xsl:when test="$first='dej'"><xsl:value-of select="$loc-skupdej"/></xsl:when>
                <xsl:when test="$first='fre'"><xsl:value-of select="$loc-skupfre"/></xsl:when>
                <xsl:when test="$first='mir'"><xsl:value-of select="$loc-skupmir"/></xsl:when>
                <xsl:when test="$first='obr'"><xsl:value-of select="$loc-skupobr"/></xsl:when>
                <xsl:when test="$first='iko'"><xsl:value-of select="$loc-skupiko"/></xsl:when>
                <xsl:when test="$first='spec'"><xsl:value-of select="$loc-skupspec"/></xsl:when>
              </xsl:choose>
              <xsl:if test="$rest">, </xsl:if>
            </xsl:if>

            <xsl:if test='$rest'>
              <xsl:call-template name='split_skupina2'>
                <xsl:with-param name='text' select='$rest'/>
              </xsl:call-template>
            </xsl:if>
            <xsl:if test='not($rest)'>
              <xsl:choose>
                <xsl:when test="$text='intr'"><xsl:value-of select="$loc-skupintr"/></xsl:when>
                <xsl:when test="$text='tran'"><xsl:value-of select="$loc-skuptran"/></xsl:when>
                <xsl:when test="$text='pohyb'"><xsl:value-of select="$loc-skuppohyb"/></xsl:when>
                <xsl:when test="$text='misto'"><xsl:value-of select="$loc-skupmisto"/></xsl:when>
                <xsl:when test="$text='prost'"><xsl:value-of select="$loc-skupprost"/></xsl:when>
                <xsl:when test="$text='subj'"><xsl:value-of select="$loc-skupsubj"/></xsl:when>
                <xsl:when test="$text='obj'"><xsl:value-of select="$loc-skupobj"/></xsl:when>
                <xsl:when test="$text='reci'"><xsl:value-of select="$loc-skupreci"/></xsl:when>
                <xsl:when test="$text='polo'"><xsl:value-of select="$loc-skuppolo"/></xsl:when>
                <xsl:when test="$text='lok'"><xsl:value-of select="$loc-skuplok"/></xsl:when>
                <xsl:when test="$text='pok'"><xsl:value-of select="$loc-skuppok"/></xsl:when>
                <xsl:when test="$text='dej'"><xsl:value-of select="$loc-skupdej"/></xsl:when>
                <xsl:when test="$text='fre'"><xsl:value-of select="$loc-skupfre"/></xsl:when>
                <xsl:when test="$text='mir'"><xsl:value-of select="$loc-skupmir"/></xsl:when>
                <xsl:when test="$text='obr'"><xsl:value-of select="$loc-skupobr"/></xsl:when>
                <xsl:when test="$text='iko'"><xsl:value-of select="$loc-skupiko"/></xsl:when>
                <xsl:when test="$text='spec'"><xsl:value-of select="$loc-skupspec"/></xsl:when>
              </xsl:choose>
            </xsl:if>
          </xsl:template>
          <xsl:template name="split_skupina3">
            <xsl:param name="text"/>
            <xsl:variable name="first" select='substring-before($text,";")'/>
            <xsl:variable name='rest' select='substring-after($text,";")'/>
            <xsl:if test='$first'>
              <xsl:choose>
                <xsl:when test="$first='redup'"><xsl:value-of select="$loc-skup3redup"/></xsl:when>
                <xsl:when test="$first='redupKLF'"><xsl:value-of select="$loc-skup3prof"/></xsl:when>
                <xsl:when test="$first='kvan'"><xsl:value-of select="$loc-skup3kvan"/></xsl:when>
                <xsl:when test="$first='ink'"><xsl:value-of select="$loc-skup3ink"/></xsl:when>
                <xsl:when test="$first='plur'"><xsl:value-of select="$loc-skup3plur"/></xsl:when>
                <xsl:when test="$first='redo'"><xsl:value-of select="$loc-skup3redo"/></xsl:when>
                <xsl:when test="$first='redt'"><xsl:value-of select="$loc-skup3redt"/></xsl:when>
                <xsl:when test="$first='okol'"><xsl:value-of select="$loc-skup3okol"/></xsl:when>
                <xsl:when test="$first='rt'"><xsl:value-of select="$loc-skup3rt"/></xsl:when>
                <xsl:when test="$first='rp'"><xsl:value-of select="$loc-skup3rp"/></xsl:when>
                <xsl:when test="$first='pp'"><xsl:value-of select="$loc-skup3pp"/></xsl:when>
                <xsl:when test="$first='po'"><xsl:value-of select="$loc-skup3po"/></xsl:when>
                <xsl:when test="$first='noplural'"><xsl:value-of select="$loc-skup3netvori"/></xsl:when>
                <xsl:when test="$first='klf'"><xsl:value-of select="$loc-skup3redupklas"/></xsl:when>
              </xsl:choose>
              <xsl:if test="$rest">,
              </xsl:if>
            </xsl:if>
            <xsl:if test='$rest'>
              <xsl:call-template name='split_skupina3'>
                <xsl:with-param name='text' select='$rest'/>
              </xsl:call-template>
            </xsl:if>
            <xsl:if test='not($rest)'>
              <xsl:choose>
                <xsl:when test="$text='redup'"><xsl:value-of select="$loc-skup3redup"/></xsl:when>
                <xsl:when test="$text='redupKLF'"><xsl:value-of select="$loc-skup3prof"/></xsl:when>
                <xsl:when test="$text='kvan'"><xsl:value-of select="$loc-skup3kvan"/></xsl:when>
                <xsl:when test="$text='ink'"><xsl:value-of select="$loc-skup3ink"/></xsl:when>
                <xsl:when test="$text='plur'"><xsl:value-of select="$loc-skup3plur"/></xsl:when>
                <xsl:when test="$text='redo'"><xsl:value-of select="$loc-skup3redo"/></xsl:when>
                <xsl:when test="$text='redt'"><xsl:value-of select="$loc-skup3redt"/></xsl:when>
                <xsl:when test="$text='okol'"><xsl:value-of select="$loc-skup3okol"/></xsl:when>
                <xsl:when test="$text='rt'"><xsl:value-of select="$loc-skup3rt"/></xsl:when>
                <xsl:when test="$text='rp'"><xsl:value-of select="$loc-skup3rp"/></xsl:when>
                <xsl:when test="$text='pp'"><xsl:value-of select="$loc-skup3pp"/></xsl:when>
                <xsl:when test="$text='po'"><xsl:value-of select="$loc-skup3po"/></xsl:when>
                <xsl:when test="$text='noplural'"><xsl:value-of select="$loc-skup3netvori"/></xsl:when>
                <xsl:when test="$text='klf'"><xsl:value-of select="$loc-skup3redupklas"/></xsl:when>
              </xsl:choose>
            </xsl:if>
          </xsl:template>
          <xsl:template name="status_publish">
            <xsl:param name="status"/>
            <xsl:param name="type"/>
            <xsl:param name="element"/>
            <xsl:if test="$status='published'"><img src="/editor/img/checked.png" title="{$loc-schvaleno}" /></xsl:if>
            <xsl:if test="$status!='published'"><!-- skryto -->
              <xsl:text>
              </xsl:text>
              <xsl:choose>
                <xsl:when test="$type='relation'">
                  <input type="button" class="button-small" onclick="publish_relation('{$dictcode}', '{/entry/@id}', '{$element/../@id}', '{$element/@meaning_id}', '{$element/@type}', this)" value="{$loc-schvalit}"/>
                </xsl:when>
                <xsl:when test="$type='usage'">
                  <input type="button" class="button-small" onclick="publish_usage('{$dictcode}', '{/entry/@id}', '{$element/../../@id}', '{$element/@id}', this)" value="{$loc-schvalit}"/>
                </xsl:when>
                <xsl:when test="$type='meaning'">
                  <input type="button" class="button-small" onclick="publish_meaning('{$dictcode}', '{/entry/@id}', '{$element/@id}', this)" value="{$loc-schvalit}"/>
                </xsl:when>
                <xsl:when test="$type='gram' or $type='style' or $type='sw' or $type='hns' or $type='video_front' or $type='video_side' or $type='entry'">
                  <input type="button" class="button-small" onclick="publish('{$dictcode}', '{/entry/@id}', '{$type}', this)" value="{$loc-schvalit}"/>
                </xsl:when>
              </xsl:choose>
            </xsl:if>
          </xsl:template>
        </xsl:stylesheet>
