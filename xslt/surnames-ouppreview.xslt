<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="utf-8" method="html" indent="no" omit-xml-declaration="yes"/>
  <xsl:variable name="lower">
   abcdefghijklmnopqrstuvwxyz
 </xsl:variable>
 <xsl:variable name="upper">
   ABCDEFGHIJKLMNOPQRSTUVWXYZ
 </xsl:variable>

  <xsl:template match="fanuk">
 <html>
  <head>
    <meta content='text/html; charset=utf-8' http-equiv='Content-Type'/>
    <link href='surnames.css' rel='stylesheet' type='text/css'/>
  </head>
  <body class="preview">
    <xsl:apply-templates/>
  </body>
</html>
  </xsl:template>

<xsl:template match="entry">
  <h2 class="head blue"><xsl:value-of select="hw"/></h2>
  <xsl:if test="count(var)>0">
  <br />
    <span class="green">Variants: </span>
      <xsl:for-each select="var">
        <a href="oup?action=search&amp;name={.}"><xsl:value-of select="."/></a><xsl:if test="position()!=last()">, </xsl:if></xsl:for-each>.
      <br/>
  </xsl:if>
  <span class="green">&#8226; Current <xsl:if test="freq_ire!='' and freq_ire!='-' and freq_ire!='0'">frequencies</xsl:if><xsl:if test="freq_ire='' or freq_ire=0 or freq_ire='-'">frequency</xsl:if>: GB: </span><xsl:value-of select="freq_gb"/><xsl:if test="freq_ire!='' and freq_ire!='-' and freq_ire!='0'">,<xsl:text> </xsl:text>
  <span class="green">Ireland: </span><xsl:value-of select="freq_ire"/></xsl:if>.
  <xsl:if test="freq_gb_1881!='' and freq_gb_1881!='-' and freq_gb_1881!='0'">
    <br />
    <span class="green">&#8226; GB frequency, 1881: </span><xsl:value-of select="freq_gb_1881"/> 
  </xsl:if>
  <xsl:if test="gb_distrib!='' and gb_distrib!='-'">
    <br/><span class="green">&#8226; Main GB location 1881:</span><xsl:text> </xsl:text><xsl:value-of select="gb_distrib"/>
  </xsl:if>
  <xsl:if test="ire_distrib!='' and ire_distrib!='-'">
    <br />
    <span class="green">&#8226; Main Irish location 1847-64:</span><xsl:text> </xsl:text><xsl:value-of select="ire_distrib"/>
  </xsl:if>


  <xsl:apply-templates select="sense"/>

  
</xsl:template>



<xsl:template match="eb">
  <xsl:if test="count(../eb)>1">
    <xsl:if test="position()>1"><br /></xsl:if>
    <xsl:number count="arch" format="(i)"/><xsl:text> </xsl:text>
  </xsl:if>
  <xsl:if test="@type!=''"><em><xsl:value-of select="concat(translate(substring(@type,1,1), $lower, $upper), substring(@type, 2),"/>:</em> </xsl:if> 
  <xsl:apply-templates/>
</xsl:template>


<xsl:template match="expl">
  <xsl:param name="start"/>
  <xsl:if test="count(../expl)>1">
    <xsl:if test="position()>1"><br /></xsl:if>
  </xsl:if>
  <xsl:if test="@category!='' and $start='y'"><xsl:text> </xsl:text><xsl:value-of select="@category"/>: </xsl:if>
  <xsl:if test="@category!='' and $start='n'"><xsl:text> </xsl:text><xsl:value-of select="translate(@category, $upper, $lower)"/>: </xsl:if>
  <xsl:apply-templates/>
</xsl:template>

<xsl:template match="references">
  <br/><span class="green">References:</span> <xsl:apply-templates/>
</xsl:template>
<xsl:template match="further_information">
  <br/><span class="green">Further information:</span> <xsl:apply-templates/>
</xsl:template>


<xsl:template match="sense">
  <br  />
    <xsl:if test="count(../sense)>1"><b><xsl:if test="@nr!=''"><xsl:value-of select="@nr"/></xsl:if><xsl:if test="@nr=''"><xsl:value-of select="position()"/></xsl:if></b></xsl:if>
    <xsl:if test="@language_culture!='' and @language_culture!='-'"><xsl:text> </xsl:text><xsl:value-of select="@language_culture"/><xsl:text>: </xsl:text>
    <xsl:apply-templates select="expls/expl"><xsl:with-param name="start">n</xsl:with-param></xsl:apply-templates>
    </xsl:if>
    <xsl:if test="not(@language_culture!='' and @language_culture!='-')"><xsl:text> </xsl:text>
    <xsl:apply-templates select="expls/expl"><xsl:with-param name="start">y</xsl:with-param></xsl:apply-templates>
    </xsl:if>
    <xsl:if test="count(ebs/eb)>0">
    <br  />
      <span class="green">Early Bearers: </span>
      <xsl:apply-templates select="ebs/eb"/>
  </xsl:if>
  <xsl:apply-templates select="references|further_information"/>
</xsl:template>

<xsl:template match="s">
  <b><xsl:value-of select="."/></b>
</xsl:template>

<xsl:template match="b">
  <b><xsl:value-of select="."/></b>
</xsl:template>

<xsl:template match="src">
  <span style="font-family:sans-serif;color:green"><xsl:value-of select="."/></span>
</xsl:template>

<xsl:template match="xr">
  <i><a href="oup?action=search&amp;name={.}"><xsl:value-of select="."/></a></i>
</xsl:template>
<xsl:template match="me">
  <b><a href="oup?action=search&amp;name={.}"><xsl:value-of select="."/></a></b>
</xsl:template>


<xsl:template match="i">
<i><xsl:apply-templates/></i>
</xsl:template>

<xsl:template match="sn">
<span class="sn"><xsl:value-of select="."/></span></xsl:template>

<xsl:template match="br">
<br/>
</xsl:template>


</xsl:stylesheet>

