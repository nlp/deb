<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="utf-8" method="text"/>

<xsl:param name="slovnik"/>

<xsl:template match="SYNSET">
  <xsl:for-each select="SYNONYM/LITERAL">&lt;urn:nlp:wn<xsl:value-of select="$slovnik"/>/<xsl:value-of select="/SYNSET/ID"/>&gt; &lt;urn:nlp:wn<xsl:value-of select="$slovnik"/>/LITERAL&gt; &lt;urn:nlp:wn<xsl:value-of select="$slovnik"/>/<xsl:value-of select="."/>&gt; .
&lt;urn:nlp:wn<xsl:value-of select="$slovnik"/>/<xsl:value-of select="."/>&gt; &lt;http://www.w3.org/1999/02/22-rdf-syntax-ns#type&gt; &lt;urn:nlp:wn<xsl:value-of select="$slovnik"/>/Literal&gt; .
</xsl:for-each>&lt;urn:nlp:wn<xsl:value-of select="$slovnik"/>/<xsl:value-of select="ID"/>&gt; &lt;urn:nlp:wn/POS&gt; &lt;urn:nlp:wn/<xsl:choose>
<xsl:when test="POS='n'">Noun</xsl:when>
<xsl:when test="POS='v'">Verb</xsl:when>
<xsl:when test="POS='a'">Adjective</xsl:when>
<xsl:when test="POS='b'">Adverb</xsl:when>
</xsl:choose>&gt; .
&lt;urn:nlp:wn<xsl:value-of select="$slovnik"/>/<xsl:choose>
<xsl:when test="POS='n'">Noun</xsl:when>
<xsl:when test="POS='v'">Verb</xsl:when>
<xsl:when test="POS='a'">Adjective</xsl:when>
<xsl:when test="POS='b'">Adverb</xsl:when>
</xsl:choose>&gt; &lt;http://www.w3.org/1999/02/22-rdf-syntax-ns#type&gt; &lt;urn:nlp:wn/PartOfSpeech&gt; .
&lt;urn:nlp:wn<xsl:value-of select="$slovnik"/>/<xsl:value-of select="ID"/>&gt; &lt;urn:nlp:wn/DEF&gt; "<xsl:value-of select="DEF"/>" .
<xsl:for-each select="ILR">&lt;urn:nlp:wn<xsl:value-of select="$slovnik"/>/<xsl:value-of select="/SYNSET/ID"/>&gt; &lt;urn:nlp:wn/<xsl:value-of select="@type"/>&gt; &lt;urn:nlp:wn<xsl:value-of select="$slovnik"/>/<xsl:value-of select="."/>&gt; .
&lt;urn:nlp:wn<xsl:value-of select="$slovnik"/>/<xsl:value-of select="."/>&gt; &lt;http://www.w3.org/1999/02/22-rdf-syntax-ns#type&gt; &lt;urn:nlp:wn<xsl:value-of select="$slovnik"/>/Synset&gt; .
</xsl:for-each>
<xsl:for-each select="RILR">
&lt;urn:nlp:wn<xsl:value-of select="$slovnik"/>/<xsl:value-of select="."/>&gt; &lt;urn:nlp:wn/<xsl:value-of select="@type"/>&gt; &lt;urn:nlp:wn<xsl:value-of select="$slovnik"/>/<xsl:value-of select="/SYNSET/ID"/>&gt; .
&lt;urn:nlp:wn<xsl:value-of select="$slovnik"/>/<xsl:value-of select="."/>&gt; &lt;http://www.w3.org/1999/02/22-rdf-syntax-ns#type&gt; &lt;urn:nlp:wn<xsl:value-of select="$slovnik"/>/Synset&gt; .
</xsl:for-each>
</xsl:template>
</xsl:stylesheet>

