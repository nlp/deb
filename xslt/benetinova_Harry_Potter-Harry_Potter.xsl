<?xml version='1.0' encoding='utf-8'?>
<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'><xsl:template match='/entry'><html><head><meta content='text/html[-semicolon-] charset=utf-8' http-equiv='Content-Type'/><title>Harry Potter: <xsl:value-of select="hw"/> (DEBWrite)</title><style type="text/css">.hw{color: blue}
.usage{font-style:italic}
</style>
</head>
<body style="font-family:Courier New"><h1><xsl:value-of select="hw"/></h1>
<xsl:apply-templates/></body></html></xsl:template>
<xsl:template name="file"><xsl:param name="file_element"/><br/><xsl:if test="starts-with($file_element/@mime, 'image')"><img src="/files/benetinova_Harry_Potter/{.}" style="max-width:400px"/></xsl:if><xsl:if test="starts-with($file_element/@mime, 'audio')"><audio src="/files/benetinova_Harry_Potter/{.}" controls="true"/></xsl:if><xsl:if test="starts-with($file_element/@mime, 'video')"><video src="/files/benetinova_Harry_Potter/{.}" controls="true" style="max-width:400px"/></xsl:if><br/></xsl:template>

<xsl:template match="hw"><span class="hw type_text"><xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="transl"><div class="transl type_container"><xsl:apply-templates/></div><br/></xsl:template>
<xsl:template match="transl_lang"><span class="transl_lang type_select"><xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="transl_text"><span class="transl_text type_text"><xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="meaning"><div class="meaning type_container"><xsl:apply-templates/></div><br/></xsl:template>
<xsl:template match="etym"><span class="etym type_text">etymology: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="def"><span class="def type_textarea"><xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="usage"><span class="usage type_textarea"><xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="etym"><span class="etym type_textarea">etymology: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="reference"><span class="reference type_textarea">references: <xsl:apply-templates/></span><br/></xsl:template>
</xsl:stylesheet>
