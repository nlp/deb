<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output encoding="utf-8" method="text" omit-xml-declaration="yes" indent="no"/>

<xsl:param name="slovnik"/>
<xsl:param name="servername"/>
<xsl:param name="default"/>
<xsl:param name="package"/>

<xsl:template match="part">
  <xsl:if test="@id != normalize-space(//SYNSET/ID)">
    <xsl:variable name="id"><xsl:value-of select="@id"/></xsl:variable>
    {"id":"<xsl:value-of select="@id"/>","def":"<xsl:value-of select="@def"/>","synset":[
    <xsl:for-each select="lit">{"name":"<xsl:value-of select="@literal"/>","meaning":"<xsl:value-of select="@sense"/>"}
      <xsl:if test="position()!=last()">,</xsl:if>
    </xsl:for-each>
    ]}
    <xsl:if test="//meta/path/part[@up=$id]/@id != normalize-space(//SYNSET/ID)">,</xsl:if>
    <xsl:apply-templates select="//meta/path/part[@up=$id]"/>
  </xsl:if>
</xsl:template>

<xsl:template match="SYNSET">
{
  "id":"<xsl:value-of select="normalize-space(ID)"/>",
  "pos":"<xsl:value-of select="normalize-space(POS)"/>",
  "def":"<xsl:value-of select="normalize-space(DEF)"/>",
  "synset":[
  <xsl:for-each select="SYNONYM/LITERAL">{"name":"<xsl:value-of select="normalize-space(text())"/>","meaning":"<xsl:value-of select="@sense"/>"}
    <xsl:if test="position()!=last()">,</xsl:if>
  </xsl:for-each>
  ],
  "paths":[
  <xsl:for-each select="//meta/path[part[@up='koren']/@id != //SYNSET/ID]">
    {
    "name":"path-<xsl:value-of select="position()"/>",
    "breadcrumbs":[
        <xsl:apply-templates select="//meta/path/part[@up='koren']"/>
    ]
    }    <xsl:if test="position()!=last()">,</xsl:if>
  </xsl:for-each>
  ],
  "children":[
  {"name":"hyperCat","children":[
    {"name":"hypernym",
    "children":[
    <xsl:for-each select="ILR[@type='hypernym']">
      {"id":"<xsl:value-of select="@link"/>","def":"<xsl:value-of select="@def"/>","synset":[
      <xsl:for-each select="lit">{"name":"<xsl:value-of select="@literal"/>","meaning":"<xsl:value-of select="@sense"/>"}
        <xsl:if test="position()!=last()">,</xsl:if>
      </xsl:for-each>
    ]}<xsl:if test="position()!=last()">,</xsl:if>
    </xsl:for-each>
    ]
    }
    ]},
    {"name":"hyponym",
    "children":[
    <xsl:for-each select="RILR[@type='hypernym']">
      {"id":"<xsl:value-of select="@link"/>","def":"<xsl:value-of select="@def"/>","synset":[
      <xsl:for-each select="lit">{"name":"<xsl:value-of select="@literal"/>","meaning":"<xsl:value-of select="@sense"/>"}
        <xsl:if test="position()!=last()">,</xsl:if>
      </xsl:for-each>
    ]}<xsl:if test="position()!=last()">,</xsl:if>
    </xsl:for-each>
    ]
    },
    {"name":"near_antonym",
    "children":[
    <xsl:for-each select="ILR[@type='near_antonym']">
      {"id":"<xsl:value-of select="@link"/>","def":"<xsl:value-of select="@def"/>","synset":[
      <xsl:for-each select="lit">{"name":"<xsl:value-of select="@literal"/>","meaning":"<xsl:value-of select="@sense"/>"}
        <xsl:if test="position()!=last()">,</xsl:if>
      </xsl:for-each>
    ]}<xsl:if test="position()!=last()">,</xsl:if>
    </xsl:for-each>
    ]
    },
    {"name":"part",
    "children":[
    <xsl:for-each select="RILR[@type='holo_member' or @type='holo_portion' or @type='holo_part' or @type='mero_member' or @type='mero_portion' or @type='mero_part']">
      {"id":"<xsl:value-of select="@link"/>","def":"<xsl:value-of select="@def"/>","synset":[
      <xsl:for-each select="lit">{"name":"<xsl:value-of select="@literal"/>","meaning":"<xsl:value-of select="@sense"/>"}
        <xsl:if test="position()!=last()">,</xsl:if>
      </xsl:for-each>
    ]}<xsl:if test="position()!=last()">,</xsl:if>
    </xsl:for-each>
    ]
    }
  ]
}

</xsl:template>


</xsl:stylesheet>

