<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output encoding="utf-8" method="xml" omit-xml-declaration="yes" indent="yes"/>
  
  <xsl:param name="dictionary"/>
  <xsl:param name="frame"/>
  <xsl:param name="gwgdicts"/>
 
<xsl:template match="part">
  <xsl:if test="@id != //SYNSET/ID">
    <xsl:variable name="id"><xsl:value-of select="@id"/></xsl:variable>
    <xsl:if test="$frame!='true'">
      <a href="/{$dictionary}?action=listPreview&amp;query={@id}"><xsl:value-of select="@text"/></a>
    </xsl:if>
    <xsl:if test="$frame='true'">
      <a href="/?action=frame&amp;query={@id}&amp;dict={$gwgdicts}" target="_top"><xsl:value-of select="@text"/></a>
    </xsl:if>
    <xsl:if test="//meta/path/part[@up=$id]/@id != //SYNSET/ID">&gt;</xsl:if>
    <xsl:apply-templates select="//meta/path/part[@up=$id]"/>
  </xsl:if>
</xsl:template>

  <xsl:template match="SYNSET">
    <div class="showin-select">
      <span style="cursor:pointer;" onmouseover="document.getElementById('select-showin-{/SYNSET/ID}').style.display='block'" onclick="document.getElementById('select-showin-{/SYNSET/ID}').style.display='none'">Show in:</span>
      <div style="display:none" id="select-showin-{/SYNSET/ID}">
        <ul class="showin-ul">
          <xsl:for-each select="//meta/dict">
            <xsl:sort select="@name"/>
            <li><a href="/{@code}?action=listPreview&amp;query={/SYNSET/ID}&amp;frame={$frame}&amp;gwgdicts={$gwgdicts}" target="{@code}"><xsl:value-of select="@name"/></a></li>
          </xsl:for-each>
        </ul>
      </div>
    </div>
    <xsl:if test="//meta/path/part[@up='koren']/@id != //SYNSET/ID">
      <span class="path">
        <xsl:apply-templates select="//meta/path/part[@up='koren']"/>
      </span>
      <br/>
    </xsl:if>
    <span class="BLUE">POS: </span>
    <span class="RED"><xsl:value-of select="POS"/></span>
    <xsl:text> </xsl:text>
    <span class="BLUE">ID: </span>
    <span class="RED"><xsl:value-of select="ID"/></span>
    <xsl:text> </xsl:text>
    <xsl:apply-templates select="BCS"/>
    <br/>
    <span class="BLUE">Synonyms: </span>
    <strong>
    <xsl:for-each select="SYNONYM/LITERAL">
      <xsl:value-of select="text()"/>:<xsl:value-of select="@sense"/>
      <xsl:if test="position()!=last()"><xsl:text>, </xsl:text></xsl:if>
    </xsl:for-each>
    </strong>
    <br/>
    <br/>
    <xsl:apply-templates select="DEF"/>
    <xsl:for-each select="USAGE">
      <span class="BLUE">Usage: </span>
      <span class="DARK_RED"><xsl:value-of select="."/></span><br/>
    </xsl:for-each>
    <xsl:apply-templates select="DOMAIN"/>
    <xsl:for-each select="SUMO">
      <span class="BLUE">SUMO/MILO: </span>
      <span class="GREEN"><xsl:value-of select="TYPE"/><xsl:text> </xsl:text>
        <!--<a href="/sumo?action=listPreview&amp;query={.}" target="_blank"><xsl:value-of select="text()"/></a>-->
        <a href="http://sigma.ontologyportal.org:4010/sigma/Browse.jsp?kb=SUMO&amp;lang=en&amp;term={.}" target="sumo"><xsl:value-of select="text()"/></a>
      </span><br/>
    </xsl:for-each>
    <xsl:for-each select="ILR[text()!='']">
      <span class="BLUE">--&gt;&gt;</span><xsl:text> </xsl:text>
      <xsl:choose>
        <xsl:when test="@type='hypernym'">
          <span class="GREEN">[<xsl:value-of select="@type"/>]</span>
        </xsl:when>
        <xsl:when test="@type='holo_member' or @type='holo_portion' or @type='holo_part'">
          <span class="BLUE">[<xsl:value-of select="@type"/>]</span>
        </xsl:when>
        <xsl:when test="@type='derived'">
          <span class="GREEN2">[<xsl:value-of select="@type"/>]</span>
        </xsl:when>
        <xsl:otherwise>
          <span class="DARK_RED">[<xsl:value-of select="@type"/>]</span>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:text> </xsl:text>
      <xsl:if test="$frame!='true'">
        <a href="/{$dictionary}?action=listPreview&amp;query={@link}"><xsl:value-of select="text()"/></a>
      </xsl:if>
      <xsl:if test="$frame='true'">
        <a href="/?action=frame&amp;query={@link}&amp;dict={$gwgdicts}" target="_top"><xsl:value-of select="text()"/></a>
      </xsl:if>
      <br/>
    </xsl:for-each>
    <xsl:for-each select="RILR[text()!='']">
      <span class="BLUE">&lt;&lt;--</span><xsl:text> </xsl:text>
      <xsl:choose>
        <xsl:when test="@type='hypernym'">
          <span class="GREEN">[hyponym]</span>
        </xsl:when>
        <xsl:when test="@type='holo_member'">
          <span class="BLUE">[mero_member]</span>
        </xsl:when>
        <xsl:when test="@type='holo_portion'">
          <span class="BLUE">[mero_portion]</span>
        </xsl:when>
        <xsl:when test="@type='holo_part'">
          <span class="BLUE">[mero_part]</span>
        </xsl:when>
        <xsl:when test="@type='derived'">
          <span class="GREEN2">[<xsl:value-of select="@type"/>]</span>
        </xsl:when>
        <xsl:when test="@type='category_domain'">
          <span class="DARK_RED">[category_member]</span>
        </xsl:when>
        <xsl:when test="@type='region_domain'">
          <span class="DARK_RED">[region_member]</span>
        </xsl:when>
        <xsl:when test="@type='usage_domain'">
          <span class="DARK_RED">[usage_member]</span>
        </xsl:when>
        <xsl:otherwise>
          <span class="DARK_RED">[<xsl:value-of select="@type"/>]</span>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:text> </xsl:text>
      <xsl:if test="$frame!='true'">
        <a href="/{$dictionary}?action=listPreview&amp;query={@link}"><xsl:value-of select="text()"/></a>
      </xsl:if>
      <xsl:if test="$frame='true'">
        <a href="/?action=frame&amp;query={@link}&amp;dict={$gwgdicts}" target="_top"><xsl:value-of select="text()"/></a>
      </xsl:if>
      <br/>
    </xsl:for-each>
    
    <xsl:for-each select="VALENCY">
      <span class="BLUE">VALENCY: </span>
      <ul>
        <xsl:for-each select="FRAME">
          <li><span class="DARK_RED"><xsl:value-of select="."/></span></li>
        </xsl:for-each>
      </ul>
    </xsl:for-each>
    
    <xsl:if test="STAMP/text()!='' or CREATED/text()!=''">
      <br/>
      <span class="BLUE">STAMP: </span>
      <span class="RED"><xsl:value-of select="STAMP"/></span><xsl:text> / </xsl:text>
      <span class="DARK_RED"><xsl:value-of select="CREATED"/>&#160; </span>
    </xsl:if>
    
    <hr/>
  </xsl:template>
  
  <xsl:template match="BCS">
    <span class="BLUE">BCS: </span>
    <span class="RED"><xsl:value-of select="."/>&#160;</span>
  </xsl:template>
  
  <xsl:template match="DEF">
    <span class="BLUE">Definition: </span>
    <span class="RED"><xsl:value-of select="."/></span>
    <br/>
  </xsl:template>
  <xsl:template match="DOMAIN">
    <span class="BLUE">Domain: </span>
    <span class="GREEN"><xsl:value-of select="."/></span><br/>
  </xsl:template>
  
</xsl:stylesheet>

