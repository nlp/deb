<?xml version='1.0' encoding='utf-8'?>
<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'><xsl:template match='/entry'><html><head><meta content='text/html[-semicolon-] charset=utf-8' http-equiv='Content-Type'/><title>Harry Potter: <xsl:value-of select="term"/> (DEBWrite)</title><style type="text/css">.term{color: blue}
.type_container {border: 1px solid #000}
.type_container {background-color:#eee}
</style>
</head>
<body style="font-family:Courier New"><h1><xsl:value-of select="term"/></h1>
<xsl:apply-templates/></body></html></xsl:template>
<xsl:template name="file"><xsl:param name="file_element"/><br/><xsl:if test="starts-with($file_element/@mime, 'image')"><img src="/files/deb_Harry_Potter/{.}" style="max-width:400px"/></xsl:if><xsl:if test="starts-with($file_element/@mime, 'audio')"><audio src="/files/deb_Harry_Potter/{.}" controls="true"/></xsl:if><xsl:if test="starts-with($file_element/@mime, 'video')"><video src="/files/deb_Harry_Potter/{.}" controls="true" style="max-width:400px"/></xsl:if><br/></xsl:template>

<xsl:template match="term"><span class="term type_text">term: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="term_en"><span class="term_en type_text">AJ: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="term_sl"><span class="term_sl type_text">SL: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="category"><span class="category type_select">kategorie: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="def"><span class="def type_textarea">definice: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="reference"><span class="reference type_textarea">příklad: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="valency"><span class="valency type_text">častá valence: <xsl:apply-templates/></span><br/></xsl:template>
</xsl:stylesheet>
