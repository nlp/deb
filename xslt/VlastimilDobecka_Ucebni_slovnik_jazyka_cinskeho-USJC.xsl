<?xml version='1.0' encoding='utf-8'?>
<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'><xsl:template match='/entry'><html><head><meta content='text/html[-semicolon-] charset=utf-8' http-equiv='Content-Type'/><title>Učební slovník jazyka čínského: <xsl:value-of select="jtz"/> (DEBWrite)</title><style type="text/css">.jtz{color: blue}
.type_container {border: 1px solid #000}
.type_container {background-color:#eee}
</style>
</head>
<body><h1><xsl:value-of select="jtz"/></h1>
<xsl:apply-templates/></body></html></xsl:template>
<xsl:template name="file"><xsl:param name="file_element"/><br/><xsl:if test="starts-with($file_element/@mime, 'image')"><img src="/files/VlastimilDobecka_Ucebni_slovnik_jazyka_cinskeho/{.}" style="max-width:400px"/></xsl:if><xsl:if test="starts-with($file_element/@mime, 'audio')"><audio src="/files/VlastimilDobecka_Ucebni_slovnik_jazyka_cinskeho/{.}" controls="true"/></xsl:if><xsl:if test="starts-with($file_element/@mime, 'video')"><video src="/files/VlastimilDobecka_Ucebni_slovnik_jazyka_cinskeho/{.}" controls="true" style="max-width:400px"/></xsl:if><br/></xsl:template>

<xsl:template match="head"><div class="head type_container">head line: <xsl:apply-templates/></div><br/></xsl:template>
<xsl:template match="no"><span class="no type_number">number: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="abcorder"><span class="abcorder type_number">abc order: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="fr"><span class="fr type_number">frequency: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="pinyin"><span class="pinyin type_text">pinyin: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="pinyina"><span class="pinyina type_text">pinyin alternative: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="vide"><span class="vide type_text">reference: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="hz"><div class="hz type_container">hanzi: <xsl:apply-templates/></div><br/></xsl:template>
<xsl:template match="jtz"><span class="jtz type_text">jiantizi: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="jrad"><span class="jrad type_text">jiantizi radical: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="jns"><span class="jns type_number">jiantizi number of strokes: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="ftz"><span class="ftz type_text">fantizi: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="frad"><span class="frad type_text">fantizi radical: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="fns"><span class="fns type_number">fantizi number of strokes: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="meaning"><div class="meaning type_container">meaning: <xsl:apply-templates/></div><br/></xsl:template>
<xsl:template match="nr"><span class="nr type_text">sense nr: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="note"><span class="note type_text">note: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="sense"><span class="sense type_text">sense: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="fch"><div class="fch type_container">functional characteristics: <xsl:apply-templates/></div><br/></xsl:template>
<xsl:template match="sntno1"><span class="sntno1 type_text">sentence number: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="fch"><span class="fch type_select">functional characteristics: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="sense"><span class="sense type_text">sense: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="usage"><div class="usage type_container">usage example: <xsl:apply-templates/></div><br/></xsl:template>
<xsl:template match="jtz"><span class="jtz type_text">jiantizi: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="py"><span class="py type_text">pinyin: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="te"><span class="te type_text">translation equivalent: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="fch"><span class="fch type_select">functional characteristics: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="stcno"><span class="stcno type_number">sentence number: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="note"><span class="note type_text">note: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="pragm"><span class="pragm type_text">pragmatics: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="domain"><span class="domain type_text">domain: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="note"><span class="note type_text">note: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="xhc"><div class="xhc type_container">xhc: <xsl:apply-templates/></div><br/></xsl:template>
<xsl:template match="xhcno"><span class="xhcno type_text">xhc number: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="note"><span class="note type_text">note: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="te"><span class="te type_text">translation equivalent: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="sentence"><div class="sentence type_container">sentence: <xsl:apply-templates/></div><br/></xsl:template>
<xsl:template match="stno"><span class="stno type_text">sentence number: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="jtz"><span class="jtz type_text">jiantizi: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="prosody"><span class="prosody type_text">prosodic transcription: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="czech"><span class="czech type_text">czech sentence: <xsl:apply-templates/></span><br/></xsl:template>
</xsl:stylesheet>
