<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="utf-8" method="xml" omit-xml-declaration="yes"/>

<xsl:template match="ENTRY">
      <p><xsl:apply-templates/></p>
</xsl:template>

<xsl:template match="HEAD">
      <b class="head"><xsl:value-of select="."/></b>
<xsl:if test="$freq!=''">
  <xsl:text> (frekvence v korpusu: </xsl:text><xsl:value-of select="$freq"/>)
</xsl:if>
<br/>
</xsl:template>

<xsl:template match="LINE">
      <xsl:value-of select="."/><br/>
</xsl:template>

</xsl:stylesheet>

