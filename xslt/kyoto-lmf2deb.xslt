<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="utf-8" method="xml" indent="yes" />

  <xsl:template match="LexicalResource">
    <WN>
      <INFO globallabel="{GlobalInformation/@label}" languageCoding="{Lexicon/@languageCoding}" label="{Lexicon/@label}" language="{Lexicon/@language}" owner="{Lexicon/@owner}" version="{Lexicon/@version}"/>
    <xsl:apply-templates select="Lexicon"/>
    </WN>
  </xsl:template>

  <xsl:template match="Lexicon">
    <xsl:apply-templates select="Synset"/>
  </xsl:template>

  <xsl:template match="Synset">
    <xsl:variable name="id"><xsl:value-of select="@id"/></xsl:variable>
    <SYNSET>
      <ID><xsl:value-of select="@id"/></ID>
      <POS><xsl:value-of select="//LexicalEntry[Sense/@synset=$id]/Lemma/@partOfSpeech"/></POS>
      <SYNONYM>
        <xsl:for-each select="//LexicalEntry/Sense[@synset=$id]">
          <LITERAL id="{@id}"><xsl:value-of select="../Lemma/@writtenForm"/></LITERAL>
          <xsl:call-template name="split">
            <xsl:with-param name="words" select="../Lemma/@writtenForm"/>
          </xsl:call-template>
        </xsl:for-each>
      </SYNONYM>
      <DEF><xsl:value-of select="Definition/@gloss"/></DEF>
      <xsl:for-each select="Definition/Statement/@example|//LexicalEntry/Sense[@synset=$id]/Definition/Statement/@example">
        <USAGE><xsl:value-of select="."/></USAGE>
      </xsl:for-each>
      <xsl:for-each select="SynsetRelations/SynsetRelation">
        <ILR author="{Meta/@author}" date="{Meta/@date}" source="{Meta/@source}" status="{Meta/@status}" confidenceScore="{Meta/@confidenceScore}">
          <xsl:attribute name="type">
            <xsl:choose>
              <xsl:when test="@relType='has_hyperonym'">hypernym</xsl:when>
              <xsl:otherwise><xsl:value-of select="@relType"/></xsl:otherwise>
            </xsl:choose>
          </xsl:attribute>
          <xsl:value-of select="@target"/>
        </ILR>
      </xsl:for-each>
      <xsl:for-each select="MonolingualExternalRefs/MonolingualExternalRef[@externalSystem!='SUMO' and @externalSystem!='Domain']">
        <ELR type="{@relType}" system="{@externalSystem}"><xsl:value-of select="@externalReference"/></ELR>
      </xsl:for-each>
      <xsl:for-each select="MonolingualExternalRefs/MonolingualExternalRef[@externalSystem='SUMO']">
        <SUMO type="{@relType}"><xsl:value-of select="@externalReference"/></SUMO>
      </xsl:for-each>
      <xsl:for-each select="MonolingualExternalRefs/MonolingualExternalRef[@externalSystem='Domain']">
        <DOMAIN><xsl:value-of select="@externalReference"/></DOMAIN>
      </xsl:for-each>
      <xsl:for-each select="//SenseAxes/SenseAxis[Target/@ID=$id]">
        <ELR sa_id="{@id}" type="{@relType}" author="{Meta/@author}" date="{Meta/@date}" source="{Meta/@source}" confidenceScore="{Meta/@confidenceScore}"><xsl:value-of select="Target[not(@ID=$id)]/@ID"/></ELR>
      </xsl:for-each>
      <xsl:if test="@baseConcept">
        <BCS><xsl:value-of select="@baseConcept"/></BCS>
      </xsl:if>
    </SYNSET>
  </xsl:template>

  <xsl:template name="split">
    <xsl:param name="words"/>
    <xsl:variable name="first" select='substring-before($words," ")'/>
    <xsl:variable name='rest' select='substring-after($words," ")'/>
    <xsl:if test='$first'>
      <WORD><xsl:value-of select="$first"/></WORD>
    </xsl:if>

    <xsl:if test='$rest'>
      <xsl:call-template name='split'>
        <xsl:with-param name='words' select='$rest'/>
      </xsl:call-template>
    </xsl:if>
    <xsl:if test='not($rest)'>
      <WORD><xsl:value-of select='$words'/></WORD>
    </xsl:if>
  </xsl:template>

</xsl:stylesheet>
