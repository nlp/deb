<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="utf-8"/>

<xsl:param name="dictionary"/>
<xsl:param name="servername"/>
<xsl:param name="default"/>

<xsl:template match="cdb_lu">
    <body style="background-color: white;" id="body">

      <span class="BLUE">C_LU_ID: </span>
      <span class="RED"><xsl:value-of select="@c_lu_id"/></span>
      <xsl:text> </xsl:text>
      <span class="BLUE">C_SEQ_NR: </span>
      <span class="RED"><xsl:value-of select="@c_seq_nr"/></span>
      <br/>

      <xsl:choose>
        <xsl:when test="form/@form-cat='adj'">
          <xsl:call-template name="adj"/>
        </xsl:when>
        <xsl:when test="form/@form-cat='verb'">
          <xsl:call-template name="verb"/>
        </xsl:when>
        <xsl:when test="form/@form-cat='noun'">
          <xsl:call-template name="noun"/>
        </xsl:when>
      </xsl:choose>

      <xsl:if test="sem-definition">
        <span class="DARK_RED">definition: </span> <span class="BLUE"><xsl:value-of select="sem-definition/sem-defSource"/></span>: <span class="GREEN">
        <xsl:if test="sem-definition/sem-def">
          <xsl:value-of select="sem-definition/sem-def"/>
        </xsl:if>
        <xsl:if test="sem-definition/sem-def-noun">
          <xsl:value-of select="sem-definition/sem-def-noun/sem-specificae"/>
        </xsl:if>
        </span><br/>
      </xsl:if>
      <xsl:if test="sem-synonyms">
        <span class="DARK_RED">synonyms: </span> <span class="GREEN">
          <xsl:for-each select="sem-synonyms/sem-synonym">
            <xsl:value-of select="@r_form"/><xsl:if test="position()!=last()">, </xsl:if>
          </xsl:for-each>
        </span><br/>
      </xsl:if>
      <xsl:if test="sem-hypernyms">
        <span class="DARK_RED">hypernyms: </span> <span class="GREEN">
          <xsl:for-each select="sem-hypernyms/sem-hypernym">
            <xsl:value-of select="@r_form"/><xsl:if test="position()!=last()">, </xsl:if>
          </xsl:for-each>
        </span><br/>
      </xsl:if>

      <br/><span class="BLUE">Pragmatics: </span><br/> 
      <span class="DARK_RED">origin: </span> <span class="GREEN"><xsl:value-of select="pragmatics/prag-origin"/></span><br/>
      <span class="DARK_RED">style: </span> <span class="GREEN"><xsl:value-of select="pragmatics/prag-style"/></span><br/>
      <span class="DARK_RED">connotation: </span> <span class="GREEN"><xsl:value-of select="pragmatics/prag-connotation"/></span><br/>
      <span class="DARK_RED">socGroup: </span> <span class="GREEN"><xsl:value-of select="pragmatics/prag-socGroup"/></span><br/>
      <span class="DARK_RED">domain: </span> <span class="GREEN"><xsl:value-of select="pragmatics/prag-domain/@subjectfield"/> (general: <xsl:value-of select="pragmatics/prag-domain/@general"/>)</span><br/>
      <span class="DARK_RED">chronology: </span> <span class="GREEN"><xsl:value-of select="pragmatics/prag-chronology"/></span><br/>
      <span class="DARK_RED">subj-gen: </span> <span class="GREEN"><xsl:value-of select="pragmatics/prag-subj-gen"/></span><br/>
      <span class="DARK_RED">geography: </span> <span class="GREEN"><xsl:value-of select="pragmatics/prag-geography"/></span><br/>
      <span class="DARK_RED">frequency: </span> <span class="GREEN"><xsl:value-of select="pragmatics/prag-frequency"/></span><br/>
    </body>
</xsl:template>

<xsl:template name="adj">
      <span class="BLUE">Syntax: </span><br/> 
      <span class="DARK_RED">position: </span> <span class="GREEN"><xsl:value-of select="syntax_adj/sy-position"/></span><br/>
      <span class="DARK_RED">advusage: </span> <span class="GREEN"><xsl:value-of select="syntax_adj/sy-advusage"/></span><br/>
      <span class="DARK_RED">complementation: </span> 
      <xsl:for-each select="syntax_adj/sy-complementation/sy-comp">
        <span class="GREEN"><xsl:value-of select="."/></span>
        <xsl:if test="position()!=last()">
          <xsl:text>, </xsl:text>
        </xsl:if>
      </xsl:for-each>

      <br/>
      <br/>
      <span class="BLUE">Semantics: </span><br/>
      <span class="DARK_RED">type: </span> <span class="GREEN"><xsl:value-of select="semantics_adj/sem-type"/></span><br/>
      <span class="DARK_RED">subclass: </span> <span class="GREEN"><xsl:value-of select="semantics_adj/sem-subclass"/></span><br/>
      <span class="DARK_RED">shift: </span> <span class="GREEN"><xsl:value-of select="semantics_adj/sem-shift"/></span><br/>
      <span class="DARK_RED">selrestrictions: </span> 
      <xsl:for-each select="semantics_adj/sem-selrestrictions/sem-selrestriction">
        <br/><span class="GREEN"><xsl:value-of select="selrestriction"/>:<xsl:text> </xsl:text>
        <xsl:for-each select="synset_list/synset">
          <xsl:value-of select="@c_sy_id"/><xsl:text>, </xsl:text>
        </xsl:for-each>
        </span>
      </xsl:for-each>
      <br/>
      <span class="DARK_RED">resume: </span> <span class="GREEN"><xsl:value-of select="semantics_adj/sem-resume"/></span><br/>
</xsl:template>

<xsl:template name="noun">
      <span class="BLUE">Syntax: </span><br/> 
      <span class="DARK_RED">gender: </span> <span class="GREEN"><xsl:value-of select="syntax_noun/sy-gender"/></span><br/>
      <span class="DARK_RED">number: </span> <span class="GREEN"><xsl:value-of select="syntax_noun/sy-number"/></span><br/>
      <span class="DARK_RED">article: </span> <span class="GREEN"><xsl:value-of select="syntax_noun/sy-article"/></span><br/>
      <span class="DARK_RED">complementation: </span> 
      <xsl:for-each select="syntax_noun/sy-complementation/sy-comp">
        <span class="GREEN"><xsl:value-of select="."/></span>
        <xsl:if test="position()!=last()">
          <xsl:text>, </xsl:text>
        </xsl:if>
      </xsl:for-each>
      <br/><br/>
      <span class="BLUE">Semantics: </span><br/>
      <span class="DARK_RED">reference: </span> <span class="GREEN"><xsl:value-of select="semantics_noun/sem-reference"/></span><br/>
      <span class="DARK_RED">countability: </span> <span class="GREEN"><xsl:value-of select="semantics_noun/sem-countability"/></span><br/>
      <span class="DARK_RED">type: </span> 
      <xsl:for-each select="semantics_noun/sem-type">
        <span class="GREEN"><xsl:value-of select="."/></span>
        <xsl:if test="position()!=last()">
          <xsl:text>, </xsl:text>
        </xsl:if>
      </xsl:for-each>
      <br/>
      <span class="DARK_RED">shift: </span> <span class="GREEN"><xsl:value-of select="semantics_noun/sem-shift"/></span><br/>
      <span class="DARK_RED">subclass: </span> <span class="GREEN"><xsl:value-of select="semantics_noun/sem-subclass"/></span><br/>
      <span class="DARK_RED">selrestriction: </span> 
      <xsl:for-each select="semantics_noun/sem-selrestrictions">
        <span class="GREEN"><xsl:value-of select="."/></span>
        <xsl:if test="position()!=last()">
          <xsl:text>, </xsl:text>
        </xsl:if>
      </xsl:for-each>
      <br/>
      <span class="DARK_RED">resume: </span> <span class="GREEN"><xsl:value-of select="semantics_noun/sem-resume"/></span><br/>
</xsl:template>

<xsl:template name="verb">
      <span class="BLUE">Syntax: </span><br/> 
      <span class="DARK_RED">trans: </span> <span class="GREEN"><xsl:value-of select="syntax_verb/sy-trans"/></span><br/>
      <span class="DARK_RED">separ: </span> <span class="GREEN"><xsl:value-of select="syntax_verb/sy-separ"/></span><br/>
      <span class="DARK_RED">class: </span> <span class="GREEN"><xsl:value-of select="syntax_verb/sy-class"/></span><br/>
      <span class="DARK_RED">peraux: </span> <span class="GREEN"><xsl:value-of select="syntax_verb/sy-peraux"/></span><br/>
      <span class="DARK_RED">valency: </span> <span class="GREEN"><xsl:value-of select="syntax_verb/sy-valency"/></span><br/>
      <span class="DARK_RED">reflexiv: </span> <span class="GREEN"><xsl:value-of select="syntax_verb/sy-reflexiv"/></span><br/>
      <span class="DARK_RED">subject: </span> <span class="GREEN"><xsl:value-of select="syntax_verb/sy-subject"/></span><br/>
      <span class="DARK_RED">complementation: </span> 
      <xsl:for-each select="syntax_verb/sy-complementation/sy-comp">
        <span class="GREEN"><xsl:value-of select="."/></span>
        <xsl:if test="position()!=last()">
          <xsl:text>, </xsl:text>
        </xsl:if>
      </xsl:for-each>
      <xsl:if test="count(syntax_verb/sy-complementation/sy-comp)>0 and count(syntax_verb/sy-complementation/sy-compl-text)>0">
        <br/>
      </xsl:if>
      <xsl:for-each select="syntax_verb/sy-complementation/sy-compl-text">
        <span class="GREEN"><xsl:value-of select="."/></span>
        <xsl:if test="position()!=last()">
          <xsl:text>, </xsl:text>
        </xsl:if>
      </xsl:for-each>
      <br/><br/>
      
      <span class="BLUE">Semantics: </span><br/> 
      <span class="DARK_RED">type: </span> <span class="GREEN"><xsl:value-of select="semantics_verb/sem-type"/></span><br/>
      <span class="DARK_RED">caseframe: </span> <span class="GREEN"><xsl:value-of select="semantics_verb/sem-caseframe/caseframe"/></span><br/>
      <xsl:for-each select="semantics_verb/sem-caseframe/args/arg">
        <span class="DARK_RED">caserole: </span> <span class="GREEN"><xsl:value-of select="caserole"/></span><xsl:text> </xsl:text>
        <span class="DARK_RED">selrestrole: </span> <span class="GREEN"><xsl:value-of select="selrestrole"/></span><xsl:text> </xsl:text>
        <xsl:for-each select="synset_list/synset">
          <xsl:value-of select="@c_sy_id"/><xsl:text>, </xsl:text>
        </xsl:for-each>
        <br/>
      </xsl:for-each>
      <span class="DARK_RED">selrestriction: </span>
      <xsl:for-each select="semantics_verb/sem-selrestrictions">
        <span class="GREEN"><xsl:value-of select="."/></span>
        <xsl:if test="position()!=last()">
          <xsl:text>, </xsl:text>
        </xsl:if>
      </xsl:for-each>
      <span class="DARK_RED">resume: </span> <span class="GREEN"><xsl:value-of select="semantics_verb/sem-resume"/></span><br/>
</xsl:template>

</xsl:stylesheet>

