<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output encoding="utf-8" method="xml" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" />
<xsl:param name="perm"/>
<xsl:param name="dictcode"/>
  <xsl:param name="public">false</xsl:param>
 <xsl:variable name="skupina_test">
    <!--ma uzivatel stejnou skupinu jako heslo? nebo bez omezeni-->
    <xsl:if test="contains(/entry/@user_skupina,'all') or contains($perm, 'admin')">true</xsl:if>
    <xsl:if test="$public!='true' and not(contains(/entry/@user_skupina,'all')) and not(contains($perm, 'admin'))"><xsl:value-of select="contains(/entry/@user_skupina, /entry/lemma/pracskupina)"/></xsl:if>
    <xsl:if test="$public='true'">false</xsl:if>
  </xsl:variable>
<xsl:template match="entry">
  <html>
     <head>
      <title>IS: 
          <xsl:value-of select="@id"/>
        </title>     
      <style type="text/css">
    </style>
        <link rel="stylesheet" href="/editor/css/simplebar.css" type="text/css" media="screen"/>
        <script type="text/javascript" src="/editor/css/prettify.js">//</script>
        <script type="text/javascript" src="/editor/css/jquery.js">//</script>
        <script type="text/javascript" src="/editor/css/jquery.scrollbar.js">//</script>
        <script type="text/javascript" src="/editor/css/wrapper.js">//</script>

    <link rel="stylesheet" type="text/css" media="screen" href="/editor/css/czj-common.css" ></link>
    <link rel="stylesheet" type="text/css" media="screen" href="/editor/css/czj-preview7.css" ></link>
    <link rel="stylesheet" type="text/css" media="screen" href="/editor/css/czj-preview7-min.css" ></link>

    <link href="/editor/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="/editor/lib/skin/functional.css" rel="stylesheet" type="text/css" />         
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web:400,200,300,600,700,900&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css" />
     
    <!-- <link rel="stylesheet" type="text/css" media="screen" href="/editor/czjprev4.css" /> -->
    <link rel="stylesheet" href="/editor/lib/jquery-ui.css"></link>
    <script type="text/javascript" src="/editor/lib/jquery.min.js"></script>
    <script src="/editor/lib/flowplayer.min.js"></script>
    <script src="/editor/lib/jquery-ui.js">//</script>
    <link rel="stylesheet" href="/editor/lib/minimalist.css"/>
    <script type="text/javascript" src="/media/fancybox/jquery.fancybox.pack.js"></script>
    <link rel="stylesheet" href="/media/fancybox/jquery.fancybox.css" type="text/css" media="screen" />

    <script type="text/javascript" src="/editor/imagesloaded.pkgd.min.js">//</script>
    <script type="text/javascript" src="/editor/czjmain.js">//</script>
    <script><![CDATA[
      function change_trans(type) {
        if ($('.trans'+type).css('display')=='none') {
          $('.trans'+type).css('display', 'inline-block');
        } else {
          $('.trans'+type).css('display', 'none');
        }
      }

      function change_video(type) {
        switch(type) {
        case 'front':
          $('.topvideosign_front').show();
          $('.topvideosign_side').hide();
          $('.videoselect-front').addClass('videoselect-active');
          $('.videoselect-side').removeClass('videoselect-active');
          break;
        case 'side':
          $('.topvideosign_front').hide();
          $('.topvideosign_side').show();
          $('.videoselect-front').removeClass('videoselect-active');
          $('.videoselect-side').addClass('videoselect-active');
          break;
        }
      }

      function change_view(eid) {
        if (document.getElementById(eid).style.display=='none') {
          if (eid == 'lemma_parts_box') { 
            if (document.getElementById(eid).getAttribute('data-type') == 'coll') {
              document.getElementById('lemma_parts_info').innerHTML = 'hide links to components';
            } else {
              document.getElementById('lemma_parts_info').innerHTML = 'show details';
            }
          }
          document.getElementById(eid).style.display='block';
          user_resize_sw();
        } else {
          if (eid == 'lemma_parts_box') { 
            if (document.getElementById(eid).getAttribute('data-type') == 'coll') {
              document.getElementById('lemma_parts_info').innerHTML = 'show links to components';
            } else {
              document.getElementById('lemma_parts_info').innerHTML = 'hide details';
            }
          }
          document.getElementById(eid).style.display='none';
        }
      }

      function player_change(pid) {
        if (document.getElementById(pid).style.display=='none') {
          document.getElementById(pid).style.display='block'; 
        } else {
          document.getElementById(pid).style.display='none'; 
        } 
      }

      $(document).ready(function() {
        $(".flowmouse").bind("mouseenter mouseleave", function(e) {
          flowplayer(this)[/ee/.test(e.type) ? "resume":"pause"]();
        });
      });

      $(document).ready(function() {
        $('#navselect').change(function() {
          var option = $(this.options[this.selectedIndex]).val();
          $('#sidevideo').css('display', 'none');
          $('#sidesw').css('display', 'none');
          $('#sidehamnosys').css('display', 'none');
          $('#side'+option).css('display', 'block');
        });

        $('.topvideosign_side').hide();

        $("a.fancybox").fancybox({
        'autoScale'      : false,
        'transitionIn'   : 'elastic',
        'transitionOut'  : 'elastic',
        'titlePosition'  : 'inside',
        'hideOnContentClick' : true,
        'speedIn'        : 100,
        'speedOut'     : 100,
        'changeSpeed'    : 100,
        'centerOnScroll' : false,
        padding: 0,
        closeClick: true
        });
        $("a.hand").fancybox({
        'autoScale'      : false,
        'transitionIn'   : 'elastic',
        'transitionOut'  : 'elastic',
        'titlePosition'  : 'inside',
        'hideOnContentClick' : true,
        'speedIn'        : 100,
        'speedOut'     : 100,
        'changeSpeed'    : 100,
        'centerOnScroll' : false,
        padding: 0,
        closeClick: true
        });
      });
      $("a.fancybox_avatar").fancybox({
        'autoScale'      : false,
        'width'          : 612,
        'height'         : 640,
        'transitionIn'  : 'elastic',
        'transitionOut'   : 'elastic',
        'titlePosition'  : 'inside',
        'hideOnContentClick' : false,
        'speedIn'        : 100,
        'speedOut'    : 100,
        'changeSpeed'    : 100,
        'centerOnScroll' : false
        });
    $('#sw_search_link').fancybox({
      'autoScale'      : false,
      'transitionIn'   : 'elastic',
      'transitionOut'  : 'elastic',
      'titlePosition'  : 'inside',
      'hideOnContentClick' : true,
      'speedIn'        : 100,
      'speedOut'     : 100,
      'changeSpeed'    : 100,
      'centerOnScroll' : false,
      'height': 500,
      autoSize: false,
      padding: 0,
      closeClick: true,
      afterLoad: function(cur, prev) {
        cur.content[0].contentWindow.onload(prepare_swe());
      }
    });

      function open_iframe(id, dict) {
        var iframe = document.getElementById('colloc'+id);
        if (iframe.style.display=='none') {
          iframe.style.display='inline';
        } else {
          iframe.style.display='none';
        }
        if (iframe.src == 'about:blank') {
          if (dict == 'cs') {
            iframe.src = '/cs?action=getdoc&amp;id='+id+'&amp;tr=inline-en';
          } else { 
            iframe.src = '/is?action=getdoc&amp;id='+id+'&amp;tr=inline-en';
          }
        }
      }
      function iframe_loaded(id) {
        var iframe = document.getElementById('colloc'+id);
        if (iframe.src != 'about:blank') {
          //alert(iframe.contentWindow.document.body.scrollHeight);
          iframe.height = iframe.contentWindow.document.body.scrollHeight + 'px;'
        }
      }

      $(function() {
        $("#search").tabs();
        $("#search-cs").tabs();
      });

      $(document).ready(function() {
        // set drag drop
        $('.swlink').draggable({
          overflow:'visible',containment:'document',appendTo: 'body', helper:'clone',
          start: function(event, ui) {
            $('#search').show();
          }
        });
        $('#search').droppable({
          activeClass: 'ui-state-hover',
          drop:function(event,ui){
            console.log(ui.draggable.attr('data-sw'));
            $('#swpreview').attr('src', 'http://znaky.zcu.cz/proxy/tts/signwriting.png?generator[sw]='+ui.draggable.attr('data-sw')+'&generator[align]=top_left&generator[set]=sw10');
            $('#searchsw').val(ui.draggable.attr('data-sw'));
            $("#select-tab-sw").click();
          }
        });
      });
      ]]></script>

    <style type="text/css">
      /* override jquery ui tabs*/
      #search, #search-cs {
        font-family: 'Titillium Web', sans-serif;
        font-size: 100%;
        border: 0;
      }
      .ui-tabs-nav {        border: 0;        background: white;      }
      .ui-tabs .ui-tabs-nav li a {
        padding: 0.2em 0.5em;
        color: white;
      }
      .ui-tabs .ui-tabs-nav li {
        background: #a3caaa;
        margin: 0;
      }
       .ui-tabs-nav li.ui-tabs-active {
        background: #a3ca49;
      }
      .ui-tabs-nav li.ui-tabs-active a {
        font-weight: bold;
      }
      .ui-tabs .ui-tabs-panel {
        padding: 0.2em;
      }
      .flowplayer .fp-buffer{background-color: white; }
      .flowplayer .fp-progress{background-color: #03C9A9; }  
a.none {color: black; text-decoration: none;}           
    </style>
  </head>
  <body>   Dictio.info
 
  
    <!---<xsl:apply-templates />-->
      <div class="czj-container">
        <div class="czj-inner-wrapper">
          <div class="czj-inner-left">
            PEVNÉ ZÁHLAVÍ LEMMATU 
            <div id="sidevideo" style="">
            <!--<xsl:apply-templates select="media/file[type='sign_front' or type='sign_side']" mode="side"/>-->
              <xsl:apply-templates select="media/file[location=//lemma/video_front][1]" mode="side">
                <xsl:with-param name="video_type">sign_front</xsl:with-param>
              </xsl:apply-templates>
              <xsl:apply-templates select="media/file[location=//lemma/video_side][1]" mode="side">
                <xsl:with-param name="video_type">sign_side</xsl:with-param>
              </xsl:apply-templates>
            </div>
            <div id="sidesw" style="display:none">
           <xsl:apply-templates select="lemma/swmix/sw" mode="rel"/>
         </div>
         <div id="sidehamnosys" style="display:none">
           <xsl:if test="lemma/hamnosys">
             <img width="200" src="http://znaky.zcu.cz/proxy/tts/tex2img.png?generator[template]=hamnosys&amp;generator[dpi]=200&amp;generator[engine]=x&amp;generator[tex]={lemma/hamnosys}"/>
           </xsl:if>
         </div>
            PEVNÉ ZÁHLAVÍ LEMMATU 
          
   </div>       
         <div class="czj-inner-right" data-simplebar="init">
        <div class="simplebar-scroll-content" style="padding-right: 17px; margin-bottom: -34px;">                
            <div class="simplebar-content" style="padding-bottom: 17px; margin-right: -17px;">                           
              <div class="czj-mainlemma">              
                  <xsl:if test="lemma/lemma_type='collocation' and ($perm!='ro' or collocations/@status!='hidden')">             
                    <!-- <xsl:attribute name="style">background-color: lightgrey;</xsl:attribute> -->            
                  </xsl:if> 
			 <!--zdroj čelní video-->                          
                  <xsl:if test="//lemma/video_front!=''">                 
                    <xsl:variable name="vf"><xsl:value-of select="//lemma/video_front"/>
                    </xsl:variable>                                       
                    <xsl:if test="//media/file[location=$vf]/id_meta_source!='' or //media/file[location=$vf]/id_meta_author!=''">
                      <div style="float: left; width: 648px;" class="source">
                      <xsl:choose>
                        <xsl:when test="//media/file[location=$vf]/id_meta_source!=''">Source: <xsl:value-of select="//media/file[location=$vf]/id_meta_source"/></xsl:when>                                                              
                        <xsl:when test="//media/file[location=$vf]/id_meta_source='' and //media/file[location=$vf]/id_meta_copyright='UP Olomouc'">Zdroj: LANGER, J. a kol. Znaková zásoba českého znakového jazyka ..</xsl:when>
                      <xsl:otherwise>Author: <a href="/asl?action=page&amp;page=about#skupiny"><xsl:value-of select="//media/file[location=$vf]/id_meta_author"/></a></xsl:otherwise>   
                      </xsl:choose>                                        
                      </div>
                    </xsl:if>                         
                    <div class="author-info" style="display:none;">                
                      <br />
                      <xsl:if test="//media/file[location=$vf]/id_meta_source!=''"> 
                      <span class="author-author">Author: 
                        <a href="/asl?action=page&amp;page=about#skupiny"><xsl:value-of select="//media/file[location=$vf]/id_meta_author"/></a> 
                      ; </span>
                      </xsl:if>
                      <xsl:if test="//media/file[location=$vf]/id_meta_copyright!=''">                                                       
                        <span class="author-copyright">Recorded by: <xsl:value-of select="//media/file[location=$vf]/id_meta_copyright"/></span>
                      </xsl:if>              
                    </div>            
                  </xsl:if>
                  <!--zdroj čelní video-->                
          <!--uživatel=<xsl:value-of select="/entry/@user_skupina"/> / heslo=<xsl:value-of select="/entry/lemma/pracskupina"/> / <xsl:value-of select="$skupina_test"/>--> 
                  <xsl:if test="$perm!='ro' or $skupina_test='true'">  
             <span style="float: right;">
               <xsl:if test="$perm!='ro'"><a href="/editoris/?id={@id}&amp;lang=en">edit<img src="/editor/img/edit.png" /></a></xsl:if>
               <xsl:choose>
                 <xsl:when test="lemma/@auto_complete='1'"><a style="color:blue;cursor:pointer" onclick="window.location = 'http://www.dictio.info'+window.location.pathname+window.location.search"><img src="/editor/publikovano.png" border="0" title="switch to public view"/></a></xsl:when>
                  <xsl:otherwise><img src="/editor/nepublikovano.png" title="lemma not published"/> </xsl:otherwise>
                </xsl:choose>
                    </span>             
		 <xsl:if test="$perm!='ro'">
                 <span class="publish">
                  <xsl:if test="/entry/lemma/completeness='1'">             
                    <xsl:attribute name="style">color: yellow;</xsl:attribute> 
                  </xsl:if> 
                   publishing: 
                <xsl:choose>
                   <xsl:when test="/entry/lemma/completeness='0'">automatic </xsl:when>
                   <xsl:when test="/entry/lemma/completeness='1'">none</xsl:when>                   
                   <xsl:when test="/entry/lemma/completeness='2'">all non-empty parts</xsl:when>
                   <xsl:when test="/entry/lemma/completeness='100'">approved parts only</xsl:when>
                </xsl:choose></span>
	        <span class="author-info" style="margin-left: 5px;"><strong>direct link: </strong> <span id="dirURL">https://www.dictio.info/is?action=search&amp;getdoc=<xsl:value-of select="@id"/>&amp;lang=en</span><button class="button-small" style="background-color: #D7E1E4; font-size: 10px; color: gray;" id="clipBoard" onclick="myFunction()">copy</button></span>               
			<br />
                    <span class="set-status">             ID: 
                  <xsl:value-of select="@id"/> | label: 
                      <xsl:value-of select="/entry/media/file[location=/entry/lemma/video_front]/label/text()"/>
              </span>               
         
                  <br />           
                  </xsl:if>           
                 </xsl:if>  
                  <xsl:if test="$perm='ro' and $skupina_test!='true'">  
			<span style="float: right;">
				<a href="/is?action=page&amp;page=nastaveni"><img src="editor/img/settings.png" border="0" title="nastavení zobrazení hesla"/></a>
			</span>
        	    </xsl:if>
                  <xsl:if test="lemma/lemma_type='collocation' and (($perm!='ro' or $skupina_test='true') or collocations/@status!='hidden')">               
               <span id="lemma_parts">
               <!-- <strong>Collocation. </strong>-->
                <a href="#" onclick="change_view('lemma_parts_box')" id="lemma_parts_info">Show links to components</a>
                <br />
                 <span id="lemma_parts_box" data-type="coll" style="display: none;">
                   <xsl:apply-templates select="collocations/colloc"/>
                 </span>
               </span>
             <div style="height: 1px; width: 100%; display: block; background-color: white; margin-top: 5px; margin-bottom: 10px;"></div>             
                  </xsl:if>
                  <div class="czj-mainlemma-inner">
                                   

           <div class="czj-mainlemma-video">
           <span class="videoblock">
              <div class="czj-mainlemma-videoblock">                      
             <!--<xsl:apply-templates select="media/file[type='sign_front' or type='sign_side']" mode="inlinetop"/>-->
          <xsl:if test="//lemma/video_front!=''">
                          <xsl:apply-templates select="media/file[location=//lemma/video_front and ($perm!='ro' or $skupina_test='true' or status!='hidden')][1]" mode="inlinetop2">                 
                 <xsl:with-param name="video_type">sign_front</xsl:with-param>
               </xsl:apply-templates>
             </xsl:if>
             <xsl:if test="@auto_complete!='1' and $perm='ro' and $skupina_test!='true'"><div style="position: absolute; background-color: yellow; top: 440px; margin-left: 30px; padding: 2px 50px 2px 50px;">CHYBA: nemáte dostatečná práva pro zobrazení všech částí tohoto hesla</div></xsl:if>              
             <xsl:if test="//lemma/video_front='' or not(//lemma/video_front)">
               <span class="topvideosign_front">
                 <img src="/media/img/emptyvideo.jpg"/>
               </span>
             </xsl:if>
             <xsl:if test="//lemma/video_side!=''">
                          <xsl:apply-templates select="media/file[location=//lemma/video_side and ($perm!='ro' or $skupina_test='true' or status!='hidden')][1]" mode="inlinetop2">                 
                 <xsl:with-param name="video_type">sign_side</xsl:with-param>
               </xsl:apply-templates>
             </xsl:if>
             <xsl:if test="//lemma/video_side='' or not(//lemma/video_side)">
               <span class="topvideosign_side">
                 <img src="/media/img/emptyvideo.jpg"/>
               </span>
             </xsl:if>
              </div>
              <div class="czj-mainlemma-videoblock-buttons">
                <a href="#" style="text-decoration: none; color: black;">
                  <span onclick="change_video('front');" class="videoselect videoselect-front videoselect-active">Front view</span>
                </a>
                <a href="#" style="text-decoration: none; color: black;">
                <span class="videoselect videoselect-side" onclick="change_video('side')">Side view</span>
                </a>
              </div>
                      <!--dominance -->              
                      <xsl:if test="//lemma/video_front!=''">                 
                        <xsl:variable name="vf">
                          <xsl:value-of select="//lemma/video_front"/>
                        </xsl:variable>                 
                        <xsl:choose>                   
                          <xsl:when test="//media/file[location=$vf]/orient='pr'">
                            <span style="color: #03C9A9;; position: relative;  top: -235px; left: +75px; z-index: 10;"> R
                            </span>
                          </xsl:when>                   
                          <xsl:when test="//media/file[location=$vf]/orient='lr'">
                            <span style="color: #03C9A9;; position: relative;  top: -235px; left: -178px; z-index: 10;"> L
                            </span>
                          </xsl:when>                 
                        </xsl:choose>               
                      </xsl:if>              
                      <div class="cleaner">
                      </div>           
                    </span>
                    <!-- videoblock -->                
                </div>
                  <xsl:if test="lemma/swmix/sw and ($perm!='ro' or $skupina_test='true' or lemma/@swstatus!='hidden' or (lemma/lemma_type!='single'))">                  
                  <div class="czj-mainlemma-sw">
                    <!-- <div class="czj-mainlemma-sw-text">SignWriting</div> -->
                    <div class="czj-mainlemma-sw-images">
                      <div class="czj-mainlemma-sw-images-inner">
                        <xsl:apply-templates select="lemma/swmix/sw"/>
                      </div>
                    </div>
                  </div>
                </xsl:if> 
                  <xsl:if test="lemma/hamnosys[$perm='ro' and @status!='hidden' and $skupina_test='true'] and lemma/hamnosys!=''">                                                                 
                <div id="hamnava">                  
                   <span id="avatar" class="transavatar">
                     <!--3D avatar:<br/>-->
                     <a data-fancybox-type="iframe" class="fancybox_avatar iframe" href="http://znaky.zcu.cz/avatar/show?unicode_hamnosys={lemma/hamnosys}">
                       <img src="/media/avatar.png" alt="click to play avatar"/>
                     </a>
                   </span>
                   </div>
               <div style="clear:both"></div>
                 </xsl:if>        
                  <!--<xsl:if test="lemma/hamnosys=''"><div style="width: 145px; height: 284px; background-color: silver;"></div></xsl:if>-->                                  
                 <!--schvalovani-->
                <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_lemmaczj')">
               <span class="set-status">
                 <br /><br />lemma type: 
                 <xsl:choose>
                   <xsl:when test="/entry/lemma/lemma_type='single'">single</xsl:when>
                   <xsl:when test="/entry/lemma/lemma_type='derivat'">derivative</xsl:when>
                   <xsl:when test="/entry/lemma/lemma_type='kompozitum'">compositum</xsl:when>
                   <xsl:when test="/entry/lemma/lemma_type='fingerspell'">fingerspelled sign</xsl:when>
                   <xsl:when test="/entry/lemma/lemma_type='collocation'">collocation</xsl:when>
                 </xsl:choose>
                 <xsl:call-template name="status_publish">
                   <xsl:with-param name="status" select="/entry/lemma/status"/>
                   <xsl:with-param name="type">entry</xsl:with-param>
                 </xsl:call-template>
               </span>
               <br/><br/>
             </xsl:if>
              <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_video')">
               <span class="set-status">front view: 
                 <xsl:if test="//file[location=//lemma/video_front and status='published']"><img src="/editor/img/checked.png" title="approved" /></xsl:if>
                      <xsl:if test="not(//file[location=//lemma/video_front and status='published'])">
                        <xsl:call-template name="status_publish">                   
                          <xsl:with-param name="status" select="//file[location=//lemma/video_front]/status"/>                   
                          <xsl:with-param name="type">video_front</xsl:with-param>                 
                        </xsl:call-template>               
                      </xsl:if>
               </span>
               <br/>
             </xsl:if>
              <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_video')">
               <span class="set-status">side view: 
                 <xsl:if test="//file[location=//lemma/video_side and status='published']"><img src="/editor/img/checked.png" title="approved" /></xsl:if>
                   <xsl:if test="not(//file[location=//lemma/video_side and status='published'])">
                     <xsl:call-template name="status_publish">                   
                       <xsl:with-param name="status" select="//file[location=//lemma/video_side]/status"/>                   
                       <xsl:with-param name="type">video_side</xsl:with-param>                 
                     </xsl:call-template>               
                   </xsl:if>
               </span>
               <br/>
             </xsl:if>
             <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_transkripce')">
               <span class="set-status">SW: 
                 <xsl:call-template name="status_publish">
                   <xsl:with-param name="status" select="/entry/lemma/@swstatus"/>
                   <xsl:with-param name="type">sw</xsl:with-param>
                 </xsl:call-template>
               </span>
               <br/>
             </xsl:if>
              <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_transkripce')">
               <span class="set-status">HNS:  
                 <xsl:call-template name="status_publish">
                   <xsl:with-param name="status" select="/entry/lemma/hamnosys/@status"/>
                   <xsl:with-param name="type">hns</xsl:with-param>
                 </xsl:call-template>
               </span>
               <br/>
             </xsl:if>  
            <!--schvalovani-->
                  <xsl:if test="lemma/hamnosys[($perm!='ro' or $skupina_test='true') or @status!='hidden'] and lemma/hamnosys!=''">                  
                  <div id="hamnosys" class="transhamn" style="width: 660px; height: 35px; float: left; background-color: white; margin-top: 20px; padding: 10px 10px 0px 10px;">
                     <xsl:if test="count(../sw)>1"></xsl:if>
                     <a class="fancybox" href="http://znaky.zcu.cz/proxy/tts/tex2img.png?generator[template]=hamnosys&amp;generator[dpi]=400&amp;generator[engine]=x&amp;generator[tex]={lemma/hamnosys}">
                       <img src="http://znaky.zcu.cz/proxy/tts/tex2img.png?generator[template]=hamnosys&amp;generator[dpi]=200&amp;generator[engine]=x&amp;generator[tex]={lemma/hamnosys}"/></a>
                   </div>
                   <xsl:if test="count(lemma/sw)>1"><br/></xsl:if>
               </xsl:if>
              </div>
              <!-- czj-mainlemma-inner -->
            </div> 
            <!-- czj-mainlemma -->
            <xsl:if test="((lemma/grammar_note/@slovni_druh!='' or lemma/grammar_note/variant or lemma/grammar_note/@slovni_druh!='') and (lemma/grammar_note/@status!='hidden' or ($perm!='ro' or $skupina_test='true'))) or ((lemma/lemma_type='derivat' or lemma/lemma_type='kompozitum') and (($perm!='ro' or $skupina_test='true') or collocations/@status!='hidden'))"> 
            <div class="czj-gram-popis">
              <img src="/editor/img/grey-corner.png" style="position: relative; top: 0px; left: -45px;" valign="top"/>
              <!--<div class="czj-lemma-h1">Gramatický popis</div>-->
                   <xsl:if test="lemma/grammar_note/@source!=''">
                      <span style="float: right;" class="source">Source: <xsl:value-of select="lemma/grammar_note/@source"/>;                                                                                                              
                      </span>
                  </xsl:if>                        
                  <div class="author-info" style="display:none">                
                    <xsl:if test="lemma/grammar_note/@author!=''">
                    <span class="author-author">Autor: 
                      <a href="/is?action=page&amp;page=about#skupiny"><xsl:value-of select="lemma/grammar_note/@author"/></a>, 
                    </span>
                    </xsl:if>                
                    <!--<span class="author-copyright">recorded by: 
                      <xsl:value-of select="lemma/grammar_note/@copyright"/>
                    </span>-->
                  </div>                
                  <!--schvalování-->               
                  <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_lingvist')">                 
                    <span class="set-status">grammar desc.:                     

                   <xsl:call-template name="status_publish">
                     <xsl:with-param name="status" select="/entry/lemma/grammar_note[1]/@status"/>
                     <xsl:with-param name="type">gram</xsl:with-param>
                   </xsl:call-template>
                 </span>
                </xsl:if>
               <!--schvalování konec-->
               <br />
                <xsl:if test="($perm!='ro' or $skupina_test='true') or lemma/grammar_note[1]/@status!='hidden'">
                <xsl:if test="lemma/lemma_type!='collocation' and lemma/grammar_note/@slovni_druh!=''"><strong>Lexical category</strong>                                             
                 <xsl:for-each select="lemma/grammar_note">
                   <xsl:if test="@slovni_druh!=''">        
                      <xsl:if test="count(//lemma/grammar_note)&gt;1"><strong><xsl:text> </xsl:text><xsl:number/></strong></xsl:if>:                       
                    
                     <xsl:if test="@skupina!=''">
                       <xsl:choose>
 
                        <xsl:when test="@skupina='konk'">general</xsl:when>
                         <xsl:when test="@skupina='abs'">general</xsl:when>
                         <xsl:when test="@skupina='jmen'">proper</xsl:when>
                         <xsl:when test="@skupina='proste'">plain</xsl:when>
                         <xsl:when test="@skupina='prostor' and @slovni_druh='verb'">indicating</xsl:when>
                         <xsl:when test="@skupina='shodove'">locative </xsl:when>
                         <xsl:when test="@skupina='modal'">modal </xsl:when>
                         <xsl:when test="@skupina='ukaz'">demonstrative</xsl:when>
                         <xsl:when test="@skupina='priv'">possessive</xsl:when>
                         <xsl:when test="@skupina='klas'">depicting</xsl:when>
                         <xsl:when test="@skupina='kval'">qualification</xsl:when>
                         <xsl:when test="@skupina='cas'">time</xsl:when>
                         <xsl:when test="@skupina='dej'">aspect</xsl:when>
                         <xsl:when test="@skupina='prostor' and @slovni_druh='modif'">space</xsl:when>
                         <xsl:when test="@skupina='zakl'">cardinal </xsl:when>
                         <xsl:when test="@skupina='rad'">ordinal</xsl:when>
                         <xsl:when test="@skupina='nas'">multiplicative</xsl:when>
                         <xsl:when test="@skupina='ikon'">iconic</xsl:when>
                         <xsl:when test="@skupina='neur'">indefinite</xsl:when>
                         <xsl:when test="@skupina='partneg'">negative</xsl:when>                         
                         <xsl:when test="@skupina='partcont'">contacting</xsl:when>                       
                       </xsl:choose>
                       <xsl:text> <!--mezera--></xsl:text>
                     </xsl:if>
                     <xsl:choose>
                       <xsl:when test="@slovni_druh='subst'">noun</xsl:when>
                       <xsl:when test="@slovni_druh='verb'">verb</xsl:when>
                       <xsl:when test="@slovni_druh='modif'">modifier</xsl:when>
                       <xsl:when test="@slovni_druh='pron'">pronoun</xsl:when>
                       <xsl:when test="@slovni_druh='num'">numeral</xsl:when>
                       <xsl:when test="@slovni_druh='konj'">conjunction</xsl:when>
                       <xsl:when test="@slovni_druh='part'">particle</xsl:when>
                       <xsl:when test="@slovni_druh='taz'">interrogative expression</xsl:when>
                       <xsl:when test="@slovni_druh='kat'">categorization expression</xsl:when>
                     </xsl:choose>
                     
                     <xsl:if test="@skupina2!=''">
                      <xsl:text> <!-- mezera --></xsl:text>
                      <xsl:call-template name="split_skupina2"><xsl:with-param name="text" select="@skupina2"/></xsl:call-template>
                     </xsl:if>
                     
                     <xsl:if test="@skupina3!=''">
                       <xsl:text>; </xsl:text>
                       <xsl:choose>
                         <!--<xsl:when test="@slovni_druh='verb'">subtype II: </xsl:when>-->
                         <xsl:when test="@slovni_druh='subst'">plural form derivation: </xsl:when>
                         <xsl:when test="@slovni_druh='cas'">plural form derivation: </xsl:when>
                       </xsl:choose>
                       <xsl:call-template name="split_skupina3"><xsl:with-param name="text" select="@skupina3"/></xsl:call-template>
                     </xsl:if>;
                   </xsl:if>
                 </xsl:for-each>
               </xsl:if>
                 <xsl:if test="lemma/grammar_note/@slovni_druh!=''"><br /></xsl:if>                          
                 <xsl:if test="lemma/grammar_note/@oral_komp!=''">
                   <b>Mouth gesture: </b> [<xsl:value-of select="lemma/grammar_note/@oral_komp"/>] 
                   <xsl:choose>
                     <!-- <xsl:when test="lemma/grammar_note/@oral_komp_sel='nezadano'"> <em>nezadáno</em></xsl:when>-->
                     <xsl:when test="lemma/grammar_note/@oral_komp_sel='povinny'"> -<em> povinný</em></xsl:when>
                     <xsl:when test="lemma/grammar_note/@oral_komp_sel='nepovinny'"> - <em>nepovinný</em></xsl:when>
                   </xsl:choose>
                   <xsl:if test="lemma/grammar_note/@mluv_komp!=''">, </xsl:if>
                 </xsl:if>
                 <xsl:if test="lemma/grammar_note/@mluv_komp!=''">
                   <b>Mouthing: </b> [<xsl:value-of select="lemma/grammar_note/@mluv_komp"/>]
                   <xsl:choose>
                     <!--<xsl:when test="lemma/grammar_note/@mluv_komp_sel='nezadano'"> - <em>nezadáno</em></xsl:when>-->
                     <xsl:when test="lemma/grammar_note/@mluv_komp_sel='povinny'"> - <em>compulsory</em></xsl:when>
                     <xsl:when test="lemma/grammar_note/@mluv_komp_sel='nepovinny'"> - <em>optional</em></xsl:when>
                   </xsl:choose>
                 </xsl:if>
                 <xsl:if test="lemma/grammar_note/@mluv_komp!='' or lemma/grammar_note/@oral_komp!=''"><br/> </xsl:if>
                 <xsl:if test="lemma/grammar_note!=''">
                   <xsl:apply-templates mode="grammar_note" select="lemma/grammar_note"/>
                 </xsl:if>
               </xsl:if>
               <xsl:if test="lemma/grammar_note/file"><br/></xsl:if>
               <xsl:for-each select="lemma/grammar_note/file">
                 <xsl:variable name="mid">
                   <xsl:value-of select="@media_id"/>
                 </xsl:variable>
                   <xsl:apply-templates select="//media/file[(@id=$mid or id=$mid) and main_for_entry and main_for_entry/@completeness!='1']" mode="var">
                     <xsl:with-param name="variant_sw" select="swmix"/>
                     <xsl:with-param name="in_grammar">true</xsl:with-param>
                     <xsl:with-param name="in_note">G<xsl:value-of select="position()"/></xsl:with-param>
                   </xsl:apply-templates>
                   <xsl:apply-templates select="//media/file[(@id=$mid or id=$mid) and (not(main_for_entry) or main_for_entry/@completeness='1')]">
                     <xsl:with-param name="in_grammar">true</xsl:with-param>
                     <xsl:with-param name="in_note">G<xsl:value-of select="position()"/></xsl:with-param>
                   </xsl:apply-templates>                 
               </xsl:for-each>
               <div class="cleaner"></div>
               <xsl:if test="lemma/grammar_note/variant">
                 <div class="czj-lemma-h2">Grammatical variants</div>
                 <div class="czj-relation-wrapper">
                 <xsl:apply-templates select="lemma/grammar_note/variant" mode="file">
                    <xsl:with-param name="gram_var_count">0</xsl:with-param>                        
                 </xsl:apply-templates>
                 </div>
               </xsl:if>
                  <xsl:if test="(lemma/lemma_type='derivat' or lemma/lemma_type='kompozitum') and (($perm!='ro' or $skupina_test='true') or collocations/@status!='hidden')">                 
                 <div id="lemma_parts">
                   <xsl:if test="lemma/grammar_note"><br/></xsl:if>
                   <strong>Etymology: </strong><a style="cursor:pointer;color:blue"  onclick="change_view('lemma_parts_box')" id="lemma_parts_info">view details</a><br />
                   <span id="lemma_parts_box" style="display:none">
                   <div class="czj-relation-wrapper">
                     <xsl:apply-templates select="collocations/colloc"/>               
                   </div>
                   </span>                   
                 </div>
               </xsl:if>              
            </div>
            <!-- czj-gram-popis -->
            </xsl:if>
            <xsl:if test="(lemma/style_note/file or lemma/style_note/variant or lemma/grammar_note/@region!='' or lemma/style_note/@kategorie!='' or lemma/style_note/@generace!='' or lemma/style_note/text()!='') and (lemma/style_note/@status!='hidden' or $perm!='ro')">
            <div class="czj-styl-popis">
                  <!--<img src="/editor/img/grey-corner.png" style="position: relative; top: 0px; left: -45px;" valign="top"/>-->                
            <xsl:if test="lemma/style_note/@source!=''">
                      <span style="float: right;" class="source">Source: <xsl:value-of select="lemma/style_note/@source"/>;                                     
                      </span>
                  </xsl:if>              
              <!--<div class="czj-lemma-h1">Stylistický popis</div>-->
              <div class="author-info" style="display:none">
                     <xsl:if test="lemma/style_note/@author!=''"> 
                    <span class="author-author">Autor: <a href="/is?action=page&amp;page=about#skupiny"><xsl:value-of select="lemma/style_note/@author"/></a>;</span>
                    </xsl:if>              
                    <!--<span class="author-copyright">Autor videozáznamu: <xsl:value-of select="lemma/style_note/@copyright"/></span>-->            
                  </div>               
                  <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_lingvist')">                 
                 <span class="set-status">stylistic description: 
                   <xsl:call-template name="status_publish">
                     <xsl:with-param name="status" select="/entry/lemma/style_note[1]/@status"/>
                     <xsl:with-param name="type">style</xsl:with-param>
                   </xsl:call-template>
                 </span>
               </xsl:if>
         <xsl:if test="lemma/style_note/file or lemma/style_note/variant or lemma/grammar_note/@region!='' or lemma/style_note/@kategorie!='' or lemma/style_note/@generace!='' or lemma/style_note/text()!=''">
           <br/>
           <div id="styletop">
             <div class="grammar">
                        <xsl:if test="($perm!='ro' or $skupina_test='true') or lemma/style_note/@status!='hidden'">  
                 <xsl:if test="lemma/style_note/@kategorie != ''">
                    <div>
                       <xsl:value-of select="lemma/style_note/@kategorie"/>
                    </div>
                 </xsl:if>
                 <xsl:if test="lemma/grammar_note/@region != ''">
                   <div><b>Distribution area: </b> 
                      <img src="/media/img/region/cr.png" class="oblast"/>
                         <xsl:call-template name="split_region">
                            <xsl:with-param name="text" select="lemma/grammar_note/@region"/>
                          </xsl:call-template>
                   </div>                 
                 </xsl:if>                                                      
                 
                 <xsl:if test="lemma/style_note/@generace != ''">
                  <div><b>Distribution age: </b> 
                              <xsl:call-template name="split_generace">
                                <xsl:with-param name="text" select="lemma/style_note/@generace"/>
                              </xsl:call-template>
                            </div>                 
                          </xsl:if>                 
                          <xsl:if test="lemma/style_note/@gender != ''">                    
                            <div><!--chybí lokalizace-->
                              <xsl:value-of select="lemma/style_note/@gender"/>
                            </div>                 
                 </xsl:if>
               </xsl:if>
                   <xsl:apply-templates mode="style_note" select="lemma/style_note"/>
                        <xsl:if test="lemma/style_note/file">
                          <br/>
                        </xsl:if>               
                        <xsl:for-each select="lemma/style_note/file">                 
                          <xsl:variable name="mid">
                            <xsl:value-of select="@media_id"/>
                          </xsl:variable>                   
                          <xsl:apply-templates select="//media/file[(@id=$mid or id=$mid) and main_for_entry and main_for_entry/@completeness!='1']" mode="var">                     
                            <xsl:with-param name="variant_sw" select="swmix"/>
                            <xsl:with-param name="in_grammar">true</xsl:with-param>                     
                            <xsl:with-param name="in_note">S<xsl:value-of select="position()"/>
                            </xsl:with-param>                   
                          </xsl:apply-templates>                   
                          <xsl:apply-templates select="//media/file[(@id=$mid or id=$mid) and (not(main_for_entry) or main_for_entry/@completeness='1')]">                     
                            <xsl:with-param name="in_grammar">true</xsl:with-param>                     
                            <xsl:with-param name="in_note">S<xsl:value-of select="position()"/>
                            </xsl:with-param>                   
                          </xsl:apply-templates>               
                        </xsl:for-each>               
                        <div class="cleaner">
                        </div>               
               <xsl:if test="lemma/style_note/variant">
                 <div class="czj-lemma-h2">Stylistic variants</div>
                 <div class="czj-relation-wrapper">                 
                    <xsl:apply-templates select="lemma/style_note/variant" mode="file">
                       <xsl:with-param name="gram_var_count" select="count(lemma/grammar_note/variant)"/>                                                            
                    </xsl:apply-templates>
                 </div>               
              </xsl:if>
             </div>
             </div>         
            </xsl:if>            
           </div> 
                <!-- czj-styl-popis -->          
          </xsl:if>                         
          <div class="czj-vyznam">
            <div id="meanings">
              <xsl:apply-templates select="meanings/meaning[($perm!='ro' or $skupina_test='true') or (status!='hidden' and (count(relation[@type='translation' and @status!='hidden'])>0 or is_translation_unknown='1')) or (status!='hidden' and //lemma/completeness='100')]">
                <xsl:sort select="@number" data-type="number"/>
              </xsl:apply-templates>
              <div style="clear:both">
              </div>
            </div>                                                       
          </div> 
           <!-- czj-vyznam -->            
            <div id="collocations"><!--slovni spojeni-->
             <xsl:if test="count(collocations/revcolloc[@lemma_type='collocation' and (($perm!='ro' or $skupina_test='true') or @auto_complete='1')])>0">
             <!--<span class="divtitle">Slovní spojení s tímto heslem</span>-->
                 <img src="/editor/img/grey-corner.png" style="position: relative; top: 0px; left: -45px;" valign="top"/>
              <div class="czj-lemma-h1">Collocation with this lemma</div>
             <div class="collocations">
                    <xsl:apply-templates select="collocations/revcolloc[@lemma_type='collocation' and (($perm!='ro' or $skupina_test='true') or @auto_complete='1')]"/>             
             </div>
             <div style="clear:both">
             </div>
           </xsl:if>
         </div>
        <input type="button" class="button-zdroje" value="show/hide autors" id="show-author" onclick="$('.author-info').toggle();"/> 
         </div></div></div> <!-- czj-right -->
        </div> <!-- czj-wrapper -->
        <div class="cleaner"></div>
      </div> <!-- czj-container -->        
       <div id="outer">
       <div id="side">
         <div id="sidevideo" style="">
           <!--<xsl:apply-templates select="media/file[type='sign_front' or type='sign_side']" mode="side"/>-->
         </div>
         <div id="sidesw" style="display:none">
           <xsl:apply-templates select="lemma/swmix/sw" mode="rel"/>
         </div>
         <div id="sidehamnosys" style="display:none">
           <xsl:if test="lemma/hamnosys">
             <img width="200" src="http://znaky.zcu.cz/proxy/tts/tex2img.png?generator[template]=hamnosys&amp;generator[dpi]=200&amp;generator[engine]=x&amp;generator[tex]={lemma/hamnosys}"/>
           </xsl:if>
         </div>
    </div>
</div>
     
     <!-- Bootstrap core JavaScript -->
     <!-- Placed at the end of the document so the pages load faster -->
     <script src="/editor/lib/bootstrap.min.js"></script>
   </body>
 </html>
</xsl:template>

<xsl:template match="file" mode="grammar_note">
  <em>see video G<xsl:value-of select="count(preceding-sibling::file)+1"/></em>
</xsl:template>
<xsl:template match="file" mode="style_note">
  <em>see video S<xsl:value-of select="count(preceding-sibling::file)+1"/></em>
</xsl:template>

<xsl:template match="sw">
  <xsl:if test=".!=''">
    <span class="sw" style="vertical-align:top; display:inline-block;">
     <div class="author-info" style="display:none">
          <xsl:if test="@source!=''"><span class="author-source">Source: <xsl:value-of select="@source"/>; </span></xsl:if>      
      <span class="author-author">Author: <a href="/is?action=page&amp;page=about#skupiny"><xsl:value-of select="@author"/></a>; </span>      
<!--      <span class="author-copyright">recorded by: <xsl:value-of select="@copyright"/></span>-->
    </div>
  <a href="http://znaky.zcu.cz/proxy/tts/signwriting.png?generator[sw]={.}&amp;generator[align]=top_left&amp;generator[set]=sw10" class="fancybox swlink ui-draggable" data-sw="{.}">
      <img src="http://znaky.zcu.cz/proxy/tts/signwriting.png?generator[sw]={.}&amp;generator[align]=top_left&amp;generator[set]=sw10" alt="signwriting" class="signwriting" ratio="0.8"/>
    </a>
    </span>
  </xsl:if>
</xsl:template>

<xsl:template match="sw" mode="rel">
  <xsl:if test=".!=''">
    <a href="http://znaky.zcu.cz/proxy/tts/signwriting.png?generator[sw]={.}&amp;generator[align]=top_left&amp;generator[set]=sw10" class="fancybox swlink ui-draggable" data-sw="{.}">
      <img src="http://znaky.zcu.cz/proxy/tts/signwriting.png?generator[sw]={.}&amp;generator[align]=top_left&amp;generator[set]=sw10" alt="signwriting" class="signwriting" ratio="0.5" style=""/>
    </a>
  </xsl:if>
</xsl:template>

<xsl:template match="variant" mode="style_note">
</xsl:template>

<xsl:template match="variant" mode="grammar_note">
</xsl:template>

  <xsl:template match="variant" mode="file">
    <xsl:param name="gram_var_count"/>
    <xsl:variable name="f">
      <xsl:value-of select="./text()"/>
    </xsl:variable>  
     <xsl:if test="($perm!='ro' or $skupina_test='true')">
      <xsl:apply-templates select="//media/file[(@id=$f) and (main_for_entry)]" mode="var">    
        <xsl:with-param name="variant_sw" select="swmix"/>
        <xsl:with-param name="variant_pos" select="position()+$gram_var_count"/> 
      </xsl:apply-templates>  
      <xsl:apply-templates select="//media/file[(@id=$f) and (not(main_for_entry))]">    
        <xsl:with-param name="parent_id" select="position()"/>    
        <xsl:with-param name="variant_sw" select="swmix"/>
        <xsl:with-param name="variant_pos" select="position()+$gram_var_count"/> 
      </xsl:apply-templates>
    </xsl:if>
    <xsl:if test="not($perm!='ro' or $skupina_test='true')">
      <xsl:apply-templates select="//media/file[(@id=$f) and (main_for_entry and main_for_entry/@completeness!='1')]" mode="var">    
        <xsl:with-param name="variant_sw" select="swmix"/>
        <xsl:with-param name="variant_pos" select="position()+$gram_var_count"/> 
      </xsl:apply-templates>  
      <xsl:apply-templates select="//media/file[(@id=$f) and (not(main_for_entry) or main_for_entry/@completeness='1')]">    
        <xsl:with-param name="parent_id" select="position()"/>    
        <xsl:with-param name="variant_sw" select="swmix"/>
        <xsl:with-param name="variant_pos" select="position()+$gram_var_count"/> 
      </xsl:apply-templates>
    </xsl:if>
  <xsl:if test="@desc!=''"><span class="author-info">pozn.: <xsl:value-of select="@desc"/></span><!-- desc varianta --></xsl:if>          
  </xsl:template>
  
  <xsl:template match="file" mode="inline"><!--video význam -->  
  <xsl:param name="media_id"/>
  <xsl:if test="(id=$media_id or @id=$media_id) and $media_id!=''">
        <xsl:if test="$perm!='ro' or $skupina_test='true'">      
        <span class="publish"><xsl:value-of select="location"/></span> 
     <br />
    </xsl:if>
          <span id="flowvideoin{@id}" data-ratio="0.8" class="meaningfile" style="width:285px; background:#000 url(/media/video{$dictcode}/thumb/{location}/thumb.jpg) no-repeat; background-size: 285px 228px">    
            <video poster="/media/video{$dictcode}/thumb/{location}/thumb.jpg" controls="" width="285px" height="228px">              
                <source type="video/mp4" src="/media/video{$dictcode}/{location}"/>              
            </video>      
          </span>
          <xsl:choose>
            <xsl:when test="location='Synonymum-odkaz.mp4'"><span id="video-odkaz" style="color: white; position: relative;  top: -20px; left: -190px; z-index: 10; ">see synonymum</span></xsl:when>
            <xsl:when test="location='Varianty-odkaz.mp4'"><span id="video-odkaz" style="color: white; position: relative;  top: -20px; left: -190px; z-index: 10; ">see variant 1</span></xsl:when>
          </xsl:choose>          
        <br />    
        <div style="float: left; width: 286px;" class="source">
          <xsl:if test="id_meta_source!='' or id_meta_author!=''">
              <span class="source">
              <xsl:choose>
                 <xsl:when test="id_meta_source!=''">Source: <xsl:value-of select="id_meta_source"/></xsl:when>                   
                 <xsl:otherwise>Author: <a href="/is?action=page&amp;page=about#skupiny"><xsl:value-of select="id_meta_author"/></a>;</xsl:otherwise>                   
              </xsl:choose>                                        
              <xsl:text> </xsl:text></span>              
         </xsl:if>
         
         <div class="author-info" style="display:none">      
            <xsl:if test="id_meta_source!=''">
            <span class="author-author">Author: <a href="/is?action=page&amp;page=about#skupiny"><xsl:value-of select="id_meta_author"/></a>; </span>
         </xsl:if>    
             <xsl:if test="id_meta_copyright!=''">     
            <span class="author-copyright">recorded by: <xsl:value-of select="id_meta_copyright"/></span> 
            </xsl:if>   
        </div> 
      </div>     
    </xsl:if>
  </xsl:template>
  <!--<xsl:template match="file" mode="inlinetop">  
  <span id="flowvideoin{@id}" data-ratio="0.8" class="flowplayer meaningfile topvideo{type}" style="width:285px; background:#777 url(/media/video{$dictcode}/thumb/{location}/thumb.jpg) no-repeat; background-size: 285px 228px">
    <video >
        <xsl:if test="substring(location, string-length(location)-3, 4) = '.flv'">
          <source type="video/flash" src="/media/video{$dictcode}/{location}"/>
        </xsl:if>
        <xsl:if test="substring(location, string-length(location)-3, 4) = '.mp4'">
          <source type="video/mp4" src="/media/video{$dictcode}/{location}"/>
        </xsl:if>
      </video>    
    </span>
  </xsl:template>-->
  <!-- čelní / boční video -->
<xsl:template match="file" mode="inlinetop2">
  <xsl:param name="video_type"/>
  <!--<xsl:if test="substring(location, string-length(location)-3, 4) = '.flv'">
      <span id="flowvideoin{@id}" data-ratio="0.8" class="flowplayer meaningfile topvideo{$video_type}" style="width:285px; background:#777 url(/media/video{$dictcode}/thumb/{location}/thumb.jpg) no-repeat; background-size: 285px 228px">      
        <video > 
          <xsl:if test="substring(location, string-length(location)-3, 4) = '.flv'">
            <source type="video/flash" src="/media/video{$dictcode}/{location}"/>
          </xsl:if>
          <xsl:if test="substring(location, string-length(location)-3, 4) = '.mp4'">
            <source type="video/mp4" src="/media/video{$dictcode}/{location}"/>
          </xsl:if>
        </video>    
      </span>  
    </xsl:if>-->  
    <xsl:if test="substring(location, string-length(location)-3, 4) = '.mp4'">
      <span id="flowvideoin{@id}" data-ratio="0.8" class="meaningfile topvideo{$video_type}" style="width:285px; background:#777 url(/media/video{$dictcode}/thumb/{location}/thumb.jpg) no-repeat; background-size: 285px 228px">      
        <video controls="" width="285px" height="228px" poster="/media/video{$dictcode}/thumb/{location}/thumb.jpg">      
          <source type="video/mp4" src="/media/video{$dictcode}/{location}"/>      
        </video>    
      </span>  
    </xsl:if>  
    <xsl:if test="not(substring(location, string-length(location)-3, 4) = '.flv' or substring(location, string-length(location)-3, 4) = '.mp4')">
      <span  class="meaningfile topvideo{$video_type}" style="width:285px; background:#777 url(/media/video{$dictcode}/{location}) no-repeat; background-size: 285px 228px">      
        <img style="width:285px;height:228px" src="/media/video{$dictcode}/{location}"/>    
      </span>  
    </xsl:if>
</xsl:template>

<xsl:template match="file" mode="side">
    <div id="flowvideo{id}" data-ratio="0.8" class="flowmouse" style="float: left; width:120px; background:#777 url(/media/video{$dictcode}/thumb/{location}/thumb.jpg) no-repeat; background-size: 120px 96px; float: left;">    
      <video loop="loop" poster="/media/video{$dictcode}/thumb/{location}/thumb.jpg" width="120px" height="96px" onmouseover="this.play()" onmouseout="this.pause()">   
          <source type="video/mp4" src="/media/video{$dictcode}/{location}"/>
      </video>
    </div>
</xsl:template>

<xsl:template match="file" mode="rel">
     <xsl:param name="target"><xsl:value-of select="$dictcode"/></xsl:param>
    <xsl:if test="substring(location, string-length(location)-3, 4) = '.flv'">
      <div id="flowvideorel{id}" data-ratio="0.8" class="flowplayer flowmouse" style="width:120px; background:#777 url(/media/video{$target}/thumb/{location}/thumb.jpg) no-repeat; background-size: 120px 96px; float: left;">    
    <video loop="loop">
      <source type="video/flash" src="/media/video{$target}/{location}"/>
    </video>
  </div>
      </xsl:if>
      <xsl:if test="substring(location, string-length(location)-3, 4) = '.mp4'">      
        <div id="flowvideorel{id}" data-ratio="0.8" class="flowmouse" style="width:120px; height: 96px; background:#000 url(/media/video{$target}/thumb/{
      location}/thumb.jpg) no-repeat; background-size: 120px 96px; float: left;">
          <video loop="loop" poster="/media/video{$target}/thumb/{location}/thumb.jpg" width="120px" height="96px" onmouseover="this.play()" onmouseout="this.pause()">
            <source type="video/mp4" src="/media/video{$target}/{location}"/>
          </video>
         </div>   
      </xsl:if>
  <script type="text/javascript">   
    $("#flowvideorel<xsl:value-of select="id"/>").on("click", function(e) {
   window.location='/<xsl:value-of select="$target"/>?action=search&amp;getdoc=<xsl:value-of select="../@lemma_id"/>;&amp;lang=en';
    });
  </script>
</xsl:template>

<xsl:template match="file" mode="var">
  <xsl:param name="var_id"/> 
  <xsl:param name="in_grammar"/> 
  <xsl:param name="in_note"/> 
  <xsl:param name="variant_sw"/>
  <xsl:param name="variant_pos"/>  
  <div class="czj-relation-item">
    <div class="czj-relation relationvideo">
      <div class="czj-lemma-preview-sm">
        <div class="czj-lemma-preview-sm-header">                   
          <xsl:if test="$in_note!=''">
              <div class="alignleft">
                <xsl:value-of select="$in_note"/>
              </div>            
            </xsl:if>            
            <xsl:if test="$in_note=''">              
              <div class="alignleft">V<xsl:value-of select="$variant_pos"/>  
                 <xsl:if test="$perm!='ro' or $skupina_test='true'">
                  <span class="set-status">ID
                    <xsl:value-of select="main_for_entry/@lemma_id"/>
                  </span> 
                </xsl:if>                
              </div>            
            </xsl:if>            
            <div class="alignright">
              <a href="/is?action=search&amp;getdoc={main_for_entry/@lemma_id}&amp;lang=en">                
                <img src="/media/img/slovnik-open_w.png" height="20"/></a>            
            </div>          
          </div>      
        </div>
        <div style="clear:both">
        </div>  
  <div id="flowvideovar{@id}" data-ratio="0.8" class="flowmouse" style="width:120px; background:#777 url(/media/video{$dictcode}/thumb/{location}/thumb.jpg) no-repeat; background-size: 120px 96px; float: left;">
         <video loop="loop" poster="/media/video{$dictcode}/thumb/{location}/thumb.jpg" width="120px" height="96px" onmouseover="this.play()" onmouseout="this.pause()">
              <source type="video/mp4" src="/media/video{$dictcode}/{location}"/>            
    </video>
  </div>
  <div class="czj-lemma-preview-sm-sw">  
    <span class="sw transsw" style="vertical-align: top; display: inline-block; height: 96px;">
            <xsl:if test="($variant_sw/sw)">
              <xsl:apply-templates select="$variant_sw/sw" mode="rel"/>
            </xsl:if>  
            <xsl:if test="not($variant_sw/sw)">
              <xsl:if test="main_for_entry[1]/@lemma_type='single' or not(main_for_entry[1]/@lemma_type)">        
                <xsl:if test="main_for_entry[1]/swmix/sw[@primary='true']">          
                  <xsl:apply-templates select="main_for_entry[1]/swmix/sw[@primary='true']" mode="rel"/>        
                </xsl:if>        
                <xsl:if test="not(main_for_entry[1]/swmix/sw[@primary='true'])">          
                  <xsl:apply-templates select="main_for_entry[1]/swmix/sw[1]" mode="rel"/>        
                </xsl:if>      
              </xsl:if>      
              <xsl:if test="main_for_entry[1]/@lemma_type!='single'">        
                <xsl:apply-templates select="main_for_entry[1]/swmix/sw" mode="rel"/>      
              </xsl:if>  
            </xsl:if>  
      </span>
  </div>

  <script type="text/javascript">
    $("#flowvideovar<xsl:value-of select="id"/>").on("click", function(e) {
    window.location='/is?action=search&amp;getdoc=<xsl:value-of select="main_for_entry/@lemma_id"/>&amp;lang=en';
      });
  </script> 
  </div>
  </div>
</xsl:template>
<xsl:template match="file" mode="colloc">
  <div id="flowvideocol{id}" data-ratio="0.8" class="flowplayer flowmouse" style="width:120px; background:#777 url(/media/video{$dictcode}/thumb/{location}/thumb.jpg) no-repeat; background-size: 120px 96px">
    <video loop="loop">
        <xsl:if test="substring(location, string-length(location)-3, 4) = '.flv'">
          <source type="video/flash" src="/media/video{$dictcode}/{location}"/>
        </xsl:if>
        <xsl:if test="substring(location, string-length(location)-3, 4) = '.mp4'">
          <source type="video/mp4" src="/media/video{$dictcode}/{location}"/>
        </xsl:if>
    </video>
  </div>
  <script type="text/javascript">
    $("#flowvideocol<xsl:value-of select="id"/>").on("click", function(e) {
      open_iframe('<xsl:value-of select="../@lemma_id"/>', 'is', 'en');
    });
  </script>
</xsl:template>

<xsl:template match="file">
  <xsl:param name="parent_id"/>
  <xsl:param name="variant_sw"/>
  <xsl:param name="variant_pos"/>  
  <xsl:param name="in_grammar"/> 
  <xsl:param name="in_note"/> 
  <div class="czj-relation-item">
      <xsl:if test="not($variant_sw)">
        <xsl:attribute name="style">background-color: grey;>
        </xsl:attribute>
      </xsl:if>      
    <div class="czj-relation relationvideo">
        <div class="czj-lemma-preview-sm">
          <div class="czj-lemma-preview-sm-header" style="background-color: none;">
            <xsl:if test="$in_note!=''">
              <div class="alignleft">
                <xsl:value-of select="$in_note"/>
              </div>            
            </xsl:if>            
            <xsl:if test="$in_note=''">              
              <div class="alignleft">V<xsl:value-of select="$variant_pos"/>              
              </div>            
            </xsl:if>            
            <div class="alignright">
            </div>          
          </div>        
        </div>  
        <div id="flowvideorel{id}" data-ratio="0.8" class="flowmouse" style="width:120px; height: 96px; background:#000 url(/media/video{$dictcode}/thumb/{location}/thumb.jpg) no-repeat; background-size: 120px 96px">
          <video loop="loop" poster="/media/video{$dictcode}/thumb/{location}/thumb.jpg" width="120px" height="96px" onmouseover="this.play()" onmouseout="this.pause()">
            <source type="video/mp4" src="/media/video{$dictcode}/{location}"/>
          </video>
        </div>   
                                  
        <xsl:if test="$variant_sw">  
          <div class="czj-lemma-preview-sm-sw">      
            <span class="sw transsw" style="vertical-align: top; display: inline-block; height: 96px;">    
              <xsl:apply-templates select="$variant_sw/sw" mode="rel"/>    
            </span>    
          </div>  
        </xsl:if>  
        <xsl:if test="main_for_entry/sw and not($variant_sw)">    
    <span class="sw transsw" style="vertical-align: top; display: inline-block; height: 96px;">
    <!--<span class="sw transsw" style="vertical-align:top; display:inline-block;">-->
      <xsl:if test="main_for_entry/sw[@primary='true']">
        <xsl:apply-templates select="main_for_entry/sw[@primary='true']" mode="rel"/>
      </xsl:if>
      <xsl:if test="not(main_for_entry/sw[@primary='true'])">
        <xsl:apply-templates select="main_for_entry/sw[1]" mode="rel"/>
      </xsl:if>
    </span>
  </xsl:if>
  </div>
  </div>
  <script type="text/javascript">
   <xsl:variable name="jstype">
    <xsl:if test="contains(location,'.flv')">flash</xsl:if>
    <xsl:if test="contains(location,'.mp4')">mp4</xsl:if>
  </xsl:variable>
    $("#flowvideo2<xsl:value-of select="@id"/>-<xsl:value-of select="$parent_id"/>").on("click", function(e) {
      flowplayer(this).pause();
      var container = $('<div/>');
      console.log(container);
      container.css('background', '#777 url(/media/video<xsl:value-of select="$dictcode"/>/thumb/<xsl:value-of select="location"/>/thumb.jpg) no-repeat');
      container.css('background-size', '285px 228px');
      var video_type = ('/media/video{$dictcode}/<xsl:value-of select="location"/>'.substr(-3, 3) == 'mp4')? 'mp4':'flash';
      container.flowplayer({
        autoPlay: true,
        autoBuffering: true,
        width: 285,                             
        height: 228,
        ratio: 0.8,
        playlist: [
        [{'<xsl:value-of select="$jstype"/>': '/media/video<xsl:value-of select="$dictcode"/>/<xsl:value-of select="location"/>'}]
        ],
      });
      $.fancybox({
        content: container,
        width: 285,
        height: 228,
        scrolling: 'no',
        autoSize: false
      });
      flowplayer(container[0]).load([{'<xsl:value-of select="$jstype"/>': '/media/video<xsl:value-of select="$dictcode"/>/<xsl:value-of select="location"/>'}]);
       flowplayer(container[0]).resume();
    });
  </script>
</xsl:template>

<xsl:template match="file" mode="usage">
  <xsl:param name="parent_id"/>
  <xsl:param name="variant_sw"/>
  <div class="usage">
        <div id="flowvideo{@id}-{$parent_id}" data-ratio="0.8" class="usage" style="width:120px;">
          <video controls="" width="120px" height="96px" poster="/media/video{$dictcode}/thumb/{location}/thumb.jpg" onmouseover="this.play()" onmouseout="this.pause()">        
            <source type="video/mp4" src="/media/video{$dictcode}/{location}"/>      
          </video>      
        </div>  
        <xsl:choose>
            <xsl:when test="location='Synonymum-odkaz.flv'"><span id="video-odkaz" style="color: white; position: relative;  top: -5px; left: -122px; z-index: 10; ">viz synonymum</span></xsl:when>
            <xsl:when test="location='Varianty-odkaz.flv'"><span id="video-odkaz" style="color: white; position: relative;  top: -5px; left: -112px; z-index: 10; ">viz varianta 1</span></xsl:when>            
        </xsl:choose>                                      
           <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_lemmaczj')"><div class="publish"><xsl:value-of select="location"/></div></xsl:if>         
      <xsl:if test="id_meta_source!='' or id_meta_author!=''">
         <span style="float: left;" class="source"> 
         <xsl:choose>
            <xsl:when test="id_meta_source!=''">Source: <xsl:value-of select="id_meta_source"/></xsl:when>                   
            <xsl:otherwise>Author: <a href="/is?action=page&amp;page=about#skupiny"><xsl:value-of select="id_meta_author"/></a></xsl:otherwise>                   
         </xsl:choose>                                        
         </span>
      </xsl:if>
        <div class="author-info" style="display:none">   
          <xsl:if test="id_meta_source!=''">   
          <span class="author-author">Author: 
            <a href="/is?action=page&amp;page=about#skupiny"><xsl:value-of select="id_meta_author"/></a>; 
          </span>
          </xsl:if>
          <xsl:if test="id_meta_copyright!=''">                  
            <span class="author-copyright">recorded by: <br /><xsl:value-of select="id_meta_copyright"/></span>
          </xsl:if>    
        </div>    
    </div>  
    <xsl:if test="substring(location, string-length(location)-3, 4) = '.mp4'">
      <script type="text/javascript">
        $("#flowvideo<xsl:value-of select="@id"/>-<xsl:value-of select="$parent_id"/>").on("click", function(e) {
          var container = $('<div data-ratio="0.8" style="width:285px;">
            <video controls="" width="285px" height="228px" poster="/media/video{$dictcode}/thumb/{location}/thumb.jpg" autoplay="">
            <source type="video/mp4" src="/media/video{$dictcode}/{location}"/>
            </video>
            </div>');
          $.fancybox({
            content: container,
            width: 285,
            height: 228,
            scrolling: 'no',
            autoSize: false
          });
        });
      </script>
    </xsl:if>
    <xsl:if test="substring(location, string-length(location)-3, 4) = '.flv'">
      <script type="text/javascript">
      $("#flowvideo<xsl:value-of select="@id"/>-<xsl:value-of select="$parent_id"/>").on("click", function(e) {
        flowplayer(this).pause();
        var container = $('<div/>');
        console.log(container);
        container.css('background', '#777 url(/media/video<xsl:value-of select="$dictcode"/>/thumb/<xsl:value-of select="location"/>/thumb.jpg) no-repeat');
        container.css('background-size', '285px 228px');
        container.flowplayer({
          autoPlay: true,
          autoBuffering: true,
          width: 285,                             
          height: 228,
          ratio: 0.8,
          playlist: [
          [{'flash': '/media/video<xsl:value-of select="$dictcode"/>/<xsl:value-of select="location"/>'}]
          ],
        });
        $.fancybox({
          content: container,
          width: 285,
          height: 228,
          scrolling: 'no',
          autoSize: false
        });
        flowplayer(container[0]).load([{'<xsl:value-of select="$jstype"/>': '/media/video<xsl:value-of select="$dictcode"/>/<xsl:value-of select="location"/>'}]);
        flowplayer(container[0]).resume();
        });
      </script>
    </xsl:if>
</xsl:template>

<xsl:template match="meaning">
  <img src="/editor/img/grey-corner.png" style="position: relative; top: 0px; left: -45px;" valign="top"/>  
    <div class="meaningtop">         
 
       <xsl:if test="$perm!='ro' or text!=''"><!--pokud neni video vyklad-->     
      <div class="czj-lemma-h1">Meaning     
        <xsl:if test="count(//meanings/meaning[status='published'])>1">
          <xsl:value-of select="position()"/>
        </xsl:if>
      <xsl:if test="count(//meanings/meaning[status='published'])&lt;=1 and ($perm!='ro' or $skupina_test='true')">
                      <xsl:value-of select="@number"/>
                  </xsl:if>
                  <xsl:if test="($perm!='ro' or $skupina_test='true') and @number!=substring-after(@id,'-')"> <span class="source">[! id:<xsl:value-of select="@id"/>]</span></xsl:if>
        <xsl:if test="@source!='' or @copyright!=''"><span style="float: right;" class="source">Source: <xsl:value-of select="@source"/> 
          <xsl:if test="@source='' and @copyright!=''"><xsl:value-of select="@copyright"/></xsl:if>         
          </span><br />
        </xsl:if>
        <div class="source" style="display:none; float: right;">      
          <xsl:if test="@author!=''"><span class="author-author">author: <a href="/is?action=page&amp;page=about#skupiny"><xsl:value-of select="@author"/></a>; </span></xsl:if>      
          <xsl:if test="@copyright!=''"><span class="author-copyright">recorded by: <xsl:value-of select="@copyright"/></span></xsl:if>    
        </div>          
      </div>    
</xsl:if><!--neni video vyklad konec-->    
    <div id="meaning{@id}" class="meaning">
    <xsl:if test="category!=''">
        <span class="category"><b>Semantic field: </b>
          <xsl:choose>
        <xsl:when test="category=6">informatics</xsl:when>
            <xsl:when test="category=14">business</xsl:when>
            <xsl:when test="category=28">biology</xsl:when>
            <xsl:when test="category=27">mathematics</xsl:when>
            <xsl:when test="category='anat'">anatomy</xsl:when>
            <xsl:when test="category='antr'">anthropology</xsl:when>
            <xsl:when test="category='archeol'">archeology</xsl:when>
            <xsl:when test="category='archit'">architecture</xsl:when>
            <xsl:when test="category='biol'">biology</xsl:when>
            <xsl:when test="category='bot'">botanics</xsl:when>
            <xsl:when test="category='dipl'">diplomatics</xsl:when>
            <xsl:when test="category='div'">theatre</xsl:when>
            <xsl:when test="category='dopr'">transport</xsl:when>
            <xsl:when test="category='ekol'">ecology</xsl:when>
            <xsl:when test="category='ekon'">economics</xsl:when>
            <xsl:when test="category='eltech'">electrotechnics</xsl:when>
            <xsl:when test="category='etn'">ethnography</xsl:when>
            <xsl:when test="category='feud'">feudalism</xsl:when>
            <xsl:when test="category='filat'">philately</xsl:when>
            <xsl:when test="category='film'">film</xsl:when>
            <xsl:when test="category='filoz'">philosophy</xsl:when>
            <xsl:when test="category='fot'">photography</xsl:when>
            <xsl:when test="category='fyz'">physics</xsl:when>
            <xsl:when test="category='fyziol'">physiology</xsl:when>
            <xsl:when test="category='geol'">geology</xsl:when>
            <xsl:when test="category='geom'">geometry</xsl:when>
            <xsl:when test="category='gnoz'">gnoseology</xsl:when>
            <xsl:when test="category='hist'">history</xsl:when>
            <xsl:when test="category='horn'">mining</xsl:when>
            <xsl:when test="category='horol'">mountaineering</xsl:when>
            <xsl:when test="category='hosp'">industry</xsl:when>
            <xsl:when test="category='hud'">musicology</xsl:when>
            <xsl:when test="category='hut'">metallurgy</xsl:when>
            <xsl:when test="category='hvězd'">astronomy</xsl:when>
            <xsl:when test="category='chem'">chemistry</xsl:when>
            <xsl:when test="category='ideal'">idealism</xsl:when>
            <xsl:when test="category='jad'">nuclear physics</xsl:when>
            <xsl:when test="category='jaz'">philology</xsl:when>
            <xsl:when test="category='kapit'">capitalism</xsl:when>
            <xsl:when test="category='karet'">card expression</xsl:when>
            <xsl:when test="category='katol církvi'">catholicism</xsl:when>
            <xsl:when test="category='krim'">criminology</xsl:when>
            <xsl:when test="category='křesť'">christianity</xsl:when>
            <xsl:when test="category='kuch'">cookery</xsl:when>
            <xsl:when test="category='kult'">culture</xsl:when>
            <xsl:when test="category='kyb'">cybernetics</xsl:when>
            <xsl:when test="category='lék'">medicine</xsl:when>
            <xsl:when test="category='lékár'">pharmaceutics</xsl:when>
            <xsl:when test="category='let'">aviation</xsl:when>
            <xsl:when test="category='liter'">literary science</xsl:when>
            <xsl:when test="category='log'">logic</xsl:when>
            <xsl:when test="category='marx'">marxism</xsl:when>
            <xsl:when test="category='mat'">mathematics</xsl:when>
            <xsl:when test="category='meteor'">meteorology</xsl:when>
            <xsl:when test="category='miner'">mineralogy</xsl:when>
            <xsl:when test="category='motor'">motorism</xsl:when>
            <xsl:when test="category='mysl'">hunting expression</xsl:when>
            <xsl:when test="category='mytol'">mythology</xsl:when>
            <xsl:when test="category='náb'">religion</xsl:when>
            <xsl:when test="category='nár'">ethnography</xsl:when>
            <xsl:when test="category='obch'">trade</xsl:when>
            <xsl:when test="category='pedag'">pedagogy</xsl:when>
            <xsl:when test="category='peněž'">banking</xsl:when>
            <xsl:when test="category='polit'">politics</xsl:when>
            <xsl:when test="category='polygr'">polygraphy</xsl:when>
            <xsl:when test="category='pošt'">postal operations</xsl:when>
            <xsl:when test="category='potrav'">food industry</xsl:when>
            <xsl:when test="category='práv'">law, jurisprudence</xsl:when>
            <xsl:when test="category='prům'">industry</xsl:when>
            <xsl:when test="category='přír'">nature</xsl:when>
            <xsl:when test="category='psych'">psychology</xsl:when>
            <xsl:when test="category='rybn'">fish farming</xsl:when>
            <xsl:when test="category='řem'">craft</xsl:when>
            <xsl:when test="category='sklář'">glassblowing</xsl:when>
            <xsl:when test="category='soc'">socialism</xsl:when>
            <xsl:when test="category='sociol'">sociology</xsl:when>
            <xsl:when test="category='stat'">statistics</xsl:when>
            <xsl:when test="category='stav'">civil engineering</xsl:when>
            <xsl:when test="category='škol'">education</xsl:when>
            <xsl:when test="category='tech'">technology</xsl:when>
            <xsl:when test="category='těl'">physical education</xsl:when>
            <xsl:when test="category='text'">textile industry</xsl:when>
            <xsl:when test="category='úč'">accountancy</xsl:when>
            <xsl:when test="category='úř'">official expression</xsl:when>
            <xsl:when test="category='veř spr'">public administration</xsl:when>
            <xsl:when test="category='vet'">veterinary</xsl:when>
            <xsl:when test="category='voj'">military</xsl:when>
            <xsl:when test="category='výp tech'">computer technology</xsl:when>
            <xsl:when test="category='výr'">production</xsl:when>
            <xsl:when test="category='výtv'">design</xsl:when>
            <xsl:when test="category='zahr'">horticulture</xsl:when>
            <xsl:when test="category='zbož'">merchandise</xsl:when>
            <xsl:when test="category='zeměd'">agriculture</xsl:when>
            <xsl:when test="category='zeměp'">geography</xsl:when>
            <xsl:when test="category='zool'">zoology</xsl:when>
            <xsl:when test="category='cirkev'">religious expression</xsl:when>
          </xsl:choose>
        </span><br />
      </xsl:if>
      <xsl:if test="@style_region != '' or @style_kategorie != '' or @style_generace != ''">
        <br/>
      </xsl:if>
      <xsl:if test="@style_region != ''">
          <xsl:call-template name="split_region">
            <xsl:with-param name="text" select="@style_region"/>
          </xsl:call-template>      
        </xsl:if>      
        <xsl:if test="@style_kategorie != ''">, 
          <xsl:value-of select="@style_kategorie"/>      
        </xsl:if>      
        <xsl:if test="@style_generace != ''">, 
          <xsl:call-template name="split_generace">
            <xsl:with-param name="text" select="@style_generace"/>
          </xsl:call-template>      
        </xsl:if>           
        <div class="vyklad">   
      <span class="videoblock">
      <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_lingvist')">
        <span class="set-status"> 
          <xsl:call-template name="status_publish">
            <xsl:with-param name="status" select="status"/>
            <xsl:with-param name="type">meaning</xsl:with-param>
            <xsl:with-param name="element" select="."/>
          </xsl:call-template>
        </span>
        </xsl:if>                  
        <xsl:if test="not(text//file)">
          <br/>
          <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_lingvist')">  
          <img src="/media/img/emptyvideo.jpg" width="285px"/>
        </xsl:if>
          </xsl:if>   
        <xsl:for-each select="text//file[@media_id!='']">
          <xsl:variable name="mid" select="@media_id"/>
          <xsl:apply-templates select="//media/file[id=$mid or @id=$mid][1]" mode="inline">
            <xsl:with-param name="media_id">
              <xsl:value-of select="$mid"/>
            </xsl:with-param>
          </xsl:apply-templates>
        </xsl:for-each>
        <xsl:if test="text!=''">
          <xsl:value-of select="text"/>
        </xsl:if>
      </span>
        </div>  
      <div style="clear:both"/>
      <xsl:if test="usages/usage[@id]!=''">   
        <div class="usages">
          <div class="czj-lemma-h2">Examples of use</div>
            <xsl:apply-templates select="usages/usage[(status!='hidden' or ($perm!='ro' or $skupina_test='true'))]"/>        
        </div>
      </xsl:if>

        <xsl:if test="relation[@type!='translation' and (($perm!='ro' or $skupina_test='true') or (@status!='hidden' and  (@auto_complete='1' or @completeness='2' or @completeness='100')))] ">        
        <div class="synanto">
          <div class="cleaner"></div>
          <div class="czj-lemma-h2">Relations
          </div>
          <div class="czj-relation-wrapper">
            <xsl:apply-templates select="relation[(@type='hyperonym' or @type='hyponym' or @type='synonym' or @type='antonym' or @type='synonym_strategie') and (@status!='hidden' or $perm!='ro')]"/>
          </div>
        </div>
      </xsl:if>
      <div style="clear:both"/>  
    
        <xsl:if test="relation[@type='translation' and ((@status!='hidden') or ($perm!='ro' or $skupina_test='true'))] and not(is_translation_unknown='1')"> 
        <span class="relations">
         <div class="cleaner">
         </div>
          <div class="czj-lemma-h2">Translation
          </div>
          <div class="czj-relation-wrapper">
               <p><xsl:apply-templates select="relation[@type='translation' and ((@status!='hidden') or ($perm!='ro' or $skupina_test='true')) and (@target='cs' or @target='en')]">
                <xsl:sort data-type="number" order="ascending" select="((@target='cs')*1) + ((@target='en')*2)"/>
              </xsl:apply-templates></p>
              <xsl:apply-templates select="relation[@type='translation' and ((@status!='hidden') or ($perm!='ro' or $skupina_test='true')) and not(@target='cs' or @target='en')]">
                <xsl:sort data-type="number" order="ascending" select="((@target='czj')*1) + ((@target='is')*2) + ((@target='asl')*3)"/>
                <xsl:sort data-type="number" order="descending" select="((@lemma_region='cr')*3) + ((@lemma_region='cechy')*2) +((@lemma_region='morava')*1)"/>
              </xsl:apply-templates> 
        </div>
        </span>
      </xsl:if>
             <br />
             <div style="clear:both"></div>
    </div>
  </div>
</xsl:template>

  <xsl:template match="relation[@type='translation' and not(@target='cs' or @target='en')]">
    <xsl:if test="((@status!='hidden' or name(..)='usage' and ../status='published') and @auto_complete='1') or $perm!='ro'">     <!-- doplnit do anglické verze a inline -->
    <xsl:variable name="lemma"><xsl:value-of select="@lemma_id"/></xsl:variable>
    <xsl:variable name="rtype"><xsl:value-of select="@type"/></xsl:variable>    
    <xsl:if test="count(preceding-sibling::relation[@lemma_id=$lemma and @type=$rtype])=0">

    <div class="czj-relation-item dict-{@target}">
    <div class="czj-relation relationvideo">
      <div class="czj-lemma-preview-sm">
        <div class="czj-lemma-preview-sm-header dict-{@target}">

          <div class="alignleft" style="white-space: nowrap; overflow: hidden; width: 160px;">
           <xsl:choose>
              <xsl:when test="@target='czj'">ČZJ </xsl:when>          
              <xsl:when test="@target='is'">IS </xsl:when>
              <xsl:when test="@target='asl'">ASL </xsl:when>
              <xsl:otherwise>?:</xsl:otherwise>
            </xsl:choose>
          
          <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_cjlingvist')">
              <xsl:if test="@meaning_nr='' or @meaning_count = 1">
                <span class="set-status">
                  <xsl:call-template name="status_publish">
                    <xsl:with-param name="status" select="@status"/>
                    <xsl:with-param name="type">relation</xsl:with-param>
                    <xsl:with-param name="element" select="."/>
                  </xsl:call-template>
                </span>
              </xsl:if>
              <!--<br/>-->
              </xsl:if><xsl:if test="@meaning_nr!='' and @meaning_count > 1">meaning <xsl:value-of select="@meaning_nr"/>
                <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_cjlingvist')">
                  <span class="set-status">
                    <xsl:call-template name="status_publish">
                      <xsl:with-param name="status" select="@status"/>
                      <xsl:with-param name="type">relation</xsl:with-param>
                      <xsl:with-param name="element" select="."/>
                    </xsl:call-template>
                  </span>
                </xsl:if>
              <xsl:if test="count(following-sibling::relation[@lemma_id=$lemma and @type=$rtype]) &gt; 0">
              <xsl:for-each select="following-sibling::relation[@lemma_id=$lemma and @type=$rtype]">
              <xsl:sort select="@meaning_nr"/>
              <xsl:text>, </xsl:text><xsl:value-of select="@meaning_nr"/>
                  <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_cjlingvist')">
                    <span class="set-status">
                      <xsl:call-template name="status_publish">
                        <xsl:with-param name="status" select="@status"/>
                        <xsl:with-param name="type">relation</xsl:with-param>
                        <xsl:with-param name="element" select="."/>
                      </xsl:call-template>
                    </span>
                  </xsl:if>
                </xsl:for-each>
              </xsl:if>
          </xsl:if></div>
          <div class="alignright">
            <a href="/{@target}?action=search&amp;getdoc={@lemma_id}&amp;lang=en">
              <!--<xsl:value-of select="@lemma_id"/>-->
              <img src="/media/img/slovnik-open_w.png" height="20"/>
              <xsl:if test="@type='translation_colloc'">
                <xsl:value-of select="translation"/>
              </xsl:if>
            </a>
          </div> 
        </div> 
        <xsl:apply-templates select="file" mode="rel">
          <xsl:with-param name="target"><xsl:value-of select="@target"/></xsl:with-param>
        </xsl:apply-templates>
        <div class="czj-lemma-preview-sm-sw">
        <span class="sw transsw" style="vertical-align: top; display: inline-block; height: 96px;">  
          <xsl:apply-templates select="swmix/sw" mode="rel"/>
        </span>
        </div>
      </div>
     </div>
     </div> 
      <xsl:if test="@type='translation_colloc'">
        <br/>
      </xsl:if>
    </xsl:if>
    </xsl:if>
  </xsl:template>

  <xsl:template match="relation[@type='translation' and (@target='cs' or @target='en')]">
    <xsl:variable name="lemma">
      <xsl:value-of select="@lemma_id"/>
    </xsl:variable>

    <span class="relation-entry dict-{@target}">        
      <xsl:if test="@title_only!='true'">
        <xsl:if test="count(preceding-sibling::relation[@lemma_id=$lemma and @type='translation'])=0">          
          <xsl:if test="@auto_complete='1' or ($perm!='ro' or $skupina_test='true')">
            <span style="color: gray">        
              <xsl:choose>
                <xsl:when test="@target='cs'">CZ: </xsl:when>          
                <xsl:when test="@target='en'">EN: </xsl:when>              
                <xsl:otherwise>?</xsl:otherwise>
              </xsl:choose>
            </span>
            <a href="/{@target}?action=search&amp;getdoc={@lemma_id}&amp;lang=en">
              <xsl:value-of select="title"/></a>                           
          </xsl:if>          
          <xsl:if test="@auto_complete!='1' and $perm='ro' and $skupina_test!='true'">
            <span style="color: gray">        
              <xsl:choose>
                 <xsl:when test="@target='cs'">CZ: </xsl:when>          
                 <xsl:when test="@target='en'">EN: </xsl:when>              
                <xsl:otherwise>?</xsl:otherwise>
              </xsl:choose>
            </span>
          <xsl:value-of select="title"/>
       </xsl:if>                                   
          <xsl:if test="(@meaning_nr!='' and @meaning_count > 1) and (@auto_complete='1' or ($perm!='ro' or $skupina_test='true'))"> (ve významu


            <xsl:value-of select="@meaning_nr"/>              
            <!--schvalování-->              
            <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_lingvist')">                
              <span class="set-status">                    
                <xsl:call-template name="status_publish">                    
                  <xsl:with-param name="status" select="@status"/>                    
                  <xsl:with-param name="type">relation</xsl:with-param>                    
                  <xsl:with-param name="element" select="."/>                  
                </xsl:call-template>                
              </span>              
            </xsl:if><!--konec schvalování-->                            
            <xsl:if test="count(following-sibling::relation[@lemma_id=$lemma and @type='translation']) &gt; 0">              
              <xsl:for-each select="following-sibling::relation[@lemma_id=$lemma and @type='translation']">  
                <xsl:sort select="@meaning_nr" data-type="number"/>              
                <xsl:text>, </xsl:text>
                <xsl:value-of select="@meaning_nr"/>                
                <!--schvalování-->                
                <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_lingvist')">                  
                  <span class="set-status">                      
                    <xsl:call-template name="status_publish">                      
                      <xsl:with-param name="status" select="@status"/>                      
                      <xsl:with-param name="type">relation</xsl:with-param>                      
                      <xsl:with-param name="element" select="."/>                    
                    </xsl:call-template>                  
                  </span>                
                </xsl:if>
                <!--konec schvalování-->                
              </xsl:for-each>            
            </xsl:if>)           
          </xsl:if>          
          <xsl:if test="@meaning_nr='' or @meaning_count = 1">            
            <!--schvalování-->            
            <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_lingvist')">              
              <span class="set-status">                  
                <xsl:call-template name="status_publish">                  
                  <xsl:with-param name="status" select="@status"/>                  
                  <xsl:with-param name="type">relation</xsl:with-param>                  
                  <xsl:with-param name="element" select="."/>                
                </xsl:call-template>              
              </span>            
            </xsl:if><!--konec schvalování-->            
          </xsl:if>          

          <!--<xsl:if test="position()!=last()">, </xsl:if>-->          
          <xsl:if test="position()!=last()">, </xsl:if>        
        </xsl:if>      
      </xsl:if>      
      <xsl:if test="@title_only='true'">
        <span style="color: gray">        
          <xsl:choose>
              <xsl:when test="@target='cs'">CZ: </xsl:when>          
              <xsl:when test="@target='en'">EN: </xsl:when>              
              <xsl:otherwise>?</xsl:otherwise>
          </xsl:choose>
        </span>        
        <xsl:value-of select="title"/>
                 
        <!--schvalování-->    
        <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_lingvist')">        
          <span class="set-status">            
            <xsl:call-template name="status_publish">            
              <xsl:with-param name="status" select="@status"/>            
              <xsl:with-param name="type">relation</xsl:with-param>            
              <xsl:with-param name="element" select="."/>          
            </xsl:call-template>        
          </span>      
        </xsl:if><!--konec schvalování-->                  

        <xsl:if test="position()!=last()">, </xsl:if>      
      </xsl:if>    
    </span>  
  </xsl:template>

  <xsl:template match="relation[@type!='translation']">
    <div class="czj-relation-item">      
      <div class="czj-relation relationvideo">        
        <div class="czj-lemma-preview-sm">          
          <div class="czj-lemma-preview-sm-header">            
            <div class="alignleft" style="white-space: nowrap; overflow: hidden; width: 165px;">                   
              <xsl:choose>          
                <xsl:when test="@type='synonym'">synonym
                </xsl:when>          
                <xsl:when test="@type='synonym_strategie'">syn. strategy
                </xsl:when>          
                <xsl:when test="@type='antonym'">antony
                </xsl:when>          
                <xsl:when test="@type='hyponym'">hyponym
                </xsl:when>          
                <xsl:when test="@type='hyperonym'">hyperonym
                </xsl:when>          
                <xsl:when test="@type='revcolloc'">
                </xsl:when>          
                <xsl:otherwise>
                  <xsl:value-of select="@type"/>
                </xsl:otherwise>            
              </xsl:choose>        
              <xsl:if test="@meaning_nr!='' and @meaning_count > 1"> (ve výz. 
                <xsl:value-of select="@meaning_nr"/>)
              </xsl:if>
              <!--schvalování-->        
              <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_lingvist')">          
                <span class="set-status">               
                  <xsl:call-template name="status_publish">              
                    <xsl:with-param name="status" select="@status"/>              
                    <xsl:with-param name="type">relation</xsl:with-param>              
                    <xsl:with-param name="element" select="."/>            
                  </xsl:call-template>          
                </span>        
              </xsl:if>
              <!--schvalování konec-->          
            </div>          
            <div class="alignright">            
              <a href="/is?action=search&amp;getdoc={@lemma_id}&amp;lang=en" >              
                <img src="/media/img/slovnik-open_w.png" height="20"/>              
                <!--<xsl:value-of select="@lemma_id"/>-->            </a>          
            </div>          
          </div>         
        </div>    
        <div style="clear:both">
    </div>  
      <xsl:if test="not(file)">
        <img src="/media/img/emptyvideo.jpg" height="96" width="120"/>
      </xsl:if>
    
    <xsl:apply-templates select="file" mode="rel"/>
    <div class="czj-lemma-preview-sm-sw">
    <span class="sw transsw" style="vertical-align: top; display: inline-block; height: 96px;">
    <!--<span class="sw transsw" style="vertical-align:top; display:inline-block;">-->
      <xsl:apply-templates select="swmix/sw" mode="rel"/>
    </span>
   </div>
    <br/>
    <span class="" style="vertical-align:top; display:none;">
      <xsl:value-of select="translation"/>
    </span>
  </div>
  
  </div>
</xsl:template>
  <xsl:template match="usage"><!--příklady užití objekt-->  
    <xsl:variable name="ppp" select="current()"/>  
    <div class="relation-entry" style="float: left">  
      <!--schvalování-->  
      <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_lingvist')">      
        <span class="set-status">          
          <xsl:call-template name="status_publish">          
            <xsl:with-param name="status" select="status"/>          
            <xsl:with-param name="type">usage</xsl:with-param>          
            <xsl:with-param name="element" select="."/>        
          </xsl:call-template>      
        </span>      
        <br/>    
      </xsl:if><!--schvalování konec-->
      <xsl:if test="not(text//file)">          
            <img src="/media/img/emptyvideo.jpg" height="96" width="120"/>
      </xsl:if>    
      <xsl:for-each select="text//file">               
        <xsl:variable name="mid">
          <xsl:value-of select="@media_id"/>
        </xsl:variable>      
        <xsl:apply-templates select="//media/file[id=$mid or @id=$mid]" mode="usage">        
          <xsl:with-param name="parent_id" select="generate-id($ppp)"/>      
        </xsl:apply-templates>    
      </xsl:for-each>    
      <xsl:if test="text!=''">      
        <xsl:value-of select="text"/>      
      </xsl:if>  
  <xsl:if test="relation">
        <em style="font-size:80%; cursor: pointer;" onclick="$(this).next().toggle();$(this).find('.usagetrans-toggle').toggle();"> <span class="usagetrans-toggle">(zobrazit překlady<img src="/media/img/slovnik-expand.png" height="10" />)</span><span class="usagetrans-toggle" style="display:none;">(skrýt překlady<img src="/media/img/slovnik-collapse.png" height="10" />)</span></em>
        <div class="usage-translation" style="display:none; padding-left: 20px; font-style: italic; font-size:90%">
          <div><strong>Překlady: </strong></div>
          <xsl:for-each select="relation[@target='en' or @target='cs']">
            <div class="dict-@target"><xsl:choose><xsl:when test="@target='en'"><span style="color: grey">EN:</span></xsl:when><xsl:when test="@target='cs'"><span style="color: grey">ČJ:</span></xsl:when></xsl:choose>
              <xsl:value-of select="title"/></div>
          </xsl:for-each>
          <xsl:apply-templates select="relation[@target!='en' and @target!='cs']" mode="usage"/>
          <div style="clear:both"/>
        </div>
      </xsl:if>
    </div>
  </xsl:template>
  <xsl:template match="relation" mode="usage">
    <xsl:param name="variant_pos"/>  
    <xsl:apply-templates select="file" mode="usage_translation">
      <xsl:with-param name="variant_sw" select="swmix"/>
      <!--<xsl:with-param name="variant_pos" select="$variant_pos"/> -->
      <xsl:with-param name="variant_pos" select="position()"/> 
      <xsl:with-param name="usage_target" select="@target"/> 
      <xsl:with-param name="relation_link"><xsl:if test="(@auto_complete='1' or $perm!='ro') and not(contains(@meaning_id,'_us'))">true</xsl:if></xsl:with-param>
    </xsl:apply-templates>  
  </xsl:template>
  <xsl:template match="file" mode="usage_translation">
    <xsl:param name="variant_sw"/>
    <xsl:param name="variant_pos"/>
    <xsl:param name="usage_target"/>
    <xsl:param name="relation_link"/>
    <div class="czj-relation-item dict-czj">    
      <div class="czj-relation relationvideo">      
        <div class="czj-lemma-preview-sm">          
          <div class="czj-lemma-preview-sm-header dict-{$usage_target}"> 
            <div class="alignleft"><xsl:choose><xsl:when test="$usage_target='czj'">ČZJ</xsl:when>
                <xsl:when test="$usage_target='is'">IS</xsl:when>
                <xsl:when test="$usage_target='asl'">ASL</xsl:when></xsl:choose>
            </div>
            <xsl:if test="$relation_link='true'">
              <div class="alignright">
                <a href="/{$usage_target}?action=search&amp;getdoc={../@lemma_id}">
                  <img src="/media/img/slovnik-open_w.png" height="20"/></a>
              </div>
            </xsl:if>
          </div>

        </div>
        <div style="clear:both">
        </div>  
           
      <xsl:if test="substring(location, string-length(location)-3, 4) = '.flv'">
        <div id="flowvideousg{@id}-{../@meaning_id}" data-ratio="0.8" class="flowplayer flowmouse usage-video{$relation_link}" style="background:#777 url(/media/video{$usage_target}/thumb/{location}/thumb.jpg) no-repeat;">
          <video preload="none" loop="loop">      
            <source type="video/flash" src="/media/video{$usage_target}/{location}"/>
          </video>            
        </div><br />
      </xsl:if>
      <xsl:if test="substring(location, string-length(location)-3, 4) = '.mp4'">
        <div id="flowvideousg{@id}-{../@meaning_id}" data-ratio="0.8" class="flowmouse usage-video{$relation_link}" style="background: url(/media/video{$usage_target}/thumb/{location}/thumb.jpg) no-repeat;">
          <video preload="none" width="120px" height="96px" poster="/media/video{$usage_target}/thumb/{location}/thumb.jpg" onmouseover="this.play()" onmouseout="this.pause()">
            <source type="video/mp4" src="/media/video{$usage_target}/{location}"/>
          </video>
        </div>  
      </xsl:if>  
           
      <div class="czj-lemma-preview-sm-sw">
        <xsl:if test="($variant_sw/sw)">
          <xsl:apply-templates select="$variant_sw/sw" mode="rel"/>
        </xsl:if>
      </div>

        <xsl:if test="$relation_link='true'">
           <script type="text/javascript">
             $("#flowvideousg<xsl:value-of select="@id"/>-<xsl:value-of select="../@meaning_id"/>").on("click", function(e) {
               window.location='/<xsl:value-of select="$usage_target"/>?action=search&amp;getdoc=<xsl:value-of select="../@lemma_id"/>';
                 });
             </script>
        </xsl:if>
        <xsl:if test="not($relation_link='true')">
<script type="text/javascript">
  $("#flowvideousg<xsl:value-of select="@id"/>-<xsl:value-of select="../@meaning_id"/>").on("click", function(e) {
          var container = $('<div data-ratio="0.8" style="width:285px;">
            <video preload="none" controls="" width="285px" height="228px" poster="/media/video{$usage_target}/thumb/{location}/thumb.jpg" autoplay="">
            <source type="video/mp4" src="/media/video{$usage_target}/{location}"/>
            </video>
            </div>');
          $.fancybox({
            content: container,
            width: 285,
            height: 228,
            scrolling: 'no',
            autoSize: false
          });
        });
      </script>
    </xsl:if>
      </div>  
    </div>
  </xsl:template>
  <xsl:template match="colloc">
    <xsl:if test="($perm!='ro' or $skupina_test='true') or @auto_complete='1'">
    <div class="czj-relation-item">  
      <span class="czj-relation relationvideo">    
        <div class="czj-lemma-preview-sm">      
          <div class="czj-lemma-preview-sm-header">        
            <div class="alignleft"> 
              <xsl:if test="$perm!='ro' or $skupina_test='true'">
                <span class="set-status">
                  <xsl:value-of select="@lemma_id"/>
                </span> 
              </xsl:if>
            </div>             
            <div class="alignright">
              <a href="/is?action=search&amp;getdoc={@lemma_id}&amp;lang=en">
                <img src="/media/img/slovnik-open_w.png" height="20"/></a>
            </div>        
          </div>    
        </div>         
        <xsl:if test="not(file)">        
          <img src="/media/img/emptyvideo.jpg" height="96" width="120"/>      
        </xsl:if>         
        <xsl:apply-templates select="file" mode="rel"/>    
        <div class="czj-lemma-preview-sm-sw">    
          <span class="sw transsw" style="vertical-align: top; display: inline-block; height: 96px;">    
            <!--<span class="sw transsw" style="vertical-align:top; display:inline-block;">-->      
            <xsl:apply-templates select="swmix/sw" mode="rel"/>    
          </span>
        </div>    
        <br/>
        <span class="" style="vertical-align:top; display:none;">      
          <xsl:value-of select="translation"/>    
        </span>  
      </span>  
    </div>
  </xsl:if>
  </xsl:template>
<xsl:template match="revcolloc">
  <xsl:if test="@auto_complete='1' or $perm!='ro' or $skupina_test='true'">
    <br/>
    <div style="background-color: #D7E1E4;">
    <span class="relation-entry" >
     
      <xsl:apply-templates select="file" mode="colloc"/>
      <span class="sw transsw" style="width:auto;">
      <!--<span class="sw transsw" style="vertical-align:top; display:inline-block;">-->
        <xsl:apply-templates select="swmix/sw" mode="rel"/>
      </span>
      <span class="" style="vertical-align:top; display:inline-block;">
        <!-- <xsl:value-of select="translation"/> -->
      </span>     
       <span style="vertical-align:top; display:inline-block; float: right;">
        <span onclick="open_iframe('{@lemma_id}','is', 'en')" style="cursor:pointer;color:blue;text-decoration:underline">
          <!--<xsl:value-of select="@lemma_id"/>-->
          <img src="/media/img/slovnik-expand.png" height="20"/>
        </span>
      </span> 
      <div style="clear:both; margin-bottom: 5px;">
      </div>
    </span>
    <iframe class="colloc-iframe" id="colloc{@lemma_id}" src="about:blank" style="display:none" onload="iframe_loaded('{@lemma_id}')"></iframe>
    </div>
  </xsl:if> 
</xsl:template>

<xsl:template name="split_misto">
    <xsl:param name="text"/>
    <xsl:variable name="first" select='substring-before($text,";")'/>
    <xsl:variable name='rest' select='substring-after($text,";")'/>
    <xsl:if test='$first'>
      <xsl:choose>
     <xsl:when test="$first='neutral'">neutral space</xsl:when>
        <xsl:when test="$first='hlava'">head</xsl:when>
        <xsl:when test="$first='oblicej'">- face (neutr. space in front of face)</xsl:when>
        <xsl:when test="$first='temeno'">- top of the head (above the head)</xsl:when>
        <xsl:when test="$first='celo'">- forehead</xsl:when>
        <xsl:when test="$first='oci'">- eyes</xsl:when>
        <xsl:when test="$first='nos'">- nose</xsl:when>
        <xsl:when test="$first='usi'">- ears</xsl:when>
        <xsl:when test="$first='tvare'">- cheeks</xsl:when>
        <xsl:when test="$first='usta'">- mouth</xsl:when>
        <xsl:when test="$first='brada'">- chin</xsl:when>
        <xsl:when test="$first='krk'">- neck</xsl:when>
        <xsl:when test="$first='hrud'">upper half of torso (chest)</xsl:when>
        <xsl:when test="$first='paze'">arm</xsl:when>
        <xsl:when test="$first='ruka'">hand</xsl:when>
        <xsl:when test="$first='pas'">lower half of torso (abdomen, waist)</xsl:when>
        <xsl:when test="$first='dolni'">lower body</xsl:when>
      </xsl:choose>
      <xsl:if test="$rest">, </xsl:if>
    </xsl:if>

    <xsl:if test='$rest'>
      <xsl:call-template name='split_misto'>
        <xsl:with-param name='text' select='$rest'/>
      </xsl:call-template>
    </xsl:if>
    <xsl:if test='not($rest)'>
      <xsl:choose>
        <xsl:when test="$text='neutral'">neutral space</xsl:when>
        <xsl:when test="$text='hlava'">head</xsl:when>
        <xsl:when test="$text='oblicej'">- face (neutr. space in front of face)</xsl:when>
        <xsl:when test="$text='temeno'">- top of the head (above the head)</xsl:when>
        <xsl:when test="$text='celo'">- forehead</xsl:when>
        <xsl:when test="$text='oci'">- eyes</xsl:when>
        <xsl:when test="$text='nos'">- nose</xsl:when>
        <xsl:when test="$text='usi'">- ears</xsl:when>
        <xsl:when test="$text='tvare'">- cheeks</xsl:when>
        <xsl:when test="$text='usta'">- mouth</xsl:when>
        <xsl:when test="$text='brada'">- chin</xsl:when>
        <xsl:when test="$text='krk'">- neck</xsl:when>
        <xsl:when test="$text='hrud'">upper half of torso (chest)</xsl:when>
        <xsl:when test="$text='paze'">arm</xsl:when>
        <xsl:when test="$text='ruka'">hand</xsl:when>
        <xsl:when test="$text='pas'">lower half of torso (abdomen, waist)</xsl:when>
        <xsl:when test="$text='dolni'">lower body</xsl:when>
      </xsl:choose>
    </xsl:if>
  </xsl:template>

  <xsl:template name="split_region">
    <xsl:param name="text"/>
    <xsl:variable name="first" select='substring-before($text,";")'/>
    <xsl:variable name='rest' select='substring-after($text,";")'/>
    <xsl:if test='$first'>
      <xsl:choose>
        <xsl:when test="$first='cr'">Entire Czech Republic<img src="/media/img/region/celaCR.png" class="oblast"/></xsl:when>
        <xsl:when test="$first='praha'">Prague and surroundings<img src="/media/img/region/pha.png" class="oblast"/></xsl:when>
        <xsl:when test="$first='brno'">Brno and surroundings<img src="/media/img/region/brno.png" class="oblast"/></xsl:when>
        <xsl:when test="$first='plzen'">Pilsen and surroundings<img src="/media/img/region/plzen.png" class="oblast"/></xsl:when>
        <xsl:when test="$first='vm'">Valašské Meziříčí and surroundings<img src="/media/img/region/valmez.png" class="oblast"/></xsl:when>
        <xsl:when test="$first='hk'">Hradec Králové and surroundings<img src="/media/img/region/hk.png" class="oblast"/></xsl:when>
        <xsl:when test="$first='kr'">Kroměříž and surroundings</xsl:when>
        <xsl:when test="$first='cechy'">Bohemia<img src="/media/img/region/cechy.png" class="oblast"/></xsl:when>
        <xsl:when test="$first='morava'">Moravia<img src="/media/img/region/morava.png" class="oblast"/></xsl:when>
        <xsl:when test="$first='jih'">Jihlava and surroundings<img src="/media/img/region/jihlava.png" class="oblast"/></xsl:when>
        <xsl:when test="$first='zl'">Zlín and surroundings<img src="/media/img/region/zlin.png" class="oblast"/></xsl:when>
        <xsl:when test="$first='cb'">České Budějovice and surroundings<img src="/media/img/region/cb.png" class="oblast"/></xsl:when>
        <xsl:when test="$first='ot'">Ostrava and surroundings<img src="/media/img/region/ostrava.png" class="oblast"/></xsl:when>
        <xsl:when test="$first='ol'">Olomouc and surroundings<img src="/media/img/region/ol.png" class="oblast"/></xsl:when>
        <xsl:when test="$first='ul'">Ústí nad Labem and surroundings<img src="/media/img/region/usti.png" class="oblast"/></xsl:when>
        <xsl:when test="$first='lib'">Liberec and surroundings<img src="/media/img/region/lbc.png" class="oblast"/></xsl:when>
        <xsl:when test="$first='slovensko'">Slovakia<img src="/media/img/region/sk.png" class="oblast"/></xsl:when>
      </xsl:choose>
      <xsl:if test="$rest">, </xsl:if>
    </xsl:if>                    
    <xsl:if test='$rest'>
      <xsl:call-template name='split_region'>
        <xsl:with-param name='text' select='$rest'/>
      </xsl:call-template>
    </xsl:if>
    <xsl:if test='not($rest)'>
      <xsl:choose>
        <xsl:when test="$text='cr'">Entire Czech Republic<img src="/media/img/region/celaCR.png" class="oblast"/></xsl:when>
        <xsl:when test="$text='praha'">Prague and surroundings<img src="/media/img/region/pha.png" class="oblast"/></xsl:when>
        <xsl:when test="$text='brno'">Brno and surroundings<img src="/media/img/region/brno.png" class="oblast"/></xsl:when>
        <xsl:when test="$text='plzen'">Pilsen and surroundings<img src="/media/img/region/plzen.png" class="oblast"/></xsl:when>
        <xsl:when test="$text='vm'">Valašské Meziříčí and surroundings<img src="/media/img/region/valmez.png" class="oblast"/></xsl:when>
        <xsl:when test="$text='hk'">Hradec Králové and surroundings<img src="/media/img/region/hk.png" class="oblast"/></xsl:when>
        <xsl:when test="$text='kr'">Kroměříž and surroundings</xsl:when>
        <xsl:when test="$text='cechy'">Bohemia<img src="/media/img/region/cechy.png" class="oblast"/></xsl:when>
        <xsl:when test="$text='morava'">Moravia<img src="/media/img/region/morava.png" class="oblast"/></xsl:when>
        <xsl:when test="$text='jih'">Jihlava and surroundings<img src="/media/img/region/jihlava.png" class="oblast"/></xsl:when>
        <xsl:when test="$text='zl'">Zlín and surroundings<img src="/media/img/region/zlin.png" class="oblast"/></xsl:when>
        <xsl:when test="$text='cb'">České Budějovice and surroundings<img src="/media/img/region/cb.png" class="oblast"/></xsl:when>
        <xsl:when test="$text='ot'">Ostrava and surroundings<img src="/media/img/region/ostrava.png" class="oblast"/></xsl:when>
        <xsl:when test="$text='ol'">Olomouc and surroundings<img src="/media/img/region/ol.png" class="oblast"/></xsl:when>
        <xsl:when test="$text='ul'">Ústí nad Labem and surroundings<img src="/media/img/region/usti.png" class="oblast"/></xsl:when>
        <xsl:when test="$text='lib'">Liberec and surroundings<img src="/media/img/region/lbc.png" class="oblast"/></xsl:when>
        <xsl:when test="$text='slovensko'">Slovakia<img src="/media/img/region/sk.png" class="oblast"/></xsl:when>
      </xsl:choose>
    </xsl:if>
  </xsl:template>
  <xsl:template name="split_generace">
    <xsl:param name="text"/>
    <xsl:variable name="first" select='substring-before($text,";")'/>
    <xsl:variable name='rest' select='substring-after($text,";")'/>
    <xsl:if test='$first'>
      <xsl:choose>
        <xsl:when test="$first='mlada'">youth</xsl:when>
        <xsl:when test="$first='stredni'">middle generation</xsl:when>
        <xsl:when test="$first='starsi'">elderly people</xsl:when>
        <xsl:when test="$first='deti'">children (children's sign)</xsl:when>
      </xsl:choose>
      <xsl:if test="$rest">, </xsl:if>
    </xsl:if>

    <xsl:if test='$rest'>
      <xsl:call-template name='split_generace'>
        <xsl:with-param name='text' select='$rest'/>
      </xsl:call-template>
    </xsl:if>
    <xsl:if test='not($rest)'>
      <xsl:choose>
       <xsl:when test="$text='mlada'">youth</xsl:when>
        <xsl:when test="$text='stredni'">middle generation</xsl:when>
        <xsl:when test="$text='starsi'">elderly people</xsl:when>
        <xsl:when test="$text='deti'">children (children's sign)</xsl:when>
      </xsl:choose>
    </xsl:if>
  </xsl:template>
  <xsl:template name="split_skupina2">
    <xsl:param name="text"/>
    <xsl:variable name="first" select='substring-before($text,";")'/>
    <xsl:variable name='rest' select='substring-after($text,";")'/>
    <xsl:if test='$first'>
      <xsl:choose>
       <xsl:when test="$first='intr'">intransitive</xsl:when>
        <xsl:when test="$first='tran'">transitive</xsl:when>
        <xsl:when test="$first='pohyb'">expressing movement of object in space</xsl:when>
        <xsl:when test="$first='misto'">identifying place</xsl:when>
        <xsl:when test="$first='prost'">expressing means</xsl:when>
        <xsl:when test="$first='subj'">indicating subject</xsl:when>
        <xsl:when test="$first='obj'">indicating object</xsl:when>
        <xsl:when test="$first='reci'">reciprocal</xsl:when>
        <xsl:when test="$first='polo'">half-indicating</xsl:when>
        <xsl:when test="$first='lok'">locative</xsl:when>
        <xsl:when test="$first='pok'">bent</xsl:when>
        <xsl:when test="$first='dej'">of process</xsl:when>
        <xsl:when test="$first='fre'">of frequency</xsl:when>
        <xsl:when test="$first='mir'">of rate</xsl:when>
        <xsl:when test="$first='obr'">reversed</xsl:when>
        <xsl:when test="$first='iko'">iconic</xsl:when>
        <xsl:when test="$first='spec'">specific</xsl:when>
      </xsl:choose>
      <xsl:if test="$rest">, </xsl:if>
    </xsl:if>

    <xsl:if test='$rest'>
      <xsl:call-template name='split_skupina2'>
        <xsl:with-param name='text' select='$rest'/>
      </xsl:call-template>
    </xsl:if>
    <xsl:if test='not($rest)'>
      <xsl:choose>
        <xsl:when test="$text='intr'">intransitive</xsl:when>
        <xsl:when test="$text='tran'">transitive</xsl:when>
        <xsl:when test="$text='pohyb'">expressing movement of object in space</xsl:when>
        <xsl:when test="$text='misto'">identifying place</xsl:when>
        <xsl:when test="$text='prost'">expressing means</xsl:when>
        <xsl:when test="$text='subj'">indicating subject</xsl:when>
        <xsl:when test="$text='obj'">indicating object</xsl:when>
        <xsl:when test="$text='reci'">reciprocal</xsl:when>
        <xsl:when test="$text='polo'">half-indicating</xsl:when>
        <xsl:when test="$text='lok'">locative</xsl:when>
        <xsl:when test="$text='pok'">bent</xsl:when>
        <xsl:when test="$text='dej'">of process</xsl:when>
        <xsl:when test="$text='fre'">of frequency</xsl:when>
        <xsl:when test="$text='mir'">of rate</xsl:when>
        <xsl:when test="$text='obr'">reversed</xsl:when>
        <xsl:when test="$text='iko'">iconic</xsl:when>
        <xsl:when test="$text='spec'">specific</xsl:when>
      </xsl:choose>
    </xsl:if>
  </xsl:template>
  <xsl:template name="split_skupina3">
    <xsl:param name="text"/>
    <xsl:variable name="first" select='substring-before($text,";")'/>
    <xsl:variable name='rest' select='substring-after($text,";")'/>
    <xsl:if test='$first'>
      <xsl:choose>
      <xsl:when test="$first='redup'">by reduplication </xsl:when>
        <xsl:when test="$first='prof'">by adding proform (pronoun, classificators)</xsl:when>
        <xsl:when test="$first='kvan'">by adding quantificator</xsl:when>
        <xsl:when test="$first='ink'"><!--by incorporating number morphemes-->no plural form</xsl:when>
        <xsl:when test="$first='plur'">by adding plural specificator</xsl:when>
        <xsl:when test="$first='redo'">reduplication expresses process repetition</xsl:when>
        <xsl:when test="$first='redt'">reduplication expresses process duration</xsl:when>
        <xsl:when test="$first='okol'">verb expresses process circumstances</xsl:when>
        <xsl:when test="$first='rt'">quick reduplication expresses process duration</xsl:when>
        <xsl:when test="$first='rp'">quick reduplication expresses regularity</xsl:when>
        <xsl:when test="$first='pp'">slow reduplication expresses process continuation</xsl:when>
        <xsl:when test="$first='po'">slow reduplication expresses process repetition</xsl:when>
        <xsl:when test="$first='netvori'">no plural form</xsl:when>
        <xsl:when test="$first='redupklas'">by reduplicating classificator / speficicator</xsl:when>
      </xsl:choose>
      <xsl:if test="$rest">, </xsl:if>
    </xsl:if>

    <xsl:if test='$rest'>
      <xsl:call-template name='split_skupina3'>
        <xsl:with-param name='text' select='$rest'/>
      </xsl:call-template>
    </xsl:if>
    <xsl:if test='not($rest)'>
      <xsl:choose>
        <xsl:when test="$text='redup'">by reduplication </xsl:when>
        <xsl:when test="$text='prof'">by adding proform (pronoun, classificators)</xsl:when>
        <xsl:when test="$text='kvan'">by adding quantificator</xsl:when>
        <xsl:when test="$text='ink'"><!--by incorporating number morphemes-->no plural form</xsl:when>
        <xsl:when test="$text='plur'">by adding plural specificator</xsl:when>
        <xsl:when test="$text='redo'">reduplication expresses process repetition</xsl:when>
        <xsl:when test="$text='redt'">reduplication expresses process duration</xsl:when>
        <xsl:when test="$text='okol'">verb expresses process circumstances</xsl:when>
        <xsl:when test="$text='rt'">quick reduplication expresses process duration</xsl:when>
        <xsl:when test="$text='rp'">quick reduplication expresses regularity</xsl:when>
        <xsl:when test="$text='pp'">slow reduplication expresses process continuation</xsl:when>
        <xsl:when test="$text='po'">slow reduplication expresses process repetition</xsl:when>
        <xsl:when test="$text='netvori'">no plural form</xsl:when>
        <xsl:when test="$text='redupklas'">by reduplicating classificator / speficicator</xsl:when>
      </xsl:choose>
    </xsl:if>
  </xsl:template>
  <xsl:template name="status_publish">
    <xsl:param name="status"/>
    <xsl:param name="type"/>
    <xsl:param name="element"/>
    <xsl:if test="$status='published'"><img src="/editor/img/checked.png" title="approved" /></xsl:if>
    <xsl:if test="$status!='published'"><!--hidden-->
    <xsl:text>
    </xsl:text>
      <xsl:choose>
        <xsl:when test="$type='relation'">
          <input type="button" class="button-small" onclick="publish_relation('{$dictcode}', '{/entry/@id}', '{$element/../@id}', '{$element/@meaning_id}', '{$element/@type}', this)" value="approve"/>

        </xsl:when>
        <xsl:when test="$type='usage'">
          <input type="button" class="button-small" onclick="publish_usage('{$dictcode}', '{/entry/@id}', '{$element/../../@id}', '{$element/@id}', this)" value="approve"/>

        </xsl:when>
        <xsl:when test="$type='meaning'">
          <input type="button" class="button-small" onclick="publish_meaning('{$dictcode}', '{/entry/@id}', '{$element/@id}', this)" value="approve"/>

        </xsl:when>
        <xsl:when test="$type='gram' or $type='style' or $type='sw' or $type='hns' or $type='video_front' or $type='video_side' or $type='entry'">
          <input type="button" class="button-small" onclick="publish('{$dictcode}', '{/entry/@id}', '{$type}', this)" value="approve"/>
        </xsl:when>
      </xsl:choose>
    </xsl:if>
  </xsl:template>
</xsl:stylesheet>
