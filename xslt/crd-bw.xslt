<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="utf-8" method="xml"
  doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
  doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
  />

<xsl:template match="entry">

  <html>
     <head>
      <title>CRD: <xsl:value-of select="hw"/></title>
      <style type="text/css">
      body {
      background: #ffffff;
      color: #000000;
      }
      .default_par_style {font-size: 14pt; font-weight: bold; font-style: normal; background: white; text-indent: 0px; margin-left: 0px; margin-top: 10px; text-align: justify;}

      .default_inline_style {font-size: 14pt; font-family: Verdana; font-weight: normal; font-style: normal;  background: white; text-align: justify;}

      .headword {font-size: 18pt; font-family: Times New Roman; font-weight: bold; font-style: normal;  background: white; text-align: justify;}

      .morphology {font-size: 14pt; font-family: Times New Roman; font-weight: normal; font-style: normal;  background: white; text-align: justify;}

      .mark {font-size: 14pt; font-family: Times New Roman; font-weight: normal; font-style: italic;  background: white; text-align: justify;}

      .meaning {font-size: 14pt; font-family: Times New Roman; font-weight: normal; font-style: normal; background: white; text-align: justify;}

      .free_text {font-size: 14pt; font-family: Times New Roman; font-weight: normal; font-style: normal; background: white; text-align: justify;}

      .example {font-size: 14pt; font-family: Times New Roman; font-weight: normal; font-style: italic; background: white; text-align: justify;}

      .meaning_number {font-size: 16pt; font-family: Times New Roman; font-weight: bold; font-style: normal; background: white; text-align: justify;}

      .encyclopaedic {font-size: 14pt; font-family: Times New Roman; font-weight: normal; font-style: italic; background: white; text-align: justify;}

      .example_explanation {font-size: 12pt; font-family: Times New Roman; font-weight: normal; font-style: normal; background: white; text-align: justify;}

      .meaning_par {font-size: 14pt; font-family: Times New Roman; font-weight: normal; font-style: normal; background: white; text-indent: 4px; margin-left: 0px; margin-top: 10px; text-align: justify;}

      .pronunciation_par {font-size: 14pt; font-family: Times New Roman; font-weight: normal; font-style: normal; background: white; text-indent: 14px; margin-left: 0px; margin-top: 10px; text-align: justify;}

      .etymology_par {font-size: 14pt; font-family: Times New Roman; font-weight: normal; font-style: normal; background: white; text-indent: 14px; margin-left: 0px; margin-top: 10px; text-align: justify;}

      .phrase_par {font-size: 16pt; font-family: Times New Roman; font-weight: bold; font-style: normal; background: white; text-indent: 0px; margin-left: 60px; margin-top: 10px; text-align: justify;}

      .phrase_expl_par {font-size: 12pt; font-family: Times New Roman; font-weight: normal; font-style: normal; color: black; background: white; text-indent: 0px; margin-left: 60px; margin-top: 10px; text-align: justify;}

      .additional_meaning {font-size: 12pt; font-family: Times New Roman; font-weight: normal; font-style: normal; background: white; text-indent: 0px; margin-left: 0px; margin-top: 10px; text-align: justify;}

      .additional_phrase_meaning {font-size: 12pt; font-family: Times New Roman; font-weight: normal; font-style: normal; background: white; text-indent: 0px; margin-left: 60px; margin-top: 10px; text-align: justify;}

      .headword_syntax {font-size: 14pt; font-family: Times New Roman; font-weight: normal; font-style: normal; background: white; text-indent: 0px; margin-left: 0px; margin-top: 10px; text-align: justify;}

      .phrase {font-size: 14pt; font-family: Times New Roman; font-weight: bold; font-style: normal; background: white; text-align: justify;}

      .syntax_example {font-size: 14pt; font-family: Times New Roman; font-weight: normal; font-style: normal; background: white; text-align: justify;}

      .meaning_syntax {font-size: 14pt; font-family: Times New Roman; font-weight: normal; font-style: normal; background: white; text-align: justify;}

      .ettymology_inline {font-size: 14pt; font-family: Times New Roman; font-weight: normal; font-style: normal; background: white; text-align: justify;}

      .synonyms {font-size: 14pt; font-family: Times New Roman; font-weight: normal; font-style: normal; background: white; text-indent: 0px; margin-left: 0px; margin-top: 0px; text-align: justify;}

      .orthography {font-size: 14pt; font-family: Times New Roman; font-weight: normal; font-style: normal; background: white; text-align: justify;}

      .orthography_headword {font-size: 14pt; font-family: MS Sans Serif; font-weight: normal; font-style: normal; background: white; text-indent: 0px; margin-left: 0px; margin-top: 0px; text-align: justify;}


      </style>
     </head>
     <body>
       <xsl:apply-templates/>
       <hr />
       <p><small>Complex Russian Dictionary</small></p>
     </body>
  </html>
  </xsl:template>
  
  <xsl:template match="hw|var">
  </xsl:template>

  <xsl:template match="*">
    <xsl:element name="{name()}">
      <xsl:for-each select="./@*">
        <xsl:attribute name="{name()}">
          <xsl:value-of select="."/>
        </xsl:attribute>
      </xsl:for-each>
      <xsl:apply-templates/>
    </xsl:element>
  </xsl:template>

</xsl:stylesheet>

