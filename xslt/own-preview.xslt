<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="utf-8"/>

  <xsl:template match="SubClassOf">
    <p><span class="BLUE">SubClassOf: </span>
      <xsl:for-each select="OWLClass">
        <br/><a target="_blank">
          <xsl:attribute name="href">/own?action=get_class&amp;transform=preview&amp;uri=<xsl:call-template name="replace">
          <xsl:with-param name="string" select="string(@URI)"/>
          <xsl:with-param name="pattern" select="'#'"/>
          <xsl:with-param name="replacement" select="'%23'"/>
        </xsl:call-template></xsl:attribute>
          <xsl:value-of select="@URI"/></a>
      </xsl:for-each>
    </p>
  </xsl:template>

  <xsl:template match="EntityAnnotation">
    <p><span class="BLUE">EntityAnnotation: </span>
      <br/><a target="_blank">
          <xsl:attribute name="href">/own?action=get_class&amp;transform=preview&amp;uri=<xsl:call-template name="replace">
              <xsl:with-param name="string" select="string(OWLClass/@URI)"/>
          <xsl:with-param name="pattern" select="'#'"/>
          <xsl:with-param name="replacement" select="'%23'"/>
        </xsl:call-template></xsl:attribute>
        <xsl:value-of select="OWLClass/@URI"/></a>
      <br/><i><xsl:value-of select="Annotation/Constant"/></i>
    </p>
  </xsl:template>


  <xsl:template name="replace">
    <xsl:param name="string" select="''"/>
    <xsl:param name="pattern" select="''"/>
    <xsl:param name="replacement" select="''"/>
    <xsl:choose>
      <xsl:when test="$pattern != '' and $string != '' and contains($string, $pattern)">
        <xsl:value-of select="substring-before($string, $pattern)"/>
        <!--
        Use "xsl:copy-of" instead of "xsl:value-of" so that users
        may substitute nodes as well as strings for $replacement.
        -->
        <xsl:copy-of select="$replacement"/>
        <xsl:call-template name="replace">
          <xsl:with-param name="string" select="substring-after($string, $pattern)"/>
          <xsl:with-param name="pattern" select="$pattern"/>
          <xsl:with-param name="replacement" select="$replacement"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$string"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

</xsl:stylesheet>

