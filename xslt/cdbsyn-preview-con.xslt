<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="utf-8"/>

<xsl:param name="dictionary"/>
<xsl:param name="servername"/>
<xsl:param name="default"/>
<xsl:param name="dictcode"/>

<xsl:template match="cdb_synset">
    <body style="background-color: white;" id="body">
      <span class="BLUE">C_SY_ID: </span>
      <span class="RED"><xsl:value-of select="@c_sy_id"/></span>
      <xsl:text> </xsl:text>
      <br/>
      <span class="BLUE">Synonyms: </span>
      <xsl:for-each select="synonyms/synonym">
        <xsl:if test="$default='1'">
          <a href="{$servername}/cdb_lu?action=runQuery&amp;query={@c_lu_id}&amp;outtype=html&amp;nojson=1&amp;default=1"><span class="DARK_RED"><xsl:value-of select="@c_lu_id-previewtext"/></span><span class="synonym_id">/<xsl:value-of select="@c_lu_id"/></span></a>
        </xsl:if>
        <xsl:if test="$default!='1'">
          <span onclick="Model.show_entry('cdb_lu','{@c_lu_id}');" class="link DARK_RED"><xsl:value-of select="@c_lu_id-previewtext"/><xsl:if test="@subsense!=''">:<xsl:value-of select="@subsense"/></xsl:if></span><span onclick="Model.show_entry('cdb_lu','{@c_lu_id}');" class="synonym_id link">/<xsl:value-of select="@c_lu_id"/></span>
        </xsl:if>
        <xsl:if test="position()!=last()"><xsl:text>, </xsl:text></xsl:if>
      </xsl:for-each>
      <br/>
      <xsl:if test="@posSpecific">
        <br/><span class="BLUE">PoS specific: </span> <xsl:value-of select="@posSpecific"/>
      </xsl:if>
      <xsl:if test="definition!=''">
        <br/><span class="BLUE">Definition: </span> <xsl:value-of select="definition"/>
      </xsl:if>
      <xsl:if test="differentiae!=''">
        <br/><span class="BLUE">Differentiae: </span> <xsl:value-of select="differentiae"/>
      </xsl:if>
      <xsl:if test="vlis_domains/dom_relation">
        <br/><span class="BLUE">VLIS domains: </span>
        <xsl:for-each select="vlis_domains/dom_relation">
          <xsl:value-of select="@term"/>
          <xsl:if test="position()!=last()"><xsl:text>, </xsl:text></xsl:if>
        </xsl:for-each>
      </xsl:if>
      
      <xsl:if test="sumo_relations/ont_relation">
        <br/><span class="BLUE">SUMO: </span>
        <xsl:for-each select="sumo_relations/ont_relation">
          <xsl:if test="@negative='true'">NOT</xsl:if>
          (<xsl:value-of select="@relation_name"/>, <xsl:value-of select="@arg1"/>,
          <span class="DARK_RED">
            <xsl:if test="@name!='dwn10_pwn16_pwn20_mapping'">
              <xsl:attribute name="class">GREEN</xsl:attribute>
            </xsl:if>
            <xsl:if test="$default='1'">
              <a href="{$servername}/sumo?action=runQuery&amp;query={@arg2}&amp;outtype=html&amp;nojson=1&amp;default=1"><xsl:value-of select="@arg2"/></a>
            </xsl:if>
            <xsl:if test="$default!='1'">
              <span onclick="Model.show_entry('sumo','{@arg2}');" class="link"><xsl:value-of select="@arg2"/></span>
            </xsl:if>
          </span>)
          <xsl:if test="position()!=last()"><xsl:text>, </xsl:text></xsl:if>
        </xsl:for-each>
      </xsl:if>

      <xsl:if test="kont_relations/ont_relation">
      <br/>
        <br/><span class="BLUE">Kyoto: </span>
        <xsl:for-each select="kont_relations/ont_relation">
          <xsl:if test="@negative='true'">NOT</xsl:if>
          (<xsl:value-of select="@relation_name"/>, <xsl:value-of select="@arg1"/>,
          <span class="DARK_RED">
            <xsl:if test="@name!='dwn10_pwn16_pwn20_mapping'">
              <xsl:attribute name="class">GREEN</xsl:attribute>
            </xsl:if>
            <xsl:if test="$default='1'">
              <a href="{$servername}/kont?action=runQuery&amp;query={@arg2}&amp;outtype=html&amp;nojson=1&amp;default=1"><xsl:value-of select="@arg2"/></a>
            </xsl:if>
            <xsl:if test="$default!='1'">
              <span onclick="Model.show_entry('kont','{@arg2}');" class="link"><xsl:value-of select="@arg2"/></span>
            </xsl:if>
          </span>)
          <xsl:if test="position()!=last()"><xsl:text>, </xsl:text></xsl:if>
        </xsl:for-each>
      </xsl:if>

      <br/><br/>
      <xsl:for-each select="wn_internal_relations/relation[@relation_name='HAS_HYPERONYM' or @relation_name='HAS_HYPONYM']">
        <xsl:sort select="@relation_name"/>
        <xsl:choose>
          <xsl:when test="@relation_name='HAS_HYPONYM' or @reversed='true'">
            <xsl:text>&lt;&lt;-- </xsl:text>
          </xsl:when>
          <xsl:otherwise>
            <xsl:text>--&gt;&gt; </xsl:text>
          </xsl:otherwise>
        </xsl:choose>
        <span class="GREEN">[<xsl:value-of select="@relation_name"/>]</span>
        <xsl:text> </xsl:text>
        <span class="BLUE">
          <xsl:if test="$default='1'">
            <a href="{$servername}/{$dictcode}?action=runQuery&amp;query={@target}&amp;outtype=html&amp;nojson=1&amp;default=1"><xsl:value-of select="@target-previewtext"/><span class="synonym_id">/<xsl:value-of select="@target"/></span></a>
          </xsl:if>
          <xsl:if test="$default!='1'">
            <span onclick="Model.show_entry('{$dictcode}','{@target}');" class="link"><xsl:value-of select="@target-previewtext"/></span><span onclick="Model.show_entry('{$dictcode}','{@target}');" class="link synonym_id">/<xsl:value-of select="@target"/></span>
          </xsl:if>
        </span><br/>
      </xsl:for-each>
      <xsl:for-each select="wn_internal_relations/relation[@relation_name!='HAS_HYPERONYM' and @relation_name!='HAS_HYPONYM']">
        <xsl:sort select="@relation_name"/>
        <xsl:choose>
          <xsl:when test="@relation_name='HAS_HYPONYM' or @reversed='true'">
            <xsl:text>&lt;&lt;-- </xsl:text>
          </xsl:when>
          <xsl:otherwise>
            <xsl:text>--&gt;&gt; </xsl:text>
          </xsl:otherwise>
        </xsl:choose>
        <span class="DARK_RED">[<xsl:value-of select="@relation_name"/>]</span>
        <xsl:text> </xsl:text>
        <span class="BLUE">
          <xsl:if test="$default='1'">
            <a href="{$servername}/{$dictcode}?action=runQuery&amp;query={@target}&amp;outtype=html&amp;nojson=1&amp;default=1"><xsl:value-of select="@target-previewtext"/><span class="synonym_id">/<xsl:value-of select="@target"/></span></a>
          </xsl:if>
          <xsl:if test="$default!='1'">
            <span onclick="Model.show_entry('{$dictcode}','{@target}');" class="link"><xsl:value-of select="@target-previewtext"/></span><span onclick="Model.show_entry('{$dictcode}','{@target}');" class="synonym_id link">/<xsl:value-of select="@target"/></span>
          </xsl:if>
        </span><br/>
      </xsl:for-each>
      
      <xsl:if test="wn_equivalence_relations">
        <br/><span class="BLUE">Equivalence relations: </span><br/>
        <xsl:for-each select="wn_equivalence_relations/relation">
          <xsl:sort select="@relation_name"/>
          <span class="GREEN">[<xsl:value-of select="@relation_name"/>]</span>
          <xsl:text> </xsl:text>
          <span class="BLUE">
            <xsl:if test="$default='1'">
              <a href="{$servername}/wnen?action=runQuery&amp;query={@target20}&amp;outtype=html&amp;nojson=1&amp;default=1"><xsl:value-of select="@target20-previewtext"/><span class="synonym_id">/<xsl:value-of select="@target20"/></span></a> | 
              <a href="{$servername}/wnen30?action=runQuery&amp;query={@target30}&amp;outtype=html&amp;nojson=1&amp;default=1"><xsl:value-of select="@target30-previewtext"/><span class="synonym_id">/<xsl:value-of select="@target30"/></span></a>
            </xsl:if>
            <xsl:if test="$default!='1'">
              <span onclick="Model.show_entry('wnen','{@target20}');" class="link"><xsl:value-of select="@target20-previewtext"/></span><span onclick="Model.show_entry('wnen','{@target20}');" class="synonym_id link">/<xsl:value-of select="@target20"/></span> | 
              <span onclick="Model.show_entry('wnen30','{@target30}');" class="link"><xsl:value-of select="@target30-previewtext"/></span><span onclick="Model.show_entry('wnen30','{@target30}');" class="synonym_id link">/<xsl:value-of select="@target30"/></span>
            </xsl:if>
          </span>
          <br/>
        </xsl:for-each>
      </xsl:if>

      <xsl:if test="wn_domains/dom_relation">
        <br/><span class="BLUE">WordNet domains: </span>
        <xsl:for-each select="wn_domains/dom_relation">
          <span>
            <xsl:if test="@name!='dwn10_pwn16_mapping'">
              <xsl:attribute name="class">GREEN</xsl:attribute>
            </xsl:if>
            <xsl:value-of select="@term"/> 
          </span>
          <xsl:if test="position()!=last()"><xsl:text>, </xsl:text></xsl:if>
        </xsl:for-each>
      </xsl:if>
      
    </body>
</xsl:template>


</xsl:stylesheet>

