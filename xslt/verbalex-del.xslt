<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="utf-8" method="xml" indent="yes"/>

<xsl:template match="verb">
<html>
  <head>
    <title>smazat heslo <xsl:value-of select="@id"/>?</title>
    <meta content='text/html; charset=utf-8' http-equiv='Content-Type'/>
    <link href='/files/tedi.css' rel='stylesheet' type='text/css'/>
  </head>
  <body>
    <p><b>Opravdu smazat heslo <i>
          <xsl:for-each select="lemmas/lemma">
            <xsl:value-of select="."/>:<xsl:value-of select="@sense"/>,
          </xsl:for-each>
    </i>?</b> </p>
    <p><a href="/verbalex?action=dodelete&amp;id={@id}">Ano, smazat</a> | <a href="/verbalex?action=list">Ne, návrat</a></p>
  </body>
</html>
</xsl:template>


</xsl:stylesheet>

