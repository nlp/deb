<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="utf-8"/>

<xsl:param name="slovnik"/>
<xsl:param name="servername"/>
<xsl:param name="default"/>
<xsl:param name="package"/>

<xsl:template match="part">
  <xsl:if test="@id != //SYNSET/ID">
    <xsl:variable name="id"><xsl:value-of select="@id"/></xsl:variable>
    <a href="{$servername}/{$slovnik}?action=getdoc&amp;id={@id}"><xsl:value-of select="@text"/></a>
    <xsl:if test="//meta/path/part[@up=$id]/@id != //SYNSET/ID">&gt;</xsl:if>
    <xsl:apply-templates select="//meta/path/part[@up=$id]"/>
  </xsl:if>
</xsl:template>

<xsl:template match="SYNSET">
  <div class="wn">
    <xsl:if test="//meta/path/part[@up='koren']/@id != //SYNSET/ID">
      <span class="path">
        <xsl:apply-templates select="//meta/path/part[@up='koren']"/>
      </span>
      <br/>
    </xsl:if>
      <span class="blue">Synonyma: </span>
      <xsl:for-each select="SYNONYM/LITERAL">
        <xsl:value-of select="text()"/>:<xsl:value-of select="@sense"/>
	<xsl:if test="position()!=last()"><xsl:text>, </xsl:text></xsl:if>
      </xsl:for-each>
      <br/>
      <xsl:apply-templates select="DEF"/>
      <xsl:for-each select="USAGE">
        <span class="blue">Příklad použití: </span>
        <span><xsl:value-of select="."/></span><br/>
      </xsl:for-each>
      <xsl:apply-templates select="DOMAIN"/>
      <xsl:for-each select="SUMO">
        <span class="blue">SUMO/MILO: </span>
        <span class="green"><xsl:value-of select="TYPE"/><xsl:text> </xsl:text>
            <xsl:value-of select="text()"/>
        </span><br/>
      </xsl:for-each>
      <span class="blue">Slovní druh: </span>
      <span class="red"><xsl:value-of select="POS"/></span>
      <xsl:text> </xsl:text><br/>
      <span class="blue">ID: </span>
      <span class="red"><xsl:value-of select="ID"/></span>
      <xsl:text> </xsl:text><br/>
      <xsl:apply-templates select="BCS"/>
      <br/>
      <span class="blue">Sémantické relace:</span><br/>
      <xsl:for-each select="ILR">
        <span class="blue">--&gt;&gt;</span><xsl:text> </xsl:text>
	<xsl:choose>
	<xsl:when test="@type='hypernym'">
        <span class="green">[<xsl:value-of select="@type"/>]</span>
	</xsl:when>
	<xsl:when test="@type='holo_member' or @type='holo_portion' or @type='holo_part'">
        <span class="blue">[<xsl:value-of select="@type"/>]</span>
	</xsl:when>
	<xsl:when test="@type='derived'">
        <span class="green2">[<xsl:value-of select="@type"/>]</span>
	</xsl:when>
	<xsl:otherwise>
        <span class="dark_red">[<xsl:value-of select="@type"/>]</span>
	</xsl:otherwise>
	</xsl:choose>
	<xsl:text> </xsl:text>
  <xsl:if test="@literal!=''">
    <span class="red">(<xsl:value-of select="@literal"/>)</span><xsl:text> </xsl:text>
  </xsl:if>
  <a href="/{$slovnik}?action=getdoc&amp;id={@link}"><xsl:value-of select="text()"/></a>
	<br/>
      </xsl:for-each>
      <xsl:for-each select="RILR">
        <span class="blue">&lt;&lt;--</span><xsl:text> </xsl:text>
	<xsl:choose>
	<xsl:when test="@type='hypernym'">
        <span class="green">[hyponym]</span>
	</xsl:when>
	<xsl:when test="@type='holo_member'">
        <span class="blue">[mero_member]</span>
	</xsl:when>
	<xsl:when test="@type='holo_portion'">
        <span class="blue">[mero_portion]</span>
	</xsl:when>
	<xsl:when test="@type='holo_part'">
        <span class="blue">[mero_part]</span>
	</xsl:when>
	<xsl:when test="@type='derived'">
        <span class="green2">[<xsl:value-of select="@type"/>]</span>
	</xsl:when>
	<xsl:when test="@type='category_domain'">
        <span class="dark_red">[category_member]</span>
	</xsl:when>
	<xsl:when test="@type='region_domain'">
        <span class="dark_red">[region_member]</span>
	</xsl:when>
	<xsl:when test="@type='usage_domain'">
        <span class="dark_red">[usage_member]</span>
	</xsl:when>
	<xsl:otherwise>
        <span class="dark_red">[<xsl:value-of select="@type"/>]</span>
	</xsl:otherwise>
	</xsl:choose>
	<xsl:text> </xsl:text>
  <xsl:if test="@literal!=''">
    <span class="red">(<xsl:value-of select="@literal"/>)</span><xsl:text> </xsl:text>
  </xsl:if>
  <a href="/{$slovnik}?action=getdoc&amp;id={@link}"><xsl:value-of select="text()"/></a>
	<br/>
      </xsl:for-each>

      <br/>
      <xsl:for-each select="VALENCY">
        <span class="blue">Valenční rámec: </span>
        <ul>
        <xsl:for-each select="FRAME">
          <li><span class="dark_red"><xsl:value-of select="."/></span></li>
        </xsl:for-each>
        </ul>
      </xsl:for-each>
    </div>
     
</xsl:template>

<xsl:template match="BCS">
      <span class="blue">BCS: </span>
      <span class="red"><xsl:value-of select="."/></span>
</xsl:template>

<xsl:template match="DEF">
      <span class="blue">Definice: </span>
      <span><xsl:value-of select="."/></span>
      <br/>
</xsl:template>
<xsl:template match="DOMAIN">
      <span class="blue">Doména: </span>
      <span class="green"><xsl:value-of select="."/></span><br/>
</xsl:template>

</xsl:stylesheet>

