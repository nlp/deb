<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet [<!ENTITY nbsp "&#160;">]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="utf-8"/>

<xsl:param name="dictionary"/>
<xsl:param name="servername"/>
<xsl:param name="default"/>

<xsl:template match="cdb_lu">
    <body>
      <span class="BLUE">C_LU_ID: </span>
      <span class="RED"><xsl:value-of select="@c_lu_id"/></span>
      <xsl:text> </xsl:text>
      <span class="BLUE">C_SEQ_NR: </span>
      <span class="RED"><xsl:value-of select="@c_seq_nr"/></span>
      <br/>
      <!--<span class="BLUE">Form: </span><br/>-->
      <span class="RED">[<xsl:value-of select="form/@form-cat"/>]</span><xsl:text> </xsl:text>
      <span class="GREEN"><xsl:value-of select="form/@form-spelling"/></span>
      <xsl:if test="form/@form-spelvar!=''">
        <xsl:text> / </xsl:text>
        <span class="GREEN"><xsl:value-of select="form/@form-spelvar"/></span>
      </xsl:if>
      <xsl:text> </xsl:text>
      <xsl:if test="form/@form-length!=''">
        <span class="DARK_RED">length: </span> <span class="GREEN"><xsl:value-of select="form/@form-length"/></span>
      </xsl:if>
      <br/><br/>

      <xsl:choose>
        <xsl:when test="form/@form-cat='adj'">
          <xsl:call-template name="adj"/>
        </xsl:when>
        <xsl:when test="form/@form-cat='verb'">
          <xsl:call-template name="verb"/>
        </xsl:when>
        <xsl:when test="form/@form-cat='noun'">
          <xsl:call-template name="noun"/>
        </xsl:when>
      </xsl:choose>
      <xsl:if test="sem-definition">
        <span class="DARK_RED">definition: </span> <span class="BLUE"><xsl:value-of select="sem-definition/sem-defSource"/></span>: <span class="GREEN">
        <xsl:if test="sem-definition/sem-def">
          <xsl:value-of select="sem-definition/sem-def"/>
        </xsl:if>
        <xsl:if test="sem-definition/sem-def-noun">
          <xsl:value-of select="sem-definition/sem-def-noun/sem-specificae"/>
        </xsl:if>
        </span><br/>
      </xsl:if>
      <xsl:if test="sem-synonyms">
        <span class="DARK_RED">synonyms: </span> <span class="GREEN">
          <xsl:for-each select="sem-synonyms/sem-synonym">
            <xsl:value-of select="@r_form"/><xsl:if test="position()!=last()">, </xsl:if>
          </xsl:for-each>
        </span><br/>
      </xsl:if>
      <xsl:if test="sem-hypernyms">
        <span class="DARK_RED">hypernyms: </span> <span class="GREEN">
          <xsl:for-each select="sem-hypernyms/sem-hypernym">
            <xsl:value-of select="@r_form"/><xsl:if test="position()!=last()">, </xsl:if>
          </xsl:for-each>
        </span><br/>
      </xsl:if>
 
  <xsl:if test="pragmatics">
    <br/><span class="BLUE">Pragmatics: </span><br/> 
    <xsl:if test="pragmatics/prag-origin!=''">
      <span class="DARK_RED">origin:</span>&nbsp;<span class="GREEN"><xsl:value-of select="pragmatics/prag-origin"/></span>
      <xsl:text> </xsl:text>
    </xsl:if>
    <xsl:if test="pragmatics/prag-style!=''">
      <span class="DARK_RED">style:</span>&nbsp;<span class="GREEN"><xsl:value-of select="pragmatics/prag-style"/></span>
      <xsl:text> </xsl:text>
    </xsl:if>
    <xsl:if test="pragmatics/prag-connotation!=''">
      <span class="DARK_RED">connotation:</span>&nbsp;<span class="GREEN"><xsl:value-of select="pragmatics/prag-connotation"/></span>
      <xsl:text> </xsl:text>
    </xsl:if>
    <xsl:if test="pragmatics/prag-socGroup!=''">
      <span class="DARK_RED">socGroup:</span>&nbsp;<span class="GREEN"><xsl:value-of select="pragmatics/prag-socGroup"/></span>
      <xsl:text> </xsl:text>
    </xsl:if>
    <xsl:if test="pragmatics/prag-domain/@subjectfield!=''">
      <span class="DARK_RED">domain:</span>&nbsp;<span class="GREEN"><xsl:value-of select="pragmatics/prag-domain/@subjectfield"/> (general: <xsl:value-of select="pragmatics/prag-domain/@general"/>)</span>
      <xsl:text> </xsl:text>
    </xsl:if>
    <xsl:if test="pragmatics/prag-chronology!=''">
      <span class="DARK_RED">chronology:</span>&nbsp;<span class="GREEN"><xsl:value-of select="pragmatics/prag-chronology"/></span>
      <xsl:text> </xsl:text>
    </xsl:if>
    <xsl:if test="pragmatics/prag-subj-gen!=''">
      <span class="DARK_RED">subj-gen:</span>&nbsp;<span class="GREEN"><xsl:value-of select="pragmatics/prag-subj-gen"/></span>
      <xsl:text> </xsl:text>
    </xsl:if>
    <xsl:if test="pragmatics/prag-geography!=''">
      <span class="DARK_RED">geography:</span>&nbsp;<span class="GREEN"><xsl:value-of select="pragmatics/prag-geography"/></span>
      <xsl:text> </xsl:text>
    </xsl:if>
    <xsl:if test="pragmatics/prag-frequency!=''">
      <span class="DARK_RED">frequency:</span>&nbsp;<span class="GREEN"><xsl:value-of select="pragmatics/prag-frequency"/></span>
      <xsl:text> </xsl:text>
    </xsl:if>
  </xsl:if>
 
      <br/>
      <span class="BLUE">Examples: </span><br/>
      <xsl:apply-templates select="examples/example"/>
    </body>
</xsl:template>

<xsl:template name="adj">
      <span class="BLUE">Morphology: </span><br/>
      <xsl:if test="morphology_adj/morpho-type!=''">
        <span class="DARK_RED">type:</span>&nbsp;<span class="GREEN"><xsl:value-of select="morphology_adj/morpho-type"/></span>
        <xsl:text> </xsl:text>
      </xsl:if>
      <xsl:if test="morphology_adj/mor-base!=''">
        <span class="DARK_RED">base:</span>&nbsp;<span class="GREEN"><xsl:value-of select="morphology_adj/mor-base"/></span>
        <xsl:text> </xsl:text>
      </xsl:if>
      <xsl:if test="morphology_adj/mor-flectional-type!=''">
        <span class="DARK_RED">flectional&nbsp;type:</span>&nbsp;<span class="GREEN"><xsl:value-of select="morphology_adj/mor-flectional-type"/></span>
        <xsl:text> </xsl:text>
      </xsl:if>
      <xsl:if test="morphology_adj/morpho-structure!=''">
        <span class="DARK_RED">structure:</span>&nbsp;<span class="GREEN"><xsl:value-of select="morphology_adj/morpho-structure"/></span>
        <xsl:text> </xsl:text>
      </xsl:if>
      <xsl:if test="morphology_adj/mor-declinability!=''">
        <span class="DARK_RED">declinability:</span>&nbsp;<span class="GREEN"><xsl:value-of select="morphology_adj/mor-declinability"/></span>
        <xsl:text> </xsl:text>
      </xsl:if>
      <xsl:if test="morphology_adj/mor-comparis!=''">
        <br/><span class="BLUE">Morphology&nbsp;comparis:</span>&nbsp;
        <xsl:if test="morphology_adj/mor-comparis/mor-comparison!=''">
          <span class="DARK_RED">comparison:</span>&nbsp;<span class="GREEN"><xsl:value-of select="morphology_adj/mor-comparis/mor-comparison"/></span>
          <xsl:text> </xsl:text>
        </xsl:if>
        <xsl:if test="morphology_adj/mor-comparis/mor-comparative!=''">
          <span class="DARK_RED">comparative:</span>&nbsp;<span class="GREEN"><xsl:value-of select="morphology_adj/mor-comparis/mor-comparative"/></span>
          <xsl:text> </xsl:text>
        </xsl:if>
        <xsl:if test="morphology_adj/mor-comparis/mor-superlative!=''">
          <span class="DARK_RED">superlative:</span>&nbsp;<span class="GREEN"><xsl:value-of select="morphology_adj/mor-comparis/mor-superlative"/></span>
        </xsl:if>
      </xsl:if>
      <br/><br/>
      
      <span class="BLUE">Syntax: </span>
      <xsl:if test="syntax_adj/sy-position!='' or syntax_adj/sy-advusage!=''"><br/></xsl:if>
      <xsl:if test="syntax_adj/sy-position!=''">
        <span class="DARK_RED">position:</span>&nbsp;<span class="GREEN"><xsl:value-of select="syntax_adj/sy-position"/></span>
        <xsl:text> </xsl:text>
      </xsl:if>
      <xsl:if test="syntax_adj/sy-advusage!=''">
        <span class="DARK_RED">advusage:</span>&nbsp;<span class="GREEN"><xsl:value-of select="syntax_adj/sy-advusage"/></span>
        <xsl:text> </xsl:text>
      </xsl:if>
      <xsl:if test="syntax_adj/sy-complementation/sy-comp">
        <br/><span class="DARK_RED">complementation: </span>
        <xsl:for-each select="syntax_adj/sy-complementation/sy-comp">
          <span class="GREEN"><xsl:value-of select="."/></span>
          <xsl:if test="position()!=last()"><xsl:text>, </xsl:text></xsl:if>
        </xsl:for-each>
      </xsl:if>

      <br/><br/>
      <span class="BLUE">Semantics: </span>
      <xsl:if test="semantics_adj/sem-type!='' or semantics_adj/sem-subclass!='' or semantics_adj/sem-shift!=''"><br/></xsl:if>
      <xsl:if test="semantics_adj/sem-type!=''">
        <span class="DARK_RED">type:</span>&nbsp;<span class="GREEN"><xsl:value-of select="semantics_adj/sem-type"/></span>
        <xsl:text> </xsl:text>
      </xsl:if>
      <xsl:if test="semantics_adj/sem-subclass!=''">
        <span class="DARK_RED">subclass:</span>&nbsp;<span class="GREEN"><xsl:value-of select="semantics_adj/sem-subclass"/></span>
        <xsl:text> </xsl:text>
      </xsl:if>
      <xsl:if test="semantics_adj/sem-shift!=''">
        <span class="DARK_RED">shift:</span>&nbsp;<span class="GREEN"><xsl:value-of select="semantics_adj/sem-shift"/></span>
        <xsl:text> </xsl:text>
      </xsl:if>
      <xsl:if test="semantics_adj/sem-selrestrictions/sem-selrestriction">
        <br/><span class="DARK_RED">selrestrictions: </span> 
        <xsl:for-each select="semantics_adj/sem-selrestrictions/sem-selrestriction">
          <br/><span class="GREEN"><xsl:value-of select="selrestriction"/>:<xsl:text> </xsl:text>
          <xsl:for-each select="synset_list/synset">
            <xsl:value-of select="@c_sy_id"/><xsl:text>, </xsl:text>
          </xsl:for-each>
          </span>
        </xsl:for-each>
        <xsl:text> </xsl:text>
      </xsl:if>
      <xsl:if test="semantics_adj/sem-resume!=''">
        <br/><span class="DARK_RED">resume: </span> <span class="GREEN"><xsl:value-of select="semantics_adj/sem-resume"/></span><br/>
      </xsl:if>
</xsl:template>

<xsl:template name="noun">
      <span class="BLUE">Morphology: </span><br/>
      <xsl:if test="morphology_noun/morpho-type!=''">
        <span class="DARK_RED">type:</span>&nbsp;<span class="GREEN"><xsl:value-of select="morphology_noun/morpho-type"/></span>
        <xsl:text> </xsl:text>
      </xsl:if>
      <xsl:if test="morphology_noun/morpho-structure!=''">
        <span class="DARK_RED">structure:</span>&nbsp;<span class="GREEN"><xsl:value-of select="morphology_noun/morpho-structure"/></span>
        <xsl:text> </xsl:text>
      </xsl:if>
      <xsl:if test="morphology_noun/morpho-plurforms/morpho-plurform">
        <span class="DARK_RED">plurforms:</span>&nbsp;<xsl:for-each select="morphology_noun/morpho-plurforms/morpho-plurform">
          <span class="GREEN"><xsl:value-of select="."/></span>
          <xsl:if test="position()!=last()">
            <xsl:text>, </xsl:text>
          </xsl:if>
        </xsl:for-each>
        <xsl:text> </xsl:text>
      </xsl:if>
      
      <br/><br/>
      <span class="BLUE">Syntax: </span><br/>
      <xsl:if test="syntax_noun/sy-gender!=''">
        <span class="DARK_RED">gender:</span>&nbsp;<span class="GREEN"><xsl:value-of select="syntax_noun/sy-gender"/></span>
        <xsl:text> </xsl:text>
      </xsl:if>
      <xsl:if test="syntax_noun/sy-number!=''">
        <span class="DARK_RED">number:</span>&nbsp;<span class="GREEN"><xsl:value-of select="syntax_noun/sy-number"/></span>
        <xsl:text> </xsl:text>
      </xsl:if>
      <xsl:if test="syntax_noun/sy-article!=''">
        <span class="DARK_RED">article:</span>&nbsp;<span class="GREEN"><xsl:value-of select="syntax_noun/sy-article"/></span>
        <xsl:text> </xsl:text>
      </xsl:if>
      <xsl:if test="syntax_noun/sy-complementation/sy-comp">
        <span class="DARK_RED">complementation:</span>&nbsp;<xsl:for-each select="syntax_noun/sy-complementation/sy-comp">
          <span class="GREEN"><xsl:value-of select="."/></span>
          <xsl:if test="position()!=last()">
            <xsl:text>, </xsl:text>
          </xsl:if>
        </xsl:for-each>
        <xsl:text> </xsl:text>
      </xsl:if>
      
      <br/><br/>
      <span class="BLUE">Semantics: </span><br/>
      <xsl:if test="semantics_noun/sem-reference!=''">
        <span class="DARK_RED">reference:</span>&nbsp;<span class="GREEN"><xsl:value-of select="semantics_noun/sem-reference"/></span>
        <xsl:text> </xsl:text>
      </xsl:if>
      <xsl:if test="semantics_noun/sem-countability!=''">
        <span class="DARK_RED">countability:</span>&nbsp;<span class="GREEN"><xsl:value-of select="semantics_noun/sem-countability"/></span>
        <xsl:text> </xsl:text>
      </xsl:if>
      <xsl:if test="semantics_noun/sem-type">
        <span class="DARK_RED">type:</span>&nbsp;<xsl:for-each select="semantics_noun/sem-type">
          <span class="GREEN"><xsl:value-of select="."/></span>
          <xsl:if test="position()!=last()">
            <xsl:text>, </xsl:text>
          </xsl:if>
        </xsl:for-each>
        <xsl:text> </xsl:text>
      </xsl:if>
      <xsl:if test="semantics_noun/sem-shift!=''">
        <span class="DARK_RED">shift:</span>&nbsp;<span class="GREEN"><xsl:value-of select="semantics_noun/sem-shift"/></span>
        <xsl:text> </xsl:text>
      </xsl:if>
      <xsl:if test="semantics_noun/sem-subclass!=''">
        <span class="DARK_RED">subclass:</span>&nbsp;<span class="GREEN"><xsl:value-of select="semantics_noun/sem-subclass"/></span>
        <xsl:text> </xsl:text>
      </xsl:if>
      <xsl:if test="semantics_noun/sem-selrestrictions">
        <span class="DARK_RED">selrestriction:</span>&nbsp;<xsl:for-each select="semantics_noun/sem-selrestrictions">
          <span class="GREEN"><xsl:value-of select="."/></span>
          <xsl:if test="position()!=last()">
            <xsl:text>, </xsl:text>
          </xsl:if>
        </xsl:for-each>
        <xsl:text> </xsl:text>
      </xsl:if>
      <xsl:if test="semantics_noun/sem-resume!=''">
        <br/><span class="DARK_RED">resume:</span>&nbsp;<span class="GREEN"><xsl:value-of select="semantics_noun/sem-resume"/></span><br/>
        <xsl:text> </xsl:text>
      </xsl:if>
</xsl:template>

<xsl:template name="verb">
      <span class="BLUE">Morphology: </span><br/>
      <xsl:if test="morphology_verb/morpho-type!=''">
        <span class="DARK_RED">type:</span>&nbsp;<span class="GREEN"><xsl:value-of select="morphology_verb/morpho-type"/></span>
        <xsl:text> </xsl:text>
      </xsl:if>
      <xsl:if test="morphology_verb/morpho-structure!=''">
        <span class="DARK_RED">structure:</span>&nbsp;<span class="GREEN"><xsl:value-of select="morphology_verb/morpho-structure"/></span>
        <xsl:text> </xsl:text>
      </xsl:if>
      <xsl:if test="morphology_verb/flex-conjugation">
        <span class="RED">flex conjugation:&nbsp;</span> 
        <xsl:if test="morphology_verb/flex-conjugation/flex-conjugationtype!=''">
          <span class="DARK_RED">type:</span>&nbsp;<span class="GREEN"><xsl:value-of select="morphology_verb/flex-conjugation/flex-conjugationtype"/></span><xsl:text>, </xsl:text>
        </xsl:if>
        <xsl:if test="morphology_verb/flex-conjugation/flex-pasttense!=''">
          <span class="DARK_RED">past&nbsp;tense:</span>&nbsp;<span class="GREEN"><xsl:value-of select="morphology_verb/flex-conjugation/flex-pasttense"/></span><xsl:text>, </xsl:text>
        </xsl:if>
        <xsl:if test="morphology_verb/flex-conjugation/flex-pastpart!=''">
          <span class="DARK_RED">past&nbsp;part:</span>&nbsp;<span class="GREEN"><xsl:value-of select="morphology_verb/flex-conjugation/flex-pastpart"/></span>
        </xsl:if>
        <xsl:text> </xsl:text>
      </xsl:if>
      
      <xsl:if test="morphology_verb/flex-mode!='' or morphology_verb/flex-tense!='' or morphology_verb/flex-number!='' or morphology_verb/flex-person!=''"><br/></xsl:if>
      
      <xsl:if test="morphology_verb/flex-mode!=''">
        <span class="DARK_RED">flex&nbsp;mode:</span>&nbsp;<span class="GREEN"><xsl:value-of select="morphology_verb/flex-mode"/></span>
        <xsl:text> </xsl:text>
      </xsl:if>
      <xsl:if test="morphology_verb/flex-tense!=''">
        <span class="DARK_RED">flex&nbsp;tense:</span>&nbsp;<span class="GREEN"><xsl:value-of select="morphology_verb/flex-tense"/></span>
        <xsl:text> </xsl:text>
      </xsl:if>
      <xsl:if test="morphology_verb/flex-number!=''">
        <span class="DARK_RED">flex&nbsp;number:</span>&nbsp;<span class="GREEN"><xsl:value-of select="morphology_verb/flex-number"/></span>
        <xsl:text> </xsl:text>
      </xsl:if>
      <xsl:if test="morphology_verb/flex-person!=''">
        <span class="DARK_RED">flex&nbsp;person:</span>&nbsp;<span class="GREEN"><xsl:value-of select="morphology_verb/flex-person"/></span>
        <xsl:text> </xsl:text>
      </xsl:if>
      
      <br/><br/>
      <span class="BLUE">Syntax: </span><br/> 
      <xsl:if test="syntax_verb/sy-trans!=''">
        <span class="DARK_RED">trans:</span>&nbsp;<span class="GREEN"><xsl:value-of select="syntax_verb/sy-trans"/></span>
        <xsl:text> </xsl:text>
      </xsl:if>
      <xsl:if test="syntax_verb/sy-separ!=''">
        <span class="DARK_RED">separ:</span>&nbsp;<span class="GREEN"><xsl:value-of select="syntax_verb/sy-separ"/></span>
        <xsl:text> </xsl:text>
      </xsl:if>
      <xsl:if test="syntax_verb/sy-class!=''">
        <span class="DARK_RED">class:</span>&nbsp;<span class="GREEN"><xsl:value-of select="syntax_verb/sy-class"/></span>
        <xsl:text> </xsl:text>
      </xsl:if>
      <xsl:if test="syntax_verb/sy-peraux!=''">
        <span class="DARK_RED">peraux:</span>&nbsp;<span class="GREEN"><xsl:value-of select="syntax_verb/sy-peraux"/></span>
        <xsl:text> </xsl:text>
      </xsl:if>
      <xsl:if test="syntax_verb/sy-valency!=''">
        <span class="DARK_RED">valency:</span>&nbsp;<span class="GREEN"><xsl:value-of select="syntax_verb/sy-valency"/></span>
        <xsl:text> </xsl:text>
      </xsl:if>
      <xsl:if test="syntax_verb/sy-reflexiv!=''">
        <span class="DARK_RED">reflexiv:</span>&nbsp;<span class="GREEN"><xsl:value-of select="syntax_verb/sy-reflexiv"/></span>
        <xsl:text> </xsl:text>
      </xsl:if>
      <xsl:if test="syntax_verb/sy-subject!=''">
        <span class="DARK_RED">subject:</span>&nbsp;<span class="GREEN"><xsl:value-of select="syntax_verb/sy-subject"/></span>
        <xsl:text> </xsl:text>
      </xsl:if>
      <xsl:if test="syntax_verb/sy-complementation/sy-comp">
        <br/><span class="DARK_RED">complementation:</span>&nbsp;<xsl:for-each select="syntax_verb/sy-complementation/sy-comp">
          <span class="GREEN"><xsl:value-of select="."/></span>
          <xsl:if test="position()!=last()">
            <xsl:text>, </xsl:text>
          </xsl:if>
        </xsl:for-each>
        <xsl:text> </xsl:text>
      </xsl:if>
      
      <xsl:if test="count(syntax_verb/sy-complementation/sy-comp)>0 and count(syntax_verb/sy-complementation/sy-compl-text)>0">
        <br/>
      </xsl:if>
      <xsl:for-each select="syntax_verb/sy-complementation/sy-compl-text">
        <span class="GREEN"><xsl:value-of select="."/></span>
        <xsl:if test="position()!=last()">
          <xsl:text>, </xsl:text>
        </xsl:if>
      </xsl:for-each>
      <br/><br/>
      
      <span class="BLUE">Semantics: </span><br/> 
      <xsl:if test="semantics_verb/sem-type!=''">
        <span class="DARK_RED">type:</span>&nbsp;<span class="GREEN"><xsl:value-of select="semantics_verb/sem-type"/></span>
        <xsl:text> </xsl:text>
      </xsl:if>
      <xsl:if test="semantics_verb/sem-caseframe/caseframe!=''">
        <br/><span class="DARK_RED">caseframe:</span>&nbsp;<span class="GREEN"><xsl:value-of select="semantics_verb/sem-caseframe/caseframe"/></span>
        (<xsl:for-each select="semantics_verb/sem-caseframe/args/arg">
          <span class="DARK_RED">caserole: </span> <span class="GREEN"><xsl:value-of select="caserole"/></span><xsl:text> </xsl:text>
          <span class="DARK_RED">selrestrole: </span> <span class="GREEN"><xsl:value-of select="selrestrole"/></span><xsl:text> </xsl:text>
          <xsl:for-each select="synset_list/synset">
            <xsl:value-of select="@c_sy_id"/><xsl:text>, </xsl:text>
          </xsl:for-each>
          <xsl:if test="position()!=last()"><xsl:text>, </xsl:text></xsl:if>
        </xsl:for-each>)
        <xsl:text> </xsl:text>
      </xsl:if>
      <xsl:if test="semantics_verb/sem-selrestrictions">
        <br/><span class="DARK_RED">selrestriction: </span>
        <xsl:for-each select="semantics_verb/sem-selrestrictions">
          <span class="GREEN"><xsl:value-of select="."/></span>
          <xsl:if test="position()!=last()">
            <xsl:text>, </xsl:text>
          </xsl:if>
        </xsl:for-each>
        <xsl:text> </xsl:text>
      </xsl:if>
      <xsl:if test="semantics_verb/sem-resume!=''">
        <br/><span class="DARK_RED">resume: </span> <span class="GREEN"><xsl:value-of select="semantics_verb/sem-resume"/></span><br/>
        <xsl:text> </xsl:text>
      </xsl:if>
</xsl:template>

<xsl:template match="example">
  <!--short-->
  <span class="DARK_RED">Example: </span><span class="BLUE"><xsl:value-of select="@r_ex_id"/></span><xsl:text> </xsl:text><span class="BLACK"><a id="alongex{@r_ex_id}" onclick="javascript:switch_display('longex{@r_ex_id}');">[+]</a></span><br/>
  <span class="DARK_RED">canonical form: </span> <span class="GREEN"><xsl:value-of select="form_example/canonicalform"/></span><br/>
  <xsl:if test="syntax_example/sy-type!=''">
    <span class="DARK_RED">type: </span> <span class="GREEN"><xsl:value-of select="syntax_example/sy-type"/></span>
    <xsl:text> </xsl:text>
  </xsl:if>
  <xsl:if test="syntax_example/sy-subtype!=''">
    <span class="DARK_RED">subtype: </span> <span class="GREEN"><xsl:value-of select="syntax_example/sy-subtype"/></span>
  </xsl:if>
  <br/>
  <!--long-->
  <div id="longex{@r_ex_id}" style="display: none;">
  <span class="BLUE">Form example: </span><br/>
  <span class="DARK_RED">canonical form: </span> <span class="GREEN"><xsl:value-of select="form_example/canonicalform"/></span><br/>
  <xsl:if test="form_example/textualform!=''">
    <span class="DARK_RED">textual form: </span> <span class="GREEN"><xsl:value-of select="form_example/textualform"/></span><br/>
  </xsl:if>
  <xsl:if test="form_example/category!=''">
    <span class="DARK_RED">category: </span> <span class="GREEN"><xsl:value-of select="form_example/category"/></span>
    <xsl:text> </xsl:text>
  </xsl:if>
  <xsl:if test="form_example/text-category!=''">
    <span class="DARK_RED">text category: </span> <span class="GREEN"><xsl:value-of select="form_example/text-category"/></span>
  </xsl:if>
  
  <br/>
  
  <xsl:if test="syntax_example">
    <span class="BLUE">Syntax example: </span><br/>
    <xsl:if test="syntax_example/sy-type!=''">
      <span class="DARK_RED">type: </span> <span class="GREEN"><xsl:value-of select="syntax_example/sy-type"/></span>
      <xsl:text> </xsl:text>
    </xsl:if>
    <xsl:if test="syntax_example/sy-subtype!=''">
      <span class="DARK_RED">subtype: </span> <span class="GREEN"><xsl:value-of select="syntax_example/sy-subtype"/></span>
    </xsl:if>
    <xsl:for-each select="syntax_example/sy-combipair">
      <span class="DARK_RED">combiword: </span> <span class="GREEN"><xsl:value-of select="syntax_example/sy-combipair/sy-combiword"/></span>
      <xsl:text> </xsl:text>
      <span class="DARK_RED">combicat: </span> <span class="GREEN"><xsl:value-of select="syntax_example/sy-combipair/sy-combicat"/></span>
      <xsl:if test="position()!=last()"><br/></xsl:if>
    </xsl:for-each>
    <br/>
  </xsl:if>

  <xsl:if test="semantics_example">
    <span class="BLUE">Semantics example: </span><br/>
    <xsl:if test="semantics_example/sem-meaningdescription!=''">
      <span class="DARK_RED">meaning description: </span> <span class="GREEN"><xsl:value-of select="semantics_example/sem-meaningdescription"/></span><br/>
    </xsl:if>
    <xsl:if test="semantics_example/sem-lc-collocator!=''">
      <span class="DARK_RED">lc&nbsp;collocator:</span>&nbsp;<span class="GREEN"><xsl:value-of select="semantics_example/sem-lc-collocator"/></span>
      <xsl:text> </xsl:text>
    </xsl:if>
    <xsl:if test="semantics_example/sem-gc-compl!=''">
      <span class="DARK_RED">gc&nbsp;compl:</span>&nbsp;<span class="GREEN"><xsl:value-of select="semantics_example/sem-gc-compl"/></span>
      <xsl:text> </xsl:text>
    </xsl:if>
    <xsl:if test="semantics_example/sem-spec-collocator!=''">
      <span class="DARK_RED">spec&nbsp;collocator:</span>&nbsp;<span class="GREEN"><xsl:value-of select="semantics_example/sem-spec-collocator"/></span>
      <xsl:text> </xsl:text>
    </xsl:if>
    <xsl:if test="semantics_example/sem-subtype-argument!=''">
      <span class="DARK_RED">subtype-argument:</span>&nbsp;<span class="GREEN"><xsl:value-of select="semantics_example/sem-subtype-argument"/></span>
      <xsl:text> </xsl:text>
    </xsl:if>
    <xsl:if test="semantics_example/sem-gc-gramword!=''">
      <span class="DARK_RED">gc&nbsp;gramword:</span>&nbsp;<span class="GREEN"><xsl:value-of select="semantics_example/sem-gc-gramword"/></span>
      <xsl:text> </xsl:text>
    </xsl:if>
    <br/>
  </xsl:if>
  
  <xsl:if test="pragmatics">
    <span class="BLUE">Pragmatics: </span><br/> 
    <xsl:if test="pragmatics/prag-origin!=''">
      <span class="DARK_RED">origin:</span>&nbsp;<span class="GREEN"><xsl:value-of select="pragmatics/prag-origin"/></span>
      <xsl:text> </xsl:text>
    </xsl:if>
    <xsl:if test="pragmatics/prag-style!=''">
      <span class="DARK_RED">style:</span>&nbsp;<span class="GREEN"><xsl:value-of select="pragmatics/prag-style"/></span><br/>
      <xsl:text> </xsl:text>
    </xsl:if>
    <xsl:if test="pragmatics/prag-connotation!=''">
      <span class="DARK_RED">connotation:</span>&nbsp;<span class="GREEN"><xsl:value-of select="pragmatics/prag-connotation"/></span><br/>
      <xsl:text> </xsl:text>
    </xsl:if>
    <xsl:if test="pragmatics/prag-socGroup!=''">
      <span class="DARK_RED">socGroup:</span>&nbsp;<span class="GREEN"><xsl:value-of select="pragmatics/prag-socGroup"/></span><br/>
      <xsl:text> </xsl:text>
    </xsl:if>
    <xsl:if test="pragmatics/prag-domain/@subjectfield!=''">
      <span class="DARK_RED">domain:</span>&nbsp;<span class="GREEN"><xsl:value-of select="pragmatics/prag-domain/@subjectfield"/> (general: <xsl:value-of select="pragmatics/prag-domain/@general"/>)</span><br/>
      <xsl:text> </xsl:text>
    </xsl:if>
    <xsl:if test="pragmatics/prag-chronology!=''">
      <span class="DARK_RED">chronology:</span>&nbsp;<span class="GREEN"><xsl:value-of select="pragmatics/prag-chronology"/></span><br/>
      <xsl:text> </xsl:text>
    </xsl:if>
    <xsl:if test="pragmatics/prag-subj-gen!=''">
      <span class="DARK_RED">subj-gen:</span>&nbsp;<span class="GREEN"><xsl:value-of select="pragmatics/prag-subj-gen"/></span><br/>
      <xsl:text> </xsl:text>
    </xsl:if>
    <xsl:if test="pragmatics/prag-geography!=''">
      <span class="DARK_RED">geography:</span>&nbsp;<span class="GREEN"><xsl:value-of select="pragmatics/prag-geography"/></span><br/>
      <xsl:text> </xsl:text>
    </xsl:if>
    <xsl:if test="pragmatics/prag-frequency!=''">
      <span class="DARK_RED">frequency:</span>&nbsp;<span class="GREEN"><xsl:value-of select="pragmatics/prag-frequency"/></span><br/>
      <xsl:text> </xsl:text>
    </xsl:if>
  </xsl:if>
  </div>
  <hr/>
</xsl:template>

</xsl:stylesheet>

