<?xml version='1.0' encoding='utf-8'?>
<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>
<xsl:template match='/entry'>
<html><head><meta content='text/html; charset=utf-8' http-equiv='Content-Type'/><title>zk: <xsl:value-of select="hw"/> (DEBWrite)</title></head><body>
<h1><xsl:value-of select="hw"/></h1>
<xsl:apply-templates/></body></html></xsl:template>

<xsl:template match="hw"><span class="hw">headword: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="gram"><span class="gram">grammar info: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="pron"><span class="pron">pronunciation: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="hyph"><span class="hyph">hyphenation: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="meaning"><div class="meaning" style="border: 1px solid black; background-color:undefined">meaning: <xsl:apply-templates/></div><br/></xsl:template>
<xsl:template match="nr"><span class="nr">meaning nr: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="domain"><span class="domain">domain: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="def"><span class="def">definition: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="usage"><span class="usage">usage example: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="pos"><span class="pos">part of speech: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="synonym"><span class="synonym">synonym: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="antonym"><span class="antonym">antonym:<br/><xsl:if test="starts-with(@mime, 'image')"><img src="/files/domki9_zkskls/{.}" style="max-width:400px"/></xsl:if><xsl:if test="starts-with(@mime, 'audio')"><audio src="/files/domki9_zkskls/{.}" controls="true"/></xsl:if><xsl:if test="starts-with(@mime, 'video')"><video src="/files/domki9_zkskls/{.}" controls="true" style="max-width:400px"/></xsl:if></span><br/></xsl:template>
<xsl:template match="comment"><span class="comment">comments: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="cvcbvbvcbcb"><div class="cvcbvbvcbcb" style="border: 1px solid black; background-color:undefined">cvbcvbcvbcvb: <xsl:apply-templates/></div><br/></xsl:template>
</xsl:stylesheet>
