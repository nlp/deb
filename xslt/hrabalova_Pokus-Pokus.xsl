<?xml version='1.0' encoding='utf-8'?>
<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'><xsl:template match='/entry'><html><head><meta content='text/html[-semicolon-] charset=utf-8' http-equiv='Content-Type'/><title>Pokus: <xsl:value-of select="hw"/> (DEBWrite)</title><style type="text/css">.hw{color: green}
.type_container {border: 1px solid #000}
.type_container {background-color:#blue}
</style>
</head>
<body><h1><xsl:value-of select="hw"/></h1>
<xsl:apply-templates/></body></html></xsl:template>
<xsl:template name="file"><xsl:param name="file_element"/><br/><xsl:if test="starts-with($file_element/@mime, 'image')"><img src="/files/hrabalova_Pokus/{.}" style="max-width:400px"/></xsl:if><xsl:if test="starts-with($file_element/@mime, 'audio')"><audio src="/files/hrabalova_Pokus/{.}" controls="true"/></xsl:if><xsl:if test="starts-with($file_element/@mime, 'video')"><video src="/files/hrabalova_Pokus/{.}" controls="true" style="max-width:400px"/></xsl:if><br/></xsl:template>

<xsl:template match="hw"><span class="hw type_text">headword: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="pos"><span class="pos type_select">part of speech: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="translation"><span class="translation type_text">translation: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="category"><span class="category type_select">category: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="meaning"><span class="meaning type_text">meaning: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="usage1"><span class="usage1 type_text">usage example1: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="usage2"><span class="usage2 type_text">usage example2: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="usage3"><span class="usage3 type_text">usage example3: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="link"><span class="link type_text">link: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="link2"><span class="link2 type_text">link2: <xsl:apply-templates/></span><br/></xsl:template>
</xsl:stylesheet>
