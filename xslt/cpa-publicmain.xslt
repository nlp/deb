<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:RDF="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
  xmlns:d="http://nlp.fi.muni.cz/cpa/rdf#">

  <xsl:output encoding="utf-8" type="xml" method="html"/>

  <xsl:template match="RDF:RDF">
    <html>
      <head>
        <base href="http://deb.fi.muni.cz/pdev/"/>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <link rel="stylesheet" href="files.php?table.css" type="text/css"/>
        <title>PDEV</title>
      </head>
      <body>
        <xsl:apply-templates select="RDF:Seq"/>
        Entry count: <xsl:value-of select="count(RDF:Description)"/>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="RDF:Seq">
    <!--<xsl:value-of select="@about"/>-->
    <xsl:variable name="res0" select="@about"/>
    <xsl:apply-templates select='//RDF:Description[@about=$res0]'/>
    <xsl:if test="count(RDF:li)>0">
      <table>
        <tr>
      <td class="cell-verb"><h5 class="colhdr">Verb<br/>&#160;</h5></td>      
      <td class="cell-number"><h5 class="colhdr">No. of<br/>patterns</h5></td>      
      <td class="cell-oec"><h5 class="colhdr">OEC<br/>freq</h5></td>
      <td class="cell-bnc"><h5 class="colhdr">BNC<br/>freq</h5></td>
      <td class="cell-updated"><h5 class="colhdr">Date<br/>completed</h5></td>
      </tr>
      <xsl:for-each select="RDF:li">
        <xsl:sort select="@resource"/>
        <xsl:if test="@resource">
            <xsl:variable name="res" select="@resource"/>
            <xsl:apply-templates select='//RDF:Description[@about=$res]'/>
        </xsl:if>
        <xsl:if test="not(@resource)">
            <xsl:apply-templates select="RDF:Seq"/>
        </xsl:if>
      </xsl:for-each>
    </table>
  </xsl:if>
  </xsl:template>

  <xsl:template match="RDF:Description">
    <tr class="bottom">
      <td class="cell-verb">
        <a href="?action=patterns&amp;id={d:entry_label}" target="_blank"><xsl:value-of select="d:entry_label"/></a>
      </td>
      <td class="cell-number">
        <xsl:value-of select="d:patterns"/>
      </td>
      <td class="cell-oec">
        <xsl:value-of select="d:oec_freq"/>
      </td>
      <td class="cell-bnc">
        <xsl:value-of select="d:bnc_freq"/>
      </td>
      <td class="cell-updated">
        <xsl:value-of select="d:last_edit"/>
      </td>
    </tr>
  </xsl:template>
</xsl:stylesheet>

