<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="utf-8" method="xml" />

<xsl:param name="query"/>

<xsl:template match="RestSearchResults">
<html>
  <head>
    <title>TNA: <xsl:value-of select="$query"/></title>
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
    <!--<link rel='stylesheet' type='text/css' href='files/surnames.css'/>-->
    <style>
    .highlight {color: blue}
    </style>
  </head>
  <body>
  <h2>TNA: <xsl:value-of select="$query"/></h2>
  <!--<xsl:apply-templates select="SearchResultList/RestAssetView[Department='C' or Department='CHES' or Department='CP' or Department='CRES' or Department='DL' or Department='DURH' or Department='E' or Department='ECCL' or Department='EXT' or Department='FEC' or Department='HCA' or Department='IND' or Department='JUST' or Department='KB' or Department='LR' or Department='LS' or Department='PALA' or Department='PC' or Department='PEV' or Department='PL' or Department='PRO' or Department='PROB' or Department='PSO' or Department='QAB' or Department='REQ' or Department='SC' or Department='SP' or Department='STAC' or Department='WALE' or Department='WARD']">-->
  <xsl:apply-templates select="SearchResultList/RestAssetView">
    <xsl:sort select="StartYear"/>
  </xsl:apply-templates>
  </body>
</html>
</xsl:template>

<xsl:template match="RestAssetView">
  <p><b>Reference: <xsl:value-of select="CitableReference"/></b>, <em><xsl:value-of select="StartDate"/>--<xsl:value-of select="EndDate"/></em><br/>
  <xsl:apply-templates select="Description"/>
  <br/><b>Subjects: </b><xsl:apply-templates select="Subjects"/>
  </p>
  <hr/>
</xsl:template>

<xsl:template match="Description">
  <xsl:apply-templates/>
</xsl:template>

<xsl:template match="span">
  <span class="highlight"><xsl:apply-templates/></span>
</xsl:template>

<xsl:template match="Subjects">
  <xsl:for-each select="string">
    <xsl:value-of select="."/>
    <xsl:if test="position()!=last()">, </xsl:if>
  </xsl:for-each>
</xsl:template>

</xsl:stylesheet>

