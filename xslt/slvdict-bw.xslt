<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="utf-8" method="xml"
  doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
  doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
  />

<xsl:template match="superEntry">
  <html>
     <head>
      <title><xsl:value-of select="@id"/></title>
      <style type="text/css">
      body {
      background: #ffffff;
      color: #000000;
      }
      .head {
      font-size: 150%;
      }
      </style>
     </head>
     <body>
       <xsl:apply-templates/>
       <hr />
     </body>
  </html>
</xsl:template>

<xsl:template match="entry">
  <p><xsl:apply-templates/></p>
</xsl:template>

<xsl:template match="form">
  <xsl:apply-templates/>
</xsl:template>

<xsl:template match="orth">
  <b class="red head"><xsl:value-of select="."/></b>
  <br/>
</xsl:template>

<xsl:template match="sense">
  <br/><b class="blue"><xsl:value-of select="@no"/></b>
  <xsl:apply-templates/>
</xsl:template>

<xsl:template match="definition">
  <i class="darkred"><xsl:value-of select="."/></i>
</xsl:template>

<xsl:template match="cit/quote">
  <span class="green"><xsl:value-of select="."/></span>
</xsl:template>

<xsl:template match="unknown">
  <xsl:value-of select="."/>
</xsl:template>

</xsl:stylesheet>

