<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="utf-8" method="xml" indent="yes"/>


<xsl:template match="entry">
 <html>
  <head>
    <title>FaNUK Surnames - DAFN: <xsl:value-of select="N"/></title>
    <meta content='text/html; charset=utf-8' http-equiv='Content-Type'/>
    <link href='files/surnames.css' rel='stylesheet' type='text/css'/>
  </head>
  <body class="preview">
    <form method="get" action="/dafn">
      <input type="text" name="id" value="{N}"/>
      <input type="submit" value="show entry"/>
      <input type="hidden" name="action" value="getdoc"/>
      <input type="hidden" name="tr" value="dafn"/>
    </form>

  <h1 class="head blue"><xsl:value-of select="N"/></h1>
  <xsl:apply-templates />
</body>
</html>
</xsl:template>

<xsl:template match="E[not(s)]">
  <p>
      <xsl:apply-templates/>
  </p>
</xsl:template>

<xsl:template match="s">
  <xsl:if test="count(../s)>1">
    <strong><xsl:number count="s" format="1."/><xsl:text> </xsl:text></strong>
  </xsl:if>
      <xsl:apply-templates/>
    <br/>
</xsl:template>

<xsl:template match="E[s]">
  <p>
    <xsl:apply-templates/>
  </p>
</xsl:template>

<xsl:template match="fn">
  <p style="font-family:sans">
    <xsl:apply-templates/>
  </p>
</xsl:template>

<xsl:template match="Z">
  <span style="color:green">
    <xsl:apply-templates/>
  </span>
</xsl:template>


<xsl:template match="c">
</xsl:template>
<xsl:template match="N">
</xsl:template>
<xsl:template match="D">
  <b>
    <xsl:apply-templates/>
  </b>
</xsl:template>


<xsl:template match="X">
  <i><a href="dafn?action=getdoc&amp;id={.}&amp;tr=dafn"><xsl:value-of select="."/></a></i>
</xsl:template>


<xsl:template match="i">
<i><xsl:apply-templates/></i>
</xsl:template>
<xsl:template match="b">
<strong><xsl:apply-templates/></strong>
</xsl:template>


</xsl:stylesheet>

