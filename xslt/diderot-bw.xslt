<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="utf-8" method="xml"
  doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
  doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
  />

<xsl:template match="ENTRY">
  <html>
     <head>
      <title>Diderot: <xsl:value-of select="KEY"/></title>
      <style type="text/css">
      body {
      background: #ffffff;
      color: #000000;
      }
      /*.red {
      color: #ff0000;
      }
      .green {
      color: #007700;
      }
      .darkred {
      color: #770000;
      }
      .blue {
      color: #0000ff;
      }
      .darkblue {
      color: #000088;
      }*/
      .head {
      font-size: 150%;
      }
      </style>
     </head>
     <body>
       <p><xsl:apply-templates/></p>
       <hr />
       <p><small>Encyklopedie Diderot</small></p>
     </body>
  </html>
</xsl:template>

<xsl:template match="KEY"><b class="red head"><xsl:value-of select="text()"/></b><br/><br/></xsl:template>
<xsl:template match="MEANING"><xsl:apply-templates/><br/></xsl:template>
<xsl:template match="RIVER"><span class="blue"><xsl:value-of select="text()"/></span></xsl:template>
<xsl:template match="MAN|CITY"><span class="darkblue"><xsl:value-of select="text()"/></span></xsl:template>
<xsl:template match="COUNTRY|MOUNTAIN"><span class="green"><xsl:value-of select="text()"/></span></xsl:template>
<xsl:template match="REDIRECT">viz <a href="/diderot?action=getdoc&amp;tr=diderot&amp;id={substring-after(text(), 'viz ')}"><xsl:value-of select="substring-after(text(), 'viz ')"/></a></xsl:template>
<xsl:template match="LINK">viz též <a href="/diderot?action=getdoc&amp;tr=diderot&amp;id={substring-after(text(), 'viz též ')}"><xsl:value-of select="substring-after(text(), 'viz též ')"/></a></xsl:template>
<xsl:template match="keysmall"></xsl:template>

</xsl:stylesheet>

