<html><head><meta content='text/html; charset=utf-8' http-equiv='Content-Type'/><title>aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa: {{entry.hw.$}} (DEBWrite)</title></head><body>
<h1>{{entry.hw.$}}</h1>
<span class="hw">headword: {{entry.hw.$}}</span><br/>
<span class="pos">part of speech: {{entry.pos.$}}</span><br/>
<span class="gram">grammar info: {{entry.gram.$}}</span><br/>
<span class="pron">pronunciation: {{entry.pron.$}}</span><br/>
<span class="hyph">hyphenation: {{entry.hyph.$}}</span><br/>
{{#each entry.meaning}}
<div class="meaning" style="border: 1px solid black; background-color:undefined">meaning:
<span class="nr">meaning nr: {{nr.$}}</span><br/>
<span class="domain">domain: {{domain.$}}</span><br/>
<span class="def">definition: {{def.$}}</span><br/>
<span class="usage">usage example: {{usage.$}}</span><br/></div><br/>{{/each }}

<span class="synonym">synonym: {{entry.synonym.$}}</span><br/>
<span class="antonym">antonym: {{entry.antonym.$}}</span><br/>
<span class="reference">references: {{entry.reference.$}}</span><br/>
<span class="comment">comments: {{entry.comment.$}}</span><br/>
</body></html>
