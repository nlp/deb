<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output encoding="utf-8" method="xml"
  doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
  doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
  />

<xsl:param name="perm"/>
<xsl:param name="type"/>

<xsl:template match="entry">
<form onkeypress="return event.keyCode != 13;">
    <div class="titem">
      
      <span class="titem_id" style="display: none;"><xsl:value-of select="@id"/></span>

      <xsl:if test="$type='del'">
        <div class="row">
          <div class="col-lg-12">            
            <div class="alert alert-danger" role="alert">      
              <span class="glyphicon glyphicon-exclamation-sign">  
              </span>
              Smazaný termín
            </div>
          </div>
        </div>
      </xsl:if>

      <div class="row">
        <div class="col-lg-12">
          <h1>
            <xsl:apply-templates select="terms" mode="main"/>        
          </h1>          
        </div>
      </div>

      <div class="row">
        <div class="col-lg-6">
	 <span title="
 Označuje statut podle důvěryhodnosti informací a termínu: &#13;
  - terminologické (schválené Terminologickou komisí) &#13;
  - používané (používané, ale nezařazené mezi terminologické termíny) &#13;
  - používané nezpracované (zatím nezpracované Terminologickou komisí) &#13;
  - odmítnuté (zamítnuté/smazané kandidátní termíny z automaticky navržených)
	 ">
          <xsl:choose>
            <xsl:when test="@status = '1'">
              <span class="badge badge-1">terminologický</span>
            </xsl:when>
            <xsl:when test="@status = '2'">
              <span class="badge badge-2">používaný</span>
            </xsl:when>
            <xsl:when test="@status = '3'">
              <span class="badge badge-3">používaný nezpracovaný</span>
            </xsl:when>
            <xsl:when test="@status = '4'">
              <span class="badge badge-4">odmítnutý</span>
            </xsl:when>
            <xsl:when test="@status = '5'">
              <span class="badge badge-5">používaný nezpracovaný</span>
            </xsl:when>
            <xsl:otherwise>
              <span class="badge badge-2">používaný</span>
            </xsl:otherwise>
          </xsl:choose>
	 </span>  
        </div>
        
        <div class="col-lg-6">          
          <a href="tezaurus.html?id={@id}&amp;opentree=true" id="share_link" title="Odkaz na heslo" type="button" class="btn btn-default btn-sm pull-right">            
            <span class="glyphicon glyphicon-new-window"></span> ID: <xsl:value-of select="@id"/>
          </a>
        </div>
      
      </div>
      
      <div class="row">
        <div class="col-lg-12 defs">
          <xsl:apply-templates select="defs[def]"/>
        </div>
      </div>
      

      <div class="row">
          <div class="col-lg-6 trans row-pad">
            <xsl:apply-templates select="terms[term[@lang!='cs']]" mode="trans"/>
          </div>
        <div class="col-lg-6 doms row-pad">
          <xsl:apply-templates select="domains[dom]"/>
        </div>
      </div>

      <xsl:apply-templates select="refs[ref]"/>

      <xsl:if test="alsos[also] or sees[see] or paths/path/path_node">
        <div class="row row-pad">
          <div class="col-lg-12">
            <div class="links">
              <div class="box-heading">
                <i class="fa fa-mouse-pointer"></i> Odkazy
              </div>

              <div class="box-inside">

                <div class="row">                
                  <div class="col-lg-12 hypers">
                    <xsl:if test="paths/path/path_node">
                      <div class="box-heading second-level">
                        <i class="fa fa-thumb-tack"></i>
                        Nadřazené pojmy
                      </div>
                      <div class="wrapper">
                        <xsl:for-each select="paths/path">
                          <div class="col-lg-3 col-md-4 col-sm-6 box-inside second-level label-center">                      
                            <xsl:apply-templates select="./path_node"/>
                          </div>          
                        </xsl:for-each>
                      </div>
                    </xsl:if>
                  </div>
                </div>

                <div class="row">
                  <xsl:apply-templates select="alsos[also]"/>
                  <xsl:apply-templates select="sees[see]"/>
                </div>  

              </div>
            </div>
          </div>
        </div>
      </xsl:if>
       
      <div class="row row-pad">
        <div class="col-lg-12 synonyms">
            
            <xsl:if test="not(contains(terms/term[1]/text(), ' '))">        
              <div class="box-heading auto_synonyms" data-method="view" data-url="corpname=tecu_Czech&amp;reload=&amp;lemma={terms/term[1]}&amp;maxthesitems=12&amp;minsim=0.15;format=json" title="Pokud je k dispozici dostatečný vzorek dat pro daný termín, zobrazí se termíny vyskytující se v podobných kontextech. &#13; Existující termíny se zobrazují modrou barvou. Seznam je uspořádán sestupně podle míry podobnosti.">
                <i class="fa fa-clone"></i> Termíny vyskytující se ve stejných kontextech</div>
              <div class="box-inside"></div>     
            </xsl:if>
        
            <xsl:if test="contains(terms/term[1]/text(), ' ')">
              <div class="box-heading auto_synonyms" title="Pokud je k dispozici dostatečný vzorek dat pro daný termín, zobrazí se termíny vyskytující se v podobných kontextech. &#13; Existující termíny se zobrazují modrou barvou. Seznam je uspořádán sestupně podle míry podobnosti.">
                <i class="fa fa-clone"></i> Termíny vyskytující se ve stejných kontextech</div>
              <div class="box-inside">                
              </div>
            </xsl:if>
          
        </div>
      </div>
  

      <div class="row row-pad">
        <div class="col-lg-12 concordance">
          <div class="box-heading">
            <span class="glyphicon glyphicon-align-center"></span> Příklady užití
          </div>        

          <div class="hide-this box-inside">

            <div class="link-box"></div>            
            </div>

          <div class="show-hide-button" onclick="showHide($(this))">
            <span class="glyphicon glyphicon-chevron-down"></span> <a id="conc-button" target="concordance">
                  <xsl:attribute name="href">corpname=tecu_Czech&amp;structs=g,s&amp;q=q<xsl:for-each select="terms/term[@lang='cs']">(<xsl:call-template name="split"><xsl:with-param name="text" select="."/></xsl:call-template>)<xsl:if test="position()!=last()">|</xsl:if></xsl:for-each></xsl:attribute>Zobrazit</a>
          </div>
        </div>
      </div>

      <xsl:apply-templates select="notes[note]"/>

    </div>


  <xsl:if test="$perm='w'">

   <div class="row edit-row">
    <div class="col-lg-12">   
    <div id="edit_div">
      <div class="row">
          <div class="col-lg-12">

            <xsl:if test="$type='del'">
              <button type="button" onclick="undelete_id('{@id}'); return false;" class="btn btn-primary pull-right">
                <span class="glyphicon glyphicon-repeat"></span>
                 Obnovit heslo
              </button>
            </xsl:if>

            <xsl:if test="$type!='del'">             
                  <a href="#" onclick="edit_id(); return false;" type="button" class="btn btn-primary pull-right">
                    <span class="glyphicon glyphicon-pencil"></span>
                    Editovat
                  </a>
            </xsl:if>
          </div>
      </div>

    </div>
    <div id="edit_form" style="display: none; font-size: 90%;">

        <div id="required_message" class="alert alert-danger" role="alert">
          <span class="glyphicon glyphicon-exclamation-sign"></span>
          Vyplňte prosím všechny povinné prvky (vyznačené červeně).
        </div>

        <fieldset>

          <div class="row">
            <div class="col-lg-12 term-info"> 
              <div class="box-heading">
                <span clas="glyphicon glyphicon-cog"></span> Heslo
              </div>
              
              <div class="box-inside">
                <div class="row">
                    <div class="col-lg-2">
                      <span class="term-info-text">ID</span>
                      <input type="text" id="edit_form_id" class="form-control">
                          <xsl:attribute name="disabled">true</xsl:attribute>
                      </input>
                    </div>
                    <div class="col-lg-2">
                      <span class="term-info-text">Jazyk</span>
                      <input type="text" id="edit_form_lang" class="form-control">
                            <xsl:attribute name="disabled">true</xsl:attribute>
                      </input>
                    </div>  
                    <div class="col-lg-2">
                      <span class="term-info-text">Index</span>
                      <input type="text" id="edit_form_index" class="form-control">
                          <xsl:attribute name="disabled">true</xsl:attribute>
                      </input>
                    </div>
                    <div class="col-lg-6">
                      <span class="term-info-text">Status</span>
                      <select id="edit_form_status" class="form-control">
                        <option value="1">terminologický</option>
                        <option value="2">používaný</option>
                        <option value="3">používaný nezpracovaný</option>
                        <option value="4">odmítnutný</option>
                        <option value="5">automatický</option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </fieldset>

        <fieldset>

          <div class="row row-pad">
            <div class="col-lg-12 refs"> 
              <div class="box-heading">
                <i class="fa fa-file-text"></i> Termíny
              </div>
              
              <div class="box-inside">
                <table id="edit_form_terms_table">
                    <thead>
                        <tr>
                            <th class="term-info-text">Číslo</th>
                            <th class="term-info-text">Jazyk</th>
                            <th class="term-info-text">Termín</th>
                            <th class="term-info-text">Ref.</th>
                            <th class="term-info-text">Morf.</th>
                            <th class="term-info-text">Zkratka</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="ttmpl" style="display: none;">
                            <td style="width: 7%">
                              <input title="Číslo" type="number" min="1" max="10" class="num required form-control" /></td>
                            <td style="width: 10%">
                              <select title="Jazyk" class="lng required form-control">
                                <option value="cs">cs</option>
                                <option value="sk">sk</option>
                                <option value="en">en</option>
                                <option value="de">de</option>
                                <option value="fr">fr</option>
                                <option value="ru">ru</option>
                              </select>
                            </td>
                            <td style="width:25 %">
                              <input title="Termín" type="text" name="term" class="required form-control" />
                            </td>
                            <td style="width: 7%">
                              <input title="Odkaz na termín" type="number" name="refno" min="1" max="10" class="form-control" />
                            </td>
                            <td style="width: 7%">
                              <input title="Morfologie" type="text" name="morf" class="form-control" />
                            </td>
                            <td style="width: 7%">
                              <input title="Termín je zkratkou" type="checkbox" name="is_abbr" class="form-control input-abbr" />
                            </td>                        
                            <td style="width: 7%">
                              <button type="button" class="btn btn-default" onclick="$(this).closest('tr').remove(); return false;">
                                <span class="glyphicon glyphicon-remove"></span>
                              </button>
                            </td>
                            <td class="autotr" style="display: none; width: 32%">
                              <input type="button" class="btn btn-default" onclick="trans_cand(this); return false;" value="Návrh" />
                            </td>
                        </tr>
                    </tbody>
                </table>

                <button type="button" class="btn btn-default" onclick="add_input(this); return false;">
                  <span class="glyphicon glyphicon-plus"></span>
                </button>
              
              </div>
            </div>
          </div>
                        
        </fieldset>

        <fieldset>            
          <div class="row row-pad">
            <div class="col-lg-12 refs"> 
              <div class="box-heading">
                <i class="fa fa-file-text"></i> Definice
              </div>
              
              <div class="box-inside">
                <table id="edit_form_defs_table">
                  <thead>
                      <tr>
                          <th>Číslo</th>
                          <th>Znění definice</th>
                      </tr>
                  </thead>
                  <tbody>
                    <tr class="ttmpl" style="display: none;">
                      <td>
                        <input title="číslo definice" type="number" min="1" max="10" class="num required form-control" /></td>
                      <td class="td-textarea">
                        <textarea title="znění definice" rows="2" class="required form-control"></textarea></td>
                      <td>
                        <button type="button" class="btn btn-default" onclick="$(this).closest('tr').remove(); return false;">
                            <span class="glyphicon glyphicon-remove"></span>
                        </button>
                      </td>
                    </tr>
                  </tbody>
                </table> 

                <button type="button" class="btn btn-default" onclick="add_input(this); return false;">
                  <span class="glyphicon glyphicon-plus"></span>
                </button>
              </div>
            </div>
          </div>

        </fieldset>

        <fieldset>
          <div class="row row-pad">
            <div class="col-lg-12 refs"> 
              <div class="box-heading">
                <i class="fa fa-map-signs"></i> Obory
              </div>
              
              <div class="box-inside">
                <table id="edit_form_domains_table">
                  <tbody>
                    <tr class="ttmpl" style="display: none;">
                      <td>
                          <button class="btn btn-default not-button">
                            <i class="fa fa-circle"></i>
                          </button>
                      </td>
                      <td class="td-input">
                        <input title="doména" type="text" class="required form-control" />
                      </td>
                      <td>
                        <button type="button" class="btn btn-default" onclick="$(this).closest('tr').remove(); return false;">
                            <span class="glyphicon glyphicon-remove"></span>
                        </button>
                      </td>
                    </tr>
                  </tbody>
                </table>            

                <button type="button" class="btn btn-default" onclick="add_input(this); return false;">
                  <span class="glyphicon glyphicon-plus"></span>
                </button>
            </div>
          </div>
        </div>
         
        </fieldset>

        <fieldset>

          <div class="row row-pad">
            <div class="col-lg-12 refs">      
               <div class="box-heading">
                <span class="glyphicon glyphicon-book"> </span>
                 Reference
               </div>
               <div class="box-inside">                    
                
                <table id="edit_form_refs_table">
                  <tbody>                    
                    <tr class="ttmpl" style="display: none;">
                      <td>
                        <button class="btn btn-default not-button">
                          <i class="fa fa-circle"></i>
                        </button>
                      </td>
                      <td class="td-input">
                        <input title="reference, odkaz" type="text" class="required form-control"/>
                      </td>
                      <td>
                        <button type="button" class="btn btn-default" onclick="$(this).closest('tr').remove(); return false;">
                          <span class="glyphicon glyphicon-remove"></span>
                        </button>
                      </td>
                    </tr>
                  </tbody>
                </table>

                 <button type="button" class="btn btn-default" onclick="add_input(this); return false;">
                    <span class="glyphicon glyphicon-plus"></span>
                  </button>

              </div>      
            </div>
          </div>
        </fieldset>

        <fieldset>
          <div class="row row-pad">
            <div class="col-lg-12">
              <div class="links">
                <div class="box-heading">
                  <i class="fa fa-mouse-pointer"></i> Odkazy
                </div>
                            
                <div class="box-inside">
                  <div class="row">
                    
                    <div class="col-lg-12 ">
                      <div class="box-heading second-level">
                        <i class="fa fa-thumb-tack"></i>
                        Nadřazené pojmy
                      </div>
                      <div class="box-inside second-level">

                        <table id="edit_form_hyper_table">
                          <thead>
                              <tr>
                                  <th>ID</th>
                                  <th>Hyperonymum</th>
                                  <th></th>
                                  <th>Návrhy</th>
                              </tr>
                          </thead>
                          <tbody>
                              <tr class="ttmpl" style="display: none;">
                                <td style="width: 20%">
                                    <input title="Zadání hyperonyma pomocí ID" type="text" size="3" class="hyper-search required form-control disabled">
                                    </input>
                                </td>
                                <td style="width: 50%">
                                    <input type="text" title="Zadání hyperonyma textem" class="link form-control td-input" name="h" autocomplete="off" />                                    
                                 </td>
                                <td>
                                  <button type="button" class="btn btn-default" onclick="$(this).closest('tr').remove(); return false;" >
                                    <span class="glyphicon glyphicon-remove"></span>
                                  </button>
                                </td>
                              </tr>
                          </tbody>
                      </table>
                      
                      <button type="button" class="btn btn-default" onclick="add_input(this); hyper_search(); return false;">
                        <span class="glyphicon glyphicon-plus"></span>
                      </button>

                      </div>
                    </div>

                    <div class="col-lg-12 row-pad">
                      <div class="box-heading second-level">
                        <i class="fa fa-thumb-tack"></i>
                        Také
                      </div>
                      <div class="box-inside second-level">                      
                        
                        <table id="edit_form_alsos_table">
                          <thead>
                              <tr>
                                  <th>Jazyk</th>
                                  <th>Také</th>
                              </tr>
                          </thead>
                          <tbody>
                              <tr class="ttmpl" style="display: none;">
                                  <td style="width: 5%">
                                      <select title="dvoumístný kód jazyk" class="lng required form-control lng-code">
                                          <option value="cs">cs</option>
                                          <option value="sk">sk</option>
                                          <option value="en">en</option>
                                          <option value="de">de</option>
                                          <option value="fr">fr</option>
                                          <option value="ru">ru</option>
                                      </select>
                                  </td>
                                  <td style="width: 100%">
                                    <input title="také / also" type="text" class="also required link form-control td-input" />                                    
                                  </td>
                                  <td>
                                    <button type="button" class="btn btn-default" onclick="$(this).closest('tr').remove(); return false;" >
                                      <span class="glyphicon glyphicon-remove"></span>
                                    </button>
                                  </td>
                              </tr>
                          </tbody>
                      </table>            
                      <button type="button" class="btn btn-default" onclick="add_input(this); return false;">
                        <span class="glyphicon glyphicon-plus"></span>
                      </button>

                      </div>
                    </div>

                    <div class="col-lg-12 row-pad">
                      <div class="box-heading second-level">
                        <i class="fa fa-thumb-tack"></i>
                        Viz
                      </div>
                      <div class="box-inside second-level"> 

                        <table id="edit_form_sees_table">
                          <thead>
                              <tr>
                                  <th>Jazyk</th>
                                  <th>Viz</th>
                              </tr>
                          </thead>
                            <tbody>
                                <tr class="ttmpl" style="display: none;">
                                    <td style="width: 5%">
                                        <select title="dvoumístný kód jazyk" class="lng required form-control lng-code">
                                            <option value="cs">cs</option>
                                            <option value="sk">sk</option>
                                            <option value="en">en</option>
                                            <option value="de">de</option>
                                            <option value="fr">fr</option>
                                            <option value="ru">ru</option>
                                        </select>
                                    </td>
                                    <td style="width: 100%">
                                      <input title="viz / see" type="text" class="see required link form-control td-input" />
                                      </td>
                                    <td>
                                      <button type="button" class="btn btn-default" onclick="$(this).closest('tr').remove(); return false;" >
                                        <span class="glyphicon glyphicon-remove"></span>
                                      </button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                         
                        <button type="button" class="btn btn-default" onclick="add_input(this); return false;">
                          <span class="glyphicon glyphicon-plus"></span>
                        </button>

                      </div>
                    </div>

                  </div>  
                </div>
              </div>
            </div>
          </div>
        </fieldset>

        <fieldset>
          <div class="row row-pad">
            <div class="col-lg-12 edit-mode">
            <div class="box-heading">
              <span class="glyphicon glyphicon-comment"></span>
              Komentář k úpravě</div>

            <div class="box-inside">
              <div class="input-group comment">
                <input title="komentář" type="text" id="note_comment" class="form-control" />                
              </div>              
            </div>
            
          </div>
        </div>
        </fieldset>

        <div class="buttons">
          <button type="button" onclick="save_id(); return false;" class="btn btn-success">
            <span class="glyphicon glyphicon-ok"></span> Uložit
          </button>
          
          <button type="button" onclick="storno(); window.scrollTo(0, 0); return false;" class="btn btn-default">
            <span class="glyphicon glyphicon-remove"></span> Neukládat
          </button>
          
          <button type="button" onclick="delete_id(); return false;" class="btn btn-danger pull-right">
            <span class="glyphicon glyphicon-trash"></span> Smazat
          </button>          
        </div>

      </div>
    </div>
  </div>
    </xsl:if>
</form>
</xsl:template>

<xsl:template match="terms" mode="main">
   <xsl:for-each select="term[@lang='cs']">
     <xsl:value-of select="."/>
     <xsl:choose>
       <xsl:when test="position() != last()">, </xsl:when>
     </xsl:choose>
   </xsl:for-each>
 </xsl:template>

<xsl:template match="terms" mode="trans">  
  <div class="box-heading box-heading-language">
      <i class="fa fa-language"></i> Překlady
  </div>
  <div class="box-inside"> 
    <xsl:apply-templates select="term[@lang!='cs']" mode="trans"/>
  </div>
</xsl:template>

<xsl:template match="term" mode="trans">
  <div class="tr box-line">
    <!--<span class="badge badge-trans"><xsl:value-of select="@lang"/></span><xsl:text> </xsl:text><a target="concordance" href="https://makaria07.fi.muni.cz/bonito/run.cgi/first?corpname=tecu_{@lang}&amp;iquery={./text()}" title="Otevřít v korpusu"><xsl:value-of select="."/></a>-->
    <span class="badge badge-trans"><xsl:value-of select="@lang"/></span><xsl:text> </xsl:text><xsl:value-of select="."/>
     <xsl:if test="@morf!=''"> 
      (<xsl:value-of select="@morf"/>)
    </xsl:if>
  </div>
</xsl:template>

<xsl:template match="domains">
   <div class="box-heading">
    <i class="fa fa-map-signs"></i> Obory
   </div>
   <div class="box-inside">
    <ul>
      <xsl:apply-templates select="dom"/>
    </ul>
  </div>
</xsl:template>

<xsl:template match="defs">   
  <ol>
    <xsl:apply-templates select="def"/>
  </ol>
</xsl:template>

<xsl:template match="refs">
  <div class="row row-pad">
    <div class="col-lg-12 refs">      
       <div class="box-heading">
        <span class="glyphicon glyphicon-book"> </span>
         Reference
       </div>
       <div class="box-inside">
        <ul>
          <xsl:apply-templates select="ref"/>
        </ul>          
      </div>      
    </div>
  </div>
</xsl:template>

<xsl:template match="notes">
  <div class="row row-pad">
    <div class="col-lg-12 notes">
      <div class="notes box">
         
         <div class="box-heading">
          <i class="fa fa-history"></i> 
          Historie editace
         </div>

         <div id="notes-detail" class="hide-this box-inside">
           <xsl:apply-templates select="note"/>
        </div>

        <div class="show-hide-button" onclick="showHide($(this))">
          <span class="glyphicon glyphicon-chevron-down"></span> Zobrazit
        </div>

      </div>
    </div>
  </div>
</xsl:template>

<xsl:template match="alsos">
   <div class="col-lg-6 alsos row-pad">
       <div class="box-heading second-level">                
          <i class="fa fa-thumb-tack"></i>
          Také
       </div>
       <div class="box-inside second-level label-center">

       <xsl:for-each select="also">
           <xsl:if test="not(@entry-id) or @entry-id=/entry/@id">
            <div class="also label-box label-nonactive"> 
             <xsl:value-of select="."/>
            </div>
           </xsl:if>
           
           <xsl:if test="@entry-id!='' and @entry-id!=/entry/@id">
             <a class="label-box label-active" href="tezaurus.html?id={@entry-id}&amp;opentree=true"><xsl:value-of select="."/></a>            
           </xsl:if>
         
       </xsl:for-each>

      </div>
   </div>
</xsl:template>

<xsl:template match="sees">
    <div class="col-lg-6 sees row-pad">
	<div class="box-heading second-level">
            <i class="fa fa-thumb-tack"></i>
            Viz
         </div>
         <div class="box-inside second-level label-center">
            
            <xsl:for-each select="see">              
                <xsl:if test="not(@entry-id) or @entry-id=/entry/@id">
                  <span class="see label-box label-nonactive"> 
                    <xsl:value-of select="."/>
                  </span>
                </xsl:if>
            
              <xsl:if test="@entry-id!='' and @entry-id!=/entry/@id">
                  <span class="see label-box label-active"> 
                  <a href="tezaurus.html?id={@entry-id}&amp;opentree=true"><xsl:value-of select="."/></a>
                </span>
              </xsl:if>
            
            </xsl:for-each>
        </div>
    </div>
</xsl:template>

<xsl:template match="hyper">
         <a class="hyperlink" href="#" onclick="get_id_html(this); return false;" data-id="{@id}"><xsl:value-of select="@term"/></a>
     <xsl:choose><xsl:when test="position() != last()">; </xsl:when></xsl:choose>
</xsl:template>

<xsl:template match="path_node">  
    <a class="hyperlink label-box label-active" data-id="{@id}" href="tezaurus.html?id={@id}&amp;opentree=true"><xsl:value-of select="@term"/></a>
    <xsl:choose>
      <xsl:when test="position() != last()">        
        <span class="label-triangle glyphicon glyphicon-triangle-bottom"></span>
      </xsl:when>
    </xsl:choose>
</xsl:template>

<xsl:template match="def">
    <li class="def">
      <xsl:apply-templates/>
    </li>
</xsl:template>

<xsl:template match="def/a">
  <a href="{@href}&amp;opentree=true"><xsl:value-of select="text()"/></a>
</xsl:template>

<xsl:template match="dom">  
  <li class="dom box-line"><xsl:value-of select="."/></li>  
</xsl:template>

<xsl:template match="ref">
  <li><xsl:value-of select="."/></li>

</xsl:template>

<xsl:template match="note">
  <div class="row">
    
    <div class="col-lg-2">
      <span class="note label label-default pull-right">
        <span class="glyphicon glyphicon-user"> </span> <xsl:value-of select="@author"/>        
      </span>
    </div>

    <div class="col-lg-3">
      <span class="glyphicon glyphicon-time"> </span> <xsl:value-of select="@time"/>
    </div>

    <div class="col-lg-7">
      <span class="glyphicon glyphicon-comment"> </span> <xsl:value-of select="."/>
    </div>

  </div>
  
</xsl:template>


<xsl:template name="split">
  <xsl:param name="text" select="."/>
  <xsl:param name="separator" select="' '"/>
  <xsl:choose>
    <xsl:when test="not(contains($text, $separator))">
      [lc="<xsl:value-of select="normalize-space($text)"/>"|lemma_lc="<xsl:value-of select="normalize-space($text)"/>"]
    </xsl:when>
    <xsl:otherwise>
      [lc="<xsl:value-of select="normalize-space(substring-before($text, $separator))"/>"|lemma_lc="<xsl:value-of select="normalize-space(substring-before($text, $separator))"/>"]
      <xsl:call-template name="split">
        <xsl:with-param name="text" select="substring-after($text, $separator)"/>
      </xsl:call-template>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>


</xsl:stylesheet>
