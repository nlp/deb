<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns="http://www.w3c.org/1999/xhtml">
	<xsl:output encoding="utf-8" method="html" indent="yes" version="4.0"/>

<xsl:param name="base"/>
<xsl:template match="entry">
<html xmlns="http://www.w3c.org/1999/xhtml">
<head><title><xsl:value-of select="hw"/>, edit</title>
    <base href="$base" />
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <script type="text/javascript;e4x=1" src="files/surnames.js"><xsl:text> </xsl:text></script>
  <script type="text/javascript" src="files/global.js"><xsl:text> </xsl:text></script>
  <!--<script type="text/javascript" src="files/cm/js/codemirror.js"><xsl:text> </xsl:text></script>
  <script type="text/javascript" src="files/cm/js/mirrorframe.js"><xsl:text> </xsl:text></script>-->
  <script type="text/javascript" src="files/sm/subModal/submodalsource.js"><xsl:text> </xsl:text></script>
  <script type="text/javascript" src="files/textedit.js"><xsl:text> </xsl:text></script>
  <script type="text/javascript">
    id['var'] = <xsl:value-of select="count(var)+2"/>;
    id['ref'] = <xsl:value-of select="count(ref)+2"/>;
    id['ex'] =  <xsl:value-of select="count(//ex)+2"/>;
    id['cit'] =  <xsl:value-of select="count(//cit)+2"/>;
    id['sense'] =  <xsl:value-of select="count(sense)+2"/>;
    //ta_i = <xsl:value-of select="count(//ex|//cit)"/>;
    complete_time = '<xsl:value-of select="@complete_time"/>';
    /*subModal setting*/
    var selchar_object = null;
    var selsrc_object = null;
    setPopUpLoadingPage('/files/popload.html');
    /*ikony z famfamfam.com*/
  </script>
  <link rel="stylesheet" href="files/surned.css" type="text/css"/>
  <link rel="stylesheet" href="files/sm/subModal/submodal.css" type="text/css"/>
</head>
<body onload="if (event.target == document) edit_onload();" >
  <button onclick="alert(get_xml())" label="test">test</button><xsl:text> </xsl:text>
  <button onclick="back_list=false;save_xml();reload_main();" disabled="true" id="but_save" label="Save">Save</button><xsl:text> </xsl:text>
  <button onclick="back_list=false;save_xml(true);reload_main();" disabled="true" id="but_save_back" label="Save &amp; close">Save &amp; close</button><xsl:text> </xsl:text>
  <button onclick="reload_main();close_window();" label="Close">Close</button><xsl:text> </xsl:text>
 
<p>ID: <input type="text" disabled="true" id="entry_id"/>
<span id="modified" style="display:none;font-style:italic;"> *modified*</span>
<!--<input type="hidden" id="entry_id" value="{hw}"/>-->
          <label>quarantine <input type="checkbox" id="quarantine">
            <xsl:if test="@quarantine='true'"><xsl:attribute name="checked">true</xsl:attribute></xsl:if>
          </input></label>
          <xsl:text> | </xsl:text><xsl:text> </xsl:text><label>complete <input type="checkbox" id="complete" onclick="update()">
            <xsl:if test="@complete='true'"><xsl:attribute name="checked">true</xsl:attribute></xsl:if>
          </input></label>
          <xsl:text> | </xsl:text><label>me <input type="checkbox" id="is_me" onclick="update()">
            <xsl:if test="@me='true'"><xsl:attribute name="checked">true</xsl:attribute></xsl:if>
          </input></label>
          <xsl:text> | </xsl:text><label>xref <input type="checkbox" id="is_xref" onclick="update()">
            <xsl:if test="@xref='true'"><xsl:attribute name="checked">true</xsl:attribute></xsl:if>
          </input></label>
          <input type="hidden" id="is_new" value="{@new}"/>
          <!--<xsl:if test="//sense">
          <button onclick="change_to_ref()" id="button_to_ref">change to x-ref</button>
          <button onclick="change_to_full()" id="button_to_full" style="display:none">change to main</button>
          </xsl:if>
          <xsl:if test="not(//sense)">
          <button onclick="change_to_ref()" id="button_to_ref" style="display:none">change to x-ref</button>
          <button onclick="change_to_full()" id="button_to_full">change to main</button>
          </xsl:if>-->
</p>
<table id="data_table">
<tr><th>headword</th>
<td>
<input type="text" id="hw" class="word" value="{hw}" onchange="update();check_hw(this);"/><xsl:text> </xsl:text>
          GB freq.
<input type="text" id="freq_uk" class="freq" value="{freq_uk}" onchange="update()"/><xsl:text> </xsl:text>
Main location, 1881
<input type="text" id="heartland_gb" class="heartland" value="{heartland_gb}" onchange="update()"/><xsl:text> </xsl:text>
          </td>
</tr>
<tr>
<th>source</th>
<td><input type="text" id="source" class="heartland" value="{source}" onchange="update()"/>
          Ire freq.
<input type="text" id="freq_roi" class="freq" value="{freq_roi}" onchange="update()"/><xsl:text> </xsl:text>
Ire distrib.
<input type="text" id="heartland_ir" class="heartland" value="{heartland_ir}" onchange="update()"/><xsl:text> </xsl:text>
</td>
</tr>
         <tr><th>variants</th>
<td>
        <div id="box_var">
          <xsl:for-each select="me">
            <xsl:if test="position()=1">
              <span id="hbvar1">
                <input type="text" class="var" id="var1" info="varinfo1" value="{.}" onchange="update(); var_info(this);"/>
                <button onclick="add('box_var', 'var', 'bef_var');">+</button><xsl:text> </xsl:text>
                <!--<button onclick="go_var(this.previousSibling.previousSibling.previousSibling);">go</button>-->
                <button onclick="open_entry(this.previousSibling.previousSibling.previousSibling.value);">go</button>
                <button onclick="move_var_down(this.parentNode);">v</button>
                &#160;&#160;&#160;&#160;<span class="varinfo" id="varinfo1"></span>
              </span> 
            </xsl:if>
            <xsl:if test="position()>1">
              <span id="hbvar{position()}">
                <br/>
                <input type="text" class="var" id="var{position()}" info="varinfo{position()}" value="{.}" onchange="update(); var_info(this);"/>
                <button onclick="remove('hbvar{position()}');">-</button><xsl:text> </xsl:text>
                <!--<button onclick="go_var(this.previousSibling.previousSibling.previousSibling);">go</button>-->
                <button onclick="open_entry(this.previousSibling.previousSibling.previousSibling.value);">go</button>
                <button onclick="move_var_up(this.parentNode);">^</button>
                <button onclick="move_var_down(this.parentNode);">v</button>
                <span id="varinfo{position()}" class="varinfo"></span>
              </span> 
            </xsl:if>
            </xsl:for-each>
            <xsl:if test="count(me)=0">
              <span id="hbvar1">
                <span id="hbvar1"></span>
                <input type="text" class="var" id="var1" info="varinfo1" value="" onchange="update(); var_info(this);"/>
                <button onclick="add('box_var', 'var', 'bef_var');">+</button><xsl:text> </xsl:text>
                <!--<button onclick="go_var(this.previousSibling.previousSibling.previousSibling);">go</button>-->
                <button onclick="open_entry(this.previousSibling.previousSibling.previousSibling.value);">go</button>
                <button onclick="move_var_up(this.parentNode);">^</button>
                <button onclick="move_var_down(this.parentNode);">v</button>
                &#160;&#160;&#160;&#160;<span class="varinfo" id="varinfo1"></span>
              </span> 
            </xsl:if>
          <box hidden="true" id="bef_var"/>
          </div>
          </td>
        </tr>
        <tr id="before_ref"><td></td></tr>

      <xsl:if test="count(xref)>0">
         <tr id="row_ref"><th>references</th>
<td>
        <div id="box_ref">
          <xsl:for-each select="xref">
            <xsl:if test="position()=1">
              <span id="hbref1">
                <input type="text" class="ref" id="ref1" value="{.}" onchange="update()"/>
                <button onclick="add('box_ref', 'ref', 'bef_ref');">+</button><xsl:text> </xsl:text>
                <button onclick="go_var(this.previousSibling.previousSibling.previousSibling);">go</button>
              </span> 
            </xsl:if>
            <xsl:if test="position()>1">
              <span id="hbref{position()}">
                <br/><input type="text" class="ref" id="ref{position()}" value="{.}" onchange="update()"/>
                <button onclick="remove('hbref{position()}');">-</button><xsl:text> </xsl:text>
                <button onclick="go_var(this.previousSibling.previousSibling.previousSibling);">go</button>
              </span> 
            </xsl:if>
            </xsl:for-each>
            <xsl:if test="count(xref)=0">
              <span id="hbref1">
                <input type="text" class="ref" id="ref1" value="" onchange="update()"/>
                <button onclick="add('box_ref', 'ref', 'bef_ref');">+</button><xsl:text> </xsl:text>
                <button onclick="go_var(this.previousSibling.previousSibling.previousSibling);">go</button>
              </span> 
            </xsl:if>
          <box hidden="true" id="bef_ref"/>
          </div>
          </td>
        </tr>
      </xsl:if>
        <tr id="before_sense"><td></td></tr>
      <xsl:apply-templates select="sense">
        <xsl:sort select="s"/>
      </xsl:apply-templates>
      <xsl:if test="count(sense)=0">
           <tr><td></td><td> <button onclick="add_data(document.getElementById('before_sense'), 'sense', '')">+ sense</button></td></tr>
      </xsl:if>
      <tr>
        <th>comment<br/>(not for<br/> publication)</th>
        <td><textarea id="comment" onchange="update();" class="comment"><xsl:value-of select="comment"/></textarea></td>
      </tr>
      <tr class="dataend"/>
    </table>




</body>
</html>
</xsl:template>

<xsl:template match="cit|ex">
  <xsl:param name="in_sense"/>
  <xsl:param name="sense_pos"/>
  <tr id="{$in_sense}{$sense_pos}citexrow{position()}" data="{$in_sense}citex" element="{name()}">
  <th>
    <xsl:choose>
      <xsl:when test="name()='cit'">
        early bearers
      </xsl:when>
      <xsl:when test="name()='ex'">
        expl.
      </xsl:when>
    </xsl:choose>
  </th>
  <td>
  <table>
  <tr><td>
  <div class="textarea_updiv">
    <div class="textarea_div">
      <textarea class="{name()}" element="{name()}" id="{$in_sense}{$sense_pos}citex{position()}" data="{$in_sense}{$sense_pos}citex" onchange="update()"><xsl:value-of select="."/></textarea>
    </div>
    </div>
      </td>
      <td valign="top">
        <xsl:if test="name()='ex' or name()='cit'">
          <button onclick="add_data(document.getElementById('{$in_sense}{$sense_pos}citexrow{position()}'), 'ex', '{$in_sense}', '{$sense_pos}')">+ expl.</button>
          <br/><button onclick="edit_sources(document.getElementById('{$in_sense}{$sense_pos}citexrow{position()}'))">edit src</button>
        </xsl:if>
        <br />
        <button onclick="add_data(document.getElementById('{$in_sense}{$sense_pos}citexrow{position()}'), 'cit', '{$in_sense}', '{$sense_pos}')">+ e.b.</button>
        <xsl:if test="$in_sense != 'sense'">
          <br /><button onclick="add_data(document.getElementById('citexrow{position()}'), 'sense', '', '')">+ sense</button>
        </xsl:if>
        </td>
      </tr></table>
    </td>
  <script type="text/javascript">
  make_textedit(document.getElementById('<xsl:value-of select="$in_sense"/><xsl:value-of select="$sense_pos"/>citex<xsl:value-of select="position()"/>'));
    ta_i++;
  </script>
  <!--<script type="text/javascript">
    var textarea = document.getElementById('<xsl:value-of select="$in_sense"/><xsl:value-of select="$sense_pos"/>citex<xsl:value-of select="position()"/>');
    textareas[ta_i] = new MirrorFrame(CodeMirror.replace(textarea), {
      parserfile: 'parsexml.js',
      stylesheet: '/files/cm/css/xmlcolors.css',
      path: '/files/cm/js/',
      content: textarea.value,
      width: '40em',
      height: '6em',
      disableSpellcheck: false,
    });

    document.getElementById('<xsl:value-of select="$in_sense"/><xsl:value-of select="$sense_pos"/>citexrow<xsl:value-of select="position()"/>').setAttribute('editor', ta_i);
    ta_i++;
    /*http://marijn.haverbeke.nl/codemirror/index.html*/
  </script>-->
  </tr>
</xsl:template>

<xsl:template match="sense">
  <tr class="sense" id="citexrow{position()}" data="citex" element="sense">
  <td colspan="2">
  <table>
  <tr><th>nr.</th><td><input type="text" data="nr" class="sense_nr" value="{s}" />
            <button onclick="add_data(document.getElementById('citexrow{position()}'), 'ex', '')">+ expl.</button>
            <!--<button onclick="add_data(document.getElementById('citexrow{position()}'), 'cit', '')">+ e.b.</button>-->
            <button onclick="add_data(document.getElementById('citexrow{position()}'), 'sense', '')">+ sense</button>
          </td></tr>
        <xsl:apply-templates select="cit|ex">
          <xsl:sort select="name()"/>
          <xsl:with-param name="in_sense">sense</xsl:with-param>
          <xsl:with-param name="sense_pos"><xsl:value-of select="position()"/></xsl:with-param>
        </xsl:apply-templates>
        <!--<xsl:apply-templates select="ex">
          <xsl:with-param name="in_sense">sense</xsl:with-param>
          <xsl:with-param name="sense_pos"><xsl:value-of select="position()"/></xsl:with-param>
        </xsl:apply-templates>-->
        <tr class="dataend"/>
    </table>
    </td>
  </tr>
</xsl:template>

</xsl:stylesheet>

