<?xml version='1.0' encoding='utf-8'?>
<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>
<xsl:template match='/entry'>
<html><head><meta content='text/html; charset=utf-8' http-equiv='Content-Type'/><title>PLIN: <xsl:value-of select="hs"/> (DEBWrite)</title></head><body>
<xsl:apply-templates/></body></html></xsl:template>

<xsl:template match="hs"><span class="hs" style="color:blue"><b><xsl:apply-templates/></b></span>,</xsl:template>
<xsl:template match="gram"><span class="gram"> <xsl:apply-templates/></span></xsl:template>
<xsl:template match="sd"><span class="sd"> <font size="1"><xsl:apply-templates/></font></span><br/></xsl:template>
<xsl:template match="rod"><span class="rod"><b>rod: </b><xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="deleni"><span class="deleni"><b>dělení: </b><xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="vysl"><span class="vysl"><b>výslovnost: </b><xsl:apply-templates/></span><br/></xsl:template>


<xsl:template match="vyzn">
<xsl:if test="count(preceding-sibling::vyzn)=0"><b>význam: </b></xsl:if>
<div class="vyzn"> <xsl:apply-templates/></div></xsl:template>
<xsl:template match="cislo"><span class="cislo" style="color:blue"> <xsl:apply-templates/></span></xsl:template>
<xsl:template match="def"><span class="def"> <xsl:apply-templates/></span></xsl:template>
<xsl:template match="pr"><span class="pr" style="color:green"> <i><small><xsl:apply-templates/></small></i></span><br/></xsl:template>
<xsl:template match="syn"><span class="syn"><b>synonymum: </b><xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="ant"><span class="ant"><b>antonymum: </b><xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="reference"><span class="reference"><b>reference: </b><xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="koment"><span class="koment"><b>komentář: </b><xsl:apply-templates/></span><br/></xsl:template>
</xsl:stylesheet>
