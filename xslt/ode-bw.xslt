<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="utf-8" method="xml"
  doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
  doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
  />

<xsl:template match="se">
  <html>
     <head>
      <title>ODE: <xsl:value-of select="hg/hw"/> <xsl:value-of select="hg/hm"/></title>
      <style type="text/css">
      body {
      background: #ffffff;
      color: #000000;
      }
      .head {
      font-size: 150%;
      }
      .sans {
        font-family: sans-serif;
      }
      .caps {
        font-variant: small-caps;
      }
      .indent {
        margin-left: 1em;
      }
      .nlp {
        margin-left: 1em;
        font-size: 70%;
      }
      .innernlp {
        display: none;
      }
      .normal {
        font-weight: normal;
      }
      </style>
     </head>
     <body>
       <p><xsl:apply-templates/></p>
       <hr />
       <p><small>Oxford Dictionary of English, revised ed. (2005)</small></p>
     </body>
  </html>
</xsl:template>

<xsl:template match="hg">
  <b class="red head"><xsl:value-of select="hw"/><sup><xsl:value-of select="hm"/></sup></b><br/>
  <xsl:apply-templates select="*[not(self::hw or self::hm)]"/>
</xsl:template>

<xsl:template match="vgz">
  <xsl:apply-templates/><br/>
</xsl:template>

<xsl:template match="i">
  <i><xsl:apply-templates/></i>
</xsl:template>

<xsl:template match="su">
  <sup><xsl:apply-templates/></sup>
</xsl:template>

<xsl:template match="f">
  <i><xsl:apply-templates/></i>
</xsl:template>

<xsl:template match="v">
  <b><xsl:value-of select="text()"/></b>
</xsl:template>

<xsl:template match="vz">
  <i><xsl:value-of select="text()"/></i>
</xsl:template>

<xsl:template match="r">
  <xsl:value-of select="text()"/>
</xsl:template>

<xsl:template match="fg">
  <span class="blue">[<xsl:apply-templates/>]</span>
</xsl:template>

<xsl:template match="ge">
  <span class="blue">[<xsl:apply-templates/>]</span>
</xsl:template>

<xsl:template match="sj">
  <span class="green">[<xsl:apply-templates/>]</span>
</xsl:template>

<xsl:template match="reg">
  <span class="green">[<xsl:apply-templates/>]</span>
</xsl:template>

<xsl:template match="xg">
  <span class="darkred">[<xsl:apply-templates/>]</span>
</xsl:template>

<xsl:template match="gg">
  <span class="darkred">[<xsl:apply-templates/>]</span>
</xsl:template>

<xsl:template match="infg">
  <span class="darkred">[<xsl:apply-templates/>]</span>
</xsl:template>

<xsl:template match="inf">
  <b><xsl:value-of select="text()"/></b>
</xsl:template>

<xsl:template match="xu">
  <i><xsl:value-of select="text()"/></i>
</xsl:template>

<xsl:template match="ex">
  <i><xsl:apply-templates/></i>
</xsl:template>

<xsl:template match="exb">
  <b><xsl:apply-templates/></b>
</xsl:template>

<xsl:template match="ps">
  &#9654; <b><xsl:value-of select="text()"/></b>
</xsl:template>

<xsl:template match="xr">
  <xsl:if test="hm">
    <a href="ode?action=getdoc&amp;tr=ode&amp;id={x}%20*{hm}"><xsl:value-of select="x"/><xsl:text> </xsl:text><xsl:value-of select="hm"/></a>
    <xsl:apply-templates select="*[not(self::x or self::hm)]"/>
  </xsl:if>
  <xsl:if test="not(hm)">
    <xsl:apply-templates/>
  </xsl:if>
</xsl:template>

<xsl:template match="x">
  <a href="ode?action=getdoc&amp;tr=ode&amp;id={text()}"><xsl:value-of select="."/></a>
</xsl:template>

<xsl:template match="pr">
  <xsl:apply-templates/><br/>
</xsl:template>

<xsl:template match="s1">
  <xsl:apply-templates/><br/>
</xsl:template>

<xsl:template match="s2">
  <b class="red"><xsl:value-of select="@num"/>. </b><xsl:apply-templates/><br/>
</xsl:template>

<xsl:template match="df">
  <xsl:apply-templates/>
</xsl:template>

<xsl:template match="nlp">
  <br/><span class="nlp"><b onclick="var style2 = this.nextSibling.style; style2.display = (style2.display)? '':'inline';">NLP:</b><span class="innernlp"><xsl:text> </xsl:text><xsl:apply-templates/></span></span>
</xsl:template>

<xsl:template match="morph">
  <xsl:apply-templates/>
</xsl:template>

<xsl:template match="mu">
  {<xsl:value-of select="@sy"/><xsl:text> </xsl:text>
  <xsl:value-of select="@v"/><xsl:text> </xsl:text>
  <xsl:value-of select="@gen"/>}<xsl:text> </xsl:text>
  <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ph">
  /<xsl:apply-templates/>/
</xsl:template>

<xsl:template match="ss">
  [<xsl:apply-templates/>]
</xsl:template>

<xsl:template match="cat">
  <i><xsl:apply-templates/></i>
</xsl:template>

<xsl:template match="ms">
  <br/>&#9642; <xsl:apply-templates/>
</xsl:template>

<xsl:template match="drv">
  <br/>&#8212;<span class="caps blue">Derivatives</span><xsl:text> </xsl:text><xsl:apply-templates/>
</xsl:template>

<xsl:template match="drv//ps">
  <span class="sans"><xsl:value-of select="text()"/></span>
</xsl:template>

<xsl:template match="l">
  <b><xsl:apply-templates/></b>
</xsl:template>

<xsl:template match="etym">
  <br/>&#8212;<span class="caps green">Origin</span><xsl:text> </xsl:text><xsl:apply-templates/>
</xsl:template>

<xsl:template match="phr">
  <br/>&#8212;<span class="caps green">Phrases</span><xsl:text> </xsl:text><xsl:apply-templates/>
</xsl:template>

<xsl:template match="ff">
  <i><xsl:apply-templates/></i>
</xsl:template>

<xsl:template match="phr/sub">
  <xsl:if test="position()!=1"><br/></xsl:if>
  <xsl:apply-templates/>
</xsl:template>

<xsl:template match="tr">
  &#8216;<xsl:apply-templates/>&#8217;
</xsl:template>

<xsl:template match="vg">
  <span class="normal">(<xsl:apply-templates/>)</span>
</xsl:template>

<xsl:template match="pv">
  <br/>&#8212;<span class="caps blue">Phrasal verbs</span><xsl:text> </xsl:text><xsl:apply-templates/>
</xsl:template>
<xsl:template match="pv/sub">
  <xsl:apply-templates/><br/>
</xsl:template>

<xsl:template match="key|cn"/>

</xsl:stylesheet>

