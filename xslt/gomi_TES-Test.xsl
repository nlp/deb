<?xml version='1.0' encoding='utf-8'?>
<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'><xsl:template match='/entry'><html><head><meta content='text/html[-semicolon-] charset=utf-8' http-equiv='Content-Type'/><title>Testovací slovník: <xsl:value-of select="hw"/> (DEBWrite)</title><style type="text/css">.hw{color: blue}
.type_container {border: 1px solid #000}
.type_container {background-color:#eee}
</style>
</head>
<body><h1><xsl:value-of select="hw"/></h1>
<xsl:apply-templates/></body></html></xsl:template>
<xsl:template name="file"><xsl:param name="file_element"/><br/><xsl:if test="starts-with($file_element/@mime, 'image')"><img src="/files/gomi_TES/{.}" style="max-width:400px"/></xsl:if><xsl:if test="starts-with($file_element/@mime, 'audio')"><audio src="/files/gomi_TES/{.}" controls="true"/></xsl:if><xsl:if test="starts-with($file_element/@mime, 'video')"><video src="/files/gomi_TES/{.}" controls="true" style="max-width:400px"/></xsl:if><br/></xsl:template>

<xsl:template match="hw"><span class="hw type_text">Termín: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="pos"><span class="pos type_select">Slovní druh: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="gram"><span class="gram type_text">Gramatické informace: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="pron"><span class="pron type_text">Výslovnost: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="meaning"><div class="meaning type_container">Význam: <xsl:apply-templates/></div><br/></xsl:template>
<xsl:template match="def"><span class="def type_textarea">Definice: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="usage"><span class="usage type_textarea">Příklad užití: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="synonym"><span class="synonym type_text">Synonyma: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="antonym"><span class="antonym type_text">Antonyma: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="reference"><span class="reference type_textarea">Odkazy: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="comment"><span class="comment type_textarea">Komentáře: <xsl:apply-templates/></span><br/></xsl:template>
</xsl:stylesheet>
