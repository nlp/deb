<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="utf-8" method="xml"
  doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
  doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
  />

<xsl:template match="ar">
  <html>
    <head>
      <title><xsl:value-of select="name"/></title>
      <style type="text/css">
      body {
      background: #ffffff;
      color: #000000;
      }
      /*.red {
      color: #ff0000;
      }
      .green {
      color: #007700;
      }
      .darkred {
      color: #770000;
      }
      .darkblue {
      color: #000088;
      }*/
      .head {
      font-size: 150%;
      }
      </style>
    </head>
    <body>
      <p><xsl:apply-templates/></p>
      <a id="map"/><img src="{//map/@src}"/>
      <hr/>
      <p><small><a href="http://www.cia.gov/cia/publications/factbook/">CIA World Factbook</a></small></p>
    </body>
  </html>
</xsl:template>

<xsl:template match="name">
  <h1 class="red head"><xsl:value-of select="."/></h1>
  <div style="position: absolute; top: 10px; right: 10px;">
  <img src="{//flag/@src}"/>
  </div>
  <a href="#map">map</a>
</xsl:template>

<xsl:template match="section">
  <h2 class="green"><xsl:value-of select="@title"/></h2>
  <table>
  <xsl:apply-templates/>
  </table>
</xsl:template>

<xsl:template match="field">
  <tr><td width="20%" valign="top" align="right"><b class="darkblue"><xsl:value-of select="@title"/></b></td>
  <td><xsl:apply-templates/></td></tr>
</xsl:template>
<xsl:template match="br"><br/></xsl:template>

</xsl:stylesheet>

