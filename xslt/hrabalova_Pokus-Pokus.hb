<html><head><meta content='text/html[-semicolon-] charset=utf-8' http-equiv='Content-Type'/><title>Pokus: {{entry.hw}} (DEBWrite)</title></head><body>
<h1>{{entry.hw}}</h1>
<span><b>headword</b>: {{entry.hw}}</span><br/>
<span><b>part of speech</b>: {{entry.pos}}</span><br/>
<span><b>translation</b>: {{entry.translation}}</span><br/>
<span><b>category</b>: {{entry.category}}</span><br/>
<span><b>meaning</b>: {{entry.meaning}}</span><br/>
<span><b>usage example1</b>: {{entry.usage1}}</span><br/>
<span><b>usage example2</b>: {{entry.usage2}}</span><br/>
<span><b>usage example3</b>: {{entry.usage3}}</span><br/>
<span><b>link</b>: {{entry.link}}</span><br/>
<span><b>link2</b>: {{entry.link2}}</span><br/>
</body></html>
