<html><head><meta content='text/html[-semicolon-] charset=utf-8' http-equiv='Content-Type'/><title>aaaa: {{entry.reference}} (DEBWrite)</title></head><body>
<h1>{{entry.reference}}</h1>
<span><b>headword</b>: {{entry.hw}}</span><br/>
<span><b>part of speech</b>: {{entry.pos}}</span><br/>
<span><b>grammar info</b>: {{entry.gram}}</span><br/>
<span><b>pronunciation</b>: {{entry.pron}}</span><br/>
<span><b>hyphenation</b>: {{entry.hyph}}</span><br/>
<div style="border: 1px solid #000">
<span><b>meaning nr</b>: {{entry.nr}}</span><br/>
<span><b>domain</b>: {{entry.domain}}</span><br/>
<span><b>definition</b>: {{entry.def}}</span><br/>
<span><b>usage example</b>: {{entry.usage}}</span><br/>
</div>
<span><b>synonym</b>: {{entry.synonym}}</span><br/>
<div style="border: 1px solid #000">
<span><b>ta</b>: {{entry.ta}}</span><br/>
<div style="border: 1px solid #000">
<span><b>antonym</b>: {{entry.antonym}}</span><br/>
<span><b>selectdd</b>: {{entry.selectd}}</span><br/>
</div>
</div>
<div style="border: 1px solid #000">
<span><b>comments</b>: {{entry.comment}}</span><br/>
<span><b>references</b>: {{entry.reference}}</span><br/>
</div>
<span><b>onefile</b>: {{entry.onefile}}</span><br/>
<span><b>morefile</b>: {{entry.morefile}}</span><br/>
</body></html>
