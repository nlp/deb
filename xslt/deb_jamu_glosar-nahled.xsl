<?xml version='1.0' encoding='utf-8'?>
<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'><xsl:template match='/entry'><html><head><meta content='text/html[-semicolon-] charset=utf-8' http-equiv='Content-Type'/><title>JAMU glosář: <xsl:value-of select="word_cz"/> (DEBWrite)</title><style type="text/css">.word_cz{color: blue}
.type_container {border: 1px solid #000}
.type_container {background-color:#eee}
.src {color:green}
.domain {color:orange}
</style>
</head>
<body><h1><xsl:value-of select="word_cz"/></h1>
<xsl:apply-templates/></body></html></xsl:template>
<xsl:template name="file"><xsl:param name="file_element"/><br/><xsl:if test="starts-with($file_element/@mime, 'image')"><img src="/files/deb_jamu_glosar/{.}" style="max-width:400px"/></xsl:if><xsl:if test="starts-with($file_element/@mime, 'audio')"><audio src="/files/deb_jamu_glosar/{.}" controls="true"/></xsl:if><xsl:if test="starts-with($file_element/@mime, 'video')"><video src="/files/deb_jamu_glosar/{.}" controls="true" style="max-width:400px"/></xsl:if><br/></xsl:template>

<xsl:template match="word_cz"><span class="word_cz type_text">česky: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="word_en"><span class="word_en type_text">anglicky: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="word_de"><span class="word_de type_text">německy: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="word_fr"><span class="word_fr type_text">francouzsky: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="word_sp"><span class="word_sp type_text">španělsky: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="word_ru"><span class="word_ru type_text">rusky: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="var"><span class="var type_text">varianta: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="styl"><span class="styl type_select">stylový příznak: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="domain"><span class="domain type_select">obor: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="def"><span class="def type_textarea">definice: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="def_en"><span class="def_en type_textarea">definice anglicky: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="examples"><div class="examples type_container">příklady: <xsl:apply-templates/></div><br/></xsl:template>
<xsl:template match="example"><span class="example type_textarea">příklad: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="reference"><span class="reference type_textarea">reference: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="comment"><span class="comment type_textarea">poznámka: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="files"><div class="files type_container">soubory: <xsl:apply-templates/></div><br/></xsl:template>
<xsl:template match="file"><span class="file type_file">soubor: <xsl:call-template name="file"><xsl:with-param name="file_element" select="."/></xsl:call-template></span><br/></xsl:template>
<xsl:template match="src"><span class="src type_select">zdroj: <xsl:apply-templates/></span><br/></xsl:template>
</xsl:stylesheet>
