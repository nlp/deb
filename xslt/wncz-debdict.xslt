<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="utf-8"/>

<xsl:param name="slovnik"/>
<xsl:param name="servername"/>
<xsl:param name="default"/>
<xsl:param name="package"/>

<xsl:template match="part">
  <xsl:if test="@id != //SYNSET/ID">
    <xsl:variable name="id"><xsl:value-of select="@id"/></xsl:variable>
    <xsl:if test="$servername='_'">
      <a href="/{$slovnik}?action=getdoc&amp;id={@id}"><xsl:value-of select="@text"/></a>
    </xsl:if>
    <xsl:if test="$servername!='_'">
      <a href="{$servername}/{$slovnik}?action=getdoc&amp;id={@id}"><xsl:value-of select="@text"/></a>
    </xsl:if>
    <xsl:if test="//meta/path/part[@up=$id]/@id != //SYNSET/ID">&gt;</xsl:if>
    <xsl:apply-templates select="//meta/path/part[@up=$id]"/>
  </xsl:if>
</xsl:template>

<xsl:template match="SYNSET">
  <html>
    <head>
        <style type="text/css">
            body {
                background-color: white;
                /* font-family: Trebuchet, Verdana, Helvetica, sans-serif;
                font-size: 12px; */
                line-height: 170%;
            }
            a {
		cursor: pointer;
                text-decoration: underline;
                color: black;
            } 
            .blue { color: blue; }
            .red { color: red; }
            .green { color: green; }
            .green2 { color: #18bd3b; }
            .dark_red { color: #ac1e36; }
            .stamp { color: gray; }

    .path {
      background-color: #fff3ae;
    }
		</style>
    </head>
    <body style="background-color: white;" id="body">
      <xsl:if test="//meta/path/part[@up='koren']/@id != //SYNSET/ID">
      <span class="path">
        <xsl:apply-templates select="//meta/path/part[@up='koren']"/>
      </span>
      <br/>
    </xsl:if>
      <span class="blue">Synonyma: </span>
      <xsl:for-each select="SYNONYM/LITERAL">
        <xsl:value-of select="text()"/>:<xsl:value-of select="@sense"/>
	<xsl:if test="position()!=last()"><xsl:text>, </xsl:text></xsl:if>
      </xsl:for-each>
      <br/>
      <xsl:apply-templates select="DEF"/>
      <xsl:for-each select="USAGE">
        <span class="blue">Příklad použití: </span>
        <span><xsl:value-of select="."/></span><br/>
      </xsl:for-each>
      <xsl:apply-templates select="DOMAIN"/>
      <xsl:for-each select="SUMO">
        <span class="blue">SUMO/MILO: </span>
        <span class="green"><xsl:value-of select="TYPE"/><xsl:text> </xsl:text>
            <xsl:value-of select="text()"/>
        </span><br/>
      </xsl:for-each>
      <span class="blue">Slovní druh: </span>
      <span class="red">
        <xsl:choose>
          <xsl:when test="POS='n'">
            podstatné jméno
          </xsl:when>
          <xsl:when test="POS='a'">
            přídavné jméno
          </xsl:when>
          <xsl:when test="POS='v'">
            sloveso
          </xsl:when>
          <xsl:when test="POS='b'">
            příslovce
          </xsl:when>
        </xsl:choose>
        (<xsl:value-of select="POS"/>)
        </span>
      <xsl:text> </xsl:text><br/>
      <span class="blue">ID: </span>
      <span class="red"><xsl:value-of select="ID"/></span>
      <xsl:text> </xsl:text><br/>
      <xsl:apply-templates select="BCS"/>
      <br/>
      <span class="blue">Sémantické relace:</span><br/>
      <xsl:for-each select="ILR">
        <span class="blue">--&gt;&gt;</span><xsl:text> </xsl:text>
	<xsl:choose>
	<xsl:when test="@type='hypernym'">
        <span class="green">[<xsl:value-of select="@type"/>]</span>
	</xsl:when>
	<xsl:when test="@type='holo_member' or @type='holo_portion' or @type='holo_part'">
        <span class="blue">[<xsl:value-of select="@type"/>]</span>
	</xsl:when>
	<xsl:when test="@type='derived'">
        <span class="green2">[<xsl:value-of select="@type"/>]</span>
	</xsl:when>
	<xsl:otherwise>
        <span class="dark_red">[<xsl:value-of select="@type"/>]</span>
	</xsl:otherwise>
	</xsl:choose>
	<xsl:text> </xsl:text>
  <xsl:if test="@literal!=''">
    <span class="red">(<xsl:value-of select="@literal"/>)</span><xsl:text> </xsl:text>
  </xsl:if>
  <xsl:if test="$servername='_'">
    <a href="/{$slovnik}?action=getdoc&amp;id={@link}"><xsl:value-of select="text()"/></a>
  </xsl:if>
  <xsl:if test="$servername!='_'">
    <a href="{$servername}/{$slovnik}?action=getdoc&amp;id={@link}"><xsl:value-of select="text()"/></a>
  </xsl:if>
	<br/>
      </xsl:for-each>
      <br/>
      <xsl:if test="count(RILR[@type='hypernym'])>0">
        hyponym: <xsl:value-of select="count(RILR[@type='hypernym'])"/><br/>
      </xsl:if>
      <xsl:for-each select="RILR">
        <xsl:sort select="@type"/>
        <span class="blue">&lt;&lt;--</span><xsl:text> </xsl:text>
	<xsl:choose>
	<xsl:when test="@type='hypernym'">
        <span class="green">[hyponym]</span>
	</xsl:when>
	<xsl:when test="@type='holo_member'">
        <span class="blue">[mero_member]</span>
	</xsl:when>
	<xsl:when test="@type='holo_portion'">
        <span class="blue">[mero_portion]</span>
	</xsl:when>
	<xsl:when test="@type='holo_part'">
        <span class="blue">[mero_part]</span>
	</xsl:when>
	<xsl:when test="@type='derived'">
        <span class="green2">[<xsl:value-of select="@type"/>]</span>
	</xsl:when>
	<xsl:when test="@type='category_domain'">
        <span class="dark_red">[category_member]</span>
	</xsl:when>
	<xsl:when test="@type='region_domain'">
        <span class="dark_red">[region_member]</span>
	</xsl:when>
	<xsl:when test="@type='usage_domain'">
        <span class="dark_red">[usage_member]</span>
	</xsl:when>
	<xsl:otherwise>
        <span class="dark_red">[<xsl:value-of select="@type"/>]</span>
	</xsl:otherwise>
	</xsl:choose>
	<xsl:text> </xsl:text>
  <xsl:if test="@literal!=''">
    <span class="red">(<xsl:value-of select="@literal"/>)</span><xsl:text> </xsl:text>
  </xsl:if>
  <xsl:if test="$servername='_'">
    <a href="/{$slovnik}?action=getdoc&amp;id={@link}"><xsl:value-of select="text()"/></a>
  </xsl:if>
  <xsl:if test="$servername='_'">
    <a href="{$servername}/{$slovnik}?action=getdoc&amp;id={@link}"><xsl:value-of select="text()"/></a>
  </xsl:if>
	<br/>
      </xsl:for-each>

      <br/>
      <xsl:for-each select="VALENCY">
        <span class="blue">Valenční rámec: </span>
        <ul>
        <xsl:for-each select="FRAME">
          <li><span class="dark_red"><xsl:value-of select="."/></span></li>
        </xsl:for-each>
        </ul>
      </xsl:for-each>

      <br/>
      <div class="stamp">
       <span>Poslední úprava: </span>
       <span><xsl:value-of select="STAMP"/></span><xsl:text> / </xsl:text>
       <span><xsl:value-of select="CREATED"/></span>
      </div>
     
    </body>
  </html>
</xsl:template>

<xsl:template match="BCS">
  <xsl:if test="text()!=''">
      <span class="blue">BCS: </span>
      <span class="red"><xsl:value-of select="."/></span>
    </xsl:if>
</xsl:template>

<xsl:template match="DEF">
  <xsl:if test="text()!=''">
      <span class="blue">Definice: </span>
      <span><xsl:value-of select="."/></span>
      <br/>
    </xsl:if>
</xsl:template>
<xsl:template match="DOMAIN">
  <xsl:if test="text()!=''">
      <span class="blue">Doména: </span>
      <span class="green"><xsl:value-of select="."/></span><br/>
    </xsl:if>
</xsl:template>

</xsl:stylesheet>

