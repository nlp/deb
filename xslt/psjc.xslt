<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="utf-8" method="xml"
  doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
  doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
  />

<xsl:template match="h">
  <html>
     <head>
      <title>PSJČ: <xsl:value-of select="h"/></title>
      <style type="text/css">
      body {
        background: #ffffff;
        color: #000000;
      }
      p { font: 12pt serif; margin-bottom: 12pt }
      .Heslo { font: bold 12pt sans-serif;color: #ff0000;  }
      .Char { font: italic 10pt serif;  color: #007700; }
      .Tvar {  }
      .Sep { font-weight: bold;  }
      .Pram { color: #000088; }
      .Vyzn { font-style: italic; color: #770000; }
      .Dokl {  }
      .Gram { color: #007700;  }
      .Vysl {color: #007700 }
      .Zav { font-weight: bold; color: navy;  }
      .Homo { font-weight: bold;  }
      .Odkazovaci { font-style: italic; }
      .DokladDI { font-size: 9pt;  vertical-align: sub }
      .DokladHI { font-size: 9pt;  vertical-align: super }
      .VyznamDI { font: italic 9pt serif;  vertical-align: sub }
      .VyznamHI { font: italic 9pt serif;  vertical-align: super }
      .Podheslo { background: #FF6600 }
      .Odkaz { color: black;  }
      .Odkaz a {font: 12pt sans-serif;}
      .Odkaz a:hover {text-decoration: none}
      .Valence { font: italic 9pt serif; border: 1.5pt solid red;  vertical-align: middle }
      .Konekt { color: white; }
      .Slovnispojeni { text-decoration: underline }
      </style>
     </head>
     <body>
       <p><xsl:apply-templates/></p>
       <hr />
       <p><small>Příruční slovník jazyka českého</small></p>
     </body>
  </html>
</xsl:template>

<xsl:template match="Cil"></xsl:template>
<xsl:template match="Odkaz"><span class="{name()}"><a href="psjc?action=getdoc&amp;id={text()}&amp;tr=psjc"><xsl:value-of select="."/></a></span></xsl:template>
<xsl:template match="Vyzn"><span class="Vyzn"><xsl:value-of select="."/></span></xsl:template>
<xsl:template match="Heslo"><span class="Heslo"><xsl:value-of select="text()"/></span>
<xsl:if test="$freq!=''">
  <xsl:text> (frekvence v korpusu: </xsl:text><xsl:value-of select="$freq"/>)
</xsl:if>
</xsl:template>
<xsl:template match="*"><span class="{name()}"><xsl:value-of select="."/></span></xsl:template>

</xsl:stylesheet>

