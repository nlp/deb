<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="utf-8" method="xml" omit-xml-declaration="yes"/>

<xsl:variable name="smallcase" select="'abcdefghijklmnopqrstuvwxyz'" />
<xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'" />


<xsl:template match="entry">
      <div class="definice-wrap" id="page-content">
        <h1><xsl:value-of select="head/lemma"/></h1>
        <xsl:for-each select="vars">
          <h2><xsl:value-of select="."/></h2>
        </xsl:for-each>
         <xsl:for-each select="trans">
           <h2>
             <xsl:value-of select="."/>
             <xsl:if test="@lang!=''">
               <xsl:choose>
                 <xsl:when test="@lang='br'"><xsl:text> </xsl:text><em class="small">(BrE)</em></xsl:when>
                 <xsl:when test="@lang='am'"><xsl:text> </xsl:text><em class="small">(AmE)</em></xsl:when>
                 <xsl:when test="@lang='cze'"><xsl:text> </xsl:text><em class="small">(Cze)</em></xsl:when>
                 <xsl:when test="@lang='un'"><xsl:text> </xsl:text><em class="small">(UN)</em></xsl:when>
                 <xsl:when test="@lang='eng'"><xsl:text> </xsl:text><em class="small">(Eng)</em></xsl:when>
                 <xsl:when test="@lang='archaic'"><xsl:text> </xsl:text><em class="small">(archaic)</em></xsl:when>
               </xsl:choose>
             </xsl:if>
           </h2>
         </xsl:for-each>

         <xsl:for-each select="ency/sense[def!='' or example!='']">
           <div class="box">
             <p class="definice">
               <xsl:if test="def/@num!=''"><xsl:value-of select="def/@num"/>: </xsl:if>
               <xsl:if test="def/@gb='true'">GB </xsl:if>
               <xsl:if test="def/@eng='true'">ENG </xsl:if>
               <xsl:if test="def/@un='true'">UN </xsl:if>
               <xsl:if test="def/@cz='true'">CZ </xsl:if>
               <xsl:if test="def/@us='true'">US </xsl:if>
               <xsl:value-of select="def"/>
             </p>
             <xsl:if test="example!=''"><p class="enc_example">[<xsl:value-of select="example"/>]</p></xsl:if>
             <xsl:if test="count(trans)>0">
               <ul>
                 <xsl:for-each select="trans">
                 <li>
                   <xsl:value-of select="."/><xsl:text> </xsl:text>
                   <xsl:if test="@lang!=''">
                     <xsl:choose>
                       <xsl:when test="@lang='br'">(<em class="small">BrE</em>)</xsl:when>
                       <xsl:when test="@lang='am'">(<em class="small">AmE</em>)</xsl:when>
                       <xsl:when test="@lang='cze'">(<em class="small">Cze</em>)</xsl:when>
                       <xsl:when test="@lang='un'">(<em class="small">UN</em>)</xsl:when>
                       <xsl:when test="@lang='eng'">(<em class="small">Eng</em>)</xsl:when>
                       <xsl:when test="@lang='archaic'">(<em class="small">archaic</em>)</xsl:when>
                     </xsl:choose>
                   </xsl:if>
                   </li>
                 </xsl:for-each>
               </ul>
             </xsl:if>
             <xsl:if test="count(semlink)>0">
               <ul>
                 <xsl:apply-templates select="semlink">
                   <xsl:sort select="@type" lang="cs"/>
                   <xsl:sort select="." lang="cs"/>
                 </xsl:apply-templates>
               </ul>
             </xsl:if>
             <xsl:if test="count(kolok)>0">
               <ul class="kolokace">
                 <xsl:apply-templates select="kolok">
                   <xsl:sort select="kolok" lang="cs"/>
                 </xsl:apply-templates>
               </ul>
             </xsl:if>
             <xsl:if test="count(deriv)>0">
               <ul class="odkazy"> 
                 <xsl:apply-templates select="deriv">
                   <xsl:sort select="." lang="cs"/>
                 </xsl:apply-templates>
               </ul>
             </xsl:if>
             <xsl:if test="count(refer)>0">
               <ul class="odkazy"> 
                 <xsl:apply-templates select="refer">
                   <xsl:sort select="." lang="cs"/>
                 </xsl:apply-templates>
               </ul>
             </xsl:if>
             <xsl:apply-templates select="cite"/>
           </div>
         </xsl:for-each>

         <xsl:if test="ency/example!=''">
           <p>[<xsl:value-of select="ency/example"/>]</p>
         </xsl:if>
         <xsl:if test="count(semlink)>0">
           <ul>
             <xsl:apply-templates select="semlink[@type='syn']">
               <xsl:sort select="@type" lang="cs"/>
               <xsl:sort select="." lang="cs"/>
             </xsl:apply-templates>
             <xsl:apply-templates select="semlink[@type='ant']">
               <xsl:sort select="@type" lang="cs"/>
               <xsl:sort select="." lang="cs"/>
             </xsl:apply-templates>
             <xsl:apply-templates select="semlink[@type='hyper']">
               <xsl:sort select="@type" lang="cs"/>
               <xsl:sort select="." lang="cs"/>
             </xsl:apply-templates>
             <xsl:apply-templates select="semlink[@type='hypo']">
               <xsl:sort select="@type" lang="cs"/>
               <xsl:sort select="." lang="cs"/>
             </xsl:apply-templates>
           </ul>
         </xsl:if>
         <xsl:if test="count(kolok)>0">
           <ul class="kolokace">
             <xsl:apply-templates select="kolok">
               <xsl:sort select="kolok" lang="cs"/>
             </xsl:apply-templates>
           </ul>
         </xsl:if>

         <xsl:if test="count(deriv)>0">
           <ul class="odkazy">
             <xsl:apply-templates select="deriv">
               <xsl:sort select="." lang="cs"/>
             </xsl:apply-templates>
           </ul>
         </xsl:if>
         <xsl:if test="count(refer)>0">
           <ul class="odkazy"> 
             <xsl:apply-templates select="refer">
               <xsl:sort select="." lang="cs"/>
             </xsl:apply-templates>
           </ul>
         </xsl:if>
         <xsl:apply-templates select="cite"/>

         <h3>Kategorie:</h3>
         <p class="kategorie"><xsl:apply-templates select="head/klas"/></p>

      </div><!-- detail-wrap -->
</xsl:template>

<xsl:template match="semlink">
  <li><xsl:value-of select="translate(@type, $smallcase, $uppercase)"/><xsl:text> </xsl:text><xsl:value-of select="."/></li>
</xsl:template>

<xsl:template match="kolok">
  <li>
    <strong><xsl:value-of select="kolok"/></strong><xsl:text> </xsl:text>
    <xsl:if test="trans!=''"> = <xsl:value-of select="trans"/><xsl:text> </xsl:text></xsl:if>
  <xsl:if test="ref!=''">
    <xsl:if test="ref/@link!=''">
       <a onclick="load_document('{ref/@link}')"><xsl:value-of select="ref"/></a>
    </xsl:if>
    <xsl:if test="not(ref/@link) or ref/@link=''">
       <xsl:value-of select="ref"/>
    </xsl:if>
  </xsl:if>
  </li>
</xsl:template>

<xsl:template match="refer">
  <xsl:if test="@link!=''">
    <li><a onclick="load_document('{@link}')"><xsl:value-of select="."/></a></li>
  </xsl:if>
  <xsl:if test="not(@link) or @link=''">
    <li><xsl:value-of select="."/></li>
  </xsl:if>
</xsl:template>

<xsl:template match="deriv">
  DER <strong><xsl:value-of select="deriv"/></strong><xsl:text> = </xsl:text>
  <em><xsl:value-of select="full"/></em>
</xsl:template>

<xsl:template match="cite">
  <p><i><xsl:value-of select="."/></i></p>
</xsl:template>

<xsl:template match="klas">
  <xsl:choose>
    <xsl:when test=".='vytvar'">Výtvarná kultura</xsl:when>
    <xsl:when test=".='odev'">Lidový oděv</xsl:when>
    <xsl:when test=".='obchod'">Obchod</xsl:when>
    <xsl:when test=".='doprava'">Doprava a komunikace</xsl:when>
    <xsl:when test=".='stavit'">Lidové stavitelství</xsl:when>
    <xsl:when test=".='rukod'">Rukodělná výroba</xsl:when>
    <xsl:when test=".='zemed'">Zemědělství a obživa</xsl:when>
    <xsl:when test=".='zakladni'">Základní pojmy</xsl:when>
    <xsl:when test=".='pramen'">Pramenné studium, výzkum a publikace</xsl:when>
    <xsl:when test=".='zak_pojmy'">Základní pojmy z etnologie a antropologie</xsl:when>
    <xsl:when test=".='geo_nazvy'">Geografické názvy</xsl:when>
    <xsl:when test=".='nazvy_inst'">Názvy odborných institucí</xsl:when>
    <xsl:when test=".='nehmotna'">Nehmotná kultura</xsl:when>
    <xsl:when test=".='ob_tradice'">Obyčejová tradice</xsl:when>
    <xsl:when test=".='vyrocni_ob'">Výroční obyčeje</xsl:when>
    <xsl:when test=".='rodinne_ob'">Rodinné obyčeje</xsl:when>
    <xsl:when test=".='sp_vztahy'">Společenské vztahy</xsl:when>
    <xsl:when test=".='obci'">Vztahy v obci</xsl:when>
    <xsl:when test=".='rod_vztah'">Rodinné vztahy</xsl:when>
    <xsl:when test=".='lid_relig'">Lidová religiozita</xsl:when>
    <xsl:when test=".='nabozenstvi'">Náboženství</xsl:when>
    <xsl:when test=".='magie'">Pověrečné představy, magie, kulty</xsl:when>
    <xsl:when test=".='folklor'">Folklor</xsl:when>
    <xsl:when test=".='f_slovesny'">Slovesný folklor</xsl:when>
    <xsl:when test=".='f_hudebni'">Hudební folklor</xsl:when>
    <xsl:when test=".='f_tanecni'">Taneční folklor</xsl:when>
    <xsl:when test=".='lid_divadlo'">Lidové divadlo</xsl:when>
    <xsl:when test=".='f_detsky'">Dětský folklor</xsl:when>
    <xsl:when test=".='hmotna'">Hmotná kultura</xsl:when>
  </xsl:choose>
  <xsl:if test="position()!=last()"><xsl:text> | </xsl:text></xsl:if>
</xsl:template>


</xsl:stylesheet>

