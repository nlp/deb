<?xml version='1.0' encoding='utf-8'?>
<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'><xsl:template match='/entry'><html><head><meta content='text/html[-semicolon-] charset=utf-8' http-equiv='Content-Type'/><title>Betlémy: <xsl:value-of select="null"/> (DEBWrite)</title><style type="text/css">.null{color: blue}
.type_container {border: 1px solid #000}
.type_container {background-color:#eee}
</style>
</head>
<body><h1><xsl:value-of select="null"/></h1>
<xsl:apply-templates/></body></html></xsl:template>
<xsl:template name="file"><xsl:param name="file_element"/><br/><xsl:if test="starts-with($file_element/@mime, 'image')"><img src="/files/cernorizec2_betlem/{.}" style="max-width:400px"/></xsl:if><xsl:if test="starts-with($file_element/@mime, 'audio')"><audio src="/files/cernorizec2_betlem/{.}" controls="true"/></xsl:if><xsl:if test="starts-with($file_element/@mime, 'video')"><video src="/files/cernorizec2_betlem/{.}" controls="true" style="max-width:400px"/></xsl:if><br/></xsl:template>

</xsl:stylesheet>
