<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output encoding="utf-8" />
  
  <xsl:param name="pat_id"/>

  <xsl:template match="pattern_set">
    <xsl:apply-templates select="//pattern[@id=$pat_id]"/>
  </xsl:template>
  
  <xsl:template match="pattern">
    <html>
      <head>
        <base href="http://deb.fi.muni.cz/pdev/"/>
        <title>PDEV: <xsl:value-of select="//verb_stem"/>:<xsl:value-of select="@num"/></title>
        <style type="text/css">
        body {
          background: #ffffff;
          color: #000000;
        }
        .red {
          color: #ff0000;
        }
        h1, .green {
          color: #007700;
        }
        .darkred {
          color: #770000;
        }
        h3, .darkblue {
          color: #000088;
        }
        .head {
          font-size: 150%;
        }
        .indent {
          margin: 20px;
        }
        </style>
      </head>
      <body>
      <h1>Pattern <xsl:value-of select="//verb_stem"/><xsl:text> </xsl:text><xsl:value-of select="@num"/></h1>

      <p class="text"><a target="_blank" href="doc.php?action=corpora&amp;id={$entry_id}&amp;num={@num}">Examples from corpus</a> /
        <a target="_blank" href="doc.php?action=corpora&amp;id={$entry_id}&amp;num={@num}.e">Show exploitations</a>
      </p>

      <!--subject-->
      <p><b>Subject: </b>
        <xsl:apply-templates select="template/subject/argspec" mode="subj"/>
      </p>
      <xsl:if test="template/subject/alternation">
        <p><b>Subject alternation: </b>
          <xsl:for-each select="template/subject/alternation">
            <xsl:apply-templates select="argspec" mode="subj"/>
            <br/>
          </xsl:for-each>
        </p>
      </xsl:if>
      
      <p><b>Verb form: </b>  <xsl:value-of select="template/verb_form"/></p>

      <!--object-->
      <p><b>Object: </b>
        <xsl:if test="template/@object_none='true'">
          <xsl:text>[NO OBJ]</xsl:text>
        </xsl:if>
        <xsl:if test="not(template/@object_none='true')">
          <xsl:apply-templates select="template/object/argspec" mode="obj"/>
        </xsl:if>
      </p> 
      <xsl:if test="template/object/alternation">
        <p><b>Object alternation: </b>
          <xsl:for-each select="template/object/alternation">
            <xsl:apply-templates select="argspec" mode="obj"/>
            <br/>
          </xsl:for-each>
        </p>
      </xsl:if>
      <xsl:if test="template/object/indirect">
        <p><b>Indir. object: </b>
          <xsl:for-each select="template/object/indirect">
            <xsl:apply-templates select="argspec" mode="obj"/>
            <br/>
          </xsl:for-each>
        </p>
      </xsl:if>
      
      <xsl:if test="template/oclausals/clausal or template/oclausals/@to='true' or template/oclausals/@ing='true' or template/oclausals/@that='true' or template/oclausals/@wh='true' or template/oclausals/@quote='true'">
        <p><b>Clausal obj.: </b>
          <i>
          <xsl:if test="template/oclausals/@to='true'"> to/INF [V]</xsl:if>
          <xsl:if test="template/oclausals/@ing='true'"> -ING</xsl:if>
          <xsl:if test="template/oclausals/@that='true'"> that [CLAUSE]</xsl:if>
          <xsl:if test="template/oclausals/@wh='true'"> WH- [CLAUSE]</xsl:if>
          <xsl:if test="template/oclausals/@quote='true'"> [QUOTE]</xsl:if>
          </i>
          <xsl:for-each select="template/oclausals/clausal">
            <xsl:apply-templates select="argspec" mode="obj"/>            
          </xsl:for-each>
        </p>
      </xsl:if>
      
      <!--adverbial-->
      <xsl:if test="template/adverbial/alternation or template/adverbial/@func!='' or template/@adverbial_none='true'">
        <p><b>Adverbial: </b>
          <xsl:if test="template/@adverbial_none='true'">[NO ADVL]</xsl:if>
          <xsl:if test="template/@adverbial_none!='true'">
            <xsl:for-each select="template/adverbial">
              <xsl:apply-templates select="."/>
            </xsl:for-each>
          </xsl:if>
        </p>
      </xsl:if>

      <xsl:if test="template/clausals/clausal or template/clausals/@to='true' or template/clausals/@ing='true' or template/clausals/@that='true' or template/clausals/@wh='true' or template/clausals/@quote='true'">
        <p><b>Clausal: </b>
          <i>
          <xsl:if test="template/clausals/@to='true'"> to/INF [V]</xsl:if>
          <xsl:if test="template/clausals/@ing='true'"> -ING</xsl:if>
          <xsl:if test="template/clausals/@that='true'"> that [CLAUSE]</xsl:if>
          <xsl:if test="template/clausals/@wh='true'"> WH- [CLAUSE]</xsl:if>
          <xsl:if test="template/clausals/@quote='true'"> [QUOTE]</xsl:if>
          </i>
          <xsl:for-each select="template/clausals/clausal">
            <xsl:apply-templates select="argspec" mode="obj"/>            
          </xsl:for-each>
        </p>
      </xsl:if>

        <!--complement-->
        <xsl:if test="template/comp">
          <p><b>Complement: </b>
          <xsl:for-each select="template/comp">
            (<xsl:value-of select="@type"/>)
            <xsl:value-of select="text()"/>
            <br/>
          </xsl:for-each>
          </p>
        </xsl:if>
        
        <p><b>Primary implicature: </b> <xsl:value-of select="primary_implicature"/>
          <xsl:if test="primary_implicature/@idiom='true'"><i> (idiom)</i></xsl:if>
          <xsl:if test="primary_implicature/@phrasal='true'"><i> (pv)</i></xsl:if>
        </p>

        <xsl:if test="secondary_implicature">
          <p><b>Secondary implicature: </b>
            <xsl:for-each select="secondary_implicature">
              <xsl:value-of select="."/><br/>
            </xsl:for-each>
          </p>
        </xsl:if>

        <xsl:if test="framenet">
          <p><b>FrameNet: </b> <xsl:value-of select="framenet"/></p>
        </xsl:if>
        <xsl:if test="domain/text() != ''">
          <p><b>Domain: </b> <xsl:value-of select="domain"/></p>
        </xsl:if>
        <xsl:if test="register/text() != ''">
          <p><b>Register: </b> <xsl:value-of select="register"/></p>
        </xsl:if>

        <xsl:if test="comment/text() != ''">
          <p><b>Comment: </b> <xsl:if test="comment/@type != ''"><xsl:value-of select="comment/@type"/>: </xsl:if><xsl:value-of select="comment"/></p>
        </xsl:if>

      </body>
      </html>
  </xsl:template>

  <xsl:template match="subspec">
        <xsl:if test="Role">
          <xsl:text>, Role=</xsl:text><xsl:value-of select="Role/@name"/>
        </xsl:if>
        <xsl:if test="attributes">
          <xsl:text>, Attr.=</xsl:text><xsl:value-of select="attributes"/>
        </xsl:if>
        <xsl:if test="Lexset">
          <xsl:text>, Lexset=</xsl:text><xsl:value-of select="Lexset/item"/>
        </xsl:if>
  </xsl:template>

  <xsl:template match="argspec" mode="subj">
    [[<xsl:value-of select="BSO_type/@name"/>]]
    <xsl:apply-templates select="subspec"/>
  </xsl:template>
  
  <xsl:template match="argspec" mode="obj">
    <xsl:if test="@type='optional'">(optional) </xsl:if>
    <xsl:value-of select="@headword"/><xsl:text> </xsl:text>[[<xsl:value-of select="BSO_type/@name"/>]]
    <xsl:apply-templates select="subspec"/>
  </xsl:template>
  
  <xsl:template match="adverbial">
    <xsl:if test="@type='optional'">(optional) </xsl:if>
    <xsl:if test="@func!=''">
      <i><xsl:value-of select="@func"/> </i>
    </xsl:if>
    <xsl:for-each select="alternation">
      <xsl:if test="@type='optional'">(optional) </xsl:if>
      <xsl:value-of select="ptag/@headword"/><xsl:text> </xsl:text><xsl:if test="ptag/argspec/BSO_type/@name!=''">[[<xsl:value-of select="ptag/argspec/BSO_type/@name"/>]]</xsl:if>
      <xsl:apply-templates select="ptag/argspec/subspec"/>
      <xsl:if test="ptag/argspec/adverbs/text()!=''">Adverbs=<xsl:value-of select="ptag/argspec/adverbs"/></xsl:if>
      <br/>
    </xsl:for-each>
    <br/>
  </xsl:template>

</xsl:stylesheet>

