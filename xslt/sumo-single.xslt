<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="utf-8"/>

<xsl:param name="dictionary"/>

<xsl:template match="CONCEPT">
      <span class="BLUE">ID: </span>
      <span class="RED"><xsl:value-of select="ID"/></span>
      <br/>
      <xsl:for-each select="ILR">
        <span class="BLUE">--&gt;&gt;</span><xsl:text> </xsl:text>
	        <xsl:choose>
            <xsl:when test="@type='instance'">
              <span class="RED">[<xsl:value-of select="@type"/>]</span>
          	</xsl:when>
            <xsl:when test="@type='subclass'">
              <span class="GREEN">[<xsl:value-of select="@type"/>]</span>
            </xsl:when>
          	<xsl:otherwise>
              <span class="DARK_RED">[<xsl:value-of select="@type"/>]</span>
          	</xsl:otherwise>
        	</xsl:choose>
    	<xsl:text> </xsl:text>
        <a href="/{$dictionary}?action=listPreview&amp;query={.}"><xsl:value-of select="text()"/></a>
    	<br/>
      </xsl:for-each>
      <xsl:for-each select="RILR">
        <span class="BLUE">&lt;&lt;--</span><xsl:text> </xsl:text>
	<xsl:choose>
	<xsl:when test="@type='instance'">
        <span class="RED">[instance]</span>
	</xsl:when>
	<xsl:when test="@type='subclass'">
        <span class="GREEN">[subclass]</span>
	</xsl:when>
	<xsl:otherwise>
        <span class="DARK_RED">[<xsl:value-of select="@type"/>]</span>
	</xsl:otherwise>
	</xsl:choose>
	<xsl:text> </xsl:text>
        <a href="/{$dictionary}?action=listPreview&amp;query={.}"><xsl:value-of select="text()"/></a>
	<br/>
      </xsl:for-each>
      Show in <a href="http://sigma.ontologyportal.org:4010/sigma/Browse.jsp?kb=SUMO&amp;lang=en&amp;term={ID}" target="sigma">Sigma</a>
     <hr/> 
</xsl:template>

<xsl:template match="BCS">
      <span class="BLUE">BCS: </span>
      <span class="RED"><xsl:value-of select="."/></span>
</xsl:template>

<xsl:template match="DEF">
      <span class="BLUE">Definition: </span>
      <span class="RED"><xsl:value-of select="."/></span>
      <br/>
</xsl:template>
<xsl:template match="DOMAIN">
      <span class="BLUE">Domain: </span>
      <span class="GREEN"><xsl:value-of select="."/></span><br/>
</xsl:template>

</xsl:stylesheet>

