<?xml version='1.0' encoding='utf-8'?>
<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>
<xsl:template match='/entry'>
<html><head><meta content='text/html; charset=utf-8' http-equiv='Content-Type'/><title>slovník 24: <xsl:value-of select="hw"/> (DEBWrite)</title></head><body>
<h1><xsl:value-of select="hw"/></h1>
<xsl:apply-templates/></body></html></xsl:template>

<xsl:template match="hw"><span class="hw">headword: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="pos"><span class="pos">PoS: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="gram"><span class="gram">grammar: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="pron"><span class="pron">pronunciation audio:<br/><xsl:if test="starts-with(@mime, 'image')"><img src="/files/deb_slovnik/{.}" style="max-width:400px"/></xsl:if><xsl:if test="starts-with(@mime, 'audio')"><audio src="/files/deb_slovnik/{.}" controls="true"/></xsl:if><xsl:if test="starts-with(@mime, 'video')"><video src="/files/deb_slovnik/{.}" controls="true" style="max-width:400px"/></xsl:if></span><br/></xsl:template>
<xsl:template match="mean"><div class="mean" style="border: 1px solid black; background-color:undefined">meaning: <xsl:apply-templates/></div><br/></xsl:template>
<xsl:template match="nr"><span class="nr">meaning nr: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="def"><span class="def">explanation: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="ex"><span class="ex">usage: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="com"><span class="com">comment: <xsl:apply-templates/></span><br/></xsl:template>
</xsl:stylesheet>
