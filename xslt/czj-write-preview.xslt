<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:import href="czj-locale.xslt"/>
  <xsl:output encoding="utf-8" method="xml"
    doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
    doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
    />

  <xsl:param name="perm"/>
  <xsl:param name="lang"/>
  <xsl:param name="dictcode"/>
  <xsl:param name="public">false</xsl:param>
  <xsl:variable name="skupina_test">
    <!--ma uzivatel stejnou skupinu jako heslo? nebo bez omezeni-->
    <xsl:if test="contains(/entry/@user_skupina,'all') or contains($perm, 'admin')">true</xsl:if>
    <xsl:if test="$public!='true' and not(contains(/entry/@user_skupina,'all')) and not(contains($perm, 'admin'))"><xsl:value-of select="contains(/entry/@user_skupina, /entry/lemma/pracskupina)"/></xsl:if>
    <xsl:if test="$public='true'">false</xsl:if>
  </xsl:variable>
 
 <xsl:template match="entry">
    <html>
      <head>
        <title><xsl:choose>
            <xsl:when test="$dictcode='cs'">ČJ</xsl:when>
            <xsl:when test="$dictcode='en'">EN</xsl:when>
            <xsl:when test="$dictcode='sj'">SK</xsl:when>
        </xsl:choose>: <xsl:value-of select="lemma/title"/></title>

        <link rel="stylesheet" type="text/css" media="screen" href="/editor/czjprev4.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="/editor/css-6.2/czj-common2.css" ></link>
        <link rel="stylesheet" type="text/css" media="screen" href="/editor/css/empty.css" ></link>
        <link rel="stylesheet" type="text/css" media="screen" href="/editor/css-6.2/cj-preview2.css" ></link>
        <link href='/editor/css-6.2/czj-preview7-min2.css' rel='stylesheet' type='text/css' media='screen'/>

        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Titillium+Web:400,200,300,600,700,900&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="/media/jwplayer/jwplayer.js" ></script>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    <script src="//releases.flowplayer.org/5.4.3/flowplayer.min.js"></script>
    <link rel="stylesheet" href="//releases.flowplayer.org/5.4.3/skin/minimalist.css"/>

    <script type="text/javascript" src="/media/fancybox/jquery.fancybox.min.js"></script>
    <link rel="stylesheet" href="/media/fancybox/jquery.fancybox.min.css" type="text/css" media="screen" />

    <script type="text/javascript" src="/editor/imagesloaded.pkgd.min.js">//</script>
    <script type="text/javascript" src="/editor/czjmain.js">//</script>
    <script><![CDATA[
      function change_view(eid) {
        if (document.getElementById(eid).style.display=='none') {
          if (eid == 'lemma_parts_box') { document.getElementById('lemma_parts_info').innerHTML = 'skrýt podrobnosti'}
          document.getElementById(eid).style.display='block';
        } else {
          if (eid == 'lemma_parts_box') { document.getElementById('lemma_parts_info').innerHTML = 'zobrazit podrobnosti'}
          document.getElementById(eid).style.display='none';
        }
      }

      $(document).ready(function() {
      $(".flowmouse").bind("mouseenter mouseleave", function(e) {
        flowplayer(this)[/ee/.test(e.type) ? "resume":"pause"]();
        });        
      });
      $(document).ready(function() {
        $('#navselect').change(function() {
          var option = $(this.options[this.selectedIndex]).val();
          $('#sidevideo').css('display', 'none');
          $('#sidesw').css('display', 'none');
          $('#sidehamnosys').css('display', 'none');
          $('#side'+option).css('display', 'block');
        });
        $('.topvideosign_side').hide();
        $("a.fancybox").fancybox({
        'autoScale'      : false,
        'transitionIn'   : 'elastic',
        'transitionOut'  : 'elastic',
        'titlePosition'  : 'inside',
        'hideOnContentClick' : true,
        'speedIn'        : 100,
        'speedOut'     : 100,
        'changeSpeed'    : 100,
        'centerOnScroll' : false,
        padding: 0,
        closeClick: true
        });

      function open_iframe(id, dict) {
        var iframe = document.getElementById('colloc'+id);
        if (iframe.style.display=='none') {
          iframe.style.display='inline';
        } else {
          iframe.style.display='none';
        }
        if (iframe.src == 'about:blank') {
        iframe.src = '/'+dict+'?action=getdoc&amp;id='+id+'&amp;tr=inline';
        }
      }
      function iframe_loaded(id) {
        var iframe = document.getElementById('colloc'+id);
        if (iframe.src != 'about:blank') {
          //alert(iframe.contentWindow.document.body.scrollHeight);
          iframe.height = iframe.contentWindow.document.body.scrollHeight + 'px;'
        }
      }

      ]]></script>
      <style>
	body {width: 760px;}
	</style>
      </head>
      <body>

        <div id="outer">      <!-- jen preview ZACATEK -->

    
          <div id="inner" class="czj-inner-right">   <!-- jen preview KONEC -->
            <div class="cj-mainlemma TOP{$dictcode}">
           <xsl:if test="(lemma/lemma_type='collocation' or lemma/lemma_type='prislovi') and ($perm!='ro' or $skupina_test='true' or collocations/@status!='hidden')">
           <!-- <xsl:attribute name="style">background-color: lightgrey;</xsl:attribute> -->
            </xsl:if>
           <xsl:if test="$perm!='ro'">
                        <span class="author-info" style="margin-left: 5px;"><strong><xsl:value-of select="$loc-primy"/>: </strong> <span id="dirURL">http://www.dictio.info/<xsl:value-of select="$dictcode"/>?lang=<xsl:value-of select="$lang"/>&amp;action=search&amp;getdoc=<xsl:value-of select="@id"/></span><button class="button-small" style="background-color: #D7E1E4; font-size: 10px; color: gray;" id="clipBoard" onclick="myFunction()"><xsl:value-of select="$loc-kopirovat"/></button></span>
		<br />              
             <span class="set-status">ID: <xsl:value-of select="@id"/></span> 
             <span class="publish">  <xsl:value-of select="$loc-publikovani"/>:
                <xsl:choose>
                            <xsl:when test="/entry/lemma/completeness='0'"><xsl:value-of select="$loc-automaticky"/> </xsl:when>
                            <xsl:when test="/entry/lemma/completeness='1'"><xsl:value-of select="$loc-nepublikovat"/> </xsl:when>
                            <xsl:when test="/entry/lemma/completeness='2'"><xsl:value-of select="$loc-vyplnene"/> </xsl:when>
                            <xsl:when test="/entry/lemma/completeness='100'"><xsl:value-of select="$loc-schvalene"/> </xsl:when>
                </xsl:choose></span>                 
	    </xsl:if>
             <span style="float: right">
               <xsl:if test="$perm!='ro'"><a href="/editor{$dictcode}/?id={@id}&amp;lang={$lang}&amp;empty={$empty}"><xsl:value-of select="$loc-edit"/><img src="/editor/img/edit.png" /></a>
                      <xsl:choose>
                        <xsl:when test="lemma/@auto_complete='1' and lemma/completeness!='1'"><a style="color:blue;cursor:pointer" onclick="window.location = 'http://www.dictio.info'+window.location.pathname+window.location.search"><img src="/editor/publikovano.png" border="0" title="přepnout na veřejné zobrazení"/></a></xsl:when>
                        <xsl:otherwise><img src="/editor/nepublikovano.png" title="nepublikované heslo"/> </xsl:otherwise>
                      </xsl:choose>
			 </xsl:if>
             </span> <br/>
           
            
           <xsl:if test="(lemma/lemma_type='collocation' or lemma/lemma_type='prislovi') and ($perm!='ro' or $skupina_test='true' or collocations/@status!='hidden')">
             <span id="lemma_parts"><img src="/editor/img/spojeni.png" title="ustálené slovní spojení" style="padding-right: 5px;"/> <!--<strong>Slovní spojení. </strong>--><a style="cursor:pointer;color:blue"  onclick="change_view('lemma_parts_box')" id="lemma_parts_info"><xsl:value-of select="$loc-zobrazitcasti"/></a><br/>
                 <span id="lemma_parts_box" style="display:none">
                   <xsl:apply-templates select="collocations/colloc"/>
                 </span>
               </span>
             </xsl:if>
             <h1 style="display:inline"><xsl:value-of select="lemma/title"/><xsl:if test="/entry/lemma/title_var!=''">, <xsl:value-of select="lemma/title_var"/></xsl:if></h1><!-- implementovat do ilnine a EN -->
             <xsl:if test="lemma/pron!=''"><span style="margin-left: 30px;">[<xsl:value-of select="lemma/pron"/>]</span></xsl:if>
              <!--schvalování-->
           <xsl:if test="$perm!='ro' or $skupina_test='true'">
              <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_lemmacj')">
                <input type="button" class="button-small" style="background-color: gray" onclick="publish_all(this)" value="{$loc-hromadne}"/>
               <br/>
               <span class="set-status">
                 <xsl:value-of select="$loc-typhesla"/>: 
                 <xsl:choose>
                   <xsl:when test="/entry/lemma/lemma_type='single'"><xsl:value-of select="$loc-jednoduche"/></xsl:when>
                   <xsl:when test="/entry/lemma/lemma_type='predpona'"><xsl:value-of select="$loc-predpona"/></xsl:when>
                   <xsl:when test="/entry/lemma/lemma_type='collocation'"><xsl:value-of select="$loc-typcollocation"/></xsl:when>
                   <xsl:when test="/entry/lemma/lemma_type='prislovi'"><xsl:value-of select="$loc-prislovi"/></xsl:when>
                 </xsl:choose>
                 <xsl:call-template name="status_publish">
                   <xsl:with-param name="status" select="/entry/collocations/@status"/>
                   <xsl:with-param name="type">entry</xsl:with-param>
                 </xsl:call-template>
               </span>
             </xsl:if>
           </xsl:if><!--konec schvalovani-->
             
           </div> <!-- cj-mainlemma -->
           <xsl:if test="html and not(meanings/meaning)">
             <div>
               <span class="divtitle">SSČ</span>
               <div class="cj-grammar">
                 <xsl:apply-templates select="html"/>
               </div>
             </div>
           </xsl:if>
           <xsl:if test="lemma/grammar_note/text()!='' or lemma/style_note/text()!='' or lemma/grammar_note/variant or lemma/style_note/variant or lemma/grammar_note/@slovni_druh!='' or lemma/grammar_note/hyphen!='' or lemma/style_note/@kategorie!='' or lemma/style_note/@stylpriznak!=''">
             <div class="cj-gram-popis">
              <!-- <div class="cj-lemma-h1">Formální popis</div> -->
               <div class="cj-grammar">
               <!--schvalování-->
                 <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_cjlingvist')">
                   <span class="set-status">
                     <xsl:value-of select="$loc-grampopis"/>: 
                     <xsl:call-template name="status_publish">
                       <xsl:with-param name="status" select="/entry/lemma/grammar_note[1]/@status"/>
                       <xsl:with-param name="type">gram</xsl:with-param>
                     </xsl:call-template>
                   </span>
                   
                 </xsl:if><!--konec schvalování-->
    <div class="author-info" style="display:none">
      <xsl:if test="lemma/grammar_note/@source!=''"><span class="author-source"><xsl:value-of select="$loc-zdroj"/>: <xsl:value-of select="lemma/grammar_note/@source"/></span>; </xsl:if>
      <xsl:if test="lemma/grammar_note/@author!=''"><span class="author-author"><xsl:value-of select="$loc-autor"/>: <xsl:value-of select="lemma/grammar_note/@author"/></span>; </xsl:if>
      <xsl:if test="lemma/grammar_note/@copyright!=''"><span class="author-copyright"><xsl:value-of select="$loc-copyright"/>: <xsl:value-of select="lemma/grammar_note/@copyright"/></span></xsl:if>
    </div>

             <br />
             <xsl:if test="lemma/puvod!=''">
               <div>
                 <b><xsl:value-of select="$loc-puvodslova"/>: </b>
                 <xsl:choose>
                   <xsl:when test="lemma/puvod='a'"><xsl:value-of select="$loc-puvoda"/></xsl:when>
                   <xsl:when test="lemma/puvod='afr'"><xsl:value-of select="$loc-puvodafr"/></xsl:when>
                   <xsl:when test="lemma/puvod='alb'"><xsl:value-of select="$loc-puvodalb"/></xsl:when>
                   <xsl:when test="lemma/puvod='am'"><xsl:value-of select="$loc-puvodam"/></xsl:when>
                   <xsl:when test="lemma/puvod='ar'"><xsl:value-of select="$loc-puvodar"/></xsl:when>
                   <xsl:when test="lemma/puvod='aram'"><xsl:value-of select="$loc-puvodaram"/></xsl:when>
                   <xsl:when test="lemma/puvod='b'"><xsl:value-of select="$loc-puvodb"/></xsl:when>
                   <xsl:when test="lemma/puvod='bantu'"><xsl:value-of select="$loc-puvodbantu"/></xsl:when>
                   <xsl:when test="lemma/puvod='bask'"><xsl:value-of select="$loc-puvodbask"/></xsl:when>
                   <xsl:when test="lemma/puvod='br'"><xsl:value-of select="$loc-puvodbr"/></xsl:when>
                   <xsl:when test="lemma/puvod='cik'"><xsl:value-of select="$loc-puvodcik"/></xsl:when>
                   <xsl:when test="lemma/puvod='čes'"><xsl:value-of select="$loc-puvodčes"/></xsl:when>
                   <xsl:when test="lemma/puvod='čín'"><xsl:value-of select="$loc-puvodčín"/></xsl:when>
                   <xsl:when test="lemma/puvod='dán'"><xsl:value-of select="$loc-puvoddán"/></xsl:when>
                   <xsl:when test="lemma/puvod='eg'"><xsl:value-of select="$loc-puvodeg"/></xsl:when>
                   <xsl:when test="lemma/puvod='est'"><xsl:value-of select="$loc-puvodest"/></xsl:when>
                   <xsl:when test="lemma/puvod='f'"><xsl:value-of select="$loc-puvodf"/></xsl:when>
                   <xsl:when test="lemma/puvod='fin'"><xsl:value-of select="$loc-puvodfin"/></xsl:when>
                   <xsl:when test="lemma/puvod='g'"><xsl:value-of select="$loc-puvodg"/></xsl:when>
                   <xsl:when test="lemma/puvod='hebr'"><xsl:value-of select="$loc-puvodhebr"/></xsl:when>
                   <xsl:when test="lemma/puvod='hind'"><xsl:value-of select="$loc-puvodhind"/></xsl:when>
                   <xsl:when test="lemma/puvod='i'"><xsl:value-of select="$loc-puvodi"/></xsl:when>
                   <xsl:when test="lemma/puvod='ind'"><xsl:value-of select="$loc-puvodind"/></xsl:when>
                   <xsl:when test="lemma/puvod='indián'"><xsl:value-of select="$loc-puvodindián"/></xsl:when>
                   <xsl:when test="lemma/puvod='indonés'"><xsl:value-of select="$loc-puvodindonés"/></xsl:when>
                   <xsl:when test="lemma/puvod='ir'"><xsl:value-of select="$loc-puvodir"/></xsl:when>
                   <xsl:when test="lemma/puvod='island'"><xsl:value-of select="$loc-puvodisland"/></xsl:when>
                   <xsl:when test="lemma/puvod='j'"><xsl:value-of select="$loc-puvodj"/></xsl:when>
                   <xsl:when test="lemma/puvod='jakut'"><xsl:value-of select="$loc-puvodjakut"/></xsl:when>
                   <xsl:when test="lemma/puvod='jslov'"><xsl:value-of select="$loc-puvodjslov"/></xsl:when>
                   <xsl:when test="lemma/puvod='kelt'"><xsl:value-of select="$loc-puvodkelt"/></xsl:when>
                   <xsl:when test="lemma/puvod='korej'"><xsl:value-of select="$loc-puvodkorej"/></xsl:when>
                   <xsl:when test="lemma/puvod='l'"><xsl:value-of select="$loc-puvodl"/></xsl:when>
                   <xsl:when test="lemma/puvod='lfran'"><xsl:value-of select="$loc-puvodlfran"/></xsl:when>
                   <xsl:when test="lemma/puvod='lapon'"><xsl:value-of select="$loc-puvodlapon"/></xsl:when>
                   <xsl:when test="lemma/puvod='lit'"><xsl:value-of select="$loc-puvodlit"/></xsl:when>
                   <xsl:when test="lemma/puvod='lot'"><xsl:value-of select="$loc-puvodlot"/></xsl:when>
                   <xsl:when test="lemma/puvod='luž'"><xsl:value-of select="$loc-puvodluž"/></xsl:when>
                   <xsl:when test="lemma/puvod='maď'"><xsl:value-of select="$loc-puvodmaď"/></xsl:when>
                   <xsl:when test="lemma/puvod='maked'"><xsl:value-of select="$loc-puvodmaked"/></xsl:when>
                   <xsl:when test="lemma/puvod='mal'"><xsl:value-of select="$loc-puvodmal"/></xsl:when>
                   <xsl:when test="lemma/puvod='mong'"><xsl:value-of select="$loc-puvodmong"/></xsl:when>
                   <xsl:when test="lemma/puvod='n'"><xsl:value-of select="$loc-puvodn"/></xsl:when>
                   <xsl:when test="lemma/puvod='niz'"><xsl:value-of select="$loc-puvodniz"/></xsl:when>
                   <xsl:when test="lemma/puvod='NJ'"><xsl:value-of select="$loc-puvodNJ"/></xsl:when>
                   <xsl:when test="lemma/puvod='nor'"><xsl:value-of select="$loc-puvodnor"/></xsl:when>
                   <xsl:when test="lemma/puvod='OJ'"><xsl:value-of select="$loc-puvodOJ"/></xsl:when>
                   <xsl:when test="lemma/puvod='or'"><xsl:value-of select="$loc-puvodor"/></xsl:when>
                   <xsl:when test="lemma/puvod='p'"><xsl:value-of select="$loc-puvodp"/></xsl:when>
                   <xsl:when test="lemma/puvod='per'"><xsl:value-of select="$loc-puvodper"/></xsl:when>
                   <xsl:when test="lemma/puvod='polynés'"><xsl:value-of select="$loc-puvodpolynés"/></xsl:when>
                   <xsl:when test="lemma/puvod='port'"><xsl:value-of select="$loc-puvodport"/></xsl:when>
                   <xsl:when test="lemma/puvod='provens'"><xsl:value-of select="$loc-puvodprovens"/></xsl:when>
                   <xsl:when test="lemma/puvod='r'"><xsl:value-of select="$loc-puvodr"/></xsl:when>
                   <xsl:when test="lemma/puvod='rom'"><xsl:value-of select="$loc-puvodrom"/></xsl:when>
                   <xsl:when test="lemma/puvod='rum'"><xsl:value-of select="$loc-puvodrum"/></xsl:when>
                   <xsl:when test="lemma/puvod='ř'"><xsl:value-of select="$loc-puvodř"/></xsl:when>
                   <xsl:when test="lemma/puvod='semit'"><xsl:value-of select="$loc-puvodsemit"/></xsl:when>
                   <xsl:when test="lemma/puvod='sch'"><xsl:value-of select="$loc-puvodsch"/></xsl:when>
                   <xsl:when test="lemma/puvod='skan'"><xsl:value-of select="$loc-puvodskan"/></xsl:when>
                   <xsl:when test="lemma/puvod='sl'"><xsl:value-of select="$loc-puvodsl"/></xsl:when>
                   <xsl:when test="lemma/puvod='sln'"><xsl:value-of select="$loc-puvodsln"/></xsl:when>
                   <xsl:when test="lemma/puvod='srb'"><xsl:value-of select="$loc-puvodsrb"/></xsl:when>
                   <xsl:when test="lemma/puvod='střl'"><xsl:value-of select="$loc-puvodstřl"/></xsl:when>
                   <xsl:when test="lemma/puvod='svahil'"><xsl:value-of select="$loc-puvodsvahil"/></xsl:when>
                   <xsl:when test="lemma/puvod='š'"><xsl:value-of select="$loc-puvodš"/></xsl:when>
                   <xsl:when test="lemma/puvod='šv'"><xsl:value-of select="$loc-puvodšv"/></xsl:when>
                   <xsl:when test="lemma/puvod='t'"><xsl:value-of select="$loc-puvodt"/></xsl:when>
                   <xsl:when test="lemma/puvod='tat'"><xsl:value-of select="$loc-puvodtat"/></xsl:when>
                   <xsl:when test="lemma/puvod='tib'"><xsl:value-of select="$loc-puvodtib"/></xsl:when>
                   <xsl:when test="lemma/puvod='ttat'"><xsl:value-of select="$loc-puvodttat"/></xsl:when>
                   <xsl:when test="lemma/puvod='tung'"><xsl:value-of select="$loc-puvodtung"/></xsl:when>
                   <xsl:when test="lemma/puvod='u'"><xsl:value-of select="$loc-puvodu"/></xsl:when>
                   <xsl:when test="lemma/puvod='VJ'"><xsl:value-of select="$loc-puvodVJ"/></xsl:when>
                   <xsl:when test="lemma/puvod='vslov'"><xsl:value-of select="$loc-puvodvslov"/></xsl:when>
                   <xsl:when test="lemma/puvod='ZJ'"><xsl:value-of select="$loc-puvodZJ"/></xsl:when>
                   <xsl:when test="lemma/puvod='zslov'"><xsl:value-of select="$loc-puvodzslov"/></xsl:when>
                   <xsl:when test="lemma/puvod='žid'"><xsl:value-of select="$loc-puvodžid"/></xsl:when>

                 <xsl:otherwise><xsl:value-of select="lemma/puvod"/></xsl:otherwise>
               </xsl:choose>  </div>
             </xsl:if>
                 
                 <xsl:if test="$perm!='ro' or $skupina_test='true' or lemma/grammar_note[1]/@status!='hidden'">
                   
                   <xsl:for-each select="lemma/grammar_note">
                     <xsl:if test="@slovni_druh!=''">
                      <xsl:if test="@slovni_druh='subst' or @slovni_druh='adj' or @slovni_druh='pron' or @slovni_druh='num' or @slovni_druh='verb' or @slovni_druh='prep' or @slovni_druh='kon' or @slovni_druh='adv' or @slovni_druh='par' or @slovni_druh='int'">
                        <b><xsl:value-of select="$loc-lexkategorie"/>: </b>  </xsl:if>
                       <xsl:choose>
                         <xsl:when test="@slovni_druh='subst'"><xsl:value-of select="$loc-lexsubst"/></xsl:when>
                         <xsl:when test="@slovni_druh='adj'"><xsl:value-of select="$loc-lexadj"/></xsl:when>
                         <xsl:when test="@slovni_druh='pron'"><xsl:value-of select="$loc-lexpron"/></xsl:when>
                         <xsl:when test="@slovni_druh='num'"><xsl:value-of select="$loc-lexnum"/></xsl:when>
                         <xsl:when test="@slovni_druh='verb'"><xsl:value-of select="$loc-lexverb"/></xsl:when>
                         <xsl:when test="@slovni_druh='adv'"><xsl:value-of select="$loc-lexadv"/></xsl:when>
                         <xsl:when test="@slovni_druh='prep'"><xsl:value-of select="$loc-lexprep"/></xsl:when>
                         <xsl:when test="@slovni_druh='ust'"><xsl:value-of select="$loc-lexust"/></xsl:when>
                         <xsl:when test="@slovni_druh='kon'"><xsl:value-of select="$loc-lexkonj"/></xsl:when>
                         <xsl:when test="@slovni_druh='par'"><xsl:value-of select="$loc-lexpart"/></xsl:when>
                         <xsl:when test="@slovni_druh='int'"><xsl:value-of select="$loc-lexint"/></xsl:when>
                         <xsl:when test="@slovni_druh='sprezka'"><xsl:value-of select="$loc-lexsprezka"/></xsl:when>
                         <xsl:when test="@slovni_druh='predpona'"><xsl:value-of select="$loc-predpona"/></xsl:when>
                         <xsl:when test="@slovni_druh='zkratka'"><xsl:value-of select="$loc-lexzkratka"/></xsl:when>
                         <!--
                         <xsl:when test="@slovni_druh='ustalene'">ustálené slovní spojení</xsl:when>
                         <xsl:when test="@slovni_druh='prirovnani'">přirovnání</xsl:when>
                         <xsl:when test="@slovni_druh='prislovi'">přísloví</xsl:when>
                         <xsl:when test="@slovni_druh='porekadlo'">pořekadlo</xsl:when>
                         <xsl:when test="@slovni_druh='frazem'">frazém</xsl:when>-->
                       </xsl:choose>
                       <xsl:if test="@skupina!=''">
                         <xsl:if test="@slovni_druh!='ustalene'">
                            <xsl:text>, </xsl:text> 
                         </xsl:if>
                         <xsl:call-template name="split_skupina"><xsl:with-param name="text" select="@skupina"/></xsl:call-template>
                       </xsl:if>
                       <xsl:if test="@skupina2!=''">
                         <xsl:text>, </xsl:text>
                         <xsl:call-template name="split_skupina2"><xsl:with-param name="text" select="@skupina2"/></xsl:call-template>
                       </xsl:if>
                     </xsl:if>
                     <xsl:if test="position()!=last()"><br/></xsl:if>
                   </xsl:for-each>
                   <xsl:if test="lemma/grammar_note/@flexe_neskl='true'">, <xsl:value-of select="$loc-nesklonne"/></xsl:if>
                     
                     <xsl:if test="lemma/grammar_note/variant">
                       <br/><b>
                         <xsl:if test="lemma/grammar_note/@slovni_druh!='ustalene'"><xsl:value-of select="$loc-gramaticka"/> </xsl:if>
                         <xsl:value-of select="$loc-varianta"/>: </b>
                       <xsl:apply-templates select="lemma/grammar_note/variant"/>
                     </xsl:if>
                     <xsl:if test="normalize-space(lemma/grammar_note/text())!=''"><br/><xsl:apply-templates select="lemma/grammar_note" mode="ingram"/></xsl:if>
                     <xsl:if test="lemma/grammar_note/hyphen"><b><xsl:value-of select="$loc-deleni"/>: </b><xsl:value-of select="lemma/grammar_note/hyphen"/><br/></xsl:if>
                   
                    <!--tabulka-->
                   <xsl:if test="lemma/gram/form">
                     <xsl:choose>
                       <xsl:when test="lemma/grammar_note/@slovni_druh='subst' or lemma/grammar_note/@slovni_druh=''">
                         <table id="cj-gram-table">
                           <tr><td></td><td><xsl:value-of select="$loc-jednotne"/></td><td><xsl:value-of select="$loc-mnozne"/></td></tr>
                           <tr><td><xsl:value-of select="$loc-1pad"/></td><td><xsl:value-of select="lemma/gram/form[@tag='nSc1']"/></td><td><xsl:value-of select="lemma/gram/form[@tag='nPc1']"/></td></tr>
                           <tr><td><xsl:value-of select="$loc-2pad"/></td><td><xsl:value-of select="lemma/gram/form[@tag='nSc2']"/></td><td><xsl:value-of select="lemma/gram/form[@tag='nPc2']"/></td></tr>
                           <tr><td><xsl:value-of select="$loc-3pad"/></td><td><xsl:value-of select="lemma/gram/form[@tag='nSc3']"/></td><td><xsl:value-of select="lemma/gram/form[@tag='nPc3']"/></td></tr>
                           <tr><td><xsl:value-of select="$loc-4pad"/></td><td><xsl:value-of select="lemma/gram/form[@tag='nSc4']"/></td><td><xsl:value-of select="lemma/gram/form[@tag='nPc4']"/></td></tr>
                           <tr><td><xsl:value-of select="$loc-5pad"/></td><td><xsl:value-of select="lemma/gram/form[@tag='nSc5']"/></td><td><xsl:value-of select="lemma/gram/form[@tag='nPc5']"/></td></tr>
                           <tr><td><xsl:value-of select="$loc-6pad"/></td><td><xsl:value-of select="lemma/gram/form[@tag='nSc6']"/></td><td><xsl:value-of select="lemma/gram/form[@tag='nPc6']"/></td></tr>
                           <tr><td><xsl:value-of select="$loc-7pad"/></td><td><xsl:value-of select="lemma/gram/form[@tag='nSc7']"/></td><td><xsl:value-of select="lemma/gram/form[@tag='nPc7']"/></td></tr>
                         </table>
                       </xsl:when>
                       <xsl:when test="lemma/grammar_note/@slovni_druh='adj'">
                         <table id="cj-gram-table">
                           <tr><td><xsl:value-of select="$loc-2stupen"/></td><td><xsl:value-of select="lemma/gram/form[@tag='d2']"/></td></tr>
                           <tr><td><xsl:value-of select="$loc-3stupen"/></td><td><xsl:value-of select="lemma/gram/form[@tag='d3']"/></td></tr>
                         </table>
                       </xsl:when>
                       <xsl:when test="lemma/grammar_note/@slovni_druh='adv'">
                         <table id="cj-gram-table">
                           <tr><td><xsl:value-of select="$loc-2stupen"/></td><td><xsl:value-of select="lemma/gram/form[@tag='d2']"/></td></tr>
                           <tr><td><xsl:value-of select="$loc-3stupen"/></td><td><xsl:value-of select="lemma/gram/form[@tag='d3']"/></td></tr>
                         </table>
                       </xsl:when>
                       <xsl:when test="lemma/grammar_note/@slovni_druh='verb'">
                         <table id="cj-gram-table">
                           <tr><td></td><td><xsl:value-of select="$loc-jednotne"/></td><td><xsl:value-of select="$loc-mnozne"/></td></tr>
                           <tr><td><xsl:value-of select="$loc-1osoba"/></td><td><xsl:value-of select="lemma/gram/form[@tag='mIp1nS']"/></td><td><xsl:value-of select="lemma/gram/form[@tag='mIp1nP']"/></td></tr>
                           <tr><td><xsl:value-of select="$loc-2osoba"/></td><td><xsl:value-of select="lemma/gram/form[@tag='mIp2nS']"/></td><td><xsl:value-of select="lemma/gram/form[@tag='mIp2nP']"/></td></tr>
                           <tr><td><xsl:value-of select="$loc-3osoba"/></td><td><xsl:value-of select="lemma/gram/form[@tag='mIp3nS']"/></td><td><xsl:value-of select="lemma/gram/form[@tag='mIp3nP']"/></td></tr>
                           <tr><td><xsl:value-of select="$loc-rozkazovaci"/></td><td><xsl:value-of select="lemma/gram/form[@tag='mRp2nS']"/></td><td><xsl:value-of select="lemma/gram/form[@tag='mRp2nP']"/></td></tr>
                           <xsl:if test="lemma/gram/form[@tag='mAgMnS']"><tr><td><xsl:value-of select="$loc-pricinne"/></td><td colspan="2"><xsl:value-of select="lemma/gram/form[@tag='mAgMnS']"/></td></tr></xsl:if>
                           <xsl:if test="lemma/gram/form[@tag='mNgMnS']"><tr><td><xsl:value-of select="$loc-pritrpne"/></td><td colspan="2"><xsl:value-of select="lemma/gram/form[@tag='mNgMnS']"/></td></tr></xsl:if>
                           <xsl:if test="lemma/gram/form[@tag='mSgMnS']">
                             <tr><td><xsl:value-of select="$loc-prechm"/></td><td><xsl:value-of select="lemma/gram/form[@tag='mSgMnS']"/></td><td rowspan="2"><xsl:value-of select="lemma/gram/form[@tag='mSgMnP']"/></td></tr>
                             <tr><td><xsl:value-of select="$loc-prechzs"/></td><td><xsl:value-of select="lemma/gram/form[@tag='mSgFnS']"/></td></tr>
                           </xsl:if>
                           <xsl:if test="lemma/gram/form[@tag='mDgMnS']">
                             <tr><td><xsl:value-of select="$loc-prechm"/></td><td><xsl:value-of select="lemma/gram/form[@tag='mDgMnS']"/></td><td rowspan="2"><xsl:value-of select="lemma/gram/form[@tag='mDgMnP']"/></td></tr>
                             <tr><td><xsl:value-of select="$loc-prechzs"/></td><td><xsl:value-of select="lemma/gram/form[@tag='mDgFnS']"/></td></tr>
                           </xsl:if>
                           <tr><td><xsl:value-of select="$loc-versub"/></td><td colspan="2"><xsl:value-of select="lemma/gram/form[@tag='gNnSc1']"/></td></tr>
                         </table>
                       </xsl:when>
                       <xsl:when test="lemma/grammar_note/@slovni_druh='num' or lemma/grammar_note/@slovni_druh='pron'">
                         <table id="cj-gram-table">
                           <xsl:if test="lemma/gram/form[@tag='cP1']"><tr><td></td><td><xsl:value-of select="$loc-jednotne"/></td><td><xsl:value-of select="$loc-mnozne"/></td></tr></xsl:if>
                           <tr><td><xsl:value-of select="$loc-1pad"/></td><td><xsl:value-of select="lemma/gram/form[@tag='c1']"/></td><xsl:if test="lemma/gram/form[@tag='cP1']"><td><xsl:value-of select="lemma/gram/form[@tag='cP1']"/></td></xsl:if></tr>
                           <tr><td><xsl:value-of select="$loc-2pad"/></td><td><xsl:value-of select="lemma/gram/form[@tag='c2']"/></td><xsl:if test="lemma/gram/form[@tag='cP1']"><td><xsl:value-of select="lemma/gram/form[@tag='cP2']"/></td></xsl:if></tr>
                           <tr><td><xsl:value-of select="$loc-3pad"/></td><td><xsl:value-of select="lemma/gram/form[@tag='c3']"/></td><xsl:if test="lemma/gram/form[@tag='cP1']"><td><xsl:value-of select="lemma/gram/form[@tag='cP3']"/></td></xsl:if></tr>
                           <tr><td><xsl:value-of select="$loc-4pad"/></td><td><xsl:value-of select="lemma/gram/form[@tag='c4']"/></td><xsl:if test="lemma/gram/form[@tag='cP1']"><td><xsl:value-of select="lemma/gram/form[@tag='cP4']"/></td></xsl:if></tr>
                           <tr><td><xsl:value-of select="$loc-5pad"/></td><td><xsl:value-of select="lemma/gram/form[@tag='c5']"/></td><xsl:if test="lemma/gram/form[@tag='cP1']"><td><xsl:value-of select="lemma/gram/form[@tag='cP5']"/></td></xsl:if></tr>
                           <tr><td><xsl:value-of select="$loc-6pad"/></td><td><xsl:value-of select="lemma/gram/form[@tag='c6']"/></td><xsl:if test="lemma/gram/form[@tag='cP1']"><td><xsl:value-of select="lemma/gram/form[@tag='cP6']"/></td></xsl:if></tr>
                           <tr><td><xsl:value-of select="$loc-7pad"/></td><td><xsl:value-of select="lemma/gram/form[@tag='c7']"/></td><xsl:if test="lemma/gram/form[@tag='cP1']"><td><xsl:value-of select="lemma/gram/form[@tag='cP7']"/></td></xsl:if></tr>
                         </table>
                       </xsl:when>
                       <xsl:otherwise>
                         <table>
                           <xsl:for-each select="lemma/gram/form">
                             <tr>
                               <td><xsl:value-of select="@tag"/></td>
                               <td><xsl:value-of select="."/></td>
                             </tr>
                           </xsl:for-each>
                         </table>
                       </xsl:otherwise>
                     </xsl:choose>
                   </xsl:if>  <!--konec tabulky-->
                   
                   
                   <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_cjlingvist')">
                   <br /><span class="set-status">
                     <xsl:value-of select="$loc-stylpopis"/>: 
                     <xsl:call-template name="status_publish">
                       <xsl:with-param name="status" select="/entry/lemma/style_note[1]/@status"/>
                       <xsl:with-param name="type">style</xsl:with-param>
                     </xsl:call-template>
                   </span>
                   <br/>
                 </xsl:if>
    <div class="author-info" style="display:none">      
     <xsl:if test="lemma/style_note/@source!=''"><span class="author-source"><xsl:value-of select="$loc-zdroj"/>: <xsl:value-of select="lemma/style_note/@source"/></span>; </xsl:if>
     <xsl:if test="lemma/style_note/@author!=''"><span class="author-author"><xsl:value-of select="$loc-autor"/>: <xsl:value-of select="lemma/style_note/@author"/></span>; </xsl:if>
     <xsl:if test="lemma/style_note/@copyright!=''"><span class="author-copyright"><xsl:value-of select="$loc-copyright"/>: <xsl:value-of select="lemma/style_note/@copyright"/></span></xsl:if>
    </div>

    <xsl:if test="lemma/style_note/@kategorie!=''"><br/><b><xsl:value-of select="$loc-stylhod"/>: </b>
                     <xsl:call-template name="split_hodnoceni"><xsl:with-param name="text" select="lemma/style_note/@kategorie"/></xsl:call-template>
                   </xsl:if>
                   <xsl:if test="lemma/style_note/@stylpriznak!=''"><br/><b><xsl:value-of select="$loc-stylpri"/>: </b>
                     <xsl:call-template name="split_priznak"><xsl:with-param name="text" select="lemma/style_note/@stylpriznak"/></xsl:call-template>
                   </xsl:if>
                   
                   <xsl:if test="lemma/style_note/variant">
                     <br/><b>
                       <xsl:if test="lemma/grammar_note/@slovni_druh!='ustalene'"><xsl:value-of select="$loc-stylisticka"/> </xsl:if>
                       <xsl:value-of select="$loc-varianta"/>: </b>
                     <xsl:apply-templates select="lemma/style_note/variant"/>
                   </xsl:if>
                   <xsl:if test="normalize-space(lemma/style_note/text())!=''"><br/><xsl:apply-templates select="lemma/style_note" mode="ingram"/></xsl:if>
                   
                 </xsl:if>
                 
               </div>
             </div>
           </xsl:if>
           <div class="cj-vyznam">
           <xsl:for-each select="meanings/meaning[(text!='' or count(relation)>0 or usages/usage/text!='') and (status!='hidden' or $perm!='ro' or $skupina_test='true')]">
              <xsl:sort select="@number" data-type="number"/>
              <div class="meaningtop" id="meaning{@id}">
                <div class="author-info" style="display:none">
                  <xsl:if test="@source!=''"><span class="author-source"><xsl:value-of select="$loc-zdroj"/>: <xsl:value-of select="@source"/></span>; </xsl:if>
                  <xsl:if test="@author!=''"><span class="author-author"><xsl:value-of select="$loc-autor"/>: <xsl:value-of select="@author"/></span>; </xsl:if>
                  <xsl:if test="@copyright!=''"><span class="author-copyright"><xsl:value-of select="$loc-copyright"/>: <xsl:value-of select="@copyright"/></span></xsl:if>
                </div>
                <div class="cj-lemma-h1"><xsl:value-of select="$loc-vyznam"/><xsl:text> </xsl:text>
                  <xsl:if test="count(//meanings/meaning[status='published'])>1">
                    <xsl:value-of select="position()"/>
                  </xsl:if>
                  <xsl:if test="count(//meanings/meaning[status='published'])&lt;=1 and ($perm!='ro' or $skupina_test='true')">
                    <xsl:value-of select="@number"/>
                  </xsl:if>
                  <xsl:if test="($perm!='ro' or $skupina_test='true') and @number!=substring-after(@id,'-')"> <span class="source">[! id:<xsl:value-of select="@id"/>]</span></xsl:if>
                </div>
                <div id="@id" class="cj-meaning">                  
                  <xsl:if test="@oblast!=''">
                    <span class="category"><b><xsl:value-of select="$loc-vyzobla"/>: </b>
                      <xsl:call-template name="split_oblast"><xsl:with-param name="text" select="@oblast"/></xsl:call-template>
                    </span><br/>
                  </xsl:if>
                  <xsl:apply-templates select="text"/>
		      <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_cjlingvist')">
                    <span class="set-status"> 
                      <xsl:call-template name="status_publish">
                        <xsl:with-param name="status" select="status"/>
                        <xsl:with-param name="type">meaning</xsl:with-param>
                        <xsl:with-param name="element" select="."/>
                      </xsl:call-template>
                    </span>
                    <br/>
                  </xsl:if>

              <xsl:if test="relation[@type='translation_colloc']">
                <p><strong><xsl:value-of select="$loc-slovnispojeni"/>:</strong></p>
                <xsl:apply-templates select="relation[@type='translation_colloc' and ((@status!='hidden' and @auto_complete='1') or $perm!='ro' or $skupina_test='true')]"/>
              </xsl:if>
              <xsl:if test="relation[@type='synonym' and ((@status!='hidden' or @auto_complete='1') or $perm!='ro' or $skupina_test='true')]">
                <p><strong><xsl:value-of select="$loc-synonyma"/>: </strong>
                  <xsl:apply-templates select="relation[@type='synonym' and ((@status!='hidden' or @auto_complete='1') or $perm!='ro' or $skupina_test='true')]">
                    <xsl:sort select="@meaning_id"/>
                  </xsl:apply-templates>
                </p>
              </xsl:if>
              <xsl:if test="relation[@type='antonym' and ((@status!='hidden' or @auto_complete='1') or $perm!='ro' or $skupina_test='true')]">
                <p><strong><xsl:value-of select="$loc-antonyma"/>: </strong>
                  <xsl:apply-templates select="relation[@type='antonym' and ((@status!='hidden' or @auto_complete='1') or $perm!='ro' or $skupina_test='true')]">
                  </xsl:apply-templates>
                </p>
              </xsl:if>
              <xsl:if test="count(usages/usage[(status!='hidden' or $perm!='ro' or $skupina_test='true') and text!=''])>0"> 
                <div class="cj-lemma-h2"><xsl:value-of select="$loc-priklady"/>:</div>
                <xsl:for-each select="usages/usage[((status!='hidden' and (relation/@auto_complete='1' or relation/@front_video='published')) or $perm!='ro' or $skupina_test='true') and @type='colloc']"> 
                  <xsl:if test="text!=''">
                    <div style="margin-bottom: 7px;"><em><xsl:apply-templates select="text"/></em>
                      <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_cjlingvist')">
                        <span class="set-status">
                          <xsl:call-template name="status_publish">
                            <xsl:with-param name="status" select="status"/>
                            <xsl:with-param name="type">usage</xsl:with-param>
                            <xsl:with-param name="element" select="."/>
                          </xsl:call-template>
                        </span>
                      </xsl:if>
                      <xsl:if test="relation">
                        <em style="font-size:80%; cursor: pointer;" onclick="$(this).next().toggle();$(this).find('.usagetrans-toggle').toggle();resize_sw();"> <span class="usagetrans-toggle">(<xsl:value-of select="$loc-prekladyzobr"/><img src="/media/img/slovnik-expand.png" height="10" />)</span><span class="usagetrans-toggle" style="display:none;">(<xsl:value-of select="$loc-prekladyskryt"/><img src="/media/img/slovnik-collapse.png" height="10" />)</span></em>
                        <div class="usage-translation" style="display:none; padding-left: 20px; font-style: italic; font-size:90%">
                          <div><strong><xsl:value-of select="$loc-preklady"/>: </strong></div>
                          <xsl:for-each select="relation[@target='en' or @target='cs' or @target='sj']">
                            <div class="dict-@target"><xsl:choose><xsl:when test="@target='en'"><span style="color: grey">EN:</span></xsl:when><xsl:when test="@target='cs'"><span style="color: grey">ČJ:</span></xsl:when><xsl:when test="@target='sj'"><span style="color: grey">SJ:</span></xsl:when></xsl:choose>
                              <xsl:value-of select="title"/></div>
                          </xsl:for-each>
                          <xsl:apply-templates select="relation[@target!='en' and @target!='cs' and @target!='sj']" mode="usage">
                            <xsl:with-param name="variant_pos" select="position()"/> 
                          </xsl:apply-templates>
                          <div style="clear:both"/>
                        </div>
                      </xsl:if>
                    </div>
                  </xsl:if>
                  <div class="author-info" style="display:none">
                    <xsl:if test="@source!=''"><span class="author-source"><xsl:value-of select="$loc-zdroj"/>: <xsl:value-of select="@source"/></span>; </xsl:if>
                    <xsl:if test="@author!=''"><span class="author-author"><xsl:value-of select="$loc-autor"/>: <xsl:value-of select="@author"/></span>; </xsl:if>
                    <xsl:if test="@copyright!=''"><span class="author-copyright"><xsl:value-of select="$loc-copyright"/>: <xsl:value-of select="@copyright"/></span></xsl:if>
                  </div>
                </xsl:for-each>
                <!--<xsl:for-each select="usages/usage[((status!='hidden' and (relation/@auto_complete='1' or relation/@front_video='published')) or $perm!='ro' or $skupina_test='true') and @type='colloc']">
                  <xsl:if test="relation">
                    <xsl:apply-templates select="relation[@target='czj']" mode="usage">
                      <xsl:with-param name="variant_pos" select="position()"/> 
                    </xsl:apply-templates>
                    <xsl:apply-templates select="relation[@target='is']" mode="usage">
                      <xsl:with-param name="variant_pos" select="position()"/> 
                    </xsl:apply-templates>
                    <xsl:apply-templates select="relation[@target='asl']" mode="usage">
                      <xsl:with-param name="variant_pos" select="position()"/> 
                    </xsl:apply-templates>
                  </xsl:if>
                  <xsl:if test="position()=last()"><div style="clear:both"/></xsl:if>
                </xsl:for-each>-->
                <xsl:for-each select="usages/usage[(status!='hidden' or $perm!='ro' or $skupina_test='true') and not(@type='colloc')]">
                  <xsl:if test="text!=''">
                    <div style="margin-bottom: 7px;"><em><xsl:apply-templates select="text"/></em>
                      <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_cjlingvist')">
                        <span class="set-status">
                          <xsl:call-template name="status_publish">
                            <xsl:with-param name="status" select="status"/>
                            <xsl:with-param name="type">usage</xsl:with-param>
                            <xsl:with-param name="element" select="."/>
                          </xsl:call-template>
                        </span>
                      </xsl:if>
                    </div>
                  </xsl:if>
                  <div class="author-info" style="display:none">
                    <xsl:if test="@source!=''"><span class="author-source"><xsl:value-of select="$loc-zdroj"/>: <xsl:value-of select="@source"/></span>; </xsl:if>
                    <xsl:if test="@author!=''"><span class="author-author"><xsl:value-of select="$loc-autor"/>: <xsl:value-of select="@author"/></span>; </xsl:if>
                    <xsl:if test="@copyright!=''"><span class="author-copyright"><xsl:value-of select="$loc-copyright"/>: <xsl:value-of select="@copyright"/></span></xsl:if>
                  </div>
                </xsl:for-each>
              </xsl:if>
              <xsl:if test="relation[@type='translation' and ((@status!='hidden') or $perm!='ro' or $skupina_test='true')]">
                <p><strong><xsl:value-of select="$loc-preklady"/>:</strong></p>
                <div class="czj-relation-wrapper">                
                <p><xsl:apply-templates select="relation[@type='translation' and ((@status!='hidden') or ($perm!='ro' or $skupina_test='true')) and (@target='cs' or @target='en' or @target='sj')]">
                 <xsl:sort data-type="number" order="ascending" select="((@target='cs')*1) + ((@target='en')*2)"/>
                </xsl:apply-templates></p>
                <xsl:apply-templates select="relation[@type='translation' and ((@status!='hidden') or ($perm!='ro' or $skupina_test='true')) and not(@target='cs' or @target='en' or @target='sj')]">
                 <xsl:sort data-type="number" order="ascending" select="((@target='czj')*1) + ((@target='is')*2) + ((@target='asl')*3)"/>
                 <xsl:sort data-type="number" order="descending" select="((@lemma_region='cr')*3) + ((@lemma_region='cechy')*2) +((@lemma_region='morava')*1)"/>
                 <xsl:sort data-type="number" order="ascending" select="((@lemma_kategorie='')*1) + ((@lemma_kategorie='neologismus')*2) + ((@lemma_kategorie='Archaismus')*3)"/>
                </xsl:apply-templates>                                              
                </div>
              </xsl:if>
            </div>
          </div>
        </xsl:for-each> 
        </div> <!-- cj-vyznam -->
        
        <div style="clear:both"></div>
        <div id="collocations">
          <xsl:if test="collocations/revcolloc[$perm!='ro' or $skupina_test='true' or @auto_complete='1' or @completeness='2' or @completeness='100']">
            <div class="cj-lemma-h1"><xsl:value-of select="$loc-sheslem"/></div>
            <!--<div class="collocations">-->              
              <xsl:apply-templates select="collocations/revcolloc[$perm!='ro' or $skupina_test='true' or @auto_complete='1' or @completeness='2' or @completeness='100']">
              <xsl:sort select="title"/>
              </xsl:apply-templates>
            <!--</div>-->
            <div style="clear:both"></div>
          </xsl:if>
        </div>

        <input type="button" class="button-zdroje" value="{$loc-zobrazitautory}" id="show-author" onclick="$('.author-info').toggle();"/> 
      </div> <!-- jen preview -->
    </div>   <!-- jen preview -->
  </body>
</html>
  </xsl:template>
  
  <xsl:template match="relation[(@type='translation' or @type='translation_colloc') and not(@target='cs' or @target='en' or @target='sj')]">
    <xsl:if test="((@status!='hidden' or (name(..)='usage' and ../status='published')) and (@auto_complete='1' or @front_video='published')) or $perm!='ro' or $skupina_test='true'">     <!-- oprava pro zobrazení překladů bez odkazu  @auto_complete='1' -->
      
    <xsl:variable name="lemma"><xsl:value-of select="@lemma_id"/></xsl:variable>
    <xsl:variable name="rtype"><xsl:value-of select="@type"/></xsl:variable>    
    <xsl:if test="count(preceding-sibling::relation[@lemma_id=$lemma and @type=$rtype])=0">

    <div class="czj-relation-item dict-{@target}">
    <div class="czj-relation relationvideo">
      <div class="czj-lemma-preview-sm">
        <div class="czj-lemma-preview-sm-header dict-{@target}">            

          <div class="alignleft" style="white-space: nowrap; overflow: hidden; width: 160px;">
          <xsl:choose>
              <xsl:when test="@target='czj'">ČZJ </xsl:when>          
              <xsl:when test="@target='is'">IS </xsl:when>
              <xsl:when test="@target='asl'">ASL </xsl:when>
              <xsl:when test="@target='spj'">SPJ </xsl:when>
              <xsl:otherwise>?:</xsl:otherwise>
            </xsl:choose>
          
            <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_cjlingvist') or contains($perm, 'revizor_preklad')">
              <xsl:if test="@meaning_nr='' or @meaning_count = 1">
                <span class="set-status">
                  <xsl:call-template name="status_publish">
                    <xsl:with-param name="status" select="@status"/>
                    <xsl:with-param name="type">relation</xsl:with-param>
                    <xsl:with-param name="element" select="."/>
                  </xsl:call-template>
                </span>
              </xsl:if>
              <!--<br/>-->
              </xsl:if><xsl:if test="@meaning_nr!='' and @meaning_count > 1">(<xsl:value-of select="$loc-vevyznamu"/> <xsl:value-of select="@meaning_nr"/>
                <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_cjlingvist') or contains($perm, 'revizor_preklad')">
                  <span class="set-status">
                    <xsl:call-template name="status_publish">
                      <xsl:with-param name="status" select="@status"/>
                      <xsl:with-param name="type">relation</xsl:with-param>
                      <xsl:with-param name="element" select="."/>
                    </xsl:call-template>
                  </span>
                </xsl:if>
              <xsl:if test="count(following-sibling::relation[@lemma_id=$lemma and @type=$rtype]) &gt; 0">
              <xsl:for-each select="following-sibling::relation[@lemma_id=$lemma and @type=$rtype]">
              <xsl:sort select="@meaning_nr"/>
              <xsl:text>, </xsl:text><xsl:value-of select="@meaning_nr"/>
                  <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_cjlingvist') or contains($perm, 'revizor_preklad')">
                    <span class="set-status">
                      <xsl:call-template name="status_publish">
                        <xsl:with-param name="status" select="@status"/>
                        <xsl:with-param name="type">relation</xsl:with-param>
                        <xsl:with-param name="element" select="."/>
                      </xsl:call-template>
                    </span>
                  </xsl:if>
                </xsl:for-each>
              </xsl:if>)
          </xsl:if></div>
          <xsl:if test="$perm!='ro' or @auto_complete='1'">
          <div class="alignright">
            <a href="/{@target}?lang={$lang}&amp;action=search&amp;getdoc={@lemma_id}&amp;empty={$empty}">           
              <!--<xsl:value-of select="@lemma_id"/>-->
              <img src="/media/img/slovnik-open_w.png" height="20"/>
              <xsl:if test="@type='translation_colloc'">
                <xsl:value-of select="translation"/>
              </xsl:if>
            </a>
          </div> 
          </xsl:if>
        </div> 
        <xsl:apply-templates select="file" mode="rel">
          <xsl:with-param name="target"><xsl:value-of select="@target"/></xsl:with-param>
          <xsl:with-param name="text_link"><xsl:if test="$perm='ro' and @auto_complete='0' and @front_video='published'">true</xsl:if></xsl:with-param>
        </xsl:apply-templates>
        <div class="czj-lemma-preview-sm-sw">
          <span class="sw transsw">
            <xsl:apply-templates select="swmix/sw" mode="rel"/>
          </span>
        </div>
      </div>
     </div>
     </div> 
      <xsl:if test="@type='translation_colloc'">
        <br/>
      </xsl:if>
    </xsl:if>
    </xsl:if>
  </xsl:template>  
<xsl:template match="relation[@type='translation' and (@target='cs' or @target='en' or @target='sj')]">
    <xsl:variable name="lemma">
      <xsl:value-of select="@lemma_id"/>
    </xsl:variable>
    <span class="relation-entry dict-{@target}">
      <span style="color: gray">        
          <xsl:choose>
              <xsl:when test="@target='cs'">CZ: </xsl:when>          
              <xsl:when test="@target='en'">EN: </xsl:when>              
              <xsl:when test="@target='sj'">SJ: </xsl:when>              
              <xsl:otherwise>?</xsl:otherwise>
          </xsl:choose>
        </span>
      <xsl:if test="@title_only!='true'">
        <xsl:if test="count(preceding-sibling::relation[@lemma_id=$lemma and @type='translation'])=0">          
          <xsl:if test="@auto_complete='1' or ($perm!='ro' or $skupina_test='true')">
            <a href="/{@target}?lang={$lang}&amp;action=search&amp;getdoc={@lemma_id}&amp;empty={$empty}">
              <xsl:value-of select="title"/></a>                     
          </xsl:if>          
          <xsl:if test="@auto_complete!='1' and $perm='ro' and $skupina_test!='true'"><xsl:value-of select="title"/>           
            </xsl:if>          
            <xsl:if test="(@meaning_nr!='' and @meaning_count > 1) and (@auto_complete='1' or $perm!='ro' or $skupina_test='true')"> (<xsl:value-of select="$loc-vevyznamu"/>
            <xsl:value-of select="@meaning_nr"/>              
            <!--schvalování-->              
            <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_lingvist') or contains($perm, 'revizor_preklad')">
              <span class="set-status">                    
                <xsl:call-template name="status_publish">                    
                  <xsl:with-param name="status" select="@status"/>                    
                  <xsl:with-param name="type">relation</xsl:with-param>                    
                  <xsl:with-param name="element" select="."/>                  
                </xsl:call-template>                
              </span>              
            </xsl:if><!--konec schvalování-->                            
            <xsl:if test="count(following-sibling::relation[@lemma_id=$lemma and @type='translation']) &gt; 0">              
              <xsl:for-each select="following-sibling::relation[@lemma_id=$lemma and @type='translation']">  
                <xsl:sort select="@meaning_nr" data-type="number"/>              
                <xsl:text>, </xsl:text>
                <xsl:value-of select="@meaning_nr"/>                
                <!--schvalování-->                
                <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_lingvist') or contains($perm, 'revizor_preklad')">
                  <span class="set-status">                      
                    <xsl:call-template name="status_publish">                      
                      <xsl:with-param name="status" select="@status"/>                      
                      <xsl:with-param name="type">relation</xsl:with-param>                      
                      <xsl:with-param name="element" select="."/>                    
                    </xsl:call-template>                  
                  </span>                
                </xsl:if>
                <!--konec schvalování-->                
              </xsl:for-each>            
            </xsl:if>)           
          </xsl:if>          
          <xsl:if test="@meaning_nr='' or @meaning_count = 1">            
            <!--schvalování-->            
            <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_lingvist') or contains($perm, 'revizor_preklad')">
              <span class="set-status">                  
                <xsl:call-template name="status_publish">                  
                  <xsl:with-param name="status" select="@status"/>                  
                  <xsl:with-param name="type">relation</xsl:with-param>                  
                  <xsl:with-param name="element" select="."/>                
                </xsl:call-template>              
              </span>            
            </xsl:if><!--konec schvalování-->            
          </xsl:if>          

          <!--<xsl:if test="position()!=last()">, </xsl:if>-->          
          <xsl:if test="position()!=last()">, </xsl:if>        
        </xsl:if>      
      </xsl:if>      
      <xsl:if test="@title_only='true'">        
        <xsl:value-of select="title"/>           
        <!--schvalování-->    
        <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_lingvist') or contains($perm, 'revizor_preklad')">
          <span class="set-status">            
            <xsl:call-template name="status_publish">            
              <xsl:with-param name="status" select="@status"/>            
              <xsl:with-param name="type">relation</xsl:with-param>            
              <xsl:with-param name="element" select="."/>          
            </xsl:call-template>        
          </span>      
        </xsl:if><!--konec schvalování-->                  

        <xsl:if test="position()!=last()">, </xsl:if>      
      </xsl:if>    
    </span>  
  </xsl:template>
  <xsl:template match="relation" mode="usage">
    <xsl:param name="variant_pos"/>  
    <xsl:apply-templates select="file" mode="usage_translation">
      <xsl:with-param name="variant_sw" select="swmix"/>
      <!--<xsl:with-param name="variant_pos" select="$variant_pos"/> -->
      <xsl:with-param name="variant_pos" select="position()"/> 
      <xsl:with-param name="usage_target" select="@target"/> 
      <xsl:with-param name="relation_link"><xsl:if test="(@auto_complete='1' or $perm!='ro') and not(contains(@meaning_id,'_us'))">true</xsl:if></xsl:with-param>
    </xsl:apply-templates>  
  </xsl:template>
  <xsl:template match="file" mode="usage_translation">
    <xsl:param name="variant_sw"/>
    <xsl:param name="variant_pos"/>
    <xsl:param name="usage_target"/>
    <xsl:param name="relation_link"/>
    <div class="czj-relation-item dict-czj">
      <div class="czj-relation relationvideo">
        <div class="czj-lemma-preview-sm">      
          <div class="czj-lemma-preview-sm-header dict-{$usage_target}">
            <div class="alignleft"><xsl:choose><xsl:when test="$usage_target='czj'">ČZJ</xsl:when>
                <xsl:when test="$usage_target='is'">IS</xsl:when>
                <xsl:when test="$usage_target='asl'">ASL</xsl:when>
                <xsl:when test="$usage_target='spj'">SPJ</xsl:when>                
                </xsl:choose>
            </div>
            <xsl:if test="$relation_link='true'">
              <div class="alignright">
                <a href="/{$usage_target}?lang={$lang}&amp;action=search&amp;getdoc={../@lemma_id}&amp;empty={$empty}">
                  <img src="/media/img/slovnik-open_w.png" height="20"/></a>
              </div>
            </xsl:if>
          </div>

        </div>
        <div style="clear:both">
        </div>

      <xsl:if test="substring(location, string-length(location)-3, 4) = '.flv'">
        <div id="flowvideousg{@id}-{../@meaning_id}" data-ratio="0.8" class="flowplayer flowmouse usage-video{$relation_link}" style="background:#777 url(/media/video{$usage_target}/thumb/{location}/thumb.jpg) no-repeat;">
          <video preload="none" loop="loop">      
            <source type="video/flash" src="/media/video{$usage_target}/{location}"/>
          </video>            
        </div><br />
      </xsl:if>
      <xsl:if test="substring(location, string-length(location)-3, 4) = '.mp4'">
        <div id="flowvideousg{@id}-{../@meaning_id}" data-ratio="0.8" class="flowmouse usage-video{$relation_link}" style="background: url(/media/video{$usage_target}/thumb/{location}/thumb.jpg) no-repeat;">
          <video preload="none" width="120px" height="96px" poster="/media/video{$usage_target}/thumb/{location}/thumb.jpg" onmouseover="this.play()" onmouseout="this.pause()">            
            <source type="video/mp4" src="/media/video{$usage_target}/{location}"/>
          </video>
        </div>  
      </xsl:if>  
           
      <div class="czj-lemma-preview-sm-sw">
        <xsl:if test="($variant_sw/sw)">
          <span class="sw transsw">
            <xsl:apply-templates select="$variant_sw/sw" mode="rel"/>
          </span>
        </xsl:if>
      </div>

        <xsl:if test="$relation_link='true'">
           <script type="text/javascript">
             $("#flowvideousg<xsl:value-of select="@id"/>-<xsl:value-of select="../@meaning_id"/>").on("click", function(e) {
             window.location='/<xsl:value-of select="$usage_target"/>?lang=<xsl:value-of select="$lang"/>&amp;action=search&amp;getdoc=<xsl:value-of select="../@lemma_id"/>';
                 });
             </script>
        </xsl:if>
        <xsl:if test="not($relation_link='true')">
<script type="text/javascript">
  $("#flowvideousg<xsl:value-of select="@id"/>-<xsl:value-of select="../@meaning_id"/>").on("click", function(e) {
          var container = $('<div data-ratio="0.8" style="width:285px;">
            <video preload="none" controls="" width="285px" height="228px" poster="/media/video{$usage_target}/thumb/{location}/thumb.jpg" autoplay="">
            <source type="video/mp4" src="/media/video{$usage_target}/{location}"/>
            </video>
            </div>');
          $.fancybox({
            content: container,
            width: 285,
            height: 228,
            scrolling: 'no',
            autoSize: false
          });
        });
      </script>
    </xsl:if>
      </div>  
    </div>
  </xsl:template>

  <xsl:template match="relation[@type!='translation' and @type!='translation_colloc']">
    <xsl:variable name="lemma"><xsl:value-of select="@lemma_id"/></xsl:variable>
    <xsl:variable name="rtype"><xsl:value-of select="@type"/></xsl:variable>
    <xsl:if test="count(preceding-sibling::relation[@lemma_id=$lemma and @type=$rtype])=0">        
        <xsl:if test="@title_only!='true'">
          <xsl:if test="@auto_complete='1' or $perm!='ro' or $skupina_test='true'">
            <a href="/{$dictcode}?lang={$lang}&amp;action=search&amp;getdoc={@lemma_id}&amp;empty={$empty}"><xsl:value-of select="title"/></a>
          </xsl:if>
          <xsl:if test="@auto_complete='0' and @status='published' and $perm='ro' and $skupina_test!='true'">
            <xsl:value-of select="title"/>
            <xsl:if test="position()!=last()"><xsl:text>, </xsl:text></xsl:if>
          </xsl:if>
          <xsl:if test="@meaning_nr!=''">
          <xsl:if test="@auto_complete='1' or $perm!='ro' or $skupina_test='true'"> 
            (<xsl:value-of select="$loc-vevyznamu"/> <xsl:value-of select="@meaning_nr"/>
            <xsl:if test="count(following-sibling::relation[@lemma_id=$lemma and @type=$rtype]) &gt; 0">
              <xsl:for-each select="following-sibling::relation[@lemma_id=$lemma and @type=$rtype]">
              <xsl:sort select="@meaning_nr" data-type="number"/>
                <xsl:text>, </xsl:text><xsl:value-of select="@meaning_nr"/>
              </xsl:for-each>
          </xsl:if>)
          </xsl:if>                    
        </xsl:if>
          </xsl:if>
        <xsl:if test="@title_only='true'">
          <xsl:value-of select="title"/>
            <xsl:if test="position()!=last()"><xsl:text>, </xsl:text></xsl:if>
        </xsl:if>
	 <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_cjlingvist')">
          <span class="set-status">
            <xsl:call-template name="status_publish">
              <xsl:with-param name="status" select="@status"/>
              <xsl:with-param name="type">relation</xsl:with-param>
              <xsl:with-param name="element" select="."/>
            </xsl:call-template>
          </span>
        </xsl:if>
        <!--<xsl:value-of select="count(preceding-sibling::relation[@type=$rtype])"/>=
        <xsl:value-of select="count(following-sibling::relation[@type=$rtype and @lemma_id!=$lemma])"/>*
        <xsl:value-of select="count(following-sibling::relation[@type=$rtype and @lemma_id!=$lemma and @lemma_id=preceding-sibling::relation[@type=$rtype and @lemma_id!=$lemma]/@lemma_id])"/>x
        <xsl:value-of select="(count(following-sibling::relation[@type=$rtype and @lemma_id!=$lemma])-count(following-sibling::relation[@type=$rtype and @lemma_id!=$lemma and @lemma_id=preceding-sibling::relation[@type=$rtype]/@lemma_id]))"/>-->
        <xsl:if test="(count(following-sibling::relation[@type=$rtype and @lemma_id!=$lemma])-count(following-sibling::relation[@type=$rtype and @lemma_id!=$lemma and @lemma_id=preceding-sibling::relation[@type=$rtype]/@lemma_id]))!=0"><xsl:text>, </xsl:text></xsl:if>
        <!--<xsl:if test="count(following-sibling::relation[@type=$rtype and @lemma_id!=$lemma])!=0"><xsl:text>, </xsl:text></xsl:if>-->
    </xsl:if>
  </xsl:template>

  <xsl:template match="sw" mode="rel">
    <xsl:if test=".!=''">
        <a href="http://znaky.zcu.cz/proxy/tts/signwriting.png?generator[sw]={.}&amp;generator[align]=top_left&amp;generator[set]=sw10" data-fancybox="images" class="fancybox swlink" data-sw="{.}">
          <img src="http://znaky.zcu.cz/proxy/tts/signwriting.png?generator[sw]={.}&amp;generator[align]=top_left&amp;generator[set]=sw10" alt="signwriting" class="signwriting" ratio="0.5" style=""/>

          <!-- <img style="" src="http://znaky.zcu.cz/proxy/tts/signwriting.png?generator[sw]={.}&amp;generator[align]=top_left&amp;generator[set]=sw10" alt="signwriting" class="signwriting"/>-->
      </a>
    </xsl:if>
  </xsl:template>

  <xsl:template match="file" mode="rel">    
        <xsl:param name="target"><xsl:value-of select="$dictcode"/></xsl:param>
        <xsl:param name="text_link">false</xsl:param>

        <xsl:if test="substring(location, string-length(location)-3, 4) = '.flv'">
          <div id="flowvideorel{id}" data-ratio="0.8" class="flowplayer flowmouse" style="width:120px; background:#777 url(/media/video{$target}/thumb/{location}/thumb.jpg) no-repeat; background-size: 120px 96px">
            <video loop="loop">
              <source type="video/flash" src="/media/video{$target}/{location}"/>
            </video>
          </div>    
        </xsl:if>
        <xsl:if test="substring(location, string-length(location)-3, 4) = '.mp4'">
          <div id="flowvideorel{id}" data-ratio="0.8" class="flowmouse" style="width:120px; background: url(/media/video{$target}/thumb/{location}/thumb.jpg) no-repeat; background-size: 120px 96px; float:left;">    
             <video loop="loop" width="120px" height="96px" poster="/media/video{$target}/{location}/thumb.jpg" onmouseover="this.play()" onmouseout="this.pause()">
             <xsl:if test="$video='webm'">
                  <source type="video/webm" src="/media/video{$target}/{location}.webm"/>
             </xsl:if>     
                  <source type="video/mp4" src="/media/video{$target}/{location}"/>
                </video>
           </div>   
        </xsl:if>

  <xsl:if test="$text_link!='true'">
    <script type="text/javascript">
      $("#flowvideorel<xsl:value-of select="id"/>").on("click", function(e) {
      window.location='/<xsl:value-of select="$target"/>?lang=<xsl:value-of select="$lang"/>&amp;action=search&amp;getdoc=<xsl:value-of select="../@lemma_id"/>&amp;empty=<xsl:value-of select="$empty"/>';
      });
    </script>
  </xsl:if>
  </xsl:template>

<xsl:template match="colloc">
     <span style="vertical-align:top;">
      <xsl:if test="not(title)">
        <xsl:value-of select="@lemma_id"/>
      </xsl:if>
      <xsl:if test="title">
        <a href="/{$dictcode}?lang={$lang}&amp;action=search&amp;getdoc={@lemma_id}&amp;empty={$empty}">
          <xsl:value-of select="title"/>
        </a>
      </xsl:if>
      <xsl:if test="position()!=last()">
        <xsl:text>,&#160;</xsl:text>
      </xsl:if>
    </span>
</xsl:template>


  <xsl:template match="table|tr|td|tbody">
    <xsl:element name="{name(.)}">
      <xsl:apply-templates/>
    </xsl:element>
  </xsl:template>

  <xsl:template name="split_skupina">
    <xsl:param name="text"/>
    <xsl:variable name="first" select='substring-before($text,";")'/>
    <xsl:variable name='rest' select='substring-after($text,";")'/>
    <xsl:if test='$first'>
      <xsl:choose>
        <xsl:when test="$first='pomnozne'"><xsl:value-of select="$loc-csskupinapomnozne"/></xsl:when>
        <xsl:when test="$first='hromadne'"><xsl:value-of select="$loc-csskupinahromadne"/></xsl:when>
        <xsl:when test="$first='vlastni'"><xsl:value-of select="$loc-csskupinavlastni"/></xsl:when>
        <xsl:when test="$first='osobni'"><xsl:value-of select="$loc-csskupinaosobni"/></xsl:when>
        <xsl:when test="$first='ukazovaci'"><xsl:value-of select="$loc-csskupinaukazovaci"/></xsl:when>
        <xsl:when test="$first='vztazne'"><xsl:value-of select="$loc-csskupinavztazne"/></xsl:when>
        <xsl:when test="$first='zvratne'"><xsl:value-of select="$loc-csskupinazvratne"/></xsl:when>
        <xsl:when test="$first='zaporne'"><xsl:value-of select="$loc-csskupinazaporne"/></xsl:when>
        <xsl:when test="$first='neurcite'"><xsl:value-of select="$loc-csskupinaneurcite"/></xsl:when>
        <xsl:when test="$first='privl'"><xsl:value-of select="$loc-csskupinaprivl"/></xsl:when>
        <xsl:when test="$first='zakladni'"><xsl:value-of select="$loc-csskupinazakladni"/></xsl:when>
        <xsl:when test="$first='druhova'"><xsl:value-of select="$loc-csskupinadruhova"/></xsl:when>
        <xsl:when test="$first='radova'"><xsl:value-of select="$loc-csskupinaradova"/></xsl:when>
        <xsl:when test="$first='nasobna'"><xsl:value-of select="$loc-csskupinanasobna"/></xsl:when>
        <xsl:when test="$first='tazaci'"><xsl:value-of select="$loc-csskupinatazaci"/></xsl:when>
        <xsl:when test="$first='dok'"><xsl:value-of select="$loc-csskupinadok"/></xsl:when>
        <xsl:when test="$first='nedok'"><xsl:value-of select="$loc-csskupinanedok"/></xsl:when>
        <xsl:when test="$first='doknedok'"><xsl:value-of select="$loc-csskupinadoknedok"/></xsl:when>
        <xsl:when test="$first='zajmenne'"><xsl:value-of select="$loc-csskupinazajmenne"/></xsl:when>
        <xsl:when test="$first='sourad'"><xsl:value-of select="$loc-csskupinasourad"/></xsl:when>
        <xsl:when test="$first='podrad'"><xsl:value-of select="$loc-csskupinapodrad"/></xsl:when>
        <xsl:when test="$first='nasobnapris'"><xsl:value-of select="$loc-csskupinanasobnapris"/></xsl:when>
        <xsl:when test="$first='nasobnaprisneu'"><xsl:value-of select="$loc-csskupinanasobnaprisneu"/></xsl:when>
        <xsl:when test="$first='zajmenna'"><xsl:value-of select="$loc-csskupinazajmenna"/></xsl:when>
        <xsl:when test="$first='frazem'"><xsl:value-of select="$loc-csskupinafrazem"/></xsl:when>
        <xsl:when test="$first='porekadlo'"><xsl:value-of select="$loc-csskupinaporekadlo"/></xsl:when>
        <xsl:when test="$first='prirovnani'"><xsl:value-of select="$loc-csskupinaprirovnani"/></xsl:when>
        <xsl:when test="$first='prislovi'"><xsl:value-of select="$loc-csskupinaprislovi"/></xsl:when>
      </xsl:choose>
      <xsl:if test="$rest">, </xsl:if>
    </xsl:if>

    <xsl:if test='$rest'>
      <xsl:call-template name='split_skupina'>
        <xsl:with-param name='text' select='$rest'/>
      </xsl:call-template>
    </xsl:if>
    <xsl:if test='not($rest)'>
      <xsl:choose>
        <xsl:when test="$text='pomnozne'"><xsl:value-of select="$loc-csskupinapomnozne"/></xsl:when>
        <xsl:when test="$text='hromadne'"><xsl:value-of select="$loc-csskupinahromadne"/></xsl:when>
        <xsl:when test="$text='vlastni'"><xsl:value-of select="$loc-csskupinavlastni"/></xsl:when>
        <xsl:when test="$text='osobni'"><xsl:value-of select="$loc-csskupinaosobni"/></xsl:when>
        <xsl:when test="$text='ukazovaci'"><xsl:value-of select="$loc-csskupinaukazovaci"/></xsl:when>
        <xsl:when test="$text='vztazne'"><xsl:value-of select="$loc-csskupinavztazne"/></xsl:when>
        <xsl:when test="$text='zvratne'"><xsl:value-of select="$loc-csskupinazvratne"/></xsl:when>
        <xsl:when test="$text='zaporne'"><xsl:value-of select="$loc-csskupinazaporne"/></xsl:when>
        <xsl:when test="$text='neurcite'"><xsl:value-of select="$loc-csskupinaneurcite"/></xsl:when>
        <xsl:when test="$text='privl'"><xsl:value-of select="$loc-csskupinaprivl"/></xsl:when>
        <xsl:when test="$text='zakladni'"><xsl:value-of select="$loc-csskupinazakladni"/></xsl:when>
        <xsl:when test="$text='druhova'"><xsl:value-of select="$loc-csskupinadruhova"/></xsl:when>
        <xsl:when test="$text='radova'"><xsl:value-of select="$loc-csskupinaradova"/></xsl:when>
        <xsl:when test="$text='nasobna'"><xsl:value-of select="$loc-csskupinanasobna"/></xsl:when>
        <xsl:when test="$text='tazaci'"><xsl:value-of select="$loc-csskupinatazaci"/></xsl:when>
        <xsl:when test="$text='dok'"><xsl:value-of select="$loc-csskupinadok"/></xsl:when>
        <xsl:when test="$text='nedok'"><xsl:value-of select="$loc-csskupinanedok"/></xsl:when>
        <xsl:when test="$text='doknedok'"><xsl:value-of select="$loc-csskupinadoknedok"/></xsl:when>
        <xsl:when test="$text='zajmenne'"><xsl:value-of select="$loc-csskupinazajmenne"/></xsl:when>
        <xsl:when test="$text='sourad'"><xsl:value-of select="$loc-csskupinasourad"/></xsl:when>
        <xsl:when test="$text='podrad'"><xsl:value-of select="$loc-csskupinapodrad"/></xsl:when>
        <xsl:when test="$text='nasobnapris'"><xsl:value-of select="$loc-csskupinanasobnapris"/></xsl:when>
        <xsl:when test="$text='nasobnaprisneu'"><xsl:value-of select="$loc-csskupinanasobnaprisneu"/></xsl:when>
        <xsl:when test="$text='zajmenna'"><xsl:value-of select="$loc-csskupinazajmenna"/></xsl:when>
        <xsl:when test="$text='frazem'"><xsl:value-of select="$loc-csskupinafrazem"/></xsl:when>
        <xsl:when test="$text='porekadlo'"><xsl:value-of select="$loc-csskupinaporekadlo"/></xsl:when>
        <xsl:when test="$text='prirovnani'"><xsl:value-of select="$loc-csskupinaprirovnani"/></xsl:when>
        <xsl:when test="$text='prislovi'"><xsl:value-of select="$loc-csskupinaprislovi"/></xsl:when>
      </xsl:choose>
    </xsl:if>
  </xsl:template>

  <xsl:template name="split_skupina2">
    <xsl:param name="text"/>
    <xsl:variable name="first" select='substring-before($text,";")'/>
    <xsl:variable name='rest' select='substring-after($text,";")'/>
    <xsl:if test='$first'>
      <xsl:choose>
        <xsl:when test="$first='m'"><xsl:value-of select="$loc-csskupina2m"/></xsl:when>
        <xsl:when test="$first='z'"><xsl:value-of select="$loc-csskupina2z"/></xsl:when>
        <xsl:when test="$first='s'"><xsl:value-of select="$loc-csskupina2s"/></xsl:when>
        <xsl:when test="$first='zpusobove'"><xsl:value-of select="$loc-csskupina2zpusobove"/></xsl:when>
        <xsl:when test="$first='ukazovaci'"><xsl:value-of select="$loc-csskupina2ukazovaci"/></xsl:when>
        <xsl:when test="$first='tazaci'"><xsl:value-of select="$loc-csskupina2tazaci"/></xsl:when>
        <xsl:when test="$first='vztazne'"><xsl:value-of select="$loc-csskupina2vztazne"/></xsl:when>
        <xsl:when test="$first='neurcite'"><xsl:value-of select="$loc-csskupina2neurcite"/></xsl:when>
        <xsl:when test="$first='zaporne'"><xsl:value-of select="$loc-csskupina2zaporne"/></xsl:when>
        <xsl:when test="$first='prisudku'"><xsl:value-of select="$loc-csskupina2prisudku"/></xsl:when>
        <xsl:when test="$first='vymez'"><xsl:value-of select="$loc-csskupina2vymez"/></xsl:when>
        <xsl:when test="$first='zduraz'"><xsl:value-of select="$loc-csskupina2zduraz"/></xsl:when>
        <xsl:when test="$first='obsahova'"><xsl:value-of select="$loc-csskupina2obsahova"/></xsl:when>
        <xsl:when test="$first='modalni'"><xsl:value-of select="$loc-csskupina2modalni"/></xsl:when>
        <xsl:when test="$first='citoslovecna'"><xsl:value-of select="$loc-csskupina2citoslovecna"/></xsl:when>
      </xsl:choose>
      <xsl:if test="$rest">, </xsl:if>
    </xsl:if>

    <xsl:if test='$rest'>
      <xsl:call-template name='split_skupina2'>
        <xsl:with-param name='text' select='$rest'/>
      </xsl:call-template>
    </xsl:if>
    <xsl:if test='not($rest)'>
      <xsl:choose>
        <xsl:when test="$text='m'"><xsl:value-of select="$loc-csskupina2m"/></xsl:when>
        <xsl:when test="$text='z'"><xsl:value-of select="$loc-csskupina2z"/></xsl:when>
        <xsl:when test="$text='s'"><xsl:value-of select="$loc-csskupina2s"/></xsl:when>
        <xsl:when test="$text='zpusobove'"><xsl:value-of select="$loc-csskupina2zpusobove"/></xsl:when>
        <xsl:when test="$text='ukazovaci'"><xsl:value-of select="$loc-csskupina2ukazovaci"/></xsl:when>
        <xsl:when test="$text='tazaci'"><xsl:value-of select="$loc-csskupina2tazaci"/></xsl:when>
        <xsl:when test="$text='vztazne'"><xsl:value-of select="$loc-csskupina2vztazne"/></xsl:when>
        <xsl:when test="$text='neurcite'"><xsl:value-of select="$loc-csskupina2neurcite"/></xsl:when>
        <xsl:when test="$text='zaporne'"><xsl:value-of select="$loc-csskupina2zaporne"/></xsl:when>
        <xsl:when test="$text='prisudku'"><xsl:value-of select="$loc-csskupina2prisudku"/></xsl:when>
        <xsl:when test="$text='vymez'"><xsl:value-of select="$loc-csskupina2vymez"/></xsl:when>
        <xsl:when test="$text='zduraz'"><xsl:value-of select="$loc-csskupina2zduraz"/></xsl:when>
        <xsl:when test="$text='obsahova'"><xsl:value-of select="$loc-csskupina2obsahova"/></xsl:when>
        <xsl:when test="$text='modalni'"><xsl:value-of select="$loc-csskupina2modalni"/></xsl:when>
        <xsl:when test="$text='citoslovecna'"><xsl:value-of select="$loc-csskupina2citoslovecna"/></xsl:when>
      </xsl:choose>
    </xsl:if>
  </xsl:template>

  <xsl:template name="split_hodnoceni">
    <xsl:param name="text"/>
    <xsl:variable name="first" select='substring-before($text,";")'/>
    <xsl:variable name='rest' select='substring-after($text,";")'/>
    <xsl:if test='$first'>
      <xsl:choose>
        <xsl:when test="$first='argot'"><xsl:value-of select="$loc-cshodnoceniargot"/></xsl:when>
        <xsl:when test="$first='hovorove'"><xsl:value-of select="$loc-cshodnocenihovorove"/></xsl:when>
        <xsl:when test="$first='morav'"><xsl:value-of select="$loc-cshodnocenimorav"/></xsl:when>
        <xsl:when test="$first='narec'"><xsl:value-of select="$loc-cshodnoceninarec"/></xsl:when>
        <xsl:when test="$first='neologismus' or $first='Neologismus'"><xsl:value-of select="$loc-cshodnocenineologismus"/></xsl:when>
        <xsl:when test="$first='obecne'"><xsl:value-of select="$loc-cshodnoceniobecne"/></xsl:when>
        <xsl:when test="$first='oblast'"><xsl:value-of select="$loc-cshodnocenioblast"/></xsl:when>
        <xsl:when test="$first='slang'"><xsl:value-of select="$loc-cshodnocenislang"/></xsl:when>
        <xsl:when test="$first='spisovne'"><xsl:value-of select="$loc-cshodnocenispisovne"/></xsl:when>
      </xsl:choose>    
      <xsl:if test="$rest">, </xsl:if>
    </xsl:if>
    <xsl:if test='$rest'>
      <xsl:call-template name='split_hodnoceni'>
        <xsl:with-param name='text' select='$rest'/>
      </xsl:call-template>
    </xsl:if>
    <xsl:if test='not($rest)'>
      <xsl:choose>
        <xsl:when test="$text='argot'"><xsl:value-of select="$loc-cshodnoceniargot"/></xsl:when>
        <xsl:when test="$text='hovorove'"><xsl:value-of select="$loc-cshodnocenihovorove"/></xsl:when>
        <xsl:when test="$text='morav'"><xsl:value-of select="$loc-cshodnocenimorav"/></xsl:when>
        <xsl:when test="$text='narec'"><xsl:value-of select="$loc-cshodnoceninarec"/></xsl:when>
        <xsl:when test="$text='neologismus' or $text='Neologismus'"><xsl:value-of select="$loc-cshodnocenineologismus"/></xsl:when>
        <xsl:when test="$text='obecne'"><xsl:value-of select="$loc-cshodnoceniobecne"/></xsl:when>
        <xsl:when test="$text='oblast'"><xsl:value-of select="$loc-cshodnocenioblast"/></xsl:when>
        <xsl:when test="$text='slang'"><xsl:value-of select="$loc-cshodnocenislang"/></xsl:when>
        <xsl:when test="$text='spisovne'"><xsl:value-of select="$loc-cshodnocenispisovne"/></xsl:when>
      </xsl:choose>    
    </xsl:if>
  </xsl:template>

  <xsl:template name="split_priznak">
    <xsl:param name="text"/>
    <xsl:variable name="first" select='substring-before($text,";")'/>
    <xsl:variable name='rest' select='substring-after($text,";")'/>
    <xsl:if test='$first'>
      <xsl:choose>
        <xsl:when test="$first='abstrakt'"><xsl:value-of select="$loc-cspriznakabstrakt"/></xsl:when>
        <xsl:when test="$first='basnicky'"><xsl:value-of select="$loc-cspriznakbasnicky"/></xsl:when>
        <xsl:when test="$first='biblicky'"><xsl:value-of select="$loc-cspriznakbiblicky"/></xsl:when>
        <xsl:when test="$first='detsky'"><xsl:value-of select="$loc-cspriznakdetsky"/></xsl:when>
        <xsl:when test="$first='domacke'"><xsl:value-of select="$loc-cspriznakdomacke"/></xsl:when>
        <xsl:when test="$first='duverny'"><xsl:value-of select="$loc-cspriznakduverny"/></xsl:when>
        <xsl:when test="$first='eufem'"><xsl:value-of select="$loc-cspriznakeufem"/></xsl:when>
        <xsl:when test="$first='expres'"><xsl:value-of select="$loc-cspriznakexpres"/></xsl:when>
        <xsl:when test="$first='famil'"><xsl:value-of select="$loc-cspriznakfamil"/></xsl:when>
        <xsl:when test="$first='hanlivy'"><xsl:value-of select="$loc-cspriznakhanlivy"/></xsl:when>
        <xsl:when test="$first='ironicky'"><xsl:value-of select="$loc-cspriznakironicky"/></xsl:when>
        <xsl:when test="$first='kladny'"><xsl:value-of select="$loc-cspriznakkladny"/></xsl:when>
        <xsl:when test="$first='knizni'"><xsl:value-of select="$loc-cspriznakknizni"/></xsl:when>
        <xsl:when test="$first='konkretni'"><xsl:value-of select="$loc-cspriznakkonkretni"/></xsl:when>
        <xsl:when test="$first='lidovy'"><xsl:value-of select="$loc-cspriznaklidovy"/></xsl:when>
        <xsl:when test="$first='lichotivy'"><xsl:value-of select="$loc-cspriznaklichotivy"/></xsl:when>
        <xsl:when test="$first='mazlivy'"><xsl:value-of select="$loc-cspriznakmazlivy"/></xsl:when>
        <xsl:when test="$first='odborny'"><xsl:value-of select="$loc-cspriznakodborny"/></xsl:when>
        <xsl:when test="$first='pohadkovy'"><xsl:value-of select="$loc-cspriznakpohadkovy"/></xsl:when>
        <xsl:when test="$first='publ'"><xsl:value-of select="$loc-cspriznakpubl"/></xsl:when>
        <xsl:when test="$first='prenes'"><xsl:value-of select="$loc-cspriznakprenes"/></xsl:when>
        <xsl:when test="$first='zdvor'"><xsl:value-of select="$loc-cspriznakzdvor"/></xsl:when>
        <xsl:when test="$first='zhrubely'"><xsl:value-of select="$loc-cspriznakzhrubely"/></xsl:when>
        <xsl:when test="$first='zert'"><xsl:value-of select="$loc-cspriznakzert"/></xsl:when>
        <xsl:when test="$first='zlomkovy'"><xsl:value-of select="$loc-cspriznakzlomkovy"/></xsl:when>
        <xsl:when test="$first='zdrob'"><xsl:value-of select="$loc-cspriznakzdrob"/></xsl:when>
        <xsl:when test="$first='zastar'"><xsl:value-of select="$loc-cspriznakzastar"/></xsl:when>
        <xsl:when test="$first='cirkev'"><xsl:value-of select="$loc-cspriznakcirkev"/></xsl:when>
      </xsl:choose>    
      <xsl:if test="$rest">, </xsl:if>
    </xsl:if>
    <xsl:if test='$rest'>
      <xsl:call-template name='split_priznak'>
        <xsl:with-param name='text' select='$rest'/>
      </xsl:call-template>
    </xsl:if>
    <xsl:if test='not($rest)'>
      <xsl:choose>
        <xsl:when test="$text='abstrakt'"><xsl:value-of select="$loc-cspriznakabstrakt"/></xsl:when>
        <xsl:when test="$text='basnicky'"><xsl:value-of select="$loc-cspriznakbasnicky"/></xsl:when>
        <xsl:when test="$text='biblicky'"><xsl:value-of select="$loc-cspriznakbiblicky"/></xsl:when>
        <xsl:when test="$text='detsky'"><xsl:value-of select="$loc-cspriznakdetsky"/></xsl:when>
        <xsl:when test="$text='domacke'"><xsl:value-of select="$loc-cspriznakdomacke"/></xsl:when>
        <xsl:when test="$text='duverny'"><xsl:value-of select="$loc-cspriznakduverny"/></xsl:when>
        <xsl:when test="$text='eufem'"><xsl:value-of select="$loc-cspriznakeufem"/></xsl:when>
        <xsl:when test="$text='expres'"><xsl:value-of select="$loc-cspriznakexpres"/></xsl:when>
        <xsl:when test="$text='famil'"><xsl:value-of select="$loc-cspriznakfamil"/></xsl:when>
        <xsl:when test="$text='hanlivy'"><xsl:value-of select="$loc-cspriznakhanlivy"/></xsl:when>
        <xsl:when test="$text='ironicky'"><xsl:value-of select="$loc-cspriznakironicky"/></xsl:when>
        <xsl:when test="$text='kladny'"><xsl:value-of select="$loc-cspriznakkladny"/></xsl:when>
        <xsl:when test="$text='knizni'"><xsl:value-of select="$loc-cspriznakknizni"/></xsl:when>
        <xsl:when test="$text='konkretni'"><xsl:value-of select="$loc-cspriznakkonkretni"/></xsl:when>
        <xsl:when test="$text='lidovy'"><xsl:value-of select="$loc-cspriznaklidovy"/></xsl:when>
        <xsl:when test="$text='lichotivy'"><xsl:value-of select="$loc-cspriznaklichotivy"/></xsl:when>
        <xsl:when test="$text='mazlivy'"><xsl:value-of select="$loc-cspriznakmazlivy"/></xsl:when>
        <xsl:when test="$text='odborny'"><xsl:value-of select="$loc-cspriznakodborny"/></xsl:when>
        <xsl:when test="$text='pohadkovy'"><xsl:value-of select="$loc-cspriznakpohadkovy"/></xsl:when>
        <xsl:when test="$text='publ'"><xsl:value-of select="$loc-cspriznakpubl"/></xsl:when>
        <xsl:when test="$text='prenes'"><xsl:value-of select="$loc-cspriznakprenes"/></xsl:when>
        <xsl:when test="$text='zdvor'"><xsl:value-of select="$loc-cspriznakzdvor"/></xsl:when>
        <xsl:when test="$text='zhrubely'"><xsl:value-of select="$loc-cspriznakzhrubely"/></xsl:when>
        <xsl:when test="$text='zert'"><xsl:value-of select="$loc-cspriznakzert"/></xsl:when>
        <xsl:when test="$text='zlomkovy'"><xsl:value-of select="$loc-cspriznakzlomkovy"/></xsl:when>
        <xsl:when test="$text='zdrob'"><xsl:value-of select="$loc-cspriznakzdrob"/></xsl:when>
        <xsl:when test="$text='zastar'"><xsl:value-of select="$loc-cspriznakzastar"/></xsl:when>
        <xsl:when test="$text='cirkev'"><xsl:value-of select="$loc-cspriznakcirkev"/></xsl:when>
      </xsl:choose>    
    </xsl:if>
  </xsl:template>

  <xsl:template name="split_oblast">
    <xsl:param name="text"/>
    <xsl:variable name="first" select='substring-before($text,";")'/>
    <xsl:variable name='rest' select='substring-after($text,";")'/>
    <xsl:if test='$first'>
      <xsl:choose>
        <xsl:when test="$first='anat'"><xsl:value-of select="$loc-csoblastanat"/></xsl:when>
        <xsl:when test="$first='antr'"><xsl:value-of select="$loc-csoblastantr"/></xsl:when>
        <xsl:when test="$first='archeol'"><xsl:value-of select="$loc-csoblastarcheol"/></xsl:when>
        <xsl:when test="$first='archit'"><xsl:value-of select="$loc-csoblastarchit"/></xsl:when>
        <xsl:when test="$first='biol'"><xsl:value-of select="$loc-csoblastbiol"/></xsl:when>
        <xsl:when test="$first='bot'"><xsl:value-of select="$loc-csoblastbot"/></xsl:when>
        <xsl:when test="$first='dipl'"><xsl:value-of select="$loc-csoblastdipl"/></xsl:when>
        <xsl:when test="$first='div'"><xsl:value-of select="$loc-csoblastdiv"/></xsl:when>
        <xsl:when test="$first='dopr'"><xsl:value-of select="$loc-csoblastdopr"/></xsl:when>
        <xsl:when test="$first='ekol'"><xsl:value-of select="$loc-csoblastekol"/></xsl:when>
        <xsl:when test="$first='ekon'"><xsl:value-of select="$loc-csoblastekon"/></xsl:when>
        <xsl:when test="$first='eltech'"><xsl:value-of select="$loc-cateltech"/></xsl:when>
        <xsl:when test="$first='etn'"><xsl:value-of select="$loc-csoblastetn"/></xsl:when>
        <xsl:when test="$first='feud'"><xsl:value-of select="$loc-csoblastfeud"/></xsl:when>
        <xsl:when test="$first='filat'"><xsl:value-of select="$loc-csoblastfilat"/></xsl:when>
        <xsl:when test="$first='film'"><xsl:value-of select="$loc-csoblastfilm"/></xsl:when>
        <xsl:when test="$first='filoz'"><xsl:value-of select="$loc-csoblastfiloz"/></xsl:when>
        <xsl:when test="$first='fot'"><xsl:value-of select="$loc-csoblastfot"/></xsl:when>
        <xsl:when test="$first='fyz'"><xsl:value-of select="$loc-csoblastfyz"/></xsl:when>
        <xsl:when test="$first='fyziol'"><xsl:value-of select="$loc-csoblastfyziol"/></xsl:when>
        <xsl:when test="$first='geol'"><xsl:value-of select="$loc-csoblastgeol"/></xsl:when>
        <xsl:when test="$first='geom'"><xsl:value-of select="$loc-csoblastgeom"/></xsl:when>
        <xsl:when test="$first='gnoz'"><xsl:value-of select="$loc-csoblastgnoz"/></xsl:when>
        <xsl:when test="$first='hist'"><xsl:value-of select="$loc-csoblasthist"/></xsl:when>
        <xsl:when test="$first='horn'"><xsl:value-of select="$loc-csoblasthorn"/></xsl:when>
        <xsl:when test="$first='horol'"><xsl:value-of select="$loc-csoblasthorol"/></xsl:when>
        <xsl:when test="$first='hosp'"><xsl:value-of select="$loc-csoblasthosp"/></xsl:when>
        <xsl:when test="$first='hud'"><xsl:value-of select="$loc-csoblasthud"/></xsl:when>
        <xsl:when test="$first='hut'"><xsl:value-of select="$loc-csoblasthut"/></xsl:when>
        <xsl:when test="$first='hvězd'"><xsl:value-of select="$loc-csoblasthvězd"/></xsl:when>
        <xsl:when test="$first='chem'"><xsl:value-of select="$loc-csoblastchem"/></xsl:when>
        <xsl:when test="$first='ideal'"><xsl:value-of select="$loc-csoblastideal"/></xsl:when>
        <xsl:when test="$first='informatika'"><xsl:value-of select="$loc-csoblastinformatika"/></xsl:when>
        <xsl:when test="$first='jad'"><xsl:value-of select="$loc-csoblastjad"/></xsl:when>
        <xsl:when test="$first='jaz'"><xsl:value-of select="$loc-csoblastjaz"/></xsl:when>
        <xsl:when test="$first='kapit'"><xsl:value-of select="$loc-csoblastkapit"/></xsl:when>
        <xsl:when test="$first='karet'"><xsl:value-of select="$loc-csoblastkaret"/></xsl:when>
        <xsl:when test="$first='katolcírkvi'"><xsl:value-of select="$loc-csoblastkatolcírkvi"/></xsl:when>
        <xsl:when test="$first='krim'"><xsl:value-of select="$loc-csoblastkrim"/></xsl:when>
        <xsl:when test="$first='křesť'"><xsl:value-of select="$loc-csoblastkřesť"/></xsl:when>
        <xsl:when test="$first='kuch'"><xsl:value-of select="$loc-csoblastkuch"/></xsl:when>
        <xsl:when test="$first='kult'"><xsl:value-of select="$loc-csoblastkult"/></xsl:when>
        <xsl:when test="$first='kyb'"><xsl:value-of select="$loc-csoblastkyb"/></xsl:when>
        <xsl:when test="$first='lék'"><xsl:value-of select="$loc-csoblastlék"/></xsl:when>
        <xsl:when test="$first='lékár'"><xsl:value-of select="$loc-csoblastlékár"/></xsl:when>
        <xsl:when test="$first='let'"><xsl:value-of select="$loc-csoblastlet"/></xsl:when>
        <xsl:when test="$first='liter'"><xsl:value-of select="$loc-catliter"/></xsl:when>
        <xsl:when test="$first='log'"><xsl:value-of select="$loc-csoblastlog"/></xsl:when>
        <xsl:when test="$first='marx'"><xsl:value-of select="$loc-csoblastmarx"/></xsl:when>
        <xsl:when test="$first='mat'"><xsl:value-of select="$loc-csoblastmat"/></xsl:when>
        <xsl:when test="$first='meteor'"><xsl:value-of select="$loc-csoblastmeteor"/></xsl:when>
        <xsl:when test="$first='miner'"><xsl:value-of select="$loc-csoblastminer"/></xsl:when>
        <xsl:when test="$first='motor'"><xsl:value-of select="$loc-csoblastmotor"/></xsl:when>
        <xsl:when test="$first='mysl'"><xsl:value-of select="$loc-csoblastmysl"/></xsl:when>
        <xsl:when test="$first='mytol'"><xsl:value-of select="$loc-csoblastmytol"/></xsl:when>
        <xsl:when test="$first='náb'"><xsl:value-of select="$loc-csoblastnáb"/></xsl:when>
        <xsl:when test="$first='nár'"><xsl:value-of select="$loc-csoblastnár"/></xsl:when>
        <xsl:when test="$first='obch'"><xsl:value-of select="$loc-csoblastobch"/></xsl:when>
        <xsl:when test="$first='pedag'"><xsl:value-of select="$loc-csoblastpedag"/></xsl:when>
        <xsl:when test="$first='peněž'"><xsl:value-of select="$loc-csoblastpeněž"/></xsl:when>
        <xsl:when test="$first='podnikani'"><xsl:value-of select="$loc-csoblastpodnikani"/></xsl:when>
        <xsl:when test="$first='polit'"><xsl:value-of select="$loc-csoblastpolit"/></xsl:when>
        <xsl:when test="$first='polygr'"><xsl:value-of select="$loc-csoblastpolygr"/></xsl:when>
        <xsl:when test="$first='pošt'"><xsl:value-of select="$loc-csoblastpošt"/></xsl:when>
        <xsl:when test="$first='potrav'"><xsl:value-of select="$loc-csoblastpotrav"/></xsl:when>
        <xsl:when test="$first='práv'"><xsl:value-of select="$loc-csoblastpráv"/></xsl:when>
        <xsl:when test="$first='prům'"><xsl:value-of select="$loc-csoblastprům"/></xsl:when>
        <xsl:when test="$first='přír'"><xsl:value-of select="$loc-csoblastpřír"/></xsl:when>
        <xsl:when test="$first='psych'"><xsl:value-of select="$loc-csoblastpsych"/></xsl:when>
        <xsl:when test="$first='rybn'"><xsl:value-of select="$loc-csoblastrybn"/></xsl:when>
        <xsl:when test="$first='řem'"><xsl:value-of select="$loc-csoblastřem"/></xsl:when>
        <xsl:when test="$first='sklář'"><xsl:value-of select="$loc-csoblastsklář"/></xsl:when>
        <xsl:when test="$first='soc'"><xsl:value-of select="$loc-csoblastsoc"/></xsl:when>
        <xsl:when test="$first='sociol'"><xsl:value-of select="$loc-csoblastsociol"/></xsl:when>
        <xsl:when test="$first='stat'"><xsl:value-of select="$loc-csoblaststat"/></xsl:when>
        <xsl:when test="$first='stav'"><xsl:value-of select="$loc-csoblaststav"/></xsl:when>
        <xsl:when test="$first='škol'"><xsl:value-of select="$loc-csoblastškol"/></xsl:when>
        <xsl:when test="$first='tech'"><xsl:value-of select="$loc-csoblasttech"/></xsl:when>
        <xsl:when test="$first='těl'"><xsl:value-of select="$loc-csoblasttěl"/></xsl:when>
        <xsl:when test="$first='text'"><xsl:value-of select="$loc-csoblasttext"/></xsl:when>
        <xsl:when test="$first='úč'"><xsl:value-of select="$loc-csoblastúč"/></xsl:when>
        <xsl:when test="$first='úř'"><xsl:value-of select="$loc-csoblastúř"/></xsl:when>
        <xsl:when test="$first='veřspr'"><xsl:value-of select="$loc-csoblastveřspr"/></xsl:when>
        <xsl:when test="$first='vet'"><xsl:value-of select="$loc-csoblastvet"/></xsl:when>
        <xsl:when test="$first='voj'"><xsl:value-of select="$loc-csoblastvoj"/></xsl:when>
        <xsl:when test="$first='výptech'"><xsl:value-of select="$loc-csoblastvýptech"/></xsl:when>
        <xsl:when test="$first='výr'"><xsl:value-of select="$loc-csoblastvýr"/></xsl:when>
        <xsl:when test="$first='výtv'"><xsl:value-of select="$loc-csoblastvýtv"/></xsl:when>
        <xsl:when test="$first='zahr'"><xsl:value-of select="$loc-csoblastzahr"/></xsl:when>
        <xsl:when test="$first='zbož'"><xsl:value-of select="$loc-csoblastzbož"/></xsl:when>
        <xsl:when test="$first='zeměd'"><xsl:value-of select="$loc-csoblastzeměd"/></xsl:when>
        <xsl:when test="$first='zeměp'"><xsl:value-of select="$loc-csoblastzeměp"/></xsl:when>
        <xsl:when test="$first='zool'"><xsl:value-of select="$loc-csoblastzool"/></xsl:when>
        <xsl:when test="$first='cirkev'"><xsl:value-of select="$loc-csoblastcirkev"/></xsl:when>
        <xsl:when test="$first='adm'"><xsl:value-of select="$loc-csoblastadm"/></xsl:when>
        <xsl:when test="$first='artistika'"><xsl:value-of select="$loc-csoblastartistika"/></xsl:when>
        <xsl:when test="$first='astro'"><xsl:value-of select="$loc-csoblastastro"/></xsl:when>
        <xsl:when test="$first='biblhist'"><xsl:value-of select="$loc-csoblastbiblhist"/></xsl:when>
        <xsl:when test="$first='bibl'"><xsl:value-of select="$loc-csoblastbibl"/></xsl:when>
        <xsl:when test="$first='biochemie'"><xsl:value-of select="$loc-csoblastbiochemie"/></xsl:when>
        <xsl:when test="$first='cirar'"><xsl:value-of select="$loc-csoblastcirar"/></xsl:when>
        <xsl:when test="$first='cirhi'"><xsl:value-of select="$loc-csoblastcirhi"/></xsl:when>
        <xsl:when test="$first='cirhu'"><xsl:value-of select="$loc-csoblastcirhu"/></xsl:when>
        <xsl:when test="$first='cirkr'"><xsl:value-of select="$loc-csoblastcirkr"/></xsl:when>
        <xsl:when test="$first='cirpol'"><xsl:value-of select="$loc-csoblastcirpol"/></xsl:when>
        <xsl:when test="$first='cirpr'"><xsl:value-of select="$loc-csoblastcirpr"/></xsl:when>
        <xsl:when test="$first='cukr'"><xsl:value-of select="$loc-csoblastcukr"/></xsl:when>
        <xsl:when test="$first='ekpub'"><xsl:value-of select="$loc-csoblastekpub"/></xsl:when>
        <xsl:when test="$first='eltech'"><xsl:value-of select="$loc-csoblasteltech"/></xsl:when>
        <xsl:when test="$first='estet'"><xsl:value-of select="$loc-csoblastestet"/></xsl:when>
        <xsl:when test="$first='farmak'"><xsl:value-of select="$loc-csoblastfarmak"/></xsl:when>
        <xsl:when test="$first='folklor'"><xsl:value-of select="$loc-csoblastfolklor"/></xsl:when>
        <xsl:when test="$first='fonetika'"><xsl:value-of select="$loc-csoblastfonetika"/></xsl:when>
        <xsl:when test="$first='fyzchem'"><xsl:value-of select="$loc-csoblastfyzchem"/></xsl:when>
        <xsl:when test="$first='genetika'"><xsl:value-of select="$loc-csoblastgenetika"/></xsl:when>
        <xsl:when test="$first='geochem'"><xsl:value-of select="$loc-csoblastgeochem"/></xsl:when>
        <xsl:when test="$first='geodez'"><xsl:value-of select="$loc-csoblastgeodez"/></xsl:when>
        <xsl:when test="$first='geofyz'"><xsl:value-of select="$loc-csoblastgeofyz"/></xsl:when>
        <xsl:when test="$first='geogr'"><xsl:value-of select="$loc-csoblastgeogr"/></xsl:when>
        <xsl:when test="$first='geolzem'"><xsl:value-of select="$loc-csoblastgeolzem"/></xsl:when>
        <xsl:when test="$first='hiadm'"><xsl:value-of select="$loc-csoblasthiadm"/></xsl:when>
        <xsl:when test="$first='hiar'"><xsl:value-of select="$loc-csoblasthiar"/></xsl:when>
        <xsl:when test="$first='hiast'"><xsl:value-of select="$loc-csoblasthiast"/></xsl:when>
        <xsl:when test="$first='hidip'"><xsl:value-of select="$loc-csoblasthidip"/></xsl:when>
        <xsl:when test="$first='hidiv'"><xsl:value-of select="$loc-csoblasthidiv"/></xsl:when>
        <xsl:when test="$first='hieko'"><xsl:value-of select="$loc-csoblasthieko"/></xsl:when>
        <xsl:when test="$first='hietno'"><xsl:value-of select="$loc-csoblasthietno"/></xsl:when>
        <xsl:when test="$first='hifil'"><xsl:value-of select="$loc-csoblasthifil"/></xsl:when>
        <xsl:when test="$first='hifot'"><xsl:value-of select="$loc-csoblasthifot"/></xsl:when>
        <xsl:when test="$first='higeo'"><xsl:value-of select="$loc-csoblasthigeo"/></xsl:when>
        <xsl:when test="$first='hilit'"><xsl:value-of select="$loc-csoblasthilit"/></xsl:when>
        <xsl:when test="$first='himat'"><xsl:value-of select="$loc-csoblasthimat"/></xsl:when>
        <xsl:when test="$first='hinab'"><xsl:value-of select="$loc-csoblasthinab"/></xsl:when>
        <xsl:when test="$first='hipen'"><xsl:value-of select="$loc-csoblasthipen"/></xsl:when>
        <xsl:when test="$first='hipol'"><xsl:value-of select="$loc-csoblasthipol"/></xsl:when>
        <xsl:when test="$first='hipr'"><xsl:value-of select="$loc-csoblasthipr"/></xsl:when>
        <xsl:when test="$first='hisk'"><xsl:value-of select="$loc-csoblasthisk"/></xsl:when>
        <xsl:when test="$first='hispo'"><xsl:value-of select="$loc-csoblasthispo"/></xsl:when>
        <xsl:when test="$first='hista'"><xsl:value-of select="$loc-csoblasthista"/></xsl:when>
        <xsl:when test="$first='hitech'"><xsl:value-of select="$loc-csoblasthitech"/></xsl:when>
        <xsl:when test="$first='hivoj'"><xsl:value-of select="$loc-csoblasthivoj"/></xsl:when>
        <xsl:when test="$first='sach'"><xsl:value-of select="$loc-csoblastsach"/></xsl:when>
        <xsl:when test="$first='hudpubl'"><xsl:value-of select="$loc-csoblasthudpubl"/></xsl:when>
        <xsl:when test="$first='jadtech'"><xsl:value-of select="$loc-csoblastjadtech"/></xsl:when>
        <xsl:when test="$first='keram'"><xsl:value-of select="$loc-csoblastkeram"/></xsl:when>
        <xsl:when test="$first='kniho'"><xsl:value-of select="$loc-csoblastkniho"/></xsl:when>
        <xsl:when test="$first='kosmet'"><xsl:value-of select="$loc-csoblastkosmet"/></xsl:when>
        <xsl:when test="$first='kosmon'"><xsl:value-of select="$loc-csoblastkosmon"/></xsl:when>
        <xsl:when test="$first='kozed'"><xsl:value-of select="$loc-csoblastkozed"/></xsl:when>
        <xsl:when test="$first='krejc'"><xsl:value-of select="$loc-csoblastkrejc"/></xsl:when>
        <xsl:when test="$first='kynol'"><xsl:value-of select="$loc-csoblastkynol"/></xsl:when>
        <xsl:when test="$first='lesnic'"><xsl:value-of select="$loc-csoblastlesnic"/></xsl:when>
        <xsl:when test="$first='lethist'"><xsl:value-of select="$loc-csoblastlethist"/></xsl:when>
        <xsl:when test="$first='lingv'"><xsl:value-of select="$loc-csoblastlingv"/></xsl:when>
        <xsl:when test="$first='liter'"><xsl:value-of select="$loc-catliter"/></xsl:when>
        <xsl:when test="$first='lodar'"><xsl:value-of select="$loc-csoblastlodar"/></xsl:when>
        <xsl:when test="$first='matlog'"><xsl:value-of select="$loc-csoblastmatlog"/></xsl:when>
        <xsl:when test="$first='mezobch'"><xsl:value-of select="$loc-csoblastmezobch"/></xsl:when>
        <xsl:when test="$first='mezpr'"><xsl:value-of select="$loc-csoblastmezpr"/></xsl:when>
        <xsl:when test="$first='nabpol'"><xsl:value-of select="$loc-csoblastnabpol"/></xsl:when>
        <xsl:when test="$first='nabps'"><xsl:value-of select="$loc-csoblastnabps"/></xsl:when>
        <xsl:when test="$first='namor'"><xsl:value-of select="$loc-csoblastnamor"/></xsl:when>
        <xsl:when test="$first='numiz'"><xsl:value-of select="$loc-csoblastnumiz"/></xsl:when>
        <xsl:when test="$first='obchpr'"><xsl:value-of select="$loc-csoblastobchpr"/></xsl:when>
        <xsl:when test="$first='obuv'"><xsl:value-of select="$loc-csoblastobuv"/></xsl:when>
        <xsl:when test="$first='odb'"><xsl:value-of select="$loc-csoblastodb"/></xsl:when>
        <xsl:when test="$first='optika'"><xsl:value-of select="$loc-csoblastoptika"/></xsl:when>
        <xsl:when test="$first='orient'"><xsl:value-of select="$loc-csoblastorient"/></xsl:when>
        <xsl:when test="$first='paleo'"><xsl:value-of select="$loc-csoblastpaleo"/></xsl:when>
        <xsl:when test="$first='papir'"><xsl:value-of select="$loc-csoblastpapir"/></xsl:when>
        <xsl:when test="$first='pedol'"><xsl:value-of select="$loc-csoblastpedol"/></xsl:when>
        <xsl:when test="$first='piv'"><xsl:value-of select="$loc-csoblastpiv"/></xsl:when>
        <xsl:when test="$first='politadm'"><xsl:value-of select="$loc-csoblastpolitadm"/></xsl:when>
        <xsl:when test="$first='politeko'"><xsl:value-of select="$loc-csoblastpoliteko"/></xsl:when>
        <xsl:when test="$first='potrobch'"><xsl:value-of select="$loc-csoblastpotrobch"/></xsl:when>
        <xsl:when test="$first='hipr'"><xsl:value-of select="$loc-csoblasthipr"/></xsl:when>
        <xsl:when test="$first='publ'"><xsl:value-of select="$loc-csoblastpubl"/></xsl:when>
        <xsl:when test="$first='rybar'"><xsl:value-of select="$loc-csoblastrybar"/></xsl:when>
        <xsl:when test="$first='sdel'"><xsl:value-of select="$loc-csoblastsdel"/></xsl:when>
        <xsl:when test="$first='sport'"><xsl:value-of select="$loc-csoblastsport"/></xsl:when>
        <xsl:when test="$first='sporpub'"><xsl:value-of select="$loc-csoblastsporpub"/></xsl:when>
        <xsl:when test="$first='stroj'"><xsl:value-of select="$loc-csoblaststroj"/></xsl:when>
        <xsl:when test="$first='tanec'"><xsl:value-of select="$loc-csoblasttanec"/></xsl:when>
        <xsl:when test="$first='teol'"><xsl:value-of select="$loc-csoblastteol"/></xsl:when>
        <xsl:when test="$first='tesn'"><xsl:value-of select="$loc-csoblasttesn"/></xsl:when>
        <xsl:when test="$first='textobch'"><xsl:value-of select="$loc-csoblasttextobch"/></xsl:when>
        <xsl:when test="$first='umel'"><xsl:value-of select="$loc-csoblastumel"/></xsl:when>
        <xsl:when test="$first='umrem'"><xsl:value-of select="$loc-csoblastumrem"/></xsl:when>
        <xsl:when test="$first='vcel'"><xsl:value-of select="$loc-csoblastvcel"/></xsl:when>
        <xsl:when test="$first='vinar'"><xsl:value-of select="$loc-csoblastvinar"/></xsl:when>
        <xsl:when test="$first='vodohos'"><xsl:value-of select="$loc-csoblastvodohos"/></xsl:when>
        <xsl:when test="$first='vojhist'"><xsl:value-of select="$loc-csoblastvojhist"/></xsl:when>
        <xsl:when test="$first='vojnam'"><xsl:value-of select="$loc-csoblastvojnam"/></xsl:when>
        <xsl:when test="$first='vor'"><xsl:value-of select="$loc-csoblastvor"/></xsl:when>
        <xsl:when test="$first='zeldop'"><xsl:value-of select="$loc-csoblastzeldop"/></xsl:when>
        <xsl:when test="$first='zpravpubl'"><xsl:value-of select="$loc-csoblastzpravpubl"/></xsl:when>
        <xsl:when test="$first='zurn'"><xsl:value-of select="$loc-csoblastzurn"/></xsl:when>
      </xsl:choose>    
      <xsl:if test="$rest">, </xsl:if>
    </xsl:if>
    <xsl:if test='$rest'>
      <xsl:call-template name='split_oblast'>
        <xsl:with-param name='text' select='$rest'/>
      </xsl:call-template>
    </xsl:if>
    <xsl:if test='not($rest)'>
      <xsl:choose>
        <xsl:when test="$text='anat'"><xsl:value-of select="$loc-csoblastanat"/></xsl:when>
        <xsl:when test="$text='antr'"><xsl:value-of select="$loc-csoblastantr"/></xsl:when>
        <xsl:when test="$text='archeol'"><xsl:value-of select="$loc-csoblastarcheol"/></xsl:when>
        <xsl:when test="$text='archit'"><xsl:value-of select="$loc-csoblastarchit"/></xsl:when>
        <xsl:when test="$text='biol'"><xsl:value-of select="$loc-csoblastbiol"/></xsl:when>
        <xsl:when test="$text='bot'"><xsl:value-of select="$loc-csoblastbot"/></xsl:when>
        <xsl:when test="$text='dipl'"><xsl:value-of select="$loc-csoblastdipl"/></xsl:when>
        <xsl:when test="$text='div'"><xsl:value-of select="$loc-csoblastdiv"/></xsl:when>
        <xsl:when test="$text='dopr'"><xsl:value-of select="$loc-csoblastdopr"/></xsl:when>
        <xsl:when test="$text='ekol'"><xsl:value-of select="$loc-csoblastekol"/></xsl:when>
        <xsl:when test="$text='ekon'"><xsl:value-of select="$loc-csoblastekon"/></xsl:when>
        <xsl:when test="$text='eltech'"><xsl:value-of select="$loc-csoblasteltech"/></xsl:when>
        <xsl:when test="$text='etn'"><xsl:value-of select="$loc-csoblastetn"/></xsl:when>
        <xsl:when test="$text='feud'"><xsl:value-of select="$loc-csoblastfeud"/></xsl:when>
        <xsl:when test="$text='filat'"><xsl:value-of select="$loc-csoblastfilat"/></xsl:when>
        <xsl:when test="$text='film'"><xsl:value-of select="$loc-csoblastfilm"/></xsl:when>
        <xsl:when test="$text='filoz'"><xsl:value-of select="$loc-csoblastfiloz"/></xsl:when>
        <xsl:when test="$text='fot'"><xsl:value-of select="$loc-csoblastfot"/></xsl:when>
        <xsl:when test="$text='fyz'"><xsl:value-of select="$loc-csoblastfyz"/></xsl:when>
        <xsl:when test="$text='fyziol'"><xsl:value-of select="$loc-csoblastfyziol"/></xsl:when>
        <xsl:when test="$text='geol'"><xsl:value-of select="$loc-csoblastgeol"/></xsl:when>
        <xsl:when test="$text='geom'"><xsl:value-of select="$loc-csoblastgeom"/></xsl:when>
        <xsl:when test="$text='gnoz'"><xsl:value-of select="$loc-csoblastgnoz"/></xsl:when>
        <xsl:when test="$text='hist'"><xsl:value-of select="$loc-csoblasthist"/></xsl:when>
        <xsl:when test="$text='horn'"><xsl:value-of select="$loc-csoblasthorn"/></xsl:when>
        <xsl:when test="$text='horol'"><xsl:value-of select="$loc-csoblasthorol"/></xsl:when>
        <xsl:when test="$text='hosp'"><xsl:value-of select="$loc-csoblasthosp"/></xsl:when>
        <xsl:when test="$text='hud'"><xsl:value-of select="$loc-csoblasthud"/></xsl:when>
        <xsl:when test="$text='hut'"><xsl:value-of select="$loc-csoblasthut"/></xsl:when>
        <xsl:when test="$text='hvězd'"><xsl:value-of select="$loc-csoblasthvězd"/></xsl:when>
        <xsl:when test="$text='chem'"><xsl:value-of select="$loc-csoblastchem"/></xsl:when>
        <xsl:when test="$text='ideal'"><xsl:value-of select="$loc-csoblastideal"/></xsl:when>
        <xsl:when test="$text='informatika'"><xsl:value-of select="$loc-csoblastinformatika"/></xsl:when>
        <xsl:when test="$text='jad'"><xsl:value-of select="$loc-csoblastjad"/></xsl:when>
        <xsl:when test="$text='jaz'"><xsl:value-of select="$loc-csoblastjaz"/></xsl:when>
        <xsl:when test="$text='kapit'"><xsl:value-of select="$loc-csoblastkapit"/></xsl:when>
        <xsl:when test="$text='karet'"><xsl:value-of select="$loc-csoblastkaret"/></xsl:when>
        <xsl:when test="$text='katolcírkvi'"><xsl:value-of select="$loc-csoblastkatolcírkvi"/></xsl:when>
        <xsl:when test="$text='krim'"><xsl:value-of select="$loc-csoblastkrim"/></xsl:when>
        <xsl:when test="$text='křesť'"><xsl:value-of select="$loc-csoblastkřesť"/></xsl:when>
        <xsl:when test="$text='kuch'"><xsl:value-of select="$loc-csoblastkuch"/></xsl:when>
        <xsl:when test="$text='kult'"><xsl:value-of select="$loc-csoblastkult"/></xsl:when>
        <xsl:when test="$text='kyb'"><xsl:value-of select="$loc-csoblastkyb"/></xsl:when>
        <xsl:when test="$text='lék'"><xsl:value-of select="$loc-csoblastlék"/></xsl:when>
        <xsl:when test="$text='lékár'"><xsl:value-of select="$loc-csoblastlékár"/></xsl:when>
        <xsl:when test="$text='let'"><xsl:value-of select="$loc-csoblastlet"/></xsl:when>
        <xsl:when test="$text='liter'"><xsl:value-of select="$loc-csoblastliter"/></xsl:when>
        <xsl:when test="$text='log'"><xsl:value-of select="$loc-csoblastlog"/></xsl:when>
        <xsl:when test="$text='marx'"><xsl:value-of select="$loc-csoblastmarx"/></xsl:when>
        <xsl:when test="$text='mat'"><xsl:value-of select="$loc-csoblastmat"/></xsl:when>
        <xsl:when test="$text='meteor'"><xsl:value-of select="$loc-csoblastmeteor"/></xsl:when>
        <xsl:when test="$text='miner'"><xsl:value-of select="$loc-csoblastminer"/></xsl:when>
        <xsl:when test="$text='motor'"><xsl:value-of select="$loc-csoblastmotor"/></xsl:when>
        <xsl:when test="$text='mysl'"><xsl:value-of select="$loc-csoblastmysl"/></xsl:when>
        <xsl:when test="$text='mytol'"><xsl:value-of select="$loc-csoblastmytol"/></xsl:when>
        <xsl:when test="$text='náb'"><xsl:value-of select="$loc-csoblastnáb"/></xsl:when>
        <xsl:when test="$text='nár'"><xsl:value-of select="$loc-csoblastnár"/></xsl:when>
        <xsl:when test="$text='obch'"><xsl:value-of select="$loc-csoblastobch"/></xsl:when>
        <xsl:when test="$text='pedag'"><xsl:value-of select="$loc-csoblastpedag"/></xsl:when>
        <xsl:when test="$text='peněž'"><xsl:value-of select="$loc-csoblastpeněž"/></xsl:when>
        <xsl:when test="$text='podnikani'"><xsl:value-of select="$loc-csoblastpodnikani"/></xsl:when>
        <xsl:when test="$text='polit'"><xsl:value-of select="$loc-csoblastpolit"/></xsl:when>
        <xsl:when test="$text='polygr'"><xsl:value-of select="$loc-csoblastpolygr"/></xsl:when>
        <xsl:when test="$text='pošt'"><xsl:value-of select="$loc-csoblastpošt"/></xsl:when>
        <xsl:when test="$text='potrav'"><xsl:value-of select="$loc-csoblastpotrav"/></xsl:when>
        <xsl:when test="$text='práv'"><xsl:value-of select="$loc-csoblastpráv"/></xsl:when>
        <xsl:when test="$text='prům'"><xsl:value-of select="$loc-csoblastprům"/></xsl:when>
        <xsl:when test="$text='přír'"><xsl:value-of select="$loc-csoblastpřír"/></xsl:when>
        <xsl:when test="$text='psych'"><xsl:value-of select="$loc-csoblastpsych"/></xsl:when>
        <xsl:when test="$text='rybn'"><xsl:value-of select="$loc-csoblastrybn"/></xsl:when>
        <xsl:when test="$text='řem'"><xsl:value-of select="$loc-csoblastřem"/></xsl:when>
        <xsl:when test="$text='sklář'"><xsl:value-of select="$loc-csoblastsklář"/></xsl:when>
        <xsl:when test="$text='soc'"><xsl:value-of select="$loc-csoblastsoc"/></xsl:when>
        <xsl:when test="$text='sociol'"><xsl:value-of select="$loc-csoblastsociol"/></xsl:when>
        <xsl:when test="$text='stat'"><xsl:value-of select="$loc-csoblaststat"/></xsl:when>
        <xsl:when test="$text='stav'"><xsl:value-of select="$loc-csoblaststav"/></xsl:when>
        <xsl:when test="$text='škol'"><xsl:value-of select="$loc-csoblastškol"/></xsl:when>
        <xsl:when test="$text='tech'"><xsl:value-of select="$loc-csoblasttech"/></xsl:when>
        <xsl:when test="$text='těl'"><xsl:value-of select="$loc-csoblasttěl"/></xsl:when>
        <xsl:when test="$text='text'"><xsl:value-of select="$loc-csoblasttext"/></xsl:when>
        <xsl:when test="$text='úč'"><xsl:value-of select="$loc-csoblastúč"/></xsl:when>
        <xsl:when test="$text='úř'"><xsl:value-of select="$loc-csoblastúř"/></xsl:when>
        <xsl:when test="$text='veřspr'"><xsl:value-of select="$loc-csoblastveřspr"/></xsl:when>
        <xsl:when test="$text='vet'"><xsl:value-of select="$loc-csoblastvet"/></xsl:when>
        <xsl:when test="$text='voj'"><xsl:value-of select="$loc-csoblastvoj"/></xsl:when>
        <xsl:when test="$text='výptech'"><xsl:value-of select="$loc-csoblastvýptech"/></xsl:when>
        <xsl:when test="$text='výr'"><xsl:value-of select="$loc-csoblastvýr"/></xsl:when>
        <xsl:when test="$text='výtv'"><xsl:value-of select="$loc-csoblastvýtv"/></xsl:when>
        <xsl:when test="$text='zahr'"><xsl:value-of select="$loc-csoblastzahr"/></xsl:when>
        <xsl:when test="$text='zbož'"><xsl:value-of select="$loc-csoblastzbož"/></xsl:when>
        <xsl:when test="$text='zeměd'"><xsl:value-of select="$loc-csoblastzeměd"/></xsl:when>
        <xsl:when test="$text='zeměp'"><xsl:value-of select="$loc-csoblastzeměp"/></xsl:when>
        <xsl:when test="$text='zool'"><xsl:value-of select="$loc-csoblastzool"/></xsl:when>
        <xsl:when test="$text='cirkev'"><xsl:value-of select="$loc-csoblastcirkev"/></xsl:when>
        <xsl:when test="$text='adm'"><xsl:value-of select="$loc-csoblastadm"/></xsl:when>
        <xsl:when test="$text='artistika'"><xsl:value-of select="$loc-csoblastartistika"/></xsl:when>
        <xsl:when test="$text='astro'"><xsl:value-of select="$loc-csoblastastro"/></xsl:when>
        <xsl:when test="$text='biblhist'"><xsl:value-of select="$loc-csoblastbiblhist"/></xsl:when>
        <xsl:when test="$text='bibl'"><xsl:value-of select="$loc-csoblastbibl"/></xsl:when>
        <xsl:when test="$text='biochemie'"><xsl:value-of select="$loc-csoblastbiochemie"/></xsl:when>
        <xsl:when test="$text='cirar'"><xsl:value-of select="$loc-csoblastcirar"/></xsl:when>
        <xsl:when test="$text='cirhi'"><xsl:value-of select="$loc-csoblastcirhi"/></xsl:when>
        <xsl:when test="$text='cirhu'"><xsl:value-of select="$loc-csoblastcirhu"/></xsl:when>
        <xsl:when test="$text='cirkr'"><xsl:value-of select="$loc-csoblastcirkr"/></xsl:when>
        <xsl:when test="$text='cirpol'"><xsl:value-of select="$loc-csoblastcirpol"/></xsl:when>
        <xsl:when test="$text='cirpr'"><xsl:value-of select="$loc-csoblastcirpr"/></xsl:when>
        <xsl:when test="$text='cukr'"><xsl:value-of select="$loc-csoblastcukr"/></xsl:when>
        <xsl:when test="$text='ekpub'"><xsl:value-of select="$loc-csoblastekpub"/></xsl:when>
        <xsl:when test="$text='eltech'"><xsl:value-of select="$loc-cateltech"/></xsl:when>
        <xsl:when test="$text='estet'"><xsl:value-of select="$loc-csoblastestet"/></xsl:when>
        <xsl:when test="$text='farmak'"><xsl:value-of select="$loc-csoblastfarmak"/></xsl:when>
        <xsl:when test="$text='folklor'"><xsl:value-of select="$loc-csoblastfolklor"/></xsl:when>
        <xsl:when test="$text='fonetika'"><xsl:value-of select="$loc-csoblastfonetika"/></xsl:when>
        <xsl:when test="$text='fyzchem'"><xsl:value-of select="$loc-csoblastfyzchem"/></xsl:when>
        <xsl:when test="$text='genetika'"><xsl:value-of select="$loc-csoblastgenetika"/></xsl:when>
        <xsl:when test="$text='geochem'"><xsl:value-of select="$loc-csoblastgeochem"/></xsl:when>
        <xsl:when test="$text='geodez'"><xsl:value-of select="$loc-csoblastgeodez"/></xsl:when>
        <xsl:when test="$text='geofyz'"><xsl:value-of select="$loc-csoblastgeofyz"/></xsl:when>
        <xsl:when test="$text='geogr'"><xsl:value-of select="$loc-csoblastgeogr"/></xsl:when>
        <xsl:when test="$text='geolzem'"><xsl:value-of select="$loc-csoblastgeolzem"/></xsl:when>
        <xsl:when test="$text='hiadm'"><xsl:value-of select="$loc-csoblasthiadm"/></xsl:when>
        <xsl:when test="$text='hiar'"><xsl:value-of select="$loc-csoblasthiar"/></xsl:when>
        <xsl:when test="$text='hiast'"><xsl:value-of select="$loc-csoblasthiast"/></xsl:when>
        <xsl:when test="$text='hidip'"><xsl:value-of select="$loc-csoblasthidip"/></xsl:when>
        <xsl:when test="$text='hidiv'"><xsl:value-of select="$loc-csoblasthidiv"/></xsl:when>
        <xsl:when test="$text='hieko'"><xsl:value-of select="$loc-csoblasthieko"/></xsl:when>
        <xsl:when test="$text='hietno'"><xsl:value-of select="$loc-csoblasthietno"/></xsl:when>
        <xsl:when test="$text='hifil'"><xsl:value-of select="$loc-csoblasthifil"/></xsl:when>
        <xsl:when test="$text='hifot'"><xsl:value-of select="$loc-csoblasthifot"/></xsl:when>
        <xsl:when test="$text='higeo'"><xsl:value-of select="$loc-csoblasthigeo"/></xsl:when>
        <xsl:when test="$text='hilit'"><xsl:value-of select="$loc-csoblasthilit"/></xsl:when>
        <xsl:when test="$text='himat'"><xsl:value-of select="$loc-csoblasthimat"/></xsl:when>
        <xsl:when test="$text='hinab'"><xsl:value-of select="$loc-csoblasthinab"/></xsl:when>
        <xsl:when test="$text='hipen'"><xsl:value-of select="$loc-csoblasthipen"/></xsl:when>
        <xsl:when test="$text='hipol'"><xsl:value-of select="$loc-csoblasthipol"/></xsl:when>
        <xsl:when test="$text='hipr'"><xsl:value-of select="$loc-csoblasthipr"/></xsl:when>
        <xsl:when test="$text='hisk'"><xsl:value-of select="$loc-csoblasthisk"/></xsl:when>
        <xsl:when test="$text='hispo'"><xsl:value-of select="$loc-csoblasthispo"/></xsl:when>
        <xsl:when test="$text='hista'"><xsl:value-of select="$loc-csoblasthista"/></xsl:when>
        <xsl:when test="$text='hitech'"><xsl:value-of select="$loc-csoblasthitech"/></xsl:when>
        <xsl:when test="$text='hivoj'"><xsl:value-of select="$loc-csoblasthivoj"/></xsl:when>
        <xsl:when test="$text='sach'"><xsl:value-of select="$loc-csoblastsach"/></xsl:when>
        <xsl:when test="$text='hudpubl'"><xsl:value-of select="$loc-csoblasthudpubl"/></xsl:when>
        <xsl:when test="$text='jadtech'"><xsl:value-of select="$loc-csoblastjadtech"/></xsl:when>
        <xsl:when test="$text='keram'"><xsl:value-of select="$loc-csoblastkeram"/></xsl:when>
        <xsl:when test="$text='kniho'"><xsl:value-of select="$loc-csoblastkniho"/></xsl:when>
        <xsl:when test="$text='kosmet'"><xsl:value-of select="$loc-csoblastkosmet"/></xsl:when>
        <xsl:when test="$text='kosmon'"><xsl:value-of select="$loc-csoblastkosmon"/></xsl:when>
        <xsl:when test="$text='kozed'"><xsl:value-of select="$loc-csoblastkozed"/></xsl:when>
        <xsl:when test="$text='krejc'"><xsl:value-of select="$loc-csoblastkrejc"/></xsl:when>
        <xsl:when test="$text='kynol'"><xsl:value-of select="$loc-csoblastkynol"/></xsl:when>
        <xsl:when test="$text='lesnic'"><xsl:value-of select="$loc-csoblastlesnic"/></xsl:when>
        <xsl:when test="$text='lethist'"><xsl:value-of select="$loc-csoblastlethist"/></xsl:when>
        <xsl:when test="$text='lingv'"><xsl:value-of select="$loc-csoblastlingv"/></xsl:when>
        <xsl:when test="$text='liter'"><xsl:value-of select="$loc-csoblastliter"/></xsl:when>
        <xsl:when test="$text='lodar'"><xsl:value-of select="$loc-csoblastlodar"/></xsl:when>
        <xsl:when test="$text='matlog'"><xsl:value-of select="$loc-csoblastmatlog"/></xsl:when>
        <xsl:when test="$text='mezobch'"><xsl:value-of select="$loc-csoblastmezobch"/></xsl:when>
        <xsl:when test="$text='mezpr'"><xsl:value-of select="$loc-csoblastmezpr"/></xsl:when>
        <xsl:when test="$text='nabpol'"><xsl:value-of select="$loc-csoblastnabpol"/></xsl:when>
        <xsl:when test="$text='nabps'"><xsl:value-of select="$loc-csoblastnabps"/></xsl:when>
        <xsl:when test="$text='namor'"><xsl:value-of select="$loc-csoblastnamor"/></xsl:when>
        <xsl:when test="$text='numiz'"><xsl:value-of select="$loc-csoblastnumiz"/></xsl:when>
        <xsl:when test="$text='obchpr'"><xsl:value-of select="$loc-csoblastobchpr"/></xsl:when>
        <xsl:when test="$text='obuv'"><xsl:value-of select="$loc-csoblastobuv"/></xsl:when>
        <xsl:when test="$text='odb'"><xsl:value-of select="$loc-csoblastodb"/></xsl:when>
        <xsl:when test="$text='optika'"><xsl:value-of select="$loc-csoblastoptika"/></xsl:when>
        <xsl:when test="$text='orient'"><xsl:value-of select="$loc-csoblastorient"/></xsl:when>
        <xsl:when test="$text='paleo'"><xsl:value-of select="$loc-csoblastpaleo"/></xsl:when>
        <xsl:when test="$text='papir'"><xsl:value-of select="$loc-csoblastpapir"/></xsl:when>
        <xsl:when test="$text='pedol'"><xsl:value-of select="$loc-csoblastpedol"/></xsl:when>
        <xsl:when test="$text='piv'"><xsl:value-of select="$loc-csoblastpiv"/></xsl:when>
        <xsl:when test="$text='politadm'"><xsl:value-of select="$loc-csoblastpolitadm"/></xsl:when>
        <xsl:when test="$text='politeko'"><xsl:value-of select="$loc-csoblastpoliteko"/></xsl:when>
        <xsl:when test="$text='potrobch'"><xsl:value-of select="$loc-csoblastpotrobch"/></xsl:when>
        <xsl:when test="$text='hipr'"><xsl:value-of select="$loc-csoblasthipr"/></xsl:when>
        <xsl:when test="$text='publ'"><xsl:value-of select="$loc-csoblastpubl"/></xsl:when>
        <xsl:when test="$text='rybar'"><xsl:value-of select="$loc-csoblastrybar"/></xsl:when>
        <xsl:when test="$text='sdel'"><xsl:value-of select="$loc-csoblastsdel"/></xsl:when>
        <xsl:when test="$text='sport'"><xsl:value-of select="$loc-csoblastsport"/></xsl:when>
        <xsl:when test="$text='sporpub'"><xsl:value-of select="$loc-csoblastsporpub"/></xsl:when>
        <xsl:when test="$text='stroj'"><xsl:value-of select="$loc-csoblaststroj"/></xsl:when>
        <xsl:when test="$text='tanec'"><xsl:value-of select="$loc-csoblasttanec"/></xsl:when>
        <xsl:when test="$text='teol'"><xsl:value-of select="$loc-csoblastteol"/></xsl:when>
        <xsl:when test="$text='tesn'"><xsl:value-of select="$loc-csoblasttesn"/></xsl:when>
        <xsl:when test="$text='textobch'"><xsl:value-of select="$loc-csoblasttextobch"/></xsl:when>
        <xsl:when test="$text='umel'"><xsl:value-of select="$loc-csoblastumel"/></xsl:when>
        <xsl:when test="$text='umrem'"><xsl:value-of select="$loc-csoblastumrem"/></xsl:when>
        <xsl:when test="$text='vcel'"><xsl:value-of select="$loc-csoblastvcel"/></xsl:when>
        <xsl:when test="$text='vinar'"><xsl:value-of select="$loc-csoblastvinar"/></xsl:when>
        <xsl:when test="$text='vodohos'"><xsl:value-of select="$loc-csoblastvodohos"/></xsl:when>
        <xsl:when test="$text='vojhist'"><xsl:value-of select="$loc-csoblastvojhist"/></xsl:when>
        <xsl:when test="$text='vojnam'"><xsl:value-of select="$loc-csoblastvojnam"/></xsl:when>
        <xsl:when test="$text='vor'"><xsl:value-of select="$loc-csoblastvor"/></xsl:when>
        <xsl:when test="$text='zeldop'"><xsl:value-of select="$loc-csoblastzeldop"/></xsl:when>
        <xsl:when test="$text='zpravpubl'"><xsl:value-of select="$loc-csoblastzpravpubl"/></xsl:when>
        <xsl:when test="$text='zurn'"><xsl:value-of select="$loc-csoblastzurn"/></xsl:when>
      </xsl:choose>    
    </xsl:if>
  </xsl:template>

  <xsl:template match="variant" mode="ingram">
  </xsl:template>

  <xsl:template match="variant">
    <xsl:if test="./@lemma_id!=''">
      <a href="/{$dictcode}?lang={$lang}&amp;action=search&amp;getdoc={@lemma_id}&amp;empty={$empty}"><xsl:value-of select="@title"/></a>
    </xsl:if>
    <xsl:if test="./@lemma_id='' or not(./@lemma_id)">
      <xsl:value-of select="."/>
    </xsl:if>
    <xsl:if test="position()!=last()">, </xsl:if>
  </xsl:template>

  <xsl:template match="revcolloc">
    <br/>
    <!--<div class="czj-lemma-preview-sm">-->
      <!--<div class="czj-lemma-preview-sm-header">
        <div class="alignleft"></div>
        <div class="alignright"><i class="fa fa-external-link"></i></div> 
      </div>-->  

      <span style="vertical-align:top; display:inline-block;">
        <span onclick="open_iframe('{@lemma_id}', '{$dictcode}')" style="cursor:pointer;color:blue;text-decoration:underline">
          <!--<xsl:value-of select="@lemma_id"/>-->
          <img src="/editor/img/spojeni.png" align="top"/>
          <span class="colloctitle" style="vertical-align:top; display:inline-block;">
            <xsl:value-of select="title"/>
          </span>
          <img src="/media/img/slovnik-expand.png" height="20"/>
        </span>
      </span>
      <div style="clear:both; margin-bottom: 5px;"></div>
    <!--</div>-->
    <iframe class="colloc-iframe" id="colloc{@lemma_id}" src="about:blank" style="display:none" onload="iframe_loaded('{@lemma_id}')"></iframe>
  </xsl:template>

  <xsl:template match="text">
    <xsl:apply-templates/>
  </xsl:template>
  <xsl:template match="text" mode="ingram">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="i">
    <i><xsl:apply-templates/></i>
  </xsl:template>
  <xsl:template match="i" mode="ingram">
    <i><xsl:apply-templates/></i>
  </xsl:template>

  <xsl:template match="small">
    <small><xsl:apply-templates/></small>
  </xsl:template>

  <xsl:template match="b">
    <b><xsl:apply-templates/></b>
  </xsl:template>

  <xsl:template match="sub">
    <sub><xsl:apply-templates/></sub>
  </xsl:template>

  <xsl:template match="sup">
    <sup><xsl:apply-templates/></sup>
  </xsl:template>
  <xsl:template match="small" mode="ingram">
    <small><xsl:apply-templates/></small>
  </xsl:template>

  <xsl:template match="b" mode="ingram">
    <b><xsl:apply-templates/></b>
  </xsl:template>

  <xsl:template match="sub" mode="ingram">
    <sub><xsl:apply-templates/></sub>
  </xsl:template>

  <xsl:template match="sup" mode="ingram">
    <sup><xsl:apply-templates/></sup>
  </xsl:template>

  <xsl:template match="link">
    <xsl:if test="not(link_mean)">
     <xsl:if test="@auto_complete='1' or $perm!='ro' or $skupina_test='true'">
       <a href="/{$dictcode}?lang={$lang}&amp;action=search&amp;getdoc={@lemma_id}&amp;empty={$empty}"><xsl:value-of select="."/></a><!-- tady to funguje špatně -->
     </xsl:if>
     <xsl:if test="not(@auto_complete='1' or $perm!='ro' or $skupina_test='true')">
      <xsl:value-of select="."/>
     </xsl:if>
    </xsl:if>
    <xsl:if test="link_mean">
      <xsl:value-of select="."/> 
      <xsl:if test="@auto_complete='1' or $perm!='ro' or $skupina_test='true'"> 
      (<xsl:for-each select="link_mean">
        <xsl:sort select="link_mean"/>
        <xsl:if test="@status='published' or $perm!='ro' or $skupina_test='true'">     
          <a href="/{$dictcode}?lang={$lang}&amp;action=search&amp;getdoc={../@lemma_id}&amp;empty={$empty}"><xsl:value-of select="$loc-vevyznamu"/> <xsl:value-of select="@number"/></a>
        </xsl:if>     
        <!--<xsl:if test="@auto_complete!='1' or $perm='ro'">ve významu <xsl:value-of select="@number"/>    
        </xsl:if> pokud není schválené heslo, toto se vůbec nezobrazí -->
      <xsl:if test="position()!=last()">, </xsl:if>
      </xsl:for-each><xsl:text>)</xsl:text>
    </xsl:if>
    </xsl:if>
  </xsl:template>
  <xsl:template match="link" mode="ingram">
    <xsl:if test="not(link_mean)">
      <a href="/{$dictcode}?lang={$lang}&amp;action=search&amp;getdoc={@lemma_id}&amp;empty={$empty}"><xsl:value-of select="."/></a>
    </xsl:if>
    <xsl:if test="link_mean">
    <xsl:value-of select="."/>
      (<xsl:for-each select="link_mean">
       <xsl:sort select="link_mean"/>
       <a href="/{$dictcode}?lang={$lang}&amp;action=search&amp;getdoc={../@lemma_id}&amp;empty={$empty}"><xsl:value-of select="$loc-vevyznamu"/> <xsl:value-of select="@number"/></a>
        <xsl:if test="position()!=last()">, </xsl:if>
      </xsl:for-each><xsl:text>)</xsl:text>
    </xsl:if>
  </xsl:template>

  <xsl:template match="html//*">
    <xsl:copy>
      <xsl:apply-templates/>
    </xsl:copy>
  </xsl:template>
  <xsl:template name="status_publish">
    <xsl:param name="status"/>
    <xsl:param name="type"/>
    <xsl:param name="element"/>
    <xsl:if test="$status='published'">
      <img src="/editor/img/checked.png" title="{$loc-schvaleno}" />
    </xsl:if>
    <xsl:if test="$status!='published'">
       <!--skryto--><xsl:text> </xsl:text>
      <xsl:choose>
        <xsl:when test="$type='relation'">
          <input type="button" class="button-small" data-type="{$element/@type}" onclick="publish_relation('{$dictcode}', '{/entry/@id}', '{$element/../@id}', '{$element/@meaning_id}', '{$element/@type}', this)" value="{$loc-schvalit}"/>
        </xsl:when>
        <xsl:when test="$type='usage'">
          <input type="button" class="button-small" data-type="usage" onclick="publish_usage('{$dictcode}', '{/entry/@id}', '{$element/../../@id}', '{$element/@id}', this)" value="{$loc-schvalit}"/>
        </xsl:when>
        <xsl:when test="$type='meaning'">
          <input type="button" class="button-small" data-type="meaning" onclick="publish_meaning('{$dictcode}', '{/entry/@id}', '{$element/@id}', this)" value="{$loc-schvalit}"/>
        </xsl:when>
        <xsl:when test="$type='gram' or $type='style' or $type='sw' or $type='hns' or $type='video_front' or $type='video_side' or $type='entry'">
          <input type="button" class="button-small" data-type="entry" onclick="publish('{$dictcode}', '{/entry/@id}', '{$type}', this)" value="{$loc-schvalit}"/>
        </xsl:when>
      </xsl:choose>
    </xsl:if>
  </xsl:template>

</xsl:stylesheet>
