<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output encoding="utf-8" method="xml"
    doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
    doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
    />

  <xsl:param name="perm"/>
  <xsl:param name="dictcode"/>
  <xsl:param name="public">false</xsl:param>
  <xsl:variable name="skupina_test">
    <!--ma uzivatel stejnou skupinu jako heslo? nebo bez omezeni-->
    <xsl:if test="contains(/entry/@user_skupina,'all') or contains($perm, 'admin')">true</xsl:if>
    <xsl:if test="$public!='true' and not(contains(/entry/@user_skupina,'all')) and not(contains($perm, 'admin'))"><xsl:value-of select="contains(/entry/@user_skupina, /entry/lemma/pracskupina)"/></xsl:if>
    <xsl:if test="$public='true'">false</xsl:if>
  </xsl:variable>

  <xsl:template match="entry">
    <html>
      <head>
        <title>ČJ: <xsl:value-of select="lemma/title"/></title>

        <link rel="stylesheet" type="text/css" media="screen" href="/editor/czjprev4.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="/editor/css/czj-common.css" ></link>
        <link rel="stylesheet" type="text/css" media="screen" href="/editor/css/cj-preview.css" ></link>
        <link href='/editor/css/czj-preview7-min.css' rel='stylesheet' type='text/css' media='screen'/>

        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Titillium+Web:400,200,300,600,700,900&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="/media/jwplayer/jwplayer.js" ></script>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    <script src="//releases.flowplayer.org/5.4.3/flowplayer.min.js"></script>
    <link rel="stylesheet" href="//releases.flowplayer.org/5.4.3/skin/minimalist.css"/>

    <script type="text/javascript" src="/media/fancybox/jquery.fancybox.pack.js"></script>
    <link rel="stylesheet" href="/media/fancybox/jquery.fancybox.css" type="text/css" media="screen" />

    <script type="text/javascript" src="/editor/imagesloaded.pkgd.min.js">//</script>
    <script type="text/javascript" src="/editor/czjmain.js">//</script>
    <script><![CDATA[
      function change_view(eid) {
        if (document.getElementById(eid).style.display=='none') {
          if (eid == 'lemma_parts_box') { document.getElementById('lemma_parts_info').innerHTML = 'skrýt podrobnosti'}
          document.getElementById(eid).style.display='block';
        } else {
          if (eid == 'lemma_parts_box') { document.getElementById('lemma_parts_info').innerHTML = 'zobrazit podrobnosti'}
          document.getElementById(eid).style.display='none';
        }
      }

      $(document).ready(function() {
      $(".flowmouse").bind("mouseenter mouseleave", function(e) {
        flowplayer(this)[/ee/.test(e.type) ? "resume":"pause"]();
        });
      });
     $(document).ready(function() {
        $('#navselect').change(function() {
          var option = $(this.options[this.selectedIndex]).val();
          $('#sidevideo').css('display', 'none');
          $('#sidesw').css('display', 'none');
          $('#sidehamnosys').css('display', 'none');
          $('#side'+option).css('display', 'block');
        });
        $('.topvideosign_side').hide();
        $("a.fancybox").fancybox({
        'autoScale'      : false,
        'transitionIn'   : 'elastic',
        'transitionOut'  : 'elastic',
        'titlePosition'  : 'inside',
        'hideOnContentClick' : true,
        'speedIn'        : 100,
        'speedOut'     : 100,
        'changeSpeed'    : 100,
        'centerOnScroll' : false,
        padding: 0,
        closeClick: true
        });

      function open_iframe(id, dict) {
        var iframe = document.getElementById('colloc'+id);
        if (iframe.style.display=='none') {
          iframe.style.display='inline';
        } else {
          iframe.style.display='none';
        }
        if (iframe.src == 'about:blank') {
      iframe.src = '/'+dict+'?action=getdoc&amp;id='+id+'&amp;tr=inline';
        }
      }
      function iframe_loaded(id) {
        var iframe = document.getElementById('colloc'+id);
        if (iframe.src != 'about:blank') {
          //alert(iframe.contentWindow.document.body.scrollHeight);
          iframe.height = iframe.contentWindow.document.body.scrollHeight + 'px;'
        }
      }

      ]]></script>
      </head>
      <body>

        <div id="outer">       

          <div id="side" style="position: fixed">
            <strong><xsl:value-of select="lemma/title"/></strong>
            <br/> 
            
<form method="get" action="/cs">
    <input type="hidden" name="action" value="search"/>
    <input type="hidden" name="type" value="text"/>
     <!--<input type="hidden" name="new" value="true"/>-->
    <input type="text" name="search" size="10" value=""/>
    <input type="submit" value="hledat"/>
    </form>
         <xsl:if test="$perm!='ro'"><a href="/editoren/?id={@id}&amp;lang=en">edit</a><br/></xsl:if>
          </div>
          <div id="inner">
            <div class="cj-mainlemma">
           <xsl:if test="(lemma/lemma_type='collocation' or lemma/lemma_type='prislovi') and ($perm!='ro' or $skupina_test='true' or collocations/@status!='hidden')">
             <xsl:attribute name="style">background-color: lightgrey;</xsl:attribute>
            </xsl:if>
           <xsl:if test="$perm!='ro'">
             <span class="set-status">ID: <xsl:value-of select="@id"/></span> 
              <span class="publish"> publishing: 
                <xsl:choose>
                   <xsl:when test="/entry/lemma/completeness='0'">automatic</xsl:when>
                   <xsl:when test="/entry/lemma/completeness='1'">hide all</xsl:when>                   
                   <xsl:when test="/entry/lemma/completeness='2'">non-empty parts only</xsl:when>
                   <xsl:when test="/entry/lemma/completeness='100'">approved parts only</xsl:when>
                </xsl:choose></span>                 
             </xsl:if>
             <span style="float: right">
               <xsl:if test="$perm!='ro'"><a href="/editoren/?id={@id}&amp;lang=en">edit<img src="/editor/img/edit.png" /></a></xsl:if>
               <xsl:choose>
                 <xsl:when test="lemma/@auto_complete='1'"><a style="color:blue;cursor:pointer" onclick="window.location = 'http://www.dictio.info'+window.location.pathname+window.location.search"><img src="/editor/publikovano.png" border="0" title="switch to public view"/></a></xsl:when>
                  <xsl:otherwise><img src="/editor/nepublikovano.png" title="lemma not published"/> </xsl:otherwise>
                </xsl:choose>
             </span> <br/>
            
           <xsl:if test="(lemma/lemma_type='collocation' or lemma/lemma_type='prislovi') and ($perm!='ro' or $skupina_test='true' or collocations/@status!='hidden')">
               <span id="lemma_parts"><strong><!-- Collocation --></strong><a style="cursor:pointer;color:blue"  onclick="change_view('lemma_parts_box')" id="lemma_parts_info">Show links to components.</a><br/>
                 <span id="lemma_parts_box" style="display:none">
                   <xsl:apply-templates select="collocations/colloc"/>
                 </span>
               </span>
             </xsl:if>
             <h1 style="display:inline"><xsl:value-of select="lemma/title"/><xsl:if test="/entry/lemma/title_var!=''">, <xsl:value-of select="lemma/title_var"/></xsl:if></h1>
             <xsl:if test="lemma/pron!=''"><span style="margin-left: 30px;">[<xsl:value-of select="lemma/pron"/>]</span></xsl:if>
              <!--schvalování-->
     <xsl:if test="$perm!='ro' or $skupina_test='true'">
              <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_lemmacj')">
                <input type="button" onclick="publish_all(this)" value="schválit hromadně"/>
               <br/>
               <span class="set-status">
                 lemma type: 
                 <xsl:choose>
                   <xsl:when test="/entry/lemma/lemma_type='single'">single</xsl:when>
                   <xsl:when test="/entry/lemma/lemma_type='predpona'">previx</xsl:when>
                   <xsl:when test="/entry/lemma/lemma_type='collocation'">collocation / phrase</xsl:when>
                   <xsl:when test="/entry/lemma/lemma_type='prislovi'">proverb</xsl:when>
                 </xsl:choose>
                 <xsl:call-template name="status_publish">
                   <xsl:with-param name="status" select="/entry/collocations/@status"/>
                   <xsl:with-param name="type">entry</xsl:with-param>
                 </xsl:call-template>
               </span>
             </xsl:if>
           </xsl:if><!--konec schvalovani-->
             
           </div> <!-- cj-mainlemma -->
           <xsl:if test="html and not(meanings/meaning)">
             <div>
               <span class="divtitle">Origin form</span>
               <div class="cj-grammar">
                 <xsl:apply-templates select="html"/>
               </div>
             </div>
           </xsl:if>
           <xsl:if test="lemma/grammar_note/text()!='' or lemma/style_note/text()!='' or lemma/grammar_note/variant or lemma/style_note/variant or lemma/grammar_note/@slovni_druh!='' or lemma/grammar_note/hyphen!='' or lemma/style_note/@kategorie!='' or lemma/style_note/@stylpriznak!=''">
             <div class="cj-gram-popis">
              <!-- <div class="cj-lemma-h1">Formální popis</div> -->
               <div class="cj-grammar">
               <!--schvalování-->
                 <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_cjlingvist')">
                   <span class="set-status">
                     gramatical description: 
                     <xsl:call-template name="status_publish">
                       <xsl:with-param name="status" select="/entry/lemma/grammar_note[1]/@status"/>
                       <xsl:with-param name="type">gram</xsl:with-param>
                     </xsl:call-template>
                   </span>
                   
                 </xsl:if><!--konec schvalování-->
   <div class="author-info" style="display:none">
      <xsl:if test="lemma/grammar_note/@source!=''"><span class="author-source">source: <xsl:value-of select="lemma/grammar_note/@source"/></span>; </xsl:if>
      <xsl:if test="lemma/grammar_note/@author!=''"><span class="author-author">author: <xsl:value-of select="lemma/grammar_note/@author"/></span>; </xsl:if>
      <xsl:if test="lemma/grammar_note/@copyright!=''"><span class="author-copyright">copyright: <xsl:value-of select="lemma/grammar_note/@copyright"/></span></xsl:if>
    </div>
             <br />
             <xsl:if test="lemma/puvod!=''">
               <div>
               <b>Word origin: </b>
               <xsl:choose>
                 <xsl:when test="lemma/puvod='a'">English</xsl:when>
                 <xsl:when test="lemma/puvod='afr'">African languages</xsl:when>
                 <xsl:when test="lemma/puvod='alb'">Albanian</xsl:when>
                 <xsl:when test="lemma/puvod='am'">American English</xsl:when>
                 <xsl:when test="lemma/puvod='ar'">Arabic</xsl:when>
                 <xsl:when test="lemma/puvod='aram'">Aramaic</xsl:when>
                 <xsl:when test="lemma/puvod='b'">Bulgarian</xsl:when>
                 <xsl:when test="lemma/puvod='bantu'">Bantu languages</xsl:when>
                 <xsl:when test="lemma/puvod='bask'">Basque</xsl:when>
                 <xsl:when test="lemma/puvod='br'">Belarusian</xsl:when>
                 <xsl:when test="lemma/puvod='cik'">Romani</xsl:when>
                 <xsl:when test="lemma/puvod='čes'">Czech</xsl:when>
                 <xsl:when test="lemma/puvod='čín'">Chinese</xsl:when>
                 <xsl:when test="lemma/puvod='dán'">Danish</xsl:when>
                 <xsl:when test="lemma/puvod='eg'">Egyptian</xsl:when>
                 <xsl:when test="lemma/puvod='est'">Estonian</xsl:when>
                 <xsl:when test="lemma/puvod='f'">French</xsl:when>
                 <xsl:when test="lemma/puvod='fin'">Finnish</xsl:when>
                 <xsl:when test="lemma/puvod='g'">Germanic languages</xsl:when>
                 <xsl:when test="lemma/puvod='hebr'">Hebrew</xsl:when>
                 <xsl:when test="lemma/puvod='hind'">Hindi</xsl:when>
                 <xsl:when test="lemma/puvod='i'">Italian</xsl:when>
                 <xsl:when test="lemma/puvod='ind'">Indic languages</xsl:when>
                 <xsl:when test="lemma/puvod='indián'">Indian languages</xsl:when>
                 <xsl:when test="lemma/puvod='indonés'">Indonesian</xsl:when>
                 <xsl:when test="lemma/puvod='ir'">Irish</xsl:when>
                 <xsl:when test="lemma/puvod='island'">Icelandic</xsl:when>
                 <xsl:when test="lemma/puvod='j'">Japanese</xsl:when>
                 <xsl:when test="lemma/puvod='jakut'">Yakut language</xsl:when>
                 <xsl:when test="lemma/puvod='jslov'">South Slavic languages</xsl:when>
                 <xsl:when test="lemma/puvod='kelt'">Celtic languages</xsl:when>
                 <xsl:when test="lemma/puvod='korej'">Korean</xsl:when>
                 <xsl:when test="lemma/puvod='l'">Latin</xsl:when>
                 <xsl:when test="lemma/puvod='lfran'">Latin via French</xsl:when>
                 <xsl:when test="lemma/puvod='lapon'">Sami</xsl:when>
                 <xsl:when test="lemma/puvod='lit'">Lithuanian</xsl:when>
                 <xsl:when test="lemma/puvod='lot'">Latvian</xsl:when>
                 <xsl:when test="lemma/puvod='luž'">Sorbian</xsl:when>
                 <xsl:when test="lemma/puvod='maď'">Hungarian</xsl:when>
                 <xsl:when test="lemma/puvod='maked'">Macedonian</xsl:when>
                 <xsl:when test="lemma/puvod='mal'">Malay</xsl:when>
                 <xsl:when test="lemma/puvod='mong'">Mongolian</xsl:when>
                 <xsl:when test="lemma/puvod='n'">German</xsl:when>
                 <xsl:when test="lemma/puvod='niz'">Dutch</xsl:when>
                 <xsl:when test="lemma/puvod='NJ'">nation name</xsl:when>
                 <xsl:when test="lemma/puvod='nor'">Norwegian</xsl:when>
                 <xsl:when test="lemma/puvod='OJ'">personal name</xsl:when>
                 <xsl:when test="lemma/puvod='or'">oriental languages</xsl:when>
                 <xsl:when test="lemma/puvod='p'">Polish</xsl:when>
                 <xsl:when test="lemma/puvod='per'">Persian</xsl:when>
                 <xsl:when test="lemma/puvod='polynés'">Polynesian languages</xsl:when>
                 <xsl:when test="lemma/puvod='port'">Portuguese</xsl:when>
                 <xsl:when test="lemma/puvod='provens'">Provencal</xsl:when>
                 <xsl:when test="lemma/puvod='r'">Russian</xsl:when>
                 <xsl:when test="lemma/puvod='rom'">Romance languages</xsl:when>
                 <xsl:when test="lemma/puvod='rum'">Romanian</xsl:when>
                 <xsl:when test="lemma/puvod='ř'">Greek</xsl:when>
                 <xsl:when test="lemma/puvod='semit'">Semitic</xsl:when>
                 <xsl:when test="lemma/puvod='sch'">Serbo-Croatian</xsl:when>
                 <xsl:when test="lemma/puvod='skan'">Scandinavian languages</xsl:when>
                 <xsl:when test="lemma/puvod='sl'">Slovak</xsl:when>
                 <xsl:when test="lemma/puvod='sln'">Slovenian</xsl:when>
                 <xsl:when test="lemma/puvod='srb'">Serbian</xsl:when>
                 <xsl:when test="lemma/puvod='střl'">Medieval Latin</xsl:when>
                 <xsl:when test="lemma/puvod='svahil'">Swahili</xsl:when>
                 <xsl:when test="lemma/puvod='š'">Spanish</xsl:when>
                 <xsl:when test="lemma/puvod='šv'">Swedish</xsl:when>
                 <xsl:when test="lemma/puvod='t'">Turkish</xsl:when>
                 <xsl:when test="lemma/puvod='tat'">Tartar</xsl:when>
                 <xsl:when test="lemma/puvod='tib'">Tibetan</xsl:when>
                 <xsl:when test="lemma/puvod='ttat'">Turkic languages</xsl:when>
                 <xsl:when test="lemma/puvod='tung'">Tungusian</xsl:when>
                 <xsl:when test="lemma/puvod='u'">Ukrainian</xsl:when>
                 <xsl:when test="lemma/puvod='VJ'">proper noun</xsl:when>
                 <xsl:when test="lemma/puvod='vslov'">East Slavic languages</xsl:when>
                 <xsl:when test="lemma/puvod='ZJ'">geographic name</xsl:when>
                 <xsl:when test="lemma/puvod='zslov'">West Slavic languages</xsl:when>
                 <xsl:when test="lemma/puvod='žid'">Jewish jargon</xsl:when>
                 <xsl:otherwise><xsl:value-of select="lemma/puvod"/></xsl:otherwise>
               </xsl:choose>  </div>
             </xsl:if>
                 
          <xsl:if test="$perm!='ro' or $skupina_test='true' or lemma/grammar_note[1]/@status!='hidden'">
                   
                   <xsl:for-each select="lemma/grammar_note">
                     <xsl:if test="@slovni_druh!=''">
                      <xsl:if test="@slovni_druh='subst' or @slovni_druh='adj' or @slovni_druh='pron' or @slovni_druh='num' or @slovni_druh='verb' or @slovni_druh='prep' or @slovni_druh='kon' or @slovni_druh='par' or @slovni_druh='int'">
                     <b>Lexical category: </b>  </xsl:if>
                       <xsl:choose>
                        <xsl:when test="@slovni_druh='subst'">noun</xsl:when>
                         <xsl:when test="@slovni_druh='adj'">adjective</xsl:when>
                         <xsl:when test="@slovni_druh='pron'">pronoun</xsl:when>
                         <xsl:when test="@slovni_druh='num'">number</xsl:when>
                         <xsl:when test="@slovni_druh='verb'">verb</xsl:when>
                         <xsl:when test="@slovni_druh='adv'">adverb</xsl:when>
                         <xsl:when test="@slovni_druh='prep'">preposition</xsl:when>
                         <xsl:when test="@slovni_druh='ust'">set prepositional collocation</xsl:when>
                         <xsl:when test="@slovni_druh='kon'">conjunction</xsl:when>
                         <xsl:when test="@slovni_druh='par'">particle</xsl:when>
                         <xsl:when test="@slovni_druh='int'">interjection</xsl:when>
                         <xsl:when test="@slovni_druh='sprezka'">adverbial compound</xsl:when>
                         <xsl:when test="@slovni_druh='predpona'">prefix</xsl:when>
                         <xsl:when test="@slovni_druh='zkratka'">abbreviation</xsl:when>
                         <!--
                         <xsl:when test="@slovni_druh='ustalene'">set collocation</xsl:when>
                         <xsl:when test="@slovni_druh='prirovnani'">simile</xsl:when>
                         <xsl:when test="@slovni_druh='prislovi'">proverb</xsl:when>
                         <xsl:when test="@slovni_druh='porekadlo'">adage</xsl:when>
                         <xsl:when test="@slovni_druh='frazem'">phraseme</xsl:when>-->
                       </xsl:choose>
                       <xsl:if test="@skupina!=''">
                       <xsl:if test="@slovni_druh!='ustalene'">                        
                        <xsl:text>, </xsl:text>
                       </xsl:if>     
                         <xsl:call-template name="split_skupina"><xsl:with-param name="text" select="@skupina"/></xsl:call-template>
                       </xsl:if>
                       <xsl:if test="@skupina2!=''">
                         <xsl:text>, </xsl:text>
                         <xsl:call-template name="split_skupina2"><xsl:with-param name="text" select="@skupina2"/></xsl:call-template>
                       </xsl:if>
                     </xsl:if>
                     <xsl:if test="position()!=last()"><br/></xsl:if>
                   </xsl:for-each>
                   <xsl:if test="lemma/grammar_note/@flexe_neskl='true'">, indeclinable</xsl:if>
                     
                     <xsl:if test="lemma/grammar_note/variant">
                         <br/><b>
                        <xsl:if test="lemma/grammar_note/@slovni_druh!='ustalene'">Gramatical </xsl:if>
                         variant: </b>
                       <xsl:apply-templates select="lemma/grammar_note/variant"/>
                     </xsl:if>
                     <xsl:if test="normalize-space(lemma/grammar_note/text())!=''"><br/><xsl:apply-templates select="lemma/grammar_note" mode="ingram"/></xsl:if>
                   <xsl:if test="lemma/grammar_note/hyphen"><b>Division into syllables: </b><xsl:value-of select="lemma/grammar_note/hyphen"/><br/></xsl:if>
                   
                    <!--tabulka-->
                   <xsl:if test="lemma/gram/form">
                     <xsl:choose>
                       <xsl:when test="lemma/grammar_note/@slovni_druh='subst' or lemma/grammar_note/@slovni_druh=''">
                         <table id="cj-gram-table">
                           <tr><td></td><td>singular</td><td>plural</td></tr>
                           <tr><td>nominative</td><td><xsl:value-of select="lemma/gram/form[@tag='nSc1']"/></td><td><xsl:value-of select="lemma/gram/form[@tag='nPc1']"/></td></tr>
                           <tr><td>genitive</td><td><xsl:value-of select="lemma/gram/form[@tag='nSc2']"/></td><td><xsl:value-of select="lemma/gram/form[@tag='nPc2']"/></td></tr>
                           <tr><td>dative</td><td><xsl:value-of select="lemma/gram/form[@tag='nSc3']"/></td><td><xsl:value-of select="lemma/gram/form[@tag='nPc3']"/></td></tr>
                           <tr><td>accusative</td><td><xsl:value-of select="lemma/gram/form[@tag='nSc4']"/></td><td><xsl:value-of select="lemma/gram/form[@tag='nPc4']"/></td></tr>
                           <tr><td>vocative</td><td><xsl:value-of select="lemma/gram/form[@tag='nSc5']"/></td><td><xsl:value-of select="lemma/gram/form[@tag='nPc5']"/></td></tr>
                           <tr><td>locative</td><td><xsl:value-of select="lemma/gram/form[@tag='nSc6']"/></td><td><xsl:value-of select="lemma/gram/form[@tag='nPc6']"/></td></tr>
                           <tr><td>instrumental</td><td><xsl:value-of select="lemma/gram/form[@tag='nSc7']"/></td><td><xsl:value-of select="lemma/gram/form[@tag='nPc7']"/></td></tr>
                         </table>
                       </xsl:when>
                       <xsl:when test="lemma/grammar_note/@slovni_druh='adj'">
                         <table id="cj-gram-table">
                           <tr><td>comparative</td><td><xsl:value-of select="lemma/gram/form[@tag='d2']"/></td></tr>
                           <tr><td>superlative</td><td><xsl:value-of select="lemma/gram/form[@tag='d3']"/></td></tr>
                         </table>
                       </xsl:when>
                       <xsl:when test="lemma/grammar_note/@slovni_druh='adv'">
                         <table id="cj-gram-table">
                           <tr><td>comparative</td><td><xsl:value-of select="lemma/gram/form[@tag='d2']"/></td></tr>
                           <tr><td>superlative</td><td><xsl:value-of select="lemma/gram/form[@tag='d3']"/></td></tr>
                         </table>
                       </xsl:when>
                       <xsl:when test="lemma/grammar_note/@slovni_druh='verb'">
                         <table id="cj-gram-table">
                           <tr><td></td><td>singular</td><td>plural</td></tr>
                           <tr><td>first person</td><td><xsl:value-of select="lemma/gram/form[@tag='mIp1nS']"/></td><td><xsl:value-of select="lemma/gram/form[@tag='mIp1nP']"/></td></tr>
                           <tr><td>second person</td><td><xsl:value-of select="lemma/gram/form[@tag='mIp2nS']"/></td><td><xsl:value-of select="lemma/gram/form[@tag='mIp2nP']"/></td></tr>
                           <tr><td>third person</td><td><xsl:value-of select="lemma/gram/form[@tag='mIp3nS']"/></td><td><xsl:value-of select="lemma/gram/form[@tag='mIp3nP']"/></td></tr>
                           <tr><td>imperative</td><td><xsl:value-of select="lemma/gram/form[@tag='mRp2nS']"/></td><td><xsl:value-of select="lemma/gram/form[@tag='mRp2nP']"/></td></tr>
                          <xsl:if test="lemma/gram/form[@tag='mAgMnS']"><tr><td>participle active</td><td colspan="2"><xsl:value-of select="lemma/gram/form[@tag='mAgMnS']"/></td></tr></xsl:if>
                          <xsl:if test="lemma/gram/form[@tag='mNgMnS']"><tr><td>participle passive</td><td colspan="2"><xsl:value-of select="lemma/gram/form[@tag='mNgMnS']"/></td></tr></xsl:if>
                          <xsl:if test="lemma/gram/form[@tag='mSgMnS']">
                           <tr><td>transgressive masculine</td><td><xsl:value-of select="lemma/gram/form[@tag='mSgMnS']"/></td><td rowspan="2"><xsl:value-of select="lemma/gram/form[@tag='mSgMnP']"/></td></tr>
                           <tr><td>transgressive feminine, neutral</td><td><xsl:value-of select="lemma/gram/form[@tag='mSgFnS']"/></td></tr>
                           </xsl:if>
                           <xsl:if test="lemma/gram/form[@tag='mDgMnS']">
                             <tr><td>transgressive masculine</td><td><xsl:value-of select="lemma/gram/form[@tag='mDgMnS']"/></td><td rowspan="2"><xsl:value-of select="lemma/gram/form[@tag='mDgMnP']"/></td></tr>
                             <tr><td>transgressive feminine, neutral.</td><td><xsl:value-of select="lemma/gram/form[@tag='mDgFnS']"/></td></tr>
                           </xsl:if>
                           <tr><td>verbal substantive</td><td colspan="2"><xsl:value-of select="lemma/gram/form[@tag='gNnSc1']"/></td></tr>
                         </table>
                       </xsl:when>
                       <xsl:when test="lemma/grammar_note/@slovni_druh='num' or lemma/grammar_note/@slovni_druh='pron'">
                         <table id="cj-gram-table">
                          <xsl:if test="lemma/gram/form[@tag='cP1']"><tr><td></td><td>singular</td><td>plural</td></tr></xsl:if>                        
                           <tr><td>nominative</td><td><xsl:value-of select="lemma/gram/form[@tag='c1']"/></td><xsl:if test="lemma/gram/form[@tag='cP1']"><td><xsl:value-of select="lemma/gram/form[@tag='cP1']"/></td></xsl:if></tr>
                           <tr><td>genitive</td><td><xsl:value-of select="lemma/gram/form[@tag='c2']"/></td><xsl:if test="lemma/gram/form[@tag='cP1']"><td><xsl:value-of select="lemma/gram/form[@tag='cP2']"/></td></xsl:if></tr>
                           <tr><td>dative</td><td><xsl:value-of select="lemma/gram/form[@tag='c3']"/></td><xsl:if test="lemma/gram/form[@tag='cP1']"><td><xsl:value-of select="lemma/gram/form[@tag='cP3']"/></td></xsl:if></tr>
                           <tr><td>accusative</td><td><xsl:value-of select="lemma/gram/form[@tag='c4']"/></td><xsl:if test="lemma/gram/form[@tag='cP1']"><td><xsl:value-of select="lemma/gram/form[@tag='cP4']"/></td></xsl:if></tr>
                           <tr><td>vocative</td><td><xsl:value-of select="lemma/gram/form[@tag='c5']"/></td><xsl:if test="lemma/gram/form[@tag='cP1']"><td><xsl:value-of select="lemma/gram/form[@tag='cP5']"/></td></xsl:if></tr>
                           <tr><td>locative</td><td><xsl:value-of select="lemma/gram/form[@tag='c6']"/></td><xsl:if test="lemma/gram/form[@tag='cP1']"><td><xsl:value-of select="lemma/gram/form[@tag='cP6']"/></td></xsl:if></tr>
                           <tr><td>instrumental</td><td><xsl:value-of select="lemma/gram/form[@tag='c7']"/></td><xsl:if test="lemma/gram/form[@tag='cP1']"><td><xsl:value-of select="lemma/gram/form[@tag='cP7']"/></td></xsl:if></tr>
                         </table>
                       </xsl:when>
                       <xsl:otherwise>
                         <table>
                           <xsl:for-each select="lemma/gram/form">
                             <tr>
                               <td><xsl:value-of select="@tag"/></td>
                               <td><xsl:value-of select="."/></td>
                             </tr>
                           </xsl:for-each>
                         </table>
                       </xsl:otherwise>
                     </xsl:choose>
                   </xsl:if>  <!--konec tabulky-->
                   
                   
                   <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_cjlingvist')">
                   <br /><span class="set-status">
                     Stylistic description: 
                     <xsl:call-template name="status_publish">
                       <xsl:with-param name="status" select="/entry/lemma/style_note[1]/@status"/>
                       <xsl:with-param name="type">style</xsl:with-param>
                     </xsl:call-template>
                   </span>
                   <br/>
                 </xsl:if>
                 <div class="author-info" style="display:none">      
                  <xsl:if test="lemma/style_note/@source!=''"><span class="author-source">source: <xsl:value-of select="lemma/style_note/@source"/></span>; </xsl:if>
                  <xsl:if test="lemma/style_note/@author!=''"><span class="author-author">author: <xsl:value-of select="lemma/style_note/@author"/></span>; </xsl:if>
                  <xsl:if test="lemma/style_note/@copyright!=''"><span class="author-copyright">copyright: <xsl:value-of select="lemma/style_note/@copyright"/></span></xsl:if>
                  </div>                   

                   <xsl:if test="lemma/style_note/@kategorie!=''"><br/><b>Style<!--rating-->: </b>
                     <xsl:choose>
                       <xsl:when test="lemma/style_note/@kategorie='argot'">argot</xsl:when>
                       <xsl:when test="lemma/style_note/@kategorie='hovorove'">colloquial</xsl:when>
                       <xsl:when test="lemma/style_note/@kategorie='morav'">Moravian regional expression</xsl:when>
                       <xsl:when test="lemma/style_note/@kategorie='narec'">dialectal expression</xsl:when>
                       <xsl:when test="lemma/style_note/@kategorie='neologismus' or lemma/style_note/@kategorie='Neologismus'">neologisms</xsl:when>
                       <xsl:when test="lemma/style_note/@kategorie='obecne'">general Czech expression</xsl:when>
                       <xsl:when test="lemma/style_note/@kategorie='oblast'">regional expression</xsl:when>
                       <xsl:when test="lemma/style_note/@kategorie='slang'">slang expression</xsl:when>
                       <xsl:when test="lemma/style_note/@kategorie='spisovne'">standard expression</xsl:when>
                     </xsl:choose>
                   </xsl:if>
                   <xsl:if test="lemma/style_note/@stylpriznak!=''"><br/><b>Style flag: </b>
                     <xsl:call-template name="split_priznak"><xsl:with-param name="text" select="lemma/style_note/@stylpriznak"/></xsl:call-template>
                   </xsl:if>
                   
                   <xsl:if test="lemma/style_note/variant">
                     <br/><b>
                       <xsl:if test="lemma/grammar_note/@slovni_druh!='ustalene'">Stylistic </xsl:if>
                         variant: </b>
                     <xsl:apply-templates select="lemma/style_note/variant"/>
                   </xsl:if>
                   <xsl:if test="normalize-space(lemma/style_note/text())!=''"><br/><xsl:apply-templates select="lemma/style_note" mode="ingram"/></xsl:if>
                   
                 </xsl:if>
                 
               </div>
             </div>
           </xsl:if>
           <div class="cj-vyznam">
           <xsl:for-each select="meanings/meaning[(text!='' or count(relation)>0 or usages/usage/text!='') and (status!='hidden' or $perm!='ro' or $skupina_test='true')]">
              <xsl:sort select="@number" data-type="number"/>
              <div class="meaningtop" id="meaning{@id}">
   <div class="author-info" style="display:none">      
      <xsl:if test="@source!=''"><span class="author-source">source: <xsl:value-of select="@source"/></span>; </xsl:if>
      <xsl:if test="@author!=''"><span class="author-author">author: <xsl:value-of select="@author"/></span>; </xsl:if>
      <xsl:if test="@copyright!=''"><span class="author-copyright">copyright: <xsl:value-of select="@copyright"/></span></xsl:if>
    </div>   
   
          <div class="cj-lemma-h1">Meaning
              <xsl:if test="count(//meanings/meaning[status='published'])>1">
                    <xsl:value-of select="position()"/>
                  </xsl:if>
                  <xsl:if test="count(//meanings/meaning[status='published'])&lt;=1 and ($perm!='ro' or $skupina_test='true')">
                      <xsl:value-of select="@number"/>
                  </xsl:if>
                  <xsl:if test="($perm!='ro' or $skupina_test='true') and @number!=substring-after(@id,'-')"> <span class="source">[! id:<xsl:value-of select="@id"/>]</span></xsl:if>
                </div>
                <div id="@id" class="cj-meaning">
                  <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_cjlingvist')">
                    <span class="set-status"> 
                      <xsl:call-template name="status_publish">
                        <xsl:with-param name="status" select="status"/>
                        <xsl:with-param name="type">meaning</xsl:with-param>
                        <xsl:with-param name="element" select="."/>
                      </xsl:call-template>
                    </span>
                    <br/>
                  </xsl:if>

                  <xsl:if test="@oblast!=''">
                    <span class="category"><b>Semantic area: </b>
                      <xsl:call-template name="split_oblast"><xsl:with-param name="text" select="@oblast"/></xsl:call-template>
                    </span><br/>
                  </xsl:if>
                  <xsl:apply-templates select="text"/>
              <xsl:if test="relation[@type='translation_colloc']">
                <p><strong>Collocation:</strong></p>
                <xsl:apply-templates select="relation[@type='translation_colloc' and ((@status!='hidden' and @auto_complete='1') or $perm!='ro' or $skupina_test='true')]"/>
              </xsl:if>
              <xsl:if test="relation[@type='synonym']">
                <p><strong>Synonyms: </strong>
                  <xsl:apply-templates select="relation[@type='synonym' and ((@status!='hidden' or @auto_complete='1') or $perm!='ro' or $skupina_test='true')]"/>
                </p>
              </xsl:if>
              <xsl:if test="relation[@type='antonym']">
                <p><strong>Antonyms: </strong>
                  <xsl:apply-templates select="relation[@type='antonym' and ((@status!='hidden' or @auto_complete='1') or $perm!='ro' or $skupina_test='true')]"/>
                </p>
              </xsl:if>
              <xsl:if test="count(usages/usage[text!=''])>0">
                <div class="cj-lemma-h2">Examples of use:</div>
                <xsl:for-each select="usages/usage[((status!='hidden' and relation/@auto_complete='1') or $perm!='ro' or $skupina_test='true') and @type='colloc']"> 
                  <xsl:if test="text!=''">
                    <div style="margin-bottom: 7px;"><em><xsl:apply-templates select="text"/></em>
                      <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_cjlingvist')">
                        <span class="set-status">
                          <xsl:call-template name="status_publish">
                            <xsl:with-param name="status" select="status"/>
                            <xsl:with-param name="type">usage</xsl:with-param>
                            <xsl:with-param name="element" select="."/>
                          </xsl:call-template>
                        </span>
                      </xsl:if>
                      <span class="dict-czj"><em style="font-size:80%">&#160;(ČZJ translation - see video U<xsl:value-of select="position()"/>)</em></span>   
                    </div>
                  </xsl:if>
                  <div class="author-info" style="display:none">
                    <xsl:if test="@source!=''"><span class="author-source">source: <xsl:value-of select="@source"/></span>; </xsl:if>
                    <xsl:if test="@author!=''"><span class="author-author">author: <xsl:value-of select="@author"/></span>; </xsl:if>
                    <xsl:if test="@copyright!=''"><span class="author-copyright">copyright: <xsl:value-of select="@copyright"/></span></xsl:if>
                  </div>
                </xsl:for-each>
                <xsl:for-each select="usages/usage[((status!='hidden' and relation/@auto_complete='1') or $perm!='ro' or $skupina_test='true') and @type='colloc']">
                  <xsl:if test="relation">
                    <xsl:apply-templates select="relation" mode="usage">
                      <xsl:with-param name="variant_pos" select="position()"/> 
                    </xsl:apply-templates>
                  </xsl:if>
                  <xsl:if test="position()=last()"><div style="clear:both"/></xsl:if>
                </xsl:for-each>
                <xsl:for-each select="usages/usage[(status!='hidden' or $perm!='ro' or $skupina_test='true') and not(@type='colloc')]">
                  <xsl:if test="text!=''">
                    <div style="margin-bottom: 7px;"><em><xsl:apply-templates select="text"/></em>
                      <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_cjlingvist')">
                        <span class="set-status">
                          <xsl:call-template name="status_publish">
                            <xsl:with-param name="status" select="status"/>
                            <xsl:with-param name="type">usage</xsl:with-param>
                            <xsl:with-param name="element" select="."/>
                          </xsl:call-template>
                        </span>
                      </xsl:if>
                    </div>
                  </xsl:if>
                  <div class="author-info" style="display:none">
                    <xsl:if test="@source!=''"><span class="author-source">source: <xsl:value-of select="@source"/></span>; </xsl:if>
                    <xsl:if test="@author!=''"><span class="author-author">author: <xsl:value-of select="@author"/></span>; </xsl:if>
                    <xsl:if test="@copyright!=''"><span class="author-copyright">copyright: <xsl:value-of select="@copyright"/></span></xsl:if>
                  </div>
                </xsl:for-each>
              </xsl:if>
              <xsl:if test="relation[@type='translation' and ((@status!='hidden') or $perm!='ro' or $skupina_test='true')]">
                <p><strong>Translations:</strong></p>
                <div class="czj-relation-wrapper">
       <p><xsl:apply-templates select="relation[@type='translation' and ((@status!='hidden') or ($perm!='ro' or $skupina_test='true')) and (@target='cs' or @target='en')]">
                 <xsl:sort data-type="number" order="ascending" select="((@target='cs')*1) + ((@target='en')*2)"/>
                </xsl:apply-templates></p>
                <xsl:apply-templates select="relation[@type='translation' and ((@status!='hidden') or ($perm!='ro' or $skupina_test='true')) and not(@target='cs' or @target='en')]">
                 <xsl:sort data-type="number" order="ascending" select="((@target='czj')*1) + ((@target='is')*2) + ((@target='asl')*3)"/>
                 <xsl:sort data-type="number" order="descending" select="((@lemma_region='cr')*3) + ((@lemma_region='cechy')*2) +((@lemma_region='morava')*1)"/>
                </xsl:apply-templates>    
                </div>
              </xsl:if>
            </div>
          </div>
        </xsl:for-each> 
        </div> <!-- cj-vyznam -->
        
        <div style="clear:both"></div>
        <div id="collocations">
          <xsl:if test="collocations/revcolloc[$perm!='ro' or $skupina_test='true' or @auto_complete='1' or @completeness='2' or @completeness='100']">
            <div class="cj-lemma-h1">Collocations with this lemma</div>
            <!--<div class="collocations">-->
              <xsl:apply-templates select="collocations/revcolloc[$perm!='ro' or $skupina_test='true' or @auto_complete='1' or @completeness='2' or @completeness='100']">
              <xsl:sort select="title"/>
              </xsl:apply-templates>
            <!--</div>-->
            <div style="clear:both"></div>
          </xsl:if>
        </div>

         <input type="button" value="show/hide sources" id="show-author" onclick="$('.author-info').toggle();"/> 
      </div>
    </div>
  </body>
</html>
  </xsl:template>
  
  <xsl:template match="relation[(@type='translation' or @type='translation_colloc') and not(@target='cs' or @target='en')]">
    <xsl:if test="((@status!='hidden' or (name(..)='usage' and ../status='published')) and (@auto_complete='1' or @front_video='published')) or $perm!='ro' or $skupina_test='true'">     <!-- doplnit do anglické verze a inline -->

    <xsl:variable name="lemma"><xsl:value-of select="@lemma_id"/></xsl:variable>
    <xsl:variable name="rtype"><xsl:value-of select="@type"/></xsl:variable>    
    <xsl:if test="count(preceding-sibling::relation[@lemma_id=$lemma and @type=$rtype])=0">

    <div class="czj-relation-item dict-{@target}">
    <div class="czj-relation relationvideo">
      <div class="czj-lemma-preview-sm">
        <div class="czj-lemma-preview-sm-header dict-{@target}">            

        <div class="alignleft" style="white-space: nowrap; overflow: hidden; width: 160px;">
           <xsl:choose>
              <xsl:when test="@target='czj'">ČZJ </xsl:when>          
              <xsl:when test="@target='is'">IS </xsl:when>
              <xsl:when test="@target='asl'">ASL </xsl:when>
              <xsl:otherwise>?:</xsl:otherwise>
            </xsl:choose>
          
            <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_cjlingvist')">
              <xsl:if test="@meaning_nr='' or @meaning_count = 1">
                <span class="set-status">
                  <xsl:call-template name="status_publish">
                    <xsl:with-param name="status" select="@status"/>
                    <xsl:with-param name="type">relation</xsl:with-param>
                    <xsl:with-param name="element" select="."/>
                  </xsl:call-template>
                </span>
              </xsl:if>
              <!--<br/>-->
              </xsl:if><xsl:if test="@meaning_nr!='' and @meaning_count > 1">(meaning <xsl:value-of select="@meaning_nr"/>)
              <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_cjlingvist')">
                <span class="set-status">
                  <xsl:call-template name="status_publish">
                    <xsl:with-param name="status" select="@status"/>
                    <xsl:with-param name="type">relation</xsl:with-param>
                    <xsl:with-param name="element" select="."/>
                  </xsl:call-template>
                </span>
              </xsl:if>
              <xsl:if test="count(following-sibling::relation[@lemma_id=$lemma and @type=$rtype]) &gt; 0">
                <xsl:for-each select="following-sibling::relation[@lemma_id=$lemma and @type=$rtype]">
                 <xsl:sort select="@meaning_nr"/>
                  <xsl:text>, </xsl:text><xsl:value-of select="@meaning_nr"/>
                  <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_cjlingvist')">
                    <span class="set-status">
                      <xsl:call-template name="status_publish">
                        <xsl:with-param name="status" select="@status"/>
                        <xsl:with-param name="type">relation</xsl:with-param>
                        <xsl:with-param name="element" select="."/>
                      </xsl:call-template>
                    </span>
                  </xsl:if>
                </xsl:for-each>
              </xsl:if>
          </xsl:if></div>
          <xsl:if test="$perm!='ro' or @auto_complete='1'">
          <div class="alignright">
            <a href="/{@target}?action=search&amp;getdoc={@lemma_id}&amp;lang=en">           
              <!--<xsl:value-of select="@lemma_id"/>-->
              <img src="/media/img/slovnik-open_w.png" height="20"/>
              <xsl:if test="@type='translation_colloc'">
                <xsl:value-of select="translation"/>
              </xsl:if>
            </a>
          </div> 
          </xsl:if>
        </div> 
     <xsl:apply-templates select="file" mode="rel">
          <xsl:with-param name="target"><xsl:value-of select="@target"/></xsl:with-param>
          <xsl:with-param name="text_link"><xsl:if test="$perm='ro' and @auto_complete='0' and @front_video='published'">true</xsl:if></xsl:with-param>
        </xsl:apply-templates>
        <div class="czj-lemma-preview-sm-sw">
          <xsl:apply-templates select="swmix/sw" mode="rel"/>
        </div>
      </div>
     </div>
     </div> 
      <xsl:if test="@type='translation_colloc'">
        <br/>
      </xsl:if>
    </xsl:if>
    </xsl:if>
  </xsl:template>  
<xsl:template match="relation[@type='translation' and (@target='cs' or @target='en')]">
    <xsl:variable name="lemma">
      <xsl:value-of select="@lemma_id"/>
    </xsl:variable>
    <span class="relation-entry dict-{@target}">        
      <xsl:if test="@title_only!='true'">
        <xsl:if test="count(preceding-sibling::relation[@lemma_id=$lemma and @type='translation'])=0">          
          <xsl:if test="@auto_complete='1' or ($perm!='ro' or $skupina_test='true')">
            <span style="color: gray">        
              <xsl:choose>
                <xsl:when test="@target='cs'">CZ: </xsl:when>          
                <xsl:when test="@target='en'">EN: </xsl:when>              
                <xsl:otherwise>?</xsl:otherwise>
              </xsl:choose>
            </span>
            <a href="/{@target}?action=search&amp;getdoc={@lemma_id}&amp;lang=en">
              <xsl:value-of select="title"/></a>                      
          </xsl:if>          
          <xsl:if test="@auto_complete!='1' and $perm='ro' and $skupina_test!='true'">
            <span style="color: gray">        
              <xsl:choose>
                <xsl:when test="@target='cs'">CZ: </xsl:when>          
                <xsl:when test="@target='en'">EN: </xsl:when>              
                <xsl:otherwise>?</xsl:otherwise>
              </xsl:choose>
            </span>
          <xsl:value-of select="title"/>              
            </xsl:if>          
          <xsl:if test="(@meaning_nr!='' and @meaning_count > 1) and (@auto_complete='1' or $perm!='ro' or $skupina_test='true')"> (ve významu
            <xsl:value-of select="@meaning_nr"/>              
            <!--schvalování-->              
            <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_lingvist')">                
              <span class="set-status">                    
                <xsl:call-template name="status_publish">                    
                  <xsl:with-param name="status" select="@status"/>                    
                  <xsl:with-param name="type">relation</xsl:with-param>                    
                  <xsl:with-param name="element" select="."/>                  
                </xsl:call-template>                
              </span>              
            </xsl:if><!--konec schvalování-->                            
            <xsl:if test="count(following-sibling::relation[@lemma_id=$lemma and @type='translation']) &gt; 0">              
              <xsl:for-each select="following-sibling::relation[@lemma_id=$lemma and @type='translation']">  
                <xsl:sort select="@meaning_nr" data-type="number"/>              
                <xsl:text>, </xsl:text>
                <xsl:value-of select="@meaning_nr"/>                
                <!--schvalování-->                
                <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_lingvist')">                  
                  <span class="set-status">                      
                    <xsl:call-template name="status_publish">                      
                      <xsl:with-param name="status" select="@status"/>                      
                      <xsl:with-param name="type">relation</xsl:with-param>                      
                      <xsl:with-param name="element" select="."/>                    
                    </xsl:call-template>                  
                  </span>                
                </xsl:if>
                <!--konec schvalování-->                
              </xsl:for-each>            
            </xsl:if>)           
          </xsl:if>          
          <xsl:if test="@meaning_nr='' or @meaning_count = 1">            
            <!--schvalování-->            
            <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_lingvist')">              
              <span class="set-status">                  
                <xsl:call-template name="status_publish">                  
                  <xsl:with-param name="status" select="@status"/>                  
                  <xsl:with-param name="type">relation</xsl:with-param>                  
                  <xsl:with-param name="element" select="."/>                
                </xsl:call-template>              
              </span>            
            </xsl:if><!--konec schvalování-->            
          </xsl:if>          

          <!--<xsl:if test="position()!=last()">, </xsl:if>-->          
          <xsl:if test="position()!=last()">, </xsl:if>        
        </xsl:if>      
      </xsl:if>      
      <xsl:if test="@title_only='true'">
        <span style="color: gray">        
          <xsl:choose>
              <xsl:when test="@target='cs'">CZ: </xsl:when>          
              <xsl:when test="@target='en'">EN: </xsl:when>              
              <xsl:otherwise>?</xsl:otherwise>
          </xsl:choose>
        </span>        
        <xsl:value-of select="title"/>                 
        <!--schvalování-->    
        <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_lingvist')">        
          <span class="set-status">            
            <xsl:call-template name="status_publish">            
              <xsl:with-param name="status" select="@status"/>            
              <xsl:with-param name="type">relation</xsl:with-param>            
              <xsl:with-param name="element" select="."/>          
            </xsl:call-template>        
          </span>      
        </xsl:if><!--konec schvalování-->                  

        <xsl:if test="position()!=last()">, </xsl:if>      
      </xsl:if>    
    </span>  
  </xsl:template>
  <xsl:template match="relation" mode="usage">
    <xsl:param name="variant_pos"/>  
    <xsl:apply-templates select="file" mode="usage">
      <xsl:with-param name="variant_sw" select="swmix"/>
      <xsl:with-param name="variant_pos" select="$variant_pos"/> 
    </xsl:apply-templates>  
  </xsl:template>
  <xsl:template match="file" mode="usage">
    <xsl:param name="variant_sw"/>  
    <xsl:param name="variant_pos"/>  
    <div class="czj-relation-item dict-czj">    
      <div class="czj-relation relationvideo">      
        <div class="czj-lemma-preview-sm">          
          <div class="czj-lemma-preview-sm-header dict-czj">                                
            <div class="alignleft">U<xsl:value-of select="$variant_pos"/>  
            </div>            
            <div class="alignright">
              <a href="/czj?action=search&amp;getdoc={../@lemma_id}&amp;lang=en">                
                <img src="/media/img/slovnik-open_w.png" height="20"/></a>            
            </div>          
          </div>      
        </div>
        <div style="clear:both">
        </div>  
           
            <xsl:if test="substring(location, string-length(location)-3, 4) = '.flv'">
            <div id="flowvideovar{@id}" data-ratio="0.8" class="flowplayer flowmouse" style="width:120px; background:#777 url(/media/videoczj/thumb/{location}/thumb.jpg) no-repeat; background-size: 120px 96px">    
              <video loop="loop">   
                <source type="video/flash" src="/media/videoczj/{location}"/>
              </video>
            </div>    
            </xsl:if>
            <xsl:if test="substring(location, string-length(location)-3, 4) = '.mp4'">
             <div id="flowvideorel{id}" data-ratio="0.8" class="flowmouse" style="width:120px; background: url(/media/videoczj/thumb/{location}/thumb.jpg) no-repeat; background-size: 120px 96px; float:left;">    
                <video loop="loop" width="120px" height="96px" poster="/media/videoczj/{location}/thumb.jpg" onmouseover="this.play()" onmouseout="this.pause()">
                  <source type="video/mp4" src="/media/videoczj/{location}"/>
                </video>
            </div>                                                         
            </xsl:if>            
        <div class="czj-lemma-preview-sm-sw">     
          <span class="sw transsw" style="vertical-align: top; display: inline-block; height: 96px;">    
            <xsl:if test="($variant_sw/sw)">
              <xsl:apply-templates select="$variant_sw/sw" mode="rel"/>
            </xsl:if>  
          </span>  
        </div>          
<script type="text/javascript">
    $("#flowvideovar<xsl:value-of select="id"/>").on("click", function(e) {
    window.location='/czj?action=search&amp;getdoc=<xsl:value-of select="../@lemma_id"/>&amp;lang=en';
      });
  </script>    
      </div>  
    </div>
  </xsl:template>

  <xsl:template match="relation[@type!='translation' and @type!='translation_colloc']">
    <xsl:variable name="lemma"><xsl:value-of select="@lemma_id"/></xsl:variable>
    <xsl:variable name="rtype"><xsl:value-of select="@type"/></xsl:variable>

    <xsl:if test="count(preceding-sibling::relation[@lemma_id=$lemma and @type=$rtype])=0">
        <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_cjlingvist')">
          <span class="set-status">
            <xsl:call-template name="status_publish">
              <xsl:with-param name="status" select="@status"/>
              <xsl:with-param name="type">relation</xsl:with-param>
              <xsl:with-param name="element" select="."/>
            </xsl:call-template>
          </span>
        </xsl:if>
        <xsl:if test="@title_only!='true'">
          <xsl:if test="@auto_complete='1' or $perm!='ro' or $skupina_test='true'">
            <a href="/cs?action=search&amp;getdoc={@lemma_id}&amp;lang=en"><xsl:value-of select="title"/></a>
          </xsl:if>
          <xsl:if test="@auto_complete='0' and @status='published' and $perm='ro' and $skupina_test!='true'">
            <xsl:value-of select="title"/>
          </xsl:if>
          <xsl:if test="@meaning_nr!=''">
          <xsl:if test="@auto_complete='1' or $perm!='ro' or $skupina_test='true'"> 
            (meaning <xsl:value-of select="@meaning_nr"/>
            <xsl:if test="count(following-sibling::relation[@lemma_id=$lemma and @type=$rtype]) &gt; 0">
              <xsl:for-each select="following-sibling::relation[@lemma_id=$lemma and @type=$rtype]">
              <xsl:sort select="@meaning_nr" data-type="number"/>
                <xsl:text>, </xsl:text><xsl:value-of select="@meaning_nr"/>
              </xsl:for-each>
          </xsl:if>)
          </xsl:if>                    
        </xsl:if>
          </xsl:if>
        <xsl:if test="@title_only='true'">
          <xsl:value-of select="title"/>
        </xsl:if>
      <xsl:if test="position()!=last()"><xsl:text>, </xsl:text></xsl:if>
    </xsl:if>
  </xsl:template>

  <xsl:template match="sw" mode="rel">
  <xsl:if test=".!=''">
  <span class="sw transsw" style="vertical-align: top; display: inline-block; height: 96px;">
    <a href="http://znaky.zcu.cz/proxy/tts/signwriting.png?generator[sw]={.}&amp;generator[align]=top_left&amp;generator[set]=sw10" class="fancybox swlink" data-sw="{.}">
           <img src="http://znaky.zcu.cz/proxy/tts/signwriting.png?generator[sw]={.}&amp;generator[align]=top_left&amp;generator[set]=sw10" alt="signwriting" class="signwriting" ratio="0.5" style=""/>
     
     <!-- <img style="" src="http://znaky.zcu.cz/proxy/tts/signwriting.png?generator[sw]={.}&amp;generator[align]=top_left&amp;generator[set]=sw10" alt="signwriting" class="signwriting"/>-->
    </a></span>
  </xsl:if>
  </xsl:template>

  <xsl:template match="file" mode="rel">    
        <xsl:param name="target"><xsl:value-of select="$dictcode"/></xsl:param>
        <xsl:param name="text_link">false</xsl:param>

        <xsl:if test="substring(location, string-length(location)-3, 4) = '.flv'">
          <div id="flowvideorel{id}" data-ratio="0.8" class="flowplayer flowmouse" style="width:120px; background:#777 url(/media/video{$target}/thumb/{location}/thumb.jpg) no-repeat; background-size: 120px 96px">
            <video loop="loop">
              <source type="video/flash" src="/media/video{$target}/{location}"/>
            </video>
          </div>    
        </xsl:if>
        <xsl:if test="substring(location, string-length(location)-3, 4) = '.mp4'">
          <div id="flowvideorel{id}" data-ratio="0.8" class="flowmouse" style="width:120px; background: url(/media/video{$target}/thumb/{location}/thumb.jpg) no-repeat; background-size: 120px 96px; float:left;">    
             <video loop="loop" width="120px" height="96px" poster="/media/video{$target}/{location}/thumb.jpg" onmouseover="this.play()" onmouseout="this.pause()">
                  <source type="video/mp4" src="/media/video{$target}/{location}"/>
                </video>
           </div>   
        </xsl:if>
      
  <xsl:if test="$text_link!='true'">
    <script type="text/javascript">
      $("#flowvideorel<xsl:value-of select="id"/>").on("click", function(e) {
      window.location='/<xsl:value-of select="$target"/>?action=search&amp;getdoc=<xsl:value-of select="../@lemma_id"/>&amp;lang=en';
      });
    </script>
  </xsl:if>
  </xsl:template>

<xsl:template match="colloc">
     <span style="vertical-align:top;">
      <xsl:if test="not(title)">
        <xsl:value-of select="@lemma_id"/>
      </xsl:if>
      <xsl:if test="title">
        <a href="/cs?action=search&amp;getdoc={@lemma_id}&amp;lang=en">
          <xsl:value-of select="title"/>
        </a>
      </xsl:if>
      <xsl:if test="position()!=last()">
        <xsl:text>,&#160;</xsl:text>
      </xsl:if>
    </span>
</xsl:template>


  <xsl:template match="table|tr|td|tbody">
    <xsl:element name="{name(.)}">
      <xsl:apply-templates/>
    </xsl:element>
  </xsl:template>

  <xsl:template name="split_skupina">
    <xsl:param name="text"/>
    <xsl:variable name="first" select='substring-before($text,";")'/>
    <xsl:variable name='rest' select='substring-after($text,";")'/>
    <xsl:if test='$first'>
      <xsl:choose>
        <xsl:when test="$first='pomnozne'">plural only</xsl:when>
        <xsl:when test="$first='hromadne'">collective</xsl:when>
     <xsl:when test="$first='vlastni'">proper</xsl:when>
        <xsl:when test="$first='osobni'">personal</xsl:when>
        <xsl:when test="$first='ukazovaci'">demonstrative</xsl:when>
        <xsl:when test="$first='vztazne'">relative</xsl:when>
        <xsl:when test="$first='zaporne'">negative</xsl:when>
        <xsl:when test="$first='neurcite'">indeterminate</xsl:when>
        <xsl:when test="$first='privl'">possessive</xsl:when>
        <xsl:when test="$first='zakladni'">general</xsl:when>
        <xsl:when test="$first='druhova'">generic</xsl:when>
        <xsl:when test="$first='radova'">ordinal</xsl:when>
        <xsl:when test="$first='nasobna'">multiplicative</xsl:when>
        <xsl:when test="$first='tazaci'">questional</xsl:when>
        <xsl:when test="$first='dok'">perfective aspect</xsl:when>
        <xsl:when test="$first='nedok'">imperfective aspect</xsl:when>
        <xsl:when test="$first='zajmenne'">pronominal</xsl:when>
        <xsl:when test="$first='sourad'">coordinating</xsl:when>
        <xsl:when test="$first='podrad'">subordinating</xsl:when>
        <xsl:when test="$first='nasobnapris'">multiplicative adverbial</xsl:when>
        <xsl:when test="$first='nasobnaprisneu'">indefinite multiplicative adverbial</xsl:when>
        <xsl:when test="$first='zajmenna'">pronominal</xsl:when>
        <xsl:when test="$first='frazem'">phraseme</xsl:when>
        <xsl:when test="$first='porekadlo'">adage</xsl:when>
        <xsl:when test="$first='prirovnani'">simile</xsl:when>
        <xsl:when test="$first='prislovi'">proverb</xsl:when>          
      </xsl:choose>
      <xsl:if test="$rest">, </xsl:if>
    </xsl:if>

    <xsl:if test='$rest'>
      <xsl:call-template name='split_skupina'>
        <xsl:with-param name='text' select='$rest'/>
      </xsl:call-template>
    </xsl:if>
    <xsl:if test='not($rest)'>
      <xsl:choose>
        <xsl:when test="$text='pomnozne'">plural only</xsl:when>
        <xsl:when test="$text='hromadne'">collective</xsl:when>
        <xsl:when test="$text='vlastni'">proper</xsl:when>
        <xsl:when test="$text='osobni'">personal</xsl:when>
        <xsl:when test="$text='ukazovaci'">demonstrative</xsl:when>
        <xsl:when test="$text='vztazne'">relative</xsl:when>
        <xsl:when test="$text='zaporne'">negative</xsl:when>
        <xsl:when test="$text='neurcite'">indeterminate</xsl:when>
        <xsl:when test="$text='privl'">possessive</xsl:when>
        <xsl:when test="$text='zakladni'">general</xsl:when>
        <xsl:when test="$text='druhova'">generic</xsl:when>
        <xsl:when test="$text='radova'">ordinal</xsl:when>
        <xsl:when test="$text='nasobna'">multiplicative</xsl:when>
        <xsl:when test="$text='tazaci'">questional</xsl:when>
        <xsl:when test="$text='dok'">perfective aspect</xsl:when>
        <xsl:when test="$text='nedok'">imperfective aspect</xsl:when>
        <xsl:when test="$text='zajmenne'">pronominal</xsl:when>
        <xsl:when test="$text='sourad'">coordinating</xsl:when>
        <xsl:when test="$text='podrad'">subordinating</xsl:when>
        <xsl:when test="$text='nasobnapris'">multiplicative adverbial</xsl:when>
        <xsl:when test="$text='nasobnaprisneu'">indefinite multiplicative adverbial</xsl:when>
        <xsl:when test="$text='zajmenna'">pronominal</xsl:when>
        <xsl:when test="$text='frazem'">phraseme</xsl:when>
        <xsl:when test="$text='porekadlo'">adage</xsl:when>
        <xsl:when test="$text='prirovnani'">simile</xsl:when>
        <xsl:when test="$text='prislovi'">proverb</xsl:when>          
      </xsl:choose>
    </xsl:if>
  </xsl:template>

  <xsl:template name="split_skupina2">
    <xsl:param name="text"/>
    <xsl:variable name="first" select='substring-before($text,";")'/>
    <xsl:variable name='rest' select='substring-after($text,";")'/>
    <xsl:if test='$first'>
      <xsl:choose>
     <xsl:when test="$first='m'">masculine</xsl:when>
        <xsl:when test="$first='z'">feminine</xsl:when>
        <xsl:when test="$first='s'">neuter</xsl:when>
        <xsl:when test="$first='zpusobove'">modal</xsl:when>
        <xsl:when test="$first='ukazovaci'">demonstrative</xsl:when>
        <xsl:when test="$first='tazaci'">interrogative</xsl:when>
        <xsl:when test="$first='vztazne'">relative</xsl:when>
        <xsl:when test="$first='neurcite'">indefinite</xsl:when>
        <xsl:when test="$first='zaporne'">negative</xsl:when>
        <xsl:when test="$first='prisudku'">in predicate</xsl:when>
        <xsl:when test="$first='vymez'">determinative</xsl:when>
        <xsl:when test="$first='zduraz'">emphasizing</xsl:when>
        <xsl:when test="$first='obsahova'">substantive</xsl:when>
        <xsl:when test="$first='modalni'">modal</xsl:when>
        <xsl:when test="$first='citoslovecna'">interjectional</xsl:when>
      </xsl:choose>
      <xsl:if test="$rest">, </xsl:if>
    </xsl:if>

    <xsl:if test='$rest'>
      <xsl:call-template name='split_skupina2'>
        <xsl:with-param name='text' select='$rest'/>
      </xsl:call-template>
    </xsl:if>
    <xsl:if test='not($rest)'>
      <xsl:choose>
      <xsl:when test="$text='m'">masculine</xsl:when>
        <xsl:when test="$text='z'">feminine</xsl:when>
        <xsl:when test="$text='s'">neuter</xsl:when>
        <xsl:when test="$text='zpusobove'">modal</xsl:when>
        <xsl:when test="$text='ukazovaci'">demonstrative</xsl:when>
        <xsl:when test="$text='tazaci'">interrogative</xsl:when>
        <xsl:when test="$text='vztazne'">relative</xsl:when>
        <xsl:when test="$text='neurcite'">indefinite</xsl:when>
        <xsl:when test="$text='zaporne'">negative</xsl:when>
        <xsl:when test="$text='prisudku'">in predicate</xsl:when>
        <xsl:when test="$text='vymez'">determinative</xsl:when>
        <xsl:when test="$text='zduraz'">emphasizing</xsl:when>
        <xsl:when test="$text='obsahova'">substantive</xsl:when>
        <xsl:when test="$text='modalni'">modal</xsl:when>
        <xsl:when test="$text='citoslovecna'">interjectional</xsl:when>
      </xsl:choose>
    </xsl:if>
  </xsl:template>

  <xsl:template name="split_priznak">
    <xsl:param name="text"/>
    <xsl:variable name="first" select='substring-before($text,";")'/>
    <xsl:variable name='rest' select='substring-after($text,";")'/>
    <xsl:if test='$first'>
      <xsl:choose>
       <xsl:when test="$first='abstrakt'">abstract expression</xsl:when>
        <xsl:when test="$first='basnicky'">poetic expression</xsl:when>
        <xsl:when test="$first='biblicky'">biblical expression</xsl:when>
        <xsl:when test="$first='detsky'">children´s expression</xsl:when>
        <xsl:when test="$first='domacke'">home expression</xsl:when>
        <xsl:when test="$first='duverny'">intimate expression</xsl:when>
        <xsl:when test="$first='eufem'">euphemism</xsl:when>
        <xsl:when test="$first='expres'">expressive expression</xsl:when>
        <xsl:when test="$first='famil'">colloquial expression</xsl:when>
        <xsl:when test="$first='hanlivy'">derogatory expression</xsl:when>
        <xsl:when test="$first='ironicky'">ironical expression</xsl:when>
        <xsl:when test="$first='kladny'">positive expression</xsl:when>
        <xsl:when test="$first='knizni'">literary expression</xsl:when>
        <xsl:when test="$first='konkretni'">specific expression</xsl:when>
        <xsl:when test="$first='lidovy'">popular expression</xsl:when>
        <xsl:when test="$first='lichotivy'">flattering expression</xsl:when>
        <xsl:when test="$first='mazlivy'">affectionate expression</xsl:when>
        <xsl:when test="$first='odborny'">technical expression</xsl:when>
        <xsl:when test="$first='pohadkovy'">fairytale expression</xsl:when>
        <xsl:when test="$first='publ'">journalistic expression</xsl:when>
        <xsl:when test="$first='prenes'">metaphorically</xsl:when>
        <xsl:when test="$first='zdvor'">courtesy expression</xsl:when>
        <xsl:when test="$first='zhrubely'">vulgar expression</xsl:when>
        <xsl:when test="$first='zert'">jocular expression</xsl:when>
        <xsl:when test="$first='zlomkovy'">fractional expression</xsl:when>
        <xsl:when test="$first='zdrob'">diminutive</xsl:when>
        <xsl:when test="$first='zastar'">outdated expression</xsl:when>
        <xsl:when test="$first='cirkev'">religious expression</xsl:when>
      </xsl:choose>    
      <xsl:if test="$rest">, </xsl:if>
    </xsl:if>
    <xsl:if test='$rest'>
      <xsl:call-template name='split_priznak'>
        <xsl:with-param name='text' select='$rest'/>
      </xsl:call-template>
    </xsl:if>
    <xsl:if test='not($rest)'>
      <xsl:choose>
         <xsl:when test="$text='abstrakt'">abstract expression</xsl:when>
        <xsl:when test="$text='basnicky'">poetic expression</xsl:when>
        <xsl:when test="$text='biblicky'">biblical expression</xsl:when>
        <xsl:when test="$text='detsky'">children´s expression</xsl:when>
        <xsl:when test="$text='domacke'">home expression</xsl:when>
        <xsl:when test="$text='duverny'">intimate expression</xsl:when>
        <xsl:when test="$text='eufem'">euphemism</xsl:when>
        <xsl:when test="$text='expres'">expressive expression</xsl:when>
        <xsl:when test="$text='famil'">colloquial expression</xsl:when>
        <xsl:when test="$text='hanlivy'">derogatory expression</xsl:when>
        <xsl:when test="$text='ironicky'">ironical expression</xsl:when>
        <xsl:when test="$text='kladny'">positive expression</xsl:when>
        <xsl:when test="$text='knizni'">literary expression</xsl:when>
        <xsl:when test="$text='konkretni'">specific expression</xsl:when>
        <xsl:when test="$text='lidovy'">popular expression</xsl:when>
        <xsl:when test="$text='lichotivy'">flattering expression</xsl:when>
        <xsl:when test="$text='mazlivy'">affectionate expression</xsl:when>
        <xsl:when test="$text='odborny'">technical expression</xsl:when>
        <xsl:when test="$text='pohadkovy'">fairytale expression</xsl:when>
        <xsl:when test="$text='publ'">journalistic expression</xsl:when>
        <xsl:when test="$text='prenes'">metaphorically</xsl:when>
        <xsl:when test="$text='zdvor'">courtesy expression</xsl:when>
        <xsl:when test="$text='zhrubely'">vulgar expression</xsl:when>
        <xsl:when test="$text='zert'">jocular expression</xsl:when>
        <xsl:when test="$text='zlomkovy'">fractional expression</xsl:when>
        <xsl:when test="$text='zdrob'">diminutive</xsl:when>
        <xsl:when test="$text='zastar'">outdated expression</xsl:when>
        <xsl:when test="$text='cirkev'">religious expression</xsl:when>
      </xsl:choose>    
    </xsl:if>
  </xsl:template>

  <xsl:template name="split_oblast">
    <xsl:param name="text"/>
    <xsl:variable name="first" select='substring-before($text,";")'/>
    <xsl:variable name='rest' select='substring-after($text,";")'/>
    <xsl:if test='$first'>
      <xsl:choose>
      <xsl:when test="$first='anat'">anatomy</xsl:when>
        <xsl:when test="$first='antr'">antropology</xsl:when>
        <xsl:when test="$first='archeol'">archeology</xsl:when>
        <xsl:when test="$first='archit'">architecture</xsl:when>
        <xsl:when test="$first='biol'">biology</xsl:when>
        <xsl:when test="$first='bot'">botanics</xsl:when>
        <xsl:when test="$first='dipl'">diplomatics</xsl:when>
        <xsl:when test="$first='div'">theatre</xsl:when>
        <xsl:when test="$first='dopr'">transport</xsl:when>
        <xsl:when test="$first='ekol'">ecology</xsl:when>
        <xsl:when test="$first='ekon'">economics</xsl:when>
        <xsl:when test="$first='eltech'">elektrotechnics</xsl:when>
        <xsl:when test="$first='etn'">ethnography</xsl:when>
        <xsl:when test="$first='feud'">feudalism</xsl:when>
        <xsl:when test="$first='filat'">philately</xsl:when>
        <xsl:when test="$first='film'">film</xsl:when>
        <xsl:when test="$first='filoz'">philosopsy</xsl:when>
        <xsl:when test="$first='fot'">photography</xsl:when>
        <xsl:when test="$first='fyz'">physics</xsl:when>
        <xsl:when test="$first='fyziol'">physiology</xsl:when>
        <xsl:when test="$first='geol'">geology</xsl:when>
        <xsl:when test="$first='geom'">geometry</xsl:when>
        <xsl:when test="$first='gnoz'">gnoseology</xsl:when>
        <xsl:when test="$first='hist'">history</xsl:when>
        <xsl:when test="$first='horn'">mining</xsl:when>
        <xsl:when test="$first='horol'">mountaineering</xsl:when>
        <xsl:when test="$first='hosp'">industry</xsl:when>
        <xsl:when test="$first='hud'">musicology</xsl:when>
        <xsl:when test="$first='hut'">metallurgy</xsl:when>
        <xsl:when test="$first='hvězd'">astronomy</xsl:when>
        <xsl:when test="$first='chem'">chemistry</xsl:when>
        <xsl:when test="$first='ideal'">idealism</xsl:when>
        <xsl:when test="$first='informatika'">computer science</xsl:when>
        <xsl:when test="$first='jad'">nuclear physics</xsl:when>
        <xsl:when test="$first='jaz'">philology</xsl:when>
        <xsl:when test="$first='kapit'">capitalism</xsl:when>
        <xsl:when test="$first='karet'">card expression</xsl:when>
        <xsl:when test="$first='katol církvi'">catholicism</xsl:when>
        <xsl:when test="$first='krim'">criminology</xsl:when>
        <xsl:when test="$first='křesť'">christianity</xsl:when>
        <xsl:when test="$first='kuch'">cookery</xsl:when>
        <xsl:when test="$first='kult'">culture</xsl:when>
        <xsl:when test="$first='kyb'">cybernetics</xsl:when>
        <xsl:when test="$first='lék'">medicine</xsl:when>
        <xsl:when test="$first='lékár'">pharmaceutics</xsl:when>
        <xsl:when test="$first='let'">aviation</xsl:when>
        <xsl:when test="$first='liter'">literary science</xsl:when>
        <xsl:when test="$first='log'">logic</xsl:when>
        <xsl:when test="$first='marx'">marxism</xsl:when>
        <xsl:when test="$first='mat'">mathematics</xsl:when>
        <xsl:when test="$first='meteor'">meteorology</xsl:when>
        <xsl:when test="$first='miner'">mineralogy</xsl:when>
        <xsl:when test="$first='motor'">motorism</xsl:when>
        <xsl:when test="$first='mysl'">hunting expression</xsl:when>
        <xsl:when test="$first='mytol'">mythology</xsl:when>
        <xsl:when test="$first='náb'">religion</xsl:when>
        <xsl:when test="$first='nár'">ethnography</xsl:when>
        <xsl:when test="$first='obch'">trade</xsl:when>
        <xsl:when test="$first='pedag'">pedagogy</xsl:when>
        <xsl:when test="$first='peněž'">banking</xsl:when>
        <xsl:when test="$first='podnikani'">business</xsl:when>
        <xsl:when test="$first='polit'">politics</xsl:when>
        <xsl:when test="$first='polygr'">polygraphy</xsl:when>
        <xsl:when test="$first='pošt'">postal operations</xsl:when>
        <xsl:when test="$first='potrav'">food industry</xsl:when>
        <xsl:when test="$first='práv'">law</xsl:when>
        <xsl:when test="$first='prům'">industry</xsl:when>
        <xsl:when test="$first='přír'">nature</xsl:when>
        <xsl:when test="$first='psych'">psychology</xsl:when>
        <xsl:when test="$first='rybn'">fish farming</xsl:when>
        <xsl:when test="$first='řem'">craft</xsl:when>
        <xsl:when test="$first='sklář'">glassblowing</xsl:when>
        <xsl:when test="$first='soc'">socialism</xsl:when>
        <xsl:when test="$first='sociol'">sociology</xsl:when>
        <xsl:when test="$first='stat'">statistics</xsl:when>
        <xsl:when test="$first='stav'">civil engineering</xsl:when>
        <xsl:when test="$first='škol'">education</xsl:when>
        <xsl:when test="$first='tech'">technics</xsl:when>
        <xsl:when test="$first='těl'">sports</xsl:when>
        <xsl:when test="$first='text'">textile industry</xsl:when>
        <xsl:when test="$first='úč'">accounting</xsl:when>
        <xsl:when test="$first='úř'">official expression</xsl:when>
        <xsl:when test="$first='veř spr'">public administration</xsl:when>
        <xsl:when test="$first='vet'">veterinary</xsl:when>
        <xsl:when test="$first='voj'">military</xsl:when>
        <xsl:when test="$first='výp tech'">computer technology</xsl:when>
        <xsl:when test="$first='výr'">production</xsl:when>
        <xsl:when test="$first='výtv'">design</xsl:when>
        <xsl:when test="$first='zahr'">horticulture</xsl:when>
        <xsl:when test="$first='zbož'">merchandise</xsl:when>
        <xsl:when test="$first='zeměd'">agriculture</xsl:when>
        <xsl:when test="$first='zeměp'">geography</xsl:when>
        <xsl:when test="$first='zool'">zoology</xsl:when>
        <xsl:when test="$first='cirkev'">church</xsl:when>
        <xsl:when test="$first='adm'">administration</xsl:when>
        <xsl:when test="$first='artistika'">artistics</xsl:when>
        <xsl:when test="$first='astro'">astronomy</xsl:when>
        <xsl:when test="$first='biblhist'">bibl. hist.</xsl:when>
        <xsl:when test="$first='bibl'">biblical studies</xsl:when>
        <xsl:when test="$first='biochemie'">biochemistry</xsl:when>
        <xsl:when test="$first='cirar'">religious archit.</xsl:when>
        <xsl:when test="$first='cirhi'">church history</xsl:when>
        <xsl:when test="$first='cirhu'">church music</xsl:when>
        <xsl:when test="$first='cirkr'">christianity</xsl:when>
        <xsl:when test="$first='cirpol'">church politics</xsl:when>
        <xsl:when test="$first='cirpr'">church law</xsl:when>
        <xsl:when test="$first='cukr'">sugar industry</xsl:when>
        <xsl:when test="$first='ekpub'">economy publications</xsl:when>
        <xsl:when test="$first='eltech'">electrotechnics</xsl:when>
        <xsl:when test="$first='estet'">esthetics</xsl:when>
        <xsl:when test="$first='farmak'">pharmacology</xsl:when>
        <xsl:when test="$first='folklor'">folklore</xsl:when>
        <xsl:when test="$first='fonetika'">phonetics</xsl:when>
        <xsl:when test="$first='fyzchem'">physical chemistry</xsl:when>
        <xsl:when test="$first='genetika'">genetics</xsl:when>
        <xsl:when test="$first='geochem'">geochem.</xsl:when>
        <xsl:when test="$first='geodez'">geodesy</xsl:when>
        <xsl:when test="$first='geofyz'">geophysicsa</xsl:when>
        <xsl:when test="$first='geogr'">geography</xsl:when>
        <xsl:when test="$first='geolzem'">geology agriculture</xsl:when>
        <xsl:when test="$first='hiadm'">history of administration</xsl:when>
        <xsl:when test="$first='hiar'">history of architecture</xsl:when>
        <xsl:when test="$first='hiast'">history of astronomy</xsl:when>
        <xsl:when test="$first='hidip'">history of diplomacy</xsl:when>
        <xsl:when test="$first='hidiv'">history of theatre</xsl:when>
        <xsl:when test="$first='hieko'">history of economics</xsl:when>
        <xsl:when test="$first='hietno'">history of ethnography</xsl:when>
        <xsl:when test="$first='hifil'">history of philosophy</xsl:when>
        <xsl:when test="$first='hifot'">history of photography</xsl:when>
        <xsl:when test="$first='higeo'">history of geography</xsl:when>
        <xsl:when test="$first='hilit'">history of literature</xsl:when>
        <xsl:when test="$first='himat'">history of mathematics</xsl:when>
        <xsl:when test="$first='hinab'">history of religion</xsl:when>
        <xsl:when test="$first='hipen'">history of banking</xsl:when>
        <xsl:when test="$first='hipol'">history of politics</xsl:when>
        <xsl:when test="$first='hipr'">history of law</xsl:when>
        <xsl:when test="$first='hisk'">history of education</xsl:when>
        <xsl:when test="$first='hispo'">history of sports</xsl:when>
        <xsl:when test="$first='hista'">history of civil engineering</xsl:when>
        <xsl:when test="$first='hitech'">history of technology</xsl:when>
        <xsl:when test="$first='hivoj'">history of military</xsl:when>
        <xsl:when test="$first='sach'">chess</xsl:when>
        <xsl:when test="$first='hudpubl'">music journalism</xsl:when>
        <xsl:when test="$first='jadtech'">nuclear technology</xsl:when>
        <xsl:when test="$first='keram'">ceramics</xsl:when>
        <xsl:when test="$first='kniho'">librarianship</xsl:when>
        <xsl:when test="$first='kosmet'">cosmetics</xsl:when>
        <xsl:when test="$first='kosmon'">astronautics</xsl:when>
        <xsl:when test="$first='kozed'">leather industry</xsl:when>
        <xsl:when test="$first='krejc'">tailoring</xsl:when>
        <xsl:when test="$first='kynol'">cynology</xsl:when>
        <xsl:when test="$first='lesnic'">forestry</xsl:when>
        <xsl:when test="$first='lethist'">history of aviation</xsl:when>
        <xsl:when test="$first='lingv'">linguistics</xsl:when>
        <xsl:when test="$first='liter'">history of literature</xsl:when>
        <xsl:when test="$first='lodar'">shipbuilding</xsl:when>
        <xsl:when test="$first='matlog'">mathematical logic</xsl:when>
        <xsl:when test="$first='mezobch'">international trade</xsl:when>
        <xsl:when test="$first='mezpr'">international law</xsl:when>
        <xsl:when test="$first='nabpol'">religious politics</xsl:when>
        <xsl:when test="$first='nabps'">religious psychology</xsl:when>
        <xsl:when test="$first='namor'">marine</xsl:when>
        <xsl:when test="$first='numiz'">numismatics</xsl:when>
        <xsl:when test="$first='obchpr'">trade law</xsl:when>
        <xsl:when test="$first='obuv'">shoe industry</xsl:when>
        <xsl:when test="$first='odb'">professional</xsl:when>
        <xsl:when test="$first='optika'">optics</xsl:when>
        <xsl:when test="$first='orient'">oriental</xsl:when>
        <xsl:when test="$first='paleo'">paleontology</xsl:when>
        <xsl:when test="$first='papir'">paper industry</xsl:when>
        <xsl:when test="$first='pedol'">pedology</xsl:when>
        <xsl:when test="$first='piv'">brewing</xsl:when>
        <xsl:when test="$first='politadm'">politics and administration</xsl:when>
        <xsl:when test="$first='politeko'">political economics</xsl:when>
        <xsl:when test="$first='potrobch'">food trade</xsl:when>
        <xsl:when test="$first='hipr'">historz of law</xsl:when>
        <xsl:when test="$first='publ'">journalism</xsl:when>
        <xsl:when test="$first='rybar'">fishery</xsl:when>
        <xsl:when test="$first='sdel'">communication technology</xsl:when>
        <xsl:when test="$first='sport'">sports</xsl:when>
        <xsl:when test="$first='sporpub'">sports journalism</xsl:when>
        <xsl:when test="$first='stroj'">engineering</xsl:when>
        <xsl:when test="$first='tanec'">dance</xsl:when>
        <xsl:when test="$first='teol'">theology</xsl:when>
        <xsl:when test="$first='tesn'">těsn.</xsl:when>
        <xsl:when test="$first='textobch'">textile trade</xsl:when>
        <xsl:when test="$first='umel'">artistic</xsl:when>
        <xsl:when test="$first='umrem'">arts and crafts</xsl:when>
        <xsl:when test="$first='vcel'">beekeeping</xsl:when>
        <xsl:when test="$first='vinar'">viniculture</xsl:when>
        <xsl:when test="$first='vodohos'">water management</xsl:when>
        <xsl:when test="$first='vojhist'">history of army</xsl:when>
        <xsl:when test="$first='vojnam'">voj. nám.</xsl:when>
        <xsl:when test="$first='vor'">rafting</xsl:when>
        <xsl:when test="$first='zeldop'">rail transport</xsl:when>
        <xsl:when test="$first='zpravpubl'">news journalism</xsl:when>
        <xsl:when test="$first='zurn'">journalism</xsl:when>
      </xsl:choose>    
      <xsl:if test="$rest">, </xsl:if>
    </xsl:if>
    <xsl:if test='$rest'>
      <xsl:call-template name='split_oblast'>
        <xsl:with-param name='text' select='$rest'/>
      </xsl:call-template>
    </xsl:if>
    <xsl:if test='not($rest)'>
      <xsl:choose>
             <xsl:when test="$text='anat'">anatomy</xsl:when>
        <xsl:when test="$text='antr'">antropology</xsl:when>
        <xsl:when test="$text='archeol'">archeology</xsl:when>
        <xsl:when test="$text='archit'">architecture</xsl:when>
        <xsl:when test="$text='biol'">biology</xsl:when>
        <xsl:when test="$text='bot'">botanics</xsl:when>
        <xsl:when test="$text='dipl'">diplomatics</xsl:when>
        <xsl:when test="$text='div'">theatre</xsl:when>
        <xsl:when test="$text='dopr'">transport</xsl:when>
        <xsl:when test="$text='ekol'">ecology</xsl:when>
        <xsl:when test="$text='ekon'">economics</xsl:when>
        <xsl:when test="$text='eltech'">elektrotechnics</xsl:when>
        <xsl:when test="$text='etn'">ethnography</xsl:when>
        <xsl:when test="$text='feud'">feudalism</xsl:when>
        <xsl:when test="$text='filat'">philately</xsl:when>
        <xsl:when test="$text='film'">film</xsl:when>
        <xsl:when test="$text='filoz'">philosopsy</xsl:when>
        <xsl:when test="$text='fot'">photography</xsl:when>
        <xsl:when test="$text='fyz'">physics</xsl:when>
        <xsl:when test="$text='fyziol'">physiology</xsl:when>
        <xsl:when test="$text='geol'">geology</xsl:when>
        <xsl:when test="$text='geom'">geometry</xsl:when>
        <xsl:when test="$text='gnoz'">gnoseology</xsl:when>
        <xsl:when test="$text='hist'">history</xsl:when>
        <xsl:when test="$text='horn'">mining</xsl:when>
        <xsl:when test="$text='horol'">mountaineering</xsl:when>
        <xsl:when test="$text='hosp'">industry</xsl:when>
        <xsl:when test="$text='hud'">musicology</xsl:when>
        <xsl:when test="$text='hut'">metallurgy</xsl:when>
        <xsl:when test="$text='hvězd'">astronomy</xsl:when>
        <xsl:when test="$text='chem'">chemistry</xsl:when>
        <xsl:when test="$text='ideal'">idealism</xsl:when>
        <xsl:when test="$text='informatika'">computer science</xsl:when>
        <xsl:when test="$text='jad'">nuclear physics</xsl:when>
        <xsl:when test="$text='jaz'">philology</xsl:when>
        <xsl:when test="$text='kapit'">capitalism</xsl:when>
        <xsl:when test="$text='karet'">card expression</xsl:when>
        <xsl:when test="$text='katol církvi'">catholicism</xsl:when>
        <xsl:when test="$text='krim'">criminology</xsl:when>
        <xsl:when test="$text='křesť'">christianity</xsl:when>
        <xsl:when test="$text='kuch'">cookery</xsl:when>
        <xsl:when test="$text='kult'">culture</xsl:when>
        <xsl:when test="$text='kyb'">cybernetics</xsl:when>
        <xsl:when test="$text='lék'">medicine</xsl:when>
        <xsl:when test="$text='lékár'">pharmaceutics</xsl:when>
        <xsl:when test="$text='let'">aviation</xsl:when>
        <xsl:when test="$text='liter'">literary science</xsl:when>
        <xsl:when test="$text='log'">logic</xsl:when>
        <xsl:when test="$text='marx'">marxism</xsl:when>
        <xsl:when test="$text='mat'">mathematics</xsl:when>
        <xsl:when test="$text='meteor'">meteorology</xsl:when>
        <xsl:when test="$text='miner'">mineralogy</xsl:when>
        <xsl:when test="$text='motor'">motorism</xsl:when>
        <xsl:when test="$text='mysl'">hunting expression</xsl:when>
        <xsl:when test="$text='mytol'">mythology</xsl:when>
        <xsl:when test="$text='náb'">religion</xsl:when>
        <xsl:when test="$text='nár'">ethnography</xsl:when>
        <xsl:when test="$text='obch'">trade</xsl:when>
        <xsl:when test="$text='pedag'">pedagogy</xsl:when>
        <xsl:when test="$text='peněž'">banking</xsl:when>
        <xsl:when test="$text='podnikani'">business</xsl:when>
        <xsl:when test="$text='polit'">politics</xsl:when>
        <xsl:when test="$text='polygr'">polygraphy</xsl:when>
        <xsl:when test="$text='pošt'">postal operations</xsl:when>
        <xsl:when test="$text='potrav'">food industry</xsl:when>
        <xsl:when test="$text='práv'">law</xsl:when>
        <xsl:when test="$text='prům'">industry</xsl:when>
        <xsl:when test="$text='přír'">nature</xsl:when>
        <xsl:when test="$text='psych'">psychology</xsl:when>
        <xsl:when test="$text='rybn'">fish farming</xsl:when>
        <xsl:when test="$text='řem'">craft</xsl:when>
        <xsl:when test="$text='sklář'">glassblowing</xsl:when>
        <xsl:when test="$text='soc'">socialism</xsl:when>
        <xsl:when test="$text='sociol'">sociology</xsl:when>
        <xsl:when test="$text='stat'">statistics</xsl:when>
        <xsl:when test="$text='stav'">civil engineering</xsl:when>
        <xsl:when test="$text='škol'">education</xsl:when>
        <xsl:when test="$text='tech'">technics</xsl:when>
        <xsl:when test="$text='těl'">sports</xsl:when>
        <xsl:when test="$text='text'">textile industry</xsl:when>
        <xsl:when test="$text='úč'">accounting</xsl:when>
        <xsl:when test="$text='úř'">official expression</xsl:when>
        <xsl:when test="$text='veř spr'">public administration</xsl:when>
        <xsl:when test="$text='vet'">veterinary</xsl:when>
        <xsl:when test="$text='voj'">military</xsl:when>
        <xsl:when test="$text='výp tech'">computer technology</xsl:when>
        <xsl:when test="$text='výr'">production</xsl:when>
        <xsl:when test="$text='výtv'">design</xsl:when>
        <xsl:when test="$text='zahr'">horticulture</xsl:when>
        <xsl:when test="$text='zbož'">merchandise</xsl:when>
        <xsl:when test="$text='zeměd'">agriculture</xsl:when>
        <xsl:when test="$text='zeměp'">geography</xsl:when>
        <xsl:when test="$text='zool'">zoology</xsl:when>
        <xsl:when test="$text='cirkev'">church</xsl:when>
        <xsl:when test="$text='adm'">administration</xsl:when>
        <xsl:when test="$text='artistika'">artistics</xsl:when>
        <xsl:when test="$text='astro'">astronomy</xsl:when>
        <xsl:when test="$text='biblhist'">bibl. hist.</xsl:when>
        <xsl:when test="$text='bibl'">biblical studies</xsl:when>
        <xsl:when test="$text='biochemie'">biochemistry</xsl:when>
        <xsl:when test="$text='cirar'">religious archit.</xsl:when>
        <xsl:when test="$text='cirhi'">church history</xsl:when>
        <xsl:when test="$text='cirhu'">church music</xsl:when>
        <xsl:when test="$text='cirkr'">christianity</xsl:when>
        <xsl:when test="$text='cirpol'">church politics</xsl:when>
        <xsl:when test="$text='cirpr'">church law</xsl:when>
        <xsl:when test="$text='cukr'">sugar industry</xsl:when>
        <xsl:when test="$text='ekpub'">economy publications</xsl:when>
        <xsl:when test="$text='eltech'">electrotechnics</xsl:when>
        <xsl:when test="$text='estet'">esthetics</xsl:when>
        <xsl:when test="$text='farmak'">pharmacology</xsl:when>
        <xsl:when test="$text='folklor'">folklore</xsl:when>
        <xsl:when test="$text='fonetika'">phonetics</xsl:when>
        <xsl:when test="$text='fyzchem'">physical chemistry</xsl:when>
        <xsl:when test="$text='genetika'">genetics</xsl:when>
        <xsl:when test="$text='geochem'">geochem.</xsl:when>
        <xsl:when test="$text='geodez'">geodesy</xsl:when>
        <xsl:when test="$text='geofyz'">geophysicsa</xsl:when>
        <xsl:when test="$text='geogr'">geography</xsl:when>
        <xsl:when test="$text='geolzem'">geology agriculture</xsl:when>
        <xsl:when test="$text='hiadm'">history of administration</xsl:when>
        <xsl:when test="$text='hiar'">history of architecture</xsl:when>
        <xsl:when test="$text='hiast'">history of astronomy</xsl:when>
        <xsl:when test="$text='hidip'">history of diplomacy</xsl:when>
        <xsl:when test="$text='hidiv'">history of theatre</xsl:when>
        <xsl:when test="$text='hieko'">history of economics</xsl:when>
        <xsl:when test="$text='hietno'">history of ethnography</xsl:when>
        <xsl:when test="$text='hifil'">history of philosophy</xsl:when>
        <xsl:when test="$text='hifot'">history of photography</xsl:when>
        <xsl:when test="$text='higeo'">history of geography</xsl:when>
        <xsl:when test="$text='hilit'">history of literature</xsl:when>
        <xsl:when test="$text='himat'">history of mathematics</xsl:when>
        <xsl:when test="$text='hinab'">history of religion</xsl:when>
        <xsl:when test="$text='hipen'">history of banking</xsl:when>
        <xsl:when test="$text='hipol'">history of politics</xsl:when>
        <xsl:when test="$text='hipr'">history of law</xsl:when>
        <xsl:when test="$text='hisk'">history of education</xsl:when>
        <xsl:when test="$text='hispo'">history of sports</xsl:when>
        <xsl:when test="$text='hista'">history of civil engineering</xsl:when>
        <xsl:when test="$text='hitech'">history of technology</xsl:when>
        <xsl:when test="$text='hivoj'">history of military</xsl:when>
        <xsl:when test="$text='sach'">chess</xsl:when>
        <xsl:when test="$text='hudpubl'">music journalism</xsl:when>
        <xsl:when test="$text='jadtech'">nuclear technology</xsl:when>
        <xsl:when test="$text='keram'">ceramics</xsl:when>
        <xsl:when test="$text='kniho'">librarianship</xsl:when>
        <xsl:when test="$text='kosmet'">cosmetics</xsl:when>
        <xsl:when test="$text='kosmon'">astronautics</xsl:when>
        <xsl:when test="$text='kozed'">leather industry</xsl:when>
        <xsl:when test="$text='krejc'">tailoring</xsl:when>
        <xsl:when test="$text='kynol'">cynology</xsl:when>
        <xsl:when test="$text='lesnic'">forestry</xsl:when>
        <xsl:when test="$text='lethist'">history of aviation</xsl:when>
        <xsl:when test="$text='lingv'">linguistics</xsl:when>
        <xsl:when test="$text='liter'">history of literature</xsl:when>
        <xsl:when test="$text='lodar'">shipbuilding</xsl:when>
        <xsl:when test="$text='matlog'">mathematical logic</xsl:when>
        <xsl:when test="$text='mezobch'">international trade</xsl:when>
        <xsl:when test="$text='mezpr'">international law</xsl:when>
        <xsl:when test="$text='nabpol'">religious politics</xsl:when>
        <xsl:when test="$text='nabps'">religious psychology</xsl:when>
        <xsl:when test="$text='namor'">marine</xsl:when>
        <xsl:when test="$text='numiz'">numismatics</xsl:when>
        <xsl:when test="$text='obchpr'">trade law</xsl:when>
        <xsl:when test="$text='obuv'">shoe industry</xsl:when>
        <xsl:when test="$text='odb'">professional</xsl:when>
        <xsl:when test="$text='optika'">optics</xsl:when>
        <xsl:when test="$text='orient'">oriental</xsl:when>
        <xsl:when test="$text='paleo'">paleontology</xsl:when>
        <xsl:when test="$text='papir'">paper industry</xsl:when>
        <xsl:when test="$text='pedol'">pedology</xsl:when>
        <xsl:when test="$text='piv'">brewing</xsl:when>
        <xsl:when test="$text='politadm'">politics and administration</xsl:when>
        <xsl:when test="$text='politeko'">political economics</xsl:when>
        <xsl:when test="$text='potrobch'">food trade</xsl:when>
        <xsl:when test="$text='hipr'">historz of law</xsl:when>
        <xsl:when test="$text='publ'">journalism</xsl:when>
        <xsl:when test="$text='rybar'">fishery</xsl:when>
        <xsl:when test="$text='sdel'">communication technology</xsl:when>
        <xsl:when test="$text='sport'">sports</xsl:when>
        <xsl:when test="$text='sporpub'">sports journalism</xsl:when>
        <xsl:when test="$text='stroj'">engineering</xsl:when>
        <xsl:when test="$text='tanec'">dance</xsl:when>
        <xsl:when test="$text='teol'">theology</xsl:when>
        <xsl:when test="$text='tesn'">těsn.</xsl:when>
        <xsl:when test="$text='textobch'">textile trade</xsl:when>
        <xsl:when test="$text='umel'">artistic</xsl:when>
        <xsl:when test="$text='umrem'">arts and crafts</xsl:when>
        <xsl:when test="$text='vcel'">beekeeping</xsl:when>
        <xsl:when test="$text='vinar'">viniculture</xsl:when>
        <xsl:when test="$text='vodohos'">water management</xsl:when>
        <xsl:when test="$text='vojhist'">history of army</xsl:when>
        <xsl:when test="$text='vojnam'">voj. nám.</xsl:when>
        <xsl:when test="$text='vor'">rafting</xsl:when>
        <xsl:when test="$text='zeldop'">rail transport</xsl:when>
        <xsl:when test="$text='zpravpubl'">news journalism</xsl:when>
        <xsl:when test="$text='zurn'">journalism</xsl:when>
      </xsl:choose>    
    </xsl:if>
  </xsl:template>

  <xsl:template match="variant" mode="ingram">
  </xsl:template>

  <xsl:template match="variant">
    <xsl:if test="./@lemma_id!=''">
      <a href="/cs?action=search&amp;getdoc={@lemma_id}&amp;lang=en"><xsl:value-of select="@title"/></a>
    </xsl:if>
    <xsl:if test="./@lemma_id='' or not(./@lemma_id)">
      <xsl:value-of select="."/>
    </xsl:if>
    <xsl:if test="position()!=last()">, </xsl:if>
  </xsl:template>

  <xsl:template match="revcolloc">
    <br/>
    <!--<div class="czj-lemma-preview-sm">-->
      <!--<div class="czj-lemma-preview-sm-header">
        <div class="alignleft"></div>
        <div class="alignright"><i class="fa fa-external-link"></i></div> 
      </div>-->  

      <span style="vertical-align:top; display:inline-block;">
        <span onclick="open_iframe('{@lemma_id}', 'cs', 'en')" style="cursor:pointer;color:blue;text-decoration:underline">
          <!--<xsl:value-of select="@lemma_id"/>-->
          <img src="/media/img/slovnik-expand.png" height="20"/>
          <span class="colloctitle" style="vertical-align:top; display:inline-block;">
            <xsl:value-of select="title"/>
          </span>
        </span>
      </span>
      <div style="clear:both; margin-bottom: 5px;"></div>
    <!--</div>-->
    <iframe class="colloc-iframe" id="colloc{@lemma_id}" src="about:blank" style="display:none" onload="iframe_loaded('{@lemma_id}')"></iframe>
  </xsl:template>

  <xsl:template match="text">
    <xsl:apply-templates/>
  </xsl:template>
  <xsl:template match="text" mode="ingram">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="i">
    <i><xsl:apply-templates/></i>
  </xsl:template>
  <xsl:template match="i" mode="ingram">
    <i><xsl:apply-templates/></i>
  </xsl:template>

  <xsl:template match="small">
    <small><xsl:apply-templates/></small>
  </xsl:template>

  <xsl:template match="b">
    <b><xsl:apply-templates/></b>
  </xsl:template>

  <xsl:template match="sub">
    <sub><xsl:apply-templates/></sub>
  </xsl:template>

  <xsl:template match="sup">
    <sup><xsl:apply-templates/></sup>
  </xsl:template>
  <xsl:template match="small" mode="ingram">
    <small><xsl:apply-templates/></small>
  </xsl:template>

  <xsl:template match="b" mode="ingram">
    <b><xsl:apply-templates/></b>
  </xsl:template>

  <xsl:template match="sub" mode="ingram">
    <sub><xsl:apply-templates/></sub>
  </xsl:template>

  <xsl:template match="sup" mode="ingram">
    <sup><xsl:apply-templates/></sup>
  </xsl:template>

  <xsl:template match="link">
    <xsl:if test="not(link_mean)">
     <xsl:if test="@auto_complete='1' or $perm!='ro' or $skupina_test='true'">
      <a href="/cs?action=search&amp;getdoc={@lemma_id}&amp;lang=en"><xsl:value-of select="."/></a>
    </xsl:if>
     <xsl:if test="not(@auto_complete='1' or $perm!='ro' or $skupina_test='true')">
      <xsl:value-of select="."/>
     </xsl:if>
    </xsl:if>
    <xsl:if test="link_mean">
      <xsl:value-of select="."/> 
      <xsl:if test="@auto_complete='1' or $perm!='ro' or $skupina_test='true'"> 
      (<xsl:for-each select="link_mean">
        <xsl:sort select="link_mean"/>
        <xsl:if test="@status='published' or $perm!='ro' or $skupina_test='true'">     <!-- implementovat do ilnine a EN -->
          <a href="/cs?action=search&amp;getdoc={../@lemma_id}&amp;lang=en">meaning <xsl:value-of select="@number"/></a>
        </xsl:if>     
        <xsl:if test="position()!=last()">, </xsl:if>
      </xsl:for-each><xsl:text>)</xsl:text>
    </xsl:if>
    </xsl:if>
  </xsl:template>
  <xsl:template match="link" mode="ingram">
    <xsl:if test="not(link_mean)">
      <a href="/cs?action=search&amp;getdoc={@lemma_id}&amp;lang=en"><xsl:value-of select="."/></a>
    </xsl:if>
    <xsl:if test="link_mean">
      <xsl:value-of select="."/> 
      (<xsl:for-each select="link_mean">
       <xsl:sort select="link_mean"/>
        <a href="/cs?action=search&amp;getdoc={../@lemma_id}&amp;lang=en">meaning <xsl:value-of select="@number"/></a>
        <xsl:if test="position()!=last()">, </xsl:if>
      </xsl:for-each><xsl:text>)</xsl:text>
    </xsl:if>
  </xsl:template>

  <xsl:template match="html//*">
    <xsl:copy>
      <xsl:apply-templates/>
    </xsl:copy>
  </xsl:template>
  <xsl:template name="status_publish">
    <xsl:param name="status"/>
    <xsl:param name="type"/>
    <xsl:param name="element"/>
    <xsl:if test="$status='published'">
      approved
    </xsl:if>
    <xsl:if test="$status!='published'">
     <!--hidden--><xsl:text> </xsl:text>
      <xsl:choose>
        <xsl:when test="$type='relation'">
          <input type="button" data-type="{$element/@type}" onclick="publish_relation('cs', '{/entry/@id}', '{$element/../@id}', '{$element/@meaning_id}', '{$element/@type}', this)" value="schválit"/>
        </xsl:when>
        <xsl:when test="$type='usage'">
          <input type="button" data-type="usage" onclick="publish_usage('cs', '{/entry/@id}', '{$element/../../@id}', '{$element/@id}', this)" value="schválit"/>
        </xsl:when>
        <xsl:when test="$type='meaning'">
          <input type="button" data-type="meaning" onclick="publish_meaning('cs', '{/entry/@id}', '{$element/@id}', this)" value="schválit"/>
        </xsl:when>
        <xsl:when test="$type='gram' or $type='style' or $type='sw' or $type='hns' or $type='video_front' or $type='video_side' or $type='entry'">
          <input type="button" data-type="entry" onclick="publish('cs', '{/entry/@id}', '{$type}', this)" value="schválit"/>
        </xsl:when>
      </xsl:choose>
    </xsl:if>
  </xsl:template>

</xsl:stylesheet>
