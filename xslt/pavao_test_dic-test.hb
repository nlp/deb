<html><head><meta content='text/html[-semicolon-] charset=utf-8' http-equiv='Content-Type'/><title>Test dictionary: {{entry.hw}} (DEBWrite)</title></head><body>
<h1>{{entry.hw}}</h1>
<span><b>headword</b>: {{entry.hw}}</span><br/>
<span><b>part of speech</b>: {{entry.pos}}</span><br/>
<span><b>area</b>: {{entry.area}}</span><br/>
<div style="border: 1px solid #000">
<span><b>definition</b>: {{entry.def}}</span><br/>
</div>
<div style="border: 1px solid #000">
<span><b>west</b>: {{entry.west}}</span><br/>
<span><b>south</b>: {{entry.south}}</span><br/>
<span><b>east</b>: {{entry.east}}</span><br/>
</div>
<span><b>etymology</b>: {{entry.etym}}</span><br/>
<span><b>references</b>: {{entry.reference}}</span><br/>
<span><b>comments</b>: {{entry.comment}}</span><br/>
</body></html>
