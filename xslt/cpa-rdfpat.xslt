<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output encoding="utf-8" method="xml" indent="yes"/>
<xsl:template match="entry">
	<RDF:RDF xmlns:RDF="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:d="http://nlp.fi.muni.cz/cpa/rdf#" xmlns:nc="http://home.netscape.com/NC-rdf#">
	<!--data-->
	<xsl:apply-templates select="pattern" mode="data">
		<xsl:sort select="@pattern_order" data-type="number"/>
	</xsl:apply-templates>
	<!--tree-->
	<RDF:Seq about="urn:xmlslov:cpa:patterns">
		<xsl:apply-templates select="pattern" mode="tree">
		<xsl:sort select="@pattern_order" data-type="number"/>
		</xsl:apply-templates>
	</RDF:Seq>
	</RDF:RDF>
</xsl:template>


<!-- ================= -->
<!-- CREATE PREDICATOR -->
<!-- ================= -->
<xsl:template name="create_predicator">
	<xsl:if test="subordinator!=''">
		<xsl:value-of select="subordinator"/>
		<xsl:text> </xsl:text>
	</xsl:if>
	<xsl:if test="govern_phrase!=''">
		<xsl:value-of select="govern_phrase"/>
		<xsl:text> </xsl:text>
	</xsl:if>
	<xsl:if test="mood!=''">
		<xsl:value-of select="mood"/>
		<xsl:text> </xsl:text>
	</xsl:if>
	<xsl:if test="verb_form!=''">
		<xsl:value-of select="verb_form"/>
	</xsl:if>
	<xsl:if test="not(verb_form!='')">
		<xsl:value-of select="../lemma"/>
	</xsl:if>
	<xsl:text> </xsl:text>
	<xsl:if test="verb_form_char!=''">
		<xsl:text>* </xsl:text>
	</xsl:if>
	<xsl:if test="@negative_form='yes'">
		<xsl:text>NEG </xsl:text>
	</xsl:if>
	<xsl:if test="@mod_bare_inf">V-INF </xsl:if>
	<xsl:if test="@mod_to_inf">V-to-INF </xsl:if>
	<xsl:if test="@mod_ing">V-ing </xsl:if>
	<xsl:if test="@mod_ed">V-ed </xsl:if>
	<xsl:if test="coordinator!=''">
		<xsl:value-of select="coordinator"/>
		<xsl:text> </xsl:text>
	</xsl:if>
	<xsl:if test="lemma!=''">
		<xsl:value-of select="lemma"/>
		<xsl:text> </xsl:text>
	</xsl:if>
<!-- 	<xsl:text> </xsl:text> -->
</xsl:template>


<!-- ================= -->
<!-- CREATE ADVERBIALS -->
<!-- ================= -->
<xsl:template name="create_adverbials">
	<xsl:for-each select="adverbial">
		<xsl:if test="@adverbial_type!=''">
			<xsl:text>Adv</xsl:text>
			<xsl:value-of select="@adverbial_type"/>
		</xsl:if>
		<xsl:apply-templates select="adverbial_alternation"/>
	</xsl:for-each>
</xsl:template>


<!-- ============================================ -->
<!-- CREATE PATTERN STRING - ATTRIBUTES, SENTENCE -->
<!-- ============================================ -->
<xsl:template name="create_patstring">
	<xsl:if test="group!=''">[group: <xsl:value-of select="group"/>] </xsl:if>
	<xsl:if test="@no_object='yes'">[no object] </xsl:if>
	<xsl:if test="@no_adverbial='yes'">[no adverbial] </xsl:if>
	<xsl:if test="@idiom='yes'">[idiom] </xsl:if>
	<xsl:if test="@phrasal_verb='yes'">[phrasal verb] </xsl:if>
	<xsl:if test="@lv='yes'">[light verb] </xsl:if>
	<xsl:if test="@passive_form='yes'">[passive] </xsl:if>
	<xsl:if test=".//@reciprocity='yes'">[reciprocity] </xsl:if>
	<xsl:if test="@domain!=''">[domain=<xsl:value-of select="@domain"/>] </xsl:if>
	<xsl:if test="@register!=''">[register=<xsl:value-of select="@register"/>] </xsl:if>
	<xsl:if test="general_comment!=''">see comment</xsl:if>
	<xsl:text>
</xsl:text>
	
<!-- EXACT PHRASE -->
	<xsl:if test="exact_phrase!=''">
		exact phrase=<xsl:value-of select="exact_phrase"/>
		<xsl:text>
</xsl:text>
	</xsl:if>
	
<!-- SUBJECT -->
	<xsl:apply-templates select="subject"/>
	
<!-- PREDICATE -->
	<xsl:call-template name="create_predicator"/>
	
<!-- INDIRECT OBJECT -->
	<xsl:apply-templates select="indirect_object"/>

<!-- DIRECT OBJECT -->
	<xsl:if test="not(@no_object='yes')">
		<xsl:apply-templates select="object"/>
	</xsl:if>
	
<!-- COMPLEMENTS -->
	<xsl:apply-templates select="noun_complement"/>
	<xsl:apply-templates select="adj_complement"/>
	
<!-- ADVERBIALS -->
	<xsl:if test="not(@no_adverbial='yes')">
		<xsl:call-template name="create_adverbials"/>
	</xsl:if>
	
</xsl:template>


<!-- ============================================================ -->
<!-- CREATE HEAD - SEMANTIC TYPE | {LEXSET} | CLAUSES = ROLE = PL -->
<!-- ============================================================ -->
<xsl:template name="create_head">
<!-- SEMANTIC TYPE -->
	<xsl:if test="semantic_type!=''">
		<xsl:value-of select="semantic_type"/>
	</xsl:if>
	
<!-- LEXICAL SET -->
	<xsl:if test="lexical_set!=''">
		<xsl:if test="semantic_type!=''">
			<xsl:text> | </xsl:text>
		</xsl:if>
		<xsl:text>{</xsl:text><xsl:value-of select="lexical_set"/><xsl:text>}</xsl:text>
	</xsl:if>
	
<!-- CLAUSES -->
	<xsl:if test="(@bare_inf='yes' or @clause_allowed_to='yes' or @clause_allowed_that='yes' or @clause_allowed_ing='yes' or @clause_allowed_quote='yes' or @clause_allowed_wh='yes') and (semantic_type!='' or lexical_set!='')">
		<xsl:text> | </xsl:text>
	</xsl:if>
	
	<xsl:if test="@clause_allowed_to='yes'">
		<xsl:text>TO+INF</xsl:text>
	</xsl:if>
	<xsl:if test="@clause_allowed_ing='yes'">
		<xsl:if test="@clause_allowed_to='yes'"><xsl:text>|</xsl:text></xsl:if>
		<xsl:text>-ING</xsl:text>
	</xsl:if>
	<xsl:if test="@clause_allowed_that='yes'">
		<xsl:if test="@clause_allowed_to='yes' or @clause_allowed_ing='yes'"><xsl:text>|</xsl:text></xsl:if>
		<xsl:text>THAT-CL</xsl:text>
	</xsl:if>
	<xsl:if test="@clause_allowed_wh='yes'">
		<xsl:if test="@clause_allowed_to='yes' or @clause_allowed_ing='yes' or @clause_allowed_that='yes'"><xsl:text>|</xsl:text></xsl:if>
		<xsl:text>WH-CL</xsl:text>
	</xsl:if>
	<xsl:if test="@clause_allowed_quote='yes'">
		<xsl:if test="@clause_allowed_to='yes' or @clause_allowed_ing='yes' or @clause_allowed_that='yes' or @clause_allowed_wh='yes'"><xsl:text>|</xsl:text></xsl:if>
		<xsl:text>QUOTE</xsl:text>
	</xsl:if>
	<xsl:if test="@bare_inf='yes'">
		<xsl:if test="@clause_allowed_to='yes' or @clause_allowed_ing='yes' or @clause_allowed_that='yes' or @clause_allowed_wh='yes' or @clause_allowed_quote='yes'"><xsl:text>|</xsl:text></xsl:if>
		<xsl:text>BARE-INF</xsl:text>
	</xsl:if>
	
<!-- SEMANTIC ROLE -->
	<xsl:if test="semantic_role!=''"> = <xsl:value-of select="semantic_role"/> </xsl:if>
	<xsl:if test="role!=''"> = <xsl:value-of select="role"/> </xsl:if>
<!-- PLURAL -->
	<xsl:if test="@plural='yes'"> = PL</xsl:if>

</xsl:template>


<!-- ================== -->
<!-- CREATE NOUN PHRASE -->
<!-- ================== -->
<xsl:template name="create_noun_phrase">
<!-- PREDETERMINER -->
	<xsl:if test="predeterminer!=''">
		<xsl:value-of select="predeterminer"/>
		<xsl:text> </xsl:text>
	</xsl:if>
	
	<xsl:if test="@possessor='yes'">
		<xsl:text>POSS </xsl:text>
	</xsl:if>
	
<!-- DETERMINER/QUANTIFIER -->
	<xsl:if test="determiner!=''">
		<xsl:value-of select="determiner"/>
		<xsl:text> </xsl:text>
	</xsl:if>
	

<!-- MODIFIER -->
	<xsl:variable name="head">
		<xsl:call-template name="create_head"/>
	</xsl:variable>
	
	<xsl:if test="subject_modifier!=''">
		<xsl:call-template name="replace-string">
			<xsl:with-param name="text" select="subject_modifier"/>
			<xsl:with-param name="replace" select="'&lt;head&gt;'"/>
			<xsl:with-param name="with" select="$head"/>
		</xsl:call-template>
	</xsl:if>
	
	<xsl:if test="modifier!=''">
	<xsl:call-template name="replace-string">
		<xsl:with-param name="text" select="modifier"/>
		<xsl:with-param name="replace" select="'&lt;head&gt;'"/>
		<xsl:with-param name="with" select="$head"/>
	</xsl:call-template>
	</xsl:if>
	
	<xsl:if test="not(subject_modifier!='' or modifier!='')">	
		<xsl:value-of select="$head"/>
	</xsl:if>
	
</xsl:template>


<!-- ======================================================================  -->

<xsl:template match="pattern" mode="data">
	<RDF:Description xmlns:RDF="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:d="http://nlp.fi.muni.cz/cpa/rdf#" xmlns:nc="http://home.netscape.com/NC-rdf#" about="urn:xmlslov:cpa:pattern{@pattern_order}">
	<d:prop>main</d:prop>
	<d:patid>
		<xsl:value-of select="@pattern_id"/>
	</d:patid>
	<d:num nc:parseType="Integer">
		<xsl:value-of select="@pattern_order"/>
	</d:num>
	<d:string>
		<xsl:call-template name="create_patstring"/>
	</d:string>
	<d:primary>
		<xsl:apply-templates select="implicature"/>
	</d:primary>
	<d:chid>ch_<xsl:value-of select="../lemma"/>_<xsl:value-of select="@pattern_id"/></d:chid>
	<d:secimp>
		<xsl:apply-templates select="additional_implicatures/implicature"/>
	</d:secimp>
	<d:pvidiom>
		<xsl:if test="@idiom='yes'">idiom</xsl:if>
		<xsl:if test="@phrasal_verb='yes'"> pv</xsl:if>
	</d:pvidiom>
	<d:domain>
		<xsl:value-of select="@domain"/>
	</d:domain>
	<d:register>
		<xsl:value-of select="@register"/>
	</d:register>
	</RDF:Description>
</xsl:template>

<xsl:template match="pattern" mode="tree">
	<RDF:li xmlns:RDF="http://www.w3.org/1999/02/22-rdf-syntax-ns#" resource="urn:xmlslov:cpa:pattern{@pattern_order}"/>
</xsl:template>


<xsl:template match="subject|indirect_object|object|noun_complement|adj_complement">
	
	
	<xsl:if test="last() != 1 and position()=1">
		<xsl:if test="@optional='yes'">(</xsl:if>
		<xsl:if test="not(@optional='yes')">[</xsl:if>
<!-- 		[ -->
	</xsl:if>
	
<!-- BEGIN OF ARGUMENT -->
	<xsl:if test="@optional='yes'">(</xsl:if>
	<xsl:if test="not(@optional='yes')">[</xsl:if>
	
<!-- INDIRECT OBJECT -->
	<xsl:if test="name()='indirect_object'">
	<xsl:if test="@preposition_for='yes' or @preposition_to='yes' or @other_preposition!=''">{</xsl:if>
	<xsl:if test="@preposition_for='yes'">for</xsl:if>
	<xsl:if test="@preposition_to='yes'">
		<xsl:if test="@preposition_for='yes'">
			<xsl:text>|</xsl:text>
		</xsl:if>	
		<xsl:text>to</xsl:text>
	</xsl:if>
	<xsl:if test="@other_preposition!=''">
		<xsl:if test="@preposition_for='yes' or @preposition_to='yes'">
			<xsl:text>|</xsl:text>
		</xsl:if>	
		<xsl:value-of select="@other_preposition"/>
	</xsl:if>
	<xsl:if test="@preposition_for='yes' or @preposition_to='yes' or @other_preposition!=''">} </xsl:if>
	</xsl:if>
	
<!-- ADJ/NOUN COMPLEMENT -->
	<xsl:if test="name()='adj_complement' or name()='noun_complement'">
		<xsl:if test="@preposition_as='yes' or @preposition_like='yes' or @preposition_other!=''">{</xsl:if>
		<xsl:if test="@preposition_as='yes'">as</xsl:if>
		<xsl:if test="@preposition_like='yes'">
			<xsl:if test="@preposition_as='yes'">
				<xsl:text>|</xsl:text>
			</xsl:if>	
			<xsl:text>like</xsl:text>
		</xsl:if>
		<xsl:if test="@preposition_other!=''">
			<xsl:if test="@preposition_as='yes' or @preposition_like='yes'">
				<xsl:text>|</xsl:text>
			</xsl:if>	
			<xsl:value-of select="@preposition_other"/>
		</xsl:if>
		<xsl:if test="@preposition_as='yes' or @preposition_like='yes' or @preposition_other!=''">} </xsl:if>
	</xsl:if>
	
	<xsl:call-template name="create_noun_phrase"/>
	
<!-- END OF ARGUMENT -->
	<xsl:if test="@optional='yes'">)</xsl:if>
	<xsl:if test="not(@optional='yes')">]</xsl:if>
	
	<xsl:if test="last() != 1 and position() &lt; last()">^</xsl:if>
	
	<xsl:if test="position()=last()">
		<xsl:if test="last() != 1">
			<xsl:if test="@optional='yes'">)</xsl:if>
			<xsl:if test="not(@optional='yes')">]</xsl:if>
<!-- 		] -->
		</xsl:if>
 		<xsl:text> </xsl:text> 
 	</xsl:if>
	
</xsl:template>



<xsl:template match="adverbial_alternation">
	
	
	<xsl:if test="last() != 1 and position()=1">
		<xsl:if test="../@optional='yes'">(</xsl:if>
		<xsl:if test="not(../@optional='yes')">[</xsl:if>
	
<!-- 	[ -->
	</xsl:if>
	
<!-- BEGIN OF ARGUMENT -->
	<xsl:if test="../@optional='yes'">(</xsl:if>
	<xsl:if test="not(../@optional='yes')">[</xsl:if>
	
	
	<xsl:if test="@adverbial_alternation_type='particle'">
		<xsl:text>{</xsl:text>
		<xsl:value-of select="particle"/>
		<xsl:text>}</xsl:text>
	</xsl:if>
	<xsl:if test="@adverbial_alternation_type='prepos_phrase'">
		<xsl:text>{</xsl:text>
		<xsl:value-of select="prepos"/>
		<xsl:text>} </xsl:text>
	</xsl:if>
	<xsl:if test="@adverbial_alternation_type='adverb'">
		<xsl:text>{</xsl:text>
		<xsl:value-of select="adverbs"/>
		<xsl:text>}</xsl:text>
	</xsl:if>
	<xsl:if test="@adverbial_alternation_type='clause'">
		<xsl:if test="subordinator!=''">
			<xsl:text>{</xsl:text>
			<xsl:value-of select="subordinator"/>
			<xsl:text>} </xsl:text>
		</xsl:if>
	</xsl:if>
	<xsl:if test="@adverbial_alternation_type='other_phrase'">
		<xsl:text>{</xsl:text>
		<xsl:value-of select="other_phrase"/>
		<xsl:text>}</xsl:text>
	</xsl:if>
		
	<xsl:call-template name="create_noun_phrase"/>
	
	
<!-- END OF ARGUMENT -->
	<xsl:if test="../@optional='yes'">)</xsl:if>
	<xsl:if test="not(../@optional='yes')">]</xsl:if>
	
	<xsl:if test="last() != 1 and position() &lt; last()">^</xsl:if>
	<xsl:if test="position()=last()">
		<xsl:if test="last() != 1">
			<xsl:if test="../@optional='yes'">)</xsl:if>
			<xsl:if test="not(../@optional='yes')">]</xsl:if>
<!-- 		] -->
		</xsl:if>
 		<xsl:text> </xsl:text> 
 	</xsl:if>
</xsl:template>



<xsl:template match="implicature">
	<xsl:value-of select="."/>
	<xsl:if test="position() &lt; last()">; </xsl:if>
</xsl:template>

<xsl:template name="replace-string">
	<xsl:param name="text"/>
	<xsl:param name="replace"/>
	<xsl:param name="with"/>
	<xsl:choose>
		<xsl:when test="contains($text,$replace)">
			<xsl:value-of select="substring-before($text,$replace)"/>
			<xsl:value-of select="$with"/>
			<xsl:call-template name="replace-string">
			<xsl:with-param name="text" select="substring-after($text,$replace)"/>
			<xsl:with-param name="replace" select="$replace"/>
			<xsl:with-param name="with" select="$with"/>
			</xsl:call-template>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="$text"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>


</xsl:stylesheet>
