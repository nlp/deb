<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="utf-8" method="xml" omit-xml-declaration="yes"/>

  <xsl:param name="freq"/>
<xsl:template match="root">
       <p><xsl:apply-templates/></p>
</xsl:template>

<xsl:template match="h"><b class="red head"><xsl:value-of select="text()"/></b>
<xsl:if test="$freq!=''">
  <xsl:text> (frekvence v korpusu: </xsl:text><xsl:value-of select="$freq"/>)
</xsl:if>
<br/></xsl:template>
<xsl:template match="small"><small class="green"><xsl:value-of select="text()"/></small></xsl:template>
<xsl:template match="ital"><i class="darkred"><xsl:value-of select="text()"/></i></xsl:template>
<xsl:template match="norm"><xsl:value-of select="text()"/></xsl:template>
<xsl:template match="bold"><br/><b class="red"><xsl:value-of select="text()"/></b></xsl:template>
<xsl:template match="it_sm"><small><i class="darkblue"><xsl:value-of select="text()"/></i></small></xsl:template>
<xsl:template match="arial"><b class="green"><xsl:value-of select="text()"/></b></xsl:template>

</xsl:stylesheet>

