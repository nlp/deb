<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="utf-8" method="xml" indent="yes"/>


<xsl:template match="entry">
 <html>
  <head>
    <title>FaNUK Surnames - DAFN: <xsl:value-of select="N"/></title>
    <meta content='text/html; charset=utf-8' http-equiv='Content-Type'/>
    <link href='files/surnames.css' rel='stylesheet' type='text/css'/>
  </head>
  <body class="preview">
    <form method="get" action="/dafn">
      <input type="text" name="id" value="{N}"/>
      <input type="submit" value="show entry"/>
      <input type="hidden" name="action" value="getdoc"/>
      <input type="hidden" name="tr" value="xml"/>
    </form>

  <h1 class="head blue"><xsl:value-of select="N"/></h1>
  <xsl:apply-templates />
</body>
</html>
</xsl:template>


<xsl:template match="s">
  &lt;s&gt;
  <xsl:if test="count(../s)>1">
    <xsl:number count="s" format="1."/><xsl:text> </xsl:text>
  </xsl:if>
      <xsl:apply-templates/>
      &lt;/s&gt;
    <br/>
</xsl:template>

<xsl:template match="fn|G|E">
  <p>&lt;<xsl:value-of select="name()"/>&gt;<xsl:apply-templates/>&lt;/<xsl:value-of select="name()"/>&gt;</p>
</xsl:template>
<xsl:template match="i|b|X">
  &lt;<xsl:value-of select="name()"/>&gt;<xsl:apply-templates/>&lt;/<xsl:value-of select="name()"/>&gt;
</xsl:template>

<xsl:template match="Z|D">
    <xsl:apply-templates/>
</xsl:template>


<xsl:template match="c">
</xsl:template>
<xsl:template match="N">
</xsl:template>


</xsl:stylesheet>

