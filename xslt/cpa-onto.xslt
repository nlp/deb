<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:RDF="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
  xmlns:d="http://nlp.fi.muni.cz/cpa/rdf#">

  <xsl:output encoding="utf-8" type="xml" method="html"/>

  <xsl:template match="RDF:RDF">
    <html>
      <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <style type="text/css">
          .entry_label {}
          .comment {font-size: 80%}
          .comment:before {content: ", "}
        </style>
      </head>
      <body onload="print();">
        <xsl:apply-templates select="RDF:Seq"/>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="RDF:Seq">
    <!--<xsl:value-of select="@about"/>-->
    <xsl:variable name="res0" select="@about"/>
    <xsl:apply-templates select='//RDF:Description[@about=$res0]'/>
    <xsl:if test="count(RDF:li)>0">
    <ul>
      <xsl:for-each select="RDF:li">
        <xsl:sort select="@resource"/>
        <xsl:if test="@resource">
          <li>
            <!--<xsl:value-of select="@resource"/>-->
            <xsl:variable name="res" select="@resource"/>
            <xsl:apply-templates select='//RDF:Description[@about=$res]'/>
          </li>
        </xsl:if>
        <xsl:if test="not(@resource)">
          <li>
            <xsl:apply-templates select="RDF:Seq"/>
          </li>
        </xsl:if>
      </xsl:for-each>
    </ul>
  </xsl:if>
  </xsl:template>

  <xsl:template match="RDF:Description">
    <div class="entry">
      <span class="entry_label">
        <xsl:value-of select="d:term"/>
      </span>
      <xsl:if test="d:comment!=''">
        <span class="comment">
          <xsl:value-of select="d:comment"/>
        </span>
      </xsl:if>
    </div>
  </xsl:template>
</xsl:stylesheet>

