<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template name="get_e4x">
    <xsl:call-template name="get_path">
      <xsl:with-param name="path_first"></xsl:with-param>
      <xsl:with-param name="path_split">.</xsl:with-param>
      <xsl:with-param name="attr_split">@</xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <xsl:template name="get_id">
    <xsl:call-template name="get_path">
      <xsl:with-param name="path_first">_</xsl:with-param>
      <xsl:with-param name="path_split">_</xsl:with-param>
      <xsl:with-param name="attr_split">_</xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <xsl:template name="get_path">
    <xsl:param name="path_first">/</xsl:param>
    <xsl:param name="path_split">/</xsl:param>
    <xsl:param name="attr_split">@</xsl:param>

    <xsl:for-each select="ancestor::*[name()='element']"><xsl:if test="position()=1"><xsl:value-of select="$path_first"/></xsl:if><xsl:if test="position()>1"><xsl:value-of select="$path_split"/></xsl:if><xsl:value-of select="@name"/></xsl:for-each>
    <xsl:if test="name(../../.)='attribute'"><xsl:value-of select="$path_split"/><xsl:value-of select="$attr_split"/><xsl:value-of select="../../@name"/></xsl:if>
    <xsl:if test="name(../.)='attribute'"><xsl:value-of select="$path_split"/><xsl:value-of select="$attr_split"/><xsl:value-of select="../@name"/></xsl:if>
  </xsl:template>
</xsl:stylesheet>
