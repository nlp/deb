<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet [<!ENTITY nbsp "&#160;">]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="utf-8"/>

<xsl:param name="dictionary"/>
<xsl:param name="servername"/>
<xsl:param name="default"/>

<xsl:template match="cdb_lu">
  <body style="background-color: white;" id="body">
    <style type="text/css">
      a {
      cursor: pointer;
      text-decoration: underline;
      }
    </style>


    <span class="BLUE">C_LU_ID: </span>
    <span class="RED"><xsl:value-of select="@c_lu_id"/></span>
    <xsl:text> </xsl:text>
    <span class="BLUE">C_SEQ_NR: </span>
    <span class="RED"><xsl:value-of select="@c_seq_nr"/></span>

    <br/>
    <span class="BLUE">Examples: </span><br/>
    <xsl:apply-templates select="examples/example"/>


  </body>
</xsl:template>

<xsl:template match="example">
  <!--short-->
  <span class="DARK_RED">Example: </span><span class="BLUE"><xsl:value-of select="@r_ex_id"/></span><xsl:text> </xsl:text><span class="BLACK"><a id="a-ex-longex{@r_ex_id}" onclick="switch_display('ex-longex{@r_ex_id}');" style="cursor:pointer">[+]</a></span><br/>
  <span class="DARK_RED">canonical form: </span> <span class="GREEN"><xsl:value-of select="form_example/canonicalform"/></span><br/>
  <xsl:if test="syntax_example/sy-type!=''">
    <span class="DARK_RED">type: </span> <span class="GREEN"><xsl:value-of select="syntax_example/sy-type"/></span>
    <xsl:text> </xsl:text>
  </xsl:if>
  <xsl:if test="syntax_example/sy-subtype!=''">
    <span class="DARK_RED">subtype: </span> <span class="GREEN"><xsl:value-of select="syntax_example/sy-subtype"/></span>
  </xsl:if>
  <br/>
  <!--long-->
  <div id="ex-longex{@r_ex_id}" style="display:none;">
  <span class="BLUE">Form example: </span><br/>
  <span class="DARK_RED">canonical form: </span> <span class="GREEN"><xsl:value-of select="form_example/canonicalform"/></span><br/>
  <xsl:if test="form_example/textualform!=''">
    <span class="DARK_RED">textual form: </span> <span class="GREEN"><xsl:value-of select="form_example/textualform"/></span><br/>
  </xsl:if>
  <xsl:if test="form_example/category!=''">
    <span class="DARK_RED">category: </span> <span class="GREEN"><xsl:value-of select="form_example/category"/></span>
    <xsl:text> </xsl:text>
  </xsl:if>
  <xsl:if test="form_example/text-category!=''">
    <span class="DARK_RED">text category: </span> <span class="GREEN"><xsl:value-of select="form_example/text-category"/></span>
  </xsl:if>
  
  <br/>
  
  <xsl:if test="syntax_example">
    <span class="BLUE">Syntax example: </span><br/>
    <xsl:if test="syntax_example/sy-type!=''">
      <span class="DARK_RED">type: </span> <span class="GREEN"><xsl:value-of select="syntax_example/sy-type"/></span>
      <xsl:text> </xsl:text>
    </xsl:if>
    <xsl:if test="syntax_example/sy-subtype!=''">
      <span class="DARK_RED">subtype: </span> <span class="GREEN"><xsl:value-of select="syntax_example/sy-subtype"/></span>
    </xsl:if>
    <xsl:for-each select="syntax_example/sy-combipair">
      <span class="DARK_RED">combiword: </span> <span class="GREEN"><xsl:value-of select="syntax_example/sy-combipair/sy-combiword"/></span>
      <xsl:text> </xsl:text>
      <span class="DARK_RED">combicat: </span> <span class="GREEN"><xsl:value-of select="syntax_example/sy-combipair/sy-combicat"/></span>
      <xsl:if test="position()!=last()"><br/></xsl:if>
    </xsl:for-each>
    <br/>
  </xsl:if>

  <xsl:if test="semantics_example">
    <span class="BLUE">Semantics example: </span><br/>
    <xsl:if test="semantics_example/sem-meaningdescription!=''">
      <span class="DARK_RED">meaning description: </span> <span class="GREEN"><xsl:value-of select="semantics_example/sem-meaningdescription"/></span><br/>
    </xsl:if>
    <xsl:if test="semantics_example/sem-lc-collocator!=''">
      <span class="DARK_RED">lc&nbsp;collocator:</span>&nbsp;<span class="GREEN"><xsl:value-of select="semantics_example/sem-lc-collocator"/></span>
      <xsl:text> </xsl:text>
    </xsl:if>
    <xsl:if test="semantics_example/sem-gc-compl!=''">
      <span class="DARK_RED">gc&nbsp;compl:</span>&nbsp;<span class="GREEN"><xsl:value-of select="semantics_example/sem-gc-compl"/></span>
      <xsl:text> </xsl:text>
    </xsl:if>
    <xsl:if test="semantics_example/sem-spec-collocator!=''">
      <span class="DARK_RED">spec&nbsp;collocator:</span>&nbsp;<span class="GREEN"><xsl:value-of select="semantics_example/sem-spec-collocator"/></span>
      <xsl:text> </xsl:text>
    </xsl:if>
    <xsl:if test="semantics_example/sem-subtype-argument!=''">
      <span class="DARK_RED">subtype-argument:</span>&nbsp;<span class="GREEN"><xsl:value-of select="semantics_example/sem-subtype-argument"/></span>
      <xsl:text> </xsl:text>
    </xsl:if>
    <xsl:if test="semantics_example/sem-gc-gramword!=''">
      <span class="DARK_RED">gc&nbsp;gramword:</span>&nbsp;<span class="GREEN"><xsl:value-of select="semantics_example/sem-gc-gramword"/></span>
      <xsl:text> </xsl:text>
    </xsl:if>
    <br/>
  </xsl:if>
  
  <xsl:if test="pragmatics">
    <span class="BLUE">Pragmatics: </span><br/> 
    <xsl:if test="pragmatics/prag-origin!=''">
      <span class="DARK_RED">origin:</span>&nbsp;<span class="GREEN"><xsl:value-of select="pragmatics/prag-origin"/></span>
      <xsl:text> </xsl:text>
    </xsl:if>
    <xsl:if test="pragmatics/prag-style!=''">
      <span class="DARK_RED">style:</span>&nbsp;<span class="GREEN"><xsl:value-of select="pragmatics/prag-style"/></span>
      <xsl:text> </xsl:text>
    </xsl:if>
    <xsl:if test="pragmatics/prag-connotation!=''">
      <span class="DARK_RED">connotation:</span>&nbsp;<span class="GREEN"><xsl:value-of select="pragmatics/prag-connotation"/></span>
      <xsl:text> </xsl:text>
    </xsl:if>
    <xsl:if test="pragmatics/prag-socGroup!=''">
      <span class="DARK_RED">socGroup:</span>&nbsp;<span class="GREEN"><xsl:value-of select="pragmatics/prag-socGroup"/></span>
      <xsl:text> </xsl:text>
    </xsl:if>
    <xsl:if test="pragmatics/prag-domain/@subjectfield!=''">
      <span class="DARK_RED">domain:</span>&nbsp;<span class="GREEN"><xsl:value-of select="pragmatics/prag-domain/@subjectfield"/> (general: <xsl:value-of select="pragmatics/prag-domain/@general"/>)</span>
      <xsl:text> </xsl:text>
    </xsl:if>
    <xsl:if test="pragmatics/prag-chronology!=''">
      <span class="DARK_RED">chronology:</span>&nbsp;<span class="GREEN"><xsl:value-of select="pragmatics/prag-chronology"/></span>
      <xsl:text> </xsl:text>
    </xsl:if>
    <xsl:if test="pragmatics/prag-subj-gen!=''">
      <span class="DARK_RED">subj-gen:</span>&nbsp;<span class="GREEN"><xsl:value-of select="pragmatics/prag-subj-gen"/></span>
      <xsl:text> </xsl:text>
    </xsl:if>
    <xsl:if test="pragmatics/prag-geography!=''">
      <span class="DARK_RED">geography:</span>&nbsp;<span class="GREEN"><xsl:value-of select="pragmatics/prag-geography"/></span>
      <xsl:text> </xsl:text>
    </xsl:if>
    <xsl:if test="pragmatics/prag-frequency!=''">
      <span class="DARK_RED">frequency:</span>&nbsp;<span class="GREEN"><xsl:value-of select="pragmatics/prag-frequency"/></span>
      <xsl:text> </xsl:text>
    </xsl:if>
  </xsl:if>
  </div>
  <hr/>
</xsl:template>

</xsl:stylesheet>

