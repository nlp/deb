<html><head><meta content='text/html; charset=utf-8' http-equiv='Content-Type'/><title>slovník 2: {{entry.hw.$}} (DEBWrite)</title></head><body>
<h1>{{entry.hw.$}}</h1>
<span class="hw">headword: {{entry.hw.$}}</span><br/>
<span class="pos">PoS: {{entry.pos.$}}</span><br/>
<span class="gram">grammar: {{entry.gram.$}}</span><br/>

{{#each entry.mean}}
<div class="mean" style="border: 1px solid black; background-color:undefined">meaning:
<span class="nr">meaning nr: {{nr.$}}</span><br/>
<span class="def">explanation: {{def.$}}</span><br/>
<span class="ex">usage: {{ex.$}}</span><br/></div><br/>{{/each }}

<span class="com">comment: {{entry.com.$}}</span><br/>
</body></html>
