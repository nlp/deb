<?xml version='1.0' encoding='utf-8'?>
<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'><xsl:template match='/entry'><html><head><meta content='text/html[-semicolon-] charset=utf-8' http-equiv='Content-Type'/><title>Diagnozy: <xsl:value-of select="word"/> (DEBWrite)</title><style type="text/css">.word{color: blue}
.type_container {border: 1px solid #000}
.type_container {background-color:#eee}
</style>
</head>
<body><h1><xsl:value-of select="word"/></h1>
<xsl:apply-templates/></body></html></xsl:template>
<xsl:template name="file"><xsl:param name="file_element"/><br/><xsl:if test="starts-with($file_element/@mime, 'image')"><img src="/files/hales_diagnozy/{.}" style="max-width:400px"/></xsl:if><xsl:if test="starts-with($file_element/@mime, 'audio')"><audio src="/files/hales_diagnozy/{.}" controls="true"/></xsl:if><xsl:if test="starts-with($file_element/@mime, 'video')"><video src="/files/hales_diagnozy/{.}" controls="true" style="max-width:400px"/></xsl:if><br/></xsl:template>

<xsl:template match="word"><span class="word type_text">word: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="tag"><div class="tag type_container">POS tag: <xsl:apply-templates/></div><br/></xsl:template>
<xsl:template match="tag"><span class="tag type_select">orig.POS tag: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="lemma"><span class="lemma type_text">lemma: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="norm"><span class="norm type_text">normalized (full) form: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="morpho"><span class="morpho type_select">extended morphological tag: <xsl:apply-templates/></span><br/></xsl:template>
</xsl:stylesheet>
