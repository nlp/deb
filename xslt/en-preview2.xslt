<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output encoding="utf-8" method="xml"
    doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
    doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
    />

  <xsl:param name="perm"/>
  <xsl:param name="dictcode"/>
  <xsl:param name="public">false</xsl:param>
  <xsl:variable name="skupina_test">
    <!--ma uzivatel stejnou skupinu jako heslo? nebo bez omezeni-->
    <xsl:if test="contains(/entry/@user_skupina,'all') or contains($perm, 'admin')">true</xsl:if>
    <xsl:if test="$public!='true' and not(contains(/entry/@user_skupina,'all')) and not(contains($perm, 'admin'))"><xsl:value-of select="contains(/entry/@user_skupina, /entry/lemma/pracskupina)"/></xsl:if>
    <xsl:if test="$public='true'">false</xsl:if>
  </xsl:variable>

  <xsl:template match="entry">
    <html>
      <head>
        <title>EN: <xsl:value-of select="lemma/title"/></title>

        <link rel="stylesheet" type="text/css" media="screen" href="/editor/czjprev4.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="/editor/css/czj-common.css" ></link>
        <link rel="stylesheet" type="text/css" media="screen" href="/editor/css/cj-preview.css" ></link>
        <link href='/editor/css/czj-preview7-min.css' rel='stylesheet' type='text/css' media='screen'/>

        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Titillium+Web:400,200,300,600,700,900&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="/media/jwplayer/jwplayer.js" ></script>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    <script src="//releases.flowplayer.org/5.4.3/flowplayer.min.js"></script>
    <link rel="stylesheet" href="//releases.flowplayer.org/5.4.3/skin/minimalist.css"/>

    <script type="text/javascript" src="/media/fancybox/jquery.fancybox.pack.js"></script>
    <link rel="stylesheet" href="/media/fancybox/jquery.fancybox.css" type="text/css" media="screen" />

    <script type="text/javascript" src="/editor/imagesloaded.pkgd.min.js">//</script>
    <script type="text/javascript" src="/editor/czjmain.js">//</script>
    <script><![CDATA[
      function change_view(eid) {
        if (document.getElementById(eid).style.display=='none') {
          if (eid == 'lemma_parts_box') { document.getElementById('lemma_parts_info').innerHTML = 'skrýt podrobnosti'}
          document.getElementById(eid).style.display='block';
        } else {
          if (eid == 'lemma_parts_box') { document.getElementById('lemma_parts_info').innerHTML = 'zobrazit podrobnosti'}
          document.getElementById(eid).style.display='none';
        }
      }

      $(document).ready(function() {
      $(".flowmouse").bind("mouseenter mouseleave", function(e) {
        flowplayer(this)[/ee/.test(e.type) ? "resume":"pause"]();
        });
        
      });
        $("a.fancybox").fancybox({
        'autoScale'      : false,
        'transitionIn'   : 'elastic',
        'transitionOut'  : 'elastic',
        'titlePosition'  : 'inside',
        'hideOnContentClick' : true,
        'speedIn'        : 100,
        'speedOut'     : 100,
        'changeSpeed'    : 100,
        'centerOnScroll' : false,
        padding: 0,
        closeClick: true
        });

      function open_iframe(id, dict) {
        var iframe = document.getElementById('colloc'+id);
        if (iframe.style.display=='none') {
          iframe.style.display='inline';
        } else {
          iframe.style.display='none';
        }
        if (iframe.src == 'about:blank') {
          iframe.src = '/'+dict+'?action=getdoc&amp;id='+id+'&amp;tr=inline';
        }
      }
      function iframe_loaded(id) {
        var iframe = document.getElementById('colloc'+id);
        if (iframe.src != 'about:blank') {
          //alert(iframe.contentWindow.document.body.scrollHeight);
          iframe.height = iframe.contentWindow.document.body.scrollHeight + 'px;'
        }
      }

      ]]></script>
      </head>
      <body>

        <div id="outer">      <!-- jen preview ZACATEK -->

          <div id="side" style="position: fixed">
            <strong><xsl:value-of select="lemma/title"/></strong> 
            <br/> 
            
<form method="get" action="/cs">
    <input type="hidden" name="action" value="search"/>
    <input type="hidden" name="type" value="text"/>
     <!--<input type="hidden" name="new" value="true"/>-->
    <input type="text" name="search" size="10" value=""/>
    <input type="submit" value="hledat"/>
    </form>
         <xsl:if test="$perm!='ro'"><a href="/editorcs/?id={@id}">Editovat</a><br/></xsl:if>
          </div>
          <div id="inner">   <!-- jen preview KONEC -->
            <div class="cj-mainlemma">
           <xsl:if test="(lemma/lemma_type='collocation' or lemma/lemma_type='prislovi') and ($perm!='ro' or $skupina_test='true' or collocations/@status!='hidden')">
             <xsl:attribute name="style">background-color: lightgrey;</xsl:attribute>
            </xsl:if>
           <xsl:if test="$perm!='ro' or $skupina_test='true'">
             <span class="set-status">ID: <xsl:value-of select="@id"/></span> 
                <span class="publish"> publikování: 
                <xsl:choose>
                   <xsl:when test="/entry/lemma/completeness='0'">automaticky</xsl:when>
                   <xsl:when test="/entry/lemma/completeness='1'">skrýt vše</xsl:when>                   
                   <xsl:when test="/entry/lemma/completeness='2'">vyplněné části</xsl:when>
                   <xsl:when test="/entry/lemma/completeness='100'">jen schválené části</xsl:when>
                </xsl:choose></span>                 
	    </xsl:if>
             <span style="float: right">
                      <xsl:if test="$perm!='ro'"><a href="/editoren/?id={@id}">editovat<img src="/editor/img/edit.png" /></a></xsl:if>
                      <xsl:choose>
                        <xsl:when test="lemma/@auto_complete='1' and lemma/completeness!='1'"><a style="color:blue;cursor:pointer" onclick="window.location = 'http://www.dictio.info'+window.location.pathname+window.location.search"><img src="/editor/publikovano.png" border="0" title="přepnout na veřejné zobrazení"/></a></xsl:when>
                        <xsl:otherwise><img src="/editor/nepublikovano.png" title="nepublikované heslo"/> </xsl:otherwise>
                      </xsl:choose>
             </span> <br/>
           
            
           <xsl:if test="(lemma/lemma_type='collocation' or lemma/lemma_type='prislovi') and ($perm!='ro' or $skupina_test='true' or collocations/@status!='hidden')">
               <span id="lemma_parts"><!--<strong>Slovní spojení. </strong>--><a style="cursor:pointer;color:blue"  onclick="change_view('lemma_parts_box')" id="lemma_parts_info">Zobrazit odkazy na složky.</a><br/>
                 <span id="lemma_parts_box" style="display:none">
                   <xsl:apply-templates select="collocations/colloc"/>
                 </span>
               </span>
             </xsl:if>
             <h1 style="display:inline"><xsl:value-of select="lemma/title"/><xsl:if test="/entry/lemma/title_var!=''">, <xsl:value-of select="lemma/title_var"/></xsl:if></h1><!-- implementovat do ilnine a EN -->
             <xsl:if test="lemma/pron!=''"><span style="margin-left: 30px;">[<xsl:value-of select="lemma/pron"/>]</span></xsl:if>
              <!--schvalování-->
           <xsl:if test="$perm!='ro' or $skupina_test='true'">
              <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_lemmacj')">
                <input type="button" onclick="publish_all(this)" value="schválit hromadně"/>
               <br/>
               <span class="set-status">
                 typ hesla: 
                 <xsl:choose>
                   <xsl:when test="/entry/lemma/lemma_type='single'">jednoduché</xsl:when>
                   <xsl:when test="/entry/lemma/lemma_type='predpona'">předpona</xsl:when>
                   <xsl:when test="/entry/lemma/lemma_type='collocation'">ustálené slovní spojení/frazém</xsl:when>
                   <xsl:when test="/entry/lemma/lemma_type='prislovi'">přísloví</xsl:when>
                 </xsl:choose>
                 <xsl:call-template name="status_publish">
                   <xsl:with-param name="status" select="/entry/collocations/@status"/>
                   <xsl:with-param name="type">entry</xsl:with-param>
                 </xsl:call-template>
               </span>
             </xsl:if>
           </xsl:if><!--konec schvalovani-->
             
           </div> <!-- cj-mainlemma -->
           <xsl:if test="html and not(meanings/meaning)">
             <div>
               <span class="divtitle">SSČ</span>
               <div class="cj-grammar">
                 <xsl:apply-templates select="html"/>
               </div>
             </div>
           </xsl:if>
           <xsl:if test="lemma/grammar_note/text()!='' or lemma/style_note/text()!='' or lemma/grammar_note/variant or lemma/style_note/variant or lemma/grammar_note/@slovni_druh!='' or lemma/grammar_note/hyphen!='' or lemma/style_note/@kategorie!='' or lemma/style_note/@stylpriznak!=''">
             <div class="cj-gram-popis">
              <!-- <div class="cj-lemma-h1">Formální popis</div> -->
               <div class="cj-grammar">
               <!--schvalování-->
                 <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_cjlingvist')">
                   <span class="set-status">
                     gramatický popis: 
                     <xsl:call-template name="status_publish">
                       <xsl:with-param name="status" select="/entry/lemma/grammar_note[1]/@status"/>
                       <xsl:with-param name="type">gram</xsl:with-param>
                     </xsl:call-template>
                   </span>
                   
                 </xsl:if><!--konec schvalování-->
    <div class="author-info" style="display:none">
      <xsl:if test="lemma/grammar_note/@source!=''"><span class="author-source">zdroj: <xsl:value-of select="lemma/grammar_note/@source"/></span>; </xsl:if>
      <xsl:if test="lemma/grammar_note/@author!=''"><span class="author-author">autor: <xsl:value-of select="lemma/grammar_note/@author"/></span>; </xsl:if>
      <xsl:if test="lemma/grammar_note/@copyright!=''"><span class="author-copyright">copyright: <xsl:value-of select="lemma/grammar_note/@copyright"/></span></xsl:if>
    </div>

             <br />
             <xsl:if test="lemma/puvod!=''">
               <div>
               <b>Původ slova: </b>
               <xsl:choose>
                 <xsl:when test="lemma/puvod='a'">angličtina</xsl:when>
                 <xsl:when test="lemma/puvod='afr'">africké jazyky</xsl:when>
                 <xsl:when test="lemma/puvod='alb'">albánština</xsl:when>
                 <xsl:when test="lemma/puvod='am'">americká angličtina</xsl:when>
                 <xsl:when test="lemma/puvod='ar'">arabština</xsl:when>
                 <xsl:when test="lemma/puvod='aram'">aramejština</xsl:when>
                 <xsl:when test="lemma/puvod='b'">bulharština</xsl:when>
                 <xsl:when test="lemma/puvod='bantu'">bantuské jazyky</xsl:when>
                 <xsl:when test="lemma/puvod='bask'">baskičtina</xsl:when>
                 <xsl:when test="lemma/puvod='br'">běloruština</xsl:when>
                 <xsl:when test="lemma/puvod='cik'">cikánština</xsl:when>
                 <xsl:when test="lemma/puvod='čes'">čeština</xsl:when>
                 <xsl:when test="lemma/puvod='čín'">čínština</xsl:when>
                 <xsl:when test="lemma/puvod='dán'">dánština</xsl:when>
                 <xsl:when test="lemma/puvod='eg'">egyptština</xsl:when>
                 <xsl:when test="lemma/puvod='est'">estonština</xsl:when>
                 <xsl:when test="lemma/puvod='f'">francouzština</xsl:when>
                 <xsl:when test="lemma/puvod='fin'">finština</xsl:when>
                 <xsl:when test="lemma/puvod='g'">germánské jazyky</xsl:when>
                 <xsl:when test="lemma/puvod='hebr'">hebrejština</xsl:when>
                 <xsl:when test="lemma/puvod='hind'">hindština</xsl:when>
                 <xsl:when test="lemma/puvod='i'">italština</xsl:when>
                 <xsl:when test="lemma/puvod='ind'">indické jazyky</xsl:when>
                 <xsl:when test="lemma/puvod='indián'">indiánské jazyky</xsl:when>
                 <xsl:when test="lemma/puvod='indonés'">indonéština</xsl:when>
                 <xsl:when test="lemma/puvod='ir'">irština</xsl:when>
                 <xsl:when test="lemma/puvod='island'">islandština</xsl:when>
                 <xsl:when test="lemma/puvod='j'">japonština</xsl:when>
                 <xsl:when test="lemma/puvod='jakut'">jakutština</xsl:when>
                 <xsl:when test="lemma/puvod='jslov'">jihoslovanské jazyky</xsl:when>
                 <xsl:when test="lemma/puvod='kelt'">keltské jazyky</xsl:when>
                 <xsl:when test="lemma/puvod='korej'">korejština</xsl:when>
                 <xsl:when test="lemma/puvod='l'">latina</xsl:when>
                 <xsl:when test="lemma/puvod='lfran'">latina přes francouzštinu</xsl:when>
                 <xsl:when test="lemma/puvod='lapon'">laponština</xsl:when>
                 <xsl:when test="lemma/puvod='lit'">litevština</xsl:when>
                 <xsl:when test="lemma/puvod='lot'">lotyština</xsl:when>
                 <xsl:when test="lemma/puvod='luž'">lužická srbština</xsl:when>
                 <xsl:when test="lemma/puvod='maď'">maďarština</xsl:when>
                 <xsl:when test="lemma/puvod='maked'">makedonština</xsl:when>
                 <xsl:when test="lemma/puvod='mal'">malajština</xsl:when>
                 <xsl:when test="lemma/puvod='mong'">mongolština</xsl:when>
                 <xsl:when test="lemma/puvod='n'">němčina</xsl:when>
                 <xsl:when test="lemma/puvod='niz'">nizozemština</xsl:when>
                 <xsl:when test="lemma/puvod='NJ'">jméno národa</xsl:when>
                 <xsl:when test="lemma/puvod='nor'">norština</xsl:when>
                 <xsl:when test="lemma/puvod='OJ'">osobní jméno</xsl:when>
                 <xsl:when test="lemma/puvod='or'">orientální jazyky</xsl:when>
                 <xsl:when test="lemma/puvod='p'">polština</xsl:when>
                 <xsl:when test="lemma/puvod='per'">perština</xsl:when>
                 <xsl:when test="lemma/puvod='polynés'">polynéské jazyky</xsl:when>
                 <xsl:when test="lemma/puvod='port'">portugalština</xsl:when>
                 <xsl:when test="lemma/puvod='provens'">provensálština</xsl:when>
                 <xsl:when test="lemma/puvod='r'">ruština</xsl:when>
                 <xsl:when test="lemma/puvod='rom'">románské jazyky</xsl:when>
                 <xsl:when test="lemma/puvod='rum'">rumunština</xsl:when>
                 <xsl:when test="lemma/puvod='ř'">řečtina</xsl:when>
                 <xsl:when test="lemma/puvod='semit'">semitština</xsl:when>
                 <xsl:when test="lemma/puvod='sch'">srbochorvatština</xsl:when>
                 <xsl:when test="lemma/puvod='skan'">skandinávské jazyky</xsl:when>
                 <xsl:when test="lemma/puvod='sl'">slovenština</xsl:when>
                 <xsl:when test="lemma/puvod='sln'">slovinština</xsl:when>
                 <xsl:when test="lemma/puvod='srb'">srbština</xsl:when>
                 <xsl:when test="lemma/puvod='střl'">středověká latina</xsl:when>
                 <xsl:when test="lemma/puvod='svahil'">svahilština</xsl:when>
                 <xsl:when test="lemma/puvod='š'">španělština</xsl:when>
                 <xsl:when test="lemma/puvod='šv'">švédština</xsl:when>
                 <xsl:when test="lemma/puvod='t'">turečtina</xsl:when>
                 <xsl:when test="lemma/puvod='tat'">tatarština</xsl:when>
                 <xsl:when test="lemma/puvod='tib'">tibetština</xsl:when>
                 <xsl:when test="lemma/puvod='ttat'">turkotatarské jazyky</xsl:when>
                 <xsl:when test="lemma/puvod='tung'">tunguzština</xsl:when>
                 <xsl:when test="lemma/puvod='u'">ukrajinština</xsl:when>
                 <xsl:when test="lemma/puvod='VJ'">vlastní jméno</xsl:when>
                 <xsl:when test="lemma/puvod='vslov'">východoslovanské jazyky</xsl:when>
                 <xsl:when test="lemma/puvod='ZJ'">zeměpisné jméno</xsl:when>
                 <xsl:when test="lemma/puvod='zslov'">západoslovanské jazyky</xsl:when>
                 <xsl:when test="lemma/puvod='žid'">židovský žargon</xsl:when>
                 <xsl:otherwise><xsl:value-of select="lemma/puvod"/></xsl:otherwise>
               </xsl:choose>  </div>
             </xsl:if>
                 
                 <xsl:if test="$perm!='ro' or $skupina_test='true' or lemma/grammar_note[1]/@status!='hidden'">
                   
                   <xsl:for-each select="lemma/grammar_note">
                     <xsl:if test="@slovni_druh!=''">
                      <xsl:if test="@slovni_druh='subst' or @slovni_druh='adj' or @slovni_druh='pron' or @slovni_druh='num' or @slovni_druh='verb' or @slovni_druh='prep' or @slovni_druh='kon' or @slovni_druh='par' or @slovni_druh='int'">
                     <b>Lexikální kategorie: </b>  </xsl:if>
                       <xsl:choose>
                         <xsl:when test="@slovni_druh='subst'">podstatné jméno</xsl:when>
                         <xsl:when test="@slovni_druh='adj'">přídavné jméno</xsl:when>
                         <xsl:when test="@slovni_druh='pron'">zájmeno</xsl:when>
                         <xsl:when test="@slovni_druh='num'">číslovka</xsl:when>
                         <xsl:when test="@slovni_druh='verb'">sloveso</xsl:when>
                         <xsl:when test="@slovni_druh='adv'">příslovce</xsl:when>
                         <xsl:when test="@slovni_druh='prep'">předložka</xsl:when>
                         <xsl:when test="@slovni_druh='ust'">ustálený předložkový výraz</xsl:when>
                         <xsl:when test="@slovni_druh='kon'">spojka</xsl:when>
                         <xsl:when test="@slovni_druh='par'">částice</xsl:when>
                         <xsl:when test="@slovni_druh='int'">citoslovce</xsl:when>                                                  
                         <xsl:when test="@slovni_druh='sprezka'">příslovečná spřežka</xsl:when>                        
                         <xsl:when test="@slovni_druh='predpona'">předpona</xsl:when>
                         <xsl:when test="@slovni_druh='zkratka'">zkratka</xsl:when>
                         <!--
                         <xsl:when test="@slovni_druh='ustalene'">ustálené slovní spojení</xsl:when>
                         <xsl:when test="@slovni_druh='prirovnani'">přirovnání</xsl:when>
                         <xsl:when test="@slovni_druh='prislovi'">přísloví</xsl:when>
                         <xsl:when test="@slovni_druh='porekadlo'">pořekadlo</xsl:when>
                         <xsl:when test="@slovni_druh='frazem'">frazém</xsl:when>-->
                       </xsl:choose>
                       <xsl:if test="@skupina!=''">
                         <xsl:if test="@slovni_druh!='ustalene'">
                            <xsl:text>, </xsl:text> 
                         </xsl:if>
                         <xsl:call-template name="split_skupina"><xsl:with-param name="text" select="@skupina"/></xsl:call-template>
                       </xsl:if>
                       <xsl:if test="@skupina2!=''">
                         <xsl:text>, </xsl:text>
                         <xsl:call-template name="split_skupina2"><xsl:with-param name="text" select="@skupina2"/></xsl:call-template>
                       </xsl:if>
                     </xsl:if>
                     <xsl:if test="position()!=last()"><br/></xsl:if>
                   </xsl:for-each>
                   <xsl:if test="lemma/grammar_note/@flexe_neskl='true'">, nesklonné</xsl:if>
                     
                     <xsl:if test="lemma/grammar_note/variant">
                       <br/><b>
                        <xsl:if test="lemma/grammar_note/@slovni_druh!='ustalene'">Gramatická </xsl:if>
                         varianta: </b>
                       <xsl:apply-templates select="lemma/grammar_note/variant"/>
                     </xsl:if>
                     <xsl:if test="normalize-space(lemma/grammar_note/text())!=''"><br/><xsl:apply-templates select="lemma/grammar_note" mode="ingram"/></xsl:if>
                   <xsl:if test="lemma/grammar_note/hyphen"><b>Dělení na slabiky: </b><xsl:value-of select="lemma/grammar_note/hyphen"/><br/></xsl:if>
                   
                    <!--tabulka-->
                   <xsl:if test="lemma/gram/form">
                     <xsl:choose>
                       <xsl:when test="lemma/grammar_note/@slovni_druh='subst' or lemma/grammar_note/@slovni_druh=''">
                         <table id="cj-gram-table">
                           <tr><td></td><td>jednotné číslo</td><td>množné číslo</td></tr>
                           <tr><td>1. pád</td><td><xsl:value-of select="lemma/gram/form[@tag='nSc1']"/></td><td><xsl:value-of select="lemma/gram/form[@tag='nPc1']"/></td></tr>
                           <tr><td>2. pád</td><td><xsl:value-of select="lemma/gram/form[@tag='nSc2']"/></td><td><xsl:value-of select="lemma/gram/form[@tag='nPc2']"/></td></tr>
                           <tr><td>3. pád</td><td><xsl:value-of select="lemma/gram/form[@tag='nSc3']"/></td><td><xsl:value-of select="lemma/gram/form[@tag='nPc3']"/></td></tr>
                           <tr><td>4. pád</td><td><xsl:value-of select="lemma/gram/form[@tag='nSc4']"/></td><td><xsl:value-of select="lemma/gram/form[@tag='nPc4']"/></td></tr>
                           <tr><td>5. pád</td><td><xsl:value-of select="lemma/gram/form[@tag='nSc5']"/></td><td><xsl:value-of select="lemma/gram/form[@tag='nPc5']"/></td></tr>
                           <tr><td>6. pád</td><td><xsl:value-of select="lemma/gram/form[@tag='nSc6']"/></td><td><xsl:value-of select="lemma/gram/form[@tag='nPc6']"/></td></tr>
                           <tr><td>7. pád</td><td><xsl:value-of select="lemma/gram/form[@tag='nSc7']"/></td><td><xsl:value-of select="lemma/gram/form[@tag='nPc7']"/></td></tr>
                         </table>
                       </xsl:when>
                       <xsl:when test="lemma/grammar_note/@slovni_druh='adj'">
                         <table id="cj-gram-table">
                           <tr><td>2. stupeň</td><td><xsl:value-of select="lemma/gram/form[@tag='d2']"/></td></tr>
                           <tr><td>3. stupeň</td><td><xsl:value-of select="lemma/gram/form[@tag='d3']"/></td></tr>
                         </table>
                       </xsl:when>
                       <xsl:when test="lemma/grammar_note/@slovni_druh='adv'">
                         <table id="cj-gram-table">
                           <tr><td>2. stupeň</td><td><xsl:value-of select="lemma/gram/form[@tag='d2']"/></td></tr>
                           <tr><td>3. stupeň</td><td><xsl:value-of select="lemma/gram/form[@tag='d3']"/></td></tr>
                         </table>
                       </xsl:when>
                       <xsl:when test="lemma/grammar_note/@slovni_druh='verb'">
                         <table id="cj-gram-table">
                           <tr><td></td><td>jednotné číslo</td><td>množné číslo</td></tr>
                           <tr><td>1. osoba</td><td><xsl:value-of select="lemma/gram/form[@tag='mIp1nS']"/></td><td><xsl:value-of select="lemma/gram/form[@tag='mIp1nP']"/></td></tr>
                           <tr><td>2. osoba</td><td><xsl:value-of select="lemma/gram/form[@tag='mIp2nS']"/></td><td><xsl:value-of select="lemma/gram/form[@tag='mIp2nP']"/></td></tr>
                           <tr><td>3. osoba</td><td><xsl:value-of select="lemma/gram/form[@tag='mIp3nS']"/></td><td><xsl:value-of select="lemma/gram/form[@tag='mIp3nP']"/></td></tr>
                           <tr><td>rozkazovací způsob</td><td><xsl:value-of select="lemma/gram/form[@tag='mRp2nS']"/></td><td><xsl:value-of select="lemma/gram/form[@tag='mRp2nP']"/></td></tr>
                           <xsl:if test="lemma/gram/form[@tag='mAgMnS']"><tr><td>příčestí činné</td><td colspan="2"><xsl:value-of select="lemma/gram/form[@tag='mAgMnS']"/></td></tr></xsl:if>
                           <xsl:if test="lemma/gram/form[@tag='mNgMnS']"><tr><td>příčestí trpné</td><td colspan="2"><xsl:value-of select="lemma/gram/form[@tag='mNgMnS']"/></td></tr></xsl:if>
                           <xsl:if test="lemma/gram/form[@tag='mSgMnS']">
                             <tr><td>přechodník, m.	</td><td><xsl:value-of select="lemma/gram/form[@tag='mSgMnS']"/></td><td rowspan="2"><xsl:value-of select="lemma/gram/form[@tag='mSgMnP']"/></td></tr>
                             <tr><td>přechodník, ž. + s.</td><td><xsl:value-of select="lemma/gram/form[@tag='mSgFnS']"/></td></tr>
                           </xsl:if>
                           <xsl:if test="lemma/gram/form[@tag='mDgMnS']">
                             <tr><td>přechodník, m.	</td><td><xsl:value-of select="lemma/gram/form[@tag='mDgMnS']"/></td><td rowspan="2"><xsl:value-of select="lemma/gram/form[@tag='mDgMnP']"/></td></tr>
                             <tr><td>přechodník, ž. + s.</td><td><xsl:value-of select="lemma/gram/form[@tag='mDgFnS']"/></td></tr>
                           </xsl:if>
                           <tr><td>verbální substantivum</td><td colspan="2"><xsl:value-of select="lemma/gram/form[@tag='gNnSc1']"/></td></tr>
                         </table>
                       </xsl:when>
                       <xsl:when test="lemma/grammar_note/@slovni_druh='num' or lemma/grammar_note/@slovni_druh='pron'">
                         <table id="cj-gram-table">
                           <xsl:if test="lemma/gram/form[@tag='cP1']"><tr><td></td><td>jednotné číslo</td><td>množné číslo</td></tr></xsl:if>
                           <tr><td>1. pád</td><td><xsl:value-of select="lemma/gram/form[@tag='c1']"/></td><xsl:if test="lemma/gram/form[@tag='cP1']"><td><xsl:value-of select="lemma/gram/form[@tag='cP1']"/></td></xsl:if></tr>
                           <tr><td>2. pád</td><td><xsl:value-of select="lemma/gram/form[@tag='c2']"/></td><xsl:if test="lemma/gram/form[@tag='cP1']"><td><xsl:value-of select="lemma/gram/form[@tag='cP2']"/></td></xsl:if></tr>
                           <tr><td>3. pád</td><td><xsl:value-of select="lemma/gram/form[@tag='c3']"/></td><xsl:if test="lemma/gram/form[@tag='cP1']"><td><xsl:value-of select="lemma/gram/form[@tag='cP3']"/></td></xsl:if></tr>
                           <tr><td>4. pád</td><td><xsl:value-of select="lemma/gram/form[@tag='c4']"/></td><xsl:if test="lemma/gram/form[@tag='cP1']"><td><xsl:value-of select="lemma/gram/form[@tag='cP4']"/></td></xsl:if></tr>
                           <tr><td>5. pád</td><td><xsl:value-of select="lemma/gram/form[@tag='c5']"/></td><xsl:if test="lemma/gram/form[@tag='cP1']"><td><xsl:value-of select="lemma/gram/form[@tag='cP5']"/></td></xsl:if></tr>
                           <tr><td>6. pád</td><td><xsl:value-of select="lemma/gram/form[@tag='c6']"/></td><xsl:if test="lemma/gram/form[@tag='cP1']"><td><xsl:value-of select="lemma/gram/form[@tag='cP6']"/></td></xsl:if></tr>
                           <tr><td>7. pád</td><td><xsl:value-of select="lemma/gram/form[@tag='c7']"/></td><xsl:if test="lemma/gram/form[@tag='cP1']"><td><xsl:value-of select="lemma/gram/form[@tag='cP7']"/></td></xsl:if></tr>
                         </table>
                       </xsl:when>
                       <xsl:otherwise>
                         <table>
                           <xsl:for-each select="lemma/gram/form">
                             <tr>
                               <td><xsl:value-of select="@tag"/></td>
                               <td><xsl:value-of select="."/></td>
                             </tr>
                           </xsl:for-each>
                         </table>
                       </xsl:otherwise>
                     </xsl:choose>
                   </xsl:if>  <!--konec tabulky-->
                   
                   
                   <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_cjlingvist')">
                   <br /><span class="set-status">
                     stylistický popis: 
                     <xsl:call-template name="status_publish">
                       <xsl:with-param name="status" select="/entry/lemma/style_note[1]/@status"/>
                       <xsl:with-param name="type">style</xsl:with-param>
                     </xsl:call-template>
                   </span>
                   <br/>
                 </xsl:if>
    <div class="author-info" style="display:none">      
     <xsl:if test="lemma/style_note/@source!=''"><span class="author-source">zdroj: <xsl:value-of select="lemma/style_note/@source"/></span>; </xsl:if>
     <xsl:if test="lemma/style_note/@author!=''"><span class="author-author">autor: <xsl:value-of select="lemma/style_note/@author"/></span>; </xsl:if>
     <xsl:if test="lemma/style_note/@copyright!=''"><span class="author-copyright">copyright: <xsl:value-of select="lemma/style_note/@copyright"/></span></xsl:if>
    </div>

                     <xsl:if test="lemma/style_note/@kategorie!=''"><br/><b>Stylové hodnocení: </b>
                     <xsl:choose>
                       <xsl:when test="lemma/style_note/@kategorie='argot'">argot</xsl:when>
                       <xsl:when test="lemma/style_note/@kategorie='hovorove'">hovorově</xsl:when>
                       <xsl:when test="lemma/style_note/@kategorie='morav'">oblastní moravský výraz</xsl:when>
                       <xsl:when test="lemma/style_note/@kategorie='narec'">nářeční výraz</xsl:when>
                       <xsl:when test="lemma/style_note/@kategorie='neologismus' or lemma/style_note/@kategorie='Neologismus'">neologismus</xsl:when>
                       <xsl:when test="lemma/style_note/@kategorie='obecne'">obecně česky</xsl:when>
                       <xsl:when test="lemma/style_note/@kategorie='oblast'">oblastní výraz</xsl:when>
                       <xsl:when test="lemma/style_note/@kategorie='slang'">slangový výraz</xsl:when>
                       <xsl:when test="lemma/style_note/@kategorie='spisovne'">spisovně</xsl:when>
                     </xsl:choose>
                   </xsl:if>
                   <xsl:if test="lemma/style_note/@stylpriznak!=''"><br/><b>Stylový příznak: </b>
                     <xsl:call-template name="split_priznak"><xsl:with-param name="text" select="lemma/style_note/@stylpriznak"/></xsl:call-template>
                   </xsl:if>
                   
                   <xsl:if test="lemma/style_note/variant">
                     <br/><b>
                        <xsl:if test="lemma/grammar_note/@slovni_druh!='ustalene'">Stylistická </xsl:if>
                         varianta: </b>
                     <xsl:apply-templates select="lemma/style_note/variant"/>
                   </xsl:if>
                   <xsl:if test="normalize-space(lemma/style_note/text())!=''"><br/><xsl:apply-templates select="lemma/style_note" mode="ingram"/></xsl:if>
                   
                 </xsl:if>
                 
               </div>
             </div>
           </xsl:if>
           <div class="cj-vyznam">
           <xsl:for-each select="meanings/meaning[(text!='' or count(relation)>0 or usages/usage/text!='') and (status!='hidden' or $perm!='ro' or $skupina_test='true')]">
              <xsl:sort select="@number" data-type="number"/>
              <div class="meaningtop" id="meaning{@id}">
                <div class="author-info" style="display:none">
                  <xsl:if test="@source!=''"><span class="author-source">zdroj: <xsl:value-of select="@source"/></span>; </xsl:if>
                  <xsl:if test="@author!=''"><span class="author-author">autor: <xsl:value-of select="@author"/></span>; </xsl:if>
                  <xsl:if test="@copyright!=''"><span class="author-copyright">copyright: <xsl:value-of select="@copyright"/></span></xsl:if>
                </div>
                <div class="cj-lemma-h1">Význam 
                  <xsl:if test="count(//meanings/meaning[status='published'])>1">
                    <xsl:value-of select="position()"/>
                  </xsl:if>
                  <xsl:if test="count(//meanings/meaning[status='published'])&lt;=1 and ($perm!='ro' or $skupina_test='true')">
                    <xsl:value-of select="@number"/>
                  </xsl:if>
                   <xsl:if test="($perm!='ro' or $skupina_test='true') and @number!=substring-after(@id,'-')"> <span class="source">[! id:<xsl:value-of select="@id"/>]</span></xsl:if>
                </div>
                <div id="@id" class="cj-meaning">
                  <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_cjlingvist')">
                    <span class="set-status"> 
                      <xsl:call-template name="status_publish">
                        <xsl:with-param name="status" select="status"/>
                        <xsl:with-param name="type">meaning</xsl:with-param>
                        <xsl:with-param name="element" select="."/>
                      </xsl:call-template>
                    </span>
                    <br/>
                  </xsl:if>

                  <xsl:if test="@oblast!=''">
                    <span class="category"><b>Významová oblast: </b>
                      <xsl:call-template name="split_oblast"><xsl:with-param name="text" select="@oblast"/></xsl:call-template>
                    </span><br/>
                  </xsl:if>
                  <xsl:apply-templates select="text"/>
              <xsl:if test="relation[@type='translation_colloc']">
                <p><strong>Slovní spojení:</strong></p>
                <xsl:apply-templates select="relation[@type='translation_colloc' and ((@status!='hidden' and @auto_complete='1') or $perm!='ro' or $skupina_test='true')]"/>
              </xsl:if>
              <xsl:if test="relation[@type='synonym']">
                <p><strong>Synonyma: </strong>
                <xsl:apply-templates select="relation[@type='synonym' and ((@status!='hidden' or @auto_complete='1') or $perm!='ro' or $skupina_test='true')]"/>
               </p>
              </xsl:if>
              <xsl:if test="relation[@type='antonym']">
                <p><strong>Antonyma: </strong>
                  <xsl:apply-templates select="relation[@type='antonym' and ((@status!='hidden' or @auto_complete='1') or $perm!='ro' or $skupina_test='true')]"/>
                 </p>
              </xsl:if>
              <xsl:if test="count(usages/usage[text!=''])>0">
                <div class="cj-lemma-h2">Příklady užití:</div>
                <xsl:for-each select="usages/usage[(status!='hidden' or $perm!='ro' or $skupina_test='true') and @type='colloc']">
                  <xsl:if test="text!=''">
                    <div style="margin-bottom: 7px;"><em><xsl:apply-templates select="text"/></em>
                      <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_cjlingvist')">
                        <span class="set-status">
                          <xsl:call-template name="status_publish">
                            <xsl:with-param name="status" select="status"/>
                            <xsl:with-param name="type">usage</xsl:with-param>
                            <xsl:with-param name="element" select="."/>
                          </xsl:call-template>
                        </span>
                      </xsl:if>
                      <xsl:if test="relation">
                        <em style="font-size:80%; cursor: pointer;" onclick="$(this).next().toggle();$(this).find('.usagetrans-toggle').toggle();"> <span class="usagetrans-toggle">(zobrazit překlady<img src="/media/img/slovnik-expand.png" height="10" />)</span><span class="usagetrans-toggle" style="display:none;">(skrýt překlady<img src="/media/img/slovnik-collapse.png" height="10" />)</span></em>
                        <div class="usage-translation" style="display:none; padding-left: 20px; font-style: italic; font-size:90%">
                          <div><strong>Překlady: </strong></div>
                          <xsl:for-each select="relation[@target='en' or @target='cs']">
                            <div class="dict-@target"><xsl:choose><xsl:when test="@target='en'"><span style="color: grey">EN:</span></xsl:when><xsl:when test="@target='cs'"><span style="color: grey">ČJ:</span></xsl:when></xsl:choose>
                              <xsl:value-of select="title"/></div>
                          </xsl:for-each>
                          <xsl:apply-templates select="relation[@target!='en' and @target!='cs']" mode="usage">
                            <xsl:with-param name="variant_pos" select="position()"/> 
                          </xsl:apply-templates>
                          <div style="clear:both"/>
                        </div>
                      </xsl:if>
                    </div>
                  </xsl:if>
                  <div class="author-info" style="display:none">
                    <xsl:if test="@source!=''"><span class="author-source">zdroj: <xsl:value-of select="@source"/></span>; </xsl:if>
                    <xsl:if test="@author!=''"><span class="author-author">autor: <xsl:value-of select="@author"/></span>; </xsl:if>
                    <xsl:if test="@copyright!=''"><span class="author-copyright">copyright: <xsl:value-of select="@copyright"/></span></xsl:if>
                  </div>
                </xsl:for-each>
                <xsl:for-each select="usages/usage[(status!='hidden' or $perm!='ro' or $skupina_test='true') and not(@type='colloc')]">
                  <xsl:if test="text!=''">
                    <div style="margin-bottom: 7px;"><em><xsl:apply-templates select="text"/></em>
                      <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_cjlingvist')">
                        <span class="set-status">
                          <xsl:call-template name="status_publish">
                            <xsl:with-param name="status" select="status"/>
                            <xsl:with-param name="type">usage</xsl:with-param>
                            <xsl:with-param name="element" select="."/>
                          </xsl:call-template>
                        </span>
                      </xsl:if>
                    </div>
                  </xsl:if>
                  <div class="author-info" style="display:none">
                    <xsl:if test="@source!=''"><span class="author-source">zdroj: <xsl:value-of select="@source"/></span>; </xsl:if>
                    <xsl:if test="@author!=''"><span class="author-author">autor: <xsl:value-of select="@author"/></span>; </xsl:if>
                    <xsl:if test="@copyright!=''"><span class="author-copyright">copyright: <xsl:value-of select="@copyright"/></span></xsl:if>
                  </div>
                </xsl:for-each>
              </xsl:if>
              <xsl:if test="relation[@type='translation' and ((@status!='hidden') or $perm!='ro' or $skupina_test='true')]">
                <p><strong>Překlady:</strong></p>
                <div class="czj-relation-wrapper">
                  <p><xsl:apply-templates select="relation[@type='translation' and (@target='cs' or @target='en')]"/></p>
                  <xsl:apply-templates select="relation[@type='translation' and not(@target='cs' or @target='en')]"/>
                </div>
              </xsl:if>
            </div>
          </div>
        </xsl:for-each> 
        </div> <!-- cj-vyznam -->
        
        <div style="clear:both"></div>
        <div id="collocations">
          <xsl:if test="collocations/revcolloc[$perm!='ro' or $skupina_test='true' or @auto_complete='1' or @completeness='2' or @completeness='100']">
            <div class="cj-lemma-h1">Slovní spojení s tímto heslem</div>
            <!--<div class="collocations">-->              
              <xsl:apply-templates select="collocations/revcolloc[$perm!='ro' or $skupina_test='true' or @auto_complete='1' or @completeness='2' or @completeness='100']">
              <xsl:sort select="title"/>
              </xsl:apply-templates>
            <!--</div>-->
            <div style="clear:both"></div>
          </xsl:if>
        </div>

         <input type="button" value="zobrazit/skrýt autorské info" id="show-author" onclick="$('.author-info').toggle();"/> 
      </div> <!-- jen preview -->
    </div>   <!-- jen preview -->
  </body>
</html>
  </xsl:template>
  
  <xsl:template match="relation[(@type='translation' or @type='translation_colloc') and not(@target='cs' or @target='en')]">
    <xsl:if test="((@status!='hidden' or name(..)='usage' and ../status='published') and (@auto_complete='1' or @front_video='published')) or $perm!='ro' or $skupina_test='true'">     <!-- doplnit do anglické verze a inline -->
      
    <xsl:variable name="lemma"><xsl:value-of select="@lemma_id"/></xsl:variable>
    <xsl:variable name="rtype"><xsl:value-of select="@type"/></xsl:variable>    
    <xsl:if test="count(preceding-sibling::relation[@lemma_id=$lemma and @type=$rtype])=0">

    <div class="czj-relation-item dict-{@target}">
    <div class="czj-relation relationvideo">
      <div class="czj-lemma-preview-sm">
        <div class="czj-lemma-preview-sm-header dict-{@target}">

          <div class="alignleft" style="white-space: nowrap; overflow: hidden; width: 160px;">
          <xsl:choose>
              <xsl:when test="@target='czj'">ČZJ </xsl:when>          
              <xsl:when test="@target='is'">IS </xsl:when>
              <xsl:when test="@target='asl'">ASL </xsl:when>
              <xsl:otherwise>?:</xsl:otherwise>
            </xsl:choose>

           <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_cjlingvist')">
              <xsl:if test="@meaning_nr='' or @meaning_count = 1">
                <span class="set-status">
                  <xsl:call-template name="status_publish">
                    <xsl:with-param name="status" select="@status"/>
                    <xsl:with-param name="type">relation</xsl:with-param>
                    <xsl:with-param name="element" select="."/>
                  </xsl:call-template>
                </span>
              </xsl:if>
              <!--<br/>-->
              </xsl:if><xsl:if test="@meaning_nr!='' and @meaning_count > 1">ve významu <xsl:value-of select="@meaning_nr"/>
                <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_cjlingvist')">
                  <span class="set-status">
                    <xsl:call-template name="status_publish">
                      <xsl:with-param name="status" select="@status"/>
                      <xsl:with-param name="type">relation</xsl:with-param>
                      <xsl:with-param name="element" select="."/>
                    </xsl:call-template>
                  </span>
                </xsl:if>
              <xsl:if test="count(following-sibling::relation[@lemma_id=$lemma and @type=$rtype]) &gt; 0">
              <xsl:for-each select="following-sibling::relation[@lemma_id=$lemma and @type=$rtype]">
              <xsl:sort select="@meaning_nr"/>
              <xsl:text>, </xsl:text><xsl:value-of select="@meaning_nr"/>
                  <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_cjlingvist')">
                    <span class="set-status">
                      <xsl:call-template name="status_publish">
                        <xsl:with-param name="status" select="@status"/>
                        <xsl:with-param name="type">relation</xsl:with-param>
                        <xsl:with-param name="element" select="."/>
                      </xsl:call-template>
                    </span>
                  </xsl:if>
                </xsl:for-each>
              </xsl:if>
          </xsl:if></div>
          <xsl:if test="$perm!='ro' or @auto_complete='1'">
          <div class="alignright">
            <a href="/{@target}?action=search&amp;getdoc={@lemma_id}">
              <!--<xsl:value-of select="@lemma_id"/>-->
              <img src="/media/img/slovnik-open_w.png" height="20"/>
              <xsl:if test="@type='translation_colloc'">
                <xsl:value-of select="translation"/>
              </xsl:if>
            </a>
          </div> 
          </xsl:if>
        </div> 
        <xsl:apply-templates select="file" mode="rel">
          <xsl:with-param name="target"><xsl:value-of select="@target"/></xsl:with-param>
          <xsl:with-param name="text_link"><xsl:if test="$perm='ro' and @auto_complete='0' and @front_video='published'">true</xsl:if></xsl:with-param>
        </xsl:apply-templates>
        <div class="czj-lemma-preview-sm-sw">
          <xsl:apply-templates select="swmix/sw" mode="rel"/>
        </div>
      </div>
     </div>
     </div> 
      <xsl:if test="@type='translation_colloc'">
        <br/>
      </xsl:if>
    </xsl:if>
    </xsl:if>
  </xsl:template>  

  <xsl:template match="relation[@type='translation' and (@target='cs' or @target='en')]">
    <xsl:variable name="lemma">
      <xsl:value-of select="@lemma_id"/>
    </xsl:variable>

    <span class="relation-entry">        
    <xsl:if test="@title_only!='true'">
        <xsl:if test="count(preceding-sibling::relation[@lemma_id=$lemma and @type='translation'])=0">          
          <xsl:if test="@auto_complete='1' or ($perm!='ro' or $skupina_test='true')">
            <span style="color: gray">        
              <xsl:choose>
                <xsl:when test="@target='cs'">CZ: </xsl:when>          
                <xsl:when test="@target='en'">EN: </xsl:when>              
                <xsl:otherwise>?</xsl:otherwise>
            </xsl:choose>
            </span>
            <a href="/{@target}?action=search&amp;getdoc={@lemma_id}">
              <xsl:value-of select="title"/></a>                       
          </xsl:if>          
          <xsl:if test="@auto_complete!='1' and $perm='ro' and $skupina_test!='true'">
          <span style="color: gray">        
          <xsl:choose>
              <xsl:when test="@target='cs'">CZ: </xsl:when>          
              <xsl:when test="@target='en'">EN: </xsl:when>              
              <xsl:otherwise>?</xsl:otherwise>
          </xsl:choose>
        </span>
          <xsl:value-of select="title"/></xsl:if>          
          <xsl:if test="(@meaning_nr!='' and @meaning_count > 1) and (@auto_complete='1' or ($perm!='ro' or $skupina_test='true'))"> (ve významu
            <xsl:value-of select="@meaning_nr"/>              
            <!--schvalování-->              
            <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_lingvist')">                
              <span class="set-status">                    
                <xsl:call-template name="status_publish">                    
                  <xsl:with-param name="status" select="@status"/>                    
                  <xsl:with-param name="type">relation</xsl:with-param>                    
                  <xsl:with-param name="element" select="."/>                  
                </xsl:call-template>                
              </span>              
            </xsl:if><!--konec schvalování-->                            
            <xsl:if test="count(following-sibling::relation[@lemma_id=$lemma and @type='translation']) &gt; 0">              
              <xsl:for-each select="following-sibling::relation[@lemma_id=$lemma and @type='translation']">  
                <xsl:sort select="@meaning_nr" data-type="number"/>              
                <xsl:text>, </xsl:text>
                <xsl:value-of select="@meaning_nr"/>                
                <!--schvalování-->                
                <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_lingvist')">                  
                  <span class="set-status">                      
                    <xsl:call-template name="status_publish">                      
                      <xsl:with-param name="status" select="@status"/>                      
                      <xsl:with-param name="type">relation</xsl:with-param>                      
                      <xsl:with-param name="element" select="."/>                    
                    </xsl:call-template>                  
                  </span>                
                </xsl:if>
                <!--konec schvalování-->                
              </xsl:for-each>            
            </xsl:if>)           
          </xsl:if>          
          <xsl:if test="@meaning_nr='' or @meaning_count = 1">            
            <!--schvalování-->            
            <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_lingvist')">              
              <span class="set-status">                  
                <xsl:call-template name="status_publish">                  
                  <xsl:with-param name="status" select="@status"/>                  
                  <xsl:with-param name="type">relation</xsl:with-param>                  
                  <xsl:with-param name="element" select="."/>                
                </xsl:call-template>              
              </span>            
            </xsl:if><!--konec schvalování-->            
          </xsl:if>          

          <!--<xsl:if test="position()!=last()">, </xsl:if>-->          
          <xsl:if test="position()!=last()">, </xsl:if>        
        </xsl:if>      
      </xsl:if>      
      <xsl:if test="@title_only='true'">
        <span style="color: gray">        
          <xsl:choose>
              <xsl:when test="@target='cs'">CZ: </xsl:when>          
              <xsl:when test="@target='en'">EN: </xsl:when>              
              <xsl:otherwise>?</xsl:otherwise>
          </xsl:choose>
        </span>        
        <xsl:value-of select="title"/>        
        <!--schvalování-->    
        <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_lingvist')">        
          <span class="set-status">            
            <xsl:call-template name="status_publish">            
              <xsl:with-param name="status" select="@status"/>            
              <xsl:with-param name="type">relation</xsl:with-param>            
              <xsl:with-param name="element" select="."/>          
            </xsl:call-template>        
          </span>      
        </xsl:if><!--konec schvalování-->                  

        <xsl:if test="position()!=last()">, </xsl:if>      
      </xsl:if>    
    </span>  
  </xsl:template>
  <xsl:template match="relation" mode="usage">
    <xsl:param name="variant_pos"/>  
    <xsl:apply-templates select="file" mode="usage_translation">
      <xsl:with-param name="variant_sw" select="swmix"/>
      <xsl:with-param name="variant_pos" select="position()"/> 
      <xsl:with-param name="usage_target" select="@target"/> 
      <xsl:with-param name="relation_link"><xsl:if test="(@auto_complete='1' or $perm!='ro') and not(contains(@meaning_id,'_us'))">true</xsl:if></xsl:with-param>
    </xsl:apply-templates>  
  </xsl:template>
  <xsl:template match="file" mode="usage_translation">
    <xsl:param name="variant_sw"/>  
    <xsl:param name="variant_pos"/>  
    <xsl:param name="usage_target"/>
    <xsl:param name="relation_link"/>
    <div class="czj-relation-item">    
      <div class="czj-relation relationvideo">      
        <div class="czj-lemma-preview-sm">          
          <div class="czj-lemma-preview-sm-header">                                
            <div class="alignleft"><xsl:choose><xsl:when test="$usage_target='czj'">ČZJ</xsl:when>
                <xsl:when test="$usage_target='is'">IS</xsl:when>
                <xsl:when test="$usage_target='asl'">ASL</xsl:when></xsl:choose>
            </div>
            <xsl:if test="$relation_link='true'">
              <div class="alignright">
                <a href="/{$target}?action=search&amp;getdoc={../@lemma_id}">
                  <img src="/media/img/slovnik-open_w.png" height="20"/></a>
              </div>
            </xsl:if>
          </div>      
        </div>
        <div style="clear:both">
        </div>  
           
      <xsl:if test="substring(location, string-length(location)-3, 4) = '.flv'">
        <div id="flowvideousg{@id}-{../@meaning_id}" data-ratio="0.8" class="flowplayer flowmouse usage-video{$relation_link}" style="background:#777 url(/media/video{$usage_target}/thumb/{location}/thumb.jpg) no-repeat;">
          <video preload="none" loop="loop">      
            <source type="video/flash" src="/media/video{$usage_target}/{location}"/>
          </video>            
        </div><br />
      </xsl:if>
      <xsl:if test="substring(location, string-length(location)-3, 4) = '.mp4'">
        <div id="flowvideousg{@id}-{../@meaning_id}" data-ratio="0.8" class="flowmouse usage-video{$relation_link}" style="background: url(/media/video{$usage_target}/thumb/{location}/thumb.jpg) no-repeat;">
          <video preload="none" width="120px" height="96px" poster="/media/video{$usage_target}/thumb/{location}/thumb.jpg" onmouseover="this.play()" onmouseout="this.pause()">
            <source type="video/mp4" src="/media/video{$usage_target}/{location}"/>
          </video>
        </div>  
      </xsl:if>  
            <div class="czj-lemma-preview-sm-sw">     
              <xsl:if test="($variant_sw/sw)">
                <xsl:apply-templates select="$variant_sw/sw" mode="rel"/>
              </xsl:if>  
            </div>          
        <xsl:if test="$relation_link='true'">
           <script type="text/javascript">
             $("#flowvideousg<xsl:value-of select="@id"/>-<xsl:value-of select="../@meaning_id"/>").on("click", function(e) {
               window.location='/<xsl:value-of select="$usage_target"/>?action=search&amp;getdoc=<xsl:value-of select="../@lemma_id"/>';
                 });
             </script>
        </xsl:if>
        <xsl:if test="not($relation_link='true')">
<script type="text/javascript">
  $("#flowvideousg<xsl:value-of select="@id"/>-<xsl:value-of select="../@meaning_id"/>").on("click", function(e) {
          var container = $('<div data-ratio="0.8" style="width:285px;">
            <video preload="none" controls="" width="285px" height="228px" poster="/media/video{$usage_target}/thumb/{location}/thumb.jpg" autoplay="">
            <source type="video/mp4" src="/media/video{$usage_target}/{location}"/>
            </video>
            </div>');
          $.fancybox({
            content: container,
            width: 285,
            height: 228,
            scrolling: 'no',
            autoSize: false
          });
        });
      </script>
    </xsl:if>
      </div>  
    </div>
  </xsl:template>

  <xsl:template match="relation[@type!='translation' and @type!='translation_colloc']">
    <xsl:variable name="lemma"><xsl:value-of select="@lemma_id"/></xsl:variable>
    <xsl:variable name="rtype"><xsl:value-of select="@type"/></xsl:variable>

    <xsl:if test="count(preceding-sibling::relation[@lemma_id=$lemma and @type=$rtype])=0">
        <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_cjlingvist')">
          <span class="set-status">
            <xsl:call-template name="status_publish">
              <xsl:with-param name="status" select="@status"/>
              <xsl:with-param name="type">relation</xsl:with-param>
              <xsl:with-param name="element" select="."/>
            </xsl:call-template>
          </span>
        </xsl:if>
        <xsl:if test="@title_only!='true'">
          <xsl:if test="@auto_complete='1' or $perm!='ro' or $skupina_test='true'">
            <a href="/cs?action=search&amp;getdoc={@lemma_id}"><xsl:value-of select="title"/></a>
          </xsl:if>
          <xsl:if test="@auto_complete='0' and @status='published' and $perm='ro' and $skupina_test='true'">
            <xsl:value-of select="title"/>
          </xsl:if>
          <xsl:if test="@meaning_nr!=''">
          <xsl:if test="@auto_complete='1' or $perm!='ro' or $skupina_test='true'"> 
            (ve významu <xsl:value-of select="@meaning_nr"/>
            <xsl:if test="count(following-sibling::relation[@lemma_id=$lemma and @type=$rtype]) &gt; 0">
              <xsl:for-each select="following-sibling::relation[@lemma_id=$lemma and @type=$rtype]">
              <xsl:sort select="@meaning_nr" data-type="number"/>
                <xsl:text>, </xsl:text><xsl:value-of select="@meaning_nr"/>
              </xsl:for-each>
          </xsl:if>)
        </xsl:if>
          </xsl:if>
          </xsl:if>
        <xsl:if test="@title_only='true'">
          <xsl:value-of select="title"/>
        </xsl:if>
      <xsl:if test="position()!=last()"><xsl:text>, </xsl:text></xsl:if>
    </xsl:if>
  </xsl:template>

  <xsl:template match="sw" mode="rel">
    <xsl:if test=".!=''">
      <span class="sw transsw" style="">
        <a href="http://znaky.zcu.cz/proxy/tts/signwriting.png?generator[sw]={.}&amp;generator[align]=top_left&amp;generator[set]=sw10" class="fancybox swlink" data-sw="{.}">
          <img src="http://znaky.zcu.cz/proxy/tts/signwriting.png?generator[sw]={.}&amp;generator[align]=top_left&amp;generator[set]=sw10" alt="signwriting" class="signwriting" ratio="0.5" style=""/>

          <!-- <img style="" src="http://znaky.zcu.cz/proxy/tts/signwriting.png?generator[sw]={.}&amp;generator[align]=top_left&amp;generator[set]=sw10" alt="signwriting" class="signwriting"/>-->
      </a></span>
    </xsl:if>
  </xsl:template>

  <xsl:template match="file" mode="rel">    
     <xsl:param name="target"><xsl:value-of select="$dictcode"/></xsl:param>
        <xsl:param name="text_link">false</xsl:param>
        <xsl:if test="substring(location, string-length(location)-3, 4) = '.flv'">
          <div id="flowvideorel{id}" data-ratio="0.8" class="flowplayer flowmouse" style="width:120px; background:#777 url(/media/video{$target}/thumb/{location}/thumb.jpg) no-repeat; background-size: 120px 96px">
            <video loop="loop">
              <source type="video/flash" src="/media/video{$target}/{location}"/>
            </video>
          </div>    
        </xsl:if>
        <xsl:if test="substring(location, string-length(location)-3, 4) = '.mp4'">
           <div id="flowvideorel{id}" data-ratio="0.8" class="flowmouse" style="width:120px; background: url(/media/video{$target}/thumb/{location}/thumb.jpg) no-repeat; background-size: 120px 96px; float:left;">    
                <video loop="loop" width="120px" height="96px" poster="/media/video{$target}/{location}/thumb.jpg" onmouseover="this.play()" onmouseout="this.pause()">
                  <source type="video/mp4" src="/media/video{$target}/{location}"/>
                </video>
           </div>   
        </xsl:if>
      
  <xsl:if test="$text_link!='true'">
    <script type="text/javascript">
      $("#flowvideorel<xsl:value-of select="id"/>").on("click", function(e) {
      window.location='/<xsl:value-of select="$target"/>?action=search&amp;getdoc=<xsl:value-of select="../@lemma_id"/>';
      });
    </script>
  </xsl:if>
  </xsl:template>

<xsl:template match="colloc">
     <span style="vertical-align:top;">
      <xsl:if test="not(title)">
        <xsl:value-of select="@lemma_id"/>
      </xsl:if>
      <xsl:if test="title">
        <a href="/cs?action=search&amp;getdoc={@lemma_id}">
          <xsl:value-of select="title"/>
        </a>
      </xsl:if>
      <xsl:if test="position()!=last()">
        <xsl:text>,&#160;</xsl:text>
      </xsl:if>
    </span>
</xsl:template>


  <xsl:template match="table|tr|td|tbody">
    <xsl:element name="{name(.)}">
      <xsl:apply-templates/>
    </xsl:element>
  </xsl:template>

  <xsl:template name="split_skupina">
    <xsl:param name="text"/>
    <xsl:variable name="first" select='substring-before($text,";")'/>
    <xsl:variable name='rest' select='substring-after($text,";")'/>
    <xsl:if test='$first'>
      <xsl:choose>
        <xsl:when test="$first='pomnozne'">pomnožné</xsl:when>
        <xsl:when test="$first='hromadne'">hromadné</xsl:when>
        <xsl:when test="$first='vlastni'">vlastní</xsl:when>
        <xsl:when test="$first='osobni'">osobní</xsl:when>
        <xsl:when test="$first='ukazovaci'">ukazovací</xsl:when>
        <xsl:when test="$first='vztazne'">vztažné</xsl:when>
        <xsl:when test="$first='zaporne'">záporné</xsl:when>
        <xsl:when test="$first='neurcite'">neurčité</xsl:when>
        <xsl:when test="$first='privl'">přivlastňovací</xsl:when>
        <xsl:when test="$first='zakladni'">základní</xsl:when>
        <xsl:when test="$first='druhova'">druhová</xsl:when>
        <xsl:when test="$first='radova'">řadová</xsl:when>
        <xsl:when test="$first='nasobna'">násobná</xsl:when>
        <xsl:when test="$first='tazaci'">tázací</xsl:when>
        <xsl:when test="$first='dok'">dokonavý vid</xsl:when>
        <xsl:when test="$first='nedok'">nedokonavý vid</xsl:when>
        <xsl:when test="$first='zajmenne'">zájmenné</xsl:when>
        <xsl:when test="$first='sourad'">souřadicí</xsl:when>
        <xsl:when test="$first='podrad'">podřadicí</xsl:when>
        <xsl:when test="$first='nasobnapris'">násobná příslovečná</xsl:when>
        <xsl:when test="$first='nasobnaprisneu'">neurčitá násobná příslovečná</xsl:when>
        <xsl:when test="$first='zajmenna'">zájmenná</xsl:when>
        <xsl:when test="$first='frazem'">frazém</xsl:when>
        <xsl:when test="$first='porekadlo'">pořekadlo</xsl:when>
        <xsl:when test="$first='prirovnani'">přirovnání</xsl:when>
        <xsl:when test="$first='prislovi'">přísloví</xsl:when>          
      </xsl:choose>
      <xsl:if test="$rest">, </xsl:if>
    </xsl:if>

    <xsl:if test='$rest'>
      <xsl:call-template name='split_skupina'>
        <xsl:with-param name='text' select='$rest'/>
      </xsl:call-template>
    </xsl:if>
    <xsl:if test='not($rest)'>
      <xsl:choose>
        <xsl:when test="$text='pomnozne'">pomnožné</xsl:when>
        <xsl:when test="$text='hromadne'">hromadné</xsl:when>
        <xsl:when test="$text='vlastni'">vlastní</xsl:when>
        <xsl:when test="$text='osobni'">osobní</xsl:when>
        <xsl:when test="$text='ukazovaci'">ukazovací</xsl:when>
        <xsl:when test="$text='vztazne'">vztažné</xsl:when>
        <xsl:when test="$text='zaporne'">záporné</xsl:when>
        <xsl:when test="$text='neurcite'">neurčité</xsl:when>
        <xsl:when test="$text='privl'">přivlastňovací</xsl:when>
        <xsl:when test="$text='zakladni'">základní</xsl:when>
        <xsl:when test="$text='druhova'">druhová</xsl:when>
        <xsl:when test="$text='radova'">řadová</xsl:when>
        <xsl:when test="$text='nasobna'">násobná</xsl:when>
        <xsl:when test="$text='tazaci'">tázací</xsl:when>
        <xsl:when test="$text='dok'">dokonavý vid</xsl:when>
        <xsl:when test="$text='nedok'">nedokonavý vid</xsl:when>
        <xsl:when test="$text='zajmenne'">zájmenné</xsl:when>
        <xsl:when test="$text='sourad'">souřadicí</xsl:when>
        <xsl:when test="$text='podrad'">podřadicí</xsl:when>
        <xsl:when test="$text='nasobnapris'">násobná příslovečná</xsl:when>
        <xsl:when test="$text='nasobnaprisneu'">neurčitá násobná příslovečná</xsl:when>
        <xsl:when test="$text='zajmenna'">zájmenná</xsl:when>
        <xsl:when test="$text='frazem'">frazém</xsl:when>
        <xsl:when test="$text='porekadlo'">pořekadlo</xsl:when>
        <xsl:when test="$text='prirovnani'">přirovnání</xsl:when>
        <xsl:when test="$text='prislovi'">přísloví</xsl:when>
      </xsl:choose>
    </xsl:if>
  </xsl:template>

  <xsl:template name="split_skupina2">
    <xsl:param name="text"/>
    <xsl:variable name="first" select='substring-before($text,";")'/>
    <xsl:variable name='rest' select='substring-after($text,";")'/>
    <xsl:if test='$first'>
      <xsl:choose>
        <xsl:when test="$first='m'">mužský rod</xsl:when>
        <xsl:when test="$first='z'">ženský rod</xsl:when>
        <xsl:when test="$first='s'">střední rod</xsl:when>
        <xsl:when test="$first='zpusobove'">způsobové</xsl:when>
        <xsl:when test="$first='ukazovaci'">ukazovací</xsl:when>
        <xsl:when test="$first='tazaci'">tázací</xsl:when>
        <xsl:when test="$first='vztazne'">vztažné</xsl:when>
        <xsl:when test="$first='neurcite'">neurčité</xsl:when>
        <xsl:when test="$first='zaporne'">záporné</xsl:when>
        <xsl:when test="$first='prisudku'">v přísudku</xsl:when>
        <xsl:when test="$first='vymez'">vymezovací</xsl:when>
        <xsl:when test="$first='zduraz'">zdůrazňovací</xsl:when>
        <xsl:when test="$first='obsahova'">obsahová</xsl:when>
        <xsl:when test="$first='modalni'">modální</xsl:when>
        <xsl:when test="$first='citoslovecna'">citoslovečná</xsl:when>
      </xsl:choose>
      <xsl:if test="$rest">, </xsl:if>
    </xsl:if>

    <xsl:if test='$rest'>
      <xsl:call-template name='split_skupina2'>
        <xsl:with-param name='text' select='$rest'/>
      </xsl:call-template>
    </xsl:if>
    <xsl:if test='not($rest)'>
      <xsl:choose>
        <xsl:when test="$text='m'">mužský rod</xsl:when>
        <xsl:when test="$text='z'">ženský rod</xsl:when>
        <xsl:when test="$text='s'">střední rod</xsl:when>
        <xsl:when test="$text='zpusobove'">způsobové</xsl:when>
        <xsl:when test="$text='ukazovaci'">ukazovací</xsl:when>
        <xsl:when test="$text='tazaci'">tázací</xsl:when>
        <xsl:when test="$text='vztazne'">vztažné</xsl:when>
        <xsl:when test="$text='neurcite'">neurčité</xsl:when>
        <xsl:when test="$text='zaporne'">záporné</xsl:when>
        <xsl:when test="$text='prisudku'">v přísudku</xsl:when>
        <xsl:when test="$text='vymez'">vymezovací</xsl:when>
        <xsl:when test="$text='zduraz'">zdůrazňovací</xsl:when>
        <xsl:when test="$text='obsahova'">obsahová</xsl:when>
        <xsl:when test="$text='modalni'">modální</xsl:when>
        <xsl:when test="$text='citoslovecna'">citoslovečná</xsl:when>
      </xsl:choose>
    </xsl:if>
  </xsl:template>

  <xsl:template name="split_priznak">
    <xsl:param name="text"/>
    <xsl:variable name="first" select='substring-before($text,";")'/>
    <xsl:variable name='rest' select='substring-after($text,";")'/>
    <xsl:if test='$first'>
      <xsl:choose>
        <xsl:when test="$first='abstrakt'">abstraktní výraz</xsl:when>
        <xsl:when test="$first='basnicky'">básnický výraz</xsl:when>
        <xsl:when test="$first='biblicky'">biblický výraz</xsl:when>
        <xsl:when test="$first='detsky'">dětský výraz</xsl:when>
        <xsl:when test="$first='domacke'">domácké slovo</xsl:when>
        <xsl:when test="$first='duverny'">důvěrný výraz</xsl:when>
        <xsl:when test="$first='eufem'">eufemismus</xsl:when>
        <xsl:when test="$first='expres'">expresivní výraz</xsl:when>
        <xsl:when test="$first='famil'">familiární výraz</xsl:when>
        <xsl:when test="$first='hanlivy'">hanlivý výraz</xsl:when>
        <xsl:when test="$first='ironicky'">ironicky</xsl:when>
        <xsl:when test="$first='kladny'">kladný výraz</xsl:when>
        <xsl:when test="$first='knizni'">knižní výraz</xsl:when>
        <xsl:when test="$first='konkretni'">konkrétní výraz</xsl:when>
        <xsl:when test="$first='lidovy'">lidový výraz</xsl:when>
        <xsl:when test="$first='lichotivy'">lichotivý výraz</xsl:when>
        <xsl:when test="$first='mazlivy'">mazlivý výraz</xsl:when>
        <xsl:when test="$first='odborny'">odborný výraz</xsl:when>
        <xsl:when test="$first='pohadkovy'">pohádkový výraz</xsl:when>
        <xsl:when test="$first='publ'">publicistický výraz</xsl:when>
        <xsl:when test="$first='prenes'">přeneseně</xsl:when>
        <xsl:when test="$first='zdvor'">zdvořilostní výraz</xsl:when>
        <xsl:when test="$first='zhrubely'">zhrubělý výraz</xsl:when>
        <xsl:when test="$first='zert'">žertovný výraz</xsl:when>
        <xsl:when test="$first='zlomkovy'">zlomkový výraz</xsl:when>
        <xsl:when test="$first='zdrob'">zdrobnělina</xsl:when>
        <xsl:when test="$first='zastar'">zastaralý výraz</xsl:when>
        <xsl:when test="$first='cirkev'">církevní výraz</xsl:when>
      </xsl:choose>    
      <xsl:if test="$rest">, </xsl:if>
    </xsl:if>
    <xsl:if test='$rest'>
      <xsl:call-template name='split_priznak'>
        <xsl:with-param name='text' select='$rest'/>
      </xsl:call-template>
    </xsl:if>
    <xsl:if test='not($rest)'>
      <xsl:choose>
        <xsl:when test="$text='abstrakt'">abstraktní výraz</xsl:when>
        <xsl:when test="$text='basnicky'">básnický výraz</xsl:when>
        <xsl:when test="$text='biblicky'">biblický výraz</xsl:when>
        <xsl:when test="$text='detsky'">dětský výraz</xsl:when>
        <xsl:when test="$text='domacke'">domácké slovo</xsl:when>
        <xsl:when test="$text='duverny'">důvěrný výraz</xsl:when>
        <xsl:when test="$text='eufem'">eufemismus</xsl:when>
        <xsl:when test="$text='expres'">expresivní výraz</xsl:when>
        <xsl:when test="$text='famil'">familiární výraz</xsl:when>
        <xsl:when test="$text='hanlivy'">hanlivý výraz</xsl:when>
        <xsl:when test="$text='ironicky'">ironicky</xsl:when>
        <xsl:when test="$text='kladny'">kladný výraz</xsl:when>
        <xsl:when test="$text='knizni'">knižní výraz</xsl:when>
        <xsl:when test="$text='konkretni'">konkrétní výraz</xsl:when>
        <xsl:when test="$text='lidovy'">lidový výraz</xsl:when>
        <xsl:when test="$text='lichotivy'">lichotivý výraz</xsl:when>
        <xsl:when test="$text='mazlivy'">mazlivý výraz</xsl:when>
        <xsl:when test="$text='odborny'">odborný výraz</xsl:when>
        <xsl:when test="$text='pohadkovy'">pohádkový výraz</xsl:when>
        <xsl:when test="$text='publ'">publicistický výraz</xsl:when>
        <xsl:when test="$text='prenes'">přeneseně</xsl:when>
        <xsl:when test="$text='zdvor'">zdvořilostní výraz</xsl:when>
        <xsl:when test="$text='zhrubely'">zhrubělý výraz</xsl:when>
        <xsl:when test="$text='zert'">žertovný výraz</xsl:when>
        <xsl:when test="$text='zlomkovy'">zlomkový výraz</xsl:when>
        <xsl:when test="$text='zdrob'">zdrobnělina</xsl:when>
        <xsl:when test="$text='zastar'">zastaralý výraz</xsl:when>
        <xsl:when test="$text='cirkev'">církevní výraz</xsl:when>
      </xsl:choose>    
    </xsl:if>
  </xsl:template>

  <xsl:template name="split_oblast">
    <xsl:param name="text"/>
    <xsl:variable name="first" select='substring-before($text,";")'/>
    <xsl:variable name='rest' select='substring-after($text,";")'/>
    <xsl:if test='$first'>
      <xsl:choose>
        <xsl:when test="$first='anat'">anatomie</xsl:when>
        <xsl:when test="$first='antr'">antropologie</xsl:when>
        <xsl:when test="$first='archeol'">archeologie</xsl:when>
        <xsl:when test="$first='archit'">architektura</xsl:when>
        <xsl:when test="$first='biol'">biologie</xsl:when>
        <xsl:when test="$first='bot'">botanika</xsl:when>
        <xsl:when test="$first='dipl'">diplomacie</xsl:when>
        <xsl:when test="$first='div'">divadelnictví</xsl:when>
        <xsl:when test="$first='dopr'">doprava</xsl:when>
        <xsl:when test="$first='ekol'">ekologie</xsl:when>
        <xsl:when test="$first='ekon'">ekonomie</xsl:when>
        <xsl:when test="$first='eltech'">elektrotechnika</xsl:when>
        <xsl:when test="$first='etn'">etnografie</xsl:when>
        <xsl:when test="$first='feud'">feudalismus</xsl:when>
        <xsl:when test="$first='filat'">filatelie</xsl:when>
        <xsl:when test="$first='film'">filmařství</xsl:when>
        <xsl:when test="$first='filoz'">filozofie</xsl:when>
        <xsl:when test="$first='fot'">fotografování</xsl:when>
        <xsl:when test="$first='fyz'">fyzika</xsl:when>
        <xsl:when test="$first='fyziol'">fyziologie</xsl:when>
        <xsl:when test="$first='geol'">geologie</xsl:when>
        <xsl:when test="$first='geom'">geometrie</xsl:when>
        <xsl:when test="$first='gnoz'">gnozeologie</xsl:when>
        <xsl:when test="$first='hist'">historie</xsl:when>
        <xsl:when test="$first='horn'">hornictví</xsl:when>
        <xsl:when test="$first='horol'">horolezectví</xsl:when>
        <xsl:when test="$first='hosp'">hospodářství</xsl:when>
        <xsl:when test="$first='hud'">hudební věda</xsl:when>
        <xsl:when test="$first='hut'">hutnictví</xsl:when>
        <xsl:when test="$first='hvězd'">astronomie</xsl:when>
        <xsl:when test="$first='chem'">chemie</xsl:when>
        <xsl:when test="$first='ideal'">idealismus</xsl:when>
        <xsl:when test="$first='informatika'">informatika</xsl:when>
        <xsl:when test="$first='jad'">jaderná fyzika</xsl:when>
        <xsl:when test="$first='jaz'">jazykověda</xsl:when>
        <xsl:when test="$first='kapit'">kapitalismus</xsl:when>
        <xsl:when test="$first='karet'">karetní hra</xsl:when>
        <xsl:when test="$first='katol církvi'">katolictví</xsl:when>
        <xsl:when test="$first='krim'">kriminalistika</xsl:when>
        <xsl:when test="$first='křesť'">křesťanství</xsl:when>
        <xsl:when test="$first='kuch'">kuchařství</xsl:when>
        <xsl:when test="$first='kult'">kultura</xsl:when>
        <xsl:when test="$first='kyb'">kybernetika</xsl:when>
        <xsl:when test="$first='lék'">lékařství</xsl:when>
        <xsl:when test="$first='lékár'">lékárnictví</xsl:when>
        <xsl:when test="$first='let'">letectví</xsl:when>
        <xsl:when test="$first='liter'">literární věda</xsl:when>
        <xsl:when test="$first='log'">logika</xsl:when>
        <xsl:when test="$first='marx'">marxismus</xsl:when>
        <xsl:when test="$first='mat'">matematika</xsl:when>
        <xsl:when test="$first='meteor'">meteorologie</xsl:when>
        <xsl:when test="$first='miner'">mineralogie</xsl:when>
        <xsl:when test="$first='motor'">motorismus</xsl:when>
        <xsl:when test="$first='mysl'">myslivecký výraz</xsl:when>
        <xsl:when test="$first='mytol'">mytologie</xsl:when>
        <xsl:when test="$first='náb'">náboženství</xsl:when>
        <xsl:when test="$first='nár'">národopis</xsl:when>
        <xsl:when test="$first='obch'">obchod</xsl:when>
        <xsl:when test="$first='pedag'">pedagogika</xsl:when>
        <xsl:when test="$first='peněž'">peněžnictví</xsl:when>
        <xsl:when test="$first='podnikani'">podnikání</xsl:when>
        <xsl:when test="$first='polit'">politika</xsl:when>
        <xsl:when test="$first='polygr'">polygrafie</xsl:when>
        <xsl:when test="$first='pošt'">poštovnictví</xsl:when>
        <xsl:when test="$first='potrav'">potravinářství</xsl:when>
        <xsl:when test="$first='práv'">právo</xsl:when>
        <xsl:when test="$first='prům'">průmysl</xsl:when>
        <xsl:when test="$first='přír'">příroda</xsl:when>
        <xsl:when test="$first='psych'">psychologie</xsl:when>
        <xsl:when test="$first='rybn'">rybníkářství</xsl:when>
        <xsl:when test="$first='řem'">řemeslo</xsl:when>
        <xsl:when test="$first='sklář'">sklářství</xsl:when>
        <xsl:when test="$first='soc'">socialismus</xsl:when>
        <xsl:when test="$first='sociol'">sociologie</xsl:when>
        <xsl:when test="$first='stat'">statistika</xsl:when>
        <xsl:when test="$first='stav'">stavitelství</xsl:when>
        <xsl:when test="$first='škol'">školství</xsl:when>
        <xsl:when test="$first='tech'">technika</xsl:when>
        <xsl:when test="$first='těl'">sport</xsl:when>
        <xsl:when test="$first='text'">textilnictví</xsl:when>
        <xsl:when test="$first='úč'">účetnictví</xsl:when>
        <xsl:when test="$first='úř'">úřední výraz</xsl:when>
        <xsl:when test="$first='veř spr'">veřejná správa</xsl:when>
        <xsl:when test="$first='vet'">veterinářství</xsl:when>
        <xsl:when test="$first='voj'">vojenství</xsl:when>
        <xsl:when test="$first='výp tech'">výpočetní technika</xsl:when>
        <xsl:when test="$first='výr'">výroba</xsl:when>
        <xsl:when test="$first='výtv'">výtvarnictví</xsl:when>
        <xsl:when test="$first='zahr'">zahradnictví</xsl:when>
        <xsl:when test="$first='zbož'">zbožíznalství</xsl:when>
        <xsl:when test="$first='zeměd'">zemědělství</xsl:when>
        <xsl:when test="$first='zeměp'">zeměpis</xsl:when>
        <xsl:when test="$first='zool'">zoologie</xsl:when>
        <xsl:when test="$first='cirkev'">církev</xsl:when>
        <xsl:when test="$first='adm'">administrativa</xsl:when>
        <xsl:when test="$first='artistika'">artistika</xsl:when>
        <xsl:when test="$first='astro'">astronomie</xsl:when>
        <xsl:when test="$first='biblhist'">bibl. hist.</xsl:when>
        <xsl:when test="$first='bibl'">biblistika</xsl:when>
        <xsl:when test="$first='biochemie'">biochemie</xsl:when>
        <xsl:when test="$first='cirar'">círk. archit.</xsl:when>
        <xsl:when test="$first='cirhi'">církevní historie</xsl:when>
        <xsl:when test="$first='cirhu'">círk. hud.</xsl:when>
        <xsl:when test="$first='cirkr'">círk. křesť.</xsl:when>
        <xsl:when test="$first='cirpol'">círk. polit.</xsl:when>
        <xsl:when test="$first='cirpr'">círk. práv.</xsl:when>
        <xsl:when test="$first='cukr'">cukrovarnictví</xsl:when>
        <xsl:when test="$first='ekpub'">ekon. publ.</xsl:when>
        <xsl:when test="$first='eltech'">elektr.</xsl:when>
        <xsl:when test="$first='estet'">estetika</xsl:when>
        <xsl:when test="$first='farmak'">farmakologie</xsl:when>
        <xsl:when test="$first='folklor'">folkloristika</xsl:when>
        <xsl:when test="$first='fonetika'">fonetika</xsl:when>
        <xsl:when test="$first='fyzchem'">fyzikální chemie</xsl:when>
        <xsl:when test="$first='genetika'">genetika</xsl:when>
        <xsl:when test="$first='geochem'">geochem.</xsl:when>
        <xsl:when test="$first='geodez'">geodézie</xsl:when>
        <xsl:when test="$first='geofyz'">geofyzika</xsl:when>
        <xsl:when test="$first='geogr'">geografie</xsl:when>
        <xsl:when test="$first='geolzem'">geol. zeměd.</xsl:when>
        <xsl:when test="$first='hiadm'">historie administrativy</xsl:when>
        <xsl:when test="$first='hiar'">historie architektury</xsl:when>
        <xsl:when test="$first='hiast'">historie astronomie</xsl:when>
        <xsl:when test="$first='hidip'">historie diplomacie</xsl:when>
        <xsl:when test="$first='hidiv'">historie divadelnictví</xsl:when>
        <xsl:when test="$first='hieko'">historie ekonomie</xsl:when>
        <xsl:when test="$first='hietno'">historie etnografie</xsl:when>
        <xsl:when test="$first='hifil'">historie filozofie</xsl:when>
        <xsl:when test="$first='hifot'">historie fotografování</xsl:when>
        <xsl:when test="$first='higeo'">historie geografie</xsl:when>
        <xsl:when test="$first='hilit'">historie literatury</xsl:when>
        <xsl:when test="$first='himat'">historie matematika</xsl:when>
        <xsl:when test="$first='hinab'">historie náboženství</xsl:when>
        <xsl:when test="$first='hipen'">historie peněžnictví</xsl:when>
        <xsl:when test="$first='hipol'">historie politiky</xsl:when>
        <xsl:when test="$first='hipr'">historie práva</xsl:when>
        <xsl:when test="$first='hisk'">historie školství</xsl:when>
        <xsl:when test="$first='hispo'">historie sport</xsl:when>
        <xsl:when test="$first='hista'">historie stavitelství</xsl:when>
        <xsl:when test="$first='hitech'">historie techniky</xsl:when>
        <xsl:when test="$first='hivoj'">historie vojenství</xsl:when>
        <xsl:when test="$first='sach'">hra v šachy</xsl:when>
        <xsl:when test="$first='hudpubl'">hudební publicistika</xsl:when>
        <xsl:when test="$first='jadtech'">jad. tech.</xsl:when>
        <xsl:when test="$first='keram'">keramika</xsl:when>
        <xsl:when test="$first='kniho'">knihovnictví</xsl:when>
        <xsl:when test="$first='kosmet'">kosmet.</xsl:when>
        <xsl:when test="$first='kosmon'">kosmonautika</xsl:when>
        <xsl:when test="$first='kozed'">kožedělný průmysl</xsl:when>
        <xsl:when test="$first='krejc'">krejčovství</xsl:when>
        <xsl:when test="$first='kynol'">kynologie</xsl:when>
        <xsl:when test="$first='lesnic'">lesnictví</xsl:when>
        <xsl:when test="$first='lethist'">let. hist.</xsl:when>
        <xsl:when test="$first='lingv'">lingvistika</xsl:when>
        <xsl:when test="$first='liter'">liter. hist.</xsl:when>
        <xsl:when test="$first='lodar'">loďařství</xsl:when>
        <xsl:when test="$first='matlog'">mat. log.</xsl:when>
        <xsl:when test="$first='mezobch'">mezinár. obch.</xsl:when>
        <xsl:when test="$first='mezpr'">mezinárodní právo</xsl:when>
        <xsl:when test="$first='nabpol'">náb. polit.</xsl:when>
        <xsl:when test="$first='nabps'">náb. ps.</xsl:when>
        <xsl:when test="$first='namor'">námořnictví</xsl:when>
        <xsl:when test="$first='numiz'">numizmatika</xsl:when>
        <xsl:when test="$first='obchpr'">obch. práv.</xsl:when>
        <xsl:when test="$first='obuv'">obuvnictví</xsl:when>
        <xsl:when test="$first='odb'">odb.</xsl:when>
        <xsl:when test="$first='optika'">optika</xsl:when>
        <xsl:when test="$first='orient'">orient.</xsl:when>
        <xsl:when test="$first='paleo'">paleontologie</xsl:when>
        <xsl:when test="$first='papir'">papírenství</xsl:when>
        <xsl:when test="$first='pedol'">pedol.</xsl:when>
        <xsl:when test="$first='piv'">piv.</xsl:when>
        <xsl:when test="$first='politadm'">polit. a adm.</xsl:when>
        <xsl:when test="$first='politeko'">polit. ekon.</xsl:when>
        <xsl:when test="$first='potrobch'">potr. obch.</xsl:when>
        <xsl:when test="$first='hipr'">práv. hist.</xsl:when>
        <xsl:when test="$first='publ'">publicistika</xsl:when>
        <xsl:when test="$first='rybar'">rybářství</xsl:when>
        <xsl:when test="$first='sdel'">sdělovací technika</xsl:when>
        <xsl:when test="$first='sport'">sport</xsl:when>
        <xsl:when test="$first='sporpub'">sportovní publicistika</xsl:when>
        <xsl:when test="$first='stroj'">strojírenství</xsl:when>
        <xsl:when test="$first='tanec'">tanec</xsl:when>
        <xsl:when test="$first='teol'">teol.</xsl:when>
        <xsl:when test="$first='tesn'">těsn.</xsl:when>
        <xsl:when test="$first='textobch'">text. obch.</xsl:when>
        <xsl:when test="$first='umel'">umělecký</xsl:when>
        <xsl:when test="$first='umrem'">uměl. řem.</xsl:when>
        <xsl:when test="$first='vcel'">včel.</xsl:when>
        <xsl:when test="$first='vinar'">vinařství</xsl:when>
        <xsl:when test="$first='vodohos'">vodohospodářství</xsl:when>
        <xsl:when test="$first='vojhist'">voj. hist.</xsl:when>
        <xsl:when test="$first='vojnam'">voj. nám.</xsl:when>
        <xsl:when test="$first='vor'">vorařský</xsl:when>
        <xsl:when test="$first='zeldop'">železniční doprava</xsl:when>
        <xsl:when test="$first='zpravpubl'">zprav. publ.</xsl:when>
        <xsl:when test="$first='zurn'">žurn.</xsl:when>
      </xsl:choose>    
      <xsl:if test="$rest">, </xsl:if>
    </xsl:if>
    <xsl:if test='$rest'>
      <xsl:call-template name='split_oblast'>
        <xsl:with-param name='text' select='$rest'/>
      </xsl:call-template>
    </xsl:if>
    <xsl:if test='not($rest)'>
      <xsl:choose>
        <xsl:when test="$text='anat'">anatomie</xsl:when>
        <xsl:when test="$text='antr'">antropologie</xsl:when>
        <xsl:when test="$text='archeol'">archeologie</xsl:when>
        <xsl:when test="$text='archit'">architektura</xsl:when>
        <xsl:when test="$text='biol'">biologie</xsl:when>
        <xsl:when test="$text='bot'">botanika</xsl:when>
        <xsl:when test="$text='dipl'">diplomacie</xsl:when>
        <xsl:when test="$text='div'">divadelnictví</xsl:when>
        <xsl:when test="$text='dopr'">doprava</xsl:when>
        <xsl:when test="$text='ekol'">ekologie</xsl:when>
        <xsl:when test="$text='ekon'">ekonomie</xsl:when>
        <xsl:when test="$text='eltech'">elektrotechnika</xsl:when>
        <xsl:when test="$text='etn'">etnografie</xsl:when>
        <xsl:when test="$text='feud'">feudalismus</xsl:when>
        <xsl:when test="$text='filat'">filatelie</xsl:when>
        <xsl:when test="$text='film'">filmařství</xsl:when>
        <xsl:when test="$text='filoz'">filozofie</xsl:when>
        <xsl:when test="$text='fot'">fotografování</xsl:when>
        <xsl:when test="$text='fyz'">fyzika</xsl:when>
        <xsl:when test="$text='fyziol'">fyziologie</xsl:when>
        <xsl:when test="$text='geol'">geologie</xsl:when>
        <xsl:when test="$text='geom'">geometrie</xsl:when>
        <xsl:when test="$text='gnoz'">gnozeologie</xsl:when>
        <xsl:when test="$text='hist'">historie</xsl:when>
        <xsl:when test="$text='horn'">hornictví</xsl:when>
        <xsl:when test="$text='horol'">horolezectví</xsl:when>
        <xsl:when test="$text='hosp'">hospodářství</xsl:when>
        <xsl:when test="$text='hud'">hudební věda</xsl:when>
        <xsl:when test="$text='hut'">hutnictví</xsl:when>
        <xsl:when test="$text='hvězd'">astronomie</xsl:when>
        <xsl:when test="$text='chem'">chemie</xsl:when>
        <xsl:when test="$text='ideal'">idealismus</xsl:when>
        <xsl:when test="$text='informatika'">informatika</xsl:when>
        <xsl:when test="$text='jad'">jaderná fyzika</xsl:when>
        <xsl:when test="$text='jaz'">jazykověda</xsl:when>
        <xsl:when test="$text='kapit'">kapitalismus</xsl:when>
        <xsl:when test="$text='karet'">karetní hra</xsl:when>
        <xsl:when test="$text='katol církvi'">katolictví</xsl:when>
        <xsl:when test="$text='krim'">kriminalistika</xsl:when>
        <xsl:when test="$text='křesť'">křesťanství</xsl:when>
        <xsl:when test="$text='kuch'">kuchařství</xsl:when>
        <xsl:when test="$text='kult'">kultura</xsl:when>
        <xsl:when test="$text='kyb'">kybernetika</xsl:when>
        <xsl:when test="$text='lék'">lékařství</xsl:when>
        <xsl:when test="$text='lékár'">lékárnictví</xsl:when>
        <xsl:when test="$text='let'">letectví</xsl:when>
        <xsl:when test="$text='liter'">literární věda</xsl:when>
        <xsl:when test="$text='log'">logika</xsl:when>
        <xsl:when test="$text='marx'">marxismus</xsl:when>
        <xsl:when test="$text='mat'">matematika</xsl:when>
        <xsl:when test="$text='meteor'">meteorologie</xsl:when>
        <xsl:when test="$text='miner'">mineralogie</xsl:when>
        <xsl:when test="$text='motor'">motorismus</xsl:when>
        <xsl:when test="$text='mysl'">myslivecký výraz</xsl:when>
        <xsl:when test="$text='mytol'">mytologie</xsl:when>
        <xsl:when test="$text='náb'">náboženství</xsl:when>
        <xsl:when test="$text='nár'">národopis</xsl:when>
        <xsl:when test="$text='obch'">obchod</xsl:when>
        <xsl:when test="$text='pedag'">pedagogika</xsl:when>
        <xsl:when test="$text='peněž'">peněžnictví</xsl:when>
        <xsl:when test="$text='podnikani'">podnikání</xsl:when>
        <xsl:when test="$text='polit'">politika</xsl:when>
        <xsl:when test="$text='polygr'">polygrafie</xsl:when>
        <xsl:when test="$text='pošt'">poštovnictví</xsl:when>
        <xsl:when test="$text='potrav'">potravinářství</xsl:when>
        <xsl:when test="$text='práv'">právo</xsl:when>
        <xsl:when test="$text='prům'">průmysl</xsl:when>
        <xsl:when test="$text='přír'">příroda</xsl:when>
        <xsl:when test="$text='psych'">psychologie</xsl:when>
        <xsl:when test="$text='rybn'">rybníkářství</xsl:when>
        <xsl:when test="$text='řem'">řemeslo</xsl:when>
        <xsl:when test="$text='sklář'">sklářství</xsl:when>
        <xsl:when test="$text='soc'">socialismus</xsl:when>
        <xsl:when test="$text='sociol'">sociologie</xsl:when>
        <xsl:when test="$text='stat'">statistika</xsl:when>
        <xsl:when test="$text='stav'">stavitelství</xsl:when>
        <xsl:when test="$text='škol'">školství</xsl:when>
        <xsl:when test="$text='tech'">technika</xsl:when>
        <xsl:when test="$text='těl'">sport</xsl:when>
        <xsl:when test="$text='text'">textilnictví</xsl:when>
        <xsl:when test="$text='úč'">účetnictví</xsl:when>
        <xsl:when test="$text='úř'">úřední výraz</xsl:when>
        <xsl:when test="$text='veř spr'">veřejná správa</xsl:when>
        <xsl:when test="$text='vet'">veterinářství</xsl:when>
        <xsl:when test="$text='voj'">vojenství</xsl:when>
        <xsl:when test="$text='výp tech'">výpočetní technika</xsl:when>
        <xsl:when test="$text='výr'">výroba</xsl:when>
        <xsl:when test="$text='výtv'">výtvarnictví</xsl:when>
        <xsl:when test="$text='zahr'">zahradnictví</xsl:when>
        <xsl:when test="$text='zbož'">zbožíznalství</xsl:when>
        <xsl:when test="$text='zeměd'">zemědělství</xsl:when>
        <xsl:when test="$text='zeměp'">zeměpis</xsl:when>
        <xsl:when test="$text='zool'">zoologie</xsl:when>
        <xsl:when test="$text='cirkev'">církev</xsl:when>
        <xsl:when test="$text='adm'">administrativa</xsl:when>
        <xsl:when test="$text='artistika'">artistika</xsl:when>
        <xsl:when test="$text='astro'">astronomie</xsl:when>
        <xsl:when test="$text='biblhist'">bibl. hist.</xsl:when>
        <xsl:when test="$text='bibl'">biblistika</xsl:when>
        <xsl:when test="$text='biochemie'">biochemie</xsl:when>
        <xsl:when test="$text='cirar'">círk. archit.</xsl:when>
        <xsl:when test="$text='cirhi'">církevní historie</xsl:when>
        <xsl:when test="$text='cirhu'">círk. hud.</xsl:when>
        <xsl:when test="$text='cirkr'">círk. křesť.</xsl:when>
        <xsl:when test="$text='cirpol'">círk. polit.</xsl:when>
        <xsl:when test="$text='cirpr'">círk. práv.</xsl:when>
        <xsl:when test="$text='cukr'">cukrovarnictví</xsl:when>
        <xsl:when test="$text='ekpub'">ekon. publ.</xsl:when>
        <xsl:when test="$text='eltech'">elektr.</xsl:when>
        <xsl:when test="$text='estet'">estetika</xsl:when>
        <xsl:when test="$text='farmak'">farmakologie</xsl:when>
        <xsl:when test="$text='folklor'">folkloristika</xsl:when>
        <xsl:when test="$text='fonetika'">fonetika</xsl:when>
        <xsl:when test="$text='fyzchem'">fyzikální chemie</xsl:when>
        <xsl:when test="$text='genetika'">genetika</xsl:when>
        <xsl:when test="$text='geochem'">geochem.</xsl:when>
        <xsl:when test="$text='geodez'">geodézie</xsl:when>
        <xsl:when test="$text='geofyz'">geofyzika</xsl:when>
        <xsl:when test="$text='geogr'">geografie</xsl:when>
        <xsl:when test="$text='geolzem'">geol. zeměd.</xsl:when>
        <xsl:when test="$text='hiadm'">historie administrativy</xsl:when>
        <xsl:when test="$text='hiar'">historie architektury</xsl:when>
        <xsl:when test="$text='hiast'">historie astronomie</xsl:when>
        <xsl:when test="$text='hidip'">historie diplomacie</xsl:when>
        <xsl:when test="$text='hidiv'">historie divadelnictví</xsl:when>
        <xsl:when test="$text='hieko'">historie ekonomie</xsl:when>
        <xsl:when test="$text='hietno'">historie etnografie</xsl:when>
        <xsl:when test="$text='hifil'">historie filozofie</xsl:when>
        <xsl:when test="$text='hifot'">historie fotografování</xsl:when>
        <xsl:when test="$text='higeo'">historie geografie</xsl:when>
        <xsl:when test="$text='hilit'">historie literatury</xsl:when>
        <xsl:when test="$text='himat'">historie matematika</xsl:when>
        <xsl:when test="$text='hinab'">historie náboženství</xsl:when>
        <xsl:when test="$text='hipen'">historie peněžnictví</xsl:when>
        <xsl:when test="$text='hipol'">historie politiky</xsl:when>
        <xsl:when test="$text='hipr'">historie práva</xsl:when>
        <xsl:when test="$text='hisk'">historie školství</xsl:when>
        <xsl:when test="$text='hispo'">historie sport</xsl:when>
        <xsl:when test="$text='hista'">historie stavitelství</xsl:when>
        <xsl:when test="$text='hitech'">historie techniky</xsl:when>
        <xsl:when test="$text='hivoj'">historie vojenství</xsl:when>
        <xsl:when test="$text='sach'">hra v šachy</xsl:when>
        <xsl:when test="$text='hudpubl'">hudební publicistika</xsl:when>
        <xsl:when test="$text='jadtech'">jad. tech.</xsl:when>
        <xsl:when test="$text='keram'">keramika</xsl:when>
        <xsl:when test="$text='kniho'">knihovnictví</xsl:when>
        <xsl:when test="$text='kosmet'">kosmet.</xsl:when>
        <xsl:when test="$text='kosmon'">kosmonautika</xsl:when>
        <xsl:when test="$text='kozed'">kožedělný průmysl</xsl:when>
        <xsl:when test="$text='krejc'">krejčovství</xsl:when>
        <xsl:when test="$text='kynol'">kynologie</xsl:when>
        <xsl:when test="$text='lesnic'">lesnictví</xsl:when>
        <xsl:when test="$text='lethist'">let. hist.</xsl:when>
        <xsl:when test="$text='lingv'">lingvistika</xsl:when>
        <xsl:when test="$text='liter'">liter. hist.</xsl:when>
        <xsl:when test="$text='lodar'">loďařství</xsl:when>
        <xsl:when test="$text='matlog'">mat. log.</xsl:when>
        <xsl:when test="$text='mezobch'">mezinár. obch.</xsl:when>
        <xsl:when test="$text='mezpr'">mezinárodní právo</xsl:when>
        <xsl:when test="$text='nabpol'">náb. polit.</xsl:when>
        <xsl:when test="$text='nabps'">náb. ps.</xsl:when>
        <xsl:when test="$text='namor'">námořnictví</xsl:when>
        <xsl:when test="$text='numiz'">numizmatika</xsl:when>
        <xsl:when test="$text='obchpr'">obch. práv.</xsl:when>
        <xsl:when test="$text='obuv'">obuvnictví</xsl:when>
        <xsl:when test="$text='odb'">odb.</xsl:when>
        <xsl:when test="$text='optika'">optika</xsl:when>
        <xsl:when test="$text='orient'">orient.</xsl:when>
        <xsl:when test="$text='paleo'">paleontologie</xsl:when>
        <xsl:when test="$text='papir'">papírenství</xsl:when>
        <xsl:when test="$text='pedol'">pedol.</xsl:when>
        <xsl:when test="$text='piv'">piv.</xsl:when>
        <xsl:when test="$text='politadm'">polit. a adm.</xsl:when>
        <xsl:when test="$text='politeko'">polit. ekon.</xsl:when>
        <xsl:when test="$text='potrobch'">potr. obch.</xsl:when>
        <xsl:when test="$text='hipr'">práv. hist.</xsl:when>
        <xsl:when test="$text='publ'">publicistika</xsl:when>
        <xsl:when test="$text='rybar'">rybářství</xsl:when>
        <xsl:when test="$text='sdel'">sdělovací technika</xsl:when>
        <xsl:when test="$text='sport'">sport</xsl:when>
        <xsl:when test="$text='sporpub'">sportovní publicistika</xsl:when>
        <xsl:when test="$text='stroj'">strojírenství</xsl:when>
        <xsl:when test="$text='tanec'">tanec</xsl:when>
        <xsl:when test="$text='teol'">teol.</xsl:when>
        <xsl:when test="$text='tesn'">těsn.</xsl:when>
        <xsl:when test="$text='textobch'">text. obch.</xsl:when>
        <xsl:when test="$text='umel'">umělecký</xsl:when>
        <xsl:when test="$text='umrem'">uměl. řem.</xsl:when>
        <xsl:when test="$text='vcel'">včel.</xsl:when>
        <xsl:when test="$text='vinar'">vinařství</xsl:when>
        <xsl:when test="$text='vodohos'">vodohospodářství</xsl:when>
        <xsl:when test="$text='vojhist'">voj. hist.</xsl:when>
        <xsl:when test="$text='vojnam'">voj. nám.</xsl:when>
        <xsl:when test="$text='vor'">vorařský</xsl:when>
        <xsl:when test="$text='zeldop'">železniční doprava</xsl:when>
        <xsl:when test="$text='zpravpubl'">zprav. publ.</xsl:when>
        <xsl:when test="$text='zurn'">žurn.</xsl:when>
      </xsl:choose>    
    </xsl:if>
  </xsl:template>

  <xsl:template match="variant" mode="ingram">
  </xsl:template>

  <xsl:template match="variant">
    <xsl:if test="./@lemma_id!=''">
      <a href="/{@target}?action=search&amp;getdoc={@lemma_id}"><xsl:value-of select="@title"/></a>
    </xsl:if>
    <xsl:if test="./@lemma_id='' or not(./@lemma_id)">
      <xsl:value-of select="."/>
    </xsl:if>
    <xsl:if test="position()!=last()">, </xsl:if>
  </xsl:template>

  <xsl:template match="revcolloc">
    <br/>
    <!--<div class="czj-lemma-preview-sm">-->
      <!--<div class="czj-lemma-preview-sm-header">
        <div class="alignleft"></div>
        <div class="alignright"><i class="fa fa-external-link"></i></div> 
      </div>-->  

      <span style="vertical-align:top; display:inline-block;">
        <span onclick="open_iframe('{@lemma_id}', 'cs')" style="cursor:pointer;color:blue;text-decoration:underline">
          <!--<xsl:value-of select="@lemma_id"/>-->
          <img src="/media/img/slovnik-expand.png" height="20"/>
          <span class="colloctitle" style="vertical-align:top; display:inline-block;">
            <xsl:value-of select="title"/>
          </span>
        </span>
      </span>
      <div style="clear:both; margin-bottom: 5px;"></div>
    <!--</div>-->
    <iframe class="colloc-iframe" id="colloc{@lemma_id}" src="about:blank" style="display:none" onload="iframe_loaded('{@lemma_id}')"></iframe>
  </xsl:template>

  <xsl:template match="text">
    <xsl:apply-templates/>
  </xsl:template>
  <xsl:template match="text" mode="ingram">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="i">
    <i><xsl:apply-templates/></i>
  </xsl:template>
  <xsl:template match="i" mode="ingram">
    <i><xsl:apply-templates/></i>
  </xsl:template>

  <xsl:template match="small">
    <small><xsl:apply-templates/></small>
  </xsl:template>

  <xsl:template match="b">
    <b><xsl:apply-templates/></b>
  </xsl:template>

  <xsl:template match="sub">
    <sub><xsl:apply-templates/></sub>
  </xsl:template>

  <xsl:template match="sup">
    <sup><xsl:apply-templates/></sup>
  </xsl:template>
  <xsl:template match="small" mode="ingram">
    <small><xsl:apply-templates/></small>
  </xsl:template>

  <xsl:template match="b" mode="ingram">
    <b><xsl:apply-templates/></b>
  </xsl:template>

  <xsl:template match="sub" mode="ingram">
    <sub><xsl:apply-templates/></sub>
  </xsl:template>

  <xsl:template match="sup" mode="ingram">
    <sup><xsl:apply-templates/></sup>
  </xsl:template>

  <xsl:template match="link">
    <xsl:if test="not(link_mean)">
      <xsl:if test="@auto_complete='1' or $perm!='ro'"> <!--or $skupina_test='true'-->
      <a href="/{$target}?action=search&amp;getdoc={@lemma_id}"><xsl:value-of select="."/></a><!-- tady to funguje špatně -->
     </xsl:if>
     <xsl:if test="@auto_complete!='1' or $perm='ro' or $skupina_test!='true'">
      <xsl:value-of select="."/>
     </xsl:if>
    </xsl:if>
    <xsl:if test="link_mean">
      <xsl:value-of select="."/> 
      <xsl:if test="@auto_complete='1' or $perm!='ro' or $skupina_test='true'"> 
      (<xsl:for-each select="link_mean">
        <xsl:sort select="link_mean"/>
        <xsl:if test="@auto_complete='1' or $perm!='ro' or $skupina_test='true'">     <!-- implementovat do ilnine a EN -->
          <a href="/cs?action=search&amp;getdoc={../@lemma_id}">ve významu <xsl:value-of select="@number"/></a>
        </xsl:if>     
       <!--<xsl:if test="@auto_complete!='1' or $perm='ro'">ve významu <xsl:value-of select="@number"/>    
        </xsl:if> pokud není schválené heslo, toto se vůbec nezobrazí -->
      <xsl:if test="position()!=last()">, </xsl:if>
      </xsl:for-each><xsl:text>)</xsl:text>
    </xsl:if>
    </xsl:if>
  </xsl:template>
  <xsl:template match="link" mode="ingram">
    <xsl:if test="not(link_mean)">
      <a href="/cs?action=search&amp;getdoc={@lemma_id}"><xsl:value-of select="."/></a>
    </xsl:if>
    <xsl:if test="link_mean">
    <xsl:value-of select="."/>
      (<xsl:for-each select="link_mean">
       <xsl:sort select="link_mean"/>
        <a href="/cs?action=search&amp;getdoc={../@lemma_id}">ve významu <xsl:value-of select="@number"/></a>
        <xsl:if test="position()!=last()">, </xsl:if>
      </xsl:for-each><xsl:text>)</xsl:text>
    </xsl:if>
  </xsl:template>

  <xsl:template match="html//*">
    <xsl:copy>
      <xsl:apply-templates/>
    </xsl:copy>
  </xsl:template>
  <xsl:template name="status_publish">
    <xsl:param name="status"/>
    <xsl:param name="type"/>
    <xsl:param name="element"/>
    <xsl:if test="$status='published'">
      schváleno
    </xsl:if>
    <xsl:if test="$status!='published'">
       <!--skryto--><xsl:text> </xsl:text>
      <xsl:choose>
        <xsl:when test="$type='relation'">
          <input type="button" data-type="{$element/@type}" onclick="publish_relation('cs', '{/entry/@id}', '{$element/../@id}', '{$element/@meaning_id}', '{$element/@type}', this)" value="schválit"/>
        </xsl:when>
        <xsl:when test="$type='usage'">
          <input type="button" data-type="usage" onclick="publish_usage('cs', '{/entry/@id}', '{$element/../../@id}', '{$element/@id}', this)" value="schválit"/>
        </xsl:when>
        <xsl:when test="$type='meaning'">
          <input type="button" data-type="meaning" onclick="publish_meaning('cs', '{/entry/@id}', '{$element/@id}', this)" value="schválit"/>
        </xsl:when>
        <xsl:when test="$type='gram' or $type='style' or $type='sw' or $type='hns' or $type='video_front' or $type='video_side' or $type='entry'">
          <input type="button" data-type="entry" onclick="publish('cs', '{/entry/@id}', '{$type}', this)" value="schválit"/>
        </xsl:when>
      </xsl:choose>
    </xsl:if>
  </xsl:template>

</xsl:stylesheet>
