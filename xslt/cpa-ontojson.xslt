<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:RDF="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
  xmlns:d="http://nlp.fi.muni.cz/cpa/rdf#">

  <xsl:output encoding="utf-8" method="text"/>

  <xsl:template match="RDF:RDF">[<xsl:apply-templates select="/RDF:RDF/RDF:Seq/RDF:li"/>]</xsl:template>
  <xsl:template match="RDF:Seq">
    [
    <xsl:apply-templates select="RDF:li">
    </xsl:apply-templates>
    ]
    <xsl:if test="position()!=last()">,</xsl:if>
  </xsl:template>

  <xsl:template match="RDF:li">
    <xsl:param name="rr" />

    {
    <xsl:if test="@resource">
      <xsl:variable name="res" select="@resource"/>
      <xsl:apply-templates select='//RDF:Description[@about=$res]'/>

    </xsl:if>
    <xsl:if test="not(@resource)">
      <xsl:variable name="res0" select="RDF:Seq/@about"/>
              <xsl:apply-templates select='//RDF:Description[@about=$res0]'/>

    ,"sub":<xsl:apply-templates select="RDF:Seq"/>
    </xsl:if>
    }
    <xsl:if test="position()!=last()">,</xsl:if>
  </xsl:template>


  <!--  <xsl:template match="RDF:Seq">
    <xsl:if test="@about='urn:xmlslov:cpa:ontology'">
      <xsl:apply-templates select="RDF:li/RDF:Seq"/>
    </xsl:if>
    <xsl:if test="not(@about='urn:xmlslov:cpa:ontology')">
    <xsl:variable name="res0" select="@about"/>
    <xsl:apply-templates select='//RDF:Description[@about=$res0]'/>
    <xsl:if test="count(RDF:li)>0"> 
      <xsl:for-each select="RDF:li">
        <xsl:sort select="@resource"/>
        <xsl:if test="@resource">
          {
            <xsl:variable name="res" select="@resource"/>
            <xsl:apply-templates select='//RDF:Description[@about=$res]'/>
            }
        </xsl:if>
        <xsl:if test="not(@resource)">
          ,"sub":[            <xsl:apply-templates select="RDF:Seq"/>]
        </xsl:if>
      </xsl:for-each>
    
    </xsl:if>
    }
    <xsl:if test="position()!=last()">,</xsl:if>
  </xsl:if>
</xsl:template>
-->

  <xsl:template match="RDF:Description">
    "term":"<xsl:value-of select="d:term"/>",
    "comment":"<xsl:value-of select="d:comment"/>",
    "tid":"<xsl:value-of select="d:tid"/>",
    "prop":"<xsl:value-of select="d:prop"/>"
  </xsl:template>
</xsl:stylesheet>

