<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:rdf ="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
 xmlns:owl="http://www.w3.org/2002/07/owl#">
	<xsl:output encoding="utf-8"/>

  <xsl:template match="rdfs:Class">
    <p><span class="BLUE">Class: </span> <xsl:value-of select="@rdf:ID"/>
      <xsl:for-each select="rdfs:subClassOf">
        <br/><a target="_blank">
          <xsl:attribute name="href">/sumoowl?action=get_class&amp;transform=preview&amp;uri=<xsl:call-template name="replace">
          <xsl:with-param name="string" select="string(@rdf:resource)"/>
          <xsl:with-param name="pattern" select="'#'"/>
          <xsl:with-param name="replacement" select="''"/>
        </xsl:call-template></xsl:attribute>
          <xsl:value-of select="@rdf:resource"/></a>
      </xsl:for-each>
      <br/><i><xsl:value-of select="rdfs:comment"/></i>
    </p>
  </xsl:template>

  <xsl:template match="owl:ObjectProperty">
    <p><span class="BLUE">ObjectProperty: </span> <xsl:value-of select="@rdf:ID"/>
      <br/><i><xsl:value-of select="rdfs:comment"/></i>
      <xsl:for-each select="*[not(name()='rdfs:comment')]">
        <br/><xsl:value-of select="name()"/>: <a target="_blank">
          <xsl:attribute name="href">/sumoowl?action=get_class&amp;transform=preview&amp;uri=<xsl:call-template name="replace">
          <xsl:with-param name="string" select="string(@rdf:resource)"/>
          <xsl:with-param name="pattern" select="'#'"/>
          <xsl:with-param name="replacement" select="''"/>
        </xsl:call-template></xsl:attribute>
          <xsl:value-of select="@rdf:resource"/></a>
      </xsl:for-each>
    </p>
  </xsl:template>

  <xsl:template match="rdf:Description">
    <p><span class="BLUE">Description: </span> <xsl:value-of select="@rdf:ID"/>
      <xsl:for-each select="*[not(name()='rdfs:comment')]">
        <br/><xsl:value-of select="name()"/>: <a target="_blank">
          <xsl:attribute name="href">/sumoowl?action=get_class&amp;transform=preview&amp;uri=<xsl:call-template name="replace">
          <xsl:with-param name="string" select="string(@rdf:resource)"/>
          <xsl:with-param name="pattern" select="'#'"/>
          <xsl:with-param name="replacement" select="''"/>
        </xsl:call-template></xsl:attribute>
          <xsl:value-of select="@rdf:resource"/></a>
      </xsl:for-each>
      <br/><i><xsl:value-of select="rdfs:comment"/></i>
    </p>
  </xsl:template>


  <xsl:template name="replace">
    <xsl:param name="string" select="''"/>
    <xsl:param name="pattern" select="''"/>
    <xsl:param name="replacement" select="''"/>
    <xsl:choose>
      <xsl:when test="$pattern != '' and $string != '' and contains($string, $pattern)">
        <xsl:value-of select="substring-before($string, $pattern)"/>
        <!--
        Use "xsl:copy-of" instead of "xsl:value-of" so that users
        may substitute nodes as well as strings for $replacement.
        -->
        <xsl:copy-of select="$replacement"/>
        <xsl:call-template name="replace">
          <xsl:with-param name="string" select="substring-after($string, $pattern)"/>
          <xsl:with-param name="pattern" select="$pattern"/>
          <xsl:with-param name="replacement" select="$replacement"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$string"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

</xsl:stylesheet>

