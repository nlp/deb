<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output encoding="utf-8" />
  
  <xsl:template match="pattern_set">
    <html>
      <head>
        <title>PDEV entry for <xsl:value-of select="verb_stem"/></title>
        <style type="text/css">
        body {
          background: #ffffff;
          color: #000000;
        }
        .red {
          color: #ff0000;
        }
        h1, .green {
          color: #007700;
        }
        .darkred {
          color: #770000;
        }
        h3, .darkblue {
          color: #000088;
        }
        .head {
          font-size: 150%;
        }
        .indent {
          margin: 20px;
        }
        </style>
      </head>
      <body>
      <a href="doc?action=getdoc&amp;id={verb_stem}&amp;type=xml">XML</a>
      <h1>PDEV entry for <xsl:value-of select="verb_stem"/></h1>
      <xsl:for-each select="pattern" sort="@num">
        <p> <span class="darkblue"><b>PATTERN <xsl:value-of select="@num"/>:</b>
        <!--subject-->
        [[<xsl:value-of select="template/subject/argspec/BSO_type/@name"/>
        <xsl:apply-templates select="template/subject/argspec/subspec"/>
        <xsl:for-each select="template/subject/alternation/argspec">
          <xsl:text> | </xsl:text><xsl:value-of select="BSO_type/@name"/>
          <xsl:apply-templates select="subspec"/>
        </xsl:for-each>]]
        
        <xsl:value-of select="template/verb_form"/>
        <xsl:text> </xsl:text>

        <!--object-->
        <xsl:if test="(template/object/argspec/BSO_type/@name!='' or template/object/argspec/subspec/Lexset) and template/object/argspec/@none!='true'">
          <xsl:if test="template/object/argspec/@type='optional'">(</xsl:if>[[<xsl:value-of select="template/object/argspec/BSO_type/@name"/>
          <xsl:apply-templates select="template/object/argspec/subspec"/>
          <xsl:for-each select="template/object/alternation/argspec">
            <xsl:text> | </xsl:text><xsl:value-of select="BSO_type/@name"/>
            <xsl:apply-templates select="subspec"/>
          </xsl:for-each>]]<xsl:if test="template/object/argspec/@type='optional'">)</xsl:if>
        </xsl:if>
        <xsl:if test="template/object/argspec/@none='true'">[NO OBJ]</xsl:if>
       
        <xsl:text> </xsl:text>
        
        <!--adverbial-->
        <xsl:if test="template/@adverbial_none!='true'">
          <xsl:for-each select="template/adverbial">
            <xsl:if test="@type='optional'">(</xsl:if>{<xsl:value-of select="ptag/@headword"/>
            [[<xsl:value-of select="ptag/argspec/BSO_type/@name"/>]]}
            <xsl:if test="@type='optional'">)</xsl:if>
          </xsl:for-each>
        </xsl:if>
        <xsl:if test="template/@adverbial_none='true'">[NO ADVL]</xsl:if>

        <!--clausal-->
        <xsl:text> </xsl:text>
        <xsl:if test="template/clausals/@to='true'"> to/INF [V]</xsl:if>
        <xsl:if test="template/clausals/@ing='true'"> -ING</xsl:if>
        <xsl:if test="template/clausals/@that='true'"> that [CLAUSE]</xsl:if>
        <xsl:if test="template/clausals/@wh='true'"> WH- [CLAUSE]</xsl:if>
        <xsl:for-each select="template/clausals/clausal/argspec">
          [V[<xsl:value-of select="BSO_type/@name"/>
          <xsl:apply-templates select="subspec"/>]]
        </xsl:for-each>

        <!--complement-->
        <xsl:text> </xsl:text>
        <xsl:for-each select="template/comp">
          <xsl:if test='position() > 1'> | </xsl:if>
          <xsl:value-of select="text()"/>
        </xsl:for-each>
        
        </span><br/>
        <b></b> <xsl:value-of select="primary_implicature"/><br/>
        <xsl:if test="comment/text() != ''">
          <b>COMMENT: </b> <xsl:if test="comment/@type != ''"><xsl:value-of select="comment/@type"/>: </xsl:if><xsl:value-of select="comment"/><br/>
        </xsl:if>
        </p>

      </xsl:for-each>
      </body>
      </html>
  </xsl:template>

  <xsl:template match="subspec">
        <xsl:if test="Role">
          <xsl:text> = </xsl:text><xsl:value-of select="Role/@name"/>
        </xsl:if>
        <xsl:if test="Polarity">
          <xsl:if test="Role">
            <xsl:text> &amp; </xsl:text>
          </xsl:if>
          <xsl:if test="not(Role)">
            <xsl:text> = </xsl:text>
          </xsl:if>
          <xsl:value-of select="Polarity/@name"/>
        </xsl:if>
        <xsl:if test="Lexset">
          <xsl:if test="Role or Polarity">
            <xsl:text> &amp; </xsl:text>
          </xsl:if>
          <xsl:if test="not(Role) and not(Polarity)">
            <xsl:text> = </xsl:text>
          </xsl:if>
          LEXSET <xsl:value-of select="Lexset/@name"/> <xsl:value-of select="Lexset/item"/>
        </xsl:if>
  </xsl:template>

</xsl:stylesheet>

