<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="utf-8" method="xml"
  doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
  doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
  />

<xsl:template match="entry">
  <html>
     <head>
       <title>Urážky: <xsl:value-of select="h"/><xsl:text> </xsl:text> <xsl:value-of select="n"/></title>
      <style type="text/css">
      body {
      background: #ffffff;
      color: #000000;
      }
      .red {
      color: #ff0000;
      }
      .green {
      color: #007700;
      }
      .darkred {
      color: #770000;
      }
      .darkblue {
      color: #000088;
      }
      .head {
      font-size: 150%;
      }
      </style>
     </head>
     <body>
       <p><xsl:apply-templates/></p>
       <hr />
       <p><small>Slovník urážlivých slov</small></p>
     </body>
  </html>
</xsl:template>

<xsl:template match="h"><b class="red head"><xsl:value-of select="text()"/><sup><xsl:value-of select="//n"/></sup></b>
<br/></xsl:template>
<xsl:template match="exp"><i class="darkred"><xsl:value-of select="text()"/></i></xsl:template>
<xsl:template match="n"></xsl:template>
<xsl:template match="exam"><br/><span class="green"><xsl:apply-templates/></span></xsl:template>
<xsl:template match="w"><i><xsl:value-of select="text()"/></i></xsl:template>

<xsl:template match="ssjc">
  <xsl:if test="text()!=''">
    <br/>SSJČ: <a href="/ssjc?action=getdoc&amp;tr=ssjc&amp;id={//h}"><xsl:value-of select="text()"/></a>
    <!--<br/>SSJČ: <a href="proxy.php?dict=ssjc&amp;action=getdoc&amp;tr=ssjc&amp;id={//h}"><xsl:value-of select="text()"/></a>-->
  </xsl:if>
</xsl:template>
<xsl:template match="ssc">
  <xsl:if test="text()!=''">
    <br/>SSČ: <a href="/ssc?action=getdoc&amp;tr=ssc&amp;id={//h}"><xsl:value-of select="text()"/></a>
    <!--<br/>SSČ: <a href="proxy.php?dict=ssc&amp;action=getdoc&amp;tr=ssc&amp;id={//h}"><xsl:value-of select="text()"/></a>-->
  </xsl:if>
</xsl:template>
<xsl:template match="snc">
  <xsl:if test="text()!=''">
    <br/>SNČ: <xsl:value-of select="text()"/>
  </xsl:if>
</xsl:template>

</xsl:stylesheet>

