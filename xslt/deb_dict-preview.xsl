<?xml version='1.0' encoding='utf-8'?>
<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'><xsl:template match='/entry'><html><head><meta content='text/html[-semicolon-] charset=utf-8' http-equiv='Content-Type'/><title>dict: <xsl:value-of select="null"/> (DEBWrite)</title></head>
<body><h1><xsl:value-of select="null"/></h1>
<xsl:apply-templates/></body></html></xsl:template>
</xsl:stylesheet>
