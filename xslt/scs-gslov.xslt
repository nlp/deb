<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="utf-8" method="xml" omit-xml-declaration="yes"/>

<xsl:template match="root">
      <p><xsl:apply-templates/></p>
</xsl:template>

<xsl:template match="h">
      <b class="red head"><xsl:value-of select="."/></b><br/><br/>
</xsl:template>

<xsl:template match="ref">
  viz <a class="blue" href="scs?action=getdoc&amp;id={text()}&amp;tr=scs"><xsl:value-of select="."/></a>
</xsl:template>

<xsl:template match="t">
      <xsl:value-of select="."/>
</xsl:template>

</xsl:stylesheet>

