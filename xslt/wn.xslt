<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="utf-8"/>

<xsl:template match="SYNSET">
  <html>
    <head>
    <!--<link rel="stylesheet" type="text/css" href="chrome://lexanek/content/lexanek.css"/>-->
    <style type="text/css"></style> <!--tento tag tu musi byt vzdy a musi byt prvy-->
    </head>
    <body>
&lt;<span class="red2"><xsl:value-of select="name()"/></span>&gt;<xsl:apply-templates/><br/>&lt;<span class="red2">/<xsl:value-of select="name()"/></span>&gt;
</body>
</html>
</xsl:template>

<xsl:template match="*">
<br/>&lt;<span class="red2"><xsl:value-of select="name()"/></span>&gt;<span class="blue"><xsl:value-of select="text()"/></span><xsl:apply-templates select="./*"/>&lt;<span class="red2">/<xsl:value-of select="name()"/></span>&gt;
</xsl:template>

<xsl:template match="SYNONYM">
<br/>&lt;<span class="red2"><xsl:value-of select="name()"/></span>&gt;<span class="blue"><xsl:value-of select="text()"/></span><xsl:apply-templates select="./*"/><br/>&lt;<span class="red2">/<xsl:value-of select="name()"/></span>&gt;
</xsl:template>

<xsl:template match="WORD"/>
<xsl:template match="LITERAL">
<br/>&lt;<span class="red2"><xsl:value-of select="name()"/></span>&gt;<span class="blue"><xsl:value-of select="text()"/></span><xsl:apply-templates select="./*"/>
&lt;<span class="red2">SENSE</span>&gt;<span class="blue"><xsl:value-of select="@sense"/></span>&lt;<span class="red2">/SENSE</span>&gt;
<xsl:if test="@lnote">&lt;<span class="red2">LNOTE</span>&gt;<span class="blue"><xsl:value-of select="@lnote"/></span>&lt;<span class="red2">/LNOTE</span>&gt;</xsl:if>
<xsl:apply-templates select="./*"/>&lt;<span class="red2">/<xsl:value-of select="name()"/></span>&gt;
</xsl:template>
<xsl:template match="ILR">
<br/>&lt;<span class="red2"><xsl:value-of select="name()"/></span>&gt;<span class="blue"><xsl:value-of select="text()"/></span><xsl:apply-templates select="./*"/>
&lt;<span class="red2">TYPE</span>&gt;<span class="blue"><xsl:value-of select="@type"/></span>&lt;<span class="red2">/TYPE</span>&gt;
<xsl:apply-templates select="./*"/>&lt;<span class="red2">/<xsl:value-of select="name()"/></span>&gt;
</xsl:template>

</xsl:stylesheet>

