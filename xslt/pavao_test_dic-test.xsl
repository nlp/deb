<?xml version='1.0' encoding='utf-8'?>
<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'><xsl:template match='/entry'><html><head><meta content='text/html[-semicolon-] charset=utf-8' http-equiv='Content-Type'/><title>Test dictionary: <xsl:value-of select="hw"/> (DEBWrite)</title><style type="text/css">.hw{color: blue}
.type_container {border: 1px solid #000}
.type_container {background-color:#eee}
</style>
</head>
<body><h1><xsl:value-of select="hw"/></h1>
<xsl:apply-templates/></body></html></xsl:template>
<xsl:template name="file"><xsl:param name="file_element"/><br/><xsl:if test="starts-with($file_element/@mime, 'image')"><img src="/files/pavao_test_dic/{.}" style="max-width:400px"/></xsl:if><xsl:if test="starts-with($file_element/@mime, 'audio')"><audio src="/files/pavao_test_dic/{.}" controls="true"/></xsl:if><xsl:if test="starts-with($file_element/@mime, 'video')"><video src="/files/pavao_test_dic/{.}" controls="true" style="max-width:400px"/></xsl:if><br/></xsl:template>

<xsl:template match="hw"><span class="hw type_text">headword: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="pos"><span class="pos type_select">part of speech: <span style="font-family:verdana"><span style="font-style:italic"><xsl:apply-templates/></span></span></span><br/></xsl:template>
<xsl:template match="area"><span class="area type_select">area: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="meaning"><div class="meaning type_container">meaning: <xsl:apply-templates/></div><br/></xsl:template>
<xsl:template match="def"><span class="def type_textarea">definition: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="mention"><div class="mention type_container">mentions: <xsl:apply-templates/></div><br/></xsl:template>
<xsl:template match="west"><span class="west type_textarea">west: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="south"><span class="south type_textarea">south: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="east"><span class="east type_textarea">east: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="etym"><span class="etym type_text">etymology: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="reference"><span class="reference type_textarea">references: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="comment"><span class="comment type_textarea">comments: <xsl:apply-templates/></span><br/></xsl:template>
</xsl:stylesheet>
