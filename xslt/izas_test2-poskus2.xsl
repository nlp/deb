<?xml version='1.0' encoding='utf-8'?>
<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>
<xsl:template match='/entry'>
<html><head><meta content='text/html; charset=utf-8' http-equiv='Content-Type'/><title>test2: <xsl:value-of select="lex"/> (DEBWrite)</title></head><body>
<h1><xsl:value-of select="lex"/></h1>
<xsl:apply-templates/></body></html></xsl:template>

<xsl:template match="lex"><span class="lex">iztočnica: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="cat"><span class="cat">leksikalna kategorija: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="var"><span class="var">variantni zapis: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="def"><span class="def">pomenski opis: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="tra"><span class="tra">prevodni del: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="ex"><span class="ex">zgled: <xsl:apply-templates/></span><br/></xsl:template>
</xsl:stylesheet>
