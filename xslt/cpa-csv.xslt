<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:RDF="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
  xmlns:d="http://nlp.fi.muni.cz/cpa/rdf#">

  <xsl:output encoding="utf-8" type="xml" method="text"/>

  <xsl:template match="RDF:RDF">
        <xsl:apply-templates select="RDF:Seq"/>
  </xsl:template>

  <xsl:template match="RDF:Seq">
    <!--<xsl:value-of select="@about"/>-->
    <xsl:variable name="res0" select="@about"/>
    <xsl:apply-templates select='//RDF:Description[@about=$res0]'/>
    <xsl:if test="count(RDF:li)>0">
    <ul>
      <xsl:for-each select="RDF:li">
        <xsl:sort select="@resource"/>
        <xsl:if test="@resource">
          <li>
            <!--<xsl:value-of select="@resource"/>-->
            <xsl:variable name="res" select="@resource"/>
            <xsl:apply-templates select='//RDF:Description[@about=$res]'/>
          </li>
        </xsl:if>
        <xsl:if test="not(@resource)">
          <li>
            <xsl:apply-templates select="RDF:Seq"/>
          </li>
        </xsl:if>
      </xsl:for-each>
    </ul>
  </xsl:if>
  </xsl:template>

  <xsl:template match="RDF:Description">
        <xsl:value-of select="d:entry_label"/>;
        <xsl:value-of select="d:patterns"/>;
        <xsl:value-of select="d:oec_freq"/>;
        <xsl:value-of select="d:bnc_freq"/>;
        <xsl:value-of select="d:bnc50_freq"/>;
        <xsl:value-of select="d:create_by"/>;<xsl:value-of select="d:create"/>;
        <xsl:value-of select="d:edit_by"/>; <xsl:value-of select="d:last_edit"/>;
        <xsl:value-of select="d:sample_size"/>
  </xsl:template>
</xsl:stylesheet>

