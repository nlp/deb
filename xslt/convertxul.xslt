<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xslo="http://www.w3.org/1999/XSL/TransformAlias"
  xmlns="http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul"
  >
  <xsl:output encoding="utf-8" method="xml" indent="yes"/>
  <xsl:include href="convert.xslt"/>
  <xsl:namespace-alias stylesheet-prefix="xslo" result-prefix="xsl"/>

  <xsl:param name="dictcode">dict</xsl:param>
  <xsl:param name="dictname">dict</xsl:param>
  <xsl:param name="index_path">/entry</xsl:param>

  <xsl:template match="/element">
    <xslo:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
      <xslo:output encoding="utf-8" method="xml" indent="yes"/>

      <xslo:template match="{@name}">
        <xslo:processing-instruction name="xml-stylesheet">href="/files/<xsl:value-of select="$dictcode"/>xul.css" type="text/css"</xslo:processing-instruction>
        <window	orient="vertical" 
          title="{$dictname} Edit entry " 
          xmlns="http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul"
          id="{$dictcode}edit"
          onload="edit_onload();">

          <script type="application/x-javascript; e4x=1" src="/files/{$dictcode}.js" />
          <script type="application/x-javascript">
            <xsl:for-each select="oneOrMore/*/text|zeroOrMore/*/text|oneOrMore/*/choice|zeroOrMore/*/choice">
              id['<xsl:call-template name="get_id"/>'] = <xslo:value-of>
                <xsl:attribute name="select">count(<xsl:call-template name="get_path"/>)+2</xsl:attribute>
              </xslo:value-of>;
            </xsl:for-each>
          </script>

          <label id="entry_id">
            <xsl:attribute name="value">{<xsl:value-of select="$index_path"/>}</xsl:attribute>
          </label>

          <grid flex="0">
            <cols>
              <col/>
              <col flex="0"/>
            </cols>
            <rows>
              <xsl:apply-templates select="//text|//choice"/>
            </rows>
          </grid>

          <hbox>
            <button oncommand="save_xml()" label="Save" flex="0"/>
            <spacer style="width:100px"/>
            <button oncommand="open_list()" label="Back to list" flex="0"/>
          </hbox>
        </window>
      </xslo:template>
    </xslo:stylesheet>
  </xsl:template>

  <xsl:template match="text">
    <row>
      <xsl:if test="name(../.)='attribute'">
        <label value="@{../@name}"/>
      </xsl:if>
      <xsl:if test="name(../.)='element'">
        <label value="{../@name}"/>
      </xsl:if>

      <!--only one field-->
      <xsl:if test="name(../../.)!='oneOrMore' and name(../../.)!='zeroOrMore'">
        <xsl:if test="../@ref">
          <menulist flex="1" editable="true" class="class_{../@name}" onkeypress="if (event.target.nodeName == 'menulist' &amp;&amp; event.keyCode == event.DOM_VK_RETURN) find_entry(this, '{../@ref}')">
            <xsl:attribute name="id"><xsl:call-template name="get_id"/></xsl:attribute>
            <menupopup>
              <menuitem>
                <xsl:attribute name="label">{<xsl:call-template name="get_path"/>}</xsl:attribute>
                <xsl:attribute name="value">{<xsl:call-template name="get_path"/>}</xsl:attribute>
              </menuitem>
            </menupopup>
          </menulist>
        </xsl:if>
        <xsl:if test="not(../@ref)">
          <textbox class="class_{../@name}">
              <xsl:attribute name="id"><xsl:call-template name="get_id"/></xsl:attribute>
            <xsl:attribute name="value">{<xsl:call-template name="get_path"/>}</xsl:attribute>
          </textbox>
        </xsl:if>
      </xsl:if>

      <!--multiple fields-->
      <xsl:if test="name(../../.)='oneOrMore' or name(../../.)='zeroOrMore'">
        <vbox>
          <xsl:attribute name="id">box<xsl:call-template name="get_id"/></xsl:attribute>

          <xslo:for-each>
            <xsl:attribute name="select"><xsl:call-template name="get_path"/></xsl:attribute>
            <xslo:if test="position()=1">
              <hbox>
                <xsl:attribute name="id">hb<xsl:call-template name="get_id"/>1</xsl:attribute>

                <xsl:if test="../@ref">
                  <menulist flex="1" editable="true" class="class_{../@name}" onkeypress="if (event.target.nodeName == 'menulist' &amp;&amp; event.keyCode == event.DOM_VK_RETURN) find_entry(this, '{../@ref}')">
                    <xsl:attribute name="id"><xsl:call-template name="get_id"/>1</xsl:attribute>
                    <menupopup>
                      <menuitem>
                        <xsl:attribute name="value">{.}</xsl:attribute>
                        <xsl:attribute name="label">{.}</xsl:attribute>
                      </menuitem>
                    </menupopup>
                  </menulist>
                  <button label="+">
                    <xsl:attribute name="oncommand">addlist('box<xsl:call-template name="get_id"/>','<xsl:call-template name="get_id"/>','bef<xsl:call-template name="get_id"/>', '<xsl:value-of select="../@ref"/>');</xsl:attribute>
                  </button>
                </xsl:if>
                <xsl:if test="not(../@ref)">
                  <textbox class="class_{../@name}" >
                    <xsl:attribute name="id"><xsl:call-template name="get_id"/>1</xsl:attribute>
                    <xsl:attribute name="value">{.}</xsl:attribute>
                  </textbox>
                  <button label="+">
                    <xsl:attribute name="oncommand">add('box<xsl:call-template name="get_id"/>','<xsl:call-template name="get_id"/>','bef<xsl:call-template name="get_id"/>');</xsl:attribute>
                  </button>
                </xsl:if>
              </hbox> 
            </xslo:if>
            <xslo:if test="position()&gt;1">
              <hbox>
                <xsl:attribute name="id">hb<xsl:call-template name="get_id"/>{position()}</xsl:attribute>

                <xsl:if test="../@ref">
                  <menulist flex="1" editable="true" class="class_{../@name}" onkeypress="if (event.target.nodeName == 'menulist' &amp;&amp; event.keyCode == event.DOM_VK_RETURN) find_entry(this, '{../@ref}')">
                    <xsl:attribute name="id"><xsl:call-template name="get_id"/>{position()}</xsl:attribute>
                    <menupopup>
                      <menuitem>
                        <xsl:attribute name="value">{.}</xsl:attribute>
                        <xsl:attribute name="label">{.}</xsl:attribute>
                      </menuitem>
                    </menupopup>
                  </menulist>
                </xsl:if>
                <xsl:if test="not(../@ref)">
                  <textbox class="class_{../@name}">
                    <xsl:attribute name="id"><xsl:call-template name="get_id"/>{position()}</xsl:attribute>
                    <xsl:attribute name="value">{.}</xsl:attribute>
                  </textbox>
                </xsl:if>
                <button label="-">
                  <xsl:attribute name="oncommand">remove('hb<xsl:call-template name="get_id"/>{position()}');</xsl:attribute>
                </button>
              </hbox> 
            </xslo:if>
          </xslo:for-each>
            <xslo:if>
              <xsl:attribute name="test">count(<xsl:call-template name="get_path"/>)=0</xsl:attribute>
              <hbox>
                <xsl:attribute name="id">hb<xsl:call-template name="get_id"/>1</xsl:attribute>

                <xsl:if test="../@ref">
                  <menulist flex="1" editable="true" class="class_{../@name}" onkeypress="if (event.target.nodeName == 'menulist' &amp;&amp; event.keyCode == event.DOM_VK_RETURN) find_entry(this, '{../@ref}')">
                    <xsl:attribute name="id"><xsl:call-template name="get_id"/>1</xsl:attribute>
                    <menupopup>
                    </menupopup>
                  </menulist>
                  <button label="+">
                    <xsl:attribute name="oncommand">addlist('box<xsl:call-template name="get_id"/>','<xsl:call-template name="get_id"/>','bef<xsl:call-template name="get_id"/>', '<xsl:value-of select="../@ref"/>');</xsl:attribute>
                  </button>
                </xsl:if>
                <xsl:if test="not(../@ref)">
                  <textbox class="class_{../@name}" >
                    <xsl:attribute name="id"><xsl:call-template name="get_id"/>1</xsl:attribute>
                  </textbox>
                  <button label="+">
                    <xsl:attribute name="oncommand">add('box<xsl:call-template name="get_id"/>','<xsl:call-template name="get_id"/>','bef<xsl:call-template name="get_id"/>');</xsl:attribute>
                  </button>
                </xsl:if>
              </hbox>
            </xslo:if>
          <box hidden="true">
            <xsl:attribute name="id">bef<xsl:call-template name="get_id"/></xsl:attribute>
          </box>
          
        </vbox>
      </xsl:if>
    </row>
  </xsl:template>

  <xsl:template match="choice">
    <row>
      <xsl:if test="name(../.)='attribute'">
        <label value="@{../@name}"/>
      </xsl:if>
      <xsl:if test="name(../.)='element'">
        <label value="{../@name}"/>
      </xsl:if>

      <!--only one field-->
      <xsl:if test="name(../../.)!='oneOrMore' and name(../../.)!='zeroOrMore'">
        <menulist class="class_{../@name}">
                  <xsl:attribute name="id"><xsl:call-template name="get_id"/></xsl:attribute>
          <menupopup>
            <xsl:for-each select="value">
              <menuitem value="{.}" label="{.}">
                <xslo:if>
                  <xsl:attribute name="test"><xsl:call-template name="get_path"/>='<xsl:value-of select="."/>'</xsl:attribute>
                  <xslo:attribute name="selected">true</xslo:attribute>
                </xslo:if>
                </menuitem>
            </xsl:for-each>

          </menupopup>
        </menulist>
      </xsl:if>

      <!--multiple fields-->
      <xsl:if test="name(../../.)='oneOrMore' or name(../../.)='zeroOrMore'">
        <vbox>
            <xsl:attribute name="id">box<xsl:call-template name="get_id"/></xsl:attribute>

          <xslo:for-each>
            <xsl:attribute name="select"><xsl:call-template name="get_path"/></xsl:attribute>
            <xslo:if test="position()=1">
              <hbox>
                <xsl:attribute name="id">hb<xsl:call-template name="get_id"/>1</xsl:attribute>

        <menulist class="class_{../@name}">
          <menupopup>
                  <xsl:attribute name="id"><xsl:call-template name="get_id"/>1</xsl:attribute>
            <xsl:for-each select="value">
              <menuitem value="{.}" label="{.}">
                <xslo:if>
                  <xsl:attribute name="test">.='<xsl:value-of select="."/>'</xsl:attribute>
                  <xslo:attribute name="selected">true</xslo:attribute>
                </xslo:if>
                </menuitem>
            </xsl:for-each>

                </menupopup>
                </menulist>
                <button label="+">
                  <xsl:attribute name="oncommand">add('box<xsl:call-template name="get_id"/>','<xsl:call-template name="get_id"/>','bef<xsl:call-template name="get_id"/>');</xsl:attribute>
                </button>
              </hbox> 
            </xslo:if>
            <xslo:if test="position()&gt;1">
              <hbox>
                <xsl:attribute name="id">hb<xsl:call-template name="get_id"/>{position()}</xsl:attribute>
        <menulist class="class_{../@name}">
          <menupopup>
                  <xsl:attribute name="id"><xsl:call-template name="get_id"/>{position()}</xsl:attribute>
            <xsl:for-each select="value">
              <menuitem value="{.}" label="{.}">
                <xslo:if>
                  <xsl:attribute name="test">.='<xsl:value-of select="."/>'</xsl:attribute>
                  <xslo:attribute name="selected">true</xslo:attribute>
                </xslo:if>
                </menuitem>
            </xsl:for-each>

                </menupopup>
                </menulist>

                <button label="-">
                  <xsl:attribute name="oncommand">remove('hb<xsl:call-template name="get_id"/>{position()}');</xsl:attribute>
                </button>
              </hbox> 
            </xslo:if>
          </xslo:for-each>
            <xslo:if>
              <xsl:attribute name="test">count(<xsl:call-template name="get_path"/>)=0</xsl:attribute>
              <hbox>
                <xsl:attribute name="id">hb<xsl:call-template name="get_id"/>1</xsl:attribute>
        <menulist class="class_{../@name}">
          <menupopup>
                  <xsl:attribute name="id"><xsl:call-template name="get_id"/>1</xsl:attribute>
            <xsl:for-each select="value">
              <menuitem value="{.}" label="{.}">
                </menuitem>
            </xsl:for-each>

                </menupopup>
                </menulist>

                <button label="+">
                  <xsl:attribute name="oncommand">add('box<xsl:call-template name="get_id"/>','<xsl:call-template name="get_id"/>','bef<xsl:call-template name="get_id"/>');</xsl:attribute>
                </button>
              </hbox>
            </xslo:if>
          <box hidden="true">
            <xsl:attribute name="id">bef<xsl:call-template name="get_id"/></xsl:attribute>
          </box>
          
        </vbox>
      </xsl:if>
    </row>
  </xsl:template>
</xsl:stylesheet>
