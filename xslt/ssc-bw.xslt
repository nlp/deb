<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="utf-8" method="xml"
  doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
  doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
  />

<xsl:template match="root">
  <html>
     <head>
      <title>SSČ: <xsl:value-of select="h"/></title>
      <style type="text/css">
      body {
      background: #ffffff;
      color: #000000;
      }
      /*.red {
      color: #ff0000;
      }
      .green {
      color: #007700;
      }
      .darkred {
      color: #770000;
      }
      .blue {
      color: #0000ff;
      }
      .darkblue {
      color: #000088;
      }*/
      .head {
      font-size: 150%;
      }
      </style>
     </head>
     <body>
       <p><xsl:apply-templates/></p>
       <hr />
       <p><small>Slovník spisovné češtiny</small></p>
     </body>
  </html>
</xsl:template>

<xsl:template match="h"><b class="red head"><xsl:value-of select="text()"/></b>
<xsl:if test="$freq!=''">
  <xsl:text> (frekvence v korpusu: </xsl:text><xsl:value-of select="$freq"/>)
</xsl:if>
<br/></xsl:template>
<xsl:template match="gram"><span class="green"><xsl:value-of select="text()"/></span></xsl:template>
<xsl:template match="dom"><span class="green"><xsl:value-of select="text()"/></span></xsl:template>
<xsl:template match="orig">&lt;<xsl:value-of select="text()"/>&gt;</xsl:template>
<xsl:template match="num"><br/><b class="blue"><xsl:value-of select="text()"/>.</b><xsl:text> </xsl:text></xsl:template>
<xsl:template match="exp"><i class="darkred"><xsl:value-of select="text()"/></i></xsl:template>
<xsl:template match="other/t"><br/><b class="blue"><xsl:value-of select="text()"/></b></xsl:template>
<xsl:template match="exm/t"><xsl:value-of select="text()"/>, </xsl:template>
<xsl:template match="ref"><a href="ssc?action=getdoc&amp;id={text()}&amp;tr=ssc"><i class="green"><xsl:value-of select="text()"/>(<xsl:value-of select="refcateg"/>):<xsl:value-of select="refcond"/></i></a> </xsl:template>
<xsl:template match="phra"><b class="blue">[x]</b><xsl:text> </xsl:text><xsl:apply-templates/></xsl:template>
<xsl:template match="pron">[<xsl:value-of select="text()"/>]</xsl:template>

</xsl:stylesheet>

