<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns="http://www.w3c.org/1999/xhtml">
	<xsl:output encoding="utf-8" method="html" indent="yes" version="4.0"/>

<xsl:param name="path"/>
<xsl:param name="showdafn"/>
<xsl:param name="showigi"/>
<xsl:template match="entry">
  <script type="text/javascript">
    entries.push("<xsl:value-of select="@entry_id"/>");
    id["<xsl:value-of select="@entry_id"/>_var"] = <xsl:value-of select="count(me)+2"/>;
    id["<xsl:value-of select="@entry_id"/>_ref"] = <xsl:value-of select="count(ref)+2"/>;
    id["<xsl:value-of select="@entry_id"/>_ex"] =  <xsl:value-of select="count(//ex)+2"/>;
    id["<xsl:value-of select="@entry_id"/>_cit"] =  <xsl:value-of select="count(//cit)+2"/>;
    id["<xsl:value-of select="@entry_id"/>_sense"] =  <xsl:value-of select="count(sense)+2"/>;
    //ta_i = <xsl:value-of select="count(//ex|//cit)"/>;
    complete_time["<xsl:value-of select="@entry_id"/>"] = '<xsl:value-of select="@complete_time"/>';
    /*subModal setting*/
    /*var selchar_object = null;
    var selsrc_object = null;*/
    setPopUpLoadingPage('/files/popload.html');
    /*ikony z famfamfam.com*/
  </script>

  <div>
    <xsl:if test="@quarantine='true'"><xsl:attribute name="class">quarantine</xsl:attribute></xsl:if>
          <span style="right:20px;position:absolute">
            <span id="info_move_entry_{//@entry_id}" style="display:none">Please wait...</span>
            <span style="right:10px;position:relative">
              <xsl:if test="$path='fni'">
                <button onclick="if (save_before_move()) move_entry('fni','fnires','{//@entry_id}')">-&gt; reserve</button>
              </xsl:if>
              <xsl:if test="$path='fnires'">
                <button onclick="if (save_before_move()) move_entry('fnires','fni','{//@entry_id}')">-&gt; FNI</button>
              </xsl:if>
            </span>
          </span>
  <table border="0">
<tr><td>
<span style="display:none">ID: <input type="text" disabled="true" id="entry_id" value="{@entry_id}"/></span>
<span style="display:none">hw: <input type="text" disabled="true" id="{@entry_id}_cur_hw" value="{hw}"/></span>
          <xsl:text> | </xsl:text><label>me <input type="checkbox" id="{@entry_id}_is_me" onclick="check_qua_me('{@entry_id}');update('{@entry_id}')">
            <xsl:if test="@me='true'"><xsl:attribute name="checked">true</xsl:attribute></xsl:if>
          </input></label>
          <xsl:text> | </xsl:text><label>var <input type="checkbox" id="{@entry_id}_is_xref" onclick="update('{@entry_id}')">
            <xsl:if test="@xref='true'"><xsl:attribute name="checked">true</xsl:attribute></xsl:if>
        </input></label>
</td>
<td>
        <xsl:text> | </xsl:text><label>PR <select id="{@entry_id}_pr">
            <option value=""></option>
            <option value="smug">
              <xsl:if test="@pr='smug'"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
              SMUG
            </option>
            <option value="news">
              <xsl:if test="@pr='news'"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
              NEWS
            </option>
          </select>
        </label>
</td>
<td>
        <xsl:text> | </xsl:text>Progress:
</td>
<td>
        <label>R <input type="checkbox" id="{@entry_id}_progress_r" onclick="check_progress('{@entry_id}', 'r');update('{@entry_id}')">
            <xsl:if test="@progress_r='true'"><xsl:attribute name="checked">true</xsl:attribute></xsl:if>
        </input></label>
        <xsl:text> | </xsl:text><label style="">Has query <input type="checkbox" id="{@entry_id}_progress_q" onclick="check_progress('{@entry_id}', 'q');update('{@entry_id}')">
            <xsl:if test="@progress_q='true'"><xsl:attribute name="checked">true</xsl:attribute></xsl:if>
        </input></label>

</td>
<td>
	<span id="{@entry_id}_modified" style="display:none;font-style:italic;font-weight:bold"> *modified*</span>
</td>
</tr></table>
          <input type="hidden" id="is_new" value="{@new}"/>

<table id="data_table">
<tr><td colspan="2">
<table>
<tr><th>headword</th>
<td>
<xsl:if test="@entry_id='__new__'">
<input type="text" id="{@entry_id}_hw" class="word" value="{hw}" onchange="alert('headword is changed, please check');update('{@entry_id}');check_hw(this, '{@entry_id}');"/>
</xsl:if>
<xsl:if test="@entry_id!='__new__'">
<input type="text" id="{@entry_id}_hw" class="word" value="{hw}" disabled="disabled"  onchange="alert('headword is changed, please check');update('{@entry_id}');check_hw(this, '{@entry_id}');"/>
</xsl:if>
<xsl:text> </xsl:text>
          </td>
          <td rowspan="2" style="min-width:62%;vertical-align:top;text-align:right">
            <span class="frinfo">freq. 1911</span>
            <input type="text" id="{@entry_id}_freq_ir_1911" class="freq" value="{freq_ir_1911}" onchange="update('{@entry_id}')"/><xsl:text> </xsl:text>
            <span class="frinfo">1997</span>
            <input type="text" id="{@entry_id}_freq_roi" class="freq" value="{freq_roi}" onchange="update('{@entry_id}')"/><xsl:text> </xsl:text>

<div style="clear:all"> </div>
Main location 1847-64
<input type="text" id="{@entry_id}_heartland_ir" class="heartland" value="{heartland_ir}" onchange="update('{@entry_id}')"/><xsl:text> </xsl:text>
<div style="clear:all"> </div>
Main location 1911
<input type="text" id="{@entry_id}_heartland_1911" class="heartland" value="{heartland_1911}" onchange="update('{@entry_id}')"/><xsl:text> </xsl:text>
          </td>
</tr>
<!--<tr>
<th>source</th>
<td><input type="text" id="{@entry_id}_source" class="heartland" value="{source}" onchange="update('{@entry_id}')"/>

</td>
</tr>-->
         <tr><th>variants</th>
<td>
        <div id="{@entry_id}_box_var">
          <xsl:for-each select="me">
            <xsl:if test="position()=1">
              <span id="{//@entry_id}_hbvar1">
                <input type="text" class="var" id="{//@entry_id}_var1" info="{//@entry_id}_varinfo1" value="{.}" onchange='update("{//@entry_id}"); var_info(this); checkvars(this, "{//@entry_id}");'/>
                <button onclick="add('{//@entry_id}_box_var', 'var', '{//@entry_id}_bef_var', '{//@entry_id}');">+</button><xsl:text> </xsl:text>
                <!--<button onclick="go_var(this.previousSibling.previousSibling.previousSibling);">go</button>-->
                <!--<button onclick="open_entry(this.previousSibling.previousSibling.previousSibling.value);">go</button>-->
                <button onclick="move_var_down(this.parentNode, '{//@entry_id}');">v</button>
                &#160;&#160;&#160;&#160;<span class="varinfo" id="{//@entry_id}_varinfo1"><xsl:comment></xsl:comment></span>
                <span class="varinfo" id="{//@entry_id}_varmoveinfo1"><xsl:comment></xsl:comment></span>
              </span> 
            </xsl:if>
            <xsl:if test="position()>1">
              <span id="{//@entry_id}_hbvar{position()}" style="display:block">
                <input type="text" class="var" id="{//@entry_id}_var{position()}" info="{//@entry_id}_varinfo{position()}" value="{.}" onchange="update('{//@entry_id}'); var_info(this);checkvars(this, '{//@entry_id}');"/>
                <button onclick="remove_var('{//@entry_id}_hbvar{position()}');">-</button><xsl:text> </xsl:text>
                <!--<button onclick="go_var(this.previousSibling.previousSibling.previousSibling);">go</button>-->
                <!--<button onclick="open_entry(this.previousSibling.previousSibling.previousSibling.value);">go</button>-->
                <button onclick="move_var_up(this.parentNode);">^</button>
                <button onclick="move_var_down(this.parentNode);">v</button>
                <span class="varinfo"  id="{//@entry_id}_varinfo{position()}"><xsl:comment></xsl:comment></span>
                <span class="varinfo"  id="{//@entry_id}_varmoveinfo{position()}"><xsl:comment></xsl:comment></span>
              </span> 
            </xsl:if>
            </xsl:for-each>
            <xsl:if test="count(me)=0">
              <span id="{//@entry_id}_hbvar1">
                <span id="{//@entry_id}_varinfo1"><xsl:comment></xsl:comment></span>
                <span id="{//@entry_id}_varmoveinfo1"><xsl:comment></xsl:comment></span>
                <input type="text" class="var" id="{//@entry_id}_var1" value="" onchange='update("{//@entry_id}"); var_info(this);checkvars(this, "{//@entry_id}");'/>
                <button onclick="add('{//@entry_id}_box_var', 'var', '{//@entry_id}_bef_var', '{//@entry_id}');">+</button><xsl:text> </xsl:text>
                <!--<button onclick="open_entry(this.previousSibling.previousSibling.previousSibling.value);">go</button>-->
                <!--<button onclick="move_var_up(this.parentNode);">^</button>-->
                <button onclick="move_var_down(this.parentNode);">v</button>
              </span> 
            </xsl:if>
          <span style="display:none"  id="{@entry_id}_bef_var"><xsl:comment></xsl:comment></span>
          </div>
          </td>
        </tr>
        <tr id="{@entry_id}_before_ref"><td><xsl:comment></xsl:comment></td></tr>

      <xsl:if test="count(xref)>0">
         <tr id="{@entry_id}_row_ref"><th>references and links</th>
<td>
        <div id="{@entry_id}_box_ref">
          <xsl:for-each select="xref">
            <xsl:if test="position()=1">
              <span id="{//@entry_id}_hbref1">
                <input type="text" class="ref" id="{//@entry_id}_ref1" value="{.}" onchange="update('{//@entry_id}')"/>
                <button onclick="add('{//@entry_id}_box_ref', 'ref', '{@entry_id}_bef_ref');">+</button><xsl:text> </xsl:text>
                <button onclick="go_var(this.previousSibling.previousSibling.previousSibling);">go</button>
              </span> 
            </xsl:if>
            <xsl:if test="position()>1">
              <span id="{//@entry_id}_hbref{position()}">
                <br/><input type="text" class="ref" id="{//@entry_id}_ref{position()}" value="{.}" onchange="update('{//@entry_id}')"/>
                <button onclick="remove('{//@entry_id}_hbref{position()}');">-</button><xsl:text> </xsl:text>
                <button onclick="go_var(this.previousSibling.previousSibling.previousSibling);">go</button>
              </span> 
            </xsl:if>
            </xsl:for-each>
            <xsl:if test="count(xref)=0">
              <span id="{//@entry_id}_hbref1">
                <input type="text" class="ref" id="{//@entry_id}_ref1" value="" onchange="update('{//@entry_id}')"/>
                <button onclick="add('{//@entry_id}_box_ref', 'ref', '{//@entry_id}_bef_ref');">+</button><xsl:text> </xsl:text>
                <button onclick="go_var(this.previousSibling.previousSibling.previousSibling);">go</button>
              </span> 
            </xsl:if>
          <span style="display:none" id="{@entry_id}_bef_ref"><xsl:comment></xsl:comment></span>
          </div>
          </td>
        </tr>
      </xsl:if>
      </table>
      </td></tr>
<!--      <tr>
      <td style="text-align:right;margin:0;border:0;padding:0" colspan="2">
      <xsl:if test="eb/arch=''">
          <input type="button" value="+ general EB" id="{@entry_id}_general_button" onclick="show_general_eb('{@entry_id}')"/>
          </xsl:if>
      <xsl:if test="expl/ppkk=''">
          <input type="button" value="+ general expl" id="{@entry_id}_generalexpl_button" onclick="show_general_expl('{@entry_id}')"/>
          </xsl:if>
      </td>
      </tr>
      <tr id="{@entry_id}_general_expl">
      <td >
        <xsl:apply-templates select="expl">
          <xsl:with-param name="in_sense"></xsl:with-param>
          <xsl:with-param name="sense_pos"></xsl:with-param>
        </xsl:apply-templates>
        </td>
        </tr>
      <tr id="{@entry_id}_general_eb">
      <td >
        <xsl:apply-templates select="eb">
          <xsl:with-param name="in_sense"></xsl:with-param>
          <xsl:with-param name="sense_pos"></xsl:with-param>
        </xsl:apply-templates>
        </td>
        </tr>-->
        <tr id="{@entry_id}_before_sense"><td><xsl:comment></xsl:comment></td></tr>
      <xsl:apply-templates select="sense">
        <xsl:sort select="s" data-type="number"/>
      </xsl:apply-templates>
        <tr id="{@entry_id}_after_sense"><td><xsl:comment></xsl:comment></td></tr>
        <tr id="{@entry_id}_add_sense_box">
          <xsl:if test="count(sense)>0">
            <xsl:attribute name="style">visibility:hidden</xsl:attribute>
          </xsl:if>
          <td><xsl:comment></xsl:comment></td><td> <button onclick="add_data(document.getElementById('{@entry_id}_before_sense'), 'sense', '', '', '{//@entry_id}')">+ sense</button></td>
        </tr>
        <tr id="{@entry_id}_before_common"><td><xsl:comment></xsl:comment></td></tr>

      <tr>
        <td><xsl:comment></xsl:comment></td>
        <td>
          <!--| <a href="/files/polltax/polltaxnew{substring(hw, 1, 1)}.xls" target="_blank">Poll Tax</a>-->
          | <a target="_blank" href="http://search.atomz.com/search/?sp-q={hw}&amp;sp-a=sp1002e186&amp;sp-f=ISO-8859-1&amp;sp-t=patent&amp;sp-k=patent&amp;sp-advanced=1&amp;sp-p=all&amp;sp-w-control=1&amp;sp-c=100&amp;sp-m=1&amp;sp-s=0">Patent Rolls</a>
          | <a href="http://www.medievalgenealogy.org.uk/fines/search.php?s=1&amp;surname={hw}&amp;given=&amp;place=&amp;after=&amp;before=&amp;county=&amp;finecase=&amp;finefile=&amp;finenumber=" target="_blank">Feet of Fines</a>
          <!--| <a href="/files/prob11/prob11new{substring(hw, 1, 1)}.xls" target="_blank">PROB 11</a>-->
          | <a href="http://apps.nationalarchives.gov.uk/a2a/results.aspx?tab=2&amp;Page=1&amp;ContainAllWords={hw}" target="_blank">A2A</a>
          | <a href="http://discovery.nationalarchives.gov.uk/results/r?_q={hw}" target="_blank">Discovery</a>
          <br/>
          | <a href="/files/fiants/fiants.doc" target="_blank">Fiants</a>
          | <a href="/files/polltaxf/" target="_blank">Poll Tax forename</a>
          | <a href="https://archive.org/stream/irishnamessurnam00woul#page/n3/mode/2up">Irish Names, Woulfe</a>
          <!--| <a target="_blank" href="/tnac1?action=rest&amp;query={hw}">TNA</a>-->
        </td>
      </tr>

      <tr>
        <th colspan="2" style="text-align:left">Notes (not for publication)</th>
      </tr>
      <tr>
        <table>
          <tr>
            <th style="text-align:left">Editors</th>
            <th style="text-align:left"><xsl:comment></xsl:comment></th>
          </tr>
          <tr>
            <td><textarea id="{@entry_id}_editors" onchange="update('{@entry_id}');" class=""><xsl:value-of select="editors"/><xsl:comment></xsl:comment></textarea></td>
            <td><b>Requests</b> <textarea id="{@entry_id}_comment" onkeyup="resize(this)" onchange="update_progress('{@entry_id}');update('{@entry_id}');" class="comment"><xsl:value-of select="comment"/><xsl:comment></xsl:comment></textarea>
            <input type="button" value="show requests" id="{@entry_id}_comment_show" style="display:none" onclick="document.getElementById('{@entry_id}_comment').style.display='inline';document.getElementById('{@entry_id}_comment_show').style.display='none'"/>
            <br/><b>Musings</b> <textarea id="{@entry_id}_musings" onkeyup="resize(this)" onchange="update('{@entry_id}');" class="comment"><xsl:value-of select="musings"/><xsl:comment></xsl:comment></textarea>
            <input type="button" value="share my thoughts" id="{@entry_id}_musings_show" style="display:none" onclick="document.getElementById('{@entry_id}_musings').style.display='inline';document.getElementById('{@entry_id}_musings_show').style.display='none'"/>
            </td>
          </tr>
        </table>
      </tr>
      <tr class="{@entry_id}_dataend"/>
    </table>

<!--      <xsl:if test="eb/arch=''">
      <script type="text/javascript">
      document.getElementById('<xsl:value-of select="@entry_id"/>_citexrow_arch1').style.display = 'none';
      document.getElementById('<xsl:value-of select="@entry_id"/>_citexrow_arch1').style.visibility = 'hidden';
      </script>
      </xsl:if>
      <xsl:if test="expl/ppkk=''">
      <script type="text/javascript">
      document.getElementById('<xsl:value-of select="@entry_id"/>_citexrow_ppkk1').style.display = 'none';
      document.getElementById('<xsl:value-of select="@entry_id"/>_citexrow_ppkk1').style.visibility = 'hidden';
      </script>
      </xsl:if>-->
  </div>

  <!--<button onclick="alert(get_xml('{@entry_id}'))" label="test">test</button><xsl:text> </xsl:text>
  <button onclick="back_list=false;save_xml();reload_main();" disabled="true" id="but_save" label="Save">Save</button><xsl:text> </xsl:text>
  <button onclick="back_list=false;save_xml(true);reload_main();" disabled="true" id="but_save_back" label="Save &amp; close">Save &amp; close</button><xsl:text> </xsl:text>
  <button onclick="reload_main();close_window();" label="Close">Close</button><xsl:text> </xsl:text>-->
  <!--<button onclick="back_list=true;save_xml();" disabled="true" id="but_save_back" label="Save">Save &amp; back</button><xsl:text> </xsl:text>
  <button onclick="open_cat();" label="Back to list">Back to list</button><xsl:text> </xsl:text>
  <button onclick="open_cat('');" label="Back to main page">Back to main page</button>-->
<hr class="divider"/>
</xsl:template>

<xsl:template match="eb">
  <xsl:param name="in_sense"/>
  <xsl:param name="sense_pos"/>
  <xsl:apply-templates select="arch">
    <xsl:with-param name="in_sense"><xsl:value-of select="$in_sense"/></xsl:with-param>
    <xsl:with-param name="sense_pos"><xsl:value-of select="$sense_pos"/></xsl:with-param>
  </xsl:apply-templates>
  <!--<xsl:apply-templates select="igi">
    <xsl:with-param name="in_sense"><xsl:value-of select="$in_sense"/></xsl:with-param>
    <xsl:with-param name="sense_pos"><xsl:value-of select="$sense_pos"/></xsl:with-param>
  </xsl:apply-templates>
  <xsl:apply-templates select="med">
    <xsl:with-param name="in_sense"><xsl:value-of select="$in_sense"/></xsl:with-param>
    <xsl:with-param name="sense_pos"><xsl:value-of select="$sense_pos"/></xsl:with-param>
  </xsl:apply-templates>
  <xsl:apply-templates select="other">
    <xsl:with-param name="in_sense"><xsl:value-of select="$in_sense"/></xsl:with-param>
    <xsl:with-param name="sense_pos"><xsl:value-of select="$sense_pos"/></xsl:with-param>
  </xsl:apply-templates>-->
</xsl:template>
<xsl:template match="expl">
  <xsl:param name="in_sense"/>
  <xsl:param name="sense_pos"/>
  <xsl:apply-templates select="ppkk">
    <xsl:with-param name="in_sense"><xsl:value-of select="$in_sense"/></xsl:with-param>
    <xsl:with-param name="sense_pos"><xsl:value-of select="$sense_pos"/></xsl:with-param>
  </xsl:apply-templates>
  <!--<xsl:apply-templates select="dafn">
    <xsl:with-param name="in_sense"><xsl:value-of select="$in_sense"/></xsl:with-param>
    <xsl:with-param name="sense_pos"><xsl:value-of select="$sense_pos"/></xsl:with-param>
  </xsl:apply-templates>-->
</xsl:template>

<xsl:template match="arch|igi|med|ppkk|dafn|other_info">
<xsl:text>
</xsl:text>
  <xsl:param name="in_sense"/>
  <xsl:param name="sense_pos"/>
  <tr id="{//@entry_id}_{$in_sense}{$sense_pos}citexrow_{name()}{position()}" data="{//@entry_id}_{$in_sense}citex" element="{name()}">
  <th>
    <xsl:choose>
      <xsl:when test="name()='arch'">
        <xsl:if test="$in_sense=''">general EB</xsl:if>
        <xsl:if test="$in_sense!=''">archive EB</xsl:if>
        <span style="display:block">
          <select class="archselect" onchange="update_eb_type(this.nextSibling,this.value,this)">
            <option value=""><xsl:comment></xsl:comment></option>
            <option value="given names">given names</option>
            <option value="surnames">surnames</option>
            <option value="England">England</option>
            <option value="Ireland">Ireland</option>
            <option value="Scotland">Scotland</option>
            <option value="Wales">Wales</option>
            <option value="IoM">IoM</option>
            <option value="Ch. Isles">Ch. Isles</option>
          </select>
          <input class="archtype" type="text" id="{//@entry_id}_{$in_sense}{$sense_pos}citex_{name()}{position()}_type" value="{@type}"/>
        </span>
      </xsl:when>
      <xsl:when test="name()='igi'">
        IGI
      </xsl:when>
      <xsl:when test="name()='med'">
        MED
      </xsl:when>
      <xsl:when test="name()='ppkk'">
        expl.
        <xsl:if test="count(../ppkk) > 1"><xsl:number count="ppkk" format="(i)"/></xsl:if>
        <span style="display:block">
          <select class="explselect" onchange="update_expl_type(this.nextSibling,this.value,this)">
            <option value=""><xsl:comment></xsl:comment></option>
            <option value="Locative name">Locative name</option>
            <option value="Relationship name">Relationship name</option>
            <option value="Occupational name">Occupational name</option>
            <option value="Nickname">Nickname</option>
            <option value="Status name">Status name</option>
          </select>
          <input class="explcat" type="text" title="typology" id="{//@entry_id}_{$in_sense}{$sense_pos}citex_{name()}{position()}_cat" value="{@category}"/>
        </span>
      </xsl:when>
      <xsl:when test="name()='dafn'">
        DAFN/DoS
      </xsl:when>
      <xsl:when test="name()='other_info'">
        other info
      </xsl:when>
      <xsl:when test="name()='ex'">
        expl.
        <xsl:if test="count(../ex) > 1"><xsl:number count="ex" format="(i)"/></xsl:if>
      </xsl:when>
    </xsl:choose>
  </th>
  <td>
  <table>
  <tr><td>
    <div class="textarea_updiv">
    <div class="textarea_div">
      <textarea class="{name()}" rows="4" cols="60" onkeyup="resize(this)" element="{name()}" id="{//@entry_id}_{$in_sense}{$sense_pos}citex_{name()}{position()}" data="{$in_sense}{$sense_pos}citex" onchange="checkxmltext(this, '{//@entry_id}');update('{//@entry_id}');"><xsl:value-of select="."/><xsl:comment></xsl:comment></textarea>
    </div>
    </div>
      </td>
      <td valign="top">
        <xsl:if test="name()='arch'">
          <xsl:if test="$in_sense!=''">
          <button onclick="add_data(document.getElementById('{//@entry_id}_{$in_sense}{$sense_pos}citexrow_{name()}{position()}'), 'arch', '{$in_sense}', '{$sense_pos}', '{//@entry_id}')">+ e.b.</button>
          </xsl:if>
        </xsl:if>
        <xsl:if test="name()='ppkk'">
          <button onclick="add_data(document.getElementById('{//@entry_id}_{$in_sense}{$sense_pos}citexrow_{name()}{position()}'), 'ppkk', '{$in_sense}', '{$sense_pos}', '{//@entry_id}')">+ expl.</button>
        </xsl:if>

          <button style="display:block" onclick="edit_sources(document.getElementById('{//@entry_id}_{$in_sense}{$sense_pos}citexrow_{name()}{position()}'))">edit src</button>
          <span style="display:block">
          <select onfocus="populate_link_select(this, document.getElementById('{//@entry_id}_{$in_sense}{$sense_pos}citex_{name()}{position()}'))" onchange="open_link(this)"><option selected="selected">open links</option></select>
          </span>
        <!--<xsl:if test="$in_sense != 'sense'">
          <br /><button onclick="add_data(document.getElementById('{//@entry_id}_citexrow{position()}'), 'sense', '', '', '{//@entry_id}')">+ sense</button>
        </xsl:if>-->
      </td>
      </tr></table>
    </td>
  <script type="text/javascript">
  //alert('<xsl:value-of select="//@entry_id"/>_<xsl:value-of select="$in_sense"/><xsl:value-of select="$sense_pos"/>citex_<xsl:value-of select="name()"/><xsl:value-of select="position()"/>');
  //alert(document.getElementById('<xsl:value-of select="//@entry_id"/>_<xsl:value-of select="$in_sense"/><xsl:value-of select="$sense_pos"/>citex_<xsl:value-of select="name()"/><xsl:value-of select="position()"/>'));
    make_textedit(document.getElementById('<xsl:value-of select="//@entry_id"/>_<xsl:value-of select="$in_sense"/><xsl:value-of select="$sense_pos"/>citex_<xsl:value-of select="name()"/><xsl:value-of select="position()"/>'));
    ta_i++;
  </script>
  <!--<script type="text/javascript">
    var textarea = document.getElementById('<xsl:value-of select="//@entry_id"/>_<xsl:value-of select="$in_sense"/><xsl:value-of select="$sense_pos"/>citex<xsl:value-of select="position()"/>');
    textareas[ta_i] = new MirrorFrame(CodeMirror.replace(textarea), {
      parserfile: 'parsexml.js',
      stylesheet: '/files/cm/css/xmlcolors.css',
      path: '/files/cm/js/',
      content: textarea.value,
      width: '40em',
      height: '6em',
      disableSpellcheck: false,
    });

    document.getElementById('<xsl:value-of select="//@entry_id"/>_<xsl:value-of select="$in_sense"/><xsl:value-of select="$sense_pos"/>citexrow<xsl:value-of select="position()"/>').setAttribute('editor', ta_i);
    ta_i++;
    /*http://marijn.haverbeke.nl/codemirror/index.html*/
  </script>-->
  </tr>
</xsl:template>

<xsl:template match="sense">
<xsl:text>
</xsl:text>
  <tr class="sense" id="{//@entry_id}_citexrow{position()}" data="{//@entry_id}_citex" element="sense">
  <th>sense</th>
  <td >
  <table>
  <tr><th>nr.</th><td>
  <xsl:if test="s and not(s='')">
    <input type="text" data="{//@entry_id}_nr" class="sense_nr" value="{s}" />
  </xsl:if>
  <xsl:if test="s='' or not(s)">
    <input type="text" data="{//@entry_id}_nr" class="sense_nr" value="{position()}" />
  </xsl:if>
          <xsl:text> | </xsl:text><label>RI <input type="checkbox" data="{//@entry_id}_is_ri" onclick="update('{//@entry_id}')">
            <xsl:if test="@ri='true'"><xsl:attribute name="checked">true</xsl:attribute></xsl:if>
        </input></label>

          <select class="explselect" onchange="update_eb_type(this.nextSibling,this.value,this)">
            <option value=""><xsl:comment></xsl:comment></option>
            <option value="English">English</option>
            <option value="Huguenot">Huguenot</option>
            <option value="Irish">Irish</option>
            <option value="Jewish (Ashkenazic)">Jewish (Ashkenazic)</option>
            <option value="Jewish (Sephardic)">Jewish (Sephardic)</option>
            <option value="Norman">Norman</option>
            <option value="Scottish">Scottish</option>
            <option value="Scottish Gaelic">Scottish Gaelic</option>
            <option value="Welsh">Welsh</option>
          </select>
          <input class="explcat" title="language/culture" type="text" data="language" value="{@language}"/>
            <!--<button onclick="add_data(document.getElementById('citexrow{position()}'), 'ex', '')">+ expl.</button>-->
            <!--<button onclick="add_data(document.getElementById('citexrow{position()}'), 'cit', '')">+ e.b.</button>-->
            <button onclick="add_data(document.getElementById('{//@entry_id}_sense{position()}ebhere'), 'arch', 'sense', '{position()}', '{//@entry_id}')">+ e.b.</button>
            <button onclick="add_data(document.getElementById('{//@entry_id}_sense{position()}explhere'), 'ppkk', 'sense', '{position()}', '{//@entry_id}')">+ expl</button>
            <button onclick="add_data(document.getElementById('{//@entry_id}_citexrow{position()}'), 'sense', '', '', '{//@entry_id}')">+ sense</button>
            <button onclick="remove_sense('{//@entry_id}_citexrow{position()}', '{//@entry_id}')">remove sense</button>
          </td></tr>
          <!--<tr style="visibility:hidden;display:none" id="{//@entry_id}_sense{position()}category"><th>category</th><td>
              <input type="text" data="{//@entry_id}_cat" class="sense_cat" value="{category}" />
          </td></tr>-->
          <xsl:apply-templates select="expl/ppkk">
            <xsl:sort select="name()"/>
            <xsl:with-param name="in_sense">sense</xsl:with-param>
            <xsl:with-param name="sense_pos"><xsl:value-of select="position()"/></xsl:with-param>
          </xsl:apply-templates>
          <tr style="visibility:hidden;display:none" id="{//@entry_id}_sense{position()}explhere"><xsl:comment></xsl:comment></tr>
          <xsl:apply-templates select="other_info">
            <xsl:with-param name="in_sense">sense</xsl:with-param>
            <xsl:with-param name="sense_pos"><xsl:value-of select="position()"/></xsl:with-param>
          </xsl:apply-templates>
          <xsl:apply-templates select="eb/arch">
            <xsl:sort select="name()"/>
            <xsl:with-param name="in_sense">sense</xsl:with-param>
            <xsl:with-param name="sense_pos"><xsl:value-of select="position()"/></xsl:with-param>
          </xsl:apply-templates>
          <tr style="visibility:hidden;display:none" id="{//@entry_id}_sense{position()}ebhere"><xsl:comment></xsl:comment></tr>

          <xsl:if test="count(other_info)=0 or not(references)">
            <tr><td><xsl:comment></xsl:comment></td><td>
                <xsl:if test="count(other_info)=0">
                  <button id="{//@entry_id}sense{position()}_add_button_other_info" onclick="add_data(document.getElementById('{//@entry_id}_sense{position()}ebhere'), 'other_info', 'sense', '{position()}', '{//@entry_id}')">+ other info</button>
                </xsl:if>
                <xsl:if test="not(references)">
                  <button onclick="show_references('{//@entry_id}_sense{position()}_refrow',this)">+ references</button>
                </xsl:if>
            </td></tr>
          </xsl:if>

          <!--<tr><th>other info</th><td>
              <input type="text" data="{//@entry_id}_other" class="sense_other" value="{other_info}" onchange="update('{//@entry_id}');" />
          </td></tr>-->
          <tr id="{//@entry_id}_sense{position()}_refrow">
            <xsl:if test="not(references)"><xsl:attribute name="style">display:none</xsl:attribute></xsl:if>
            <th>references <br />and links</th><td>
              <textarea id="{//@entry_id}_sense{position()}_ref" data="{//@entry_id}_ref" class="sense_ref" onkeyup="resize(this)" onchange="update('{//@entry_id}')"><xsl:value-of select="references" />
<xsl:comment></xsl:comment>              </textarea>
              <span style="display:inline-block;vertical-align:top">
                <img onclick="insert_i('{//@entry_id}_sense{position()}_ref')" class="button_img" title="Italic" src="files/icon_italic.png"/>
                <img onclick="insert_b('{//@entry_id}_sense{position()}_ref')" class="button_img" title="Bold" src="files/icon_bold.png"/>
                <img onclick="insert_char('{//@entry_id}_sense{position()}_ref')" class="button_img" title="Special character" src="files/icon_special.png"/>
                <img onclick="insert_src('{//@entry_id}_sense{position()}_ref')" class="button_img" title="Source" src="files/icon_book.png"/>
                <img onclick="insert_quote('{//@entry_id}_sense{position()}_ref')" class="button_img" title="Quotes" src="files/icon_quote.png"/>
                <br/>
                <button onclick="edit_sources(document.getElementById('{//@entry_id}_sense{position()}_ref'))">edit src</button>
              </span>
          </td></tr>
          <!--<xsl:apply-templates select="eb/other">
            <xsl:sort select="name()"/>
            <xsl:with-param name="in_sense">sense</xsl:with-param>
            <xsl:with-param name="sense_pos"><xsl:value-of select="position()"/></xsl:with-param>
          </xsl:apply-templates>-->
          <!--<xsl:apply-templates select="expl/dafn">
            <xsl:sort select="name()"/>
            <xsl:with-param name="in_sense">sense</xsl:with-param>
            <xsl:with-param name="sense_pos"><xsl:value-of select="position()"/></xsl:with-param>
          </xsl:apply-templates>-->
          <!--<xsl:apply-templates select="eb/igi">
            <xsl:sort select="name()"/>
            <xsl:with-param name="in_sense">sense</xsl:with-param>
            <xsl:with-param name="sense_pos"><xsl:value-of select="position()"/></xsl:with-param>
          </xsl:apply-templates>-->
          <!--<xsl:apply-templates select="eb/med">
            <xsl:sort select="name()"/>
            <xsl:with-param name="in_sense">sense</xsl:with-param>
            <xsl:with-param name="sense_pos"><xsl:value-of select="position()"/></xsl:with-param>
          </xsl:apply-templates>-->
        <!--<xsl:apply-templates select="ex">
          <xsl:with-param name="in_sense">sense</xsl:with-param>
          <xsl:with-param name="sense_pos"><xsl:value-of select="position()"/></xsl:with-param>
        </xsl:apply-templates>-->
        <tr class="dataend"/>
        <tr><td><xsl:comment></xsl:comment></td><td>move this sense to entry <select update="moveselect" entry="{//@entry_id}"><xsl:comment></xsl:comment></select> <button onclick="move_sense('{//@entry_id}', '{position()}', this.previousSibling);">move</button></td></tr>
    </table>
    </td>
  </tr>
</xsl:template>

</xsl:stylesheet>

