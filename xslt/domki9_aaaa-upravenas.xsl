<?xml version='1.0' encoding='utf-8'?>
<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'><xsl:template match='/entry'><html><head><meta content='text/html[-semicolon-] charset=utf-8' http-equiv='Content-Type'/><title>aaaa: <xsl:value-of select="reference"/> (DEBWrite)</title></head>
<body><h1><xsl:value-of select="reference"/>dfgdfgdfg</h1>
<xsl:apply-templates/></body></html></xsl:template>
<xsl:template match="hw"><span class="hw">headword: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="pos"><span class="pos">part of speech: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="gram"><span class="gram">grammar info: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="pron"><span class="pron">pronunciation: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="hyph"><span class="hyph">hyphenation: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="meaning"><div class="meaning" style="border: 1px solid #000[-semicolon-] background-color:#eee[-semicolon-]">meaning: <xsl:apply-templates/></div><br/></xsl:template>
<xsl:template match="nr"><span class="nr">meaning nr: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="domain"><span class="domain">domain: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="def"><span class="def">definition: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="usage"><span class="usage">usage example: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="synonym"><span class="synonym">synonym: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="parentcontainer"><div class="parentcontainer" style="border: 1px solid #000[-semicolon-] background-color:#eee[-semicolon-]">parentcontainer: <xsl:apply-templates/></div><br/></xsl:template>
<xsl:template match="ta"><span class="ta">ta: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="subcontainer"><div class="subcontainer" style="border: 1px solid #000[-semicolon-] background-color:#eee[-semicolon-]">subcontainer: <xsl:apply-templates/></div><br/></xsl:template>
<xsl:template match="antonym"><span class="antonym">antonym: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="selectd"><span class="selectd">selectdd: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="textareacontainer"><div class="textareacontainer" style="border: 1px solid #000[-semicolon-] background-color:#eee[-semicolon-]">textareacontainer: <xsl:apply-templates/></div><br/></xsl:template>
<xsl:template match="comment"><span class="comment">comments: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="reference"><span class="reference">references: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="onefile"><span class="onefile">onefile: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="morefile"><span class="morefile">morefile: <xsl:apply-templates/></span><br/></xsl:template>
</xsl:stylesheet>
