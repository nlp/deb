<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="utf-8" method="xml" omit-xml-declaration="yes"/>

<xsl:template match="root">
       <p><xsl:apply-templates/></p>
</xsl:template>

<xsl:template match="h"><b class="head"><xsl:value-of select="text()"/></b>
<xsl:if test="$freq!=''">
  <xsl:text> (frekvence v korpusu: </xsl:text><xsl:value-of select="$freq"/>)
</xsl:if>
<br/></xsl:template>
<xsl:template match="gram"><span class=""><xsl:value-of select="text()"/></span></xsl:template>
<xsl:template match="dom"><span class=""><xsl:value-of select="text()"/></span></xsl:template>
<xsl:template match="orig">&lt;<xsl:value-of select="text()"/>&gt;</xsl:template>
<xsl:template match="num"><br/><b class=""><xsl:value-of select="text()"/>.</b><xsl:text> </xsl:text></xsl:template>
<xsl:template match="exp"><i class=""><xsl:value-of select="text()"/></i></xsl:template>
<xsl:template match="other/t"><br/><b class=""><xsl:value-of select="text()"/></b></xsl:template>
<xsl:template match="exm/t"><xsl:value-of select="text()"/>, </xsl:template>
<xsl:template match="ref"><a href="ssc?action=getdoc&amp;id={text()}&amp;tr=ssc"><i class=""><xsl:value-of select="text()"/>(<xsl:value-of select="refcateg"/>):<xsl:value-of select="refcond"/></i></a> </xsl:template>
<xsl:template match="phra"><b class="">[x]</b><xsl:text> </xsl:text><xsl:apply-templates/></xsl:template>
<xsl:template match="pron">[<xsl:value-of select="text()"/>]</xsl:template>

</xsl:stylesheet>

