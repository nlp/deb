<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="utf-8" method="xml" />

<xsl:template match="record">
  <p><i><xsl:value-of select="sign"/></i> (<xsl:value-of select="year"/>): <xsl:apply-templates select="first_person"/> vs. <xsl:apply-templates select="second_person"/>
  <br/>issue: <xsl:apply-templates select="issue"/>
  <xsl:if test="additional_names!=''">
  <br/>additional names: <xsl:apply-templates select="additional_names"/>
  </xsl:if>
</p>
<hr/>
</xsl:template>
<xsl:template match="first_person|second_person">
<xsl:apply-templates/>
</xsl:template>

<xsl:template match="name">
<xsl:apply-templates/>
</xsl:template>
<xsl:template match="surname">
<b><xsl:apply-templates/></b>
</xsl:template>

<xsl:template match="info">
(<xsl:apply-templates/>)
</xsl:template>

<xsl:template match="town|shire">
<u><xsl:apply-templates/></u>
</xsl:template>
<xsl:template match="issue">
<i><xsl:apply-templates/></i>
</xsl:template>
</xsl:stylesheet>

