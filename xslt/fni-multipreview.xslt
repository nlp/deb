<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="utf-8" method="xml"  indent="no"/>

<xsl:param name="show_log"/>
<xsl:param name="base"/>
<xsl:param name="path"/>

<xsl:template match="entry">
<html>
  <h2 class="head blue"><xsl:value-of select="hw"/></h2>
  <span class="green">Frequencies: </span> <span class="green">1911: </span><xsl:value-of select="freq_ir_1911"/>, <span class="green">1997: </span><xsl:value-of select="freq_roi"/>.
  <br />
    <span class="green">main location, 1847-64: </span> <xsl:value-of select="heartland_ir"/>
  <br />
    <span class="green">main location, 1911: </span> <xsl:value-of select="heartland_1911"/>

  <xsl:if test="count(me)>0">
  <br />
    <span class="green">Variants: </span>
      <xsl:for-each select="me">
        <a href="{$path}?action=preview&amp;id={.}"><xsl:value-of select="."/></a>
        <xsl:if test="@language!=''"> (<xsl:value-of select="@language"/>)</xsl:if>
        <xsl:if test="position()!=last()">, </xsl:if>
      </xsl:for-each>.
  </xsl:if>

  <xsl:apply-templates select="sense"/>
  <!--<xsl:if test="eb">
    <br /><i>General Early Bearers: </i>
    <xsl:apply-templates select="eb/arch"/>
  </xsl:if>
  <xsl:if test="expl">
    <br /><i>General expl.: </i>
    <xsl:apply-templates select="expl/ppkk"/>
  </xsl:if>-->
  <xsl:apply-templates select="dafn|med|igi"/>

  <xsl:apply-templates select="comment|musings"/>
  </html>
</xsl:template>

<xsl:template match="meta">
  <table border="1">
    <xsl:for-each select="edit">
      <xsl:sort select="@time" order="descending"/>
      <tr><td><xsl:value-of select="@author"/></td>
      <td><xsl:value-of select="@time"/></td></tr>
    </xsl:for-each>
  </table>
</xsl:template>

<xsl:template match="cit">
  <br /><span class="blue">Early bearers: </span>
  <xsl:apply-templates/>
</xsl:template>
<xsl:template match="arch">
  <xsl:if test="count(../arch)>1">
    <xsl:if test="position()>1"><br /></xsl:if>
    <xsl:number count="arch" format="(i)"/><xsl:text> </xsl:text>
  </xsl:if>
  <xsl:if test="@type!=''"><xsl:value-of select="@type"/>: </xsl:if> 
  <xsl:apply-templates/>
</xsl:template>
<xsl:template match="med">
  <xsl:if test="text()!=''">
    <p><span class="blue">MED: </span>
      <xsl:apply-templates/>
    </p>
  </xsl:if>
</xsl:template>
<xsl:template match="igi">
  <xsl:if test="text()!=''">
  <p><span class="blue">IGI: </span>
  <xsl:apply-templates/>
  </p>
  </xsl:if>
</xsl:template>
<xsl:template match="other">
  <xsl:if test="text()!=''">
  <br /><span class="blue">Other: </span>
  <xsl:apply-templates/>
  </xsl:if>
</xsl:template>

<xsl:template match="ex">
  <br /><span class="green">Explanation<xsl:if test="count(../ex)>1"><xsl:number format=" (i)"/></xsl:if>: </span><xsl:if test="@category!=''"><em><xsl:value-of select="@category"/>: </em></xsl:if>
  <xsl:apply-templates/>
</xsl:template>
<xsl:template match="ppkk">
  <xsl:if test="count(../ppkk)>1">
    <xsl:if test="position()>1"><br /></xsl:if>
    <xsl:number count="ppkk" format="(i)"/><xsl:text> </xsl:text>
  </xsl:if>
  <xsl:if test="@category!=''"><b><xsl:text> </xsl:text><xsl:value-of select="@category"/>: </b></xsl:if>
  <xsl:apply-templates/>
</xsl:template>
<xsl:template match="dafn">
  <xsl:if test="text()!=''">
  <p><span class="green">DAFN/DoS: </span>
  <xsl:apply-templates/>
  </p>
  </xsl:if>
</xsl:template>

<xsl:template match="category">
  <xsl:if test="text()!=''">
  <br /><span class="darkred">Category: </span>
  <xsl:apply-templates/>
  </xsl:if>
</xsl:template>
<xsl:template match="references">
  <xsl:if test=".!=''">
  <br /><span class="darkred">References: </span>
  <xsl:apply-templates/>
  </xsl:if>
</xsl:template>
<xsl:template match="other_info">
  <xsl:if test="text()!=''">
  <br /><span class="darkred">Other info: </span>
  <xsl:apply-templates/>
  </xsl:if>
</xsl:template>

<xsl:template match="sense">
  <br  />
    <xsl:if test="count(../sense)>1"><b><xsl:if test="s!=''"><xsl:value-of select="s"/></xsl:if><xsl:if test="s=''"><xsl:value-of select="position()"/></xsl:if></b></xsl:if>
    <xsl:if test="@language!=''"><span class="green"> language/culture:</span><xsl:text> </xsl:text> <xsl:value-of select="@language"/><br/></xsl:if>
    <xsl:text> </xsl:text>
    <xsl:apply-templates select="expl/ppkk"/>
  <xsl:if test="count(eb/arch)>0">
    <br  />
      <i>Early Bearers: </i>
      <xsl:apply-templates select="eb/arch"/>
  </xsl:if>
  <xsl:apply-templates select="category"/>
  <xsl:apply-templates select="other_info"/>
  <xsl:apply-templates select="references"/>
</xsl:template>

<xsl:template match="s">
  <b><xsl:value-of select="."/></b>
</xsl:template>

<xsl:template match="b">
  <b><xsl:value-of select="."/></b>
</xsl:template>

<xsl:template match="src">
  <span style="font-family:sans-serif;color:green"><xsl:value-of select="."/></span>
</xsl:template>

<xsl:template match="xr">
  <i><a href="{$path}?action=preview&amp;id={.}"><xsl:value-of select="."/></a></i>
</xsl:template>
<xsl:template match="me">
  <b><a href="{$path}?action=preview&amp;id={.}"><xsl:value-of select="."/></a></b>
</xsl:template>

<xsl:template match="sn">
  <span style="font-family:sans-serif;font-style:italic;"><xsl:value-of select="."/></span>
</xsl:template>

<xsl:template match="i">
<i><xsl:apply-templates/></i>
</xsl:template>

<xsl:template match="br">
<br/>
</xsl:template>

<xsl:template match="comment">
  <xsl:if test=".!=''">
  <span style="display:none" class="comment">Requests: <xsl:value-of select="."/><xsl:text> </xsl:text></span>
  </xsl:if>
</xsl:template>
<xsl:template match="musings">
  <xsl:if test=".!=''">
  <br/><span style="display:none" class="musings">Musings: <xsl:value-of select="."/><xsl:text> </xsl:text></span>
  </xsl:if>
</xsl:template>

</xsl:stylesheet>

