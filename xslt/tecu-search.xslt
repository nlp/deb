<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output encoding="utf-8" method="xml"
  doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
  doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
  />

<xsl:param name="search"/>
<xsl:param name="deleted"/>

<xsl:template match="result-list">
  <div class="titem">
    <xsl:if test="$search!=''">
      <h1>Hledaný termín: <xsl:value-of select="$search"/></h1>
      <a href="/tezk/editor/tezaurus.html?deleted=true">zobrazit odstraněné termíny</a>
    </xsl:if>
    <xsl:if test="$deleted='true'">
      <h1>Odstraněné termíny</h1>
    </xsl:if>
    <xsl:if test="count(result-entry)=0">
      nenalezeno
    </xsl:if>
    <xsl:if test="count(result-entry[status=1])>0">
      <h2>terminologické</h2>
      <ul>
        <xsl:apply-templates select="result-entry[status=1]"/>
      </ul>
    </xsl:if>
    <xsl:if test="count(result-entry[status=2 or not(status) or status=''])>0">
      <h2>používané</h2>
      <ul>
        <xsl:apply-templates select="result-entry[status=2 or not(status) or status='']"/>
      </ul>
    </xsl:if>
    <xsl:if test="count(result-entry[status=3])>0">
      <h2>používané nezpracované</h2>
      <ul>
        <xsl:apply-templates select="result-entry[status=3]"/>
      </ul>
    </xsl:if>
    <xsl:if test="count(result-entry[status=4])>0">
      <h2>odmítnuté</h2>
      <ul>
        <xsl:apply-templates select="result-entry[status=4]"/>
      </ul>
    </xsl:if>
    <xsl:if test="count(result-entry[status=0])>0">
      <ul>
        <xsl:apply-templates select="result-entry[status=0]"/>
      </ul>
    </xsl:if>
    </div>
</xsl:template>

<xsl:template match="result-entry">
  <li>
    <xsl:if test="status=0">
      <a class="hyperlink" href="/tezk/editor/tezaurus.html?id={id}&amp;type=del"><xsl:value-of select="head"/></a>
    </xsl:if>
    <xsl:if test="status!=0">
      <a class="hyperlink" href="/tezk/editor/tezaurus.html?id={id}&amp;opentree=true"><xsl:value-of select="head"/></a>
    </xsl:if>
  </li>
</xsl:template>

</xsl:stylesheet>
