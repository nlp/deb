<?xml version='1.0' encoding='utf-8'?>
<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'><xsl:template match='/entry'><html><head><meta content='text/html[-semicolon-] charset=utf-8' http-equiv='Content-Type'/><title>Frazeologický slovník: <xsl:value-of select="hw"/> (DEBWrite)</title><style type="text/css">.hw{color: blue}
.type_container {border: 1px solid #000}
.type_container {background-color:#eee}
</style>
</head>
<body><h1><xsl:value-of select="hw"/></h1>
<xsl:apply-templates/></body></html></xsl:template>
<xsl:template name="file"><xsl:param name="file_element"/><br/><xsl:if test="starts-with($file_element/@mime, 'image')"><img src="/files/deb_reckefraze/{.}" style="max-width:400px"/></xsl:if><xsl:if test="starts-with($file_element/@mime, 'audio')"><audio src="/files/deb_reckefraze/{.}" controls="true"/></xsl:if><xsl:if test="starts-with($file_element/@mime, 'video')"><video src="/files/deb_reckefraze/{.}" controls="true" style="max-width:400px"/></xsl:if><br/></xsl:template>

<xsl:template match="hw"><span class="hw type_text">slovo v řečtině: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="clen"><span class="clen type_select">člen: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="preklad"><span class="preklad type_text">český překlad: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="frazem"><div class="frazem type_container">frazém: <xsl:apply-templates/></div><br/></xsl:template>
<xsl:template match="frazem_gr"><span class="frazem_gr type_textarea">řecky: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="frazem_cz"><span class="frazem_cz type_textarea">česky: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="vyklad"><div class="vyklad type_container">výklad: <xsl:apply-templates/></div><br/></xsl:template>
<xsl:template match="vyklad_gr"><span class="vyklad_gr type_textarea">výklad v řečtině: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="poznamka"><span class="poznamka type_textarea">poznámka: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="varianta"><span class="varianta type_textarea">varianta: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="synonym"><span class="synonym type_textarea">synonymní: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="priklad"><span class="priklad type_textarea">názorný příklad: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="vyklad_cz"><span class="vyklad_cz type_textarea">výklad v češtině: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="ekvivalent_cz"><span class="ekvivalent_cz type_text">český ekvivalent: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="reference"><span class="reference type_textarea">references: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="comment"><span class="comment type_textarea">comments: <xsl:apply-templates/></span><br/></xsl:template>
</xsl:stylesheet>
