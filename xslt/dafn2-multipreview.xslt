<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="utf-8" method="xml"
  doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
  doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
  />

<xsl:template match="entry">
  <html>
       <h1 class="blue head"><xsl:value-of select="name"/></h1>
       <p>1997 TD: <span class="green"><xsl:value-of select="count"/></span>;
         2000 freq.: <span class="green"><xsl:value-of select="freq_2000"/></span>
         2007 freq.: <span class="green"><xsl:value-of select="freq_2007"/></span>
         2010 freq.: <span class="green"><xsl:value-of select="freq_2010"/></span>
         
       </p>
       <xsl:apply-templates select="defblock"/>
       <xsl:apply-templates select="history"/>
       <xsl:apply-templates select="forenames"/>
       <xsl:apply-templates select="comment"/>
       <xsl:apply-templates select="musings"/>
  </html>
</xsl:template>

<xsl:template match="defblock">
  <p><xsl:apply-templates select="prelim"/><xsl:apply-templates select="sense"/></p>
</xsl:template>

<xsl:template match="prelim">
  <xsl:apply-templates/><xsl:text> </xsl:text>
</xsl:template>

<xsl:template match="sense">
   <span class="blue"><xsl:if test="count(//sense)>1"><b><xsl:if test="@number!=''"><xsl:value-of select="@number"/></xsl:if><xsl:if test="not(@number) or @number=''"><xsl:value-of select="position()"/></xsl:if><xsl:text> </xsl:text> </b></xsl:if><xsl:apply-templates/></span><br/>
</xsl:template>
<xsl:template match="history">
  <xsl:if test="text()!=''">
    <p><i>History:</i> <xsl:apply-templates/></p>
  </xsl:if>
</xsl:template>
<xsl:template match="forenames">
  <xsl:if test="text()!=''">
    <p><i>Forenames: </i> <xsl:apply-templates/></p>
  </xsl:if>
</xsl:template>
<xsl:template match="fn">
<xsl:value-of select="."/>
</xsl:template>
<xsl:template match="lang">
<xsl:value-of select="."/><xsl:text> </xsl:text>
</xsl:template>
<xsl:template match="prob">
<xsl:value-of select="."/><xsl:text> </xsl:text>
</xsl:template>
<xsl:template match="forename">
  <i><xsl:apply-templates/></i>
</xsl:template>
<xsl:template match="regionalStats">
  <span class="green"><xsl:apply-templates/></span>
</xsl:template>

<xsl:template match="i">
  <i><xsl:apply-templates/></i>
</xsl:template>

<xsl:template match="su">
  <sup><xsl:apply-templates/></sup>
</xsl:template>

<xsl:template match="form">
  <i><xsl:apply-templates/></i>
</xsl:template>

<xsl:template match="xref|xr">
  <a href="/dafn2?action=preview&amp;id={.}"><i><b><xsl:apply-templates/></b></i></a>
</xsl:template>

<xsl:template match="b">
  <b><xsl:value-of select="text()"/></b>
</xsl:template>

<xsl:template match="vz">
  <i><xsl:value-of select="text()"/></i>
</xsl:template>

<xsl:template match="r">
  <xsl:value-of select="text()"/>
</xsl:template>

<xsl:template match="annotation">
</xsl:template>
<xsl:template match="comment">
 <xsl:if test="text()!=''"><span style="display:none" class="request red"><b>Requests: <xsl:value-of select="text()"/></b></span></xsl:if>
</xsl:template>
<xsl:template match="musings">
 <xsl:if test="text()!=''"><span style="display:none" class="musing red"><b>Musings: <xsl:value-of select="text()"/></b></span></xsl:if>
</xsl:template>

<xsl:template match="render">
  <img src="/files/dafnimages/gb_{@img}.JPG"/>
</xsl:template>

</xsl:stylesheet>

