<?xml version='1.0' encoding='utf-8'?>
<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'><xsl:template match='/entry'><html><head><meta content='text/html[-semicolon-] charset=utf-8' http-equiv='Content-Type'/><title>Profesní mluva barberette: <xsl:value-of select="hw"/> (DEBWrite)</title><style type="text/css">.hw{color: blue}
.type_container {border: 1px solid #000}
.type_container {background-color:#eee}
</style>
</head>
<body><h1><xsl:value-of select="hw"/></h1>
<xsl:apply-templates/></body></html></xsl:template>
<xsl:template name="file"><xsl:param name="file_element"/><br/><xsl:if test="starts-with($file_element/@mime, 'image')"><img src="/files/4bbette_Profesni_mluva_barberette/{.}" style="max-width:400px"/></xsl:if><xsl:if test="starts-with($file_element/@mime, 'audio')"><audio src="/files/4bbette_Profesni_mluva_barberette/{.}" controls="true"/></xsl:if><xsl:if test="starts-with($file_element/@mime, 'video')"><video src="/files/4bbette_Profesni_mluva_barberette/{.}" controls="true" style="max-width:400px"/></xsl:if><br/></xsl:template>

<xsl:template match="hw"><span class="hw type_text">headword: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="pos"><span class="pos type_select">part of speech: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="gram"><span class="gram type_text">grammar info: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="pron"><span class="pron type_text">pronunciation: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="hyph"><span class="hyph type_text">hyphenation: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="meaning"><div class="meaning type_container">meaning: <xsl:apply-templates/></div><br/></xsl:template>
<xsl:template match="nr"><span class="nr type_number">meaning nr: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="domain"><span class="domain type_text">domain: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="def"><span class="def type_textarea">definition: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="usage"><span class="usage type_textarea">usage example: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="synonym"><span class="synonym type_text">synonym: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="reference"><span class="reference type_textarea">references: <xsl:apply-templates/></span><br/></xsl:template>
<xsl:template match="comment"><span class="comment type_textarea">comments: <xsl:apply-templates/></span><br/></xsl:template>
</xsl:stylesheet>
