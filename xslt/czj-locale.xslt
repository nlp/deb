<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:variable name="loc-edit"><xsl:if test="$lang='cz'">editovat</xsl:if><xsl:if test="$lang='en'">edit</xsl:if><xsl:if test="$lang='sk'">editovať</xsl:if></xsl:variable>
  <xsl:variable name="loc-zdroj"><xsl:if test="$lang='cz'">Zdroj</xsl:if><xsl:if test="$lang='en'">Source</xsl:if><xsl:if test="$lang='sk'">zdroj</xsl:if></xsl:variable>
  <xsl:variable name="loc-autor"><xsl:if test="$lang='cz'">Autor</xsl:if><xsl:if test="$lang='en'">Author</xsl:if><xsl:if test="$lang='sk'">Autor</xsl:if></xsl:variable>
  <xsl:variable name="loc-copyright"><xsl:if test="$lang='cz'">copyright</xsl:if><xsl:if test="$lang='en'">Author</xsl:if><xsl:if test="$lang='sk'">copyright</xsl:if></xsl:variable>
  <xsl:variable name="loc-autorvideo"><xsl:if test="$lang='cz'">Autor videozáznamu</xsl:if><xsl:if test="$lang='en'">Recorded by</xsl:if><xsl:if test="$lang='sk'">autor videozáznamu</xsl:if></xsl:variable>
  <xsl:variable name="loc-semoblast"><xsl:if test="$lang='cz'">sémantická oblast</xsl:if><xsl:if test="$lang='en'">Semantic field</xsl:if><xsl:if test="$lang='sk'">sémantická oblasť</xsl:if></xsl:variable>
  <xsl:variable name="loc-publikovani"><xsl:if test="$lang='cz'">publikování</xsl:if><xsl:if test="$lang='en'">publishing</xsl:if><xsl:if test="$lang='sk'">publikovanie</xsl:if></xsl:variable>
  <xsl:variable name="loc-automaticky"><xsl:if test="$lang='cz'">automaticky</xsl:if><xsl:if test="$lang='en'">automatic</xsl:if><xsl:if test="$lang='sk'">automaticky</xsl:if></xsl:variable>
  <xsl:variable name="loc-nepublikovat"><xsl:if test="$lang='cz'">nepublikovat</xsl:if><xsl:if test="$lang='en'">none</xsl:if><xsl:if test="$lang='sk'">nepublikovať</xsl:if></xsl:variable>
  <xsl:variable name="loc-vyplnene"><xsl:if test="$lang='cz'">všechny vyplněné části</xsl:if><xsl:if test="$lang='en'">all non-empty parts</xsl:if><xsl:if test="$lang='sk'">všetky vyplnené časti</xsl:if></xsl:variable>
  <xsl:variable name="loc-schvalene"><xsl:if test="$lang='cz'">jen schválené části</xsl:if><xsl:if test="$lang='en'">approved parts only</xsl:if><xsl:if test="$lang='sk'">len schválené časti</xsl:if></xsl:variable>
  <xsl:variable name="loc-primy"><xsl:if test="$lang='cz'">přímý odkaz</xsl:if><xsl:if test="$lang='en'">direct link</xsl:if><xsl:if test="$lang='sk'">priamy odkaz</xsl:if></xsl:variable>
  <xsl:variable name="loc-kopirovat"><xsl:if test="$lang='cz'">zkopírovat</xsl:if><xsl:if test="$lang='en'">copy</xsl:if><xsl:if test="$lang='sk'">skopírovať</xsl:if></xsl:variable>
  <xsl:variable name="loc-zobrazitcasti"><xsl:if test="$lang='cz'">Zobrazit části slovního spojení</xsl:if><xsl:if test="$lang='en'">Show links to components</xsl:if><xsl:if test="$lang='sk'">Zobraziť časti slovného spojenia</xsl:if></xsl:variable>
  <xsl:variable name="loc-celni"><xsl:if test="$lang='cz'">Čelní pohled</xsl:if><xsl:if test="$lang='en'">Front view</xsl:if><xsl:if test="$lang='sk'">čelný pohľad</xsl:if></xsl:variable>
  <xsl:variable name="loc-bocni"><xsl:if test="$lang='cz'">Boční pohled</xsl:if><xsl:if test="$lang='en'">Side view</xsl:if><xsl:if test="$lang='sk'">bočný pohľad</xsl:if></xsl:variable>
  <xsl:variable name="loc-celnivideo"><xsl:if test="$lang='cz'">čelní video</xsl:if><xsl:if test="$lang='en'">Front view</xsl:if><xsl:if test="$lang='sk'">čelné video</xsl:if></xsl:variable>
  <xsl:variable name="loc-bocnivideo"><xsl:if test="$lang='cz'">boční video</xsl:if><xsl:if test="$lang='en'">Side view</xsl:if><xsl:if test="$lang='sk'">bočné video</xsl:if></xsl:variable>
  <xsl:variable name="loc-typhesla"><xsl:if test="$lang='cz'">typ hesla</xsl:if><xsl:if test="$lang='en'">lemma type</xsl:if><xsl:if test="$lang='sk'">typ hesla</xsl:if></xsl:variable>
  <xsl:variable name="loc-jednoduche"><xsl:if test="$lang='cz'">jednoduché</xsl:if><xsl:if test="$lang='en'">single</xsl:if><xsl:if test="$lang='sk'">jednoduchý</xsl:if></xsl:variable>
  <xsl:variable name="loc-derivat"><xsl:if test="$lang='cz'">derivát</xsl:if><xsl:if test="$lang='en'">derivative</xsl:if><xsl:if test="$lang='sk'">derivát</xsl:if></xsl:variable>
  <xsl:variable name="loc-kompozitum"><xsl:if test="$lang='cz'">kompozitum</xsl:if><xsl:if test="$lang='en'">compositum</xsl:if><xsl:if test="$lang='sk'">kompozitum</xsl:if></xsl:variable>
  <xsl:variable name="loc-spelovany"><xsl:if test="$lang='cz'">spelovaný znak</xsl:if><xsl:if test="$lang='en'">fingerspelled sign</xsl:if><xsl:if test="$lang='sk'">daktilovaný posunok</xsl:if></xsl:variable>
  <xsl:variable name="loc-spojeni"><xsl:if test="$lang='cz'">slovní spojení</xsl:if><xsl:if test="$lang='en'">collocation</xsl:if><xsl:if test="$lang='sk'">slovné spojenie</xsl:if></xsl:variable>
  <xsl:variable name="loc-grampopis"><xsl:if test="$lang='cz'">gramatický popis</xsl:if><xsl:if test="$lang='en'">grammar decription</xsl:if><xsl:if test="$lang='sk'">gramatický popis</xsl:if></xsl:variable>
  <xsl:variable name="loc-lexkategorie"><xsl:if test="$lang='cz'">Lexikální kategorie</xsl:if><xsl:if test="$lang='en'">lexical category</xsl:if><xsl:if test="$lang='sk'">Lexikálna kategória</xsl:if></xsl:variable>
  <xsl:variable name="loc-lexsubst"><xsl:if test="$lang='cz'">podstatné jméno</xsl:if><xsl:if test="$lang='en'">noun</xsl:if><xsl:if test="$lang='sk'">podstatné meno</xsl:if></xsl:variable>
  <xsl:variable name="loc-lexverb"><xsl:if test="$lang='cz'">sloveso</xsl:if><xsl:if test="$lang='en'">verb</xsl:if><xsl:if test="$lang='sk'">sloveso</xsl:if></xsl:variable>
  <xsl:variable name="loc-lexmodif"><xsl:if test="$lang='cz'">modifikátor</xsl:if><xsl:if test="$lang='en'">modifier</xsl:if><xsl:if test="$lang='sk'">modifikátor</xsl:if></xsl:variable>
  <xsl:variable name="loc-lexpron"><xsl:if test="$lang='cz'">zájmeno</xsl:if><xsl:if test="$lang='en'">pronoun</xsl:if><xsl:if test="$lang='sk'">zámeno</xsl:if></xsl:variable>
  <xsl:variable name="loc-lexnum"><xsl:if test="$lang='cz'">číslovka</xsl:if><xsl:if test="$lang='en'">numeral</xsl:if><xsl:if test="$lang='sk'">číslovka</xsl:if></xsl:variable>
  <xsl:variable name="loc-lexkonj"><xsl:if test="$lang='cz'">spojka</xsl:if><xsl:if test="$lang='en'">conjunction</xsl:if><xsl:if test="$lang='sk'">spojka</xsl:if></xsl:variable>
  <xsl:variable name="loc-lexpart"><xsl:if test="$lang='cz'">částice</xsl:if><xsl:if test="$lang='en'">particle</xsl:if><xsl:if test="$lang='sk'">častice</xsl:if></xsl:variable>
  <xsl:variable name="loc-lextaz"><xsl:if test="$lang='cz'">tázací výraz</xsl:if><xsl:if test="$lang='en'">interrogative expression</xsl:if><xsl:if test="$lang='sk'">opytovací výraz</xsl:if></xsl:variable>
  <xsl:variable name="loc-lexkat"><xsl:if test="$lang='cz'">kategorizační výraz</xsl:if><xsl:if test="$lang='en'">categorization expression</xsl:if><xsl:if test="$lang='sk'">kategorizačný výraz</xsl:if></xsl:variable>
  <xsl:variable name="loc-lexklf"><xsl:if test="$lang='cz'">klasifikátorový tvar ruky</xsl:if><xsl:if test="$lang='en'">classifier handshape</xsl:if><xsl:if test="$lang='sk'">klasifikátorový tvar ruky</xsl:if></xsl:variable>
  <xsl:variable name="loc-lexspc"><xsl:if test="$lang='cz'">specifikátor</xsl:if><xsl:if test="$lang='en'">specifier</xsl:if><xsl:if test="$lang='sk'">špecifikátor</xsl:if></xsl:variable>
  <xsl:variable name="loc-jmen"><xsl:if test="$lang='cz'">vlastní</xsl:if><xsl:if test="$lang='en'">proper</xsl:if><xsl:if test="$lang='sk'">vlastný</xsl:if></xsl:variable>
  <xsl:variable name="loc-proste"><xsl:if test="$lang='cz'">prosté</xsl:if><xsl:if test="$lang='en'">plain</xsl:if><xsl:if test="$lang='sk'">prosté</xsl:if></xsl:variable>
  <xsl:variable name="loc-prostorv"><xsl:if test="$lang='cz'">prostorové</xsl:if><xsl:if test="$lang='en'">spatial</xsl:if><xsl:if test="$lang='sk'">priestorové</xsl:if></xsl:variable>
  <xsl:variable name="loc-shodove"><xsl:if test="$lang='cz'">shodové</xsl:if><xsl:if test="$lang='en'">locative</xsl:if><xsl:if test="$lang='sk'">zhodové</xsl:if></xsl:variable>
  <xsl:variable name="loc-modal"><xsl:if test="$lang='cz'">modální</xsl:if><xsl:if test="$lang='en'">modal</xsl:if><xsl:if test="$lang='sk'">modálne</xsl:if></xsl:variable>
  <xsl:variable name="loc-ukaz"><xsl:if test="$lang='cz'">ukazovací</xsl:if><xsl:if test="$lang='en'">demonstrative</xsl:if><xsl:if test="$lang='sk'">ukazovacie</xsl:if></xsl:variable>
  <xsl:variable name="loc-priv"><xsl:if test="$lang='cz'">přivlastňovací</xsl:if><xsl:if test="$lang='en'">possesive</xsl:if><xsl:if test="$lang='sk'">privlastňovacie</xsl:if></xsl:variable>
  <xsl:variable name="loc-klas"><xsl:if test="$lang='cz'">klasifikátorové</xsl:if><xsl:if test="$lang='en'">depicting</xsl:if><xsl:if test="$lang='sk'">klasifikátorové</xsl:if></xsl:variable>
  <xsl:variable name="loc-kval"><xsl:if test="$lang='cz'">kvalifikační</xsl:if><xsl:if test="$lang='en'">qualification</xsl:if><xsl:if test="$lang='sk'">kvalifikačné</xsl:if></xsl:variable>
  <xsl:variable name="loc-cas"><xsl:if test="$lang='cz'">časový</xsl:if><xsl:if test="$lang='en'">time</xsl:if><xsl:if test="$lang='sk'">časový</xsl:if></xsl:variable>
  <xsl:variable name="loc-dej"><xsl:if test="$lang='cz'">aspektový</xsl:if><xsl:if test="$lang='en'">aspect</xsl:if><xsl:if test="$lang='sk'">aspektový</xsl:if></xsl:variable>
  <xsl:variable name="loc-prostorm"><xsl:if test="$lang='cz'">prostorový</xsl:if><xsl:if test="$lang='en'">space</xsl:if><xsl:if test="$lang='sk'">priestorový</xsl:if></xsl:variable>
  <xsl:variable name="loc-zakl"><xsl:if test="$lang='cz'">základní</xsl:if><xsl:if test="$lang='en'">cardinal</xsl:if><xsl:if test="$lang='sk'">základné</xsl:if></xsl:variable>
  <xsl:variable name="loc-rad"><xsl:if test="$lang='cz'">řadová</xsl:if><xsl:if test="$lang='en'">ordinal</xsl:if><xsl:if test="$lang='sk'">radová</xsl:if></xsl:variable>
  <xsl:variable name="loc-nas"><xsl:if test="$lang='cz'">násobná</xsl:if><xsl:if test="$lang='en'">multiplicative</xsl:if><xsl:if test="$lang='sk'">násobná</xsl:if></xsl:variable>
  <xsl:variable name="loc-ikon"><xsl:if test="$lang='cz'">ikonická</xsl:if><xsl:if test="$lang='en'">iconic</xsl:if><xsl:if test="$lang='sk'">ikonická</xsl:if></xsl:variable>
  <xsl:variable name="loc-neur"><xsl:if test="$lang='cz'">neurčitá</xsl:if><xsl:if test="$lang='en'">indefinite</xsl:if><xsl:if test="$lang='sk'">neurčitá</xsl:if></xsl:variable>
  <xsl:variable name="loc-partneg"><xsl:if test="$lang='cz'">záporná</xsl:if><xsl:if test="$lang='en'">negative</xsl:if><xsl:if test="$lang='sk'">záporná</xsl:if></xsl:variable>
  <xsl:variable name="loc-partcont"><xsl:if test="$lang='cz'">kontaktová</xsl:if><xsl:if test="$lang='en'">contacting</xsl:if><xsl:if test="$lang='sk'">kontaktová</xsl:if></xsl:variable>
  <xsl:variable name="loc-pluralu"><xsl:if test="$lang='cz'">tvoření plurálu</xsl:if><xsl:if test="$lang='en'">plural form derivation</xsl:if><xsl:if test="$lang='sk'">tvorenie plurálu</xsl:if></xsl:variable>
  <xsl:variable name="loc-oralni"><xsl:if test="$lang='cz'">Orální komp.</xsl:if><xsl:if test="$lang='en'">oral component</xsl:if><xsl:if test="$lang='sk'">Orálny komp.</xsl:if></xsl:variable>
  <xsl:variable name="loc-mluvni"><xsl:if test="$lang='cz'">Mluvní komp.</xsl:if><xsl:if test="$lang='en'">mouthing</xsl:if><xsl:if test="$lang='sk'">Hovorený komp.</xsl:if></xsl:variable>
  <xsl:variable name="loc-povinny"><xsl:if test="$lang='cz'">povinný</xsl:if><xsl:if test="$lang='en'">compulsory</xsl:if><xsl:if test="$lang='sk'">povinný</xsl:if></xsl:variable>
  <xsl:variable name="loc-nepovinny"><xsl:if test="$lang='cz'">nepovinný</xsl:if><xsl:if test="$lang='en'">optional</xsl:if><xsl:if test="$lang='sk'">nepovinný</xsl:if></xsl:variable>
  <xsl:variable name="loc-gramvar"><xsl:if test="$lang='cz'">Gramatické varianty</xsl:if><xsl:if test="$lang='en'">Gramatical variants</xsl:if><xsl:if test="$lang='sk'">Gramatické varianty</xsl:if></xsl:variable>
  <xsl:variable name="loc-podrobnosti"><xsl:if test="$lang='cz'">zobrazit podrobnosti</xsl:if><xsl:if test="$lang='en'">Show details</xsl:if><xsl:if test="$lang='sk'">zobraziť podrobnosti</xsl:if></xsl:variable>
  <xsl:variable name="loc-odvozenim"><xsl:if test="$lang='cz'">Odvozeno z</xsl:if><xsl:if test="$lang='en'">Derived from</xsl:if><xsl:if test="$lang='sk'">Odvodené od</xsl:if></xsl:variable>
  <xsl:variable name="loc-slozenim"><xsl:if test="$lang='cz'">Výraz vznikl složením znaků</xsl:if><xsl:if test="$lang='en'">Etymology</xsl:if><xsl:if test="$lang='sk'">Výraz vznikol zložením znakov</xsl:if></xsl:variable>
  <xsl:variable name="loc-stylpopis"><xsl:if test="$lang='cz'">stylistický popis</xsl:if><xsl:if test="$lang='en'">Style description</xsl:if><xsl:if test="$lang='sk'">štylistický popis</xsl:if></xsl:variable>
  <xsl:variable name="loc-neo"><xsl:if test="$lang='cz'">Neologismus</xsl:if><xsl:if test="$lang='en'">Neologism</xsl:if><xsl:if test="$lang='sk'">neologizmus</xsl:if></xsl:variable>
  <xsl:variable name="loc-arch"><xsl:if test="$lang='cz'">Archaismus</xsl:if><xsl:if test="$lang='en'">Archaism</xsl:if><xsl:if test="$lang='sk'">archaizmus</xsl:if></xsl:variable>
  <xsl:variable name="loc-oblast"><xsl:if test="$lang='cz'">Oblast užití</xsl:if><xsl:if test="$lang='en'">Distribution area</xsl:if><xsl:if test="$lang='sk'">oblasť použitia</xsl:if></xsl:variable>
  <xsl:variable name="loc-prejato"><xsl:if test="$lang='cz'">Přejato z</xsl:if><xsl:if test="$lang='en'">Origin</xsl:if><xsl:if test="$lang='sk'">prevzaté z</xsl:if></xsl:variable>
  <xsl:variable name="loc-asl"><xsl:if test="$lang='cz'">ASL (americký znakový jazyk)</xsl:if><xsl:if test="$lang='en'">ASL (American Sign Language)</xsl:if><xsl:if test="$lang='sk'">ASL (americký posunkový jazyk)</xsl:if></xsl:variable>
  <xsl:variable name="loc-bsl"><xsl:if test="$lang='cz'">BSL (britského znakový jazyk)</xsl:if><xsl:if test="$lang='en'">BSL (British Sign Language)</xsl:if><xsl:if test="$lang='sk'">BSL (britského posunkový jazyk)</xsl:if></xsl:variable>
  <xsl:variable name="loc-spj"><xsl:if test="$lang='cz'">SPJ (slovenský znakový jazyk)</xsl:if><xsl:if test="$lang='en'">SPJ (Slovak Sign Language)</xsl:if><xsl:if test="$lang='sk'">SPJ (slovenská posunkový jazyk)</xsl:if></xsl:variable>
  <xsl:variable name="loc-ogs"><xsl:if test="$lang='cz'">ÖGS (rakouský znakový jazyk)</xsl:if><xsl:if test="$lang='en'">ÖGS (Austrian Sign Language)</xsl:if><xsl:if test="$lang='sk'">OGS (rakúsky posunkový jazyk)</xsl:if></xsl:variable>
  <xsl:variable name="loc-pjm"><xsl:if test="$lang='cz'">PJM (polský znakový jazyk)</xsl:if><xsl:if test="$lang='en'">PJM (Polish Sign Language)</xsl:if><xsl:if test="$lang='sk'">PJM (poľský posunkový jazyk)</xsl:if></xsl:variable>
  <xsl:variable name="loc-dgs"><xsl:if test="$lang='cz'">DGS (německý znakový jazyk)</xsl:if><xsl:if test="$lang='en'">DGS (German Sign Language)</xsl:if><xsl:if test="$lang='sk'">DGS (nemecký posunkový jazyk)</xsl:if></xsl:variable>
  <xsl:variable name="loc-is"><xsl:if test="$lang='cz'">IS (mezinárodní znaky)</xsl:if><xsl:if test="$lang='en'">IS (International Signs)</xsl:if><xsl:if test="$lang='sk'">IS (medzinárodné znaky)</xsl:if></xsl:variable>
  <xsl:variable name="loc-loc"><xsl:if test="$lang='cz'">tamní místní znakový jazyk</xsl:if><xsl:if test="$lang='en'">local sign language</xsl:if><xsl:if test="$lang='sk'">tamnejší lokálný posunkový jazyk</xsl:if></xsl:variable>
  <xsl:variable name="loc-vekova"><xsl:if test="$lang='cz'">Věková kategorie</xsl:if><xsl:if test="$lang='en'">Distribution age</xsl:if><xsl:if test="$lang='sk'">veková kategória</xsl:if></xsl:variable>
  <xsl:variable name="loc-muzsky"><xsl:if test="$lang='cz'">mužský znak</xsl:if><xsl:if test="$lang='en'">male expression</xsl:if><xsl:if test="$lang='sk'">mužský znak</xsl:if></xsl:variable>
  <xsl:variable name="loc-zensky"><xsl:if test="$lang='cz'">ženský znak</xsl:if><xsl:if test="$lang='en'">female expression</xsl:if><xsl:if test="$lang='sk'">ženský znak</xsl:if></xsl:variable>
  <xsl:variable name="loc-stylvar"><xsl:if test="$lang='cz'">Stylistické varianty</xsl:if><xsl:if test="$lang='en'">Stylistic variants</xsl:if><xsl:if test="$lang='sk'">štylistické varianty</xsl:if></xsl:variable>
  <xsl:variable name="loc-sheslem"><xsl:if test="$lang='cz'">Slovní spojení s tímto heslem</xsl:if><xsl:if test="$lang='en'">Collocation with this lemma</xsl:if><xsl:if test="$lang='sk'">Slovné spojenie s týmto heslom</xsl:if></xsl:variable>
  <xsl:variable name="loc-viz"><xsl:if test="$lang='cz'">viz video</xsl:if><xsl:if test="$lang='en'">see video</xsl:if><xsl:if test="$lang='sk'">pozri video</xsl:if></xsl:variable>
  <xsl:variable name="loc-vizsyn"><xsl:if test="$lang='cz'">viz synonymum</xsl:if><xsl:if test="$lang='en'">see synonym</xsl:if><xsl:if test="$lang='sk'">pozri synonymum</xsl:if></xsl:variable>
  <xsl:variable name="loc-vizvar"><xsl:if test="$lang='cz'">viz varianta 1</xsl:if><xsl:if test="$lang='en'">see variant 1</xsl:if><xsl:if test="$lang='sk'">pozri variant 1</xsl:if></xsl:variable>
  <xsl:variable name="loc-vyznam"><xsl:if test="$lang='cz'">Význam</xsl:if><xsl:if test="$lang='en'">Meaning</xsl:if><xsl:if test="$lang='sk'">význam</xsl:if></xsl:variable>
  <xsl:variable name="loc-cat6"><xsl:if test="$lang='cz'">informatika</xsl:if><xsl:if test="$lang='en'">informatics</xsl:if><xsl:if test="$lang='sk'">informatika</xsl:if></xsl:variable>
  <xsl:variable name="loc-cat14"><xsl:if test="$lang='cz'">podnikání</xsl:if><xsl:if test="$lang='en'">business</xsl:if><xsl:if test="$lang='sk'">podnikania</xsl:if></xsl:variable>
  <xsl:variable name="loc-cat28"><xsl:if test="$lang='cz'">biologie</xsl:if><xsl:if test="$lang='en'">biology</xsl:if><xsl:if test="$lang='sk'">biológia</xsl:if></xsl:variable>
  <xsl:variable name="loc-cat27"><xsl:if test="$lang='cz'">matematika</xsl:if><xsl:if test="$lang='en'">matematics</xsl:if><xsl:if test="$lang='sk'">matematika</xsl:if></xsl:variable>
  <xsl:variable name="loc-catanat"><xsl:if test="$lang='cz'">anatomie</xsl:if><xsl:if test="$lang='en'">anatomy</xsl:if><xsl:if test="$lang='sk'">anatómia</xsl:if></xsl:variable>
  <xsl:variable name="loc-catantr"><xsl:if test="$lang='cz'">antropologie</xsl:if><xsl:if test="$lang='en'">anthropology</xsl:if><xsl:if test="$lang='sk'">antropológia</xsl:if></xsl:variable>
  <xsl:variable name="loc-catarcheol"><xsl:if test="$lang='cz'">archeologie</xsl:if><xsl:if test="$lang='en'">archeology</xsl:if><xsl:if test="$lang='sk'">archeológia</xsl:if></xsl:variable>
  <xsl:variable name="loc-catarchit"><xsl:if test="$lang='cz'">architektrura</xsl:if><xsl:if test="$lang='en'">architecture</xsl:if><xsl:if test="$lang='sk'">architektrura</xsl:if></xsl:variable>
  <xsl:variable name="loc-catbiol"><xsl:if test="$lang='cz'">biologie</xsl:if><xsl:if test="$lang='en'">biology</xsl:if><xsl:if test="$lang='sk'">biológia</xsl:if></xsl:variable>
  <xsl:variable name="loc-catbot"><xsl:if test="$lang='cz'">botanika</xsl:if><xsl:if test="$lang='en'">botanics</xsl:if><xsl:if test="$lang='sk'">botanika</xsl:if></xsl:variable>
  <xsl:variable name="loc-catdipl"><xsl:if test="$lang='cz'">diplomacie</xsl:if><xsl:if test="$lang='en'">diplomatics</xsl:if><xsl:if test="$lang='sk'">diplomacia</xsl:if></xsl:variable>
  <xsl:variable name="loc-catdiv"><xsl:if test="$lang='cz'">divadelnictví</xsl:if><xsl:if test="$lang='en'">theatre</xsl:if><xsl:if test="$lang='sk'">divadelníctva</xsl:if></xsl:variable>
  <xsl:variable name="loc-catdopr"><xsl:if test="$lang='cz'">doprava</xsl:if><xsl:if test="$lang='en'">transport</xsl:if><xsl:if test="$lang='sk'">doprava</xsl:if></xsl:variable>
  <xsl:variable name="loc-catekol"><xsl:if test="$lang='cz'">ekologie</xsl:if><xsl:if test="$lang='en'">ecology</xsl:if><xsl:if test="$lang='sk'">ekológia</xsl:if></xsl:variable>
  <xsl:variable name="loc-catekon"><xsl:if test="$lang='cz'">ekonomie</xsl:if><xsl:if test="$lang='en'">economics</xsl:if><xsl:if test="$lang='sk'">ekonómie</xsl:if></xsl:variable>
  <xsl:variable name="loc-cateltech"><xsl:if test="$lang='cz'">elektrotechnika</xsl:if><xsl:if test="$lang='en'">electrotechnics</xsl:if><xsl:if test="$lang='sk'">elektrotechnika</xsl:if></xsl:variable>
  <xsl:variable name="loc-catetn"><xsl:if test="$lang='cz'">etnografie</xsl:if><xsl:if test="$lang='en'">ethnography</xsl:if><xsl:if test="$lang='sk'">etnografia</xsl:if></xsl:variable>
  <xsl:variable name="loc-catfeud"><xsl:if test="$lang='cz'">feudalismus</xsl:if><xsl:if test="$lang='en'">feudalism</xsl:if><xsl:if test="$lang='sk'">feudalizmus</xsl:if></xsl:variable>
  <xsl:variable name="loc-catfilat"><xsl:if test="$lang='cz'">filatelie</xsl:if><xsl:if test="$lang='en'">philately</xsl:if><xsl:if test="$lang='sk'">filatelia</xsl:if></xsl:variable>
  <xsl:variable name="loc-catfilm"><xsl:if test="$lang='cz'">filmařství</xsl:if><xsl:if test="$lang='en'">film</xsl:if><xsl:if test="$lang='sk'">filmárstva</xsl:if></xsl:variable>
  <xsl:variable name="loc-catfiloz"><xsl:if test="$lang='cz'">filozofie</xsl:if><xsl:if test="$lang='en'">philosophy</xsl:if><xsl:if test="$lang='sk'">filozofia</xsl:if></xsl:variable>
  <xsl:variable name="loc-catfot"><xsl:if test="$lang='cz'">fotografování</xsl:if><xsl:if test="$lang='en'">photography</xsl:if><xsl:if test="$lang='sk'">fotografovanie</xsl:if></xsl:variable>
  <xsl:variable name="loc-catfyz"><xsl:if test="$lang='cz'">fyzika</xsl:if><xsl:if test="$lang='en'">physics</xsl:if><xsl:if test="$lang='sk'">fyzika</xsl:if></xsl:variable>
  <xsl:variable name="loc-catfyziol"><xsl:if test="$lang='cz'">fyziologie</xsl:if><xsl:if test="$lang='en'">physiology</xsl:if><xsl:if test="$lang='sk'">fyziológia</xsl:if></xsl:variable>
  <xsl:variable name="loc-catgeol"><xsl:if test="$lang='cz'">geologie</xsl:if><xsl:if test="$lang='en'">geology</xsl:if><xsl:if test="$lang='sk'">geológia</xsl:if></xsl:variable>
  <xsl:variable name="loc-catgeom"><xsl:if test="$lang='cz'">geometrie</xsl:if><xsl:if test="$lang='en'">geometry</xsl:if><xsl:if test="$lang='sk'">geometria</xsl:if></xsl:variable>
  <xsl:variable name="loc-catgnoz"><xsl:if test="$lang='cz'">gnozeologie</xsl:if><xsl:if test="$lang='en'">gnoseology</xsl:if><xsl:if test="$lang='sk'">teória poznania</xsl:if></xsl:variable>
  <xsl:variable name="loc-cathist"><xsl:if test="$lang='cz'">historie</xsl:if><xsl:if test="$lang='en'">history</xsl:if><xsl:if test="$lang='sk'">histórie</xsl:if></xsl:variable>
  <xsl:variable name="loc-cathorn"><xsl:if test="$lang='cz'">hornictví</xsl:if><xsl:if test="$lang='en'">mining</xsl:if><xsl:if test="$lang='sk'">baníctvo</xsl:if></xsl:variable>
  <xsl:variable name="loc-cathorol"><xsl:if test="$lang='cz'">horolezectví</xsl:if><xsl:if test="$lang='en'">mountaineering</xsl:if><xsl:if test="$lang='sk'">horolezectvo</xsl:if></xsl:variable>
  <xsl:variable name="loc-cathosp"><xsl:if test="$lang='cz'">hospodářství</xsl:if><xsl:if test="$lang='en'">industry</xsl:if><xsl:if test="$lang='sk'">hospodárstvo</xsl:if></xsl:variable>
  <xsl:variable name="loc-cathud"><xsl:if test="$lang='cz'">hudební věda</xsl:if><xsl:if test="$lang='en'">musicology</xsl:if><xsl:if test="$lang='sk'">hudobné veda</xsl:if></xsl:variable>
  <xsl:variable name="loc-cathut"><xsl:if test="$lang='cz'">hutnictví</xsl:if><xsl:if test="$lang='en'">metallurgy</xsl:if><xsl:if test="$lang='sk'">hutníctvo</xsl:if></xsl:variable>
  <xsl:variable name="loc-cathvězd"><xsl:if test="$lang='cz'">hvězdářství</xsl:if><xsl:if test="$lang='en'">astronomy</xsl:if><xsl:if test="$lang='sk'">hvezdárstvo</xsl:if></xsl:variable>
  <xsl:variable name="loc-catchem"><xsl:if test="$lang='cz'">chemie</xsl:if><xsl:if test="$lang='en'">chemistry</xsl:if><xsl:if test="$lang='sk'">chémia</xsl:if></xsl:variable>
  <xsl:variable name="loc-catideal"><xsl:if test="$lang='cz'">idealismus</xsl:if><xsl:if test="$lang='en'">idealism</xsl:if><xsl:if test="$lang='sk'">idealizmus</xsl:if></xsl:variable>
  <xsl:variable name="loc-catjad"><xsl:if test="$lang='cz'">jaderná fyzika</xsl:if><xsl:if test="$lang='en'">nuclear physics</xsl:if><xsl:if test="$lang='sk'">jadrová fyzika</xsl:if></xsl:variable>
  <xsl:variable name="loc-catjaz"><xsl:if test="$lang='cz'">jazykověda</xsl:if><xsl:if test="$lang='en'">philology</xsl:if><xsl:if test="$lang='sk'">jazykoveda</xsl:if></xsl:variable>
  <xsl:variable name="loc-catkapit"><xsl:if test="$lang='cz'">kapitalismus</xsl:if><xsl:if test="$lang='en'">capitalism</xsl:if><xsl:if test="$lang='sk'">kapitalizmus</xsl:if></xsl:variable>
  <xsl:variable name="loc-catkaret"><xsl:if test="$lang='cz'">karetní výraz</xsl:if><xsl:if test="$lang='en'">card expression</xsl:if><xsl:if test="$lang='sk'">kartové výraz</xsl:if></xsl:variable>
  <xsl:variable name="loc-catkatolcírkvi"><xsl:if test="$lang='cz'">katolictví</xsl:if><xsl:if test="$lang='en'">catholicism</xsl:if><xsl:if test="$lang='sk'">katolíctvo</xsl:if></xsl:variable>
  <xsl:variable name="loc-catkrim"><xsl:if test="$lang='cz'">kriminalistika</xsl:if><xsl:if test="$lang='en'">criminology</xsl:if><xsl:if test="$lang='sk'">kriminalistika</xsl:if></xsl:variable>
  <xsl:variable name="loc-catkuch"><xsl:if test="$lang='cz'">kuchařství</xsl:if><xsl:if test="$lang='en'">cookery</xsl:if><xsl:if test="$lang='sk'">kucharstvo</xsl:if></xsl:variable>
  <xsl:variable name="loc-catkult"><xsl:if test="$lang='cz'">kultura</xsl:if><xsl:if test="$lang='en'">culture</xsl:if><xsl:if test="$lang='sk'">kultúra</xsl:if></xsl:variable>
  <xsl:variable name="loc-catkyb"><xsl:if test="$lang='cz'">kybernetika</xsl:if><xsl:if test="$lang='en'">cybernetics</xsl:if><xsl:if test="$lang='sk'">kybernetika</xsl:if></xsl:variable>
  <xsl:variable name="loc-catlék"><xsl:if test="$lang='cz'">lékařství</xsl:if><xsl:if test="$lang='en'">medicine</xsl:if><xsl:if test="$lang='sk'">lekárstvo</xsl:if></xsl:variable>
  <xsl:variable name="loc-catlékár"><xsl:if test="$lang='cz'">lékárnictví</xsl:if><xsl:if test="$lang='en'">pharmaceutics</xsl:if><xsl:if test="$lang='sk'">lekárnictvo</xsl:if></xsl:variable>
  <xsl:variable name="loc-catlet"><xsl:if test="$lang='cz'">letectví</xsl:if><xsl:if test="$lang='en'">aviation</xsl:if><xsl:if test="$lang='sk'">letectvo</xsl:if></xsl:variable>
  <xsl:variable name="loc-catliter"><xsl:if test="$lang='cz'">literární věda</xsl:if><xsl:if test="$lang='en'">literary science</xsl:if><xsl:if test="$lang='sk'">literárna veda</xsl:if></xsl:variable>
  <xsl:variable name="loc-catlog"><xsl:if test="$lang='cz'">logika</xsl:if><xsl:if test="$lang='en'">logic</xsl:if><xsl:if test="$lang='sk'">logika</xsl:if></xsl:variable>
  <xsl:variable name="loc-catmarx"><xsl:if test="$lang='cz'">marxismus</xsl:if><xsl:if test="$lang='en'">marxism</xsl:if><xsl:if test="$lang='sk'">marxizmus</xsl:if></xsl:variable>
  <xsl:variable name="loc-catmat"><xsl:if test="$lang='cz'">matematika</xsl:if><xsl:if test="$lang='en'">mathematics</xsl:if><xsl:if test="$lang='sk'">matematika</xsl:if></xsl:variable>
  <xsl:variable name="loc-catmeteor"><xsl:if test="$lang='cz'">meteorologie</xsl:if><xsl:if test="$lang='en'">meteorology</xsl:if><xsl:if test="$lang='sk'">meteorológia</xsl:if></xsl:variable>
  <xsl:variable name="loc-catminer"><xsl:if test="$lang='cz'">mineralogie</xsl:if><xsl:if test="$lang='en'">mineralogy</xsl:if><xsl:if test="$lang='sk'">mineralógia</xsl:if></xsl:variable>
  <xsl:variable name="loc-catmotor"><xsl:if test="$lang='cz'">motorismus</xsl:if><xsl:if test="$lang='en'">motorism</xsl:if><xsl:if test="$lang='sk'">motorizmus</xsl:if></xsl:variable>
  <xsl:variable name="loc-catmysl"><xsl:if test="$lang='cz'">myslivecký výraz</xsl:if><xsl:if test="$lang='en'">hunting expression</xsl:if><xsl:if test="$lang='sk'">poľovnícky výraz</xsl:if></xsl:variable>
  <xsl:variable name="loc-catmytol"><xsl:if test="$lang='cz'">mytologie</xsl:if><xsl:if test="$lang='en'">mytology</xsl:if><xsl:if test="$lang='sk'">mytológia</xsl:if></xsl:variable>
  <xsl:variable name="loc-catnáb"><xsl:if test="$lang='cz'">náboženství</xsl:if><xsl:if test="$lang='en'">religion</xsl:if><xsl:if test="$lang='sk'">náboženstvo</xsl:if></xsl:variable>
  <xsl:variable name="loc-catnár"><xsl:if test="$lang='cz'">národopis</xsl:if><xsl:if test="$lang='en'">ethnography</xsl:if><xsl:if test="$lang='sk'">národopis</xsl:if></xsl:variable>
  <xsl:variable name="loc-catobch"><xsl:if test="$lang='cz'">obchod</xsl:if><xsl:if test="$lang='en'">trade</xsl:if><xsl:if test="$lang='sk'">obchod</xsl:if></xsl:variable>
  <xsl:variable name="loc-catpedag"><xsl:if test="$lang='cz'">pedagogika</xsl:if><xsl:if test="$lang='en'">pedagogy</xsl:if><xsl:if test="$lang='sk'">pedagogika</xsl:if></xsl:variable>
  <xsl:variable name="loc-catpeněž"><xsl:if test="$lang='cz'">peněžnictví</xsl:if><xsl:if test="$lang='en'">banking</xsl:if><xsl:if test="$lang='sk'">peňažníctvo</xsl:if></xsl:variable>
  <xsl:variable name="loc-catpolit"><xsl:if test="$lang='cz'">politika</xsl:if><xsl:if test="$lang='en'">politics</xsl:if><xsl:if test="$lang='sk'">politika</xsl:if></xsl:variable>
  <xsl:variable name="loc-catpolygr"><xsl:if test="$lang='cz'">polygrafie</xsl:if><xsl:if test="$lang='en'">polygraphy</xsl:if><xsl:if test="$lang='sk'">polygrafia</xsl:if></xsl:variable>
  <xsl:variable name="loc-catpošt"><xsl:if test="$lang='cz'">poštovnictví</xsl:if><xsl:if test="$lang='en'">postal expression</xsl:if><xsl:if test="$lang='sk'">poštovnictvo</xsl:if></xsl:variable>
  <xsl:variable name="loc-catpotrav"><xsl:if test="$lang='cz'">potravinářství</xsl:if><xsl:if test="$lang='en'">food industry</xsl:if><xsl:if test="$lang='sk'">potravinárstvo</xsl:if></xsl:variable>
  <xsl:variable name="loc-catpráv"><xsl:if test="$lang='cz'">právo, právnictví</xsl:if><xsl:if test="$lang='en'">law, jirisprudence</xsl:if><xsl:if test="$lang='sk'">právo</xsl:if></xsl:variable>
  <xsl:variable name="loc-catprům"><xsl:if test="$lang='cz'">průmysl</xsl:if><xsl:if test="$lang='en'">industry</xsl:if><xsl:if test="$lang='sk'">priemysel</xsl:if></xsl:variable>
  <xsl:variable name="loc-catpřír"><xsl:if test="$lang='cz'">příroda</xsl:if><xsl:if test="$lang='en'">nature</xsl:if><xsl:if test="$lang='sk'">príroda</xsl:if></xsl:variable>
  <xsl:variable name="loc-catpsych"><xsl:if test="$lang='cz'">psychologie</xsl:if><xsl:if test="$lang='en'">psychology</xsl:if><xsl:if test="$lang='sk'">psychológie</xsl:if></xsl:variable>
  <xsl:variable name="loc-catrybn"><xsl:if test="$lang='cz'">rybníkářství</xsl:if><xsl:if test="$lang='en'">fish farming</xsl:if><xsl:if test="$lang='sk'">rybnikarstvi</xsl:if></xsl:variable>
  <xsl:variable name="loc-catřem"><xsl:if test="$lang='cz'">řemeslo</xsl:if><xsl:if test="$lang='en'">craft</xsl:if><xsl:if test="$lang='sk'">remeslo</xsl:if></xsl:variable>
  <xsl:variable name="loc-catsklář"><xsl:if test="$lang='cz'">sklářství</xsl:if><xsl:if test="$lang='en'">glassblowing</xsl:if><xsl:if test="$lang='sk'">sklárstvo</xsl:if></xsl:variable>
  <xsl:variable name="loc-catsoc"><xsl:if test="$lang='cz'">socialismus</xsl:if><xsl:if test="$lang='en'">socialism</xsl:if><xsl:if test="$lang='sk'">socializmus</xsl:if></xsl:variable>
  <xsl:variable name="loc-catsociol"><xsl:if test="$lang='cz'">sociologie</xsl:if><xsl:if test="$lang='en'">sociology</xsl:if><xsl:if test="$lang='sk'">sociológia</xsl:if></xsl:variable>
  <xsl:variable name="loc-catstat"><xsl:if test="$lang='cz'">statistika</xsl:if><xsl:if test="$lang='en'">statistics</xsl:if><xsl:if test="$lang='sk'">štatistika</xsl:if></xsl:variable>
  <xsl:variable name="loc-catstav"><xsl:if test="$lang='cz'">stavitelství</xsl:if><xsl:if test="$lang='en'">civil engeneering</xsl:if><xsl:if test="$lang='sk'">staviteľstvo</xsl:if></xsl:variable>
  <xsl:variable name="loc-catškol"><xsl:if test="$lang='cz'">školství</xsl:if><xsl:if test="$lang='en'">education</xsl:if><xsl:if test="$lang='sk'">školstvo</xsl:if></xsl:variable>
  <xsl:variable name="loc-cattech"><xsl:if test="$lang='cz'">technika</xsl:if><xsl:if test="$lang='en'">technology</xsl:if><xsl:if test="$lang='sk'">technika</xsl:if></xsl:variable>
  <xsl:variable name="loc-cattěl"><xsl:if test="$lang='cz'">tělovýchova</xsl:if><xsl:if test="$lang='en'">physical education</xsl:if><xsl:if test="$lang='sk'">telovýchova</xsl:if></xsl:variable>
  <xsl:variable name="loc-cattext"><xsl:if test="$lang='cz'">textilnictví</xsl:if><xsl:if test="$lang='en'">textile industry</xsl:if><xsl:if test="$lang='sk'">textilníctvo</xsl:if></xsl:variable>
  <xsl:variable name="loc-catúč"><xsl:if test="$lang='cz'">účetnictví</xsl:if><xsl:if test="$lang='en'">accountancy</xsl:if><xsl:if test="$lang='sk'">účtovníctvo</xsl:if></xsl:variable>
  <xsl:variable name="loc-catúř"><xsl:if test="$lang='cz'">úřední výraz</xsl:if><xsl:if test="$lang='en'">official expression</xsl:if><xsl:if test="$lang='sk'">úradné výraz</xsl:if></xsl:variable>
  <xsl:variable name="loc-catveřspr"><xsl:if test="$lang='cz'">veřejná správa</xsl:if><xsl:if test="$lang='en'">public administration</xsl:if><xsl:if test="$lang='sk'">verejná správa</xsl:if></xsl:variable>
  <xsl:variable name="loc-catvet"><xsl:if test="$lang='cz'">veterinářství</xsl:if><xsl:if test="$lang='en'">veterinary</xsl:if><xsl:if test="$lang='sk'">veterinárstva</xsl:if></xsl:variable>
  <xsl:variable name="loc-catvoj"><xsl:if test="$lang='cz'">vojenství</xsl:if><xsl:if test="$lang='en'">military</xsl:if><xsl:if test="$lang='sk'">vojenstvo</xsl:if></xsl:variable>
  <xsl:variable name="loc-catvýptech"><xsl:if test="$lang='cz'">výpočetní technika</xsl:if><xsl:if test="$lang='en'">computer technology</xsl:if><xsl:if test="$lang='sk'">výpočtová technika</xsl:if></xsl:variable>
  <xsl:variable name="loc-catvýr"><xsl:if test="$lang='cz'">výroba</xsl:if><xsl:if test="$lang='en'">production</xsl:if><xsl:if test="$lang='sk'">výroba</xsl:if></xsl:variable>
  <xsl:variable name="loc-catvýtv"><xsl:if test="$lang='cz'">výtvarnictví</xsl:if><xsl:if test="$lang='en'">design</xsl:if><xsl:if test="$lang='sk'">výtvarníctva</xsl:if></xsl:variable>
  <xsl:variable name="loc-catzahr"><xsl:if test="$lang='cz'">zahradnictví</xsl:if><xsl:if test="$lang='en'">horticulture</xsl:if><xsl:if test="$lang='sk'">záhradníctvo</xsl:if></xsl:variable>
  <xsl:variable name="loc-catzbož"><xsl:if test="$lang='cz'">zbožíznalství</xsl:if><xsl:if test="$lang='en'">merchandise</xsl:if><xsl:if test="$lang='sk'">tovaroznalectvo</xsl:if></xsl:variable>
  <xsl:variable name="loc-catzeměd"><xsl:if test="$lang='cz'">zemědělství</xsl:if><xsl:if test="$lang='en'">agriculture</xsl:if><xsl:if test="$lang='sk'">poľnohospodárstvo</xsl:if></xsl:variable>
  <xsl:variable name="loc-catzeměp"><xsl:if test="$lang='cz'">zeměpis</xsl:if><xsl:if test="$lang='en'">geography</xsl:if><xsl:if test="$lang='sk'">zemepis</xsl:if></xsl:variable>
  <xsl:variable name="loc-catzool"><xsl:if test="$lang='cz'">zoologie</xsl:if><xsl:if test="$lang='en'">zoology</xsl:if><xsl:if test="$lang='sk'">zoológia</xsl:if></xsl:variable>
  <xsl:variable name="loc-catcirkev"><xsl:if test="$lang='cz'">církevní výraz</xsl:if><xsl:if test="$lang='en'">religious expression</xsl:if><xsl:if test="$lang='sk'">cirkevné výraz</xsl:if></xsl:variable>
  <xsl:variable name="loc-priklady"><xsl:if test="$lang='cz'">Příklady užití</xsl:if><xsl:if test="$lang='en'">Examples of use</xsl:if><xsl:if test="$lang='sk'">Príklady použitia</xsl:if></xsl:variable>
  <xsl:variable name="loc-vztahy"><xsl:if test="$lang='cz'">Vztahy</xsl:if><xsl:if test="$lang='en'">Relations</xsl:if><xsl:if test="$lang='sk'">vzťahy</xsl:if></xsl:variable>
  <xsl:variable name="loc-preklad"><xsl:if test="$lang='cz'">Překlad</xsl:if><xsl:if test="$lang='en'">Translations</xsl:if><xsl:if test="$lang='sk'">preklad</xsl:if></xsl:variable>
  <xsl:variable name="loc-neovereno"><xsl:if test="$lang='cz'">Neověřený překlad</xsl:if><xsl:if test="$lang='en'">Not approved translation</xsl:if><xsl:if test="$lang='sk'">Neoverený preklad</xsl:if></xsl:variable>
  <xsl:variable name="loc-vevyznamu"><xsl:if test="$lang='cz'">ve významu</xsl:if><xsl:if test="$lang='en'">meaning</xsl:if><xsl:if test="$lang='sk'">vo význame</xsl:if></xsl:variable>
  <xsl:variable name="loc-vevyz"><xsl:if test="$lang='cz'">ve výz.</xsl:if><xsl:if test="$lang='en'">mean.</xsl:if><xsl:if test="$lang='sk'">vo výz.</xsl:if></xsl:variable>
  <xsl:variable name="loc-synonym"><xsl:if test="$lang='cz'">synonymum</xsl:if><xsl:if test="$lang='en'">synonym</xsl:if><xsl:if test="$lang='sk'">synonymum</xsl:if></xsl:variable>
  <xsl:variable name="loc-synonym_strategie"><xsl:if test="$lang='cz'">syn. strategie</xsl:if><xsl:if test="$lang='en'">syn. strategy</xsl:if><xsl:if test="$lang='sk'">syn. stratégia</xsl:if></xsl:variable>
  <xsl:variable name="loc-antonym"><xsl:if test="$lang='cz'">antonymum</xsl:if><xsl:if test="$lang='en'">antonym</xsl:if><xsl:if test="$lang='sk'">antonymum</xsl:if></xsl:variable>
  <xsl:variable name="loc-hyponym"><xsl:if test="$lang='cz'">hyponymum</xsl:if><xsl:if test="$lang='en'">homonym</xsl:if><xsl:if test="$lang='sk'">hyponymum</xsl:if></xsl:variable>
  <xsl:variable name="loc-hyperonym"><xsl:if test="$lang='cz'">hyperonym.</xsl:if><xsl:if test="$lang='en'">hyperonym.</xsl:if><xsl:if test="$lang='sk'">hyperonym.</xsl:if></xsl:variable>
  <xsl:variable name="loc-preklady"><xsl:if test="$lang='cz'">Překlady</xsl:if><xsl:if test="$lang='en'">Translations</xsl:if><xsl:if test="$lang='sk'">preklady</xsl:if></xsl:variable>
  <xsl:variable name="loc-prekladyzobr"><xsl:if test="$lang='cz'">zobrazit překlady</xsl:if><xsl:if test="$lang='en'">Show translations</xsl:if><xsl:if test="$lang='sk'">zobraziť preklady</xsl:if></xsl:variable>
  <xsl:variable name="loc-prekladyskryt"><xsl:if test="$lang='cz'">skrýt překlady</xsl:if><xsl:if test="$lang='en'">Hide translations</xsl:if><xsl:if test="$lang='sk'">skryť preklady</xsl:if></xsl:variable>
  <xsl:variable name="loc-mistoneutral"><xsl:if test="$lang='cz'">neutrální prostor</xsl:if><xsl:if test="$lang='en'">Neutral space</xsl:if><xsl:if test="$lang='sk'">neutrálny priestor</xsl:if></xsl:variable>
  <xsl:variable name="loc-mistohlava"><xsl:if test="$lang='cz'">hlava</xsl:if><xsl:if test="$lang='en'">Head</xsl:if><xsl:if test="$lang='sk'">hlava</xsl:if></xsl:variable>
  <xsl:variable name="loc-mistooblicej"><xsl:if test="$lang='cz'">- obličej (neutr. prostor před obl.)</xsl:if><xsl:if test="$lang='en'"> - face</xsl:if><xsl:if test="$lang='sk'"> - tvár (neutr. Priestor pred obl.)</xsl:if></xsl:variable>
  <xsl:variable name="loc-mistotemeno"><xsl:if test="$lang='cz'">- temeno hlavy (nad hlavou)</xsl:if><xsl:if test="$lang='en'"> - top of the head (above the head)</xsl:if><xsl:if test="$lang='sk'"> - temeno hlavy (nad hlavou)</xsl:if></xsl:variable>
  <xsl:variable name="loc-mistocelo"><xsl:if test="$lang='cz'">- čelo</xsl:if><xsl:if test="$lang='en'"> - forehead</xsl:if><xsl:if test="$lang='sk'"> - čelo</xsl:if></xsl:variable>
  <xsl:variable name="loc-mistooci"><xsl:if test="$lang='cz'">- oči</xsl:if><xsl:if test="$lang='en'"> - eyes</xsl:if><xsl:if test="$lang='sk'"> - oči</xsl:if></xsl:variable>
  <xsl:variable name="loc-mistonos"><xsl:if test="$lang='cz'">- nos</xsl:if><xsl:if test="$lang='en'"> - nose</xsl:if><xsl:if test="$lang='sk'"> - nos</xsl:if></xsl:variable>
  <xsl:variable name="loc-mistousi"><xsl:if test="$lang='cz'">- uši</xsl:if><xsl:if test="$lang='en'"> - ears</xsl:if><xsl:if test="$lang='sk'"> - uši</xsl:if></xsl:variable>
  <xsl:variable name="loc-mistotvare"><xsl:if test="$lang='cz'">- tváře</xsl:if><xsl:if test="$lang='en'"> - cheeks</xsl:if><xsl:if test="$lang='sk'"> - tváre</xsl:if></xsl:variable>
  <xsl:variable name="loc-mistousta"><xsl:if test="$lang='cz'">- ústa</xsl:if><xsl:if test="$lang='en'"> - mouth</xsl:if><xsl:if test="$lang='sk'"> - ústa</xsl:if></xsl:variable>
  <xsl:variable name="loc-mistobrada"><xsl:if test="$lang='cz'">- brada</xsl:if><xsl:if test="$lang='en'"> - chin</xsl:if><xsl:if test="$lang='sk'"> - brada</xsl:if></xsl:variable>
  <xsl:variable name="loc-mistokrk"><xsl:if test="$lang='cz'">- krk</xsl:if><xsl:if test="$lang='en'"> - neck</xsl:if><xsl:if test="$lang='sk'"> - krk</xsl:if></xsl:variable>
  <xsl:variable name="loc-mistohrud"><xsl:if test="$lang='cz'">horní pol. trupu (hruď)</xsl:if><xsl:if test="$lang='en'">chest</xsl:if><xsl:if test="$lang='sk'">hornú pol. trupu (hruď)</xsl:if></xsl:variable>
  <xsl:variable name="loc-mistopaze"><xsl:if test="$lang='cz'">paže</xsl:if><xsl:if test="$lang='en'">arms</xsl:if><xsl:if test="$lang='sk'">paže</xsl:if></xsl:variable>
  <xsl:variable name="loc-mistoruka"><xsl:if test="$lang='cz'">ruka</xsl:if><xsl:if test="$lang='en'">hand</xsl:if><xsl:if test="$lang='sk'">ruka</xsl:if></xsl:variable>
  <xsl:variable name="loc-mistopas"><xsl:if test="$lang='cz'">dolní pol. trupu (břicho, pas)</xsl:if><xsl:if test="$lang='en'">lower body </xsl:if><xsl:if test="$lang='sk'">dolnú pol. trupu (brucho, pas)</xsl:if></xsl:variable>
  <xsl:variable name="loc-mistodolni"><xsl:if test="$lang='cz'">dolní část těla</xsl:if><xsl:if test="$lang='en'">legs</xsl:if><xsl:if test="$lang='sk'">dolná časť tela</xsl:if></xsl:variable>
  <xsl:variable name="loc-regcr"><xsl:if test="$lang='cz'">celá ČR</xsl:if><xsl:if test="$lang='en'">Entire Czech Republic</xsl:if><xsl:if test="$lang='sk'">celá ČR</xsl:if></xsl:variable>
  <xsl:variable name="loc-regpraha"><xsl:if test="$lang='cz'">Praha a okolí</xsl:if><xsl:if test="$lang='en'">Prague and surroundings</xsl:if><xsl:if test="$lang='sk'">Praha a okolie</xsl:if></xsl:variable>
  <xsl:variable name="loc-regplzen"><xsl:if test="$lang='cz'">Plzeň a okolí</xsl:if><xsl:if test="$lang='en'">Brno and surroundings</xsl:if><xsl:if test="$lang='sk'">Plzeň a okolí</xsl:if></xsl:variable>
  <xsl:variable name="loc-regbrno"><xsl:if test="$lang='cz'">Brno a okolí</xsl:if><xsl:if test="$lang='en'">Pilsen and surroundings</xsl:if><xsl:if test="$lang='sk'">Brno a okolí</xsl:if></xsl:variable>
  <xsl:variable name="loc-regvm"><xsl:if test="$lang='cz'">Valašské Meziříčí a okolí</xsl:if><xsl:if test="$lang='en'">Valašské Meziříčí and surroundings</xsl:if><xsl:if test="$lang='sk'">Valašské Meziříčí a okolie</xsl:if></xsl:variable>
  <xsl:variable name="loc-reghk"><xsl:if test="$lang='cz'">Hradec Králové a okolí</xsl:if><xsl:if test="$lang='en'">Hradec Králové and surroundings</xsl:if><xsl:if test="$lang='sk'">Hradec Králové a okolie</xsl:if></xsl:variable>
  <xsl:variable name="loc-regkr"><xsl:if test="$lang='cz'">Kroměříž a okolí</xsl:if><xsl:if test="$lang='en'">Kroměříž and surroundings</xsl:if><xsl:if test="$lang='sk'">Kroměříž a okolie</xsl:if></xsl:variable>
  <xsl:variable name="loc-regcechy"><xsl:if test="$lang='cz'">Čechy</xsl:if><xsl:if test="$lang='en'">Bohemia</xsl:if><xsl:if test="$lang='sk'">Čechy</xsl:if></xsl:variable>
  <xsl:variable name="loc-regmorava"><xsl:if test="$lang='cz'">Morava</xsl:if><xsl:if test="$lang='en'">Moravia</xsl:if><xsl:if test="$lang='sk'">Morava</xsl:if></xsl:variable>
  <xsl:variable name="loc-regjih"><xsl:if test="$lang='cz'">Jihlava a okolí</xsl:if><xsl:if test="$lang='en'">Jihlava and surroundings</xsl:if><xsl:if test="$lang='sk'">Jihlava a okolie</xsl:if></xsl:variable>
  <xsl:variable name="loc-regzl"><xsl:if test="$lang='cz'">Zlín a okolí</xsl:if><xsl:if test="$lang='en'">Zlín and surroundings</xsl:if><xsl:if test="$lang='sk'">Zlín a okolie</xsl:if></xsl:variable>
  <xsl:variable name="loc-regcb"><xsl:if test="$lang='cz'">České Budějovice a okolí</xsl:if><xsl:if test="$lang='en'">České Budějovice and surroundings</xsl:if><xsl:if test="$lang='sk'">České Budějovice a okolie</xsl:if></xsl:variable>
  <xsl:variable name="loc-regot"><xsl:if test="$lang='cz'">Ostrava a okolí</xsl:if><xsl:if test="$lang='en'">Ostrava and surroundings</xsl:if><xsl:if test="$lang='sk'">Ostrava a okolie</xsl:if></xsl:variable>
  <xsl:variable name="loc-regol"><xsl:if test="$lang='cz'">Olomouc a okolí</xsl:if><xsl:if test="$lang='en'">Olomouc and surroundings</xsl:if><xsl:if test="$lang='sk'">Olomouc a okolie</xsl:if></xsl:variable>
  <xsl:variable name="loc-regul"><xsl:if test="$lang='cz'">Ústí nad Labem a okolí</xsl:if><xsl:if test="$lang='en'">Ústí nad Labem and surroundings</xsl:if><xsl:if test="$lang='sk'">Ústí nad Labem a okolie</xsl:if></xsl:variable>
  <xsl:variable name="loc-reglib"><xsl:if test="$lang='cz'">Liberec a okolí</xsl:if><xsl:if test="$lang='en'">Liberec and surroundings</xsl:if><xsl:if test="$lang='sk'">Liberec a okolie</xsl:if></xsl:variable>
  <xsl:variable name="loc-regslovensko"><xsl:if test="$lang='cz'">Slovensko</xsl:if><xsl:if test="$lang='en'">Slovakia</xsl:if><xsl:if test="$lang='sk'">Slovensko</xsl:if></xsl:variable>
  <xsl:variable name="loc-genmlada"><xsl:if test="$lang='cz'">mládež</xsl:if><xsl:if test="$lang='en'">youth</xsl:if><xsl:if test="$lang='sk'">mladá generácia</xsl:if></xsl:variable>
  <xsl:variable name="loc-genstredni"><xsl:if test="$lang='cz'">lidé středního věku</xsl:if><xsl:if test="$lang='en'">middle age people</xsl:if><xsl:if test="$lang='sk'">stredná generácia</xsl:if></xsl:variable>
  <xsl:variable name="loc-genstarsi"><xsl:if test="$lang='cz'">senioři</xsl:if><xsl:if test="$lang='en'">elderly people</xsl:if><xsl:if test="$lang='sk'">stará generácie</xsl:if></xsl:variable>
  <xsl:variable name="loc-gendeti"><xsl:if test="$lang='cz'">děti (dětský znak)</xsl:if><xsl:if test="$lang='en'">children</xsl:if><xsl:if test="$lang='sk'">deti (detský znak)</xsl:if></xsl:variable>
  <xsl:variable name="loc-skupintr"><xsl:if test="$lang='cz'">intranzitivní</xsl:if><xsl:if test="$lang='en'">intransitive</xsl:if><xsl:if test="$lang='sk'">intranzitívne</xsl:if></xsl:variable>
  <xsl:variable name="loc-skuptran"><xsl:if test="$lang='cz'">tranzitivní</xsl:if><xsl:if test="$lang='en'">transitive</xsl:if><xsl:if test="$lang='sk'">tranzitívne</xsl:if></xsl:variable>
  <xsl:variable name="loc-skuppohyb"><xsl:if test="$lang='cz'">vyjadřující pohyb předmětu v prostoru</xsl:if><xsl:if test="$lang='en'">expressing movement of object in space</xsl:if><xsl:if test="$lang='sk'">vyjadrujúca pohyb predmetu v priestore</xsl:if></xsl:variable>
  <xsl:variable name="loc-skupmisto"><xsl:if test="$lang='cz'">určující místo</xsl:if><xsl:if test="$lang='en'">identifzing place</xsl:if><xsl:if test="$lang='sk'">určujúce miesto</xsl:if></xsl:variable>
  <xsl:variable name="loc-skupprost"><xsl:if test="$lang='cz'">vyjadřující prostředek</xsl:if><xsl:if test="$lang='en'">expressing means</xsl:if><xsl:if test="$lang='sk'">vyjadrujúca prostriedok</xsl:if></xsl:variable>
  <xsl:variable name="loc-skupsubj"><xsl:if test="$lang='cz'">(subjektově shodové)</xsl:if><xsl:if test="$lang='en'">indicating subject</xsl:if><xsl:if test="$lang='sk'">(subjektovo zhodový)</xsl:if></xsl:variable>
  <xsl:variable name="loc-skupobj"><xsl:if test="$lang='cz'">(objektově shodové)</xsl:if><xsl:if test="$lang='en'">indicating object</xsl:if><xsl:if test="$lang='sk'">(objektovo zhodový)</xsl:if></xsl:variable>
  <xsl:variable name="loc-skupreci"><xsl:if test="$lang='cz'">reciproční</xsl:if><xsl:if test="$lang='en'">reciprocal</xsl:if><xsl:if test="$lang='sk'">recipročné</xsl:if></xsl:variable>
  <xsl:variable name="loc-skuppolo"><xsl:if test="$lang='cz'">poloshodové</xsl:if><xsl:if test="$lang='en'">half-indicating</xsl:if><xsl:if test="$lang='sk'">polozhodové</xsl:if></xsl:variable>
  <xsl:variable name="loc-skuplok"><xsl:if test="$lang='cz'">lokativní</xsl:if><xsl:if test="$lang='en'">locative</xsl:if><xsl:if test="$lang='sk'">lokativná</xsl:if></xsl:variable>
  <xsl:variable name="loc-skuppok"><xsl:if test="$lang='cz'">pokrčené</xsl:if><xsl:if test="$lang='en'">bent</xsl:if><xsl:if test="$lang='sk'">pokrčená</xsl:if></xsl:variable>
  <xsl:variable name="loc-skupdej"><xsl:if test="$lang='cz'">dějové</xsl:if><xsl:if test="$lang='en'">of process</xsl:if><xsl:if test="$lang='sk'">dejové</xsl:if></xsl:variable>
  <xsl:variable name="loc-skupfre"><xsl:if test="$lang='cz'">frekvenční</xsl:if><xsl:if test="$lang='en'">of frequency</xsl:if><xsl:if test="$lang='sk'">frekvenčná</xsl:if></xsl:variable>
  <xsl:variable name="loc-skupmir"><xsl:if test="$lang='cz'">míry</xsl:if><xsl:if test="$lang='en'">of rate</xsl:if><xsl:if test="$lang='sk'">miery</xsl:if></xsl:variable>
  <xsl:variable name="loc-skupobr"><xsl:if test="$lang='cz'">obrácená</xsl:if><xsl:if test="$lang='en'">reversed</xsl:if><xsl:if test="$lang='sk'">obrátená</xsl:if></xsl:variable>
  <xsl:variable name="loc-skupiko"><xsl:if test="$lang='cz'">ikonická</xsl:if><xsl:if test="$lang='en'">iconic</xsl:if><xsl:if test="$lang='sk'">ikonická</xsl:if></xsl:variable>
  <xsl:variable name="loc-skupspec"><xsl:if test="$lang='cz'">specifická</xsl:if><xsl:if test="$lang='en'">specific</xsl:if><xsl:if test="$lang='sk'">špecifická</xsl:if></xsl:variable>
  <xsl:variable name="loc-skup3redup"><xsl:if test="$lang='cz'">reduplikací</xsl:if><xsl:if test="$lang='en'">by reduplication</xsl:if><xsl:if test="$lang='sk'">reduplikáciou</xsl:if></xsl:variable>
  <xsl:variable name="loc-skup3prof"><xsl:if test="$lang='cz'">přidáním reduplikovaného klasifikátoru nebo zájmena</xsl:if><xsl:if test="$lang='en'">by adding proform (pronoun, classificators)</xsl:if><xsl:if test="$lang='sk'">pridaním reduplikovaného klasifikátora alebo zámená</xsl:if></xsl:variable>
  <xsl:variable name="loc-skup3kvan"><xsl:if test="$lang='cz'">přidáním kvantifikátoru</xsl:if><xsl:if test="$lang='en'">by adding quantificator</xsl:if><xsl:if test="$lang='sk'">pridaním kvantifikátora</xsl:if></xsl:variable>
  <xsl:variable name="loc-skup3ink"><xsl:if test="$lang='cz'">inkorporací číselných morfémů</xsl:if><xsl:if test="$lang='en'">by adding quantificator</xsl:if><xsl:if test="$lang='sk'">inkorporáciou číselných morfémov</xsl:if></xsl:variable>
  <xsl:variable name="loc-skup3plur"><xsl:if test="$lang='cz'">přidáním plurálového specifikátoru</xsl:if><xsl:if test="$lang='en'">by adding plural specificator</xsl:if><xsl:if test="$lang='sk'">pridaním plurálového špecifikátora</xsl:if></xsl:variable>
  <xsl:variable name="loc-skup3redo"><xsl:if test="$lang='cz'">reduplikace vyjadřuje opakování děje</xsl:if><xsl:if test="$lang='en'">reduplication expresses process repetition</xsl:if><xsl:if test="$lang='sk'">reduplikácia vyjadruje opakovaní deja</xsl:if></xsl:variable>
  <xsl:variable name="loc-skup3redt"><xsl:if test="$lang='cz'">reduplikace vyjadřuje trvání děje</xsl:if><xsl:if test="$lang='en'">reduplication expresses process duration</xsl:if><xsl:if test="$lang='sk'">reduplikácia vyjadruje trvanie deja</xsl:if></xsl:variable>
  <xsl:variable name="loc-skup3okol"><xsl:if test="$lang='cz'">sloveso vyjadřuje okolnosti děje</xsl:if><xsl:if test="$lang='en'">verb expresses process circumstances</xsl:if><xsl:if test="$lang='sk'">sloveso vyjadruje okolnosti deja</xsl:if></xsl:variable>
  <xsl:variable name="loc-skup3rt"><xsl:if test="$lang='cz'">rychlá reduplikace vyjadřuje trvání děje</xsl:if><xsl:if test="$lang='en'">quick reduplication expresses process duration</xsl:if><xsl:if test="$lang='sk'">rýchla reduplikácia vyjadruje trvania deja</xsl:if></xsl:variable>
  <xsl:variable name="loc-skup3rp"><xsl:if test="$lang='cz'">rychlá reduplikace vyjadřuje pravidelnost</xsl:if><xsl:if test="$lang='en'">quick reduplication expresses regularity</xsl:if><xsl:if test="$lang='sk'">rýchla reduplikácia vyjadruje pravidelnosť</xsl:if></xsl:variable>
  <xsl:variable name="loc-skup3pp"><xsl:if test="$lang='cz'">pomalá reduplikace vyjadřuje pokračování děje</xsl:if><xsl:if test="$lang='en'">slow reduplication expresses process continuation</xsl:if><xsl:if test="$lang='sk'">pomalá reduplikácia vyjadruje pokračovanie deja</xsl:if></xsl:variable>
  <xsl:variable name="loc-skup3po"><xsl:if test="$lang='cz'">pomalá reduplikace vyjadřuje opakování děje</xsl:if><xsl:if test="$lang='en'">slow reduplication expresses process repetition</xsl:if><xsl:if test="$lang='sk'">pomalá reduplikácia vyjadruje opakovanie deja</xsl:if></xsl:variable>
  <xsl:variable name="loc-skup3netvori"><xsl:if test="$lang='cz'">netvoří plurál</xsl:if><xsl:if test="$lang='en'">no plural form</xsl:if><xsl:if test="$lang='sk'">netvorí plurál</xsl:if></xsl:variable>
  <xsl:variable name="loc-skup3redupklas"><xsl:if test="$lang='cz'">reduplikací klasifikátoru resp. specifikátoru</xsl:if><xsl:if test="$lang='en'">by reduplicating classificator / speficicator</xsl:if><xsl:if test="$lang='sk'">reduplikáciou klasifikátora resp. špecifikátora</xsl:if></xsl:variable>
  <xsl:variable name="loc-schvalit"><xsl:if test="$lang='cz'">schválit</xsl:if><xsl:if test="$lang='en'">approve</xsl:if><xsl:if test="$lang='sk'">schváliť</xsl:if></xsl:variable>
  <xsl:variable name="loc-hromadne"><xsl:if test="$lang='cz'">schválit hromadně</xsl:if><xsl:if test="$lang='en'">approve all</xsl:if><xsl:if test="$lang='sk'">schváliť hromadne</xsl:if></xsl:variable>
  <xsl:variable name="loc-predpona"><xsl:if test="$lang='cz'">předpona</xsl:if><xsl:if test="$lang='en'">prefix</xsl:if><xsl:if test="$lang='sk'">predpona</xsl:if></xsl:variable>
  <xsl:variable name="loc-typcollocation"><xsl:if test="$lang='cz'">ustálené slovní spojení/frazém</xsl:if><xsl:if test="$lang='en'">collocation</xsl:if><xsl:if test="$lang='sk'">ustálené slovné spojenie / fráz</xsl:if></xsl:variable>
  <xsl:variable name="loc-prislovi"><xsl:if test="$lang='cz'">přísloví</xsl:if><xsl:if test="$lang='en'">proverb</xsl:if><xsl:if test="$lang='sk'">príslovia</xsl:if></xsl:variable>
  <xsl:variable name="loc-zobrazitautory"><xsl:if test="$lang='cz'">zobrazit/skrýt autory a zdroje</xsl:if><xsl:if test="$lang='en'">show/hide authors and sources</xsl:if><xsl:if test="$lang='sk'">zobraziť / skryť autori a zdroje</xsl:if></xsl:variable>
  <xsl:variable name="loc-schvaleno"><xsl:if test="$lang='cz'">schváleno</xsl:if><xsl:if test="$lang='en'">approved</xsl:if><xsl:if test="$lang='sk'">schválené</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodslova"><xsl:if test="$lang='cz'">Původ slova</xsl:if><xsl:if test="$lang='en'">Origin</xsl:if><xsl:if test="$lang='sk'">pôvod slova</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvoda"><xsl:if test="$lang='cz'">angličtina</xsl:if><xsl:if test="$lang='en'">English</xsl:if><xsl:if test="$lang='sk'">angličtina</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodafr"><xsl:if test="$lang='cz'">africké jazyky</xsl:if><xsl:if test="$lang='en'">African languages</xsl:if><xsl:if test="$lang='sk'">africké jazyky</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodalb"><xsl:if test="$lang='cz'">albánština</xsl:if><xsl:if test="$lang='en'">Albanian</xsl:if><xsl:if test="$lang='sk'">albánčina</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodam"><xsl:if test="$lang='cz'">americká angličtina</xsl:if><xsl:if test="$lang='en'">American English</xsl:if><xsl:if test="$lang='sk'">americká angličtina</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodar"><xsl:if test="$lang='cz'">arabština</xsl:if><xsl:if test="$lang='en'">Arabic</xsl:if><xsl:if test="$lang='sk'">arabčina</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodaram"><xsl:if test="$lang='cz'">aramejština</xsl:if><xsl:if test="$lang='en'">Aramaic</xsl:if><xsl:if test="$lang='sk'">aramejčina</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodb"><xsl:if test="$lang='cz'">bulharština</xsl:if><xsl:if test="$lang='en'">Bulgarian</xsl:if><xsl:if test="$lang='sk'">bulharčina</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodbantu"><xsl:if test="$lang='cz'">bantuské jazyky</xsl:if><xsl:if test="$lang='en'">Bantu languages</xsl:if><xsl:if test="$lang='sk'">bantuské jazyky</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodbask"><xsl:if test="$lang='cz'">baskičtina</xsl:if><xsl:if test="$lang='en'">Basque</xsl:if><xsl:if test="$lang='sk'">baskičtina</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodbr"><xsl:if test="$lang='cz'">běloruština</xsl:if><xsl:if test="$lang='en'">Belarusian</xsl:if><xsl:if test="$lang='sk'">bieloruština</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodcik"><xsl:if test="$lang='cz'">cikánština</xsl:if><xsl:if test="$lang='en'">gypsy</xsl:if><xsl:if test="$lang='sk'">cikánština</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodčes"><xsl:if test="$lang='cz'">čeština</xsl:if><xsl:if test="$lang='en'">Czech</xsl:if><xsl:if test="$lang='sk'">Čeština</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodčín"><xsl:if test="$lang='cz'">čínština</xsl:if><xsl:if test="$lang='en'">Chinese</xsl:if><xsl:if test="$lang='sk'">čínština</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvoddán"><xsl:if test="$lang='cz'">dánština</xsl:if><xsl:if test="$lang='en'">Danish</xsl:if><xsl:if test="$lang='sk'">dánčina</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodeg"><xsl:if test="$lang='cz'">egyptština</xsl:if><xsl:if test="$lang='en'">Egyptian</xsl:if><xsl:if test="$lang='sk'">egyptčine</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodest"><xsl:if test="$lang='cz'">estonština</xsl:if><xsl:if test="$lang='en'">Estonian</xsl:if><xsl:if test="$lang='sk'">estónčina</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodf"><xsl:if test="$lang='cz'">francouzština</xsl:if><xsl:if test="$lang='en'">French</xsl:if><xsl:if test="$lang='sk'">Francúzsky</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodfin"><xsl:if test="$lang='cz'">finština</xsl:if><xsl:if test="$lang='en'">Finnish</xsl:if><xsl:if test="$lang='sk'">fínčina</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodg"><xsl:if test="$lang='cz'">germánské jazyky</xsl:if><xsl:if test="$lang='en'">Germanic languages</xsl:if><xsl:if test="$lang='sk'">germánske jazyky</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodhebr"><xsl:if test="$lang='cz'">hebrejština</xsl:if><xsl:if test="$lang='en'">Hebrew</xsl:if><xsl:if test="$lang='sk'">hebrejčina</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodhind"><xsl:if test="$lang='cz'">hindština</xsl:if><xsl:if test="$lang='en'">Hindi</xsl:if><xsl:if test="$lang='sk'">hindčina</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodi"><xsl:if test="$lang='cz'">italština</xsl:if><xsl:if test="$lang='en'">Italian</xsl:if><xsl:if test="$lang='sk'">taliančina</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodind"><xsl:if test="$lang='cz'">indické jazyky</xsl:if><xsl:if test="$lang='en'">Indian languages</xsl:if><xsl:if test="$lang='sk'">indické jazyky</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodindián"><xsl:if test="$lang='cz'">indiánské jazyky</xsl:if><xsl:if test="$lang='en'">Indian languages</xsl:if><xsl:if test="$lang='sk'">indiánske jazyky</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodindonés"><xsl:if test="$lang='cz'">indonéština</xsl:if><xsl:if test="$lang='en'">Indonesian</xsl:if><xsl:if test="$lang='sk'">indonézština</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodir"><xsl:if test="$lang='cz'">irština</xsl:if><xsl:if test="$lang='en'">Irish</xsl:if><xsl:if test="$lang='sk'">írčina</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodisland"><xsl:if test="$lang='cz'">islandština</xsl:if><xsl:if test="$lang='en'">Icelandic</xsl:if><xsl:if test="$lang='sk'">islandčina</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodj"><xsl:if test="$lang='cz'">japonština</xsl:if><xsl:if test="$lang='en'">Japanese</xsl:if><xsl:if test="$lang='sk'">japončina</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodjakut"><xsl:if test="$lang='cz'">jakutština</xsl:if><xsl:if test="$lang='en'">Jakut</xsl:if><xsl:if test="$lang='sk'">Jakutčina</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodjslov"><xsl:if test="$lang='cz'">jihoslovanské jazyky</xsl:if><xsl:if test="$lang='en'">South Slavic languages</xsl:if><xsl:if test="$lang='sk'">južnoslovanské jazyky</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodkelt"><xsl:if test="$lang='cz'">keltské jazyky</xsl:if><xsl:if test="$lang='en'">Celtic languages</xsl:if><xsl:if test="$lang='sk'">keltské jazyky</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodkorej"><xsl:if test="$lang='cz'">korejština</xsl:if><xsl:if test="$lang='en'">Korean</xsl:if><xsl:if test="$lang='sk'">kórejčina</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodl"><xsl:if test="$lang='cz'">latina</xsl:if><xsl:if test="$lang='en'">Latin</xsl:if><xsl:if test="$lang='sk'">latinčina</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodlfran"><xsl:if test="$lang='cz'">latina přes francouzštinu</xsl:if><xsl:if test="$lang='en'">Latin over French</xsl:if><xsl:if test="$lang='sk'">latina cez francúzštinu</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodlapon"><xsl:if test="$lang='cz'">laponština</xsl:if><xsl:if test="$lang='en'">Lapland</xsl:if><xsl:if test="$lang='sk'">a Sami</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodlit"><xsl:if test="$lang='cz'">litevština</xsl:if><xsl:if test="$lang='en'">Lithuanian</xsl:if><xsl:if test="$lang='sk'">litovčina</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodlot"><xsl:if test="$lang='cz'">lotyština</xsl:if><xsl:if test="$lang='en'">Latvian</xsl:if><xsl:if test="$lang='sk'">lotyština</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodluž"><xsl:if test="$lang='cz'">lužická srbština</xsl:if><xsl:if test="$lang='en'">Lusatian Serbian</xsl:if><xsl:if test="$lang='sk'">lužická srbčina</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodmaď"><xsl:if test="$lang='cz'">maďarština</xsl:if><xsl:if test="$lang='en'">Hungarian</xsl:if><xsl:if test="$lang='sk'">maďarčina</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodmaked"><xsl:if test="$lang='cz'">makedonština</xsl:if><xsl:if test="$lang='en'">Macedonian</xsl:if><xsl:if test="$lang='sk'">macedónčina</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodmal"><xsl:if test="$lang='cz'">malajština</xsl:if><xsl:if test="$lang='en'">Malay</xsl:if><xsl:if test="$lang='sk'">malajčina</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodmong"><xsl:if test="$lang='cz'">mongolština</xsl:if><xsl:if test="$lang='en'">Mongolian</xsl:if><xsl:if test="$lang='sk'">mongolčina</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodn"><xsl:if test="$lang='cz'">němčina</xsl:if><xsl:if test="$lang='en'">German</xsl:if><xsl:if test="$lang='sk'">nemčina</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodniz"><xsl:if test="$lang='cz'">nizozemština</xsl:if><xsl:if test="$lang='en'">Dutch</xsl:if><xsl:if test="$lang='sk'">holandčina</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodNJ"><xsl:if test="$lang='cz'">jméno národa</xsl:if><xsl:if test="$lang='en'">the name of the nation</xsl:if><xsl:if test="$lang='sk'">meno národa</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodnor"><xsl:if test="$lang='cz'">norština</xsl:if><xsl:if test="$lang='en'">Norwegian</xsl:if><xsl:if test="$lang='sk'">nórčina</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodOJ"><xsl:if test="$lang='cz'">osobní jméno</xsl:if><xsl:if test="$lang='en'">personal name</xsl:if><xsl:if test="$lang='sk'">osobné meno</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodor"><xsl:if test="$lang='cz'">orientální jazyky</xsl:if><xsl:if test="$lang='en'">oriental languages</xsl:if><xsl:if test="$lang='sk'">orientálne jazyky</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodp"><xsl:if test="$lang='cz'">polština</xsl:if><xsl:if test="$lang='en'">Polish</xsl:if><xsl:if test="$lang='sk'">poľština</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodper"><xsl:if test="$lang='cz'">perština</xsl:if><xsl:if test="$lang='en'">Persian</xsl:if><xsl:if test="$lang='sk'">perzština</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodpolynés"><xsl:if test="$lang='cz'">polynéské jazyky</xsl:if><xsl:if test="$lang='en'">Polynesian languages</xsl:if><xsl:if test="$lang='sk'">polynézskej jazyky</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodport"><xsl:if test="$lang='cz'">portugalština</xsl:if><xsl:if test="$lang='en'">Portuguese</xsl:if><xsl:if test="$lang='sk'">portugalčina</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodprovens"><xsl:if test="$lang='cz'">provensálština</xsl:if><xsl:if test="$lang='en'">Provençal</xsl:if><xsl:if test="$lang='sk'">provensálčina</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodr"><xsl:if test="$lang='cz'">ruština</xsl:if><xsl:if test="$lang='en'">Russian</xsl:if><xsl:if test="$lang='sk'">ruština</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodrom"><xsl:if test="$lang='cz'">románské jazyky</xsl:if><xsl:if test="$lang='en'">Roman languages</xsl:if><xsl:if test="$lang='sk'">románske jazyky</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodrum"><xsl:if test="$lang='cz'">rumunština</xsl:if><xsl:if test="$lang='en'">Romanian</xsl:if><xsl:if test="$lang='sk'">rumunčina</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodř"><xsl:if test="$lang='cz'">řečtina</xsl:if><xsl:if test="$lang='en'">Greek</xsl:if><xsl:if test="$lang='sk'">gréčtina</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodsemit"><xsl:if test="$lang='cz'">semitština</xsl:if><xsl:if test="$lang='en'">Semitic</xsl:if><xsl:if test="$lang='sk'">semitština</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodsch"><xsl:if test="$lang='cz'">srbochorvatština</xsl:if><xsl:if test="$lang='en'">Serbo-Croatian</xsl:if><xsl:if test="$lang='sk'">srbochorvátčina</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodskan"><xsl:if test="$lang='cz'">skandinávské jazyky</xsl:if><xsl:if test="$lang='en'">Scandinavian languages</xsl:if><xsl:if test="$lang='sk'">škandinávske jazyky</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodsl"><xsl:if test="$lang='cz'">slovenština</xsl:if><xsl:if test="$lang='en'">Slovak</xsl:if><xsl:if test="$lang='sk'">slovenský</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodsln"><xsl:if test="$lang='cz'">slovinština</xsl:if><xsl:if test="$lang='en'">Slovenian</xsl:if><xsl:if test="$lang='sk'">slovinčina</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodsrb"><xsl:if test="$lang='cz'">srbština</xsl:if><xsl:if test="$lang='en'">Serbian</xsl:if><xsl:if test="$lang='sk'">srbčina</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodstřl"><xsl:if test="$lang='cz'">středověká latina</xsl:if><xsl:if test="$lang='en'">medieval Latin</xsl:if><xsl:if test="$lang='sk'">stredoveká latinčina</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodsvahil"><xsl:if test="$lang='cz'">svahilština</xsl:if><xsl:if test="$lang='en'">Swahili</xsl:if><xsl:if test="$lang='sk'">swahilčina</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodš"><xsl:if test="$lang='cz'">španělština</xsl:if><xsl:if test="$lang='en'">Spanish</xsl:if><xsl:if test="$lang='sk'">španielčina</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodšv"><xsl:if test="$lang='cz'">švédština</xsl:if><xsl:if test="$lang='en'">Swedish</xsl:if><xsl:if test="$lang='sk'">švédčina</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodt"><xsl:if test="$lang='cz'">turečtina</xsl:if><xsl:if test="$lang='en'">Turkish</xsl:if><xsl:if test="$lang='sk'">turečtina</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodtat"><xsl:if test="$lang='cz'">tatarština</xsl:if><xsl:if test="$lang='en'">Tartar</xsl:if><xsl:if test="$lang='sk'">tatárčina</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodtib"><xsl:if test="$lang='cz'">tibetština</xsl:if><xsl:if test="$lang='en'">Tibetan</xsl:if><xsl:if test="$lang='sk'">tibetčina</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodttat"><xsl:if test="$lang='cz'">turkotatarské jazyky</xsl:if><xsl:if test="$lang='en'">turkotatar languages</xsl:if><xsl:if test="$lang='sk'">turkotatarské jazyky</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodtung"><xsl:if test="$lang='cz'">tunguzština</xsl:if><xsl:if test="$lang='en'">tungu</xsl:if><xsl:if test="$lang='sk'">tunguzština</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodu"><xsl:if test="$lang='cz'">ukrajinština</xsl:if><xsl:if test="$lang='en'">Ukrainian</xsl:if><xsl:if test="$lang='sk'">ukrajinčina</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodVJ"><xsl:if test="$lang='cz'">vlastní jméno</xsl:if><xsl:if test="$lang='en'">your own name</xsl:if><xsl:if test="$lang='sk'">vlastné meno</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodvslov"><xsl:if test="$lang='cz'">východoslovanské jazyky</xsl:if><xsl:if test="$lang='en'">East Slavonic languages</xsl:if><xsl:if test="$lang='sk'">východoslovanské jazyky</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodZJ"><xsl:if test="$lang='cz'">zeměpisné jméno</xsl:if><xsl:if test="$lang='en'">geographical name</xsl:if><xsl:if test="$lang='sk'">zemepisné meno</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodzslov"><xsl:if test="$lang='cz'">západoslovanské jazyky</xsl:if><xsl:if test="$lang='en'">West Slavonic languages</xsl:if><xsl:if test="$lang='sk'">západoslovanské jazyky</xsl:if></xsl:variable>
  <xsl:variable name="loc-puvodžid"><xsl:if test="$lang='cz'">židovský žargon</xsl:if><xsl:if test="$lang='en'">Jewish jargon</xsl:if><xsl:if test="$lang='sk'">židovský žargón</xsl:if></xsl:variable>
  <xsl:variable name="loc-lexadj"><xsl:if test="$lang='cz'">přídavné jméno</xsl:if><xsl:if test="$lang='en'">adjective</xsl:if><xsl:if test="$lang='sk'">prídavné meno</xsl:if></xsl:variable>
  <xsl:variable name="loc-lexadv"><xsl:if test="$lang='cz'">příslovce</xsl:if><xsl:if test="$lang='en'">adverb</xsl:if><xsl:if test="$lang='sk'">príslovka</xsl:if></xsl:variable>
  <xsl:variable name="loc-lexprep"><xsl:if test="$lang='cz'">předložka</xsl:if><xsl:if test="$lang='en'">preposition</xsl:if><xsl:if test="$lang='sk'">predložka</xsl:if></xsl:variable>
  <xsl:variable name="loc-lexust"><xsl:if test="$lang='cz'">ustálený předložkový výraz</xsl:if><xsl:if test="$lang='en'">set prepositional phrase</xsl:if><xsl:if test="$lang='sk'">ustálený predložkové výraz</xsl:if></xsl:variable>
  <xsl:variable name="loc-lexint"><xsl:if test="$lang='cz'">citoslovce</xsl:if><xsl:if test="$lang='en'">interjection</xsl:if><xsl:if test="$lang='sk'">citoslovcia</xsl:if></xsl:variable>
  <xsl:variable name="loc-lexsprezka"><xsl:if test="$lang='cz'">příslovečná spřežka</xsl:if><xsl:if test="$lang='en'">adverbial compound</xsl:if><xsl:if test="$lang='sk'">príslovečná spřežka</xsl:if></xsl:variable>
  <xsl:variable name="loc-lexzkratka"><xsl:if test="$lang='cz'">zkratka</xsl:if><xsl:if test="$lang='en'">abbreviation</xsl:if><xsl:if test="$lang='sk'">skratka</xsl:if></xsl:variable>
  <xsl:variable name="loc-nesklonne"><xsl:if test="$lang='cz'">nesklonné</xsl:if><xsl:if test="$lang='en'">inflexible</xsl:if><xsl:if test="$lang='sk'">nesklonné</xsl:if></xsl:variable>
  <xsl:variable name="loc-gramaticka"><xsl:if test="$lang='cz'">Gramatická </xsl:if><xsl:if test="$lang='en'">gramatical </xsl:if><xsl:if test="$lang='sk'">gramatická </xsl:if></xsl:variable>
  <xsl:variable name="loc-stylisticka"><xsl:if test="$lang='cz'">Stylistická </xsl:if><xsl:if test="$lang='en'">style </xsl:if><xsl:if test="$lang='sk'">štylistická </xsl:if></xsl:variable>
  <xsl:variable name="loc-varianta"><xsl:if test="$lang='cz'">varianta</xsl:if><xsl:if test="$lang='en'">variant</xsl:if><xsl:if test="$lang='sk'">varianta</xsl:if></xsl:variable>
  <xsl:variable name="loc-deleni"><xsl:if test="$lang='cz'">Dělení na slabiky</xsl:if><xsl:if test="$lang='en'">syllables division</xsl:if><xsl:if test="$lang='sk'">Delenie na slabiky</xsl:if></xsl:variable>
  <xsl:variable name="loc-jednotne"><xsl:if test="$lang='cz'">jednotné číslo</xsl:if><xsl:if test="$lang='en'">singular</xsl:if><xsl:if test="$lang='sk'">jednotné číslo</xsl:if></xsl:variable>
  <xsl:variable name="loc-mnozne"><xsl:if test="$lang='cz'">množné číslo</xsl:if><xsl:if test="$lang='en'">plural</xsl:if><xsl:if test="$lang='sk'">množné číslo</xsl:if></xsl:variable>
  <xsl:variable name="loc-1pad"><xsl:if test="$lang='cz'">1. pád</xsl:if><xsl:if test="$lang='en'">nominative</xsl:if><xsl:if test="$lang='sk'">1. pád</xsl:if></xsl:variable>
  <xsl:variable name="loc-2pad"><xsl:if test="$lang='cz'">2. pád</xsl:if><xsl:if test="$lang='en'">accusative</xsl:if><xsl:if test="$lang='sk'">2. pád</xsl:if></xsl:variable>
  <xsl:variable name="loc-3pad"><xsl:if test="$lang='cz'">3. pád</xsl:if><xsl:if test="$lang='en'">dative</xsl:if><xsl:if test="$lang='sk'">3. pád</xsl:if></xsl:variable>
  <xsl:variable name="loc-4pad"><xsl:if test="$lang='cz'">4. pád</xsl:if><xsl:if test="$lang='en'">genitive</xsl:if><xsl:if test="$lang='sk'">4. pád</xsl:if></xsl:variable>
  <xsl:variable name="loc-5pad"><xsl:if test="$lang='cz'">5. pád</xsl:if><xsl:if test="$lang='en'">vocative</xsl:if><xsl:if test="$lang='sk'">5. pád</xsl:if></xsl:variable>
  <xsl:variable name="loc-6pad"><xsl:if test="$lang='cz'">6. pád</xsl:if><xsl:if test="$lang='en'">locative</xsl:if><xsl:if test="$lang='sk'">6. pád</xsl:if></xsl:variable>
  <xsl:variable name="loc-7pad"><xsl:if test="$lang='cz'">7. pád</xsl:if><xsl:if test="$lang='en'">instrumental</xsl:if><xsl:if test="$lang='sk'">7. pád</xsl:if></xsl:variable>
  <xsl:variable name="loc-2stupen"><xsl:if test="$lang='cz'">2. stupeň</xsl:if><xsl:if test="$lang='en'">comparative</xsl:if><xsl:if test="$lang='sk'">2. stupeň</xsl:if></xsl:variable>
  <xsl:variable name="loc-3stupen"><xsl:if test="$lang='cz'">3. stupeň</xsl:if><xsl:if test="$lang='en'">superlative</xsl:if><xsl:if test="$lang='sk'">3. stupeň</xsl:if></xsl:variable>
  <xsl:variable name="loc-1osoba"><xsl:if test="$lang='cz'">1. osoba</xsl:if><xsl:if test="$lang='en'">1. person</xsl:if><xsl:if test="$lang='sk'">1. osoba</xsl:if></xsl:variable>
  <xsl:variable name="loc-2osoba"><xsl:if test="$lang='cz'">2. osoba</xsl:if><xsl:if test="$lang='en'">2. person</xsl:if><xsl:if test="$lang='sk'">2. osoba</xsl:if></xsl:variable>
  <xsl:variable name="loc-3osoba"><xsl:if test="$lang='cz'">3. osoba</xsl:if><xsl:if test="$lang='en'">3. person</xsl:if><xsl:if test="$lang='sk'">3. osoba</xsl:if></xsl:variable>
  <xsl:variable name="loc-rozkazovaci"><xsl:if test="$lang='cz'">rozkazovací způsob</xsl:if><xsl:if test="$lang='en'">imperative</xsl:if><xsl:if test="$lang='sk'">rozkazovací spôsob</xsl:if></xsl:variable>
  <xsl:variable name="loc-pricinne"><xsl:if test="$lang='cz'">příčestí činné</xsl:if><xsl:if test="$lang='en'">participle active</xsl:if><xsl:if test="$lang='sk'">príčastie činné</xsl:if></xsl:variable>
  <xsl:variable name="loc-pritrpne"><xsl:if test="$lang='cz'">příčestí trpné</xsl:if><xsl:if test="$lang='en'">participle pasive</xsl:if><xsl:if test="$lang='sk'">príčastie trpné</xsl:if></xsl:variable>
  <xsl:variable name="loc-prechm"><xsl:if test="$lang='cz'">přechodník, m.</xsl:if><xsl:if test="$lang='en'">transgressive masculine</xsl:if><xsl:if test="$lang='sk'">prechodník, m.</xsl:if></xsl:variable>
  <xsl:variable name="loc-prechzs"><xsl:if test="$lang='cz'">přechodník, ž. + s.</xsl:if><xsl:if test="$lang='en'">transgressive feminine + neutral</xsl:if><xsl:if test="$lang='sk'">prechodník, ž. + S.</xsl:if></xsl:variable>
  <xsl:variable name="loc-versub"><xsl:if test="$lang='cz'">verbální substantivum</xsl:if><xsl:if test="$lang='en'">verbal substantive</xsl:if><xsl:if test="$lang='sk'">verbálne substantívum</xsl:if></xsl:variable>
  <xsl:variable name="loc-stylhod"><xsl:if test="$lang='cz'">Stylové hodnocení</xsl:if><xsl:if test="$lang='en'">Style</xsl:if><xsl:if test="$lang='sk'">štýlové hodnotenie</xsl:if></xsl:variable>
  <xsl:variable name="loc-stylpri"><xsl:if test="$lang='cz'">Stylový příznak</xsl:if><xsl:if test="$lang='en'">Style flag</xsl:if><xsl:if test="$lang='sk'">štýlový príznak</xsl:if></xsl:variable>
  <xsl:variable name="loc-vyzobla"><xsl:if test="$lang='cz'">Významová oblast</xsl:if><xsl:if test="$lang='en'">Semantic field</xsl:if><xsl:if test="$lang='sk'">významová oblasť</xsl:if></xsl:variable>
  <xsl:variable name="loc-slovnispojeni"><xsl:if test="$lang='cz'">Slovní spojení</xsl:if><xsl:if test="$lang='en'">Collocation</xsl:if><xsl:if test="$lang='sk'">slovné spojenie</xsl:if></xsl:variable>
  <xsl:variable name="loc-synonyma"><xsl:if test="$lang='cz'">Synonyma</xsl:if><xsl:if test="$lang='en'">Synonyms</xsl:if><xsl:if test="$lang='sk'">synonymá</xsl:if></xsl:variable>
  <xsl:variable name="loc-antonyma"><xsl:if test="$lang='cz'">Antonyma</xsl:if><xsl:if test="$lang='en'">Antonyms</xsl:if><xsl:if test="$lang='sk'">antonymá</xsl:if></xsl:variable>
  <xsl:variable name="loc-csskupinapomnozne"><xsl:if test="$lang='cz'">pomnožné</xsl:if><xsl:if test="$lang='en'">plurative</xsl:if><xsl:if test="$lang='sk'">pomnožné</xsl:if></xsl:variable>
  <xsl:variable name="loc-csskupinahromadne"><xsl:if test="$lang='cz'">hromadné</xsl:if><xsl:if test="$lang='en'">mass</xsl:if><xsl:if test="$lang='sk'">hromadné</xsl:if></xsl:variable>
  <xsl:variable name="loc-csskupinavlastni"><xsl:if test="$lang='cz'">vlastní</xsl:if><xsl:if test="$lang='en'">proper</xsl:if><xsl:if test="$lang='sk'">vlastný</xsl:if></xsl:variable>
  <xsl:variable name="loc-csskupinaosobni"><xsl:if test="$lang='cz'">osobní</xsl:if><xsl:if test="$lang='en'">personal</xsl:if><xsl:if test="$lang='sk'">osobné</xsl:if></xsl:variable>
  <xsl:variable name="loc-csskupinaukazovaci"><xsl:if test="$lang='cz'">ukazovací</xsl:if><xsl:if test="$lang='en'">demonstative</xsl:if><xsl:if test="$lang='sk'">polohovacie</xsl:if></xsl:variable>
  <xsl:variable name="loc-csskupinavztazne"><xsl:if test="$lang='cz'">vztažné</xsl:if><xsl:if test="$lang='en'">relative</xsl:if><xsl:if test="$lang='sk'">vzťažné</xsl:if></xsl:variable>
  <xsl:variable name="loc-csskupinazvratne"><xsl:if test="$lang='cz'">zvratné</xsl:if><xsl:if test="$lang='en'">reflexive</xsl:if><xsl:if test="$lang='sk'">zvratné</xsl:if></xsl:variable>
  <xsl:variable name="loc-csskupinazaporne"><xsl:if test="$lang='cz'">záporné</xsl:if><xsl:if test="$lang='en'">negative</xsl:if><xsl:if test="$lang='sk'">záporné</xsl:if></xsl:variable>
  <xsl:variable name="loc-csskupinaneurcite"><xsl:if test="$lang='cz'">neurčité</xsl:if><xsl:if test="$lang='en'">indefinite</xsl:if><xsl:if test="$lang='sk'">neurčité</xsl:if></xsl:variable>
  <xsl:variable name="loc-csskupinaprivl"><xsl:if test="$lang='cz'">přivlastňovací</xsl:if><xsl:if test="$lang='en'">possessive</xsl:if><xsl:if test="$lang='sk'">privlastňovacie</xsl:if></xsl:variable>
  <xsl:variable name="loc-csskupinazakladni"><xsl:if test="$lang='cz'">základní</xsl:if><xsl:if test="$lang='en'">cardinal</xsl:if><xsl:if test="$lang='sk'">základné</xsl:if></xsl:variable>
  <xsl:variable name="loc-csskupinadruhova"><xsl:if test="$lang='cz'">druhová</xsl:if><xsl:if test="$lang='en'">ordinal</xsl:if><xsl:if test="$lang='sk'">druhová</xsl:if></xsl:variable>
  <xsl:variable name="loc-csskupinaradova"><xsl:if test="$lang='cz'">řadová</xsl:if><xsl:if test="$lang='en'">multiplicative</xsl:if><xsl:if test="$lang='sk'">radová</xsl:if></xsl:variable>
  <xsl:variable name="loc-csskupinanasobna"><xsl:if test="$lang='cz'">násobná</xsl:if><xsl:if test="$lang='en'">iconic</xsl:if><xsl:if test="$lang='sk'">násobná</xsl:if></xsl:variable>
  <xsl:variable name="loc-csskupinatazaci"><xsl:if test="$lang='cz'">tázací</xsl:if><xsl:if test="$lang='en'">interrogative</xsl:if><xsl:if test="$lang='sk'">opytovacie</xsl:if></xsl:variable>
  <xsl:variable name="loc-csskupinadok"><xsl:if test="$lang='cz'">dokonavý vid</xsl:if><xsl:if test="$lang='en'">perfective</xsl:if><xsl:if test="$lang='sk'">dokonavý vid</xsl:if></xsl:variable>
  <xsl:variable name="loc-csskupinanedok"><xsl:if test="$lang='cz'">nedokonavý vid</xsl:if><xsl:if test="$lang='en'">imperfective</xsl:if><xsl:if test="$lang='sk'">nedokonavý vid</xsl:if></xsl:variable>
  <xsl:variable name="loc-csskupinadoknedok"><xsl:if test="$lang='cz'">dokonavý i nedokonavý vid</xsl:if><xsl:if test="$lang='en'">perfective and imperfective</xsl:if><xsl:if test="$lang='sk'">dokonavý aj nedokonavý vid</xsl:if></xsl:variable>
  <xsl:variable name="loc-csskupinazajmenne"><xsl:if test="$lang='cz'">zájmenné</xsl:if><xsl:if test="$lang='en'">pronominal</xsl:if><xsl:if test="$lang='sk'">zámenné</xsl:if></xsl:variable>
  <xsl:variable name="loc-csskupinasourad"><xsl:if test="$lang='cz'">souřadicí</xsl:if><xsl:if test="$lang='en'">coordinating</xsl:if><xsl:if test="$lang='sk'">priraďovacie</xsl:if></xsl:variable>
  <xsl:variable name="loc-csskupinapodrad"><xsl:if test="$lang='cz'">podřadicí</xsl:if><xsl:if test="$lang='en'">subordinating</xsl:if><xsl:if test="$lang='sk'">podraďovacie</xsl:if></xsl:variable>
  <xsl:variable name="loc-csskupinanasobnapris"><xsl:if test="$lang='cz'">násobná příslovečná</xsl:if><xsl:if test="$lang='en'">multiplicative adverbial</xsl:if><xsl:if test="$lang='sk'">násobná príslovečná</xsl:if></xsl:variable>
  <xsl:variable name="loc-csskupinanasobnaprisneu"><xsl:if test="$lang='cz'">neurčitá násobná příslovečná</xsl:if><xsl:if test="$lang='en'">indefinitive multiplicative</xsl:if><xsl:if test="$lang='sk'">neurčitá násobná príslovečná</xsl:if></xsl:variable>
  <xsl:variable name="loc-csskupinazajmenna"><xsl:if test="$lang='cz'">zájmenná</xsl:if><xsl:if test="$lang='en'">pronominal</xsl:if><xsl:if test="$lang='sk'">zámenné</xsl:if></xsl:variable>
  <xsl:variable name="loc-csskupinafrazem"><xsl:if test="$lang='cz'">frazém</xsl:if><xsl:if test="$lang='en'">phrasal idiom</xsl:if><xsl:if test="$lang='sk'">frázy</xsl:if></xsl:variable>
  <xsl:variable name="loc-csskupinaporekadlo"><xsl:if test="$lang='cz'">pořekadlo</xsl:if><xsl:if test="$lang='en'">adage</xsl:if><xsl:if test="$lang='sk'">porekadlo</xsl:if></xsl:variable>
  <xsl:variable name="loc-csskupinaprirovnani"><xsl:if test="$lang='cz'">přirovnání</xsl:if><xsl:if test="$lang='en'">simile</xsl:if><xsl:if test="$lang='sk'">prirovnanie</xsl:if></xsl:variable>
  <xsl:variable name="loc-csskupinaprislovi"><xsl:if test="$lang='cz'">přísloví</xsl:if><xsl:if test="$lang='en'">proverb</xsl:if><xsl:if test="$lang='sk'">príslovia</xsl:if></xsl:variable>
  <xsl:variable name="loc-csskupina2m"><xsl:if test="$lang='cz'">mužský rod</xsl:if><xsl:if test="$lang='en'">masculine</xsl:if><xsl:if test="$lang='sk'">mužský rod</xsl:if></xsl:variable>
  <xsl:variable name="loc-csskupina2z"><xsl:if test="$lang='cz'">ženský rod</xsl:if><xsl:if test="$lang='en'">feminine</xsl:if><xsl:if test="$lang='sk'">ženský rod</xsl:if></xsl:variable>
  <xsl:variable name="loc-csskupina2s"><xsl:if test="$lang='cz'">střední rod</xsl:if><xsl:if test="$lang='en'">neuter</xsl:if><xsl:if test="$lang='sk'">stredný rod</xsl:if></xsl:variable>
  <xsl:variable name="loc-csskupina2zpusobove"><xsl:if test="$lang='cz'">způsobové</xsl:if><xsl:if test="$lang='en'">modal</xsl:if><xsl:if test="$lang='sk'">spôsobové</xsl:if></xsl:variable>
  <xsl:variable name="loc-csskupina2ukazovaci"><xsl:if test="$lang='cz'">ukazovací</xsl:if><xsl:if test="$lang='en'">demonstrative</xsl:if><xsl:if test="$lang='sk'">polohovacie</xsl:if></xsl:variable>
  <xsl:variable name="loc-csskupina2tazaci"><xsl:if test="$lang='cz'">tázací</xsl:if><xsl:if test="$lang='en'">interrogative</xsl:if><xsl:if test="$lang='sk'">opytovacie</xsl:if></xsl:variable>
  <xsl:variable name="loc-csskupina2vztazne"><xsl:if test="$lang='cz'">vztažné</xsl:if><xsl:if test="$lang='en'">relative</xsl:if><xsl:if test="$lang='sk'">vzťažné</xsl:if></xsl:variable>
  <xsl:variable name="loc-csskupina2neurcite"><xsl:if test="$lang='cz'">neurčité</xsl:if><xsl:if test="$lang='en'">indefinite</xsl:if><xsl:if test="$lang='sk'">neurčité</xsl:if></xsl:variable>
  <xsl:variable name="loc-csskupina2zaporne"><xsl:if test="$lang='cz'">záporné</xsl:if><xsl:if test="$lang='en'">negative</xsl:if><xsl:if test="$lang='sk'">záporné</xsl:if></xsl:variable>
  <xsl:variable name="loc-csskupina2prisudku"><xsl:if test="$lang='cz'">v přísudku</xsl:if><xsl:if test="$lang='en'">in predicate</xsl:if><xsl:if test="$lang='sk'">v prísudku</xsl:if></xsl:variable>
  <xsl:variable name="loc-csskupina2vymez"><xsl:if test="$lang='cz'">vymezovací</xsl:if><xsl:if test="$lang='en'">determinative</xsl:if><xsl:if test="$lang='sk'">vymedzovacie</xsl:if></xsl:variable>
  <xsl:variable name="loc-csskupina2zduraz"><xsl:if test="$lang='cz'">zdůrazňovací</xsl:if><xsl:if test="$lang='en'">determinative</xsl:if><xsl:if test="$lang='sk'">zdůrazňovací</xsl:if></xsl:variable>
  <xsl:variable name="loc-csskupina2obsahova"><xsl:if test="$lang='cz'">obsahová</xsl:if><xsl:if test="$lang='en'"></xsl:if><xsl:if test="$lang='sk'">obsahová</xsl:if></xsl:variable>
  <xsl:variable name="loc-csskupina2modalni"><xsl:if test="$lang='cz'">modální</xsl:if><xsl:if test="$lang='en'"></xsl:if><xsl:if test="$lang='sk'">modálne</xsl:if></xsl:variable>
  <xsl:variable name="loc-csskupina2citoslovecna"><xsl:if test="$lang='cz'">citoslovečná</xsl:if><xsl:if test="$lang='en'"></xsl:if><xsl:if test="$lang='sk'">citoslovečná</xsl:if></xsl:variable>
  <xsl:variable name="loc-cshodnoceniargot"><xsl:if test="$lang='cz'">argot</xsl:if><xsl:if test="$lang='en'">argot</xsl:if><xsl:if test="$lang='sk'">argot</xsl:if></xsl:variable>
  <xsl:variable name="loc-cshodnocenihovorove"><xsl:if test="$lang='cz'">hovorově</xsl:if><xsl:if test="$lang='en'">colloquially</xsl:if><xsl:if test="$lang='sk'">hovorovo</xsl:if></xsl:variable>
  <xsl:variable name="loc-cshodnocenimorav"><xsl:if test="$lang='cz'">oblastní moravský výraz</xsl:if><xsl:if test="$lang='en'">regional Moravian expression</xsl:if><xsl:if test="$lang='sk'">oblastné moravský výraz</xsl:if></xsl:variable>
  <xsl:variable name="loc-cshodnoceninarec"><xsl:if test="$lang='cz'">nářeční výraz</xsl:if><xsl:if test="$lang='en'">dialect expression</xsl:if><xsl:if test="$lang='sk'">nárečové výraz</xsl:if></xsl:variable>
  <xsl:variable name="loc-cshodnocenineologismus"><xsl:if test="$lang='cz'">neologismus</xsl:if><xsl:if test="$lang='en'">neologism</xsl:if><xsl:if test="$lang='sk'">neologizmus</xsl:if></xsl:variable>
  <xsl:variable name="loc-cshodnoceniobecne"><xsl:if test="$lang='cz'">obecně česky</xsl:if><xsl:if test="$lang='en'">generally Czech</xsl:if><xsl:if test="$lang='sk'">všeobecne slovensky</xsl:if></xsl:variable>
  <xsl:variable name="loc-cshodnocenioblast"><xsl:if test="$lang='cz'">oblastní výraz</xsl:if><xsl:if test="$lang='en'">regional expression</xsl:if><xsl:if test="$lang='sk'">oblastné výraz</xsl:if></xsl:variable>
  <xsl:variable name="loc-cshodnocenislang"><xsl:if test="$lang='cz'">slangový výraz</xsl:if><xsl:if test="$lang='en'">slang expression</xsl:if><xsl:if test="$lang='sk'">slangový výraz</xsl:if></xsl:variable>
  <xsl:variable name="loc-cshodnocenispisovne"><xsl:if test="$lang='cz'">spisovně</xsl:if><xsl:if test="$lang='en'"></xsl:if><xsl:if test="$lang='sk'">spisovne</xsl:if></xsl:variable>
  <xsl:variable name="loc-cspriznakabstrakt"><xsl:if test="$lang='cz'">abstraktní výraz</xsl:if><xsl:if test="$lang='en'">abstract expression</xsl:if><xsl:if test="$lang='sk'">abstraktné výraz</xsl:if></xsl:variable>
  <xsl:variable name="loc-cspriznakbasnicky"><xsl:if test="$lang='cz'">básnický výraz</xsl:if><xsl:if test="$lang='en'">poetic expression</xsl:if><xsl:if test="$lang='sk'">básnický výraz</xsl:if></xsl:variable>
  <xsl:variable name="loc-cspriznakbiblicky"><xsl:if test="$lang='cz'">biblický výraz</xsl:if><xsl:if test="$lang='en'">biblical expression</xsl:if><xsl:if test="$lang='sk'">biblický výraz</xsl:if></xsl:variable>
  <xsl:variable name="loc-cspriznakdetsky"><xsl:if test="$lang='cz'">dětský výraz</xsl:if><xsl:if test="$lang='en'">children expression</xsl:if><xsl:if test="$lang='sk'">detský výraz</xsl:if></xsl:variable>
  <xsl:variable name="loc-cspriznakdomacke"><xsl:if test="$lang='cz'">domácké slovo</xsl:if><xsl:if test="$lang='en'">homicide</xsl:if><xsl:if test="$lang='sk'">domácke slovo</xsl:if></xsl:variable>
  <xsl:variable name="loc-cspriznakduverny"><xsl:if test="$lang='cz'">důvěrný výraz</xsl:if><xsl:if test="$lang='en'">confidential expression</xsl:if><xsl:if test="$lang='sk'">dôverný výraz</xsl:if></xsl:variable>
  <xsl:variable name="loc-cspriznakeufem"><xsl:if test="$lang='cz'">eufemismus</xsl:if><xsl:if test="$lang='en'">euphemism</xsl:if><xsl:if test="$lang='sk'">eufemizmus</xsl:if></xsl:variable>
  <xsl:variable name="loc-cspriznakexpres"><xsl:if test="$lang='cz'">expresivní výraz</xsl:if><xsl:if test="$lang='en'">expressive expression</xsl:if><xsl:if test="$lang='sk'">expresívne výraz</xsl:if></xsl:variable>
  <xsl:variable name="loc-cspriznakfamil"><xsl:if test="$lang='cz'">familiární výraz</xsl:if><xsl:if test="$lang='en'">familiar expression</xsl:if><xsl:if test="$lang='sk'">familiárnej výraz</xsl:if></xsl:variable>
  <xsl:variable name="loc-cspriznakhanlivy"><xsl:if test="$lang='cz'">hanlivý výraz</xsl:if><xsl:if test="$lang='en'">defamatory expression</xsl:if><xsl:if test="$lang='sk'">hanlivý výraz</xsl:if></xsl:variable>
  <xsl:variable name="loc-cspriznakironicky"><xsl:if test="$lang='cz'">ironicky</xsl:if><xsl:if test="$lang='en'">ironic</xsl:if><xsl:if test="$lang='sk'">ironicky</xsl:if></xsl:variable>
  <xsl:variable name="loc-cspriznakkladny"><xsl:if test="$lang='cz'">kladný výraz</xsl:if><xsl:if test="$lang='en'">positive expression</xsl:if><xsl:if test="$lang='sk'">kladný výraz</xsl:if></xsl:variable>
  <xsl:variable name="loc-cspriznakknizni"><xsl:if test="$lang='cz'">knižní výraz</xsl:if><xsl:if test="$lang='en'">book expression</xsl:if><xsl:if test="$lang='sk'">knižné výraz</xsl:if></xsl:variable>
  <xsl:variable name="loc-cspriznakkonkretni"><xsl:if test="$lang='cz'">konkrétní výraz</xsl:if><xsl:if test="$lang='en'">a specific term</xsl:if><xsl:if test="$lang='sk'">konkrétny výraz</xsl:if></xsl:variable>
  <xsl:variable name="loc-cspriznaklidovy"><xsl:if test="$lang='cz'">lidový výraz</xsl:if><xsl:if test="$lang='en'">folk expression</xsl:if><xsl:if test="$lang='sk'">ľudový výraz</xsl:if></xsl:variable>
  <xsl:variable name="loc-cspriznaklichotivy"><xsl:if test="$lang='cz'">lichotivý výraz</xsl:if><xsl:if test="$lang='en'">flattering expression</xsl:if><xsl:if test="$lang='sk'">lichotivý výraz</xsl:if></xsl:variable>
  <xsl:variable name="loc-cspriznakmazlivy"><xsl:if test="$lang='cz'">mazlivý výraz</xsl:if><xsl:if test="$lang='en'">petty expression</xsl:if><xsl:if test="$lang='sk'">maznavý výraz</xsl:if></xsl:variable>
  <xsl:variable name="loc-cspriznakodborny"><xsl:if test="$lang='cz'">odborný výraz</xsl:if><xsl:if test="$lang='en'">technical term</xsl:if><xsl:if test="$lang='sk'">odborný výraz</xsl:if></xsl:variable>
  <xsl:variable name="loc-cspriznakpohadkovy"><xsl:if test="$lang='cz'">pohádkový výraz</xsl:if><xsl:if test="$lang='en'">fairy tale</xsl:if><xsl:if test="$lang='sk'">rozprávkový výraz</xsl:if></xsl:variable>
  <xsl:variable name="loc-cspriznakpubl"><xsl:if test="$lang='cz'">publicistický výraz</xsl:if><xsl:if test="$lang='en'">a publicistic expression</xsl:if><xsl:if test="$lang='sk'">publicistický výraz</xsl:if></xsl:variable>
  <xsl:variable name="loc-cspriznakprenes"><xsl:if test="$lang='cz'">přeneseně</xsl:if><xsl:if test="$lang='en'">transferred</xsl:if><xsl:if test="$lang='sk'">prenesene</xsl:if></xsl:variable>
  <xsl:variable name="loc-cspriznakzdvor"><xsl:if test="$lang='cz'">zdvořilostní výraz</xsl:if><xsl:if test="$lang='en'">courtesy expression</xsl:if><xsl:if test="$lang='sk'">zdvorilostné výraz</xsl:if></xsl:variable>
  <xsl:variable name="loc-cspriznakzhrubely"><xsl:if test="$lang='cz'">zhrubělý výraz</xsl:if><xsl:if test="$lang='en'">a rough expression</xsl:if><xsl:if test="$lang='sk'">zhrubnutý výraz</xsl:if></xsl:variable>
  <xsl:variable name="loc-cspriznakzert"><xsl:if test="$lang='cz'">žertovný výraz</xsl:if><xsl:if test="$lang='en'">joking expression</xsl:if><xsl:if test="$lang='sk'">žartovný výraz</xsl:if></xsl:variable>
  <xsl:variable name="loc-cspriznakzlomkovy"><xsl:if test="$lang='cz'">zlomkový výraz</xsl:if><xsl:if test="$lang='en'">fractional expression</xsl:if><xsl:if test="$lang='sk'">zlomkový výraz</xsl:if></xsl:variable>
  <xsl:variable name="loc-cspriznakzdrob"><xsl:if test="$lang='cz'">zdrobnělina</xsl:if><xsl:if test="$lang='en'">diminutive</xsl:if><xsl:if test="$lang='sk'">zdrobnenina</xsl:if></xsl:variable>
  <xsl:variable name="loc-cspriznakzastar"><xsl:if test="$lang='cz'">zastaralý výraz</xsl:if><xsl:if test="$lang='en'">obsolete expression</xsl:if><xsl:if test="$lang='sk'">zastaraný výraz</xsl:if></xsl:variable>
  <xsl:variable name="loc-cspriznakcirkev"><xsl:if test="$lang='cz'">církevní výraz</xsl:if><xsl:if test="$lang='en'">church expression</xsl:if><xsl:if test="$lang='sk'">cirkevné výraz</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastanat"><xsl:if test="$lang='cz'">anatomie</xsl:if><xsl:if test="$lang='en'">anatomy</xsl:if><xsl:if test="$lang='sk'">anatómia</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastantr"><xsl:if test="$lang='cz'">antropologie</xsl:if><xsl:if test="$lang='en'">anthropology</xsl:if><xsl:if test="$lang='sk'">antropológia</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastarcheol"><xsl:if test="$lang='cz'">archeologie</xsl:if><xsl:if test="$lang='en'">archeology</xsl:if><xsl:if test="$lang='sk'">archeológia</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastarchit"><xsl:if test="$lang='cz'">architektura</xsl:if><xsl:if test="$lang='en'">architecture</xsl:if><xsl:if test="$lang='sk'">architektúra</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastbiol"><xsl:if test="$lang='cz'">biologie</xsl:if><xsl:if test="$lang='en'">biology</xsl:if><xsl:if test="$lang='sk'">biológie</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastbot"><xsl:if test="$lang='cz'">botanika</xsl:if><xsl:if test="$lang='en'">botany</xsl:if><xsl:if test="$lang='sk'">botanika</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastdipl"><xsl:if test="$lang='cz'">diplomacie</xsl:if><xsl:if test="$lang='en'">diplomacy</xsl:if><xsl:if test="$lang='sk'">diplomacia</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastdiv"><xsl:if test="$lang='cz'">divadelnictví</xsl:if><xsl:if test="$lang='en'">theater</xsl:if><xsl:if test="$lang='sk'">divadelníctva</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastdopr"><xsl:if test="$lang='cz'">doprava</xsl:if><xsl:if test="$lang='en'">transport</xsl:if><xsl:if test="$lang='sk'">doprava</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastekol"><xsl:if test="$lang='cz'">ekologie</xsl:if><xsl:if test="$lang='en'">ecology</xsl:if><xsl:if test="$lang='sk'">ekológia</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastekon"><xsl:if test="$lang='cz'">ekonomie</xsl:if><xsl:if test="$lang='en'">economy</xsl:if><xsl:if test="$lang='sk'">ekonómie</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastetn"><xsl:if test="$lang='cz'">etnografie</xsl:if><xsl:if test="$lang='en'">ethnography</xsl:if><xsl:if test="$lang='sk'">etnografia</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastfeud"><xsl:if test="$lang='cz'">feudalismus</xsl:if><xsl:if test="$lang='en'">feudalism</xsl:if><xsl:if test="$lang='sk'">feudalizmus</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastfilat"><xsl:if test="$lang='cz'">filatelie</xsl:if><xsl:if test="$lang='en'">philately</xsl:if><xsl:if test="$lang='sk'">filatelia</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastfilm"><xsl:if test="$lang='cz'">filmařství</xsl:if><xsl:if test="$lang='en'">filmmaking</xsl:if><xsl:if test="$lang='sk'">filmárstva</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastfiloz"><xsl:if test="$lang='cz'">filozofie</xsl:if><xsl:if test="$lang='en'">philosophy</xsl:if><xsl:if test="$lang='sk'">filozofia</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastfot"><xsl:if test="$lang='cz'">fotografování</xsl:if><xsl:if test="$lang='en'">photography</xsl:if><xsl:if test="$lang='sk'">fotografovanie</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastfyz"><xsl:if test="$lang='cz'">fyzika</xsl:if><xsl:if test="$lang='en'">physics</xsl:if><xsl:if test="$lang='sk'">fyzika</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastfyziol"><xsl:if test="$lang='cz'">fyziologie</xsl:if><xsl:if test="$lang='en'">physiology</xsl:if><xsl:if test="$lang='sk'">fyziológia</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastgeol"><xsl:if test="$lang='cz'">geologie</xsl:if><xsl:if test="$lang='en'">geology</xsl:if><xsl:if test="$lang='sk'">geológie</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastgeom"><xsl:if test="$lang='cz'">geometrie</xsl:if><xsl:if test="$lang='en'">geometry</xsl:if><xsl:if test="$lang='sk'">geometria</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastgnoz"><xsl:if test="$lang='cz'">gnozeologie</xsl:if><xsl:if test="$lang='en'">gnosis</xsl:if><xsl:if test="$lang='sk'">teória poznania</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblasthist"><xsl:if test="$lang='cz'">historie</xsl:if><xsl:if test="$lang='en'">history</xsl:if><xsl:if test="$lang='sk'">histórie</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblasthorn"><xsl:if test="$lang='cz'">hornictví</xsl:if><xsl:if test="$lang='en'">mining</xsl:if><xsl:if test="$lang='sk'">baníctvo</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblasthorol"><xsl:if test="$lang='cz'">horolezectví</xsl:if><xsl:if test="$lang='en'">mountaineering</xsl:if><xsl:if test="$lang='sk'">horolezectvo</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblasthosp"><xsl:if test="$lang='cz'">hospodářství</xsl:if><xsl:if test="$lang='en'">economy</xsl:if><xsl:if test="$lang='sk'">hospodárstvo</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblasthud"><xsl:if test="$lang='cz'">hudební věda</xsl:if><xsl:if test="$lang='en'">musical science</xsl:if><xsl:if test="$lang='sk'">hudobné veda</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblasthut"><xsl:if test="$lang='cz'">hutnictví</xsl:if><xsl:if test="$lang='en'">metallurgy</xsl:if><xsl:if test="$lang='sk'">hutníctvo</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblasthvězd"><xsl:if test="$lang='cz'">astronomie</xsl:if><xsl:if test="$lang='en'">astronomy</xsl:if><xsl:if test="$lang='sk'">astronómie</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastchem"><xsl:if test="$lang='cz'">chemie</xsl:if><xsl:if test="$lang='en'">chemistry</xsl:if><xsl:if test="$lang='sk'">chémia</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastideal"><xsl:if test="$lang='cz'">idealismus</xsl:if><xsl:if test="$lang='en'">idealism</xsl:if><xsl:if test="$lang='sk'">idealizmus</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastinformatika"><xsl:if test="$lang='cz'">informatika</xsl:if><xsl:if test="$lang='en'">informatics</xsl:if><xsl:if test="$lang='sk'">informatika</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastjad"><xsl:if test="$lang='cz'">jaderná fyzika</xsl:if><xsl:if test="$lang='en'">nuclear physics</xsl:if><xsl:if test="$lang='sk'">jadrová fyzika</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastjaz"><xsl:if test="$lang='cz'">jazykověda</xsl:if><xsl:if test="$lang='en'">linguistics</xsl:if><xsl:if test="$lang='sk'">jazykoveda</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastkapit"><xsl:if test="$lang='cz'">kapitalismus</xsl:if><xsl:if test="$lang='en'">capitalism</xsl:if><xsl:if test="$lang='sk'">kapitalizmus</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastkaret"><xsl:if test="$lang='cz'">karetní hra</xsl:if><xsl:if test="$lang='en'">card game</xsl:if><xsl:if test="$lang='sk'">kartová hra</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastkatolcírkvi"><xsl:if test="$lang='cz'">katolictví</xsl:if><xsl:if test="$lang='en'">Catholicism</xsl:if><xsl:if test="$lang='sk'">katolíctvo</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastkrim"><xsl:if test="$lang='cz'">kriminalistika</xsl:if><xsl:if test="$lang='en'">criminology</xsl:if><xsl:if test="$lang='sk'">kriminalistika</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastkřesť"><xsl:if test="$lang='cz'">křesťanství</xsl:if><xsl:if test="$lang='en'">Christianity</xsl:if><xsl:if test="$lang='sk'">kresťanstvo</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastkuch"><xsl:if test="$lang='cz'">kuchařství</xsl:if><xsl:if test="$lang='en'">cookery</xsl:if><xsl:if test="$lang='sk'">kuchárstvo</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastkult"><xsl:if test="$lang='cz'">kultura</xsl:if><xsl:if test="$lang='en'">culture</xsl:if><xsl:if test="$lang='sk'">kultúra</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastkyb"><xsl:if test="$lang='cz'">kybernetika</xsl:if><xsl:if test="$lang='en'">cybernetics</xsl:if><xsl:if test="$lang='sk'">kybernetika</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastlék"><xsl:if test="$lang='cz'">lékařství</xsl:if><xsl:if test="$lang='en'">medicine</xsl:if><xsl:if test="$lang='sk'">lekárstvo</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastlékár"><xsl:if test="$lang='cz'">lékárnictví</xsl:if><xsl:if test="$lang='en'">pharmaceutics</xsl:if><xsl:if test="$lang='sk'">lekárnictvo</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastlet"><xsl:if test="$lang='cz'">letectví</xsl:if><xsl:if test="$lang='en'">aviation</xsl:if><xsl:if test="$lang='sk'">letectvo</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastlog"><xsl:if test="$lang='cz'">logika</xsl:if><xsl:if test="$lang='en'">logic</xsl:if><xsl:if test="$lang='sk'">logika</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastmarx"><xsl:if test="$lang='cz'">marxismus</xsl:if><xsl:if test="$lang='en'">Marxism</xsl:if><xsl:if test="$lang='sk'">marxizmus</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastmat"><xsl:if test="$lang='cz'">matematika</xsl:if><xsl:if test="$lang='en'">mathematics</xsl:if><xsl:if test="$lang='sk'">matematika</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastmeteor"><xsl:if test="$lang='cz'">meteorologie</xsl:if><xsl:if test="$lang='en'">meteorology</xsl:if><xsl:if test="$lang='sk'">meteorológia</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastminer"><xsl:if test="$lang='cz'">mineralogie</xsl:if><xsl:if test="$lang='en'">mineralogy</xsl:if><xsl:if test="$lang='sk'">mineralógia</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastmotor"><xsl:if test="$lang='cz'">motorismus</xsl:if><xsl:if test="$lang='en'">motorism</xsl:if><xsl:if test="$lang='sk'">motorizmus</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastmysl"><xsl:if test="$lang='cz'">myslivecký výraz</xsl:if><xsl:if test="$lang='en'">hunting expression</xsl:if><xsl:if test="$lang='sk'">poľovnícky výraz</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastmytol"><xsl:if test="$lang='cz'">mytologie</xsl:if><xsl:if test="$lang='en'">mythology</xsl:if><xsl:if test="$lang='sk'">mytológie</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastnáb"><xsl:if test="$lang='cz'">náboženství</xsl:if><xsl:if test="$lang='en'">religion</xsl:if><xsl:if test="$lang='sk'">náboženstvo</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastnár"><xsl:if test="$lang='cz'">národopis</xsl:if><xsl:if test="$lang='en'">ethnography</xsl:if><xsl:if test="$lang='sk'">národopis</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastobch"><xsl:if test="$lang='cz'">obchod</xsl:if><xsl:if test="$lang='en'">trade</xsl:if><xsl:if test="$lang='sk'">obchod</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastpedag"><xsl:if test="$lang='cz'">pedagogika</xsl:if><xsl:if test="$lang='en'">pedagogy</xsl:if><xsl:if test="$lang='sk'">pedagogika</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastpeněž"><xsl:if test="$lang='cz'">peněžnictví</xsl:if><xsl:if test="$lang='en'">financial intermediation</xsl:if><xsl:if test="$lang='sk'">peňažníctvo</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastpodnikani"><xsl:if test="$lang='cz'">podnikání</xsl:if><xsl:if test="$lang='en'">business</xsl:if><xsl:if test="$lang='sk'">podnikania</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastpolit"><xsl:if test="$lang='cz'">politika</xsl:if><xsl:if test="$lang='en'">policy</xsl:if><xsl:if test="$lang='sk'">politika</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastpolygr"><xsl:if test="$lang='cz'">polygrafie</xsl:if><xsl:if test="$lang='en'">polygraphy</xsl:if><xsl:if test="$lang='sk'">polygrafia</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastpošt"><xsl:if test="$lang='cz'">poštovnictví</xsl:if><xsl:if test="$lang='en'">postal services</xsl:if><xsl:if test="$lang='sk'">poštovnictvo</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastpotrav"><xsl:if test="$lang='cz'">potravinářství</xsl:if><xsl:if test="$lang='en'">food industry</xsl:if><xsl:if test="$lang='sk'">potravinárstvo</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastpráv"><xsl:if test="$lang='cz'">právo</xsl:if><xsl:if test="$lang='en'">law</xsl:if><xsl:if test="$lang='sk'">právo</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastprům"><xsl:if test="$lang='cz'">průmysl</xsl:if><xsl:if test="$lang='en'">industry</xsl:if><xsl:if test="$lang='sk'">priemysel</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastpřír"><xsl:if test="$lang='cz'">příroda</xsl:if><xsl:if test="$lang='en'">nature</xsl:if><xsl:if test="$lang='sk'">príroda</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastpsych"><xsl:if test="$lang='cz'">psychologie</xsl:if><xsl:if test="$lang='en'">psychology</xsl:if><xsl:if test="$lang='sk'">psychológia</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastrybn"><xsl:if test="$lang='cz'">rybníkářství</xsl:if><xsl:if test="$lang='en'">fish farming</xsl:if><xsl:if test="$lang='sk'">rybnikarstvi</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastřem"><xsl:if test="$lang='cz'">řemeslo</xsl:if><xsl:if test="$lang='en'">craft</xsl:if><xsl:if test="$lang='sk'">remeslo</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastsklář"><xsl:if test="$lang='cz'">sklářství</xsl:if><xsl:if test="$lang='en'">glassmaking</xsl:if><xsl:if test="$lang='sk'">sklárstvo</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastsoc"><xsl:if test="$lang='cz'">socialismus</xsl:if><xsl:if test="$lang='en'">socialism</xsl:if><xsl:if test="$lang='sk'">socializmus</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastsociol"><xsl:if test="$lang='cz'">sociologie</xsl:if><xsl:if test="$lang='en'">sociology</xsl:if><xsl:if test="$lang='sk'">sociológia</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblaststat"><xsl:if test="$lang='cz'">statistika</xsl:if><xsl:if test="$lang='en'">statistics</xsl:if><xsl:if test="$lang='sk'">štatistika</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblaststav"><xsl:if test="$lang='cz'">stavitelství</xsl:if><xsl:if test="$lang='en'">construction</xsl:if><xsl:if test="$lang='sk'">staviteľstvo</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastškol"><xsl:if test="$lang='cz'">školství</xsl:if><xsl:if test="$lang='en'">education</xsl:if><xsl:if test="$lang='sk'">školstvo</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblasttech"><xsl:if test="$lang='cz'">technika</xsl:if><xsl:if test="$lang='en'">technique</xsl:if><xsl:if test="$lang='sk'">technika</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblasttěl"><xsl:if test="$lang='cz'">sport</xsl:if><xsl:if test="$lang='en'">sport</xsl:if><xsl:if test="$lang='sk'">šport</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblasttext"><xsl:if test="$lang='cz'">textilnictví</xsl:if><xsl:if test="$lang='en'">textiles</xsl:if><xsl:if test="$lang='sk'">textilníctvo</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastúč"><xsl:if test="$lang='cz'">účetnictví</xsl:if><xsl:if test="$lang='en'">accounting</xsl:if><xsl:if test="$lang='sk'">účtovníctva</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastúř"><xsl:if test="$lang='cz'">úřední výraz</xsl:if><xsl:if test="$lang='en'">official statement</xsl:if><xsl:if test="$lang='sk'">úradné výraz</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastveřspr"><xsl:if test="$lang='cz'">veřejná správa</xsl:if><xsl:if test="$lang='en'">public administration</xsl:if><xsl:if test="$lang='sk'">verejná správa</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastvet"><xsl:if test="$lang='cz'">veterinářství</xsl:if><xsl:if test="$lang='en'">vererinarz</xsl:if><xsl:if test="$lang='sk'">veterinárstva</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastvoj"><xsl:if test="$lang='cz'">vojenství</xsl:if><xsl:if test="$lang='en'">military</xsl:if><xsl:if test="$lang='sk'">vojenstvo</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastvýptech"><xsl:if test="$lang='cz'">výpočetní technika</xsl:if><xsl:if test="$lang='en'">computing</xsl:if><xsl:if test="$lang='sk'">výpočtová technika</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastvýr"><xsl:if test="$lang='cz'">výroba</xsl:if><xsl:if test="$lang='en'">production</xsl:if><xsl:if test="$lang='sk'">výroba</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastvýtv"><xsl:if test="$lang='cz'">výtvarnictví</xsl:if><xsl:if test="$lang='en'">painting</xsl:if><xsl:if test="$lang='sk'">výtvarnictvo</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastzahr"><xsl:if test="$lang='cz'">zahradnictví</xsl:if><xsl:if test="$lang='en'">horticulture</xsl:if><xsl:if test="$lang='sk'">záhradníctvo</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastzbož"><xsl:if test="$lang='cz'">zbožíznalství</xsl:if><xsl:if test="$lang='en'">commodity knowledge</xsl:if><xsl:if test="$lang='sk'">tovaroznalectvo</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastzeměd"><xsl:if test="$lang='cz'">zemědělství</xsl:if><xsl:if test="$lang='en'">agriculture</xsl:if><xsl:if test="$lang='sk'">poľnohospodárstvo</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastzeměp"><xsl:if test="$lang='cz'">zeměpis</xsl:if><xsl:if test="$lang='en'">geography</xsl:if><xsl:if test="$lang='sk'">zemepis</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastzool"><xsl:if test="$lang='cz'">zoologie</xsl:if><xsl:if test="$lang='en'">zoology</xsl:if><xsl:if test="$lang='sk'">zoológia</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastcirkev"><xsl:if test="$lang='cz'">církev</xsl:if><xsl:if test="$lang='en'">church</xsl:if><xsl:if test="$lang='sk'">cirkev</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastadm"><xsl:if test="$lang='cz'">administrativa</xsl:if><xsl:if test="$lang='en'">administration</xsl:if><xsl:if test="$lang='sk'">administratíva</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastartistika"><xsl:if test="$lang='cz'">artistika</xsl:if><xsl:if test="$lang='en'">artistic</xsl:if><xsl:if test="$lang='sk'">artistika</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastastro"><xsl:if test="$lang='cz'">astronomie</xsl:if><xsl:if test="$lang='en'">astronomy</xsl:if><xsl:if test="$lang='sk'">astronómie</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastbiblhist"><xsl:if test="$lang='cz'">bibl. hist.</xsl:if><xsl:if test="$lang='en'">bibl. hist.</xsl:if><xsl:if test="$lang='sk'">bibl. hist.</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastbibl"><xsl:if test="$lang='cz'">biblistika</xsl:if><xsl:if test="$lang='en'">bibliography</xsl:if><xsl:if test="$lang='sk'">biblistiky</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastbiochemie"><xsl:if test="$lang='cz'">biochemie</xsl:if><xsl:if test="$lang='en'">biochemistry</xsl:if><xsl:if test="$lang='sk'">biochémia</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastcirar"><xsl:if test="$lang='cz'">círk. archit.</xsl:if><xsl:if test="$lang='en'">churches. archit.</xsl:if><xsl:if test="$lang='sk'">kačica. archit.</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastcirhi"><xsl:if test="$lang='cz'">církevní historie</xsl:if><xsl:if test="$lang='en'">church history</xsl:if><xsl:if test="$lang='sk'">cirkevná história</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastcirhu"><xsl:if test="$lang='cz'">círk. hud.</xsl:if><xsl:if test="$lang='en'">churches. hud.</xsl:if><xsl:if test="$lang='sk'">círk. hud.</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastcirkr"><xsl:if test="$lang='cz'">círk. křesť.</xsl:if><xsl:if test="$lang='en'">churches. baptism.</xsl:if><xsl:if test="$lang='sk'">círk. kresť.</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastcirpol"><xsl:if test="$lang='cz'">círk. polit.</xsl:if><xsl:if test="$lang='en'">churches. polite.</xsl:if><xsl:if test="$lang='sk'">círk. polit.</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastcirpr"><xsl:if test="$lang='cz'">círk. práv.</xsl:if><xsl:if test="$lang='en'">churches. rights.</xsl:if><xsl:if test="$lang='sk'">círk. práv.</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastcukr"><xsl:if test="$lang='cz'">cukrovarnictví</xsl:if><xsl:if test="$lang='en'">sugar industry</xsl:if><xsl:if test="$lang='sk'">cukrovarníctva</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastekpub"><xsl:if test="$lang='cz'">ekon. publ.</xsl:if><xsl:if test="$lang='en'">ekon. publ.</xsl:if><xsl:if test="$lang='sk'">ekon. publ.</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblasteltech"><xsl:if test="$lang='cz'">elektr.</xsl:if><xsl:if test="$lang='en'">electro.</xsl:if><xsl:if test="$lang='sk'">elektr.</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastestet"><xsl:if test="$lang='cz'">estetika</xsl:if><xsl:if test="$lang='en'">aesthetics</xsl:if><xsl:if test="$lang='sk'">estetika</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastfarmak"><xsl:if test="$lang='cz'">farmakologie</xsl:if><xsl:if test="$lang='en'">pharmacology</xsl:if><xsl:if test="$lang='sk'">farmakológia</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastfolklor"><xsl:if test="$lang='cz'">folkloristika</xsl:if><xsl:if test="$lang='en'">folklore</xsl:if><xsl:if test="$lang='sk'">folkloristiky</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastfonetika"><xsl:if test="$lang='cz'">fonetika</xsl:if><xsl:if test="$lang='en'">phonetics</xsl:if><xsl:if test="$lang='sk'">fonetika</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastfyzchem"><xsl:if test="$lang='cz'">fyzikální chemie</xsl:if><xsl:if test="$lang='en'">physical chemistry</xsl:if><xsl:if test="$lang='sk'">fyzikálna chémia</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastgenetika"><xsl:if test="$lang='cz'">genetika</xsl:if><xsl:if test="$lang='en'">genetics</xsl:if><xsl:if test="$lang='sk'">genetika</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastgeochem"><xsl:if test="$lang='cz'">geochem.</xsl:if><xsl:if test="$lang='en'">geochem.</xsl:if><xsl:if test="$lang='sk'">geochem.</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastgeodez"><xsl:if test="$lang='cz'">geodézie</xsl:if><xsl:if test="$lang='en'">geodesy</xsl:if><xsl:if test="$lang='sk'">geodézia</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastgeofyz"><xsl:if test="$lang='cz'">geofyzika</xsl:if><xsl:if test="$lang='en'">geophysics</xsl:if><xsl:if test="$lang='sk'">geofyzika</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastgeogr"><xsl:if test="$lang='cz'">geografie</xsl:if><xsl:if test="$lang='en'">geography</xsl:if><xsl:if test="$lang='sk'">geografia</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastgeolzem"><xsl:if test="$lang='cz'">geol. zeměd.</xsl:if><xsl:if test="$lang='en'">geol. countryside.</xsl:if><xsl:if test="$lang='sk'">geol. poľnohospodársk.</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblasthiadm"><xsl:if test="$lang='cz'">historie administrativy</xsl:if><xsl:if test="$lang='en'">history of administration</xsl:if><xsl:if test="$lang='sk'">histórie administratívy</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblasthiar"><xsl:if test="$lang='cz'">historie architektury</xsl:if><xsl:if test="$lang='en'">history of architecture</xsl:if><xsl:if test="$lang='sk'">histórie architektúry</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblasthiast"><xsl:if test="$lang='cz'">historie astronomie</xsl:if><xsl:if test="$lang='en'">history of astronomy</xsl:if><xsl:if test="$lang='sk'">histórie astronómie</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblasthidip"><xsl:if test="$lang='cz'">historie diplomacie</xsl:if><xsl:if test="$lang='en'">history of diplomacy</xsl:if><xsl:if test="$lang='sk'">histórie diplomacie</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblasthidiv"><xsl:if test="$lang='cz'">historie divadelnictví</xsl:if><xsl:if test="$lang='en'">history of theater</xsl:if><xsl:if test="$lang='sk'">histórie divadelníctva</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblasthieko"><xsl:if test="$lang='cz'">historie ekonomie</xsl:if><xsl:if test="$lang='en'">history of economics</xsl:if><xsl:if test="$lang='sk'">história ekonómie</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblasthietno"><xsl:if test="$lang='cz'">historie etnografie</xsl:if><xsl:if test="$lang='en'">history of ethnography</xsl:if><xsl:if test="$lang='sk'">histórie etnografia</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblasthifil"><xsl:if test="$lang='cz'">historie filozofie</xsl:if><xsl:if test="$lang='en'">history of philosophy</xsl:if><xsl:if test="$lang='sk'">história filozofia</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblasthifot"><xsl:if test="$lang='cz'">historie fotografování</xsl:if><xsl:if test="$lang='en'">photography history</xsl:if><xsl:if test="$lang='sk'">histórie fotografovanie</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblasthigeo"><xsl:if test="$lang='cz'">historie geografie</xsl:if><xsl:if test="$lang='en'">history of geography</xsl:if><xsl:if test="$lang='sk'">história geografia</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblasthilit"><xsl:if test="$lang='cz'">historie literatury</xsl:if><xsl:if test="$lang='en'">history of literature</xsl:if><xsl:if test="$lang='sk'">histórie literatúry</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblasthimat"><xsl:if test="$lang='cz'">historie matematika</xsl:if><xsl:if test="$lang='en'">history of mathematics</xsl:if><xsl:if test="$lang='sk'">histórie matematiky</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblasthinab"><xsl:if test="$lang='cz'">historie náboženství</xsl:if><xsl:if test="$lang='en'">history of religion</xsl:if><xsl:if test="$lang='sk'">histórie náboženstva</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblasthipen"><xsl:if test="$lang='cz'">historie peněžnictví</xsl:if><xsl:if test="$lang='en'">history of finance</xsl:if><xsl:if test="$lang='sk'">histórie peňažníctvo</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblasthipol"><xsl:if test="$lang='cz'">historie politiky</xsl:if><xsl:if test="$lang='en'">history of politics</xsl:if><xsl:if test="$lang='sk'">história politiky</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblasthipr"><xsl:if test="$lang='cz'">historie práva</xsl:if><xsl:if test="$lang='en'">history of law</xsl:if><xsl:if test="$lang='sk'">história práva</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblasthisk"><xsl:if test="$lang='cz'">historie školství</xsl:if><xsl:if test="$lang='en'">history of education</xsl:if><xsl:if test="$lang='sk'">histórie školstva</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblasthispo"><xsl:if test="$lang='cz'">historie sport</xsl:if><xsl:if test="$lang='en'">sport history</xsl:if><xsl:if test="$lang='sk'">histórie šport</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblasthista"><xsl:if test="$lang='cz'">historie stavitelství</xsl:if><xsl:if test="$lang='en'">history of construction</xsl:if><xsl:if test="$lang='sk'">história staviteľstva</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblasthitech"><xsl:if test="$lang='cz'">historie techniky</xsl:if><xsl:if test="$lang='en'">history of technology</xsl:if><xsl:if test="$lang='sk'">histórie techniky</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblasthivoj"><xsl:if test="$lang='cz'">historie vojenství</xsl:if><xsl:if test="$lang='en'">military history</xsl:if><xsl:if test="$lang='sk'">histórie vojenstva</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastsach"><xsl:if test="$lang='cz'">hra v šachy</xsl:if><xsl:if test="$lang='en'">chess game</xsl:if><xsl:if test="$lang='sk'">hra v šach</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblasthudpubl"><xsl:if test="$lang='cz'">hudební publicistika</xsl:if><xsl:if test="$lang='en'">musical journalism</xsl:if><xsl:if test="$lang='sk'">hudobné publicistika</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastjadtech"><xsl:if test="$lang='cz'">jad. tech.</xsl:if><xsl:if test="$lang='en'">I d. tech.</xsl:if><xsl:if test="$lang='sk'">jad. tech.</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastkeram"><xsl:if test="$lang='cz'">keramika</xsl:if><xsl:if test="$lang='en'">ceramics</xsl:if><xsl:if test="$lang='sk'">keramika</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastkniho"><xsl:if test="$lang='cz'">knihovnictví</xsl:if><xsl:if test="$lang='en'">librarianship</xsl:if><xsl:if test="$lang='sk'">knihovníctvo</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastkosmet"><xsl:if test="$lang='cz'">kosmet.</xsl:if><xsl:if test="$lang='en'">Kosmet.</xsl:if><xsl:if test="$lang='sk'">kozmet.</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastkosmon"><xsl:if test="$lang='cz'">kosmonautika</xsl:if><xsl:if test="$lang='en'">astronautics</xsl:if><xsl:if test="$lang='sk'">kozmonautika</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastkozed"><xsl:if test="$lang='cz'">kožedělný průmysl</xsl:if><xsl:if test="$lang='en'">the leather industry</xsl:if><xsl:if test="$lang='sk'">kožiarsky priemysel</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastkrejc"><xsl:if test="$lang='cz'">krejčovství</xsl:if><xsl:if test="$lang='en'">tailoring</xsl:if><xsl:if test="$lang='sk'">krajčírstvo</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastkynol"><xsl:if test="$lang='cz'">kynologie</xsl:if><xsl:if test="$lang='en'">kynologie</xsl:if><xsl:if test="$lang='sk'">kynológia</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastlesnic"><xsl:if test="$lang='cz'">lesnictví</xsl:if><xsl:if test="$lang='en'">forestry</xsl:if><xsl:if test="$lang='sk'">lesníctvo</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastlethist"><xsl:if test="$lang='cz'">let. hist.</xsl:if><xsl:if test="$lang='en'">flight. hist.</xsl:if><xsl:if test="$lang='sk'">rokov. hist.</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastlingv"><xsl:if test="$lang='cz'">lingvistika</xsl:if><xsl:if test="$lang='en'">linguistics</xsl:if><xsl:if test="$lang='sk'">lingvistika</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastliter"><xsl:if test="$lang='cz'">literární věda</xsl:if><xsl:if test="$lang='en'">literary criticism</xsl:if><xsl:if test="$lang='sk'">literárna veda</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastlodar"><xsl:if test="$lang='cz'">loďařství</xsl:if><xsl:if test="$lang='en'">shipbuilding</xsl:if><xsl:if test="$lang='sk'">lodiarstve</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastmatlog"><xsl:if test="$lang='cz'">mat. log.</xsl:if><xsl:if test="$lang='en'">mat. log.</xsl:if><xsl:if test="$lang='sk'">mat. log.</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastmezobch"><xsl:if test="$lang='cz'">mezinár. obch.</xsl:if><xsl:if test="$lang='en'">internat. obchod.</xsl:if><xsl:if test="$lang='sk'">medzinár. obch.</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastmezpr"><xsl:if test="$lang='cz'">mezinárodní právo</xsl:if><xsl:if test="$lang='en'">international law</xsl:if><xsl:if test="$lang='sk'">medzinárodné právo</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastnabpol"><xsl:if test="$lang='cz'">náb. polit.</xsl:if><xsl:if test="$lang='en'"></xsl:if><xsl:if test="$lang='sk'">náb. polit.</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastnabps"><xsl:if test="$lang='cz'">náb. ps.</xsl:if><xsl:if test="$lang='en'"></xsl:if><xsl:if test="$lang='sk'">náb. ps.</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastnamor"><xsl:if test="$lang='cz'">námořnictví</xsl:if><xsl:if test="$lang='en'">shipping</xsl:if><xsl:if test="$lang='sk'">námorníctva</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastnumiz"><xsl:if test="$lang='cz'">numizmatika</xsl:if><xsl:if test="$lang='en'">numismatics</xsl:if><xsl:if test="$lang='sk'">numizmatika</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastobchpr"><xsl:if test="$lang='cz'">obch. práv.</xsl:if><xsl:if test="$lang='en'">obchod. rights.</xsl:if><xsl:if test="$lang='sk'">obch. práv.</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastobuv"><xsl:if test="$lang='cz'">obuvnictví</xsl:if><xsl:if test="$lang='en'">shoe industry</xsl:if><xsl:if test="$lang='sk'">obuvníctvo</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastodb"><xsl:if test="$lang='cz'">odb.</xsl:if><xsl:if test="$lang='en'">odb.</xsl:if><xsl:if test="$lang='sk'">odb.</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastoptika"><xsl:if test="$lang='cz'">optika</xsl:if><xsl:if test="$lang='en'">optics</xsl:if><xsl:if test="$lang='sk'">optika</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastorient"><xsl:if test="$lang='cz'">orient.</xsl:if><xsl:if test="$lang='en'">orient.</xsl:if><xsl:if test="$lang='sk'">orient.</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastpaleo"><xsl:if test="$lang='cz'">paleontologie</xsl:if><xsl:if test="$lang='en'">paleontology</xsl:if><xsl:if test="$lang='sk'">paleontológia</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastpapir"><xsl:if test="$lang='cz'">papírenství</xsl:if><xsl:if test="$lang='en'">paper industry</xsl:if><xsl:if test="$lang='sk'">papierenstvo</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastpedol"><xsl:if test="$lang='cz'">pedol.</xsl:if><xsl:if test="$lang='en'">pedol.</xsl:if><xsl:if test="$lang='sk'">pedol.</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastpiv"><xsl:if test="$lang='cz'">piv.</xsl:if><xsl:if test="$lang='en'">beer.</xsl:if><xsl:if test="$lang='sk'">pív.</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastpolitadm"><xsl:if test="$lang='cz'">polit. a adm.</xsl:if><xsl:if test="$lang='en'">polite. to adm.</xsl:if><xsl:if test="$lang='sk'">polit. a adm.</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastpoliteko"><xsl:if test="$lang='cz'">polit. ekon.</xsl:if><xsl:if test="$lang='en'"></xsl:if><xsl:if test="$lang='sk'">polit. ekon.</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastpotrobch"><xsl:if test="$lang='cz'">potr. obch.</xsl:if><xsl:if test="$lang='en'"></xsl:if><xsl:if test="$lang='sk'">potr. obch.</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastpubl"><xsl:if test="$lang='cz'">publicistika</xsl:if><xsl:if test="$lang='en'">journalism</xsl:if><xsl:if test="$lang='sk'">publicistika</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastrybar"><xsl:if test="$lang='cz'">rybářství</xsl:if><xsl:if test="$lang='en'">fishing</xsl:if><xsl:if test="$lang='sk'">rybárstvo</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastsdel"><xsl:if test="$lang='cz'">sdělovací technika</xsl:if><xsl:if test="$lang='en'">Communication Technology</xsl:if><xsl:if test="$lang='sk'">oznamovacia technika</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastsport"><xsl:if test="$lang='cz'">sport</xsl:if><xsl:if test="$lang='en'">sport</xsl:if><xsl:if test="$lang='sk'">šport</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastsporpub"><xsl:if test="$lang='cz'">sportovní publicistika</xsl:if><xsl:if test="$lang='en'">sports journalism</xsl:if><xsl:if test="$lang='sk'">športové publicistika</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblaststroj"><xsl:if test="$lang='cz'">strojírenství</xsl:if><xsl:if test="$lang='en'">engineering</xsl:if><xsl:if test="$lang='sk'">strojárstvo</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblasttanec"><xsl:if test="$lang='cz'">tanec</xsl:if><xsl:if test="$lang='en'">dance</xsl:if><xsl:if test="$lang='sk'">tanec</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastteol"><xsl:if test="$lang='cz'">teol.</xsl:if><xsl:if test="$lang='en'">theol.</xsl:if><xsl:if test="$lang='sk'">Teolo.</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblasttesn"><xsl:if test="$lang='cz'">těsn.</xsl:if><xsl:if test="$lang='en'">tightly.</xsl:if><xsl:if test="$lang='sk'">tesné.</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblasttextobch"><xsl:if test="$lang='cz'">text. obch.</xsl:if><xsl:if test="$lang='en'"></xsl:if><xsl:if test="$lang='sk'">text. obch.</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastumel"><xsl:if test="$lang='cz'">umělecký</xsl:if><xsl:if test="$lang='en'">artistic</xsl:if><xsl:if test="$lang='sk'">umelecký</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastumrem"><xsl:if test="$lang='cz'">uměl. řem.</xsl:if><xsl:if test="$lang='en'"></xsl:if><xsl:if test="$lang='sk'">vedel. rom.</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastvcel"><xsl:if test="$lang='cz'">včel.</xsl:if><xsl:if test="$lang='en'">bees.</xsl:if><xsl:if test="$lang='sk'">včiel.</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastvinar"><xsl:if test="$lang='cz'">vinařství</xsl:if><xsl:if test="$lang='en'">viticulture</xsl:if><xsl:if test="$lang='sk'">vinárstvo</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastvodohos"><xsl:if test="$lang='cz'">vodohospodářství</xsl:if><xsl:if test="$lang='en'">water management</xsl:if><xsl:if test="$lang='sk'">vodohospodárstvo</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastvojhist"><xsl:if test="$lang='cz'">voj. hist.</xsl:if><xsl:if test="$lang='en'"></xsl:if><xsl:if test="$lang='sk'">voj. hist.</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastvojnam"><xsl:if test="$lang='cz'">voj. nám.</xsl:if><xsl:if test="$lang='en'"></xsl:if><xsl:if test="$lang='sk'">voj. nám.</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastvor"><xsl:if test="$lang='cz'">vorařský</xsl:if><xsl:if test="$lang='en'">voryřský</xsl:if><xsl:if test="$lang='sk'">pltníckej</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastzeldop"><xsl:if test="$lang='cz'">železniční doprava</xsl:if><xsl:if test="$lang='en'">rail transport</xsl:if><xsl:if test="$lang='sk'">železničná doprava</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastzpravpubl"><xsl:if test="$lang='cz'">zprav. publ.</xsl:if><xsl:if test="$lang='en'">report. publ.</xsl:if><xsl:if test="$lang='sk'">sprav. publ.</xsl:if></xsl:variable>
  <xsl:variable name="loc-csoblastzurn"><xsl:if test="$lang='cz'">žurn.</xsl:if><xsl:if test="$lang='en'">jurn.</xsl:if><xsl:if test="$lang='sk'">žurn.</xsl:if></xsl:variable>
  <xsl:variable name="web-napoveda"><xsl:if test="$lang='cz'">Nápověda</xsl:if><xsl:if test="$lang='en'">Help</xsl:if><xsl:if test="$lang='sk'">Pomoc</xsl:if></xsl:variable>
  <xsl:variable name="web-kontakt"><xsl:if test="$lang='cz'">Kontakt</xsl:if><xsl:if test="$lang='en'">Contact</xsl:if><xsl:if test="$lang='sk'">Kontakt</xsl:if></xsl:variable>
  <xsl:variable name="web-oslovniku"><xsl:if test="$lang='cz'">O slovníku</xsl:if><xsl:if test="$lang='en'">About</xsl:if><xsl:if test="$lang='sk'">O slovníku</xsl:if></xsl:variable>
  <xsl:variable name="web-pagetitle"><xsl:if test="$lang='cz'">Výkladový slovník online</xsl:if><xsl:if test="$lang='en'">Online Dictionary </xsl:if><xsl:if test="$lang='sk'">Výkladový slovník online</xsl:if></xsl:variable>
  <xsl:variable name="web-title"><xsl:if test="$lang='cz'">Vícejazyčný výkladový slovník online</xsl:if><xsl:if test="$lang='en'">Multilingual Online Dictionary </xsl:if><xsl:if test="$lang='sk'">Viacjazykový slovník online</xsl:if></xsl:variable>
  <xsl:variable name="web-bottomtitle1"><xsl:if test="$lang='cz'">VÍCEJAZYČNÝ VÝKLADOVÝ SLOVNÍK</xsl:if><xsl:if test="$lang='en'">Multilingual Online Dictionary </xsl:if><xsl:if test="$lang='sk'">VIACJAZYKOVÝ SLOVNÍK ONLINE</xsl:if></xsl:variable>
  <xsl:variable name="web-bottomtitle2"><xsl:if test="$lang='cz'">ONLINE</xsl:if><xsl:if test="$lang='en'">ONLINE</xsl:if><xsl:if test="$lang='sk'">ONLINE</xsl:if></xsl:variable>
  <xsl:variable name="web-bottomright1"><xsl:if test="$lang='cz'">ČEŠTINA</xsl:if><xsl:if test="$lang='en'">CZECH</xsl:if><xsl:if test="$lang='sk'">SLOVENSKÝ POSUNKOVÝ JAZYK</xsl:if></xsl:variable>
  <xsl:variable name="web-bottomright2"><xsl:if test="$lang='cz'">ČESKÝ ZNAKOVÝ JAZYK</xsl:if><xsl:if test="$lang='en'">CZECH SIGN LANGUAGE</xsl:if><xsl:if test="$lang='sk'">SLOVENČINA</xsl:if></xsl:variable>
  <xsl:variable name="web-bottomright3"><xsl:if test="$lang='cz'">MEZINÁRODNÍ ZNAKOVÝ SYSTÉM</xsl:if><xsl:if test="$lang='en'">INTERNATIONAL SIGN LANGUAGE</xsl:if><xsl:if test="$lang='sk'">MEDZINÁRODNÝ ZNAKOVÝ SYSTÉM</xsl:if></xsl:variable>
  <xsl:variable name="web-bottomright4"><xsl:if test="$lang='cz'">...</xsl:if><xsl:if test="$lang='en'">…</xsl:if><xsl:if test="$lang='sk'">…</xsl:if></xsl:variable>
  <xsl:variable name="web-copyright"><xsl:if test="$lang='cz'">© 2014-18 Masarykova univerzita | Všechna práva vyhrazena</xsl:if><xsl:if test="$lang='en'">© 2014-18 Masaryk University | All Rights Reserved</xsl:if><xsl:if test="$lang='sk'">© 2014-18 Masarykova univerzita | Všetky práva vyhradené</xsl:if></xsl:variable>
  <xsl:variable name="web-vysledekhledani"><xsl:if test="$lang='cz'">výsledek hledání</xsl:if><xsl:if test="$lang='en'">Search results</xsl:if><xsl:if test="$lang='sk'">výsledok hledánia</xsl:if></xsl:variable>
  <xsl:variable name="web-vykladovyslovnik"><xsl:if test="$lang='cz'">VÝKLADOVÝ SLOVNÍK</xsl:if><xsl:if test="$lang='en'">DICTIONARY</xsl:if><xsl:if test="$lang='sk'">VÝKLADOVÝ SLOVNÍK</xsl:if></xsl:variable>
  <xsl:variable name="web-prekladovyslovnik"><xsl:if test="$lang='cz'">PŘEKLADOVÝ SLOVNÍK</xsl:if><xsl:if test="$lang='en'">TRANSLATE</xsl:if><xsl:if test="$lang='sk'">PREKLADOVÝ SLOVNÍK</xsl:if></xsl:variable>
  <xsl:variable name="web-dict-czj"><xsl:if test="$lang='cz'">český znakový jazyk</xsl:if><xsl:if test="$lang='en'">Czech Sign Language</xsl:if><xsl:if test="$lang='sk'">český posunkový jazyk</xsl:if></xsl:variable>
  <xsl:variable name="web-dict-cs"><xsl:if test="$lang='cz'">čeština</xsl:if><xsl:if test="$lang='en'">Czech</xsl:if><xsl:if test="$lang='sk'">čeština</xsl:if></xsl:variable>
  <xsl:variable name="web-dict-en"><xsl:if test="$lang='cz'">angličtina</xsl:if><xsl:if test="$lang='en'">English</xsl:if><xsl:if test="$lang='sk'">angličtina</xsl:if></xsl:variable>
  <xsl:variable name="web-dict-asl"><xsl:if test="$lang='cz'">americký znakový jazyk</xsl:if><xsl:if test="$lang='en'">American Sign Language</xsl:if><xsl:if test="$lang='sk'">americký posunkový jazyk</xsl:if></xsl:variable>
  <xsl:variable name="web-dict-is"><xsl:if test="$lang='cz'">mezinárodní znaky</xsl:if><xsl:if test="$lang='en'">International Sings</xsl:if><xsl:if test="$lang='sk'">medzinárodné posunky</xsl:if></xsl:variable>
  <xsl:variable name="web-dict-spj"><xsl:if test="$lang='cz'">slovenský znakový jazyk</xsl:if><xsl:if test="$lang='en'">Slovak Sign Language</xsl:if><xsl:if test="$lang='sk'">slovenský posunkový jazyk</xsl:if></xsl:variable>
  <xsl:variable name="web-dict-sj"><xsl:if test="$lang='cz'">slovenština</xsl:if><xsl:if test="$lang='en'">Slovak</xsl:if><xsl:if test="$lang='sk'">slovenčina</xsl:if></xsl:variable>
  <xsl:variable name="web-uzivatel"><xsl:if test="$lang='cz'">uživatel</xsl:if><xsl:if test="$lang='en'">user</xsl:if><xsl:if test="$lang='sk'">použivaťeľ</xsl:if></xsl:variable>
  <xsl:variable name="web-vyhledavani"><xsl:if test="$lang='cz'">Vyhledávání</xsl:if><xsl:if test="$lang='en'">Search</xsl:if><xsl:if test="$lang='sk'">Hledánie</xsl:if></xsl:variable>
  <xsl:variable name="web-moznosti"><xsl:if test="$lang='cz'">Možnosti zadání</xsl:if><xsl:if test="$lang='en'">Input options</xsl:if><xsl:if test="$lang='sk'">Možnosti zadánia</xsl:if></xsl:variable>
  <xsl:variable name="web-tvarmisto"><xsl:if test="$lang='cz'">Tvar ruky + místo</xsl:if><xsl:if test="$lang='en'">Handshape&amp;Space</xsl:if><xsl:if test="$lang='sk'">Tvar ruky + miesto</xsl:if></xsl:variable>
  <xsl:variable name="web-signwriting"><xsl:if test="$lang='cz'">SignWriting</xsl:if><xsl:if test="$lang='en'">SignWriting</xsl:if><xsl:if test="$lang='sk'">SignWriting</xsl:if></xsl:variable>
  <xsl:variable name="web-czpreklad"><xsl:if test="$lang='cz'">Český překlad</xsl:if><xsl:if test="$lang='en'">Czech Translation</xsl:if><xsl:if test="$lang='sk'">Český preklad</xsl:if></xsl:variable>
 <xsl:variable name="web-dominantni-ruka"><xsl:if test="$lang='cz'">tvar dominantní ruky</xsl:if><xsl:if test="$lang='en'">dominant hand</xsl:if><xsl:if test="$lang='sk'">tvar dominantnej ruky</xsl:if></xsl:variable>
  <xsl:variable name="web-misto-artikulace"><xsl:if test="$lang='cz'">místo artikulace</xsl:if><xsl:if test="$lang='en'">signing space</xsl:if><xsl:if test="$lang='sk'">miesto artikulácie</xsl:if></xsl:variable>
  <xsl:variable name="web-obourucni"><xsl:if test="$lang='cz'">obouruční znak</xsl:if><xsl:if test="$lang='en'">two-hand sign</xsl:if><xsl:if test="$lang='sk'">dvojručný znak</xsl:if></xsl:variable>
  <xsl:variable name="web-stejnytvar"><xsl:if test="$lang='cz'">stejný tvar</xsl:if><xsl:if test="$lang='en'">both hands the same handshape</xsl:if><xsl:if test="$lang='sk'">rovnaký tvar</xsl:if></xsl:variable>
  <xsl:variable name="web-obeaktivni"><xsl:if test="$lang='cz'">obě aktivní</xsl:if><xsl:if test="$lang='en'">both hands moving</xsl:if><xsl:if test="$lang='sk'">obe aktívne</xsl:if></xsl:variable>
 <xsl:variable name="web-search-more"><xsl:if test="$lang='cz'">více možností</xsl:if><xsl:if test="$lang='en'">show more</xsl:if><xsl:if test="$lang='sk'">viac možností</xsl:if></xsl:variable>
  <xsl:variable name="web-search-less"><xsl:if test="$lang='cz'">méně možností</xsl:if><xsl:if test="$lang='en'">show less</xsl:if><xsl:if test="$lang='sk'">menej možností</xsl:if></xsl:variable>
 <xsl:variable name="web-contact"><xsl:if test="$lang='cz'">dotyk</xsl:if><xsl:if test="$lang='en'">contact</xsl:if><xsl:if test="$lang='sk'">dotyk</xsl:if></xsl:variable>
  <xsl:variable name="web-direct"><xsl:if test="$lang='cz'">přímý pohyb</xsl:if><xsl:if test="$lang='en'">straight movement</xsl:if><xsl:if test="$lang='sk'">rovný pohyb</xsl:if></xsl:variable>
  <xsl:variable name="web-curved"><xsl:if test="$lang='cz'">zakřivený pohyb</xsl:if><xsl:if test="$lang='en'">curved movement</xsl:if><xsl:if test="$lang='sk'">zakrivený pohyb</xsl:if></xsl:variable>
<xsl:variable name="web-slovnidruh"><xsl:if test="$lang='cz'">slovní druh</xsl:if><xsl:if test="$lang='en'">part of speech</xsl:if><xsl:if test="$lang='sk'">slovný druh</xsl:if></xsl:variable>
  <xsl:variable name="web-podstjm"><xsl:if test="$lang='cz'">podstatné jméno</xsl:if><xsl:if test="$lang='en'">noun</xsl:if><xsl:if test="$lang='sk'">podstatné meno</xsl:if></xsl:variable>
  <xsl:variable name="web-sloveso"><xsl:if test="$lang='cz'">sloveso</xsl:if><xsl:if test="$lang='en'">verb</xsl:if><xsl:if test="$lang='sk'">sloveso</xsl:if></xsl:variable>
  <xsl:variable name="web-modifikator"><xsl:if test="$lang='cz'">modifikátor</xsl:if><xsl:if test="$lang='en'">modifier</xsl:if><xsl:if test="$lang='sk'">modifikátor</xsl:if></xsl:variable>
  <xsl:variable name="web-zajmeno"><xsl:if test="$lang='cz'">zájmeno</xsl:if><xsl:if test="$lang='en'">pronoun</xsl:if><xsl:if test="$lang='sk'">zámeno</xsl:if></xsl:variable>
  <xsl:variable name="web-cislovka"><xsl:if test="$lang='cz'">číslovka</xsl:if><xsl:if test="$lang='en'">numeral</xsl:if><xsl:if test="$lang='sk'">číslovka</xsl:if></xsl:variable>
  <xsl:variable name="web-spojka"><xsl:if test="$lang='cz'">spojka</xsl:if><xsl:if test="$lang='en'">conjunction</xsl:if><xsl:if test="$lang='sk'">spojka</xsl:if></xsl:variable>
  <xsl:variable name="web-castice"><xsl:if test="$lang='cz'">částice</xsl:if><xsl:if test="$lang='en'">particle</xsl:if><xsl:if test="$lang='sk'">částice</xsl:if></xsl:variable>
  <xsl:variable name="web-tazaci"><xsl:if test="$lang='cz'">tázací výraz</xsl:if><xsl:if test="$lang='en'">questioner</xsl:if><xsl:if test="$lang='sk'">opytovací výraz</xsl:if></xsl:variable>
  <xsl:variable name="web-kategorizacni"><xsl:if test="$lang='cz'">kategorizační výraz</xsl:if><xsl:if test="$lang='en'">categorizationer</xsl:if><xsl:if test="$lang='sk'">kategorizačný výraz</xsl:if></xsl:variable>
  <xsl:variable name="web-klasifikatorovy"><xsl:if test="$lang='cz'">klasifikátorový tvar ruky</xsl:if><xsl:if test="$lang='en'">classifier handshape</xsl:if><xsl:if test="$lang='sk'">klasifikátorový tvar ruky</xsl:if></xsl:variable>
  <xsl:variable name="web-specifikator"><xsl:if test="$lang='cz'">specifikátor</xsl:if><xsl:if test="$lang='en'">specifier</xsl:if><xsl:if test="$lang='sk'">specifikátor</xsl:if></xsl:variable>
  <xsl:variable name="web-zadejte"><xsl:if test="$lang='cz'">zadejte slovo</xsl:if><xsl:if test="$lang='en'">enter a word</xsl:if><xsl:if test="$lang='sk'">zadajte slovo</xsl:if></xsl:variable>
  <xsl:variable name="web-zadejteid"><xsl:if test="$lang='cz'">zadejte slovo nebo ID</xsl:if><xsl:if test="$lang='en'">enter a word or ID</xsl:if><xsl:if test="$lang='sk'">zadajte slovo či ID</xsl:if></xsl:variable>
  <xsl:variable name="web-smazatvse"><xsl:if test="$lang='cz'">smazat vše</xsl:if><xsl:if test="$lang='en'">Clear All</xsl:if><xsl:if test="$lang='sk'">Zmazať všetko</xsl:if></xsl:variable>
  <xsl:variable name="web-hledat"><xsl:if test="$lang='cz'">hledat</xsl:if><xsl:if test="$lang='en'">Search</xsl:if><xsl:if test="$lang='sk'">hľadať</xsl:if></xsl:variable>
  <xsl:variable name="web-zadatupravit"><xsl:if test="$lang='cz'">zadat / upravit</xsl:if><xsl:if test="$lang='en'">Add / Modify</xsl:if><xsl:if test="$lang='sk'">zadať / upraviť</xsl:if></xsl:variable>
  <xsl:variable name="web-nalezeno"><xsl:if test="$lang='cz'">Nalezeno</xsl:if><xsl:if test="$lang='en'">Found</xsl:if><xsl:if test="$lang='sk'">Nalezeno</xsl:if></xsl:variable>
  <xsl:variable name="web-vicenez"><xsl:if test="$lang='cz'">více než</xsl:if><xsl:if test="$lang='en'">more than</xsl:if><xsl:if test="$lang='sk'">viac než</xsl:if></xsl:variable>
  <xsl:variable name="web-raditpodle"><xsl:if test="$lang='cz'">řadit podle</xsl:if><xsl:if test="$lang='en'">order by</xsl:if><xsl:if test="$lang='sk'">seradiť podľa</xsl:if></xsl:variable>
  <xsl:variable name="web-radittvaru"><xsl:if test="$lang='cz'">tvarů rukou</xsl:if><xsl:if test="$lang='en'">Handshape</xsl:if><xsl:if test="$lang='sk'">tvaru ruky</xsl:if></xsl:variable>
  <xsl:variable name="web-raditmista"><xsl:if test="$lang='cz'">místa artikulace</xsl:if><xsl:if test="$lang='en'">Signing Space</xsl:if><xsl:if test="$lang='sk'">miesta artikulácie</xsl:if></xsl:variable>
  <xsl:variable name="web-varianty"><xsl:if test="$lang='cz'">varianty</xsl:if><xsl:if test="$lang='en'">variants</xsl:if><xsl:if test="$lang='sk'">varianty</xsl:if></xsl:variable>
  <xsl:variable name="web-predchozi"><xsl:if test="$lang='cz'">předchozí</xsl:if><xsl:if test="$lang='en'">previous</xsl:if><xsl:if test="$lang='sk'">predchádzajúci</xsl:if></xsl:variable>
  <xsl:variable name="web-dalsi"><xsl:if test="$lang='cz'">další</xsl:if><xsl:if test="$lang='en'">next</xsl:if><xsl:if test="$lang='sk'">ďalší</xsl:if></xsl:variable>
  <xsl:variable name="web-javascript"><xsl:if test="$lang='cz'">Pro správné fungování slovníku je vyžadován JavaScript. Prosím povolte jej.</xsl:if><xsl:if test="$lang='en'">This site requires JavaScript to be enabled.</xsl:if><xsl:if test="$lang='sk'">Pre správne fungovanie slovníka je potrebný JavaScript. Prosím povoľte ho.</xsl:if></xsl:variable>
  <xsl:variable name="web-zadneheslo"><xsl:if test="$lang='cz'">Nenalezeno žádné heslo. </xsl:if><xsl:if test="$lang='en'">Nothing found.</xsl:if><xsl:if test="$lang='sk'">Nič nenalezeno.</xsl:if></xsl:variable>
  <xsl:variable name="web-pouzijtehledani"><xsl:if test="$lang='cz'">Pro zobrazení slovníkového hesla použijte vyhledávání v levé části okna.</xsl:if><xsl:if test="$lang='en'">Please use the searchin tool to see some entries.</xsl:if><xsl:if test="$lang='sk'">Pre zobrazenie slovníkového hesla použite vyhľadávanie v ľavej časti okna.</xsl:if></xsl:variable>
  <xsl:variable name="web-zadejtecast"><xsl:if test="$lang='cz'">Zadejte jen část slova, nebo zkuste jiný typ hledání.</xsl:if><xsl:if test="$lang='en'">Try to enter only part of the word or try different input option.</xsl:if><xsl:if test="$lang='sk'">Zadajte iba časť slova, alebo skúste iný typ hľadania.</xsl:if></xsl:variable>
  <xsl:variable name="web-zadejtekratsi"><xsl:if test="$lang='cz'">Zkuste zadat kratší slovo, nebo jen jeho část.</xsl:if><xsl:if test="$lang='en'">Try to enter only part of the word.</xsl:if><xsl:if test="$lang='sk'">Zadajte kratšie slovo alebo jeho časť.</xsl:if></xsl:variable>
  <xsl:variable name="web-seznamvybranych"><xsl:if test="$lang='cz'">seznam vybraných odborných znaků</xsl:if><xsl:if test="$lang='en'">Maybe you are interested in the list of some technical terms</xsl:if><xsl:if test="$lang='sk'">Môžete si tiež zobraziť zoznam vybraných odborných posunkov</xsl:if></xsl:variable>
  <xsl:variable name="web-slovnitvary"><xsl:if test="$lang='cz'">hledat slovní tvary</xsl:if><xsl:if test="$lang='en'"></xsl:if><xsl:if test="$lang='sk'"></xsl:if></xsl:variable>
  <xsl:variable name="web-diakritika"><xsl:if test="$lang='cz'">hledat bez diakritiky</xsl:if><xsl:if test="$lang='en'"></xsl:if><xsl:if test="$lang='sk'"></xsl:if></xsl:variable>
  <xsl:variable name="web-hledatspojeni"><xsl:if test="$lang='cz'">hledat slovní spojení</xsl:if><xsl:if test="$lang='en'"></xsl:if><xsl:if test="$lang='sk'"></xsl:if></xsl:variable>
  <xsl:variable name="web-prekladyczj"><xsl:if test="$lang='cz'">i překlady ČZ</xsl:if><xsl:if test="$lang='en'"></xsl:if><xsl:if test="$lang='sk'"></xsl:if></xsl:variable>
  <xsl:variable name="web-prelozit"><xsl:if test="$lang='cz'">přeložit</xsl:if><xsl:if test="$lang='en'">Translate</xsl:if><xsl:if test="$lang='sk'">preložiť</xsl:if></xsl:variable>
  <xsl:variable name="web-nalezenoprekladu"><xsl:if test="$lang='cz'">Nalezeno překladů</xsl:if><xsl:if test="$lang='en'">Translations found:</xsl:if><xsl:if test="$lang='sk'">Nalezeno prekladov:</xsl:if></xsl:variable>
  <xsl:variable name="web-otevritvyklad"><xsl:if test="$lang='cz'">Otevřít heslo ve výkladovém slovníku</xsl:if><xsl:if test="$lang='en'">Open entry in the dictionary</xsl:if><xsl:if test="$lang='sk'">Otvoriť heslo vo výkladovom slovníku</xsl:if></xsl:variable>
  <xsl:variable name="web-predchoziV"><xsl:if test="$lang='cz'">PŘEDCHOZÍ</xsl:if><xsl:if test="$lang='en'">PREVIOUS</xsl:if><xsl:if test="$lang='sk'">PRECHÁDZAJÚCI</xsl:if></xsl:variable>
  <xsl:variable name="web-dalsiV"><xsl:if test="$lang='cz'">DALŠÍ</xsl:if><xsl:if test="$lang='en'">NEXT</xsl:if><xsl:if test="$lang='sk'">ĎALŠÍ</xsl:if></xsl:variable>
  <xsl:variable name="web-preklad"><xsl:if test="$lang='cz'">Překlad</xsl:if><xsl:if test="$lang='en'">Translation</xsl:if><xsl:if test="$lang='sk'">Preklad</xsl:if></xsl:variable>
  <xsl:variable name="web-orientacni"><xsl:if test="$lang='cz'">orientační popis</xsl:if><xsl:if test="$lang='en'">description</xsl:if><xsl:if test="$lang='sk'">orientačný popis</xsl:if></xsl:variable>
  <xsl:variable name="web-searchpreklad-czj"><xsl:if test="$lang='cz'">Český překlad</xsl:if><xsl:if test="$lang='en'">Czech Translation</xsl:if><xsl:if test="$lang='sk'">český preklad</xsl:if></xsl:variable>
  <xsl:variable name="web-searchpreklad-asl"><xsl:if test="$lang='cz'">Anglický překlad</xsl:if><xsl:if test="$lang='en'">English Translation</xsl:if><xsl:if test="$lang='sk'">anglický preklad</xsl:if></xsl:variable>
  <xsl:variable name="web-searchpreklad-is"><xsl:if test="$lang='cz'">Anglický překlad</xsl:if><xsl:if test="$lang='en'">English Translation</xsl:if><xsl:if test="$lang='sk'">anglický preklad</xsl:if></xsl:variable>
  <xsl:variable name="web-searchpreklad-spj"><xsl:if test="$lang='cz'">Slovenský překlad</xsl:if><xsl:if test="$lang='en'">Slovak Translation</xsl:if><xsl:if test="$lang='sk'">slovenský preklad</xsl:if></xsl:variable>
 <xsl:variable name="web-chrome-warning"><xsl:if test="$lang='cz'">V prohlížeči Chrome se aktuálně nemusí některá videa přehrávat správně. V případě problémů doporučujeme jiný prohlížeč.</xsl:if><xsl:if test="$lang='en'">In Chrome may occur some videos does not play correctly. In that case please use another browser.</xsl:if><xsl:if test="$lang='sk'">V Prohlížeči Chrome sa niektorá videá nemusí správne prehrávať. V prípade problémov doporučujeme iný prehliadač.</xsl:if></xsl:variable>
<xsl:variable name="web-def-search"><xsl:if test="$lang='cz'">Celkem hesel</xsl:if><xsl:if test="$lang='en'">Entries in total</xsl:if><xsl:if test="$lang='sk'">Celkom hesiel</xsl:if></xsl:variable>
<xsl:variable name="web-def-translate"><xsl:if test="$lang='cz'">Celkem překladů</xsl:if><xsl:if test="$lang='en'">Translations in total</xsl:if><xsl:if test="$lang='sk'">Celkom prekladov</xsl:if></xsl:variable>
<xsl:variable name="web-notall"><xsl:if test="$lang='cz'">Toto není vše. Neveřejná hesla najdete v administraci.</xsl:if><xsl:if test="$lang='en'">This list doesn't contain not published entries. You can find them in the Admin section.</xsl:if><xsl:if test="$lang='sk'">Tu nieje všetko. Neverejná heslá môžete zobraziť v administrácii.</xsl:if></xsl:variable>
</xsl:stylesheet>
