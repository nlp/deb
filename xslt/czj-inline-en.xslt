<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="utf-8" method="xml"
  doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
  doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
  />

<xsl:param name="perm"/>
  <xsl:param name="dictcode"/>
  <xsl:variable name="skupina_test">
    <!--ma uzivatel stejnou skupinu jako heslo? nebo bez omezeni-->
    <xsl:if test="contains(/entry/@user_skupina,'all') or contains($perm, 'admin')">true</xsl:if>
    <xsl:if test="not(contains(/entry/@user_skupina,'all')) and not(contains($perm, 'admin'))"><xsl:value-of select="contains(/entry/@user_skupina, /entry/lemma/pracskupina)"/></xsl:if>
  </xsl:variable>

<xsl:key name="label" match="/entry/media/file/label/text()" use="." />

<xsl:template match="entry">
  <html>
     <head>
       <title>CSL: <xsl:value-of select="@id"/></title>
    <link rel="stylesheet" type="text/css" media="screen" href="/editor/css/czj-common.css" ></link>
    <link rel="stylesheet" type="text/css" media="screen" href="/editor/css/czj-preview7.css" ></link>
    <link rel="stylesheet" type="text/css" media="screen" href="/editor/css/czj-preview7-min.css" ></link>

    <link href="/editor/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="/editor/lib/skin/functional.css" rel="stylesheet" type="text/css" />         
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web:400,200,300,600,700,900&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css" />
    <!-- <link rel="stylesheet" type="text/css" media="screen" href="/editor/czjprev4.css" /> -->
    <link rel="stylesheet" href="/editor/lib/jquery-ui.css"></link>
    <script type="text/javascript" src="/editor/lib/jquery.min.js"></script>
    <script src="/editor/lib/flowplayer.min.js"></script>
    <script src="/editor/lib/jquery-ui.js">//</script>
    <script src='/editor/lib/js.cookie.js'>//</script>
    <link rel="stylesheet" href="/editor/lib/minimalist.css"/>
    <script type="text/javascript" src="/media/fancybox/jquery.fancybox.pack.js"></script>
    <link rel="stylesheet" href="/media/fancybox/jquery.fancybox.css" type="text/css" media="screen" />

<style type="text/css">
.czj-mainlemma {padding: 0 0 0 0; border-right: 1px solid #A4B0B4;}
.czj-vyznam {padding: 0px 30px 30px 0px; border-right: 1px solid #A4B0B4;}
.czj-gram-popis {border-right: 1px solid #A4B0B4;}
.czj-styl-popis {border-right: 1px solid #A4B0B4;}
#collocations {border-right: 1px solid #A4B0B4;}

</style>

    <script type="text/javascript" src="/editor/imagesloaded.pkgd.min.js">//</script>
    <script type="text/javascript" src="/editor/czjmain.js">//</script>
    <script><![CDATA[
      function change_trans(type) {
        if ($('.trans'+type).css('display')=='none') {
          $('.trans'+type).css('display', 'inline-block');
        } else {
          $('.trans'+type).css('display', 'none');
        }
      }

      function change_video(type) {
        switch(type) {
        case 'front':
          $('.topvideosign_front').show();
          $('.topvideosign_side').hide();
          $('.videoselect-front').addClass('videoselect-active');
          $('.videoselect-side').removeClass('videoselect-active');
          break;
        case 'side':
          $('.topvideosign_front').hide();
          $('.topvideosign_side').show();
          $('.videoselect-front').removeClass('videoselect-active');
          $('.videoselect-side').addClass('videoselect-active');
          break;
        }
      }

      function change_view(eid) {
        if (document.getElementById(eid).style.display=='none') {
          if (eid == 'lemma_parts_box') { 
            if (document.getElementById(eid).getAttribute('data-type') == 'coll') {
              document.getElementById('lemma_parts_info').innerHTML = 'hide links to components';
            } else {
              document.getElementById('lemma_parts_info').innerHTML = 'hide details';
            }
          }
          document.getElementById(eid).style.display='block';
          user_resize_sw();
        } else {
          if (eid == 'lemma_parts_box') { 
            if (document.getElementById(eid).getAttribute('data-type') == 'coll') {
              document.getElementById('lemma_parts_info').innerHTML = 'show links to components';
            } else {
              document.getElementById('lemma_parts_info').innerHTML = 'show details';
            }
          }
          document.getElementById(eid).style.display='none';
        }
      }

      function player_change(pid) {
        if (document.getElementById(pid).style.display=='none') {
          document.getElementById(pid).style.display='block'; 
        } else {
          document.getElementById(pid).style.display='none'; 
        } 
      }

      $(document).ready(function() {
        $(".flowmouse").bind("mouseenter mouseleave", function(e) {
          flowplayer(this)[/ee/.test(e.type) ? "resume":"pause"]();
        });
      });

      $(document).ready(function() {
        $('#navselect').change(function() {
          var option = $(this.options[this.selectedIndex]).val();
          $('#sidevideo').css('display', 'none');
          $('#sidesw').css('display', 'none');
          $('#sidehamnosys').css('display', 'none');
          $('#side'+option).css('display', 'block');
        });

        $('.topvideosign_side').hide();

        $("a.fancybox").fancybox({
        'autoScale'      : false,
        'transitionIn'   : 'elastic',
        'transitionOut'  : 'elastic',
        'titlePosition'  : 'inside',
        'hideOnContentClick' : true,
        'speedIn'        : 100,
        'speedOut'     : 100,
        'changeSpeed'    : 100,
        'centerOnScroll' : false,
        padding: 0,
        closeClick: true
        });
        $("a.hand").fancybox({
        'autoScale'      : false,
        'transitionIn'   : 'elastic',
        'transitionOut'  : 'elastic',
        'titlePosition'  : 'inside',
        'hideOnContentClick' : true,
        'speedIn'        : 100,
        'speedOut'     : 100,
        'changeSpeed'    : 100,
        'centerOnScroll' : false,
        padding: 0,
        closeClick: true
        });
      });
      $("a.fancybox_avatar").fancybox({
        'autoScale'      : false,
        'width'          : 612,
        'height'         : 640,
        'transitionIn'  : 'elastic',
        'transitionOut'   : 'elastic',
        'titlePosition'  : 'inside',
        'hideOnContentClick' : false,
        'speedIn'        : 100,
        'speedOut'    : 100,
        'changeSpeed'    : 100,
        'centerOnScroll' : false
        });
    $('#sw_search_link').fancybox({
      'autoScale'      : false,
      'transitionIn'   : 'elastic',
      'transitionOut'  : 'elastic',
      'titlePosition'  : 'inside',
      'hideOnContentClick' : true,
      'speedIn'        : 100,
      'speedOut'     : 100,
      'changeSpeed'    : 100,
      'centerOnScroll' : false,
      'height': 500,
      autoSize: false,
      padding: 0,
      closeClick: true,
      afterLoad: function(cur, prev) {
        cur.content[0].contentWindow.onload(prepare_swe());
      }
    });

      function open_iframe(id, dict) {
        var iframe = document.getElementById('colloc'+id);
        if (iframe.style.display=='none') {
          iframe.style.display='inline';
        } else {
          iframe.style.display='none';
        }
        if (iframe.src == 'about:blank') {
          if (dict == 'cs') {
            iframe.src = '/cs?action=getdoc&amp;id='+id+'&amp;tr=inline';
          } else { 
            iframe.src = '/czj?action=getdoc&amp;id='+id+'&amp;tr=inline';
          }
        }
      }
      function iframe_loaded(id) {
        var iframe = document.getElementById('colloc'+id);
        if (iframe.src != 'about:blank') {
          //alert(iframe.contentWindow.document.body.scrollHeight);
          iframe.height = iframe.contentWindow.document.body.scrollHeight + 'px;'
        }
      }

      $(function() {
        $("#search").tabs();
        $("#search-cs").tabs();
      });

      $(document).ready(function() {
        // set drag drop
        $('.swlink').draggable({
          overflow:'visible',containment:'document',appendTo: 'body', helper:'clone',
          start: function(event, ui) {
            $('#search').show();
          }
        });
        $('#search').droppable({
          activeClass: 'ui-state-hover',
          drop:function(event,ui){
            console.log(ui.draggable.attr('data-sw'));
            $('#swpreview').attr('src', 'http://znaky.zcu.cz/proxy/tts/signwriting.png?generator[sw]='+ui.draggable.attr('data-sw')+'&generator[align]=top_left&generator[set]=sw10');
            $('#searchsw').val(ui.draggable.attr('data-sw'));
            $("#select-tab-sw").click();
          }
          });
          apply_setting();
      });
      ]]></script>

    <style type="text/css">
      /* override jquery ui tabs*/
      #search, #search-cs {
        font-family: 'Titillium Web', sans-serif;
        font-size: 100%;
        border: 0;
      }
      .ui-tabs-nav {
        border: 0;
        background: white;
      }
      .ui-tabs .ui-tabs-nav li a {
        padding: 0.2em 0.5em;
        color: white;
      }
      .ui-tabs .ui-tabs-nav li {
        background: #a3caaa;
        margin: 0;
      }
      
      .ui-tabs-nav li.ui-tabs-active {
        background: #a3ca49;
      }
      .ui-tabs-nav li.ui-tabs-active a {
        font-weight: bold;
      }
      .ui-tabs .ui-tabs-panel {
        padding: 0.2em;
      }
      
      .flowplayer .fp-buffer{background-color: white; }
      .flowplayer .fp-progress{background-color: #03C9A9; }  
a.none {color: black; text-decoration: none;}           
    </style>
  </head>
  <body style="overflow: hidden;">
 
  
    <!---<xsl:apply-templates />-->
    <!--  <div class="czj-container">
        <div class="czj-inner-wrapper"> 
          <div class="czj-inner-right"> -->          
            <div class="czj-mainlemma">
              <div class="czj-mainlemma-inner">
                  <xsl:if test="lemma/lemma_type='collocation' and ($perm!='ro' or collocations/@status!='hidden')">             
                    <!-- <xsl:attribute name="style">background-color: lightgrey;</xsl:attribute> -->            
                  </xsl:if>           
                  <xsl:if test="$perm!='ro'">                         
                    <span style="float: right;">
               <xsl:if test="$perm!='ro'"><a href="/editor/?id={@id}&amp;lang=en">edit<img src="/editor/img/edit.png" /></a></xsl:if>
               <xsl:choose>
                 <xsl:when test="lemma/@auto_complete='1'"><a style="color:blue;cursor:pointer" onclick="window.location = 'http://www.dictio.info'+window.location.pathname+window.location.search"><img src="/editor/publikovano.png" border="0" title="switch to public view"/></a></xsl:when>
                  <xsl:otherwise><img src="/editor/nepublikovano.png" title="lemma not published"/> </xsl:otherwise>
                </xsl:choose>
                    </span>             
                    <span class="set-status">             ID: 
                      <xsl:value-of select="@id"/> | label: 
                      <xsl:for-each select="/entry/media/file[type='sign_side' or type='sign_front']/label/text()[generate-id() = generate-id(key('label',.)[1])]">
                        <xsl:value-of select="."/>
                        <xsl:if test="position()!=last()">, 
                        </xsl:if>
                      </xsl:for-each>
                    </span>               
                    <br/><span class="publish"> publishing: 
                <xsl:choose>
                   <xsl:when test="/entry/lemma/completeness='0'">automatic</xsl:when>
                   <xsl:when test="/entry/lemma/completeness='1'">hide all parts</xsl:when>                   
                   <xsl:when test="/entry/lemma/completeness='2'">non-empty parts only</xsl:when>
                   <xsl:when test="/entry/lemma/completeness='100'">approved parts only</xsl:when>
                </xsl:choose></span>
                  <br />                     
                  </xsl:if>           
                  <xsl:if test="lemma/lemma_type='collocation' and ($perm!='ro' or collocations/@status!='hidden')">               
                    <span id="lemma_parts">
                      <!-- <strong>Slovní spojení. </strong>-->
                      <a href="#" onclick="change_view('lemma_parts_box')" id="lemma_parts_info">Show links to components</a>
                      <br/>                 
                      <span id="lemma_parts_box" data-type="coll" style="display:none;">                   
                        <xsl:apply-templates select="collocations/colloc"/>                 
                      </span>               
                    </span>                
                    <div style="height: 1px; width: 100%; display: inline-block; background-color: white; margin-top: 5px; margin-bottom: 10px;">
                    </div>             
                  </xsl:if>
                  <!--zdroj čelní video-->                          
                  <xsl:if test="//lemma/video_front!=''">                 
                    <xsl:variable name="vf"><xsl:value-of select="//lemma/video_front"/>
                    </xsl:variable>                                       
                    <xsl:if test="//media/file[location=$vf]/id_meta_source!='' or //media/file[location=$vf]/id_meta_copyright!=''">
                      <span style="float: left;" class="source">Source:
                      <xsl:choose>
                        <xsl:when test="//media/file[location=$vf]/id_meta_source!=''"><xsl:value-of select="//media/file[location=$vf]/id_meta_source"/></xsl:when>                   
                        <xsl:when test="//media/file[location=$vf]/id_meta_source='' and //media/file[location=$vf]/id_meta_copyright='MU, Středisko Teiresiás'">MU, Středisko Teiresiás</xsl:when>                   
                        <xsl:when test="//media/file[location=$vf]/id_meta_source='' and //media/file[location=$vf]/id_meta_copyright='UP Olomouc'">LANGER, J. a kol. Znaková zásoba českého znakového jazyka ..</xsl:when>
                        <xsl:otherwise><xsl:value-of select="//media/file[location=$vf]/id_meta_copyright"/></xsl:otherwise>
                      </xsl:choose>                                        
                      </span><br />
                    </xsl:if>                         
                    <div class="author-info" style="display:none">                
                      <span class="author-author">author: 
                        <a href="/czj?action=page&amp;page=about#skupiny&amp;lang=en" target="_blank"><xsl:value-of select="//media/file[location=$vf]/id_meta_author"/></a>, 
                      </span>                
                      <span class="author-copyright">recorded by: 
                        <xsl:value-of select="//media/file[location=$vf]/id_meta_copyright"/>
                      </span>              
                    </div>            
                  </xsl:if>
                  <!--zdroj čelní video-->                                 
                  <div class="czj-mainlemma-video">           
                    <span class="videoblock">              
                      <div class="czj-mainlemma-videoblock">                                    
                        <!--<xsl:apply-templates select="media/file[type='sign_front' or type='sign_side']" mode="inlinetop"/>-->              
                        <xsl:if test="//lemma/video_front!=''">               
                          <xsl:apply-templates select="media/file[location=//lemma/video_front and ($perm!='ro' or status!='hidden')][1]" mode="inlinetop2">                 
                            <xsl:with-param name="video_type">sign_front</xsl:with-param>               
                          </xsl:apply-templates>             
                        </xsl:if>             
                        <xsl:if test="//lemma/video_front='' or not(//lemma/video_front)">               
                          <span class="topvideosign_front">                 
                            <img src="/media/img/emptyvideo.jpg"/>               
                          </span>             
                        </xsl:if>             
                        <xsl:if test="//lemma/video_side!=''">               
                          <xsl:apply-templates select="media/file[location=//lemma/video_side and ($perm!='ro' or status!='hidden')][1]" mode="inlinetop2">                 
                            <xsl:with-param name="video_type">sign_side</xsl:with-param>               
                          </xsl:apply-templates>             
                        </xsl:if>             
                        <xsl:if test="//lemma/video_side='' or not(//lemma/video_side)">               
                          <span class="topvideosign_side">                 
                            <img src="/media/img/emptyvideo.jpg"/>               
                          </span>             
                        </xsl:if>              
                      </div>              
                      <div class="czj-mainlemma-videoblock-buttons">                
                        <a href="#" style="text-decoration: none; color: black;">
                          <span onclick="change_video('front');" class="videoselect videoselect-front videoselect-active">Front view</span></a>                
                          <a href="#" style="text-decoration: none; color: black;">
                          <span class="videoselect videoselect-side" onclick="change_video('side')">Side view</span></a>              
                      </div>              
                      <!--dominance -->     
                      <xsl:if test="//lemma/video_front!=''">                 
                        <xsl:variable name="vf">
                          <xsl:value-of select="//lemma/video_front"/>
                        </xsl:variable>                 
                        <xsl:choose>                   
                          <xsl:when test="//media/file[location=$vf]/orient='pr'">
                            <span style="color: #03C9A9;; position: relative;  top: -235px; left: +45px; z-index: 10;"> R</span>
                          </xsl:when>                   
                          <xsl:when test="//media/file[location=$vf]/orient='lr'">
                            <span style="color: #03C9A9;; position: relative;  top: -235px; left: -210px; z-index: 10;"> L</span>
                          </xsl:when>                 
                        </xsl:choose>               
                      </xsl:if>              
                      <div class="cleaner"></div>           
                    </span>               <!-- videoblock -->                                                
                </div>
                <xsl:if test="lemma/swmix/sw and ($perm!='ro' or lemma/@swstatus!='hidden' or (lemma/lemma_type!='single'))">
                  <div class="czj-mainlemma-sw">
                    <!-- <div class="czj-mainlemma-sw-text">SignWriting</div> -->
                    <div class="czj-mainlemma-sw-images">
                      <div class="czj-mainlemma-sw-images-inner">
                        <xsl:apply-templates select="lemma/swmix/sw"/>
                      </div>
                    </div>
                  </div>
                </xsl:if> 
                
                 <xsl:if test="lemma/hamnosys[$perm='ro' and @status!='hidden'] and lemma/hamnosys!=''">
                <div id="hamnava">                  
                   <span id="avatar" class="transavatar">
                     <!--3D avatar:<br/>-->
                     <a data-fancybox-type="iframe" class="fancybox_avatar iframe" href="http://znaky.zcu.cz/avatar/show?unicode_hamnosys={lemma/hamnosys}">
                       <img src="/media/avatar.png" alt="click to play avatar"/>
                     </a>
                   </span>
                   </div>
               <div style="clear:both"></div>
                 </xsl:if>
                
                 <!--schvalovani-->
                <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_lemmaczj')">
               <span class="set-status">
                 <br /><br />typ hesla: 
                 <xsl:choose>
                   <xsl:when test="/entry/lemma/lemma_type='single'">single</xsl:when>
                   <xsl:when test="/entry/lemma/lemma_type='derivat'">derivative</xsl:when>
                   <xsl:when test="/entry/lemma/lemma_type='kompozitum'">compositum</xsl:when>
                   <xsl:when test="/entry/lemma/lemma_type='collocation'">collocation</xsl:when>
                 </xsl:choose>
                 <xsl:call-template name="status_publish">
                   <xsl:with-param name="status" select="/entry/lemma/status"/>
                   <xsl:with-param name="type">entry</xsl:with-param>
                 </xsl:call-template>
               </span>
               <br/><br/>
             </xsl:if>
              <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_video')">
                <span class="set-status">front record:                   
                  <xsl:if test="//file[location=//lemma/video_front and status='published']">approved</xsl:if>
                  <xsl:if test="not(//file[location=//lemma/video_front and status='published'])">
                 <xsl:call-template name="status_publish">
                  <xsl:with-param name="status" select="//file[location=//lemma/video_front]/status"/>
                  <xsl:with-param name="type">video_front</xsl:with-param>
                 </xsl:call-template>
                </xsl:if>
               </span>
               <br/>
             </xsl:if>
              <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_video')">
                <span class="set-status">side record:                   
                  <xsl:if test="//file[location=//lemma/video_side and status='published']">approved</xsl:if>
                  <xsl:if test="not(//file[location=//lemma/video_side and status='published'])">
                 <xsl:call-template name="status_publish">
                   <xsl:with-param name="status" select="//file[location=//lemma/video_side]/status"/>
                   <xsl:with-param name="type">video_side</xsl:with-param>
                 </xsl:call-template>
              </xsl:if>
               </span>
               <br/>
             </xsl:if>
             <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_transkripce')">
               <span class="set-status">
                 SW: 
                 <xsl:call-template name="status_publish">
                   <xsl:with-param name="status" select="/entry/lemma/@swstatus"/>
                   <xsl:with-param name="type">sw</xsl:with-param>
                 </xsl:call-template>
               </span>
               <br/>
             </xsl:if>
              <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_transkripce')">
               <span class="set-status">
                 HNS:  
                 <xsl:call-template name="status_publish">
                   <xsl:with-param name="status" select="/entry/lemma/hamnosys/@status"/>
                   <xsl:with-param name="type">hns</xsl:with-param>
                 </xsl:call-template>
               </span>
               <br/>
             </xsl:if>  
            <!--schvalovani-->

            <xsl:if test="lemma/hamnosys[$perm!='ro' or @status!='hidden'] and lemma/hamnosys!=''">
                  <div id="hamnosys" class="transhamn" style="width: 660px; height: 35px; float: left; background-color: white; margin-top: 20px; padding: 10px 10px 0px 10px;">
                      <xsl:if test="count(../sw)>1"></xsl:if>                     
                      <a class="fancybox" href="http://znaky.zcu.cz/proxy/tts/tex2img.png?generator[template]=hamnosys&amp;generator[dpi]=400&amp;generator[engine]=x&amp;generator[tex]={lemma/hamnosys}">                       
                        <img src="http://znaky.zcu.cz/proxy/tts/tex2img.png?generator[template]=hamnosys&amp;generator[dpi]=200&amp;generator[engine]=x&amp;generator[tex]={lemma/hamnosys}"/></a>                   
                  </div>                   
                    <xsl:if test="count(lemma/sw)>1"><br/></xsl:if>              
                  </xsl:if>              
              </div>    <!-- czj-mainlemma-inner -->            
            </div> 
              <!-- czj-mainlemma -->                         
            <xsl:if test="(lemma/grammar_note and (lemma/grammar_note/@status!='hidden' or $perm!='ro')) or ((lemma/lemma_type='derivat' or lemma/lemma_type='kompozitum') and ($perm!='ro' or collocations/@status!='hidden'))">
            <div class="czj-gram-popis" style="padding-left: 0px;">
              <img src="/editor/img/grey-corner.png" style="position: relative; top: 0px; left: -45px;" valign="top"/>
              <!--<div class="czj-lemma-h1">Gramatický popis</div>-->
              <xsl:if test="lemma/grammar_note/@source!='' or lemma/grammar_note/@copyright!=''">
                <span style="float: right;" class="source">Source:
                  <xsl:choose>
                    <xsl:when test="lemma/grammar_note/@source!=''"><xsl:value-of select="lemma/grammar_note/@source"/></xsl:when>                   
                    <xsl:otherwise><xsl:value-of select="//lemma/grammar_note/@copyright"/></xsl:otherwise>                   
                  </xsl:choose>                                        
                </span>
              </xsl:if>                        
              <div class="author-info" style="display:none">                
                <span class="author-author">Author: 
                  <a href="/czj?action=page&amp;page=about#skupiny&amp;lang=en" target="_blank"><xsl:value-of select="lemma/grammar_note/@author"/></a>, 
                </span>                
                <span class="author-copyright">Recorded by: 
                  <xsl:value-of select="lemma/grammar_note/@copyright"/>
                </span>              
              </div>                
              <!--schvalování-->               
              <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_lingvist')">                 
                 <span class="set-status">gram. desc.:                     
                   <xsl:call-template name="status_publish">
                     <xsl:with-param name="status" select="/entry/lemma/grammar_note[1]/@status"/>
                     <xsl:with-param name="type">gram</xsl:with-param>
                   </xsl:call-template>
                 </span>
              </xsl:if><!--schvalování konec-->
              <br />
               <xsl:if test="$perm!='ro' or lemma/grammar_note[1]/@status!='hidden'">                                       
                 <xsl:if test="lemma/grammar_note/@oral_komp!=''">
                   <b>Mouth gesture: </b> [<xsl:value-of select="lemma/grammar_note/@oral_komp"/>] 
                   <xsl:choose>
                     <!-- <xsl:when test="lemma/grammar_note/@oral_komp_sel='nezadano'"> <em>nezadáno</em></xsl:when>-->
                     <xsl:when test="lemma/grammar_note/@oral_komp_sel='povinny'"> -<em> compulsory</em></xsl:when>
                     <xsl:when test="lemma/grammar_note/@oral_komp_sel='nepovinny'"> - <em>optional</em></xsl:when>
                   </xsl:choose>
                   <xsl:if test="lemma/grammar_note/@mluv_komp!=''">, </xsl:if>
                 </xsl:if>
                 <xsl:if test="lemma/grammar_note/@mluv_komp!=''">
                   <b>Mouthing: </b> [<xsl:value-of select="lemma/grammar_note/@mluv_komp"/>]
                   <xsl:choose>
                     <!--<xsl:when test="lemma/grammar_note/@mluv_komp_sel='nezadano'"> - <em>nezadáno</em></xsl:when>-->
                     <xsl:when test="lemma/grammar_note/@mluv_komp_sel='povinny'"> - <em>compulsory</em></xsl:when>
                     <xsl:when test="lemma/grammar_note/@mluv_komp_sel='nepovinny'"> - <em>optional</em></xsl:when>
                   </xsl:choose>
                 </xsl:if>
                 <xsl:if test="lemma/grammar_note/@mluv_komp!='' or lemma/grammar_note/@oral_komp!=''"><br/> </xsl:if>
                 <xsl:if test="lemma/grammar_note!=''">
                   <xsl:apply-templates mode="grammar_note" select="lemma/grammar_note"/>
                 </xsl:if>
               </xsl:if>
               <xsl:if test="lemma/grammar_note/file"><br/></xsl:if>
               <xsl:for-each select="lemma/grammar_note/file">
                 <xsl:variable name="mid">
                    <xsl:value-of select="@media_id"/>
                  </xsl:variable>
                  <xsl:apply-templates select="//media/file[(@id=$mid or id=$mid) and main_for_entry and main_for_entry/@completeness!='1']" mode="var">
                  <xsl:with-param name="in_grammar">true</xsl:with-param>
                  <xsl:with-param name="in_note">G<xsl:value-of select="position()"/></xsl:with-param>
                   </xsl:apply-templates>
                   <xsl:apply-templates select="//media/file[(@id=$mid or id=$mid) and (not(main_for_entry) or main_for_entry/@completeness='1')]">
                     <xsl:with-param name="in_grammar">true</xsl:with-param>
                     <xsl:with-param name="in_note">G<xsl:value-of select="position()"/></xsl:with-param>
                   </xsl:apply-templates>
                  </xsl:for-each>
               <div class="cleaner"></div>
               <xsl:if test="lemma/grammar_note/variant">
                 <div class="czj-lemma-h2">Gramatical variants</div>
                 <div class="czj-relation-wrapper">
                      <xsl:apply-templates select="lemma/grammar_note/variant" mode="file">
                        <xsl:with-param name="gram_var_count">0</xsl:with-param>
                      </xsl:apply-templates>
                 </div>
               </xsl:if>
               <xsl:if test="(lemma/lemma_type='derivat' or lemma/lemma_type='kompozitum') and ($perm!='ro' or collocations/@status!='hidden')">
                 <div id="lemma_parts">
                   <xsl:if test="lemma/grammar_note"><br/></xsl:if>
                   <strong>etymology: </strong><a style="cursor:pointer;color:blue" url="#" onclick="change_view('lemma_parts_box')" id="lemma_parts_info">show details</a><br/>
                   <span id="lemma_parts_box" style="display:none">
                   <div class="czj-relation-wrapper">
                     <xsl:apply-templates select="collocations/colloc"/>               
                   </div>
                   </span>
                   
                 </div>
               </xsl:if>
              
            </div> <!-- czj-gram-popis -->
            </xsl:if>
            <xsl:if test="(lemma/style_note/file or lemma/style_note/variant or lemma/grammar_note/@region!='' or lemma/style_note/@kategorie!='' or lemma/style_note/@generace!='' or lemma/style_note/text()!='') and (lemma/style_note/@status!='hidden' or $perm!='ro')">            
            <div class="czj-styl-popis" style="padding-left: 0px;">
             <!--<img src="/editor/img/grey-corner.png" style="position: relative; top: 0px; left: -45px;" valign="top"/>-->                
             <xsl:if test="lemma/style_note/@source!='' or lemma/style_note/@copyright!=''">
               <span style="float: right;" class="source">Source:
                 <xsl:choose>
                   <xsl:when test="lemma/style_note/@source!=''"><xsl:value-of select="lemma/style_note/@source"/></xsl:when>                   
                   <xsl:otherwise><xsl:value-of select="lemma/style_note/@copyright"/></xsl:otherwise>                   
                 </xsl:choose>                                        
               </span>
             </xsl:if>              
              <!--<div class="czj-lemma-h1">Stylistický popis</div>-->
              <div class="author-info" style="display:none">              
                  <span class="author-author">Author: <a href="/czj?action=page&amp;page=about#skupiny&amp;lang=en" target="_blank"><xsl:value-of select="lemma/style_note/@author"/></a>, </span>              
                  <span class="author-copyright">Recorded by: <xsl:value-of select="lemma/style_note/@copyright"/></span>            
            </div>               
            <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_lingvist')">                 
              <span class="set-status">style desc.: 
                   <xsl:call-template name="status_publish">
                     <xsl:with-param name="status" select="/entry/lemma/style_note[1]/@status"/>
                     <xsl:with-param name="type">style</xsl:with-param>
                   </xsl:call-template>
               </span>
            </xsl:if>
         <xsl:if test="lemma/style_note/file or lemma/style_note/variant or lemma/grammar_note/@region!='' or lemma/style_note/@kategorie!='' or lemma/style_note/@generace!='' or lemma/style_note/text()!=''">
           <br/>
           <div id="styletop">
             <div class="grammar">
               <xsl:if test="$perm!='ro' or lemma/style_note/@status!='hidden'">
                 <xsl:if test="lemma/style_note/@kategorie != ''">
                    <div>
                      <xsl:value-of select="lemma/style_note/@kategorie"/>
                    </div>
                 </xsl:if>
                 <xsl:if test="lemma/grammar_note/@region != ''">
                     <div><b>Distribution area: </b> 
                        <img src="/media/img/region/cr.png" class="oblast"/>
                        <xsl:call-template name="split_region">
                          <xsl:with-param name="text" select="lemma/grammar_note/@region"/>
                        </xsl:call-template>
                     </div>                 
                </xsl:if>                                   
                          <xsl:if test="lemma/style_note/@generace != ''">
                            <div><b>Distribution age: </b> 
                              <xsl:call-template name="split_generace">
                                <xsl:with-param name="text" select="lemma/style_note/@generace"/>
                              </xsl:call-template>
                            </div>                 
                          </xsl:if>                 
                          <xsl:if test="lemma/style_note/@gender != ''">                    
                            <div>
                              <xsl:value-of select="lemma/style_note/@gender"/>
                            </div>                 
                          </xsl:if>                                 
                        </xsl:if>                   
                        <xsl:apply-templates mode="style_note" select="lemma/style_note"/>               
                        <xsl:if test="lemma/style_note/file"><br/>
                        </xsl:if>               
                        <xsl:for-each select="lemma/style_note/file">                 
                          <xsl:variable name="mid">
                            <xsl:value-of select="@media_id"/>
                          </xsl:variable>                   
                          <xsl:apply-templates select="//media/file[(@id=$mid or id=$mid) and main_for_entry and main_for_entry/@completeness!='1']" mode="var">                     
                            <xsl:with-param name="in_grammar">true</xsl:with-param>                     
                            <xsl:with-param name="in_note">S<xsl:value-of select="position()"/></xsl:with-param>                   
                          </xsl:apply-templates>                   
                          <xsl:apply-templates select="//media/file[(@id=$mid or id=$mid) and (not(main_for_entry) or main_for_entry/@completeness='1')]">                     
                            <xsl:with-param name="in_grammar">true</xsl:with-param>                     
                            <xsl:with-param name="in_note">S<xsl:value-of select="position()"/></xsl:with-param>                   
                          </xsl:apply-templates>               
                        </xsl:for-each>               
                        <div class="cleaner"></div>               
                        <xsl:if test="lemma/style_note/variant">                 
                          <div class="czj-lemma-h2">Stylistické varianty</div>                 
                          <div class="czj-relation-wrapper">                 
                            <xsl:apply-templates select="lemma/style_note/variant" mode="file">
                              <xsl:with-param name="gram_var_count" select="count(lemma/grammar_note/variant)"/>
                            </xsl:apply-templates>
                          </div>               
                        </xsl:if>             
                      </div>           
                    </div>         
                  </xsl:if>            
                </div> 
                <!-- czj-styl-popis -->          
              </xsl:if>                         
              <div class="czj-vyznam">              
                <!-- <img src="/editor/img/grey-corner.png" style="position: relative; top: 0px; left: -45px;" valign="top"/> -->         
                <div id="meanings">           
                  <xsl:apply-templates select="meanings/meaning[$perm!='ro' or status!='hidden']">             
                    <xsl:sort select="@number" data-type="number"/>           
                  </xsl:apply-templates>           
                  <div style="clear:both"></div>         
                </div>            
              </div> 
              <!-- czj-vyznam -->                         
              <div id="collocations">
                <!--slovni spojeni-->           
                <xsl:if test="collocations/revcolloc[@lemma_type='collocation' and ($perm!='ro' or @auto_complete='1')]">             
                  <!--<span class="divtitle">Slovní spojení s tímto heslem</span>-->                 
                  <img src="/editor/img/grey-corner.png" style="position: relative; top: 0px; left: -45px;" valign="top"/>              
                  <div class="czj-lemma-h1">Collocation with this lemma </div>             
                  <div class="collocations">               
                    <xsl:apply-templates select="collocations/revcolloc[@lemma_type='collocation' and ($perm!='ro' or @auto_complete='1')]"/>             
                  </div>             
                  <div style="clear:both">
                  </div>           
                </xsl:if>         
              </div>         
              <input type="button" value="zobrazit/skrýt autorské info" id="show-author" onclick="$('.author-info').toggle();"/>            
            <!--</div>--> 
            <!-- czj-right -->        
          <!--</div>--> 
          <!-- czj-wrapper -->        
          <div class="cleaner">
          </div>      
        <!--</div>--> 
        <!-- czj-container -->                                 
     <!-- Bootstrap core JavaScript -->
     <!-- Placed at the end of the document so the pages load faster -->
     <script src="/editor/lib/bootstrap.min.js"></script>
   </body>
 </html>
</xsl:template>

<xsl:template match="file" mode="grammar_note">
  <em>see video G<xsl:value-of select="count(preceding-sibling::file)+1"/></em>
</xsl:template>
<xsl:template match="file" mode="style_note">
  <em>see video S<xsl:value-of select="count(preceding-sibling::file)+1"/></em>
</xsl:template>

<xsl:template match="sw">
  <xsl:if test=".!=''">
    <span class="sw" style="vertical-align:top; display:inline-block;">
        <div class="author-info" style="display:none"><!-- autorské položky -->      
          <xsl:if test="@source!=''"><span class="author-source">Source: <xsl:value-of select="@source"/>; </span></xsl:if>      
          <span class="author-author">Author: <a href="/czj?action=page&amp;page=about#skupiny&amp;lang=en" target="_blank"><xsl:value-of select="@author"/></a>; </span>      
          <span class="author-copyright">Recorded by: <xsl:value-of select="@copyright"/></span>    
        </div>    
    <a href="http://znaky.zcu.cz/proxy/tts/signwriting.png?generator[sw]={.}&amp;generator[align]=top_left&amp;generator[set]=sw10" class="fancybox swlink ui-draggable" data-sw="{.}">
      <img src="http://znaky.zcu.cz/proxy/tts/signwriting.png?generator[sw]={.}&amp;generator[align]=top_left&amp;generator[set]=sw10" alt="signwriting" class="signwriting" ratio="0.8"/>
    </a>
    </span>
  </xsl:if>
</xsl:template>
<xsl:template match="sw" mode="rel">
  <xsl:if test=".!=''">
    <a href="http://znaky.zcu.cz/proxy/tts/signwriting.png?generator[sw]={.}&amp;generator[align]=top_left&amp;generator[set]=sw10" class="fancybox swlink ui-draggable" data-sw="{.}">
      <img src="http://znaky.zcu.cz/proxy/tts/signwriting.png?generator[sw]={.}&amp;generator[align]=top_left&amp;generator[set]=sw10" alt="signwriting" class="signwriting" ratio="0.5" style=""/>
    </a>
  </xsl:if>
</xsl:template>

<xsl:template match="variant" mode="style_note">
</xsl:template>

<xsl:template match="variant" mode="grammar_note">
</xsl:template>

<xsl:template match="variant" mode="file">
     <xsl:param name="gram_var_count"/>
        <xsl:variable name="f">
          <xsl:value-of select="."/>
        </xsl:variable>  
    <xsl:apply-templates select="//media/file[(@id=$f) and (main_for_entry and (main_for_entry/@completeness!='1' or $perm!='ro'))]" mode="var">
      <xsl:with-param name="variant_sw" select="string-length(substring-before('ABCDEFGHIJKLMNOPQRSTUVWXYZ', @sw))+1"/> 
      <xsl:with-param name="variant_pos" select="position()+$gram_var_count"/> 
      <!--zadava se jako A, B, C, potrebuju prepocitat na cislo-->  
    </xsl:apply-templates>  
    <xsl:apply-templates select="//media/file[(@id=$f) and (not(main_for_entry) or main_for_entry/@completeness='1')]">    
      <xsl:with-param name="parent_id" select="position()"/>    
      <xsl:with-param name="variant_sw" select="string-length(substring-before('ABCDEFGHIJKLMNOPQRSTUVWXYZ', @sw))+1"/> 
      <xsl:with-param name="variant_pos" select="position()+$gram_var_count"/> 
      <!--zadava se jako A, B, C, potrebuju prepocitat na cislo-->  
    </xsl:apply-templates>
  <xsl:if test="@desc!=''"><span class="source">note: <xsl:value-of select="@desc"/></span><!-- desc varianta --></xsl:if>          
  </xsl:template>
  
  <xsl:template match="file" mode="inline"><!--video význam -->  
    <xsl:param name="media_id"/>  
    <xsl:if test="(id=$media_id or @id=$media_id) and $media_id!=''">    
      <xsl:if test="$perm!='ro'">      
        <span class="publish"><xsl:value-of select="location"/></span>
        <br/>    
      </xsl:if>    
        
      <xsl:if test="substring(location, string-length(location)-3, 4) = '.flv'">
         <span id="flowvideoin{@id}" data-ratio="0.8" class="flowplayer meaningfile" style="width:285px; background:#777 url(/media/videoczj/thumb/{location}/thumb.jpg) no-repeat; background-size: 285px 228px">
           <video>
              <source type="video/flash" src="/media/videoczj/{location}"/>
            </video>
          </span>  
        </xsl:if>
        <xsl:if test="substring(location, string-length(location)-3, 4) = '.mp4'">
            <span id="flowvideoin{@id}" data-ratio="0.8" class="meaningfile" style="width:285px;">
              <video controls="" width="285px" height="228px" poster="/media/videoczj/thumb/{location}/thumb.jpg">        
                <source type="video/mp4" src="/media/videoczj/{location}"/>
              </video>
           </span>                                                  
       </xsl:if>
                            
      <xsl:choose>
        <xsl:when test="location='Synonymum-odkaz.mp4'"><span id="video-odkaz" style="color: white; position: relative;  top: -20px; left: -190px; z-index: 10; ">see synonym</span></xsl:when>
            <xsl:when test="location='Varianty-odkaz.mp4'"><span id="video-odkaz" style="color: white; position: relative;  top: -20px; left: -190px; z-index: 10; ">see variant 1</span></xsl:when>
      </xsl:choose>
       <br />    
        <div style="float: left; width: 286px;" class="source">
          <xsl:if test="id_meta_source!='' or id_meta_author!=''">
              <span class="source">
              <xsl:choose>
                 <xsl:when test="id_meta_source!=''">Source: <xsl:value-of select="id_meta_source"/></xsl:when>                   
                 <xsl:otherwise>Author: <a href="/czj?action=page&amp;page=about#skupiny&amp;lang=en"><xsl:value-of select="id_meta_author"/></a>;</xsl:otherwise>                   
              </xsl:choose>                                        
              <xsl:text> </xsl:text></span>              
         </xsl:if>
         
         <div class="author-info" style="display:none">      
            <xsl:if test="id_meta_source!=''">
            <span class="author-author">Author: <a href="/czj?action=page&amp;page=about#skupiny&amp;lang=en"><xsl:value-of select="id_meta_author"/></a>; </span>
            </xsl:if> 
            <xsl:if test="id_meta_copyright!=''">     
            <span class="author-copyright">Recorded by: <xsl:value-of select="id_meta_copyright"/></span> 
            </xsl:if>   
        </div> 
      </div>     
  </xsl:if>
</xsl:template>
<!--<xsl:template match="file" mode="inlinetop">
  <span id="flowvideoin{@id}" data-ratio="0.8" class="flowplayer meaningfile topvideo{type}" style="width:285px; background:#777 url(/media/videoczj/thumb/{location}/thumb.jpg) no-repeat; background-size: 285px 228px">
    <video >
      <source type="video/flash" src="/media/videoczj/{location}"/>
      </video>
    </span>
</xsl:template>-->
 <xsl:template match="file" mode="inlinetop2">  
    <xsl:param name="video_type"/>  
    <xsl:if test="substring(location, string-length(location)-3, 4) = '.flv'">
      <span id="flowvideoin{@id}" data-ratio="0.8" class="flowplayer meaningfile topvideo{$video_type}" style="width:285px; background:#777 url(/media/videoczj/thumb/{location}/thumb.jpg) no-repeat; background-size: 285px 228px">      
        <video > 
          <source type="video/flash" src="/media/videoczj/{location}"/>         
        </video>    
      </span>  
    </xsl:if>  
    <xsl:if test="substring(location, string-length(location)-3, 4) = '.mp4'">
      <span id="flowvideoin{@id}" data-ratio="0.8" class="meaningfile topvideo{$video_type}" style="width:285px;">      
        <video controls="" width="285px" height="228px" poster="/media/videoczj/thumb/{location}/thumb.jpg">        
         <source type="video/mp4" src="/media/videoczj/{location}"/>      
        </video>      
      </span>  
    </xsl:if>  
    <xsl:if test="not(substring(location, string-length(location)-3, 4) = '.flv' or substring(location, string-length(location)-3, 4) = '.mp4')">
      <span  class="meaningfile topvideo{$video_type}" style="width:285px; background:#000 url(/media/videoczj/{location}) no-repeat; background-size: 285px 228px">      
        <img style="width:285px;height:228px" src="/media/videoczj/{location}"/>    
      </span>  
    </xsl:if>
  </xsl:template>

<!--<xsl:template match="file" mode="side">
    <div id="flowvideo{id}" data-ratio="0.8" class="flowplayer flowmouse" style="width:120px; background:#777 url(/media/videoczj/thumb/{location}/thumb.jpg) no-repeat; background-size: 120px 96px">
    <video loop="loop">
      <source type="video/flash" src="/media/videoczj/{location}"/>
      </video>
    </div>
</xsl:template>-->

 <xsl:template match="file" mode="rel">        
          <xsl:if test="substring(location, string-length(location)-3, 4) = '.flv'">
            <div id="flowvideorel{id}" data-ratio="0.8" class="flowplayer flowmouse" style="width:120px; background:#777 url(/media/videoczj/thumb/{location}/thumb.jpg) no-repeat; background-size: 120px 96px">    
              <video loop="loop">    
                <source type="video/flash" src="/media/videoczj/{location}"/>
              </video>
            </div>        
          </xsl:if>      
          <xsl:if test="substring(location, string-length(location)-3, 4) = '.mp4'">
            <div id="flowvideorel{id}" data-ratio="0.8" class="flowmouse" style="width:120px; background: url(/media/videoczj/thumb/{location}/thumb.jpg) no-repeat; background-size: 120px 96px; float:left;">    
                <video loop="loop" width="120px" height="96px" poster="/media/videoczj/{location}/thumb.jpg" onmouseover="this.play()" onmouseout="this.pause()">
                  <source type="video/mp4" src="/media/videoczj/{location}"/>
                </video>
           </div>          
          </xsl:if>
      
<script type="text/javascript">   
    $("#flowvideorel<xsl:value-of select="id"/>").on("click", function(e) {
    window.location='/czj?action=search&amp;getdoc=<xsl:value-of select="../@lemma_id"/>';
    });
  </script>
  </xsl:template>
  <xsl:template match="file" mode="var">  
    <xsl:param name="var_id"/>    
    <xsl:param name="in_grammar"/>    
    <xsl:param name="in_note"/>    
    <xsl:param name="variant_sw"/>  
    <xsl:param name="variant_pos"/>  
    <div class="czj-relation-item">    
      <div class="czj-relation relationvideo">      
        <div class="czj-lemma-preview-sm">          
          <div class="czj-lemma-preview-sm-header">                                
            <xsl:if test="$in_note!=''">              
              <div class="alignleft">
                <xsl:value-of select="$in_note"/>
              </div>            
            </xsl:if>            
            <xsl:if test="$in_note=''">              
              <div class="alignleft">V<xsl:value-of select="$variant_pos"/>  
                <xsl:if test="$perm!='ro' or $skupina_test='true'">
                  <span class="set-status">
                    <xsl:value-of select="main_for_entry/@lemma_id"/>
                  </span> 
                </xsl:if>                
              </div>            
            </xsl:if>            
            <div class="alignright">  
            <xsl:if test="@auto_complete='1' or @completeness='2' or @completeness='100'">
              <a href="/czj?action=search&amp;getdoc={main_for_entry/@lemma_id}&amp;lang=en">                
                <img src="/media/img/slovnik-open_w.png" height="20"/></a>
            </xsl:if>                
            </div>          
          </div>      
        </div>
        <div style="clear:both">
        </div>  
        <xsl:if test="substring(location, string-length(location)-3, 4) = '.flv'">
          <div id="flowvideovar{@id}" data-ratio="0.8" class="flowplayer flowmouse" style="width:120px; background:#000 url(/media/videoczj/thumb/{location}/thumb.jpg) no-repeat; background-size: 120px 96px">    
            <video loop="loop">  
              <source type="video/flash" src="/media/videoczj/{location}"/>
            </video>
          </div>
        </xsl:if>
        <xsl:if test="substring(location, string-length(location)-3, 4) = '.mp4'">
          <div id="flowvideovar{@id}" data-ratio="0.8" class="flowmouse" style="width:120px; height:96px background: url(/media/videoczj/thumb/{location}/thumb.jpg) no-repeat; background-size: 120px 96px; float: left; background-color: black;">    
            <video width="120px" height="96px" loop="loop" poster="/media/videoczj/thumb/{location}/thumb.jpg" onmouseover="this.play()" onmouseout="this.pause()">
              <source type="video/mp4" src="/media/videoczj/{location}"/>
            </video>
          </div>  
        </xsl:if>
            
        <div class="czj-lemma-preview-sm-sw">     
          <span class="sw transsw" style="vertical-align: top; display: inline-block; height: 96px;">    
            <xsl:if test="($variant_sw/sw)">
              <xsl:apply-templates select="$variant_sw/sw" mode="rel"/>
            </xsl:if>  
            <xsl:if test="not($variant_sw/sw)">
              <xsl:if test="main_for_entry[1]/@lemma_type='single' or not(main_for_entry[1]/@lemma_type)">        
                <xsl:if test="main_for_entry[1]/swmix/sw[@primary='true']">          
                  <xsl:apply-templates select="main_for_entry[1]/swmix/sw[@primary='true']" mode="rel"/>        
                </xsl:if>        
                <xsl:if test="not(main_for_entry[1]/swmix/sw[@primary='true'])">          
                  <xsl:apply-templates select="main_for_entry[1]/swmix/sw[1]" mode="rel"/>        
                </xsl:if>      
              </xsl:if>      
              <xsl:if test="main_for_entry[1]/@lemma_type!='single'">        
                <xsl:apply-templates select="main_for_entry[1]/swmix/sw" mode="rel"/>      
              </xsl:if>  
            </xsl:if>  
          </span>  
        </div>          
<script type="text/javascript">
    $("#flowvideovar<xsl:value-of select="id"/>").on("click", function(e) {
    window.location='/czj?action=search&amp;getdoc=<xsl:value-of select="main_for_entry/@lemma_id"/>';
      });
  </script>    
      </div>  
    </div>
  </xsl:template>

<xsl:template match="file" mode="colloc">           
        <xsl:if test="substring(location, string-length(location)-3, 4) = '.flv'">
         <div id="flowvideocol{id}" data-ratio="0.8" class="flowplayer flowmouse" style="width:120px; background:#000 url(/media/videoczj/thumb/{location}/thumb.jpg) no-repeat; background-size: 120px 96px">    
            <video loop="loop">
              <source type="video/flash" src="/media/videoczj/{location}"/>
            </video>
         </div>     
        </xsl:if>
        <xsl:if test="substring(location, string-length(location)-3, 4) = '.mp4'">
         <div id="flowvideocol{id}" data-ratio="0.8" class="flowmouse" style="width:120px; background:#000 url(/media/videoczj/thumb/{location}/thumb.jpg) no-repeat; background-size: 120px 96px; float: left;">    
            <video loop="loop" width="120px" height="96px" poster="/media/videoczj/thumb/{location}/thumb.jpg" onmouseover="this.play()" onmouseout="this.pause()">
               <source type="video/mp4" src="/media/videoczj/{location}"/>
            </video>
         </div>     
        </xsl:if>        
<script type="text/javascript">
    $("#flowvideocol<xsl:value-of select="id"/>").on("click", function(e) {
      open_iframe('<xsl:value-of select="../@lemma_id"/>', 'czj');
    });
  </script>
  </xsl:template>

 <xsl:template match="file">  
    <xsl:param name="parent_id"/>  
    <xsl:param name="variant_sw"/>  
    <xsl:param name="variant_pos"/>  
    <xsl:param name="in_grammar"/>    
    <xsl:param name="in_note"/>    
    <div class="czj-relation-item">  
      <xsl:if test="not($variant_sw)">
        <xsl:attribute name="style">background-color: grey;>
        </xsl:attribute>
      </xsl:if>      
      <div class="czj-relation relationvideo">        
        <div class="czj-lemma-preview-sm">          
          <div class="czj-lemma-preview-sm-header" style="background-color: none;">            
            <xsl:if test="$in_note!=''">              
              <div class="alignleft">
                <xsl:value-of select="$in_note"/>
              </div>            
            </xsl:if>            
            <xsl:if test="$in_note=''">              
              <div class="alignleft">V<xsl:value-of select="$variant_pos"/>              
              </div>            
            </xsl:if>            
            <div class="alignright">
            </div>          
          </div>        
        </div>  
           <xsl:if test="substring(location, string-length(location)-3, 4) = '.flv'">
            <div id="flowvideo{@id}-{$parent_id}" data-ratio="0.8" class="flowplayer flowmouse" style="width:120px; background:#777 url(/media/videoczj/thumb/{location}/thumb.jpg) no-repeat; background-size: 120px 96px">    
                <video loop="loop">
                  <source type="video/flash" src="/media/videoczj/{location}"/>
                </video>
             </div>     
            </xsl:if>

            <xsl:if test="substring(location, string-length(location)-3, 4) = '.mp4'">
             <div id="flowvideovar{@id}" data-ratio="0.8" class="flowmouse" style="width:120px; height:96px background: url(/media/videoczj/thumb/{location}/thumb.jpg) no-repeat; background-size: 120px 96px; float: left; background-color: black;">    
                <video width="120px" height="96px" loop="loop" poster="/media/videoczj/thumb/{location}/thumb.jpg" onmouseover="this.play()" onmouseout="this.pause()">
                  <source type="video/mp4" src="/media/videoczj/{location}"/>
                </video>
              </div>                
            </xsl:if>
            
        <xsl:if test="$variant_sw">  
          <div class="czj-lemma-preview-sm-sw">      
            <span class="sw transsw" style="vertical-align: top; display: inline-block; height: 96px;">    
              <xsl:apply-templates select="$variant_sw/sw" mode="rel"/>    
            </span>    
          </div>  
        </xsl:if>  
        <xsl:if test="main_for_entry/sw and not($variant_sw)">    
          <span class="sw transsw" style="vertical-align: top; display: inline-block; height: 96px;">    
            <!--<span class="sw transsw" style="vertical-align:top; display:inline-block;">-->      
            <xsl:if test="main_for_entry/sw[@primary='true']">        
              <xsl:apply-templates select="main_for_entry/sw[@primary='true']" mode="rel"/>      
            </xsl:if>      
            <xsl:if test="not(main_for_entry/sw[@primary='true'])">        
              <xsl:apply-templates select="main_for_entry/sw[1]" mode="rel"/>      
            </xsl:if>    
          </span>  
        </xsl:if>  
      </div>
    </div>  
<script type="text/javascript">
  <xsl:variable name="jstype">
    <xsl:if test="contains(location,'.flv')">flash</xsl:if>
    <xsl:if test="contains(location,'.mp4')">mp4</xsl:if>
  </xsl:variable>
    $("#flowvideo<xsl:value-of select="@id"/>-<xsl:value-of select="$parent_id"/>").on("click", function(e) {
      flowplayer(this).pause();
      var container = $('<div/>');
      console.log(container);
      container.css('background', '#777 url(/media/videoczj/thumb/<xsl:value-of select="location"/>/thumb.jpg) no-repeat');
      container.css('background-size', '285px 228px');
      var video_type = ('/media/videoczj/<xsl:value-of select="location"/>'.substr(-3, 3) == 'mp4')? 'mp4':'flash';
      container.flowplayer({
        autoPlay: true,
        autoBuffering: true,
        width: 285,                             
        height: 228,
        ratio: 0.8,
        playlist: [
        [{'<xsl:value-of select="$jstype"/>': '/media/videoczj/<xsl:value-of select="location"/>'}]
        ],
      });
      $.fancybox({
        content: container,
        width: 285,
        height: 228,
        scrolling: 'no',
        autoSize: false
      });
      flowplayer(container[0]).load([{'<xsl:value-of select="$jstype"/>': '/media/videoczj/<xsl:value-of select="location"/>'}]);
      flowplayer(container[0]).resume();
    });
  </script>
</xsl:template>
  
  <xsl:template match="file" mode="usage"><!-- příklady užití -->  
    <xsl:param name="parent_id"/>  
    <xsl:param name="variant_sw"/>  
    <div class="usage">                  
      <xsl:if test="substring(location, string-length(location)-3, 4) = '.flv'">
        <div id="flowvideo{@id}-{$parent_id}" data-ratio="0.8" class="flowplayer flowmouse usage" style="width:120px; background:#777 url(/media/videoczj/thumb/{location}/thumb.jpg) no-repeat; background-size: 120px 96px;">    
          <video loop="loop">      
            <source type="video/flash" src="/media/videoczj/{location}"/>
          </video>            
        </div><br />
      </xsl:if>
      <xsl:if test="substring(location, string-length(location)-3, 4) = '.mp4'">
        <div id="flowvideo{@id}-{$parent_id}" data-ratio="0.8" class="usage" style="width:120px;">
          <video controls="" width="120px" height="96px" poster="/media/videoczj/thumb/{location}/thumb.jpg" onmouseover="this.play()" onmouseout="this.pause()">        
            <source type="video/mp4" src="/media/videoczj/{location}"/>      
          </video>      
        </div>  
      </xsl:if>  
        <xsl:choose>
            <xsl:when test="location='Synonymum-odkaz.mp4'"><span id="video-odkaz" style="color: white; position: relative;  top: -5px; left: -122px; z-index: 10; ">see synonym</span></xsl:when>
            <xsl:when test="location='Varianty-odkaz.mp4'"><span id="video-odkaz" style="color: white; position: relative;  top: -5px; left: -112px; z-index: 10; ">see variant 1</span></xsl:when>            
        </xsl:choose>
        <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_lemmaczj')"><div class="publish"><xsl:value-of select="location"/></div></xsl:if>                                      
      <xsl:if test="id_meta_source!='' or id_meta_author!=''">
         <span style="float: left;" class="source"> 
         <xsl:choose>
            <xsl:when test="id_meta_source!=''">Source: <xsl:value-of select="id_meta_source"/></xsl:when>                   
            <xsl:otherwise>Autor: <a href="/czj?action=page&amp;page=about#skupiny"><xsl:value-of select="id_meta_author"/></a></xsl:otherwise>                   
         </xsl:choose>                                        
         </span>
      </xsl:if>
        <div class="author-info" style="display:none">   
          <xsl:if test="id_meta_source!=''">   
          <span class="author-author">Author: 
            <a href="/czj?action=page&amp;page=about#skupiny"><xsl:value-of select="id_meta_author"/></a>; 
          </span>
          </xsl:if>
          <xsl:if test="id_meta_copyright!=''">                  
            <span class="author-copyright">Recorded by: <br /><xsl:value-of select="id_meta_copyright"/></span>
          </xsl:if>    
        </div>    
    </div>  
    <xsl:if test="substring(location, string-length(location)-3, 4) = '.mp4'">
      <script type="text/javascript">
        $("#flowvideo<xsl:value-of select="@id"/>-<xsl:value-of select="$parent_id"/>").on("click", function(e) {
          var container = $('<div data-ratio="0.8" style="width:285px;">
            <video controls="" width="285px" height="228px" poster="/media/videoczj/thumb/{location}/thumb.jpg" autoplay="">
            <source type="video/mp4" src="/media/videoczj/{location}"/>
            </video>
            </div>');
          $.fancybox({
            content: container,
            width: 285,
            height: 228,
            scrolling: 'no',
            autoSize: false
          });
        });
      </script>
    </xsl:if>
    <xsl:if test="substring(location, string-length(location)-3, 4) = '.flv'">
      <script type="text/javascript">
      $("#flowvideo<xsl:value-of select="@id"/>-<xsl:value-of select="$parent_id"/>").on("click", function(e) {
        flowplayer(this).pause();
        var container = $('<div/>');
        console.log(container);
        container.css('background', '#777 url(/media/videoczj/thumb/<xsl:value-of select="location"/>/thumb.jpg) no-repeat');
        container.css('background-size', '285px 228px');
        container.flowplayer({
          autoPlay: true,
          autoBuffering: true,
          width: 285,                             
          height: 228,
          ratio: 0.8,
          playlist: [
          [{'flash': '/media/videoczj/<xsl:value-of select="location"/>'}]
          ],
        });
        $.fancybox({
          content: container,
          width: 285,
          height: 228,
          scrolling: 'no',
          autoSize: false
        });
        flowplayer(container[0]).load([{'flash': '/media/videoczj/<xsl:value-of select="location"/>'}]);
        flowplayer(container[0]).resume();
        });
      </script>
    </xsl:if>
  </xsl:template>

<xsl:template match="meaning">
  <img src="/editor/img/grey-corner.png" style="position: relative; top: 0px; left: -45px;" valign="top"/>  
  <div class="meaningtop">
    <div class="czj-lemma-h1">Meaning 
        <xsl:if test="count(//meanings/meaning[status='published'])>1">
          <xsl:value-of select="position()"/>
        </xsl:if>
        <xsl:if test="@source!='' or @copyright!=''"><span style="float: right;" class="source">Source: <xsl:value-of select="@source"/> 
          <xsl:if test="@source='' and @copyright!=''"><xsl:value-of select="@copyright"/></xsl:if>         
          </span><br />
        </xsl:if>
        <div class="source" style="display:none; float: right;">      
          <xsl:if test="@author!=''"><span class="author-author">Author: <a href="/czj?action=page&amp;page=about#skupiny&amp;lang=en" target="_blank"><xsl:value-of select="@author"/></a>; </span></xsl:if>      
          <xsl:if test="@copyright!=''"><span class="author-copyright">Recorded by:<xsl:value-of select="@copyright"/></span></xsl:if>    
        </div>          
    </div>    
    <div id="meaning{@id}" class="meaning"> 
  <xsl:if test="category!=''">
        <span class="category"><b>Semantic field: </b>
          <xsl:choose>
            <xsl:when test="category=6">informatics</xsl:when>
            <xsl:when test="category=14">business</xsl:when>
            <xsl:when test="category=28">biology</xsl:when>
            <xsl:when test="category=27">mathematics</xsl:when>
            <xsl:when test="category='anat'">anatomy</xsl:when>
            <xsl:when test="category='antr'">anthropology</xsl:when>
            <xsl:when test="category='archeol'">archeology</xsl:when>
            <xsl:when test="category='archit'">architecture</xsl:when>
            <xsl:when test="category='biol'">biology</xsl:when>
            <xsl:when test="category='bot'">botanics</xsl:when>
            <xsl:when test="category='dipl'">diplomatics</xsl:when>
            <xsl:when test="category='div'">theatre</xsl:when>
            <xsl:when test="category='dopr'">transport</xsl:when>
            <xsl:when test="category='ekol'">ecology</xsl:when>
            <xsl:when test="category='ekon'">economics</xsl:when>
            <xsl:when test="category='eltech'">electrotechnics</xsl:when>
            <xsl:when test="category='etn'">ethnography</xsl:when>
            <xsl:when test="category='feud'">feudalism</xsl:when>
            <xsl:when test="category='filat'">philately</xsl:when>
            <xsl:when test="category='film'">film</xsl:when>
            <xsl:when test="category='filoz'">philosophy</xsl:when>
            <xsl:when test="category='fot'">photography</xsl:when>
            <xsl:when test="category='fyz'">physics</xsl:when>
            <xsl:when test="category='fyziol'">physiology</xsl:when>
            <xsl:when test="category='geol'">geology</xsl:when>
            <xsl:when test="category='geom'">geometry</xsl:when>
            <xsl:when test="category='gnoz'">gnoseology</xsl:when>
            <xsl:when test="category='hist'">history</xsl:when>
            <xsl:when test="category='horn'">mining</xsl:when>
            <xsl:when test="category='horol'">mountaineering</xsl:when>
            <xsl:when test="category='hosp'">industry</xsl:when>
            <xsl:when test="category='hud'">musicology</xsl:when>
            <xsl:when test="category='hut'">metallurgy</xsl:when>
            <xsl:when test="category='hvězd'">astronomy</xsl:when>
            <xsl:when test="category='chem'">chemistry</xsl:when>
            <xsl:when test="category='ideal'">idealism</xsl:when>
            <xsl:when test="category='jad'">nuclear physics</xsl:when>
            <xsl:when test="category='jaz'">philology</xsl:when>
            <xsl:when test="category='kapit'">capitalism</xsl:when>
            <xsl:when test="category='karet'">card expression</xsl:when>
            <xsl:when test="category='katol církvi'">catholicism</xsl:when>
            <xsl:when test="category='krim'">criminology</xsl:when>
            <xsl:when test="category='křesť'">christianity</xsl:when>
            <xsl:when test="category='kuch'">cookery</xsl:when>
            <xsl:when test="category='kult'">culture</xsl:when>
            <xsl:when test="category='kyb'">cybernetics</xsl:when>
            <xsl:when test="category='lék'">medicine</xsl:when>
            <xsl:when test="category='lékár'">pharmaceutics</xsl:when>
            <xsl:when test="category='let'">aviation</xsl:when>
            <xsl:when test="category='liter'">literary science</xsl:when>
            <xsl:when test="category='log'">logic</xsl:when>
            <xsl:when test="category='marx'">marxism</xsl:when>
            <xsl:when test="category='mat'">mathematics</xsl:when>
            <xsl:when test="category='meteor'">meteorology</xsl:when>
            <xsl:when test="category='miner'">mineralogy</xsl:when>
            <xsl:when test="category='motor'">motorism</xsl:when>
            <xsl:when test="category='mysl'">hunting expression</xsl:when>
            <xsl:when test="category='mytol'">mythology</xsl:when>
            <xsl:when test="category='náb'">religion</xsl:when>
            <xsl:when test="category='nár'">ethnography</xsl:when>
            <xsl:when test="category='obch'">trade</xsl:when>
            <xsl:when test="category='pedag'">pedagogy</xsl:when>
            <xsl:when test="category='peněž'">banking</xsl:when>
            <xsl:when test="category='polit'">politics</xsl:when>
            <xsl:when test="category='polygr'">polygraphy</xsl:when>
            <xsl:when test="category='pošt'">postal operations</xsl:when>
            <xsl:when test="category='potrav'">food industry</xsl:when>
            <xsl:when test="category='práv'">law, jurisprudence</xsl:when>
            <xsl:when test="category='prům'">industry</xsl:when>
            <xsl:when test="category='přír'">nature</xsl:when>
            <xsl:when test="category='psych'">psychology</xsl:when>
            <xsl:when test="category='rybn'">fish farming</xsl:when>
            <xsl:when test="category='řem'">craft</xsl:when>
            <xsl:when test="category='sklář'">glassblowing</xsl:when>
            <xsl:when test="category='soc'">socialism</xsl:when>
            <xsl:when test="category='sociol'">sociology</xsl:when>
            <xsl:when test="category='stat'">statistics</xsl:when>
            <xsl:when test="category='stav'">civil engineering</xsl:when>
            <xsl:when test="category='škol'">education</xsl:when>
            <xsl:when test="category='tech'">technology</xsl:when>
            <xsl:when test="category='těl'">physical education</xsl:when>
            <xsl:when test="category='text'">textile industry</xsl:when>
            <xsl:when test="category='úč'">accountancy</xsl:when>
            <xsl:when test="category='úř'">official expression</xsl:when>
            <xsl:when test="category='veř spr'">public administration</xsl:when>
            <xsl:when test="category='vet'">veterinary</xsl:when>
            <xsl:when test="category='voj'">military</xsl:when>
            <xsl:when test="category='výp tech'">computer technology</xsl:when>
            <xsl:when test="category='výr'">production</xsl:when>
            <xsl:when test="category='výtv'">design</xsl:when>
            <xsl:when test="category='zahr'">horticulture</xsl:when>
            <xsl:when test="category='zbož'">merchandise</xsl:when>
            <xsl:when test="category='zeměd'">agriculture</xsl:when>
            <xsl:when test="category='zeměp'">geography</xsl:when>
            <xsl:when test="category='zool'">zoology</xsl:when>
            <xsl:when test="category='cirkev'">religious expression</xsl:when>
          </xsl:choose>
        </span><br />
      </xsl:if>
      <xsl:if test="@style_region != '' or @style_kategorie != '' or @style_generace != ''">
        <br/>
      </xsl:if>
      <xsl:if test="@style_region != ''">
        <xsl:call-template name="split_region">
          <xsl:with-param name="text" select="@style_region"/>
        </xsl:call-template>      
      </xsl:if>      
      <xsl:if test="@style_kategorie != ''">       , 
        <xsl:value-of select="@style_kategorie"/>      
      </xsl:if>      
      <xsl:if test="@style_generace != ''">       , 
        <xsl:call-template name="split_generace">
          <xsl:with-param name="text" select="@style_generace"/>
        </xsl:call-template>      
      </xsl:if>           
      <span class="videoblock">
      <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_lingvist')">
        <span class="set-status"> 
          <xsl:call-template name="status_publish">
            <xsl:with-param name="status" select="status"/>
            <xsl:with-param name="type">meaning</xsl:with-param>
            <xsl:with-param name="element" select="."/>
          </xsl:call-template>
        </span>       
      </xsl:if>
                  
        <xsl:if test="not(text//file)">
          <br/><img src="/media/img/emptyvideo.jpg" width="285px"/>
        </xsl:if>
        <xsl:for-each select="text//file[@media_id!='']">
          <xsl:variable name="mid" select="@media_id"/>
          <xsl:apply-templates select="//media/file[id=$mid or @id=$mid][1]" mode="inline">
            <xsl:with-param name="media_id"><xsl:value-of select="$mid"/></xsl:with-param>
          </xsl:apply-templates>
        </xsl:for-each>
        <xsl:if test="text!=''">
          <xsl:value-of select="text"/>
        </xsl:if>
    </span>

      <div style="clear:both"/>
      <xsl:if test="usages/usage/text!=''">
        <div class="usages">
          <div class="czj-lemma-h2">Examples of use</div>
          <xsl:apply-templates select="usages/usage[(status!='hidden' or $perm!='ro')]"/>
        </div>
      </xsl:if>

            <xsl:if test="relation[@type!='translation' and ($perm!='ro' or (@status!='hidden' and  (@auto_complete='1' or @completeness='2' or @completeness='100')))] ">
  <div class="synanto">
          <div class="cleaner"></div>
          <div class="czj-lemma-h2">Relations</div>
          <div class="czj-relation-wrapper">
                       <xsl:apply-templates select="relation[(@type='hyperonym' or @type='hyponym' or @type='synonym' or @type='antonym' or @type='synonym_strategie') and ($perm!='ro' or (@status!='hidden' and  (@auto_complete='1' or @completeness='2' or @completeness='100')))]"/>
  </div>
        </div>
      </xsl:if>
      <div style="clear:both"/>  
    
     <xsl:if test="relation[@type='translation']">
        <span class="relations">
               <div class="cleaner"></div>          
            <div class="czj-lemma-h2">Translation</div>          
            <div class="czj-relation-wrapper">          
              <xsl:apply-templates select="relation[@type='translation' and ((@status!='hidden') or $perm!='ro')]"/>        
            </div>        
          </span>      
        </xsl:if><br />             
        <div style="clear:both"></div>    
      </div>  
    </div>
  </xsl:template>

  <xsl:template match="relation">  
    <xsl:if test="@type='translation'">    
      <xsl:variable name="lemma">
        <xsl:value-of select="@lemma_id"/>
      </xsl:variable>    
      <span class="relation-entry dict-{@target}"> 
	<span style="color: gray">
	    <xsl:choose>
                 <xsl:when test="@target='cs'">CZ: </xsl:when>          
                 <xsl:when test="@target='en'">EN: </xsl:when>              
                 <xsl:otherwise>?</xsl:otherwise>
              </xsl:choose>
	    </span>             
        <xsl:if test="@title_only!='true'">
          <xsl:if test="count(preceding-sibling::relation[@lemma_id=$lemma and @type='translation'])=0">          
            <xsl:if test="@auto_complete='1' or $perm!='ro'">
              <a href="/cs?action=search&amp;getdoc={@lemma_id}&amp;lang=en" target="_top">
                <xsl:value-of select="title"/></a>
            </xsl:if>          
            <xsl:if test="@auto_complete!='1' and $perm='ro'"><xsl:value-of select="title"/></xsl:if>          
            <xsl:if test="(@meaning_nr!='' and @meaning_count > 1) and (@auto_complete='1' or $perm!='ro')"> (ve významu
              <xsl:value-of select="@meaning_nr"/>              
              <!--schvalování-->              
              <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_lingvist')">                
                <span class="set-status">                    
                  <xsl:call-template name="status_publish">                    
                    <xsl:with-param name="status" select="@status"/>                    
                    <xsl:with-param name="type">relation</xsl:with-param>                    
                    <xsl:with-param name="element" select="."/>                  
                  </xsl:call-template>                
                </span>              
              </xsl:if>
              <!--konec schvalování-->                            
              <xsl:if test="count(following-sibling::relation[@lemma_id=$lemma and @type='translation']) &gt; 0">              
                <xsl:for-each select="following-sibling::relation[@lemma_id=$lemma and @type='translation']">  
                <xsl:sort select="@meaning_nr" data-type="number"/>              
                  <xsl:text>, </xsl:text>
                  <xsl:value-of select="@meaning_nr"/>                
                  <!--schvalování-->                
                  <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_lingvist')">                  
                    <span class="set-status">                      
                      <xsl:call-template name="status_publish">                      
                        <xsl:with-param name="status" select="@status"/>                      
                        <xsl:with-param name="type">relation</xsl:with-param>                      
                        <xsl:with-param name="element" select="."/>                    
                      </xsl:call-template>                  
                    </span>                
                  </xsl:if>
                  <!--konec schvalování-->                 
              </xsl:for-each>
            </xsl:if>)
          </xsl:if>
          <xsl:if test="@meaning_nr='' or @meaning_count = 1">
            <!--schvalování-->
            <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_lingvist')">
              <span class="set-status"> 
                <xsl:call-template name="status_publish">
                  <xsl:with-param name="status" select="@status"/>
                  <xsl:with-param name="type">relation</xsl:with-param>
                  <xsl:with-param name="element" select="."/>
                </xsl:call-template>
              </span>
              </xsl:if><!--konec schvalování-->            
            </xsl:if>          
            <!--<xsl:if test="position()!=last()">, </xsl:if>-->          
            <xsl:if test="position()!=last()">, </xsl:if>        
          </xsl:if>      
        </xsl:if>      
        <xsl:if test="@title_only='true'">        
          <xsl:value-of select="title"/>        
          <!--schvalování-->    
          <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_lingvist')">        
            <span class="set-status">            
              <xsl:call-template name="status_publish">            
                <xsl:with-param name="status" select="@status"/>            
                <xsl:with-param name="type">relation</xsl:with-param>            
                <xsl:with-param name="element" select="."/>          
              </xsl:call-template>        
            </span>      
          </xsl:if>
          <!--konec schvalování-->                  
          <xsl:if test="position()!=last()">, </xsl:if>      
      </xsl:if>
    </span>
  </xsl:if>
  <xsl:if test="@type='synonym' or @type='antonym' or @type='revcolloc' or @type='hyponym' or @type='hyperonym' or @type='synonym_strategie'">
     <div class="czj-relation-item">      
        <div class="czj-relation relationvideo">        
          <div class="czj-lemma-preview-sm">          
            <div class="czj-lemma-preview-sm-header">            
              <div class="alignleft" style="white-space: nowrap; overflow: hidden; width: 165px;">                   
                <xsl:choose>          
                  <xsl:when test="@type='synonym'">synonym
                  </xsl:when>          
                  <xsl:when test="@type='synonym_strategie'">syn. strategy
                  </xsl:when>          
                  <xsl:when test="@type='antonym'">antonym
                  </xsl:when>          
                  <xsl:when test="@type='hyponym'">hyponym
                  </xsl:when>          
                  <xsl:when test="@type='hyperonym'">hyperonym
                  </xsl:when>          
                  <xsl:when test="@type='revcolloc'">
                  </xsl:when>          
                  <xsl:otherwise>
                    <xsl:value-of select="@type"/>
                  </xsl:otherwise>            
                </xsl:choose>        
                <xsl:if test="@meaning_nr!='' and @meaning_count > 1"> (meaning 
                  <xsl:value-of select="@meaning_nr"/>)
                </xsl:if>
                <!--schvalování-->        
                <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_lingvist')">          
                  <span class="set-status">               
                    <xsl:call-template name="status_publish">              
                      <xsl:with-param name="status" select="@status"/>              
                      <xsl:with-param name="type">relation</xsl:with-param>              
                      <xsl:with-param name="element" select="."/>            
                    </xsl:call-template>          
                  </span>        
                </xsl:if>
                <!--schvalování konec-->          
              </div>          
              <div class="alignright">            
                <a href="/czj?action=search&amp;getdoc={@lemma_id}&amp;lang=en" target="_top">              
                  <img src="/media/img/slovnik-open_w.png" height="20"/>              
                  <!--<xsl:value-of select="@lemma_id"/>-->            </a>          
              </div>          
            </div>         
          </div>    
          <div style="clear:both">
          </div>    
        <xsl:if test="not(file)">
          <img src="/media/img/emptyvideo.jpg" height="96" width="120"/>
        </xsl:if>
        <xsl:apply-templates select="file" mode="rel"/>
          <div class="czj-lemma-preview-sm-sw">
            <span class="sw transsw" style="vertical-align: top; display: inline-block; height: 96px;">
              <!-- <span class="sw transsw" style="vertical-align:top; display:inline-block;"> -->
              <xsl:apply-templates select="swmix/sw" mode="rel"/>
            </span>
          </div>
          <br />
            <span class="" style="vertical-align:top; display:none;">
              <xsl:value-of select="translation"/>
            </span>
        </div> 
        <!-- czj-relation relationvideo -->      
      </div>  
    </xsl:if>
  </xsl:template>
  <xsl:template match="usage"> 
    <!--příklady užití objekt-->  
    <xsl:variable name="ppp" select="current()"/>  
    <div class="relation-entry" style="float: left">  
      <!--schvalování-->  
      <xsl:if test="contains($perm, 'admin') or contains($perm, 'revizor_lingvist')">      
        <span class="set-status">          
          <xsl:call-template name="status_publish">          
            <xsl:with-param name="status" select="status"/>          
            <xsl:with-param name="type">usage</xsl:with-param>          
            <xsl:with-param name="element" select="."/>        
          </xsl:call-template>      
        </span>      
        <br/>    
      </xsl:if>
      <!--schvalování konec-->    
      <xsl:for-each select="text//file">      
        <xsl:variable name="mid">
          <xsl:value-of select="@media_id"/>
        </xsl:variable>      
      <xsl:apply-templates select="//media/file[id=$mid or @id=$mid]" mode="usage">
        <xsl:with-param name="parent_id" select="generate-id($ppp)"/>
      </xsl:apply-templates>
    </xsl:for-each>
    <xsl:if test="text!=''">
      <xsl:value-of select="text"/>
      </xsl:if>
  </div>
</xsl:template>

<xsl:template match="colloc">
<xsl:if test="$perm!='ro' or @auto_complete='1'">
<div class="czj-relation-item">
  <span class="czj-relation relationvideo">
    <div class="czj-lemma-preview-sm">
      <div class="czj-lemma-preview-sm-header">
            <div class="alignleft"> 
              <xsl:if test="$perm!='ro'">
                <span class="set-status">
                  <xsl:value-of select="@lemma_id"/>
                </span> 
              </xsl:if>
            </div>             
            <div class="alignright">
              <a href="/czj?action=search&amp;getdoc={@lemma_id}&amp;lang=en" target="_top">
                <img src="/media/img/slovnik-open_w.png" height="20"/></a>
            </div>        
      </div>
    </div>  
      <xsl:if test="not(file)">
        <img src="/media/img/emptyvideo.jpg" height="96" width="120"/>
      </xsl:if>
    
    <xsl:apply-templates select="file" mode="rel"/>
    <div class="czj-lemma-preview-sm-sw">
      <span class="sw transsw" style="vertical-align: top; display: inline-block; height: 96px;">
      <!--<span class="sw transsw" style="vertical-align:top; display:inline-block;">-->
        <xsl:apply-templates select="swmix/sw" mode="rel"/>
     </span>
    </div>    
    <br />
    <span class="" style="vertical-align:top; display:none;">      
      <xsl:value-of select="translation"/>
    </span>
  </span>
  </div>
 </xsl:if>
</xsl:template>

<xsl:template match="revcolloc">
  <xsl:if test="@completeness!='1'">
    <br/>
    <div style="background-color: #D7E1E4;">
    <span class="relation-entry" >   
      <xsl:apply-templates select="file" mode="colloc"/>
      <span class="sw transsw" style="vertical-align: top; display: inline-block; height: 96px;">
      <!--<span class="sw transsw" style="vertical-align:top; display:inline-block;">-->
        <xsl:apply-templates select="swmix/sw" mode="rel"/>
      </span>
      <span class="" style="vertical-align:top; display:inline-block;">
        <!-- <xsl:value-of select="translation"/> -->
      </span>
       <span style="vertical-align:top; display:inline-block; float: right;">
        <span onclick="open_iframe('{@lemma_id}','czj')" style="cursor:pointer;color:blue;text-decoration:underline">
          <!--<xsl:value-of select="@lemma_id"/>-->
          <img src="/media/img/slovnik-expand.png" height="20"/>
        </span>
      </span> 
        <div style="clear:both; margin-bottom: 5px;">
        </div>    
      </span>    
      <iframe class="colloc-iframe" id="colloc{@lemma_id}" src="about:blank" style="display:none" onload="iframe_loaded('{@lemma_id}')">
      </iframe>    
    </div>  
  </xsl:if>   
</xsl:template>

<xsl:template name="split_misto">
    <xsl:param name="text"/>
    <xsl:variable name="first" select='substring-before($text,";")'/>
    <xsl:variable name='rest' select='substring-after($text,";")'/>
    <xsl:if test='$first'>
      <xsl:choose>
     <xsl:when test="$first='neutral'">neutral space</xsl:when>
        <xsl:when test="$first='hlava'">head</xsl:when>
        <xsl:when test="$first='oblicej'">- face (neutr. space in front of face)</xsl:when>
        <xsl:when test="$first='temeno'">- top of the head (above the head)</xsl:when>
        <xsl:when test="$first='celo'">- forehead</xsl:when>
        <xsl:when test="$first='oci'">- eyes</xsl:when>
        <xsl:when test="$first='nos'">- nose</xsl:when>
        <xsl:when test="$first='usi'">- ears</xsl:when>
        <xsl:when test="$first='tvare'">- cheeks</xsl:when>
        <xsl:when test="$first='usta'">- mouth</xsl:when>
        <xsl:when test="$first='brada'">- chin</xsl:when>
        <xsl:when test="$first='krk'">- neck</xsl:when>
        <xsl:when test="$first='hrud'">upper half of torso (chest)</xsl:when>
        <xsl:when test="$first='paze'">arm</xsl:when>
        <xsl:when test="$first='ruka'">hand</xsl:when>
        <xsl:when test="$first='pas'">lower half of torso (abdomen, waist)</xsl:when>
        <xsl:when test="$first='dolni'">lower body</xsl:when>
      </xsl:choose>
      <xsl:if test="$rest">, </xsl:if>
    </xsl:if>

    <xsl:if test='$rest'>
      <xsl:call-template name='split_misto'>
        <xsl:with-param name='text' select='$rest'/>
      </xsl:call-template>
    </xsl:if>
    <xsl:if test='not($rest)'>
      <xsl:choose>
        <xsl:when test="$text='neutral'">neutral space</xsl:when>
        <xsl:when test="$text='hlava'">head</xsl:when>
        <xsl:when test="$text='oblicej'">- face (neutr. space in front of face)</xsl:when>
        <xsl:when test="$text='temeno'">- top of the head (above the head)</xsl:when>
        <xsl:when test="$text='celo'">- forehead</xsl:when>
        <xsl:when test="$text='oci'">- eyes</xsl:when>
        <xsl:when test="$text='nos'">- nose</xsl:when>
        <xsl:when test="$text='usi'">- ears</xsl:when>
        <xsl:when test="$text='tvare'">- cheeks</xsl:when>
        <xsl:when test="$text='usta'">- mouth</xsl:when>
        <xsl:when test="$text='brada'">- chin</xsl:when>
        <xsl:when test="$text='krk'">- neck</xsl:when>
        <xsl:when test="$text='hrud'">upper half of torso (chest)</xsl:when>
        <xsl:when test="$text='paze'">arm</xsl:when>
        <xsl:when test="$text='ruka'">hand</xsl:when>
        <xsl:when test="$text='pas'">lower half of torso (abdomen, waist)</xsl:when>
        <xsl:when test="$text='dolni'">lower body</xsl:when>
      </xsl:choose>
    </xsl:if>
  </xsl:template>

  <xsl:template name="split_region">
    <xsl:param name="text"/>
    <xsl:variable name="first" select='substring-before($text,";")'/>
    <xsl:variable name='rest' select='substring-after($text,";")'/>
    <xsl:if test='$first'>
         <xsl:choose>        
        <xsl:when test="$first='cr'">Entire Czech Republic
          <img src="/media/img/region/celaCR.png" class="oblast"/>
        </xsl:when>        
        <xsl:when test="$first='praha'">Prague and surroundings
          <img src="/media/img/region/pha.png" class="oblast"/>
        </xsl:when>        
        <xsl:when test="$first='plzen'">Plzeň and surroundings
          <img src="/media/img/region/plzen.png" class="oblast"/>
        </xsl:when>
        <xsl:when test="$first='brno'">Brno and surroundings
          <img src="/media/img/region/brno.png" class="oblast"/>
        </xsl:when>        
        <xsl:when test="$first='vm'">Valašské Meziříčí and surroundings
          <img src="/media/img/region/valmez.png" class="oblast"/>
        </xsl:when>        
        <xsl:when test="$first='hk'">Hradec Králové and surroundings
          <img src="/media/img/region/hk.png" class="oblast"/>
        </xsl:when>        
        <xsl:when test="$first='kr'">Kroměříž and surroundings
          <img src="/media/img/region/krom.png" class="oblast"/>
        </xsl:when>        
        <xsl:when test="$first='cechy'">Bohemia
          <img src="/media/img/region/cechy.png" class="oblast"/>
        </xsl:when>        
        <xsl:when test="$first='morava'">Moravia
          <img src="/media/img/region/morava.png" class="oblast"/>
        </xsl:when>        
        <xsl:when test="$first='jih'">Jihlava and surroundings
          <img src="/media/img/region/jihlava.png" class="oblast"/>
        </xsl:when>        
        <xsl:when test="$first='zl'">Zlín and surroundings
          <img src="/media/img/region/zlin.png" class="oblast"/>
        </xsl:when>        
        <xsl:when test="$first='cb'">České Budějovice and surroundings
          <img src="/media/img/region/cb.png" class="oblast"/>
        </xsl:when>        
        <xsl:when test="$first='ot'">Ostrava and surroundings
          <img src="/media/img/region/ostrava.png" class="oblast"/>
        </xsl:when>        
        <xsl:when test="$first='ol'">Olomouc and surroundings
          <img src="/media/img/region/ol.png" class="oblast"/>
        </xsl:when>        
        <xsl:when test="$first='ul'">Ústí nad Labem and surroundings
          <img src="/media/img/region/usti.png" class="oblast"/>
        </xsl:when>        
        <xsl:when test="$first='lib'">Liberec a okolí
          <img src="/media/img/region/lbc.png" class="oblast"/>
        </xsl:when>        
        <xsl:when test="$first='slovensko'">Slovakia
          <img src="/media/img/region/sk.png" class="oblast"/>
        </xsl:when>      
      </xsl:choose>      
      <xsl:if test="$rest">, 
      </xsl:if>    
    </xsl:if>    
    <xsl:if test='$rest'>      
      <xsl:call-template name='split_region'>        
        <xsl:with-param name='text' select='$rest'/>      
      </xsl:call-template>    
    </xsl:if>    
    <xsl:if test='not($rest)'>      
      <xsl:choose>     
        <xsl:when test="$text='cr'">Entire Czech Republic
          <img src="/media/img/region/celaCR.png" class="oblast"/>
        </xsl:when>        
        <xsl:when test="$text='praha'">Prague and surroundings
          <img src="/media/img/region/pha.png" class="oblast"/>
        </xsl:when>        
        <xsl:when test="$text='plzen'">Plzeň and surroundings
          <img src="/media/img/region/plzen.png" class="oblast"/>
        </xsl:when>
        <xsl:when test="$text='brno'">Brno and surroundings
          <img src="/media/img/region/brno.png" class="oblast"/>
        </xsl:when>        
        <xsl:when test="$text='vm'">Valašské Meziříčí and surroundings
          <img src="/media/img/region/valmez.png" class="oblast"/>
        </xsl:when>        
        <xsl:when test="$text='hk'">Hradec Králové and surroundings
          <img src="/media/img/region/hk.png" class="oblast"/>
        </xsl:when>        
        <xsl:when test="$text='kr'">Kroměříž and surroundings
          <img src="/media/img/region/krom.png" class="oblast"/>
        </xsl:when>        
        <xsl:when test="$text='cechy'">Bohemia
          <img src="/media/img/region/cechy.png" class="oblast"/>
        </xsl:when>        
        <xsl:when test="$text='morava'">Moravia
          <img src="/media/img/region/morava.png" class="oblast"/>
        </xsl:when>        
        <xsl:when test="$text='jih'">Jihlava and surroundings
          <img src="/media/img/region/jihlava.png" class="oblast"/>
        </xsl:when>        
        <xsl:when test="$text='zl'">Zlín and surroundings
          <img src="/media/img/region/zlin.png" class="oblast"/>
        </xsl:when>        
        <xsl:when test="$text='cb'">České Budějovice and surroundings
          <img src="/media/img/region/cb.png" class="oblast"/>
        </xsl:when>        
        <xsl:when test="$text='ot'">Ostrava and surroundings
          <img src="/media/img/region/ostrava.png" class="oblast"/>
        </xsl:when>        
        <xsl:when test="$text='ol'">Olomouc and surroundings
          <img src="/media/img/region/ol.png" class="oblast"/>
        </xsl:when>        
        <xsl:when test="$text='ul'">Ústí nad Labem and surroundings
          <img src="/media/img/region/usti.png" class="oblast"/>
        </xsl:when>        
        <xsl:when test="$text='lib'">Liberec a okolí
          <img src="/media/img/region/lbc.png" class="oblast"/>
        </xsl:when>        
        <xsl:when test="$text='slovensko'">Slovakia
          <img src="/media/img/region/sk.png" class="oblast"/>
        </xsl:when>      
      </xsl:choose>    
    </xsl:if>  
  </xsl:template>
  <xsl:template name="split_generace">
    <xsl:param name="text"/>
    <xsl:variable name="first" select='substring-before($text,";")'/>
    <xsl:variable name='rest' select='substring-after($text,";")'/>
    <xsl:if test='$first'>
      <xsl:choose>
        <xsl:when test="$first='mlada'">youth</xsl:when>
        <xsl:when test="$first='stredni'">middle generation</xsl:when>
        <xsl:when test="$first='starsi'">elderly people</xsl:when>
        <xsl:when test="$first='deti'">children (children's sign)</xsl:when>
      </xsl:choose>      
      <xsl:if test="$rest">, 
      </xsl:if>    
    </xsl:if>    
    <xsl:if test='$rest'>      
      <xsl:call-template name='split_generace'>        
        <xsl:with-param name='text' select='$rest'/>      
      </xsl:call-template>    
    </xsl:if>    
    <xsl:if test='not($rest)'>      
      <xsl:choose>        
        <xsl:when test="$text='mlada'">youth</xsl:when>
        <xsl:when test="$text='stredni'">middle generation</xsl:when>
        <xsl:when test="$text='starsi'">elderly people</xsl:when>
        <xsl:when test="$text='deti'">children (children's sign)</xsl:when>
      </xsl:choose>
    </xsl:if>
  </xsl:template>
  <xsl:template name="split_skupina2">
    <xsl:param name="text"/>
    <xsl:variable name="first" select='substring-before($text,";")'/>
    <xsl:variable name='rest' select='substring-after($text,";")'/>
    <xsl:if test='$first'>
     <xsl:choose>   
      <xsl:when test="$first='intr'">intransitive</xsl:when>
        <xsl:when test="$first='tran'">transitive</xsl:when>
        <xsl:when test="$first='pohyb'">expressing movement of object in space</xsl:when>
        <xsl:when test="$first='misto'">identifying place</xsl:when>
        <xsl:when test="$first='prost'">expressing means</xsl:when>
        <xsl:when test="$first='subj'">indicating subject</xsl:when>
        <xsl:when test="$first='obj'">indicating object</xsl:when>
        <xsl:when test="$first='reci'">reciprocal</xsl:when>
        <xsl:when test="$first='polo'">half-indicating</xsl:when>
        <xsl:when test="$first='lok'">locative</xsl:when>
        <xsl:when test="$first='pok'">bent</xsl:when>
        <xsl:when test="$first='dej'">of process</xsl:when>
        <xsl:when test="$first='fre'">of frequency</xsl:when>
        <xsl:when test="$first='mir'">of rate</xsl:when>
        <xsl:when test="$first='obr'">reversed</xsl:when>
        <xsl:when test="$first='iko'">iconic</xsl:when>
        <xsl:when test="$first='spec'">specific</xsl:when>     
      </xsl:choose>      
      <xsl:if test="$rest">, 
      </xsl:if>    
    </xsl:if>    
    <xsl:if test='$rest'>      
      <xsl:call-template name='split_skupina2'>        
        <xsl:with-param name='text' select='$rest'/>      
      </xsl:call-template>    
    </xsl:if>    
    <xsl:if test='not($rest)'>      
      <xsl:choose>        
        <xsl:when test="$text='intr'">intransitive</xsl:when>
        <xsl:when test="$text='tran'">transitive</xsl:when>
        <xsl:when test="$text='pohyb'">expressing movement of object in space</xsl:when>
        <xsl:when test="$text='misto'">identifying place</xsl:when>
        <xsl:when test="$text='prost'">expressing means</xsl:when>
        <xsl:when test="$text='subj'">indicating subject</xsl:when>
        <xsl:when test="$text='obj'">indicating object</xsl:when>
        <xsl:when test="$text='reci'">reciprocal</xsl:when>
        <xsl:when test="$text='polo'">half-indicating</xsl:when>
        <xsl:when test="$text='lok'">locative</xsl:when>
        <xsl:when test="$text='pok'">bent</xsl:when>
        <xsl:when test="$text='dej'">of process</xsl:when>
        <xsl:when test="$text='fre'">of frequency</xsl:when>
        <xsl:when test="$text='mir'">of rate</xsl:when>
        <xsl:when test="$text='obr'">reversed</xsl:when>
        <xsl:when test="$text='iko'">iconic</xsl:when>
        <xsl:when test="$text='spec'">specific</xsl:when>
      </xsl:choose>    
    </xsl:if>
  </xsl:template>
  <xsl:template name="split_skupina3">
    <xsl:param name="text"/>
    <xsl:variable name="first" select='substring-before($text,";")'/>
    <xsl:variable name='rest' select='substring-after($text,";")'/>
    <xsl:if test='$first'>
      <xsl:choose>
        <xsl:when test="$first='redup'">by reduplication </xsl:when>
        <xsl:when test="$first='prof'">by adding proform (pronoun, classificators)</xsl:when>
        <xsl:when test="$first='kvan'">by adding quantificator</xsl:when>
        <xsl:when test="$first='ink'"><!--by incorporating number morphemes-->no plural form</xsl:when>
        <xsl:when test="$first='plur'">by adding plural specificator</xsl:when>
        <xsl:when test="$first='redo'">reduplication expresses process repetition</xsl:when>
        <xsl:when test="$first='redt'">reduplication expresses process duration</xsl:when>
        <xsl:when test="$first='okol'">verb expresses process circumstances</xsl:when>
        <xsl:when test="$first='rt'">quick reduplication expresses process duration</xsl:when>
        <xsl:when test="$first='rp'">quick reduplication expresses regularity</xsl:when>
        <xsl:when test="$first='pp'">slow reduplication expresses process continuation</xsl:when>
        <xsl:when test="$first='po'">slow reduplication expresses process repetition</xsl:when>
        <xsl:when test="$first='netvori'">no plural form</xsl:when>
        <xsl:when test="$first='redupklas'">by reduplicating classificator / speficicator</xsl:when>
      </xsl:choose>      
      <xsl:if test="$rest">, 
      </xsl:if>    
    </xsl:if>    
    <xsl:if test='$rest'>
      <xsl:call-template name='split_skupina3'>
        <xsl:with-param name='text' select='$rest'/>
      </xsl:call-template>
    </xsl:if>
    <xsl:if test='not($rest)'>
      <xsl:choose>
        <xsl:when test="$text='redup'">by reduplication </xsl:when>
        <xsl:when test="$text='prof'">by adding proform (pronoun, classificators)</xsl:when>
        <xsl:when test="$text='kvan'">by adding quantificator</xsl:when>
        <xsl:when test="$text='ink'">by incorporating number morphemes</xsl:when>
        <xsl:when test="$text='plur'"><!--by adding plural specificator-->no plural form</xsl:when>
        <xsl:when test="$text='redo'">reduplication expresses process repetition</xsl:when>
        <xsl:when test="$text='redt'">reduplication expresses process duration</xsl:when>
        <xsl:when test="$text='okol'">verb expresses process circumstances</xsl:when>
        <xsl:when test="$text='rt'">quick reduplication expresses process duration</xsl:when>
        <xsl:when test="$text='rp'">quick reduplication expresses regularity</xsl:when>
        <xsl:when test="$text='pp'">slow reduplication expresses process continuation</xsl:when>
        <xsl:when test="$text='po'">slow reduplication expresses process repetition</xsl:when>
        <xsl:when test="$text='netvori'">no plural form</xsl:when>
        <xsl:when test="$text='redupklas'">by reduplicating classificator / speficicator</xsl:when>
      </xsl:choose>    
    </xsl:if>  
  </xsl:template>  
  
  <xsl:template name="status_publish">
    <xsl:param name="status"/>
    <xsl:param name="type"/>
    <xsl:param name="element"/>
    <xsl:if test="$status='published'">approved</xsl:if>    
    <xsl:if test="$status!='published'"><!-- skryto -->
      <xsl:text> 
      </xsl:text>      
      <xsl:choose>
        <xsl:when test="$type='relation'">
          <input type="button" onclick="publish_relation('czj', '{/entry/@id}', '{$element/../@id}', '{$element/@meaning_id}', '{$element/@type}', this)" value="schválit"/>
        </xsl:when>
        <xsl:when test="$type='usage'">
          <input type="button" onclick="publish_usage('czj', '{/entry/@id}', '{$element/../../@id}', '{$element/@id}', this)" value="schválit"/>
        </xsl:when>
        <xsl:when test="$type='meaning'">
          <input type="button" onclick="publish_meaning('czj', '{/entry/@id}', '{$element/@id}', this)" value="schválit"/>
        </xsl:when>
        <xsl:when test="$type='gram' or $type='style' or $type='sw' or $type='hns' or $type='video_front' or $type='video_side' or $type='entry'">
          <input type="button" onclick="publish('czj', '{/entry/@id}', '{$type}', this)" value="schválit"/>
        </xsl:when>
      </xsl:choose>
    </xsl:if>
  </xsl:template>
</xsl:stylesheet>

