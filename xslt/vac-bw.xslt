<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="utf-8" method="xml"
  doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
  doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
  />

<xsl:template match="H">
  <html>
     <head>
       <title>VAC: <xsl:value-of select="B/Z/text()"/> <xsl:value-of select="B/Z/SUB"/></title>
      <style type="text/css">
      body {
      background: #ffffff;
      color: #000000;
      }
      .head {
      font-size: 150%;
      }
      </style>
     </head>
     <body>
       <p><xsl:apply-templates/></p>
       <hr />
       <p><small>Velký anglicko-český slovník, LEDA</small></p>
     </body>
  </html>
</xsl:template>

<xsl:template match="B">
  <b class="red head"><xsl:value-of select="Z/text()"/><sub><xsl:value-of select="Z/SUB"/></sub></b><br/>
  <xsl:apply-templates select="*[not(self::Z)]"/>
  <br/>
</xsl:template>

<xsl:template match="B/N">
  <xsl:apply-templates/>
</xsl:template>
<xsl:template match="B/N/B">
  <b><xsl:apply-templates/></b>
</xsl:template>

<xsl:template match="V|VYEN">
  <em><xsl:apply-templates/></em><xsl:text> </xsl:text>
</xsl:template>

<xsl:template match="B/S">
  <span class="green"><xsl:apply-templates/></span>
</xsl:template>

<xsl:template match="CE/S">
  <span class="green"><xsl:apply-templates/></span>
</xsl:template>

<xsl:template match="N/S">
  <span class="green"><xsl:apply-templates/></span>
</xsl:template>

<xsl:template match="N">
  <xsl:apply-templates/>
</xsl:template>

<xsl:template match="PA">
  <b class="green"><xsl:apply-templates/></b>
</xsl:template>

<xsl:template match="PC">
  <i><xsl:apply-templates/></i>
</xsl:template>

<xsl:template match="BEN">
  <b><xsl:apply-templates/></b>
</xsl:template>
<xsl:template match="PH/B">
  <b><xsl:apply-templates/></b>
</xsl:template>


<xsl:template match="IEN">
  <i><xsl:apply-templates/></i>
</xsl:template>

<xsl:template match="SEN">
  <sub><xsl:apply-templates/></sub>
</xsl:template>

<xsl:template match="CHNS">
  <span class="darkred"><xsl:apply-templates/></span>
</xsl:template>

<xsl:template match="NCZ">
  <xsl:apply-templates/>
</xsl:template>

<xsl:template match="PC/S|PA/S|CHNS/S">
  <xsl:if test=".='or'">/</xsl:if>
  <xsl:if test=".='etc'">...</xsl:if>
</xsl:template>


<xsl:template match="BE">
  <p><b class="blue"><xsl:value-of select="@value"/>:</b>
  <xsl:apply-templates/>
  </p>
</xsl:template>
<xsl:template match="CE">
  <b class="red"><xsl:value-of select="@value"/>:</b>
  <xsl:apply-templates/>
  ;
</xsl:template>


<xsl:template match="key"/>

</xsl:stylesheet>

