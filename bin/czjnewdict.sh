#!/bin/bash

if [ -z $1 ]; then
  echo "specify dictionary code";
  exit;
fi
if [ -z $2 ]; then
  echo "druh slovniku? s = znakovy, w = mluveny, napr. czjnewdict.sh czj s";
  exit;
fi

if [ ! -z $1 ]; then
  tfile=$(mktemp /tmp/czj.XXXXXXXXX)
  mfile=$(mktemp /tmp/czj.XXXXXXXXX)
  code=${1//[!a-z]/}
  echo $code
  if [ "$2" == "s" ]; then
    echo '<dict><code>'$code'</code><name>'$code'</name><class>CZJ</class><root>entry</root><key>/entry/@id</key><xslts><xslt><name>inline-en</name><file>'$code'-inline-en.xslt</file></xslt><xslt><name>inline</name><file>'$code'-inline.xslt</file></xslt><xslt><name>preview</name><file>czj-sign-preview.xslt</file></xslt></xslts></dict>' > $tfile
  fi
  if [ "$2" == "w" ]; then
    echo '<dict><code>'$code'</code><name>'$code'</name><class>CZJ</class><root>entry</root><key>/entry/@id</key><xslts><xslt><name>inline-en</name><file>'$code'-inline-en.xslt</file></xslt><xslt><name>inline</name><file>'$code'-inline.xslt</file></xslt><xslt><name>preview</name><file>czj-write-preview.xslt</file></xslt></xslts></dict>' > $tfile
  fi
  echo '<dict><code>'$code'_media</code><name>'$code' media</name><class>CZJ</class><root>media</root><key>/media/@id</key></dict>' > $mfile
  if [ `se_term -query 'count(collection("admininfo")/dict[code="'$code'"])' deb` -eq 0 ] ; then
    se_term -query 'LOAD "'$tfile'" "dict'$code'" "admininfo"' deb
    se_term -query 'LOAD "'$mfile'" "dict'$code'_media" "admininfo"' deb
    se_term -query 'update insert <dict code="'$code'"/> into doc("serviceczj","admininfo")/service/dicts' deb
    se_term -query 'update insert <dict code="'$code'_media"/> into doc("serviceczj","admininfo")/service/dicts' deb
    sudo /etc/init.d/debserver-common restart czj
    sudo /etc/init.d/debserver-common restart czjpublic
  else
    echo "dictonary " $code " already initialized"
  fi
fi

