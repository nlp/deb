#!/usr/bin/ruby

require 'create_dict'

db_path     = '/nlp/projekty/deb2/stable/didone/diderot-didone.dbxml'
source_xml  = '/nlp/projekty/deb2/stable/data/utf8-diderot.xml'
key_path    = '/ENTRY/KEY/text()'
root_tag    = 'ENTRY'

dict = Dict.new( File.dirname(db_path), File.basename(db_path), key_path )
def dict.process_key(key)
  #key.gsub(/, */, '')
  #key.gsub(/, $/, '')
  #key.gsub(/ $/, '')
  key
end

index = dict.container.index
index.add('', 'KEY', 'node-element-equality-string')
index.add('', 'KEY', 'node-element-substring-string')
index.add('', 'keysmall', 'node-element-equality-string')
index.add('', 'keysmall', 'node-element-substring-string')
dict.container.index = index

listener = StreamListener.new( root_tag, dict )
parser = StreamProxyREX.new( listener )
listener.debug = 
def listener.characters( text )
  if @inside
    if @insidetags.last == 'KEY'
      #text.gsub!(/, */, '')
      #text.gsub!(/, $/, '')
      #text.gsub!(/ $/, '')
      small = REXML::Element.new('keysmall')
      small.add_text(text.downcase)
      @current.parent.add_element(small)
    end
    @current.add_text text
  end
end
parser.parse_stream( File.new( source_xml ) )

