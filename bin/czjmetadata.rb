#!/usr/bin/ruby

require 'rubygems'
require 'sedna'
require 'xml/libxml'
require 'uri'
require 'fileutils'

autor = nil
copy = nil
zdroj = nil

sedna = Sedna.connect({:database=>'deb'})

dir = ARGV[0].sub(/\/$/,'')
restdir = dir+'_rest'
Dir.mkdir(restdir) unless File.directory?(restdir)
logfile = '/tmp/log'+dir.split('/').last.to_s+Time.now.strftime('%Y%m%d-%H%M') +'.log'
log = File.new(logfile,'w')

if File.exist?(dir+'/info.txt')
  File.open(dir+'/info.txt', 'r').each{|line|
    autor = line[6..-1].strip if line[0,6] == 'autor='
    copy = line[5..-1].strip if line[0,5] == 'copy='
    zdroj = line[6..-1].strip if line[0,6] == 'zdroj='
  }
end

Dir[dir+'/*mp4'].sort.each{|file|
  name =  file.sub(dir+'/','')
  puts 'soubor: '+name
  log.puts 'soubor: '+name
  query = 'collection("czj_media")/media[location="'+name+'"]'
  sedna.query(query).each{|e|
    doc = XML::Document.string(e.to_s, :encoding => XML::Encoding::UTF_8)
    id = doc.root['id']
    puts ' video ID: '+id
    log.puts ' video ID: '+id
    unless autor.nil?
      doc.root.find('id_meta_author').each{|m| m.remove!}
      doc.root << XML::Node.new('id_meta_author', autor)
    end
    unless copy.nil?
      doc.root.find('id_meta_copyright').each{|m| m.remove!}
      doc.root << XML::Node.new('id_meta_copyright', copy)
    end
    unless zdroj.nil?
      doc.root.find('id_meta_source').each{|m| m.remove!}
      doc.root << XML::Node.new('id_meta_source', zdroj)
    end
    #puts doc
    sedna.query('DROP DOCUMENT "'+id+'" IN COLLECTION "czj_media"')
    sedna.load_document(doc.to_s, id, "czj_media")
  }
}
log.close
