#!/usr/bin/ruby

require 'create_dict'

db_path     = '/nlp/projekty/deb2/stable/didone/ssjc-didone.dbxml'
source_xml  = '/nlp/projekty/deb2/stable/data/utf8-ssjc.xml'
key_path    = '/root/h/text()'
root_tag    = 'root'

dict = Dict.new( File.dirname(db_path), File.basename(db_path), key_path )
def dict.process_key(key)
  #key.gsub(/, */, '')
  key.gsub(/, $/, '')
  key.gsub(/ $/, '')
end

index = dict.container.index
index.add('', 'h', 'node-element-equality-string')
index.add('', 'h', 'node-element-substring-string')
index.add('', 'bold', 'node-element-equality-string')
index.add('', 'bold', 'node-element-substring-string')
index.add('', 'arial', 'node-element-equality-string')
index.add('', 'arial', 'node-element-substring-string')
dict.container.index = index

listener = StreamListener.new( root_tag, dict )
parser = StreamProxyREX.new( listener )
listener.debug = false
def listener.characters( text )
  if @inside
    if @insidetags.last == 'h'
      #text.gsub!(/, */, '')
      text.gsub!(/, $/, '')
      text.gsub!(/ $/, '')
    end
    @current.add_text text
  end
end
parser.parse_stream( File.new( source_xml ) )

