#!/bin/bash

#check if Sedna governor is running
se_rc
if [ $? -ne 0 ] ; then
  se_gov 
fi

#try to run 'deb' database
result=`se_sm deb 2>&1`
#if not successful, create it first
if [[ "$result" =~ "no database" ]] ; then
  se_cdb deb
  se_sm deb
fi

if [ `se_term -query 'count(fn:doc("$collections")//collection[@name="admininfo"])' deb` -eq "0" ] ; then
  se_term -query 'CREATE COLLECTION "admininfo"' deb
fi

for file in /var/lib/deb-server/init/tecu/*xml; do
  x=${file##/var*/}
  xml=${x%%.xml}
  if [ `se_term -query 'count(fn:doc("$documents")/documents/collection[@name="admininfo"]/document[@name="'$xml'"])' deb` -eq 0 ] ; then
    se_term -query 'LOAD "'$file'" "'$xml'" "admininfo"' deb
  else
    echo $xml" already initialized"
  fi
done
