#!/usr/bin/ruby

require 'rubygems'
require 'sedna'
require 'xml/libxml'
require 'uri'

  def norm_name(name)
    name = name.sub('.flv','')
    name = name.sub('.mp4','')
    sense = ''
    name.sub!(/^[ABDKabdk][-_]/,'')
    var = ''
    var = 'A' if name.include?('_1_FLV_HQ')
    var = 'B' if name.include?('_2_FLV_HQ')
    var = 'C' if name.include?('_3_FLV_HQ')
    var = 'D' if name.include?('_4_FLV_HQ')
    var = 'E' if name.include?('_5_FLV_HQ')
    var = 'F' if name.include?('_6_FLV_HQ')
    name.sub!('_1_FLV_HQ','')
    name.sub!('_2_FLV_HQ','')
    name.sub!('_3_FLV_HQ','')
    name.sub!('_4_FLV_HQ','')
    name.sub!('_5_FLV_HQ','')
    name.sub!('_6_FLV_HQ','')
    name.gsub!('FLV_HQ','')
    if name =~ /_[1-9]$/
      var += name[/_([1-9])$/,1]
      name.sub!(/_[1-9]$/,'')
    end
    var = 'A' if name =~ /_I$/
    var = 'B' if name =~ /_II$/
    var = 'C' if name =~ /_III$/
    var = 'C' if name =~ /[a-z]III$/
    var = 'B' if name =~ /[a-z]II$/
    var = 'D' if name =~ /_IV$/
    name.sub!(/_I$/,'')
    name.sub!(/_II$/,'')
    name.sub!(/_III$/,'')
    name.sub!(/III$/,'')
    name.sub!(/II$/,'')
    name.sub!(/_IV$/,'')
    name.sub!('A-C1','A1')
    name.sub!('A-C-D','A1d')
    got = false
    if name =~ /^[A-Z]$/
      var = 'A'
      got = true
    end
    if not got and name =~ /[A-Z]$/ and not name =~ /[A-Z][A-Z]$/
      var = name[-1,1]
      name = name[0..-2]
    end
    if name =~ /[A-Z][1-9]$/
      var = name[-2,1]
      sense = name[-1,1]
      name = name[0..-3]
    end
    if name =~ /[A-Z][1-9][a-z]$/
      var = name[-3,3]
      name = name[0..-4]
    end
    if name =~ /[0-9][a-z]$/
      var = (name[/([0-9])[a-z]$/,1].to_i+64).chr + (name[/[0-9]([a-z])$/,1][0]-96).to_s
      name = name[0..-3]
    end
    if name =~ /[a-z][0-9]$/
      var = (name[/([0-9])$/,1].to_i+64).chr
      name = name[0..-2]
    end
    var = 'A' if var == ''
    name.gsub!(/([a-z])([A-Z])/, '\1-\2')
    name.gsub!('_','-')
    name.gsub!('(','')
    name.gsub!(')','')
    name.downcase!
    return name+'=='+var, sense
  end

sedna = Sedna.connect({:database=>'deb'})

['czj', 'asl', 'is'].each{|code|
  dict = code+'_media'
  query = 'collection("'+dict+'")/media[not(label)]'
  sedna.query(query).each{|e|
    doc = XML::Document.string(e.to_s, :encoding => XML::Encoding::UTF_8)
    id = doc.root['id']
    next if doc.root.find('location').first.nil?
    file = doc.root.find('location').first.content.to_s
    file = doc.root.find('original_file_name').first.content.to_s unless doc.root.find('original_file_name').first.nil?
    puts code + ' ' +id
    puts file
    label = norm_name(file)[0]
    puts label
    doc.root << XML::Node.new('label', label)
    #puts doc
    sedna.query('DROP DOCUMENT "'+id+'" IN COLLECTION "'+dict+'"')
    sedna.load_document(doc.to_s, id, dict)
  }
}
