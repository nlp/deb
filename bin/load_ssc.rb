#!/usr/bin/ruby

#$LOAD_PATH.unshift('../lib/')
#$LOAD_PATH.unshift('../../libs/dbxml/bdb-0.5.4/bdbxml2/')
#$LOAD_PATH.unshift('../../libs/dbxml/bdb-0.5.4/src/')
#$LOAD_PATH.unshift('../../libs/dbxml/dbxml-2.0.9/install/lib/')

require 'create_dict'

db_path = '/nlp/projekty/deb2/stable/didone/ssc-didone.dbxml'
source_xml = '/nlp/projekty/deb2/stable/data/utf8-ssc.xml'
key_path = '/root/h'
root_tag = 'root'
dict = Dict.new( File.dirname(db_path), File.basename(db_path), key_path )

index = dict.container.index
index.add('', 'h', 'node-element-equality-string')
index.add('', 'h', 'node-element-substring-string')
index.add('', 't', 'node-element-equality-string')
index.add('', 't', 'node-element-substring-string')
dict.container.index = index

data = ''
id = ''

IO.foreach(source_xml) {|line|
  if line =~ /<root>/
    data = line
  else
    data += line
  end
  match_id = /<h>([^<]*)<\/h>/.match(line)
  if match_id != nil and match_id[1] != nil
    id = match_id[1]
  end
  if line =~ /<\/root>/
    puts "adding "+id
    dict.add(id, data)
    data = ''
  end
}


