#!/usr/bin/ruby

# add debserver library path
$LOAD_PATH.unshift('/var/lib/deb-server/lib/')

# include libraries
require 'bdbxml'
require 'slovnik/slovnik'
require 'servlets/slov'

# set database path
db_path = '/var/lib/deb-server/db'

# initiate DBXML environment
env = BDB::Env.new(db_path, BDB::CREATE | BDB::INIT_TRANSACTION, 0660, {'set_lk_max_lockers'=>2000, 'set_lk_max_locks'=>2000})

# open database, we don't specify keypath because it's used mainly for queries
dict = Dictionary.new(db_path, 'demo.dbxml', '', env)

# add some entries, first argument is the entry id, then data
dict.add('entry1', '<movies><movie name="Shrek" year="2001"/><movie name="Astérix et les Vikings" year="2006"/></movies>')
dict.add('entry2', '<tv><series name="The IT Crowd" year="2006"/><series name="Dead Like Me" year="2003"/></tv>')

# display some of them
entry = dict.get('entry1')
puts entry.to_s

# delete entry
dict.delete('entry1')

# add some more
dict.add('entry3', '<books><book name="The Hogfather" year="1999"/><book name="Ночной доэор" year="1998"/></books>')

# update entry
dict.update('entry2', '<tv><series name="The IT Crowd" year="2006"/><series name="Dead Like Me" year="2003"/><series name="Black Books" year="2000"/></tv>')

# display all the entries
dict.container.each{|doc|
  puts doc
}