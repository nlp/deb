#!/usr/bin/ruby

require 'rubygems'
require 'sedna'
require 'xml/libxml'
require 'uri'
require 'fileutils'

autor = nil
copy = nil
zdroj = nil

sedna = Sedna.connect({:database=>'deb'})

dir = ARGV[0].sub(/\/$/,'')
restdir = dir+'_rest'
Dir.mkdir(restdir) unless File.directory?(restdir)
logfile = '/tmp/log'+dir.split('/').last.to_s+Time.now.strftime('%Y%m%d-%H%M') +'.log'
log = File.new(logfile,'w')

if File.exist?(dir+'/info.txt')
  File.open(dir+'/info.txt', 'r').each{|line|
    autor = line[6..-1].strip if line[0,6] == 'autor='
    copy = line[5..-1].strip if line[0,5] == 'copy='
    zdroj = line[6..-1].strip if line[0,6] == 'zdroj='
  }
end

Dir[dir+'/*mp4'].sort.each{|file|
  name =  file.sub(dir+'/','')
  puts 'soubor: '+name
  log.puts 'soubor: '+name
  oldname = name.sub('.mp4', '.flv').downcase
  #puts oldname
  query = 'collection("czj_media")/media[fn:lower-case(location)="'+oldname+'"]'
  indb = false
  sedna.query(query).each{|e|
    doc = XML::Document.string(e.to_s, :encoding => XML::Encoding::UTF_8)
    id = doc.root['id']    
    puts ' video ID: '+id
    log.puts ' video ID: '+id
    next if doc.root.find('location').first.nil?
    location = doc.root.find('location').first.content.to_s
    indb = true
    #puts location
    doc.root.find('location').first.content = name
    unless autor.nil?
      doc.root.find('id_meta_author').each{|m| m.remove!}
      doc.root << XML::Node.new('id_meta_author', autor)
    end
    unless copy.nil?
      doc.root.find('id_meta_copyright').each{|m| m.remove!}
      doc.root << XML::Node.new('id_meta_copyright', copy)
    end
    unless zdroj.nil?
      doc.root.find('id_meta_source').each{|m| m.remove!}
      doc.root << XML::Node.new('id_meta_source', zdroj)
    end
    #puts doc
    sedna.query('DROP DOCUMENT "'+id+'" IN COLLECTION "czj_media"')
    sedna.load_document(doc.to_s, id, "czj_media")
    query2 = 'collection("czj")/entry[lemma/video_front="'+location+'" or lemma/video_side="'+location+'" or media/file/location="'+location+'"]'
    sedna.query(query2).each{|e2|
      doc2 = XML::Document.string(e2.to_s, :encoding => XML::Encoding::UTF_8)
      id2 = doc2.root['id']    
      puts ' heslo: '+id2
      log.puts ' heslo: '+id2
      doc2.root.find('lemma/video_front[.="'+location+'"]|lemma/video_side[.="'+location+'"]|media/file/location[.="'+location+'"]').each{|loc|
        #puts loc
        #puts loc.content
        loc.content = name
        #puts loc
      }
      #puts doc2
      sedna.query('DROP DOCUMENT "'+id2+'" IN COLLECTION "czj"')
      sedna.load_document(doc2.to_s, id2, "czj")
    }
  }
  if indb
    #copy
    FileUtils.cp(file, '/var/lib/deb-server/files/czj/media/videoczj')
    #thumb
    command = '/var/lib/deb-server/files/czj/editor/mkthumb.sh '+name
    #$stderr.puts command
    out = system(command)
  else
    FileUtils.cp(file, restdir)
  end
}
log.close
