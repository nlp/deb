#!/usr/bin/ruby

require 'create_dict'

db_path     = '/nlp/projekty/deb2/stable/didone/syno-didone.dbxml'
source_xml  = '/nlp/projekty/deb2/stable/data/utf8-syno.xml'
key_path    = 'ENTRY/HEAD/text()'
root_tag    = 'ENTRY'

dict = Dict.new( File.dirname(db_path), File.basename(db_path), key_path )
def dict.process_key(key)
  key.gsub(/, $/, '')
  key.gsub(/ $/, '')
end
      
index = dict.container.index
index.add('', 'HEAD', 'node-element-equality-string')
index.add('', 'HEAD', 'node-element-substring-string')
dict.container.index = index

listener = StreamListener.new( root_tag, dict )
parser = StreamProxyREX.new( listener )
def listener.characters( text )
  if @inside
    if @insidetags.last == 'h'
      #text.gsub!(/, */, '')
      text.gsub!(/, $/, '')
      text.gsub!(/ $/, '')
    end
    @current.add_text text
  end
end
parser.parse_stream( File.new( source_xml ) )

