#!/usr/bin/ruby

require 'optparse'

$LOAD_PATH.unshift('../lib/')

class Object
  def edump(msg='')
    $stderr.puts("edump #{msg}: " + self.to_s)
    return self
  end
end

require 'logger'
require 'rexml/document'
require 'socket'
require 'cgi'
require 'openssl'
require 'webrick'
require 'rubygems'
require 'xml/libxml'
require 'libxslt'
require 'sedna'
require 'open-uri'
require 'twitter_cldr'

# DEB modules
require 'deb_common'
require 'http_server'
require 'dbpasswd'
require 'utf8string'
require 'dict/dict-sedna'
require 'dict/admin'
require 'dict/czj'
require 'servlets/dict'
require 'servlets/hello'
require 'servlets/admin'
require 'servlets/czj'
require 'servlets/czjadmin'

server = nil

port      = 443
daemon    = nil
log_level = Logger::DEBUG
log_file  = nil
db_path   = nil
xslt_path = nil
base_path = '/var/lib/deb-server'
ssl       = true
auth      = true
ssl_ca_cert = nil
database = 'deb'

debug_levels = {
  :debug => Logger::DEBUG,
  :info  => Logger::INFO,
  :warn  => Logger::WARN,
  :error => Logger::ERROR,
  :fatal => Logger::FATAL,
}

ARGV.options do |p|
  p.banner = "usage: #{$0} [options]\n"
  p.on('-p','--port=NUMBER',    String, 'cislo portu')      { |p| port=p.to_i }
  p.on('--db-path=PATH',        String, 'cesta k databazim') { |v| db_path=v}
  p.on('--xslt-path=PATH',      String, 'cesta k sablobnam') { |v| xslt_path=v }
  p.on('--base-path=PATH',      String, 'cesta k zakladnimu adresari') { |v| base_path=v }
  p.on('-d','--daemon',                 'daemonizovat') { |d| daemon = true }
  p.on('-s','--ssl',			'use SSL') { |s| ssl = true }
  p.on(     '--ssl-cert=STRING',String, 'ssl cert/key base') { |v| ssl_cert_base=v }
  p.on(     '--ssl-ca-cert=STRING',String, 'ssl CA certificate') { |v| ssl_ca_cert=v }
  p.on(     '--auth',			'use test auth') { |a| auth = true }
  p.on(     '--text-log=FILE',  String, 'text log file') { |v| text_log=v }
  p.on('-v','--verbose=LEVEL',  String, 'log level') do |log_level| 

    symb = log_level.downcase.intern
    if debug_levels.has_key?(symb)
      log_level = debug_levels[symb]
    else
      puts p
      puts "Invalid log level #{log_level}"
      exit(1)
    end
  end
  p.on('-l','--logfile=FILE','log file') { |v| log_file=v }

  begin
    p.parse!(ARGV)
  rescue
    puts p
    exit(1)
  end
end

if daemon
  require 'daemonize'
  include Daemonize
  daemonize
end

ssl_cert_base = base_path + '/certs/host' if ssl_cert_base == nil

$logger = Logger.new( STDERR )
$logger.level = log_level
$access_log = Logger.new( log_file ? log_file : STDERR )
$access_log.level = log_level

$sedna = Sedna.connect({:database=>database})

if $sedna.query('fn:doc("$collections")//collection[@name="admininfo"]').size == 0
    puts 'Can\'t open admin database. Run debinit.sh first'
    exit
end


info = AdminDict.new('admininfo', db_path)
info.xslt_path = xslt_path
info.service_name = 'czj'
info.template_path = base_path + '/lib/templates/czj'
userdb = AdminDict.new('czj_user', db_path)
userdb.service_name = 'czj'
userdb.template_path = base_path + '/lib/templates/czj'
userdb.xslt_path = xslt_path

server = create_http_server({
  :auth         => auth, 
  :dbpasswd => userdb,
  :loadperms => true,
  :port         => port,
  :ssl          => ssl,
  :ssl_cert_base => ssl_cert_base,
  :ssl_ca_cert => ssl_ca_cert,
  :logger       => $logger,
  :access_log   => $access_log
})
server.info = info

info.remount(server, 'czj', nil)

info.array['czj_user'] = userdb

server.mount_proc('/favicon.ico') {|req, resp|
      resp.set_redirect(WEBrick::HTTPStatus::SeeOther, '/editor/img/icon.png')
}
server.mount "/hello", HelloServlet, info

server.mount "/admin", CZJAdminServlet, userdb, info.array

server.mount_proc('/') {|req, resp|
  $stderr.puts 'REQUESTED URL '+ req.path.to_s
  target = '/czj?action=page'
  target += '&lang='+req.query['lang'] if req.query['lang'].to_s != ''
  resp.set_redirect(WEBrick::HTTPStatus::SeeOther, target)
}

server.mount_proc('/proxy') {|req, resp|
  $stderr.puts 'PROXY '+ req.request_uri.to_s
  newurl = 'http://znaky.zcu.cz'+req.path.to_s+'?'+req.query_string.to_s 
  $stderr.puts 'PROXY '+ newurl
  open(newurl, 'rb') {|rf| puts resp.body = rf.read}
}
server.mount_proc('/api') {|req, resp|
  $stderr.puts 'PROXY '+ req.request_uri.to_s
  newurl = 'http://znaky.zcu.cz'+req.path.to_s+'?'+req.query_string.to_s 
  $stderr.puts 'PROXY '+ newurl
  open(newurl, 'rb') {|rf| puts resp.body = rf.read}
}
server.mount_proc('/korpus') {|req, resp|
  $stderr.puts 'PROXY '+ req.request_uri.to_s
  newurl = 'https://ske.fi.muni.cz/bonito/run.cgi/first?corpname=preloaded%2Fcztenten12_8&iquery='+req.query['lemma']+'&queryselector=iqueryrow&default_attr=word&fc_lemword_window_type=both&fc_lemword_wsize=5&gdex_enabled=1&viewmode=sentence&refs==doc.url&pagesize=40&format=json';
  $stderr.puts 'PROXY '+ newurl
  require 'net/http'
  require 'net/https'
  require 'openssl'
  uri = URI(newurl)
  http = Net::HTTP.new(uri.host, uri.port)
  http.use_ssl = true
  http.verify_mode = OpenSSL::SSL::VERIFY_NONE
  request = Net::HTTP::Get.new(uri.request_uri)
  response = http.request(request)
  resp['Content-Type'] = 'application/json; charset=utf8'
  resp.body = response.body
}
server.mount "/editor",  WEBrick::HTTPServlet::FileHandler, base_path + '/files/czj/editor'
server.mount_proc('/editorczj') {|req, resp|
  resp.set_redirect(WEBrick::HTTPStatus::SeeOther, '/editor/?'+req.query_string)
}

server.mount "/editorcs",  WEBrick::HTTPServlet::FileHandler, base_path + '/files/czj/editorcs'
server.mount "/editoris",  WEBrick::HTTPServlet::FileHandler, base_path + '/files/czj/editoris'
server.mount "/editorasl",  WEBrick::HTTPServlet::FileHandler, base_path + '/files/czj/editorasl'
server.mount "/editoren",  WEBrick::HTTPServlet::FileHandler, base_path + '/files/czj/editoren'
server.mount "/editorsj",  WEBrick::HTTPServlet::FileHandler, base_path + '/files/czj/editorsj'
server.mount "/editorspj",  WEBrick::HTTPServlet::FileHandler, base_path + '/files/czj/editorspj'
server.mount "/media",  WEBrick::HTTPServlet::FileHandler, base_path + '/files/czj/media'



trap("TERM"){
  $stderr.puts "GOT TERM"
  server_shutdown(server, info)
}
#trap("KILL"){
#  $stderr.puts "GOT KILL"
#  server_shutdown(server, info)
#}

begin
  server.start
rescue => e
  $logger.error( "exception #{e.class}: #{e.message}\n" + e.backtrace.join("\n\t\t") )
  raise
end
