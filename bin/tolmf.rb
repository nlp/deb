#!/usr/bin/ruby

require 'xml/libxml'

ARGV.each{|arg|
  puts '*'+arg+'*'
  outfile = arg+'.lmf'
  out = File.new(outfile, 'w')
  doc = XML::Document.file(arg,  :encoding => XML::Encoding::UTF_8)
  #doc.encoding = 'UTF-8'

  out.puts "<LexicalResource>
 <GlobalInformation label=''/>
 <Lexicon languageCoding='' label='' language='' owner='' version=''>
  "
  out.fsync

  lexentry = {}
  synsets = []
  axes = []
  doc.find('//SYNSET').each{|syn|
    sid = syn.find('ID').to_a.first.content.to_s
    pos = syn.find('POS').to_a.first.content.to_s
    bcs = ''
    bcs = syn.find('BCS').to_a.first.content.to_s unless syn.find('BCS').to_a.first.nil?
    definition = []
    syn.find('DEF').to_a.each{|df|
      definition << df.content.to_s
    }
    exam = []
    syn.find('USAGE').to_a.each{|df|
      exam << df.content.to_s
    }

    rels = []
    syn.find('ILR').to_a.each{|rl|
      tar = rl.content.to_s
      type = rl['type'].to_s
      type = 'has_hyperonym' if type == 'hypernym'
      rels << {'target'=>tar, 'type'=>type, 'author'=>rl['author'].to_s, 'date'=>rl['date'].to_s, 'source'=>rl['source'].to_s, 'confidenceScore'=>rl['confidenceScore'].to_s}
    }
    erels = []
    syn.find('ELR').to_a.each{|rl|
      tar = rl.content.to_s
      type = rl['type'].to_s
      erels << {'target'=>tar, 'type'=>type, 'system'=>rl['system'].to_s}
    }
    syn.find('SUMO').to_a.each{|rl|
      tar = rl.content.to_s
      erels << {'target'=>tar, 'type'=>'', 'system'=>'SUMO'}
    }
    syn.find('DOMAIN').to_a.each{|rl|
      tar = rl.content.to_s
      erels << {'target'=>tar, 'type'=>'', 'system'=>'Domain'}
    }
    
    syn.find('SYNONYM/LITERAL').each{|lit|
      lemma = lit.content
      lexentry[lemma+'_'+pos] = {'pos'=>pos,  'sense'=>[]} if lexentry[lemma+'_'+pos].nil?
      lexentry[lemma+'_'+pos]['sense'] << {'synset'=>sid, 'id'=>sid+'_'+lit['sense'].to_s}
    }
    synsets << {'id'=>sid, 'def'=>definition, 'exam'=>exam,'bcs'=>bcs, 'rels'=>rels, 'erels'=>erels}
  }

  lexentry.each{|lemmapos,info|
    lemma, pos = lemmapos.split('_')
    #puts lemmapos
    out.puts '<LexicalEntry>'
    out.puts '<Lemma writtenForm="'+lemma+'" partOfSpeech="'+info['pos']+'"/>'
    info['sense'].each{|sh|
      out.puts '<Sense id="'+sh['id']+'" synset="'+sh['synset']+'"/>'
    }
    out.puts '</LexicalEntry>'
    out.fsync
  }

  synsets.each{|syn|
    #puts syn['id']
    out.puts '<Synset id="'+syn['id']+'" baseConcept="'+syn['bcs']+'">'
    syn['def'].each{|d|
      out.puts '<Definition gloss="'+d.gsub('&','&amp;').gsub('"','&quot;')+'"/>'
    }
    syn['exam'].each{|d|
      out.puts '<Statement example="'+d.gsub('&','&amp;').gsub('"','&quot;')+'"/>'
    }
    out.puts '<SynsetRelations>'
    syn['rels'].each{|rl|
      out.puts '<SynsetRelation target="'+rl['target']+'" relType="'+rl['type']+'">'
      out.puts '<Meta author="'+rl['author']+'" date="'+rl['date']+'" source="'+rl['source']+'" confidenceScore="'+rl['confidenceScore']+'"/>'
      out.puts '</SynsetRelation>'
    }
    out.puts '</SynsetRelations>'

    out.puts '<MonolingualExternalRefs>'
    syn['erels'].each{|rl|
      out.puts '<MonolingualExternalRef externalSystem="'+rl['system']+'" externalReference="'+rl['target']+'" relType="'+rl['type']+'"/>'
    }
    out.puts '</MonolingualExternalRefs>'
    out.puts '</Synset>'
    out.fsync
  }
  out.puts '</Lexicon>'

  out.puts '<SenseAxes>'
  out.puts '</SenseAxes>'

  out.puts '</LexicalResource>'
  out.close
}
