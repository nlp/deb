#!/usr/bin/ruby

require 'create_dict'
require 'xml/libxml'

db_path     = '/var/lib/deb-server/db/psjc2.dbxml'
source_xml  = '/nlp/projekty/deb2/stable/data/psjc.xml'
key_path    = '/h/Cil'
root_tag    = 'h'

dict = Dict.new( File.dirname(db_path), File.basename(db_path), key_path )

index = dict.container.index
index.add('', 'Cil', 'node-element-equality-string')
index.add('', 'Cil', 'node-element-substring-string')
index.add('', 'Vyzn', 'node-element-equality-string')
index.add('', 'Vyzn', 'node-element-substring-string')
dict.container.index = index

def load_xml_string(string)
    p = XML::Parser.new
    p.string = string
    doc = p.parse
    doc.encoding = 'utf-8'
    return doc
  end

data = ''
id = ''

IO.foreach(source_xml) {|line|
  if line =~ /<h>/
    data = line
  else
    data += line
  end
  if line =~ /<\/h>/
    doc = load_xml_string(data)
    el = doc.find('/h/Cil').to_a.first
    id_b = el.to_s
    id = id_b
    go = false
    idcount = 1
    while not go
      begin
        dict.get(id)
        idcount += 1
        id = id_b + ' (' + idcount.to_s + ')'
      rescue
        go = true
      end
    end
    el.content = id
    data = doc.to_s(false).split("\n")[1].to_s
    puts "adding "+id
    dict.add(id, data)
    data = ''
  end
}
