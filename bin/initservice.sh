#!/bin/bash

if [[ -z "$1" || "$1" =~ "." || "$1" =~ "/" ]] ; then
  echo "specify service name as parameter, for example '/var/lib/deb-server/bin/initservice.sh debdict'"
  exit
fi

#check if Sedna governor is running
se_rc
if [ $? -ne 0 ] ; then
  se_gov 
fi

#try to run 'deb' database
result=`se_sm deb 2>&1`
#if not successful, create it first
if [[ "$result" =~ "no database" ]] ; then
  echo "run /var/lib/deb-server/bin/debinit.sh first"
  exit
fi

for file in /var/lib/deb-server/init/$1/*xml; do
  x=${file##/var*/}
  xml=${x%%.xml}
  if [ `se_term -query 'count(fn:doc("$documents")/documents/collection[@name="admininfo"]/document[@name="'$xml'"])' deb` -eq 0 ] ; then
    se_term -query 'LOAD "'$file'" "'$xml'" "admininfo"' deb
  else
    echo $xml" already initialized"
  fi
done
