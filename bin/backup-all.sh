#!/bin/bash
if [ -f /etc/deb-server/backup.conf ] ; then
  . /etc/deb-server/backup.conf
else
  echo "Config not found: /etc/deb-server/backup.conf"
  exit 1
fi

RUN_DIR=`dirname $0`

for dict in $BACKUP_DBS_XML; do
  echo $RUN_DIR/dump.rb --out-path=$BACKUP_DIR --log-path=$BACKUP_DIR --database=$dict -z
  $RUN_DIR/dump.rb --out-path=$BACKUP_DIR --log-path=$BACKUP_DIR  --database=$dict -z
done

for dict in $BACKUP_DBS_RUBY; do
  echo $RUN_DIR/dump.rb --out-path=$BACKUP_DIR --log-path=$BACKUP_DIR  --database=$dict -z -r
  $RUN_DIR/dump.rb --out-path=$BACKUP_DIR --log-path=$BACKUP_DIR  --database=$dict -z -r
done

for dict in $BACKUP_DBS_LMF; do
  cd /var/lib/deb-server/files;
  gzip -fd $dict-latest.xml.gz
  /var/lib/deb-server/bin/tolmf.rb $dict-latest.xml
  gzip -f $dict-latest.xml
  gzip -f $dict-latest.xml.lmf
done
