#!/usr/bin/ruby

require 'optparse'

$LOAD_PATH.unshift('../lib/')

class Object
  def edump(msg='')
    $stderr.puts("edump #{msg}: " + self.to_s)
    return self
  end
end

require 'logger'
require 'rexml/document'
require 'socket'
require 'cgi'
require 'openssl'
require 'webrick'
require 'rubygems'
require 'xml/libxml'
require 'libxslt'
require 'sedna'


# DEB modules
require 'deb_common'
require 'http_server'
require 'dbpasswd'
require 'dict/dict-sedna'
require 'dict/admin'
require 'dict/debdict'
require 'dict/debdictadmin'
require 'dict/wn2'
require 'servlets/dict'
require 'servlets/hello'
require 'servlets/admin'
require 'servlets/debdict'
require 'servlets/debdictdoc'
require 'servlets/htmlproxy'
require 'servlets/gslov'
require 'servlets/json_wn'

server = nil

port      = 8005
daemon    = nil
log_level = Logger::DEBUG
log_file  = nil
db_path   = nil
xslt_path = nil
base_path = '/var/lib/deb-server'
ssl       = true
auth      = true
ssl_ca_cert = nil
database = 'deb'

debug_levels = {
  :debug => Logger::DEBUG,
  :info  => Logger::INFO,
  :warn  => Logger::WARN,
  :error => Logger::ERROR,
  :fatal => Logger::FATAL,
}

ARGV.options do |p|
  p.banner = "usage: #{$0} [options]\n"
  p.on('-p','--port=NUMBER',    String, 'cislo portu')      { |p| port=p.to_i }
  p.on('--db-path=PATH',        String, 'cesta k databazim') { |v| db_path=v}
  p.on('--xslt-path=PATH',      String, 'cesta k sablobnam') { |v| xslt_path=v }
  p.on('--base-path=PATH',      String, 'cesta k zakladnimu adresari') { |v| base_path=v }
  p.on('-d','--daemon',                 'daemonizovat') { |d| daemon = true }
  p.on('-s','--ssl',			'use SSL') { |s| ssl = true }
  p.on(     '--ssl-cert=STRING',String, 'ssl cert/key base') { |v| ssl_cert_base=v }
  p.on(     '--ssl-ca-cert=STRING',String, 'ssl CA certificate') { |v| ssl_ca_cert=v }
  p.on(     '--auth',			'use test auth') { |a| auth = true }
  p.on(     '--text-log=FILE',  String, 'text log file') { |v| text_log=v }
  p.on('-v','--verbose=LEVEL',  String, 'log level') do |log_level| 

    symb = log_level.downcase.intern
    if debug_levels.has_key?(symb)
      log_level = debug_levels[symb]
    else
      puts p
      puts "Invalid log level #{log_level}"
      exit(1)
    end
  end
  p.on('-l','--logfile=FILE','log file') { |log_file| }

  begin
    p.parse!(ARGV)
  rescue
    puts p
    exit(1)
  end
end

if daemon
  require 'daemonize'
  include Daemonize
  daemonize
end

ssl_cert_base = base_path + '/certs/host' if ssl_cert_base == nil
$logger = Logger.new( STDERR )
$logger.level = log_level
$access_log = Logger.new( log_file ? log_file : STDERR )
$access_log.level = log_level

$sedna = Sedna.connect({:database=>database})

if $sedna.query('fn:doc("$collections")//collection[@name="admininfo"]').size == 0
    puts 'Can\'t open admin database. Run debinit.sh first'
    exit
end


info = DebdictAdmin.new('admininfo', db_path)
info.xslt_path = xslt_path
info.service_name = 'debdict'

server = create_http_server({
  :auth         => auth, 
  :dbpasswd => info,
  :loadperms => true,
  :port         => port,
  :ssl          => ssl,
  :ssl_cert_base => ssl_cert_base,
  :ssl_ca_cert => ssl_ca_cert,
  :logger       => $logger,
  :access_log   => $access_log
})
server.info = info

info.remount(server, 'debdict', nil)

server.mount "/hello", DebdictDocServlet, info
server.mount "/ajka", HTMLProxyServlet,
    'https://nlp.fi.muni.cz/projekty/wwwajka/WwwAjkaSkripty/morph-analyser.cgi?jazyk=0&slovo=%s&akce=3&kodovani=0',
    'iso-8859-2',
    proc { |text|
	n=0
	text.gsub!(/<center>.*?<\/center>/im) { |s|
	    n+=1
	    n<=1||n>=3 ? '' : s
	}
	text.gsub!(/<hr\b[^>]*>/im,'')
	text.gsub!(/<\/head>/im,"\n<base href='http://nlp.fi.muni.cz/projekty/wwwajka/WwwAjkaSkripty/'>\n\\&")
  text.gsub!('http://', 'https://')
    }
server.mount "/google", HTMLProxyServlet,
    'http://www.google.com/search?q=%s&oe=utf-8&hl=$lang&lr=lang_$lang',
    'force:utf-8',
    proc { |text|
	n=0
	text.gsub!(/<\/head>/im,"\n<base href='http://www.google.com/'>\n\\&")
    }
server.mount "/answers", HTMLProxyServlet,
    'http://www.answers.com/%s',
    'force:utf-8',
    proc { |text|
	n=0
	text.gsub!(/<!--\W*HEADER.*PAGE CONTENT\W*-->\s*<br\/>/im,'')
	text.gsub!(/<!--\W*ON THIS PAGE DROP DOWN.*END OF ON THIS PAGE DROP DOWN\W*-->/im,'')
	text.gsub!(/(<iframe[^>]*id="soundFrame"><\/iframe>)\s*<script.*?<\/script>/im,
	    "\\1\n<script type=\"text/javascript\">\n"+
	    "<!--\n"+
	    "var IFrameObj = document.getElementById(\"soundFrame\");\n"+
	    "function playIt(soundUrl) {\n"+
	    "   IFrameObj.src = \"about:blank\";\n"+
	    "  	IFrameObj.src = soundUrl;\n"+
	    "   setTimeout(\"endPlay()\", 10000);\n"+
	    "}\n"+
	    "function endPlay() {\n"+
	    "	IFrameObj.src = \"about:blank\";\n"+
	    "}\n"+
	    "-->\n"+
	    "</script>\n")
	text.gsub!(/<\/head>/im,"\n<base href='http://www.answers.com/'>\n\\&")
    }
server.mount "/wikipedia", HTMLProxyServlet,
    'http://$lang.wikipedia.org/wiki/Special:Search?search=%s&go=Go',
    'force:utf-8',
    proc { |text|
      text.gsub!(/<head>/, "<head>\n<base href='http://en.wikipedia.org/wiki/'>\n")
      text.gsub!(/<script type="text\/javascript" src="\/skins-1.5\/common\/wikibits.js"><\/script>/,"")
      text.gsub!(/<script type="text\/javascript" src="\/skins-1.5\/common\/wikibits.js\?[0-9]*"><!-- wikibits js --><\/script>/,"")
      text.gsub!(/ href=/, " target='_blank' href=")
    }
server.mount "/seznam", HTMLProxyServlet,
    'http://encyklopedie.seznam.cz/search?s=%s',
    'force:utf-8',
    proc { |text|
	    text.gsub!(/<\/head>/im,"\n<base href='http://encyklopedie.seznam.cz/'>\n\\&")
    }
#server.mount "/db",   DBServlet,   db_connect
#server.mount "/map",  MapServlet

server.mount "/files",  WEBrick::HTTPServlet::FileHandler, base_path + '/files'
server.mount "/debdict",  WEBrick::HTTPServlet::FileHandler, base_path + '/files/debdict'
server.mount "/gslov", GSlovServlet, {5=>{'syno'=>info.array['syno']}, 7=>{'scfin'=>info.array['scfin']}, 6=>{'scfis'=>info.array['scfis']}, 4=>{'scs'=>info.array['scs']}, 1=>{'ssjc'=>info.array['ssjc']}, 2=>{'ssc'=>info.array['ssc']}, 8=>{'diderot'=>info.array['diderot']}, 3=>{'psjc'=>info.array['psjc']}}

trap("TERM"){
  $stderr.puts "GOT TERM"
  server_shutdown(server, info)
}
#trap("KILL"){
#  $stderr.puts "GOT KILL"
#  server_shutdown(server, info)
#}

begin
  server.start
rescue => e
  $logger.error( "exception #{e.class}: #{e.message}\n" + e.backtrace.join("\n\t\t") )
  raise
end
