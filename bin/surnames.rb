#!/usr/bin/ruby

require 'optparse'

class Object
  def edump
    $stderr.puts('edump: ' + self.to_s)
    return self
  end
end

$LOAD_PATH.unshift('../lib')

require 'logger'
require 'socket'
require 'webrick'
require 'openssl'
require 'rubygems'
require 'xml/libxml'
require 'libxslt'
require 'json'
require 'sedna'

# DEB modules
require 'deb_common'
require 'http_server'
require 'dbpasswd'
require 'dict/dict-sedna'
require 'dict/surnames'
require 'dict/admin'
require 'dict/tna'
require 'dict/dafn2'
require 'dict/oup'
require 'servlets/dict'
require 'servlets/hello'
require 'servlets/surnames'
require 'servlets/tna'
require 'servlets/dafn2'
require 'servlets/oup'
#require 'utf8string'
require 'twitter_cldr'

server = nil

port      = 443
daemon    = nil
log_level = Logger::DEBUG
log_file  = nil
db_path   = '/var/lib/deb-server/db'
xslt_path = '/var/lib/deb-server/xslt'
base_path = '/var/lib/deb-server'
ssl       = true
auth      = true
ssl_ca_cert = nil
database = 'deb'

ARGV.options do |p|
  p.banner = "usage: #{$0} [options]\n"
  p.on('-p','--port=NUMBER',    String, 'port number')      { |p| port=p.to_i }
  p.on('--db-path=PATH',        String, 'database path') { |v| db_path=v }
  p.on('--xslt-path=PATH',      String, 'XSLT path') { |v| xslt_path=v }
  p.on('--base-path=PATH',      String, 'basedir path') { |v| base_path=v }
  p.on('-d','--daemon',                 'daemonize') { |d| daemon = true }
  p.on('-s','--ssl',			'use SSL') { |s| ssl = true }
  p.on(     '--ssl-cert=STRING',String, 'ssl cert/key base') { |v| ssl_cert_base=v }
  p.on(     '--ssl-ca-cert=STRING',String, 'ssl CA certificate') { |v| ssl_ca_cert=v }
  p.on(     '--auth',			'use test auth') { |a| auth = true }
  p.on(     '--htpasswd=FILE',	String,	'use htpasswd auth') { |htpasswd| }
  p.on(     '--text-log=FILE',  String, 'text log file') { |v| text_log=v }
  p.on('-v','--verbose=LEVEL',  String, 'log level') do |log_level| 
    symb = log_level.downcase.intern
    if debug_levels.has_key?(symb)
      log_level = debug_levels[symb]
    else
      puts p
      puts "Invalid log level #{log_level}"
      exit(1)
    end
  end
  p.on('-l','--logfile=FILE','log file') { |log_file| }

  begin
    p.parse!(ARGV)
  rescue
    puts p
    exit(1)
  end
end

if daemon
  require 'daemonize'
  include Daemonize
  daemonize
end

ssl_cert_base = base_path + '/certs/host' if ssl_cert_base == nil
template_path = base_path + '/lib/templates/surnames' 

$logger = Logger.new( STDERR )
$logger.level = log_level
$access_log = Logger.new( log_file ? log_file : STDERR )
$access_log.level = log_level


$sedna = Sedna.connect({:database=>database})

if $sedna.query('fn:doc("$collections")//collection[@name="admininfo"]').size == 0
  puts 'Can\'t open admin database. Run debinit.sh first'
  exit
end


info = AdminDict.new('admininfo', db_path)
info.xslt_path = xslt_path
info.service_name = 'surnames'
info.template_path = template_path


server = create_http_server({
  :auth         => auth, 
  :dbpasswd => info,
  :loadperms => true,
  :port         => port,
  :ssl          => ssl,
  :ssl_cert_base => ssl_cert_base,
  :ssl_ca_cert => ssl_ca_cert,
  :logger       => $logger,
  :access_log   => $access_log
})
server.info = info
info.remount(server, 'surnames')

server.mount_proc('/') {|req, resp| 
  resp.set_redirect(WEBrick::HTTPStatus::SeeOther, 'https://deb.fanuk.net/res1881') if req.host() == 'deb.fanuk.net'
  resp.set_redirect(WEBrick::HTTPStatus::SeeOther, 'http://www.pdev.org.uk') if req.host() == 'www.pdev.org.uk' or req.host() == 'pdev.org.uk'

}

server.mount "/hello",  HelloServlet
server.mount "/files",  WEBrick::HTTPServlet::FileHandler, base_path + '/files/surnames', {:FancyIndexing=>true}

trap("TERM"){
  $stderr.puts "GOT TERM"
  server_shutdown(server, info)
}

begin
  server.start
rescue => e
  $logger.error( "exception #{e.class}: #{e.message}\n" + e.backtrace.join("\n\t\t") )
  raise
end
