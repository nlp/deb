#!/usr/bin/ruby1.8

require 'optparse'
require 'logger'

$LOAD_PATH.unshift('../lib/')

class Object
  def edump
    $stderr.puts('edump: ' + self.to_s)
    return self
  end
end

# bezne distribucni knihovny
require 'optparse'
require 'logger'
require 'rexml/document'
require 'socket'
require 'cgi'
require 'dbi'
require 'openssl'
#rubygems a xml builder
require 'rubygems'
require 'builder'

# prelozene/upravene
require 'xml/libxml'
require 'libxslt'
require 'sedna'

# soucasti DEBu
require 'deb_common'
require 'http_server'
require 'dbpasswd'

require 'servlets/dict'
require 'servlets/cpa-sql'
require 'servlets/cpa-public'
require 'servlets/htmlproxy'
require 'servlets/hello'

require 'dict/dict-sedna'
require 'dict/admin'
require 'dict/cpa-sql'

require 'dbconf-cpasp'
require 'ske_links'

server = nil

port      = 8013
daemon    = nil
log_level = Logger::DEBUG
log_file  = nil
db_path   = nil
xslt_path = nil
base_path = '/var/lib/deb-server'
ssl       = true
auth      = true
ssl_ca_cert = nil
database = 'deb'

debug_levels = {
  :debug => Logger::DEBUG,
  :info  => Logger::INFO,
  :warn  => Logger::WARN,
  :error => Logger::ERROR,
  :fatal => Logger::FATAL,
}

ARGV.options do |p|
  p.banner = "usage: #{$0} [options]\n"
  p.on('-p','--port=NUMBER',    String, 'cislo portu')      { |p| port=p.to_i }
  p.on('--db-path=PATH',        String, 'cesta k databazim') { |db_path| }
  p.on('--xslt-path=PATH',      String, 'cesta k sablobnam') { |xslt_path| }
  p.on('--base-path=PATH',      String, 'cesta k zakladnimu adresari') { |base_path| }
  p.on('-d','--daemon',                 'daemonizovat') { |d| daemon = true }
  p.on('-s','--ssl',			'use SSL') { |s| ssl = true }
  p.on(     '--ssl-cert=STRING',String, 'ssl cert/key base') { |ssl_cert_base| }
  p.on(     '--ssl-ca-cert=STRING',String, 'ssl CA certificate') { |ssl_ca_cert| }
  p.on(     '--auth',			'use test auth') { |a| auth = true }
  p.on(     '--htpasswd=FILE',	String,	'use htpasswd auth') { |htpasswd| }
  p.on(     '--text-log=FILE',  String, 'text log file') { |text_log| }
  p.on('-v','--verbose=LEVEL',  String, 'log level') do |log_level| 

    symb = log_level.downcase.intern
    if debug_levels.has_key?(symb)
      log_level = debug_levels[symb]
    else
      puts p
      puts "Invalid log level #{log_level}"
      exit(1)
    end
  end
  p.on('-l','--logfile=FILE','log file') { |log_file| }

  begin
    p.parse!(ARGV)
  rescue
    puts p
    exit(1)
  end
end

if daemon
  require 'daemonize'
  include Daemonize
  daemonize
end

ssl_cert_base = base_path + '/certs/host' if ssl_cert_base == nil

$logger = Logger.new( STDERR )
$logger.level = log_level
$access_log = Logger.new( log_file ? log_file : STDERR )
$access_log.level = log_level
$sedna = Sedna.connect({:database=>database})

if $sedna.query('fn:doc("$collections")//collection[@name="admininfo"]').size == 0
  puts 'Can\'t open admin database. Run debinit.sh first'
  exit
end


info = AdminDict.new('admininfo', db_path)
info.xslt_path = xslt_path
info.service_name = 'cpasp'

server = create_http_server({
  :auth         => auth, 
  :dbpasswd => info,
  :loadperms => true,
  :port         => port,
  :ssl          => ssl,
  :ssl_cert_base => ssl_cert_base,
  :ssl_ca_cert => ssl_ca_cert,
  :logger       => $logger,
  :access_log   => $access_log
})
server.info = info

cpa = CPASQL.new(info.service_name)
cpa.xslt_sheets['text'] = xslt_path + 'cpa_text.xslt'
cpa.xslt_sheets['csv'] = xslt_path + 'cpa-csv.xslt'
cpa.xslt_sheets['rdf'] = xslt_path + 'cpa-rdf.xslt'
cpa.xslt_sheets['rdfgroup'] = xslt_path + 'cpa-rdfgroup.xslt'
cpa.xslt_sheets['onto'] = xslt_path + 'cpa-onto.xslt'
cpa.xslt_sheets['diff'] = xslt_path + 'cpa-diff.xslt'
cpa.xslt_sheets['public-main'] = xslt_path + 'cpa-publicmain.xslt'
cpa.xslt_sheets['public-verb'] = xslt_path + 'cpa-publicverb.xslt'
cpa.xslt_sheets['public-pat'] = xslt_path + 'cpa-publicpat.xslt'
cpa.xslt_sheets['verbprev'] = xslt_path + 'cpa-verbprev.xslt'
cpa.xslt_sheets['verbjson'] = xslt_path + 'cpa-verbjson.xslt'
cpa.xslt_sheets['ontojson'] = xslt_path + 'cpa-ontojson.xslt'
cpa.corpora = 'itwac3.nodups.fulldocs.7'
cpa.main_freq_corp = 'itwac3.nodups.fulldocs.7'


server.mount "/hello",  HelloServlet
server.mount "/doc",  CPASQLServlet,  cpa, info
server.mount "/public",  CPAPublicServlet,  cpa, info
server.mount "/files",  WEBrick::HTTPServlet::FileHandler, base_path + '/files/cpa'

server.mount_proc('/') {|req, resp| 
  resp.set_redirect(WEBrick::HTTPStatus::MovedPermanently, '/public')
}

trap("TERM"){
  $stderr.puts "GOT TERM"
  server_shutdown(server, info)
}
trap("KILL"){
  $stderr.puts "GOT KILL"
  server_shutdown(server, info)
}

server.start

