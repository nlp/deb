#!/bin/bash
groupadd deb
adduser --system --no-create-home --shell /bin/bash --ingroup deb debsrv
mkdir /etc/deb-server/
mkdir /var/log/deb-server/
chown -R debsrv:deb /var/lib/deb-server /var/log/deb-server
chmod -R ug+rw /var/lib/deb-server /var/log/deb-server
chmod -R g+s /var/lib/deb-server /var/log/deb-server
cp /var/lib/deb-server/config/init.d/debserver-common /etc/init.d/
cp /var/lib/deb-server/config/default/debserver-common /etc/default/
cp /var/lib/deb-server/config/services.conf /etc/deb-server/
cp /var/lib/deb-server/config/backup.conf /etc/deb-server/
cp /var/lib/deb-server/config/sednaconf.xml /opt/sedna/etc/
cp /var/lib/deb-server/config/debserver.service /usr/lib/systemd/system/debserver.service
/var/lib/deb-server/bin/debinit.sh
/var/lib/deb-server/bin/initservice.sh debvisdic
systemctl enable debserver.service
