require 'dict/dict-sedna'

class DebvisdicAdmin < AdminDict
  #get the list of dictionary packages
  #user: user login, if provided returs only dicts. allowed for the user
  #      with his permissions
  #old_api: use old api
  def list_packages(user='', old_api = false)
    res = {}
    userdicts = user_perms(user)
   
    list_dicts(@service_name).each {|code,dname|
      xml = get('dict'+code).to_s
      #$stderr.puts xml
      doc = load_xml_string(xml.to_s)
      key = doc.find('/dict/key').to_a.first.content.to_s.gsub!('/', '.')[1..-1]
      eqtags = {}
      doc.find('/dict/eqtags/eqtag').each {|el|
        tag = el.content.to_s
        dics = el['dics'].to_s.split(',')
        dics.map!{|x| x.strip}
        eqtags[tag] = {'tag'=>tag, 'dics'=>dics, 'name'=>el['name'].to_s, 'search_element'=>el['elem'].to_s}
      }


      lookup = []
      doc.find('/dict/lookup').each{|el|
        lookup = el.content.to_s.split(',') if el.content.to_s != nil
      }
      lookup.map!{|x| x.strip}
      reload = []
      doc.find('/dict/reload').each{|el|
        reload = el.content.to_s.split(',') if el.content.to_s != nil
      }
      reload.map!{|x| x.strip}

      user_queries = []
      user_queries = @array[code].user_queries if @array[code].respond_to?('user_queries')

      doc.find('/dict/packages/package').each{|el|
        id = el.find('code').to_a.first.content.to_s
        name = el.find('name').to_a.first.content.to_s
        lookup_merge = lookup#[id]+lookup
        lookup_merge.uniq!
        if old_api
          if user == ''
            res[id] = {'id'=>id, 'dict'=>code, 'nazov'=>name, 'prava'=>'w', 'key'=>key, 'eqtags'=>eqtags, 'dicslookup'=>lookup_merge, 'dicsreload'=>reload, 'indexes'=>[], 'user_queries' => user_queries}
          else
            if userdicts[code] != nil
              res[id] = {'id'=>id, 'dict'=>code, 'nazov'=>name, 'prava'=>userdicts[code], 'key'=>key, 'eqtags'=>eqtags, 'dicslookup'=>lookup_merge, 'dicsreload'=>reload, 'indexes'=>[], 'user_queries' => user_queries}
            end
          end
        else
          if user == ''
            res[id] = {'id'=>id, 'code'=>code, 'name'=>name, 'access'=>'w', 'key'=>key, 'eqtags'=>eqtags, 'dicslookup'=>lookup_merge, 'dicsreload'=>reload, 'indexes'=>[], 'user_queries' => user_queries}
          else
            if userdicts[code] != nil
              res[id] = {'id'=>id, 'code'=>code, 'name'=>name, 'access'=>userdicts[code], 'key'=>key, 'eqtags'=>eqtags, 'dicslookup'=>lookup_merge, 'dicsreload'=>reload, 'indexes'=>[], 'user_queries' => user_queries}
            end
          end
        end
      }
    }
    $stderr.puts res.to_s
    return res
  end

  def user_perms(user)
    userdicts = {}
    if user != ''
      xml = get('user'+user)
      #$stderr.puts xml
      doc = load_xml_string(xml.to_s)
      doc.find("/user/services/service[@code='#{@service_name}']/dict").each {|el|
        #$stderr.puts '*' + el['code'] + ':' + el['perm'].to_s
        userdicts[el['code'].to_s] = el['perm'].to_s
      }
    end
    return userdicts
  end

  def check_perm(user, dict, perm=nil)
    $stderr.puts "check " + user + "-" + dict + "-" + perm.to_s
    hash = user_perms(user)
    #$stderr.puts hash
    if (perm != nil and hash[dict] == perm) or (perm == nil and hash[dict] != nil and hash[dict] != '')
      return true
    else
      return false
    end
  end
  def get_user_dicts(user)
    dicts = {'dict_m'=>[], 'dict_w'=>[], 'dict_r'=>[], 'dict_a'=>[]}
    codes = []
    xquery_to_hash('[dict[starts-with(code,"'+user+'_wn_")]]').each {|id,xml|
      doc = load_xml_string(xml)
      name = ''
      code = doc.find('//dict/code').first.content.to_s
      name = doc.find('//dict/name').first.content.to_s unless doc.find('//dict/name').first.nil?
      dicts['dict_m'] << {'id'=>id, 'code'=>code, 'name'=>name}
      codes << code
    }
    userxml = get('user'+user)
    userdoc = load_xml_string(userxml)
    userdoc.find('/user/services/service[@code="'+@service_name+'"]/dict[not(starts-with(@code,"'+user+'_wn_"))]').each{|d|
      id = 'dict'+d['code']
      doc = load_xml_string(get(id))
      name = ''
      code = doc.find('//dict/code').first.content.to_s
      name = doc.find('//dict/name').first.content.to_s unless doc.find('//dict/name').first.nil?
      d['perm'] = 'r' if d['perm'] == 'on'
      dicts['dict_'+d['perm']] << {'id'=>id, 'code'=>code, 'name'=>name}
      codes << code
    }
    if not userdoc.find('/user/admin').first.nil? and userdoc.find('/user/admin').first.content == 'true'
      xml = get('service'+@service_name)
      doc = load_xml_string(xml)
      doc.find('/service/dicts/dict').each{|d|
        dc = d['code']
        unless codes.include?(dc)
          id = 'dict'+dc
          xmld = get(id)
          docd = load_xml_string(xmld)
          name = ''
          code = docd.find('//dict/code').first.content.to_s
          name = docd.find('//dict/name').first.content.to_s unless docd.find('//dict/name').first.nil?
          dicts['dict_a'] << {'id'=>id, 'code'=>code, 'name'=>name}
        end
      }
    end
    return dicts
  end

  def get_dict_users(dictcode)
    users = []
    xquery_to_hash('[user[services/service[@code="'+@service_name+'"]/dict[@code="'+dictcode+'"]]]').each{|id,xml|
      doc = load_xml_string(xml)
      name = ''
      email = ''
      login = doc.find('/user/login').first.content.to_s
      name = doc.find('/user/name').first.content.to_s unless doc.find('/user/name').first.nil?
      email = doc.find('/user/email').first.content.to_s unless doc.find('/user/email').first.nil?
      perm = doc.find('/user/services/service[@code="'+@service_name+'"]/dict[@code="'+dictcode+'"]').first['perm']
      users << {'login'=>login, 'name'=>name, 'perm'=>perm, 'email'=>email}
    }
    return users
  end

  def get_dict_info(dictcode)
    xml = get('dict'+dictcode)
    doc = load_xml_string(xml)
    name = ''
    schema = ''
    code = doc.find('//dict/code').first.content.to_s
    name = doc.find('//dict/name').first.content.to_s unless doc.find('//dict/name').first.nil?
    schema = doc.find('//dict/schema').first.content.to_s unless doc.find('//dict/schema').first.nil?
    dict_info = {'dict_code'=>code,'dict_name'=>name, 'schema'=>schema}
    return dict_info
  end

  def add_dict_to_service(dictcode)
    xml = get('service'+@service_name)
    doc = load_xml_string(xml)
    if doc.find('/service/dicts/dict[@code="'+dictcode+'"]').size == 0
      newdict = XML::Node.new('dict')
      newdict['code'] = dictcode
      doc.find('/service/dicts').first << newdict
      update('service'+@service_name, doc.to_s)
    end
  end

  def remove_dict_from_service(dictcode)
    xml = get('service'+@service_name)
    doc = load_xml_string(xml)
    if doc.find('/service/dicts/dict[@code="'+dictcode+'"]').size > 0
      doc.find('/service/dicts/dict[@code="'+dictcode+'"]').each{|d|
        d.remove!
      }
      update('service'+@service_name, doc.to_s)
    end
  end

  def add_user_to_dict(user, dictcode, type='r')
    xml = get('user'+user)
    doc = load_xml_string(xml)
    doc.find('/user/services/service[@code="'+@service_name+'"]/dict[@code="'+dictcode+'"]').each{|d|
      d.remove!
    }
    doc.root << XML::Node.new('services') if doc.find('/user/services').size == 0
    if doc.find('/user/services/service[@code="'+@service_name+'"]').size == 0
      nservice = XML::Node.new('service')
      nservice['code'] = @service_name
      doc.find('/user/services').first << nservice
    end
    ndict = XML::Node.new('dict')
    ndict['code'] = dictcode
    ndict['perm'] = type
    doc.find('/user/services/service[@code="'+@service_name+'"]').first << ndict
    update('user'+user, doc.to_s)
  end

  def remove_user_from_dict(user, dictcode)
    xml = get('user'+user)
    doc = load_xml_string(xml)
    doc.find('/user/services/service[@code="'+@service_name+'"]/dict[@code="'+dictcode+'"]').each{|d|
      d.remove!
    }
    update('user'+user, doc.to_s)
  end

  def remove_all_users_from_dict(dictcode)
    xquery_to_hash('[user[services/service[@code="'+@service_name+'"]/dict[@code="'+dictcode+'"]]]').each{|id,xml|
      $stderr.puts id
      doc = load_xml_string(xml)
      doc.find('/user/services/service[@code="'+@service_name+'"]/dict[@code="'+dictcode+'"]').each{|d|
        d.remove!
      }
      update(id, doc.to_s)
    }
  end

  def template_save(dictcode, name, code, template, xslt_path)
    fxsl = File.new(xslt_path+'/'+dictcode+'-'+code+'.xsl', 'w')
    fxsl.puts(template)
    fxsl.close
    xml = get('dict'+dictcode)
    doc = load_xml_string(xml)
    doc.find('/dict/xslts/xslt[file="'+dictcode+'-'+code+'.xsl"]').each{|d|
      d.remove!
    }
    nxslt = XML::Node.new('xslt')
    nxslt << XML::Node.new('name', code)
    nxslt << XML::Node.new('label', name)
    nxslt << XML::Node.new('file', dictcode+'-'+code+'.xsl')
    doc.root << XML::Node.new('xslts') if doc.find('/dict/xslts').first.nil?
    doc.find('/dict/xslts').first << nxslt
    update('dict'+dictcode, doc.to_s)
  end

  def template_remove(dictcode, code, xslt_path)
    File.delete(xslt_path+'/'+dictcode+'-'+code+'.xsl')
    xml = get('dict'+dictcode)
    doc = load_xml_string(xml)
    doc.find('/dict/xslts/xslt[name="'+code+'"]').each{|d|
      d.remove!
    }
    update('dict'+dictcode, doc.to_s)
  end


  def get_dict_templates(dictcode, xslt_path)
    templates = []
    xml = get('dict'+dictcode)
    doc = load_xml_string(xml)
    doc.find('/dict/xslts/xslt').each{|xsl|
      xslt = {'code'=>xsl.find('name').first.content}
      xslt['name'] = xsl.find('label').first.content unless xsl.find('label').first.nil?
      file = xsl.find('file').first.content.to_s
      xslt['template'] = File.read(xslt_path+'/'+file)
      templates << xslt
    }
    return templates
  end

  def copy_templates(dictcode, xml)
    oldxml = get('dict'+dictcode)
    if oldxml != ''
      olddoc = load_xml_string(oldxml)
      doc = load_xml_string(xml)
      doc.root << olddoc.find('//xslts').first.copy(true) unless olddoc.find('//xslts').first.nil?
      xml = doc.to_s
    end
    return xml
  end
end
