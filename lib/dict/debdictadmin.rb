require 'dict/dict-sedna'

class DebdictAdmin < AdminDict
  #def initialize(db_path, database, key_path='', env=nil)
  #  super
  #  @perm_array = get_perms()
  #end
  
  #get the list of dictionary packages
  #user: user login, if provided returs only dicts. allowed for the user
  #      with his permissions
  def list_dictionaries(user='')
    res = []
    userdicts = {}
    if user != ''
      xml = get('user'+user)
      $stderr.puts xml
      doc = load_xml_string(xml.to_s)
      doc.find("/user/services/service[@code='#{@service_name}']/dict").each {|el|
        #$stderr.puts '*'+el['code'].to_s
        userdicts[el['code'].to_s] = el['perm'].to_s
      }
    end
   
    list_dicts(@service_name).each {|code,dname|
      if user == ''
        res << code
      else
        if userdicts[code] != nil
          res << code
        end
      end
    }
    $stderr.puts res.to_s
    return res
  end

  def check_perm(user, dict)
    array = list_dictionaries(user)
    if array.include?(dict)
      return true
    else
      return false
    end
  end

  def list_packages
    return {}
  end

end
