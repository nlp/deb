require 'dict/dict-sedna'
require 'dict/wn2'

class CdbSynset < WordNet2
  def initialize( db_path, database, key_path='', env = nil, admindict = nil )
    super
    @servlet = CdbSynServlet
    @id_regexp = /([cdrn]_([avn]-)?[0-9]+)|(eng-30.*)/ #/[cdrn]_([avn]-)?[0-9]+/ #REGEXP FOR SYNSET ID!

    @user_queries = [
      [['select CDB-SS with wordnet-domain', 'X'], ['cdb_synset.wn_domains.dom_relation.@term=', 'X']],
      [['select CDB-SS with relation', 'X'], ['cdb_synset.wn_internal_relations.relation.@relation_name=', 'X']]
    ]

    @opposite_relations = {
      'HAS_XPOS_HYPERONYM'=>'HAS_XPOS_HYPONYM',
      'NEAR_SYNONYM'=>'NEAR_SYNONYM',
      'XPOS_NEAR_SYNONYM'=>'XPOS_NEAR_SYNONYM',
      'NEAR_ANTONYM'=>'NEAR_ANTONYM',
      'XPOS_NEAR_ANTONYM'=>'XPOS_NEAR_ANTONYM',
      'HAS_HOLONYM'=>'HAS_MERONYM',
      'HAS_HOLO_LOCATION'=>'HAS_MERO_LOCATION',
      'HAS_HOLO_MADEOF'=>'HAS_MERO_MADEOF',
      'HAS_HOLO_MEMBER'=>'HAS_MERO_MEMBER',
      'HAS_HOLO_PART'=>'HAS_MERO_PART',
      'HAS_HOLO_PORTION'=>'HAS_MERO_PORTION',
      'INVOLVED'=>'ROLE',
      'INVOLVED_AGENT'=>'ROLE_AGENT',
      'INVOLVED_PATIENT'=>'ROLE_PATIENT',
      'INVOLVED_INSTRUMENT'=>'ROLE_INSTRUMENT',
      'INVOLVED_LOCATION'=>'ROLE_LOCATION',
      'INVOLVED_RESULT'=>'ROLE_RESULT',
      'INVOLVED_DIRECTION'=>'ROLE_DIRECTION',
      'INVOLVED_SOURCE_DIRECTION'=>'ROLE_SOURCE_DIRECTION',
      'INVOLVED_TARGET_DIRECTION'=>'ROLE_TARGET_DIRECTION',
      'CO_ROLE'=>'CO_ROLE',
      'CO_AGENT_PATIENT'=>'CO_PATIENT_AGENT',
      'CO_AGENT_INSTRUMENT'=>'CO_INSTRUMENT_AGENT',
      'CO_AGENT_RESULT'=>'CO_RESULT_AGENT',
      'CO_PATIENT_INSTRUMENT'=>'CO_INSTRUMENT_PATIENT',
      'CO_PATIENT_RESULT'=>'CO_RESULT_PATIENT',
      'CO_INSTRUMENT_RESULT'=>'CO_RESULT_INSTRUMENT',
      'CAUSES'=>'IS_CAUSED_BY',
      'HAS_SUBEVENT'=>'IS_SUBEVENT_OF',
      'MANNER_OF'=>'IN_MANNER',
      'BE_IN_STATE'=>'STATE_OF',
      'FUZZYNYM'=>'FUZZYNYM',
      'XPOS_FUZZYNYM'=>'XPOS_FUZZYNYM',
      #and reversed
      'HAS_XPOS_HYPONYM'=>'HAS_XPOS_HYPERONYM',
      'HAS_MERONYM'=>'HAS_HOLONYM',
      'HAS_MERO_LOCATION'=>'HAS_HOLO_LOCATION',
      'HAS_MERO_MADEOF'=>'HAS_HOLO_MADEOF',
      'HAS_MERO_MEMBER'=>'HAS_HOLO_MEMBER',
      'HAS_MERO_PART'=>'HAS_HOLO_PART',
      'HAS_MERO_PORTION'=>'HAS_HOLO_PORTION',
      'ROLE'=>'INVOLVED',
      'ROLE_AGENT'=>'INVOLVED_AGENT',
      'ROLE_PATIENT'=>'INVOLVED_PATIENT',
      'ROLE_INSTRUMENT'=>'INVOLVED_INSTRUMENT',
      'ROLE_LOCATION'=>'INVOLVED_LOCATION',
      'ROLE_RESULT'=>'INVOLVED_RESULT',
      'ROLE_DIRECTION'=>'INVOLVED_DIRECTION',
      'ROLE_SOURCE_DIRECTION'=>'INVOLVED_SOURCE_DIRECTION',
      'ROLE_TARGET_DIRECTION'=>'INVOLVED_TARGET_DIRECTION',
      'CO_PATIENT_AGENT'=>'CO_AGENT_PATIENT',
      'CO_INSTRUMENT_AGENT'=>'CO_AGENT_INSTRUMENT',
      'CO_RESULT_AGENT'=>'CO_AGENT_RESULT',
      'CO_INSTRUMENT_PATIENT'=>'CO_PATIENT_INSTRUMENT',
      'CO_RESULT_PATIENT'=>'CO_PATIENT_RESULT',
      'CO_RESULT_INSTRUMENT'=>'CO_INSTRUMENT_RESULT',
      'IS_CAUSED_BY'=>'CAUSES',
      'IS_SUBEVENT_OF'=>'HAS_SUBEVENT',
      'IN_MANNER'=>'MANNER_OF',
      'STATE_OF'=>'BE_IN_STATE'
    }
  end
 
  def get_literals(id, array, move_word = nil)
    #$stderr.puts " get_literals(#{id}) start "+time_stamp()
    xml = get_one(id)
    doc = REXML::Document.new( xml.to_s )
    lits = []
    pos = ''
    REXML::XPath.each( doc, '/cdb_synset/synonyms/synonym' ) {|syn| 
      lu_lit = array[@dict_prefix+'cdb_lu'].get_literal(syn.attributes['c_lu_id'].to_s)
      lit = lu_lit['form'] + ':' + lu_lit['seq_nr']
      lit += ':'+syn.attributes['subsense'].to_s if syn.attributes['subsense'].to_s != ''
      if lu_lit['form'] == move_word
        lits.unshift(lit)
      else
        lits << lit
      end
      pos = lu_lit['pos'] if lu_lit['pos'].to_s != ''
    }
    #$stderr.puts " get_literals end "+time_stamp()
    return lits, pos
  end
 
  def get_list(query, array)
    $stderr.puts "cdb_syn:get_list start "+time_stamp()
    synsets = {}
    db_query = ''
    query.gsub!(/&(?!(\w+|#\d+);)/, '&amp;')
    query.gsub!(/'/, '&quot;')
    one_word = nil
    
    top = @key_path.split('/')[1]
    rest = @key_path.split('/')[2..-1].join('/') 
    if query[0,top.length] == top or query.include?"="
      search_string = parse_conditions(query, array)
      #db_query = "for $x in collection('#{@collection}')/#{top} where #{search_string} return $x"
      db_query = "for $x in collection('#{@collection}') where #{search_string} return document-uri($x)"
    elsif query =~ @id_regexp
      db_query = "for $x in collection('#{@collection}') where $x/#{top}[#{rest}='#{query}'] return document-uri($x)"
      #db_query = "collection('#{@collection}')/#{top}[#{rest}='#{query}']"
    else
      lus = array[@dict_prefix+'cdb_lu'].get_list(query)
      one_word = query.split(':')[0] if query.index('*') == nil
      qu_ar = []
      lus.each{ |luid,ar|
        qu_ar << "$p='#{luid}'" if luid.to_s != ''
      }
      if qu_ar.size == 0
        return synsets
      end
      #db_query = "for $x in collection('#{@collection}')/cdb_synset where some $p in $x/synonyms/synonym/@c_lu_id satisfies (" + qu_ar.join(' or ') + ") return $x"
      db_query = "for $x in collection('#{@collection}') where some $p in $x/cdb_synset/synonyms/synonym/@c_lu_id satisfies (" + qu_ar.join(' or ') + ") return document-uri($x)"
    end
    $stderr.puts db_query
    $stderr.puts "QUERY START " + time_stamp()
    count = 0
    query(db_query).each{|id|
      count += 1
      s = get(id)
      $stderr.puts '  '+count.to_s + ' - '+ time_stamp() + ' entry '+id
      synsets[id] = {}
      synsets[id]['literals'], synsets[id]['pos'] = get_literals(id, array, one_word)
      #res = follow_redundant(s.to_s, array['cdb_id'], array['cdb_red'])
      res = strip_redundant(s.to_s, true)
      doc = load_xml_string(res.to_s)
      synsets[id]['def'] = doc.root.find('definition').first.content.to_s unless doc.find('definition').first.nil?
      doms = []
      doc.root.find('/cdb_synset/vlis_domains/dom_relation').each{|el|
        doms << el['term'].to_s
      }
      synsets[id]['domain'] = doms.join(', ')
      lockuser = @admindict.who_locked(@dict_prefix+'cdb_syn', id)
      synsets[id]['lockstatus'] = lockuser
      break if count > @max_result
    }
    $stderr.puts "QUERY END " + time_stamp()

    synsets
  end

  def follow_redundant(xml, reddb)
    #if not @admindict.check_perm(Thread.current[:auth_user], 'cdb_red')
    #  return xml
    #end
    doc = REXML::Document.new(xml)
    c_sy_id = doc.root.attributes['c_sy_id'].to_s
    if c_sy_id != '' and not reddb.nil?
      query = "/dwn_entry[@c_sy_id='#{c_sy_id}' and @central='true']"
      $stderr.puts "REDUNDANT query "+query
      redlist = reddb.xquery_to_hash(query)
      redlist.each {|key, val|
        reddoc = REXML::Document.new(val.to_s)
        doc.root.elements['definition'] = reddoc.root.elements['definition']
        doc.root.elements['differentiae'] = reddoc.root.elements['differentiae']
        doc.root.elements['vlis_domains'] = reddoc.root.elements['vlis_domains']
        doc.root.attributes['posSpecific'] = reddoc.root.attributes['posSpecific']
        return doc.to_s
      }
    end
    doc.to_s
  end
 
  def follow_previewtext(xml, array, only_internal=false)
    doc = REXML::Document.new(xml)
    #follow relations
    REXML::XPath.each(doc, '/cdb_synset/wn_internal_relations/relation[@target]') { |el|
      target = el.attributes['target'].to_s
      lits, pos = get_literals(target, array)
      el.attributes['target-previewtext'] = lits.join(', ')
    }
    if not only_internal
      #follow synonyms
      REXML::XPath.each(doc, '/cdb_synset/synonyms/synonym') { |el|
        lu_id = el.attributes['c_lu_id'].to_s
        begin
          lulit = array[@dict_prefix+'cdb_lu'].get_literal(lu_id)
          el.attributes['c_lu_id-previewtext'] = lulit['form'] + ':' + lulit['seq_nr']
        rescue => e
          $stderr.puts 'Synonym LU not found! ' + e
        end
      }
      #follow wn relations
      if array['wnen'] != nil
        REXML::XPath.each(doc, '/cdb_synset/wn_equivalence_relations/relation') { |el|
          wnlit = array['wnen'].get_literals(el.attributes['target20'])
          el.attributes['target20-previewtext'] = wnlit
        }
      end
    end
    return doc.to_s
  end

  def delete_previewtext(data_hash)
    if data_hash['cdb_synset']['synonyms'] != nil and data_hash['cdb_synset']['synonyms']['synonym'] != nil
      data_hash['cdb_synset']['synonyms']['synonym'].each {|rel|
        rel.delete('@c_lu_id-previewtext')
      }
    end
    if data_hash['cdb_synset']['wn_internal_relations'] != nil and data_hash['cdb_synset']['wn_internal_relations']['relation'] != nil
      data_hash['cdb_synset']['wn_internal_relations']['relation'].each {|rel|
        rel.delete('@target-previewtext')
      }
    end
    if data_hash['cdb_synset']['wn_equivalence_relations'] != nil and data_hash['cdb_synset']['wn_equivalence_relations']['relation'] != nil
      data_hash['cdb_synset']['wn_equivalence_relations']['relation'].each {|rel|
        rel.delete('@target20-previewtext')
      }
    end
    return data_hash
  end

  #get reverse relations (compute HAS_HYPONYM from HAS_HYPERNYM)
  def get_rilr(xmldoc, array)
    res = ''
    doc = REXML::Document.new( xmldoc )
    id = doc.root.attributes['c_sy_id'].to_s
    $stderr.puts "for $x in collection('#{@collection}')/cdb_synset where some $p in $x/wn_internal_relations/relation satisfies ($p/@target='#{id}' and $p/upper-case(@relation_name)='HAS_HYPERONYM') return $x"
    @sedna.query("for $x in collection('#{@collection}')/cdb_synset where some $p in $x/wn_internal_relations/relation satisfies ($p/@target='#{id}' and $p/upper-case(@relation_name)='HAS_HYPERONYM') return $x").each{|s|
      doc2 = REXML::Document.new( s.to_s )
      #$stderr.puts id
      ilr = doc2.root.attributes['c_sy_id'].to_s
      #$stderr.puts ilr
      el2 = doc.root.elements['wn_internal_relations'].add_element "relation"
      el2.attributes['previewgenerated'] = 'true'
      el2.attributes['target'] = ilr
      el2.attributes['relation_name'] = 'HAS_HYPONYM'
      lits, pos = get_literals(ilr, array)
      el2.attributes['target-previewtext'] = lits.join(', ')
    }
    doc.write(res, 1)
    return res
  end
  #save HAS_HYPONYM relation to hyperonyms of entry
  #plus update previewtext for all relations
  def save_rilr_links(xml, array)
    res = ''
    doc = REXML::Document.new(xml)
    id = doc.root.attributes['c_sy_id'].to_s
    #orig. entry previewtext
    prev_ar = []
    doc.elements.to_a('/cdb_synset/synonyms/synonym').each{|el|
      prev_ar << el.attributes['c_lu_id-previewtext']
    }
    preview = prev_ar.join(', ')

    #update rilr
    doc.elements.to_a('/cdb_synset/wn_internal_relations/relation[@relation_name="HAS_HYPERONYM"]').each{|el|
      xml2 = get_one(el.attributes['target'])
      if xml2 != ''
        doc2 = REXML::Document.new(xml2.to_s)
        id2 = doc2.root.attributes['c_sy_id'].to_s
        doc2.elements.delete_all('/cdb_synset/wn_internal_relations/relation[@relation_name="HAS_HYPONYM" and @target="'+id.to_s+'"]')
        el2 = doc2.root.elements['wn_internal_relations'].add_element "relation"
        el2.attributes['previewgenerated'] = 'true'
        el2.attributes['target'] = id
        el2.attributes['target-previewtext'] = preview
        el2.attributes['relation_name'] = 'HAS_HYPONYM'
        update(id2, doc2.to_s, false)
        $stderr.puts "SAVE HYPERNYM as hyponym in #{id2}"
      end
    }
    
    #update preview for links to this entry
    @sedna.query("for $x in collection('#{@collection}')/cdb_synset where some $p in $x/wn_internal_relations/relation satisfies ($p/@target='#{id}' and $p/@relation_name!='HAS_HYPONYM') return $x") { |s|
      doc2 = REXML::Document.new( s.to_s )
      id2 = doc2.root.attributes['c_sy_id'].to_s
      doc2.elements.to_a("/cdb_synset/wn_internal_relations/relation[@relation_name!='HAS_HYPONYM' and @target='#{id}']").each{|el|
        el.attributes['target-previewtext'] = preview
      }
      update(id2, doc2.to_s, false)
      $stderr.puts "SAVE PREVIEW for links in #{id2}"
    }

    #add opposite relations if not existing
    doc.elements.to_a('/cdb_synset/wn_internal_relations/relation[@relation_name!="HAS_HYPERONYM"]').each{|el|
      orig_rel = el.attributes['relation_name']
      opposite_rel = @opposite_relations[orig_rel]
      xml2 = get_one(el.attributes['target'])
      if xml2 != ''
        doc2 = REXML::Document.new(xml2.to_s)
        id2 = doc2.root.attributes['c_sy_id'].to_s
        if doc2.elements["/cdb_synset/wn_internal_relations/relation[@relation_name='#{opposite_rel}' and @target='#{id}']"] == nil
          $stderr.puts "  Adding link from #{id2}: #{opposite_rel} -> #{id}:#{orig_rel}"
          el2 = doc2.root.elements['wn_internal_relations'].add_element "relation"
          el2.attributes['target'] = id
          el2.attributes['relation_name'] = opposite_rel
          el2.attributes['target-previewtext'] = preview
          el2.attributes['reversed'] = 'true'
          el2.attributes['generated'] = 'true'
          update(id2, doc2.to_s, false)
        end
      end
    }    
    
    doc.write(res, 1)
    return res
  end
  
  def get_tree(xmldoc, dictarray, dir = 'HAS_HYPERONYM')
    res = []
    previous = ''
    begin
      $stderr.puts dir
      $stderr.puts previous
      ar = get_up_tree(xmldoc, dictarray, dir)
      if previous == ar[0]
        ar[0] = 'koren'
      else
        previous = ar[0]
      end
      res << ar
      $stderr.puts ar[0]
      if ar[0] != 'koren'
        xmldoc = get_one(ar[0]).to_s.gsub('has_hyperonym', 'HAS_HYPERONYM')
        if dir == 'HAS_HYPERONYM'
          #xmldoc = get_rilr(xmldoc, dictarray)
        end
        if xmldoc == ''
          ar[0] = 'koren'
        end
      end
    end while ar[0] != 'koren'
    return res
  end
  
  def get_up_tree(xmldoc, dictarray, dir = 'HAS_HYPERONYM')
    $stderr.puts 'uptree'
    doc = REXML::Document.new( xmldoc )
    upilr = REXML::XPath.first( doc, "/cdb_synset/wn_internal_relations/relation[@relation_name='#{dir}']/@target").to_s
    if upilr == ''
      upilr = 'koren'
      disj = ''
    else
      disj = REXML::XPath.first( doc, "/cdb_synset/wn_internal_relations/relation[@relation_name='#{dir}']/@disjunctive").to_s
    end
    
    id = REXML::XPath.first( doc, '/cdb_synset/@c_sy_id').to_s
    count_hyper = doc.elements.to_a("/cdb_synset/wn_internal_relations/relation[@relation_name='HAS_HYPERONYM']").size.to_i
    count_child = doc.elements.to_a('/cdb_synset/wn_internal_relations/relation[@relation_name="HAS_HYPONYM"]').size
    count_des = doc.root.attributes['count_childs']
    text = get_literals(id, dictarray).join(', ')
    text = id if text == ''
    text = ''.ljust(count_hyper, 'H') + ' ' + text
    text += ' C=' + count_child.to_s + '/' + count_des.to_s
    text += ' D=' + disj.to_s
    res = [upilr, id, text]
    return res  
  end
  
  def get_subtree(xmldoc, dictarray, dir = 'HAS_HYPERONYM')
    $stderr.puts "*get_subtree " + dir
    res = []
    if xmldoc == ''
      return res
    end
    doc = REXML::Document.new( xmldoc )
    origId = doc.root.attributes['c_sy_id'].to_s
    REXML::XPath.each( doc, "/cdb_synset/wn_internal_relations/relation[@relation_name='#{dir}']") {|el|
      id = el.attributes['target'].to_s
      disj = el.attributes['disjunctive'].to_s
      #text = id + '|' + get_literals(id, dictarray).join(', ')
      text = get_literals(id, dictarray).join(', ')
      text = id if text == ''
      count_child, count_des, count_hyper = get_counts(id, dictarray)
      text = ''.ljust(count_hyper, 'H') + ' ' + text
      text += ' C=' + count_child.to_s + '/' + count_des.to_s
      text += ' D=' + disj
      res << [origId, id, text]
    }
    return res
  end
  
  def get_full_subtree(xmldoc, dictarray, dir = 'HAS_HYPERONYM')
    res = []
    if xmldoc == ''
      return res
    end
    #xmldoc = get_rilr(xmldoc, dictarray)
    doc = REXML::Document.new( xmldoc )
    origId = doc.root.attributes['c_sy_id'].to_s
    new = []
    REXML::XPath.each( doc, "/cdb_synset/wn_internal_relations/relation[@relation_name='#{dir}']") {|el|
      id = el.attributes['target'].to_s
      disj = el.attributes['disjunctive'].to_s
      text = get_literals(id, dictarray).join(', ')
      text = id if text == ''
      count_child, count_des, count_hyper = get_counts(id, dictarray)
      text = ''.ljust(count_hyper, 'H') + ' ' + text
      text += ' C=' + count_child.to_s + '/' + count_des.to_s
      text += ' D=' + disj
      res << [origId, id, text]
      new << id
    }

    #get subtree for each new id recursively
    new.each{|id|
      xml = get_one(id)
      if dir == 'HAS_HYPERONYM'
        #xml = get_rilr(xml, dictarray)
      end
      newres = get_full_subtree(xml, dictarray, dir)
      res = res.concat(newres)
    }
    return res
  end
  
  def get_counts(id, dictarray)
    xml = get_one(id)
    #xml = get_rilr(xml, dictarray)
    doc = REXML::Document.new( xml.to_s )
    count_hyper = doc.elements.to_a('/cdb_synset/wn_internal_relations/relation[@relation_name="HAS_HYPERONYM"]').size
    count_child = doc.elements.to_a('/cdb_synset/wn_internal_relations/relation[@relation_name="HAS_HYPONYM"]').size
    count_des = doc.root.attributes['count_childs'] if doc.root != nil
    return count_child, count_des, count_hyper
  end

  def get_one(id)
    begin
      res = get(id)
      return res.to_s
    rescue
      $stderr.puts 'Document not found: ' + id
      return ''
    end
  end

  def remove_synonym(sy_id, lu_id)
    begin
      xml = get(sy_id)
      doc = REXML::Document.new(xml.to_s)
      doc.elements.delete("/cdb_synset/synonyms/synonym[@c_lu_id='#{lu_id}']")
      update(sy_id, doc.to_s)
    rescue => e
      $stderr.puts "exception #{e.class.to_s}: #{e.message}"
    end
    return true
  end
  
  def add_synonym(sy_id, lu_id)
    begin
      xml = get(sy_id)
      $stderr.puts xml.to_s
      doc = load_xml_string(xml.to_s)
      if doc.root.find('/cdb_synset/synonyms/synonym[@c_lu_id="'+lu_id+'"]').length == 0
        ns = XML::Node.new('synonym')
        ns['c_lu_id'] = lu_id
        doc.root.find('/cdb_synset/synonyms').first << ns
      end
      $stderr.puts doc.to_s
      update(sy_id, doc.to_s)
    rescue => e
      $stderr.puts "ADD SYNONYM exception #{e.class.to_s}: #{e.message}"
      return 'error adding synonym'
    end
    return true
  end

  def strip_generated(xml)
    doc = REXML::Document.new(xml.to_s)
    #hyponym relations
    doc.elements.delete_all('/cdb_synset/wn_internal_relations/relation[@relation_name="HAS_HYPONYM"]')
    return doc.to_s
  end
  def strip_redundant(xml, check_access=false)
    if check_access and @admindict.check_perm(Thread.current[:auth_user], @dict_prefix+'cdb_red')
      return xml
    end
    return xml if xml.nil? or xml.to_s == ''
    doc = REXML::Document.new(xml.to_s)
    doc.root.elements.delete('definition')
    doc.root.elements.delete('differentiae')
    doc.root.elements.delete('vlis_domains')
    doc.root.attributes.delete('posSpecific')
    $stderr.puts doc.to_s
    return doc.to_s
  end
 
  def recount_childs
    childs = {}
    childs = recount_first_level()
    #write values to database
    @container.each{|xml|
      doc = REXML::Document.new(xml.to_s)
      id = doc.root.attributes['c_sy_id']
      count = 0
      count = childs[id].uniq.size.to_i if childs[id] != nil
      doc.root.attributes['count_childs'] = count
      update(id, doc.to_s, false)
      $stderr.puts id + ' ' + count.to_s
    }
    return childs
  end
  
  def recount_first_level
    start = time_stamp()
    $stderr.puts "RECOUNT " + start
    #counter = {}
    childs = {}
    uplevel = []
    #@manager.query("for $x in collection('#{@collection}')/cdb_synset where count($x/wn_internal_relations/relation[@relation_name='HAS_HYPONYM'])=0 return $x") {|xml|
    @sedna.query("for $x in collection('#{@collection}') where count($x/cdb_synset/wn_internal_relations/relation[@relation_name='HAS_HYPONYM'])=0 return document-uri($x)") {|key|
      xml = get(key)
      #counter[key] = 0
      doc = REXML::Document.new(xml.to_s)
      #$stderr.puts 'Count: ' + key.to_s + '=' + counter[key].to_s
      hypernym_el = REXML::XPath.first(doc, '/cdb_synset/wn_internal_relations/relation[@relation_name="HAS_HYPERONYM"]')
      if hypernym_el != nil
        hypernym = hypernym_el.attributes['target'].to_s
        if hypernym != key
          #$stderr.puts counter[hypernym].to_s
          #if counter[hypernym] != nil
          #  counter[hypernym] += 1
          #else
          #  counter[hypernym] = 1
          #end
          if childs[hypernym] != nil
            childs[hypernym] << key
          else
            childs[hypernym] = [key]
          end
          #$stderr.puts counter[hypernym].to_s
          if uplevel.include?(hypernym) == false and hypernym != ''
            uplevel << hypernym
          end
        end
        $stderr.puts key  + ' ^ ' + hypernym
      end
    }
    childs = recount_level(childs, uplevel)
    $stderr.puts "RECOUNT END" + start + '>' + time_stamp()
    return childs
  end

  def recount_level(childs, tocount)
    start = time_stamp()
    uplevel = []
    $stderr.puts "RECOUNT LEVEL " + start + ' tocount: ' + tocount.size.to_s
    tocount.each{ |id|
      #$stderr.puts 'Count: ' + id.to_s + '=' + counter[id].to_s
      begin
        xml = get(id)
      rescue
        xml = nil
        next
      end
      if xml != nil
        key = xml.get_metadata('http://www.sleepycat.com/2002/dbxml','name')
        doc = REXML::Document.new(xml.to_s)
        hypernym_el = REXML::XPath.first(doc, '/cdb_synset/wn_internal_relations/relation[@relation_name="HAS_HYPERONYM"]')
        if hypernym_el != nil
          hypernym = hypernym_el.attributes['target'].to_s
          if hypernym != key
            #$stderr.puts counter[hypernym].to_s
            #if counter[hypernym] != nil
            #  counter[hypernym] += counter[key] + 1
            #else
            #  counter[hypernym] = 1
            #end
            if childs[hypernym] != nil
              childs[hypernym] << key
              childs[hypernym] += childs[key]
            else
              childs[hypernym] = [key]
              childs[hypernym] += childs[key]
            end
            #$stderr.puts counter[hypernym].to_s
            if uplevel.include?(hypernym) == false and hypernym != ''
              uplevel << hypernym
            end
          end
          $stderr.puts key  + ' ^ ' + hypernym
        end
      end
    }
    $stderr.puts "RECOUNT LEVEL END" + start + '>' + time_stamp()
    if uplevel.size > 0
      childs = recount_level(childs, uplevel)
    end
    return childs
  end
  
  def first_run(dictionary, dictarray)
    start = time_stamp()
    $stderr.puts "FIRST RUN START " + start
    @sedna.query("for $x in collection('#{@collection}') return document-uri($x)").each{|key|
      res = get(key)
      res = strip_generated(res)
      res = follow_previewtext(res,  dictarray)
      res = get_rilr(res, dictarray)
      res = follow_redundant(res, dictarray[@dict_prefix+'cdb_red'])
      $stderr.puts '*Updating synset '+key.to_s
      update(key, res.to_s, false)
    }
    $stderr.puts "FIRST RUN END" + start + '>' + time_stamp()
  end
end
