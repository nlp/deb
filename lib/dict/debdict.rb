require 'dict/dict-sedna'

class DebDict < DictSedna
  def initialize(collection, journal_path, key_path='', database = 'deb', admindict = nil)
    super
    @servlet = DebdictServlet
  end


  def list_starts_with( key, dict, limit=0, searchall=false )
    r = []
    top = @key_path.split('/')[1]
    rest = @key_path.split('/')[2..-1].join('/')
    keysmall = key.downcase

    if key[0,1] == '-'
      query = "for $x in collection('#{@collection}')/#{top} where some $p in $x/#{rest} satisfies (ends-with($p,'#{key[1..-1]}')) return $x"
      $stderr.puts query
    else
      case dict
      when 'ssc'
        if searchall
          query = "for $x in collection('#{@collection}')/#{top} where some $p in $x/#{rest}|$x/other/t|$x//exp satisfies (contains($p,'#{key}'))  return $x"
        elsif key[0,1] == '!'
          query = "for $x in collection('#{@collection}')/#{top} where some $p in $x/#{rest}|$x/other/t satisfies ($p='#{key[1..-1]}')  return $x"
        else
          query = "for $x in collection('#{@collection}')/#{top} where some $p in $x/#{rest}|$x/other/t satisfies (starts-with($p,'#{key}'))  return $x"
        end
      when 'ssjc'
        if searchall
          query = "for $x in collection('#{@collection}')/#{top} where some $p in $x/#{rest}|$x/bold|$x/arial|$x/ital satisfies (contains($p,'#{key}'))  return $x"
        elsif key[0,1] == '!'
          query = "for $x in collection('#{@collection}')/#{top} where some $p in $x/#{rest}|$x/bold|$x/arial satisfies ($p='#{key[1..-1]}')  return $x"
        else
          query = "for $x in collection('#{@collection}')/#{top} where some $p in $x/#{rest}|$x/bold|$x/arial satisfies (starts-with($p,'#{key}'))  return $x"
        end
      when 'psjc'
        if searchall
          query = "for $x in collection('#{@collection}')/#{top} where some $p in $x/#{rest}|$x/Vyzn satisfies (contains($p,'#{key}'))  return $x"
        elsif key[0,1] == '!'
          query = "for $x in collection('#{@collection}')/#{top} where some $p in $x/#{rest} satisfies ($p='#{key[1..-1]}')  return $x"
        else
          query = "for $x in collection('#{@collection}')/#{top} where some $p in $x/#{rest} satisfies (starts-with($p,'#{key}'))  return $x"
        end
      when 'scs'
        if searchall
          query = "for $x in collection('#{@collection}')/#{top} where some $p in $x/#{rest}|$x/t satisfies (contains($p,'#{key}')) return $x"
        elsif key[0,1] == '!'
          query = "for $x in collection('#{@collection}')/#{top} where some $p in $x/#{rest} satisfies ($p='#{key[1..-1]}') return $x"
        else
          query = "for $x in collection('#{@collection}')/#{top} where some $p in $x/#{rest} satisfies (starts-with($p,'#{key}')) return $x"
        end
      when 'scfis', 'scfin'
        if searchall
          query = "for $x in collection('#{@collection}')/#{top} where some $p in $x/#{rest}|$x/n|$x/i satisfies (contains($p,'#{key}'))  return $x"
        else
          query = "for $x in collection('#{@collection}')/#{top} where some $p in $x/#{rest} satisfies (contains($p,'#{key}'))  return $x"
        end
      when 'diderot'
        if searchall
          query = "for $x in collection('#{@collection}')/#{top} where (some $p in $x/#{rest}|$x/MEANING/MAN|$x/MEANING/MOUNTAIN|$x/MEANING/CITY|$x/MEANING/COUNTRY satisfies (starts-with($p,'#{key}'))) or (some $p in $x/keysmall satisfies (starts-with($p, '#{keysmall}')))  return $x"
        elsif key[0,1] == '!'
          query = "for $x in collection('#{@collection}')/#{top} where (some $p in $x/#{rest} satisfies ($p='#{key[1..-1]}')) or (some $p in $x/keysmall satisfies ($p='#{keysmall[1..-1]}'))  return $x"
        else
          query = "for $x in collection('#{@collection}')/#{top} where (some $p in $x/#{rest} satisfies (starts-with($p,'#{key}'))) or (some $p in $x/keysmall satisfies (starts-with($p, '#{keysmall}')))  return $x"
        end
      when 'cia'
        if key[0,1] == '*'
          query = "collection('#{@collection}')/#{top}"
        else
          query = "collection('#{@collection}')/#{top}[contains(lower-case(#{rest}), lower-case('#{key}'))]"
        end
      when 'cod11'
        query = "collection('#{@collection}')/se[starts-with(hg/hw,'#{key}')]"
      when 'ode'
        query = "collection('#{@collection}')/se[starts-with(hg/hw,'#{key}')]"
      when 'ote'
        query = "collection('#{@collection}')/se[starts-with(hg/hw,'#{key}')]"
      when 'crd'
        query = "collection('#{@collection}')/entry[starts-with(hw,'#{key}') or starts-with(var,'#{key}')]"
      when 'vulg'
        query = "for $x in collection('#{@collection}')/entry where (some $p in $x/h|$x/var satisfies starts-with($p,'#{key}')) return $x"
      else
        if key[0,1] == '!'
          query = "for $x in collection('#{@collection}')/#{top} where some $p in $x/#{rest} satisfies ($p='#{key[1..-1]}') return $x"
        else
          query = "for $x in collection('#{@collection}')/#{top} where some $p in $x/#{rest} satisfies (starts-with($p,'#{key}')) return $x"
        end
      end
    end
    $stderr.puts query
    
    count = 0
    query(query).each do |string|
      doc = load_xml_string(string.to_s)
      unless doc.find(@key_path).to_a.first.nil?
        if doc.find(@key_path).to_a.first.is_a?(XML::Attr)
          ka = @key_path.split('/')
          s = doc.find(ka[0..-2].join('/')).to_a.first[ka[-1].sub('@','')].to_s.gsub(/, $/, '').gsub(/ $/, '')
        else
          s = doc.find(@key_path).to_a.first.content.to_s.gsub(/, $/, '').gsub(/ $/, '')
        end
        r << s
        count += 1
        if limit > 0 and count >= limit
          break
        end
      end
    end
    $stderr.puts r.join("\n");
    return r.sort.join("\n")
  end

  def get_freq(dict, slovo)
  return ''
    case dict
      when 'ssjc','ssc','syno','psjc'
        sl_a = slovo.split(' ')
        if sl_a.size > 1
          slovo = sl_a[0..-2].join(' ')
        end
        slovo = Iconv.new('iso-8859-2','utf-8').iconv(slovo)
        command = "echo '#{slovo}'|lsclex -f -s s2kc lemma|awk '{print $3}'"
        $stderr.puts command
        freq = `#{command}`.to_s
        $stderr.puts freq
        return freq
    end
    return ''
  end
end
