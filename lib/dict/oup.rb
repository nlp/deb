require 'dict/dict-sedna'

class OUP < DictSedna
  def initialize(collection, journal_path, key_path='', database = 'deb', admindict = nil)
    super
    @servlet = OUPServlet
  end
end
