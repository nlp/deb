require 'dict/dict-sedna'
require 'dict/wn2'

class CdbID < WordNet2
  def initialize( db_path, database, key_path='', env = nil, admindict = nil )
    super
    @servlet = CdbIDServlet
    @id_regexp = /[0-9]+/ #REGEXP FOR CID ID!
    @user_queries = [
      [['select CID with score ', 'X1', ' and status ', 'X2'], ['cid.@score=', 'X1', ' and cid.@status=', 'X2']],
    ]
  end

  def get_list(query, array=nil)
    $stderr.puts "cdb_id:get_list start "+time_stamp()
    synsets = {}
    db_query = ''
    query ||= ''
    query.gsub!(/&(?!(\w+|#\d+);)/, '&amp;')
    query.gsub!(/'/, '&quot;')

    top = @key_path.split('/')[1]
    rest = @key_path.split('/')[2..-1].join('/')
    
    if query[0,top.length] == top or query.include?"="
      search_string = parse_conditions(query, array)
      #search_string += " and $x/@selected='true'"
      db_query = "for $x in collection('#{@collection}') where #{search_string} return document-uri($x)"
    else
      ar = query.split(':')
      query = ar[0]
      sense = (ar[1] == nil)? '' : 'and $x/@seq_nr="' + ar[1].to_s + '"'
      sensexp = (ar[1] == nil)? '' : 'and @seq_nr="' + ar[1].to_s + '"'
      
      if (pos = query.index('*')) != nil
        search = query.gsub('*','')
        if pos == 0 and query.count('*') > 1
          #db_query = "for $x in collection('#{@collection}')/#{top} where some $p in $x/@form satisfies (contains($p, '#{search}')) and $x/@selected='true' #{sense} return $x"
          db_query = "for $x in collection('#{@collection}') where some $p in $x/cid/@form satisfies (contains($p, '#{search}')) and $x/cid/@selected='true' #{sense} return document-uri($x)"
        elsif pos == 0
          #db_query = "for $x in collection('#{@collection}')/#{top} where some $p in $x/@form satisfies (ends-with($p, '#{search}')) and $x/@selected='true' #{sense} return $x"
          db_query = "for $x in collection('#{@collection}') where some $p in $x/cid/@form satisfies (ends-with($p, '#{search}')) and $x/cid/@selected='true' #{sense} return document-uri($x)"
        else
          #db_query = "for $x in collection('#{@collection}')/#{top} where some $p in $x/@form satisfies (starts-with($p, '#{search}')) and $x/@selected='true' #{sense} return $x"
          db_query = "for $x in collection('#{@collection}') where some $p in $x/cid/@form satisfies (starts-with($p, '#{search}')) and $x/cid/@selected='true' #{sense} return document-uri($x)"
        end
      elsif query =~ @id_regexp
        #db_query = "collection('#{@collection}')/#{top}[#{rest}='#{query}']"
        db_query = "for $x in collection('#{@collection}')[#{top}[#{rest}='#{query}']] return document-uri($x)"
      else
        #db_query = "for $x in collection('#{@collection}')/#{top} where some $p in $x/@form satisfies ($p='#{query}') and $x/@selected='true' #{sense} return $x"
        db_query = "for $x in collection('#{@collection}') where some $p in $x/cid/@form satisfies ($p='#{query}') and $x/cid/@selected='true' #{sense} return document-uri($x)"
      end
    end
    $stderr.puts db_query
    $stderr.puts "QUERY START " + time_stamp()
    count = 0
    query(db_query).each{|id|
      count += 1
      s = get(id)
      $stderr.puts '  '+count.to_s + ' - '+ time_stamp() + ' entry '+id
      doc = REXML::Document.new( s.to_s )
      #id = REXML::XPath.first( doc, '/cid/@cid_id' ).to_s
      synsets[id] = {}
      synsets[id]['seq_nr'] = doc.root.attributes['seq_nr'].to_s
      synsets[id]['form'] = doc.root.attributes['form'].to_s
      synsets[id]['lu_id'] = doc.root.attributes['c_lu_id'].to_s
      synsets[id]['sy_id'] = doc.root.attributes['d_sy_id'].to_s
      synsets[id]['score'] = doc.root.attributes['score'].to_s
      synsets[id]['status'] = doc.root.attributes['status'].to_s
      synsets[id]['selected'] = doc.root.attributes['selected'].to_s
      synsets[id]['pos'] = doc.root.attributes['pos'].to_s
      lockuser = @admindict.who_locked(@dict_prefix+'cdb_id', id)
      synsets[id]['lockstatus'] = lockuser
    }
    
    $stderr.puts "QUERY END " + time_stamp()
    synsets 
  end

  def deselect_lu(lu_id)
    xquery_to_hash("[cid[@c_lu_id='#{lu_id}' and @selected='true']]").each{|id,xml|
      $stderr.puts "Deselect CID " + id
      doc = REXML::Document.new(xml.to_s)
      doc.root.attributes['selected'] = 'false'
      doc.root.attributes['status'] = 'manual'
      update(id, doc.to_s)
    }
    return true
  end
  
  def select(cid_id)
    $stderr.puts "Select CID " + cid_id
    xml = get(cid_id)
    doc = REXML::Document.new(xml.to_s)
    doc.root.attributes['selected'] = 'true'
    if doc.root.attributes['status'] == nil or doc.root.attributes['status'] == 'automatic'
      doc.root.attributes['status'] = 'manual'
    end
    update(cid_id, doc.to_s)
    return true
  end
  
  def newcid(lu_id, sy_id, admin_dict, lu_dict)
    deselect_lu(lu_id)
    newid = admin_dict.get_next_id(@dict_prefix+'cdb_id')
$stderr.puts 'x'
    luxml = lu_dict.get(lu_id)
    ludoc = REXML::Document.new(luxml.to_s)
    form = ludoc.root.elements['form'].attributes['form-spelling'].to_s
    pos = ludoc.root.elements['form'].attributes['form-cat'].to_s.upcase
    seq = ludoc.root.attributes['c_seq_nr'].to_s
    xml = "<cid name='#{Thread.current[:auth_user]}' d_sy_id='#{sy_id}' c_lu_id='#{lu_id}' cid_id='#{newid}' form='#{form}' seq_nr='#{seq}' pos='#{pos}' selected='true' status='manual'/>"
    $stderr.puts xml
    update(newid, xml)
    return newid
  end

  def delete_for_lu(lu_id)
    xquery_to_list("[cid[@c_lu_id='#{lu_id}']]").each{|id|
      $stderr.puts "REMOVE CID "+id
      delete(id)
    }
    return true
  end
  
  def delete_synset(sy_id)
    xquery_to_hash("[cid[@d_sy_id='#{sy_id}']]").each{|id, xml|
      $stderr.puts "REMOVE SYNSET FROM CID "+id
      doc = REXML::Document.new(xml.to_s)
      doc.root.attributes['d_sy_id'] = ''
      update(id, doc.to_s)
    }
    return true
  end

  def follow_lu(xml, ludict)
    doc = REXML::Document.new(xml)
    form, seqnr, pos = ludict.get_form_seq(doc.root.attributes['c_lu_id'])
    doc.root.attributes['form'] = form if form.to_s != ''
    doc.root.attributes['seq_nr'] = seqnr if seqnr.to_s != ''
    doc.root.attributes['pos'] = pos if pos.to_s != ''
    return doc.to_s
  end
  
  def first_run(dictionary, dictarray)
    start = time_stamp()
    $stderr.puts "FIRST RUN START " + start
    @sedna.query("for $x in collection('#{@collection}') return document-uri($x)").each{|key|
      res = get(key)
      res = follow_lu(res, dictarray[@dict_prefix+'cdb_lu'])
      doc = REXML::Document.new(res)
      doc.root.attributes['name'] = 'hel' if doc.root.attributes['name'].to_s == ''
      res = doc.to_s
      $stderr.puts '*Updating CID '+key.to_s
      update(key, res.to_s, false)
    }
    $stderr.puts "FIRST RUN END" + start + '>' + time_stamp()
  end
end
