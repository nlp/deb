require 'rubygems'
require 'net/smtp'
require 'open-uri'
require 'uri'
require 'set'
require 'builder'
require 'json'
require 'cobravsmongoose'
require 'fileutils'


class CPALEMON
  attr_accessor :xslt_sheets
  attr_accessor :dbh
  attr_accessor :corpora
  attr_accessor :main_freq_corp
  attr_accessor :service_name

  def initialize(service_name=nil)
    @service_name = service_name
    @dbh = DBI.connect("dbi:Pg:"+service_name+":localhost", 'xrambous', 'veverka')
    @xslt_sheets = Hash.new
    @status = {'R' => 'ready', 'W' => 'WIP', 'C' => 'complete',
        '1' => '1st check', '2' => '2nd check', 'O' => 'old',
        'N' => 'NYS', 'A' => 'AUX', 'M' => 'MODAL', 'Q' => 'Q',
        'V'=>'VALID', 'S' => 'VLF', 'G' => 'G'}
    @serv_lang = {'cpa' => 'en', 'cpait' => 'it', 'cpasp' => 'sp'}
    @language = @serv_lang[@service_name]
    @corpora = {'cpa' => 'bnc50', 'cpait' => 'itwac3.nodups.fulldocs.7', 'cpasp' => 'iula50'}
    @corpus = @corpora[@service_name]
    @dbfreqrow = {'cpa' => 'bnc50_freq', 'cpait' => 'bncfreq', 'cpasp' => 'bncfreq'}
    @freqrow = @dbfreqrow[@service_name]
    @completestatus = {'cpa' => "entries.status = 'C'", 'cpait' => "entries.status ='W'", 'cpasp' => "entries.status ='W' or entries.status ='1' or entries.status ='2'"}
    @complete = @completestatus[@service_name]
    @pattern_dict = {'cpa' => 'pde', 'cpait' => 'pdi', 'cpasp' => 'pdsp'}
    @pdv = @pattern_dict[@service_name]+'v'
    @pdp = @pattern_dict[@service_name]+'p'
    @pdn = @pattern_dict[@service_name]+'n'
    $stderr.puts @pdv,@service_name
    @destpath = ''
    @nif_s = 0
    @nif_word = 0
    @nif_wordcount = 0
    @pdvlink = "http://pdev.org.uk/#{@pdv}lemon/"
    @nif_prefix = @pdvlink+"nif/"
    @nif_file = ''
  end

    def get_babelnet_links(file)
        hasvar = Hash.new
        pos = Hash.new
        File.open(file,"r") do |handle|
            handle.each_line do |line|
                result = line.strip.split(/\t/)
                #token	verb	pattern_number	example	babelnet_synsetId	babelNet_url	score	POS 
                #verb (1)+pattern (2)= babelnet-synset-url(5)
                if (pos[result[7]].nil?)
                  pos[result[7]]=0
                end
                pos[result[7]]+=1
                if (hasvar[result[1]].nil?)
                  hasvar[result[1]] = Hash.new
                end
                if (hasvar[result[1]][result[2]].nil?)
                  hasvar[result[1]][result[2]]= Hash.new
                end
                if (not hasvar[result[1]][result[2]][result[5]].nil?)
                    hasvar[result[1]][result[2]][result[5]]+= 1
                else
                    hasvar[result[1]][result[2]][result[5]]= 1
                end
            end
        end
        posfreqfile = File.open(@destpath+'posfreq', 'w')
        pos.each{|k,v|
            posfreqfile.write(k.to_s+"\t"+v.to_s+"\n")
        }
        posfreqfile.close
        return hasvar
    end

    def maxvalkey(hash)#return array [k,v]
      hash.max_by{|k,v| v}
    end

    def turtle_verblist_local(destpath,retrieve_example = 0, variantfile = '',babelnetfile = '')
        @destpath = destpath
        @nif_file = destpath+"nif/#{@pdv}-nif.ttl"
        babelnet = Hash.new
        if babelnetfile != ''
          babelnet = get_babelnet_links(babelnetfile)
        end
        start_time = Time.new
        header = %Q[@base <http://pdev.org.uk/#{@pdv}lemon/> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix lemon: <http://www.monnet-project.eu/lemon#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix lexinfo: <http://lexinfo.net/ontology/2.0/lexinfo#> .
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .
@prefix #{@pdv}l: <http://pdev.org.uk/#{@pdv}lemon/> .
@prefix #{@pdv}l-s: <http://pdev.org.uk/#{@pdv}lemon/#{@pdv}lemon-structure/> .
@prefix #{@pdv}l-d: <http://pdev.org.uk/#{@pdv}lemon/#{@pdv}lemon-domain/> .
@prefix #{@pdv}l-r: <http://pdev.org.uk/#{@pdv}lemon/#{@pdv}lemon-register/> .
@prefix #{@pdv}l-o: <http://pdev.org.uk/#{@pdv}lemon/#{@pdv}lemon-CPASO/> .
@prefix #{@pdv}l-t: <http://pdev.org.uk/#{@pdv}lemon/#{@pdv}lemon-CoRoTaxo/> .
@prefix #{@pdv}l-p: <http://pdev.org.uk/#{@pdv}lemon/#{@pdv}lemon-PatSenses/> .
@prefix dct:  <http://purl.org/dc/terms/> .
@prefix dce: <http://purl.org/dc/elements/1.1/> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
<>
    dct:license <http://creativecommons.org/licenses/by-sa/3.0/> ;
    dct:source <http://pdev.org.uk> .\n\n]
        #create turtle ontologies
        mainpath = @destpath
        cor={@pdv+'l'=>mainpath,@pdv+'l-o'=>mainpath+@pdv+'lemon-CPASO/',@pdv+'l-p'=>mainpath+@pdv+'lemon-PatSenses/',@pdv+'l-s'=>mainpath+@pdv+'lemon-structure/',@pdv+'l-t'=>mainpath+@pdv+'lemon-CoRoTaxo/',@pdv+'l-r'=>mainpath+@pdv+'lemon-register/',@pdv+'l-d'=>mainpath+@pdv+'lemon-domain/'}
        cor.each{|k,v|
            FileUtils::mkdir_p v unless File.exists?(v)
            entryfile = File.open(mainpath+@pdv+'lemon-all.'+k, 'w')
            entryfile.write(header)
            entryfile.close
        }
        #create nif file
        nif_headers = "@prefix dc:    <http://purl.org/dc/elements/1.1/> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix nif:   <http://persistence.uni-leipzig.org/nlp2rdf/ontologies/nif-core#> .
@prefix rlog:  <http://persistence.uni-leipzig.org/nlp2rdf/ontologies/rlog#> .
@prefix p:     #{@nif_prefix} .
@prefix owl:   <http://www.w3.org/2002/07/owl#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .

p:kwic rdf:type rdf:Property;
	rdfs:label	\"kwic link\"@en;
	rdfs:comment	\"Key Word In Context link between a NIF token and a lemon entry\"@en;
	rdfs:domain	nif:RFC5147String;
	rdfs:range	lemon:LexicalEntry;\n"        
        FileUtils::mkdir_p mainpath+'/nif/' unless File.exists?(mainpath+'nif/')
        entryfile = File.open(@nif_file, 'w')
        entryfile.write(nif_headers)
        entryfile.close
        #store dicts
        megadict = build_turtle_PatSense(header,cor[@pdv+'l'])
        megadict = build_turtle_CPASO(header)
        megadict = build_turtle_DomainTaxonomy(header,megadict)
        megadict = build_turtle_RegisterTaxonomy(header,megadict,cor[@pdv+'l'])
        megadict = build_turtle_ContextualRolesTaxonomy(header,megadict,cor[@pdv+'l'])
        megadict,lex_set = build_lexical_set(megadict)
        megadict,prep_set = build_prepositions(megadict)
        hasvar = Hash.new
        var = Hash.new
        if variantfile!= ''
          hasvar,var = build_verb_variants(variantfile)
        end
        lex = Array.new
        db_query = "SELECT * FROM entries WHERE #{@complete} ORDER BY entry_id"
        q = @dbh.execute(db_query)
        q.each{|row|
            lex << @pdv+"l:"+@pdv.upcase+"_LexicalEntry_#{row['entry_id']}"
        }
        lexicon = @pdv+"l:"+@pdv.upcase+"_Lexicon_1 rdf:type lemon:Lexicon, owl:NamedIndividual ;\n"
        lexicon +=%Q[
    lemon:language "#{@language}" ;
    dce:creator "Hanks, Patrick" ;
    dce:contributor "Bradbury, Jane" ;
    dce:contributor "El Maarouf, Ismail" ;
    dce:publisher "El Maarouf, Ismail" ;
    dce:date "#{Time.now.strftime('%Y-%m-%dT%H:%M:%SZ%z')}"^^xsd:date ;\n]
        lexicon +="dce:description \"The PDEV (Pattern Dictionary of English Verbs) is a dictionary of verb patterns,"\
                  "following the principles of the Theory of Norms and Exploitations proposed by Patrick Hanks."\
                  "During the period of 2012-2015, the PDEV is being developed at the University of Wolverhampton"\
                  "in collaboration with the University of Masaryk.\" ;"
        lexicon+="\n"+'    rdfs:comment "This is '+@pdv.upcase+'-LEMON, a Linked Data resource of '+@pdv.upcase+'"@#{@language} ;'+"\n"
        lexicon += "    lemon:entry "+lex.join(", ")+" .\n"
        filename = @pdv.upcase+"_Lexicon_1"
        printme(@pdv+'l',filename,header,lexicon)
        #Preposition lexicon
        preplexicon = ''
        preplexicon +=@pdv+"l:"+@pdp.upcase+"_Lexicon_1 rdf:type lemon:Lexicon, owl:NamedIndividual ;\n"
        time = Time.new
        preplexicon +=%Q[
    lemon:language "#{@language}" ;
    dce:creator "Hanks, Patrick" ;
    dce:contributor "Bradbury, Jane" ;
    dce:contributor "El Maarouf, Ismail" ;
    dce:publisher "El Maarouf, Ismail" ;
    dce:date "#{time.year}-#{time.month}-#{time.day}"^^xsd:date ;\n]
        preplexicon +="dce:description \"The PDEP is derived from the PDEV (Pattern Dictionary of English Verbs), and is a dictionary of preposition patterns,"\
                  "following the principles of the Theory of Norms and Exploitations proposed by Patrick Hanks."\
                  "During the period of 2012-2015, the PDEV is being developed at the University of Wolverhampton"\
                  "in collaboration with the University of Masaryk.\" ;"
        preplexicon+="\n"+'    rdfs:comment "This is '+@pdp.upcase+'-LEMON, a Linked Data resource of '+@pdv.upcase+'"@#{@language} ;'+"\n"
        prepar = []
        prep_set.each{|k,v|
            prepar << @pdv+"l:"+@pdp.upcase+"_LexicalEntry_#{megadict['Prepset'][k]}"
        }
        preplexicon += "    lemon:entry "+prepar.join(', ')+" .\n"
        filename = @pdp.upcase+"_Lexicon_1"
        printme(@pdv+'l',filename,header,preplexicon)
        #Noun lexicon
        nounlexicon = ''
        nounlexicon += @pdv+"l:"+@pdn.upcase+"_Lexicon_1 rdf:type lemon:Lexicon, owl:NamedIndividual ;\n"
        time = Time.new

        nounlexicon +=%Q[
    lemon:language "#{@language}" ;
    dce:creator "Hanks, Patrick" ;
    dce:contributor "Bradbury, Jane" ;
    dce:contributor "El Maarouf, Ismail" ;
    dce:publisher "El Maarouf, Ismail" ;
    dce:date "#{time.year}-#{time.month}-#{time.day}"^^xsd:date ;\n]
        nounlexicon +="dce:description \"The PDEN is derived from the PDEV (Pattern Dictionary of English Verbs), and is a dictionary of noun patterns,"\
                  "following the principles of the Theory of Norms and Exploitations proposed by Patrick Hanks."\
                  "During the period of 2012-2015, the PDEV is being developed at the University of Wolverhampton"\
                  "in collaboration with the University of Masaryk.\" ;"
        nounlexicon+="\n"+'    rdfs:comment "This is '+@pdn.upcase+'-LEMON, a Linked Data resource of '+@pdv.upcase+'"@#{@language} ;'+"\n"
        lexar = []
        lex_set.each{|k,v|
            lexar << @pdv+"l:"+@pdn.upcase+"_LexicalEntry_#{megadict['Lexset'][k]}"
        }
        nounlexicon += "    lemon:entry "+lexar.join(', ')+" .\n"
        filename = @pdn.upcase+"_Lexicon_1"
        printme(@pdv+'l',filename,header,nounlexicon)
        #Prep entries
        q = @dbh.execute(db_query)
        prep_set.each{|k,v|
            entry = %Q[#{@pdv}l:#{@pdp.upcase}_LexicalEntry_#{megadict['Prepset'][k]} rdf:type lemon:LexicalEntry, owl:NamedIndividual ;
    lexinfo:partOfSpeech lexinfo:preposition ;
    rdfs:label "#{@pdv.upcase} Lexical Entry #{k}"@#{@language} ;
    lemon:language "#{@language}" .\n]
            printme(@pdv+'l',@pdp.upcase+"_LexicalEntry_#{megadict['Prepset'][k]}",header,entry)
        }
        #Noun entries
        q = @dbh.execute(db_query)
        entry = ""
        lex_set.each{|k,v|
            entry = %Q[#{@pdv}l:#{@pdn.upcase}_LexicalEntry_#{megadict['Lexset'][k]} rdf:type lemon:LexicalEntry, owl:NamedIndividual ;
    lexinfo:partOfSpeech lexinfo:noun ;
    rdfs:label "#{@pdn.upcase} Lexical Entry #{k}"@#{@language} ;
    lemon:language "#{@language}" .\n]
            printme(@pdv+'l',@pdv.upcase+"_LexicalEntry_#{megadict['Lexset'][k]}",header,entry)
        }
        #Verb entries
        q.each{|row|
            db_query2 = "SELECT COUNT(*) FROM patterns "\
                    "WHERE entry_id='#{row['entry_id']}'"
            q2 = @dbh.select_one(db_query2)
            patcount = q2[0].to_i
            status = @status[row['status'].to_s]
            entry = ''
            entry += %Q[#{@pdv}l:#{@pdv.upcase}_LexicalEntry_#{row['entry_id']} rdf:type lemon:LexicalEntry, owl:NamedIndividual ;
    lexinfo:partOfSpeech lexinfo:verb ;
    lemon:canonicalForm #{@pdv}l:#{@pdv.upcase}_LexicalEntry_#{row['entry_id']}_CanonicalForm ;
    #{@pdv}l-s:lexicalFrequencyOf #{@pdv}l:#{@pdv.upcase}_LexicalEntry_#{row['entry_id']}_sampleSize ;
    #{@pdv}l-s:lexicalFrequencyOf #{@pdv}l:#{@pdv.upcase}_LexicalEntry_#{row['entry_id']}_#{@corpus}Freq ;\n]
            for i in 1..patcount
                entry+="    lemon:sense #{@pdv}l:#{@pdv.upcase}_LexicalSense_#{row['entry_id']}_#{i} ;\n"
            end
            for i in 1..patcount
                entry+="    lemon:synBehavior #{@pdv}l:#{@pdv.upcase}_Pattern_#{row['entry_id']}_#{i} ;\n"
            end
            entry+=%Q[    rdfs:label "#{@pdv.upcase} Lexical Entry #{row['entry_id']}"@#{@language} .\n]
            samplesize = row['sample_size']
            corpusfreq = row[@freqrow] || 0
            samplesize = row[@freqrow] if ((!samplesize.nil?) and samplesize.downcase.include?('all')) or samplesize ==''
            samplesize = 0 if samplesize == ''
            printme(@pdv+'l',@pdv.upcase+"_LexicalEntry_#{row['entry_id']}",header,entry)
            form=%Q[#{@pdv}l:#{@pdv.upcase}_LexicalEntry_#{row['entry_id']}_CanonicalForm rdf:type lemon:Form, owl:NamedIndividual ;
    lemon:writtenRep "#{row['entry_id']}"@#{@language} ;\n]
            # if a variant, the variant must be expressed as a form, and a relation between forms must be expressed
            if hasvar.key?(row['entry_id'])
                hasvar[row['entry_id']].each{|var,v|
                    form+=%Q[    lemon:formVariant #{@pdv}l:#{@pdv.upcase}_LexicalEntry_#{var}_VariantForm ;\n]
                }
                form+=%Q[    rdfs:label "Canonical Form of #{row['entry_id']}"@#{@language} .\n]
                printme(@pdv+'l',@pdv.upcase+"_LexicalEntry_#{row['entry_id']}_CanonicalForm",header,form)
                hasvar[row['entry_id']].each{|var,v|
                    form=%Q[#{@pdv}l:#{@pdv.upcase}_LexicalEntry_#{var}_VariantForm rdf:type lemon:Form, owl:NamedIndividual ;
    lemon:writtenRep "#{var}"@#{@language} ;
    rdfs:label "Variant Form of #{row['entry_id']}"@#{@language} ;
    #{@pdv}l-s:formVariantof #{@pdv}l:#{@pdv.upcase}_LexicalEntry_#{row['entry_id']}_CanonicalForm .\n]
                    printme(@pdv+'l',@pdv.upcase+"_LexicalEntry_#{var}_VariantForm",header,form)
                }
            else
                form+=%Q[    rdfs:label "Canonical Form of #{row['entry_id']}"@#{@language} .\n]
                printme(@pdv+'l',@pdv.upcase+"_LexicalEntry_#{row['entry_id']}_CanonicalForm",header,form)
            end
            entry=%Q[#{@pdv}l:#{@pdv.upcase}_LexicalEntry_#{row['entry_id']}_sampleSize rdf:type #{@pdv}l-s:Frequency, owl:NamedIndividual ;
    rdfs:label "selected sample size in corpus #{@corpus}"@#{@language} ;
    #{@pdv}l-s:ofCorpus "#{@corpus}" ;
    #{@pdv}l-s:frequencyValue #{samplesize} .\n]
            printme(@pdv+'l',@pdv.upcase+"_LexicalEntry_#{row['entry_id']}_sampleSize",header,entry)
            entry=%Q[#{@pdv}l:#{@pdv.upcase}_LexicalEntry_#{row['entry_id']}_#{@corpus}Freq rdf:type #{@pdv}l-s:Frequency, owl:NamedIndividual ;
    rdfs:label "Frequency of #{row['entry_id']} in corpus #{@corpus}"@#{@language} ;
    #{@pdv}l-s:ofCorpus "#{@corpus}" ;
    #{@pdv}l-s:frequencyValue #{corpusfreq} .\n]
            printme(@pdv+'l',@pdv.upcase+"_LexicalEntry_#{row['entry_id']}_"+@corpus+"Freq",header,entry)
        }
        #senses
        q = @dbh.execute(db_query)
        q.each{|row|
            db_query2 = "SELECT COUNT(*) FROM patterns "\
                    "WHERE entry_id='#{row['entry_id']}'"
            q2 = @dbh.select_one(db_query2)
            patcount = q2[0].to_i
            samplesize = row['sample_size'].to_s
            samplesize = 'all' if samplesize.downcase.include?('all') or samplesize == ''
            examples_link = Hash.new
            if retrieve_example == 1
                examples_link = examples(row['entry_id'],samplesize)
            end
            url = ''
            if @language == 'en'
                url = 'http://www.fanuk.net/bonito/run.cgi/lngroupinfo?q=q[lempos="'+row['entry_id']+'-v"];corpname='+@corpus+';annotconc='+row['entry_id']+'-v;format=json'
            else
                url = 'http://www.fanuk.net/bonito/run.cgi/lngroupinfo?q=q[lemma="'+row['entry_id']+'"];corpname='+@corpus+';annotconc='+row['entry_id']+';format=json'
            end
            url += ';q=r'+samplesize if samplesize != 'all'
            map = JSON.parse(open(URI.escape(url), :http_basic_authentication=>['guest', 'GG0we5t']).read)
            norm={}
            exploitation={}
            for i in 1..patcount
                norm[i.to_s]=0
                exploitation[i.to_s]=0
            end
            map['LineGroups'].each do |row3|
                freq = row3['name'].to_s
                norm[freq] = row3['freq'] if freq =~ /^\d+$/
                #a, e, w, f, s are exploitations
                if freq =~ /^\d+\.[aefs]$/
                    if exploitation[freq.split('.').first].nil?
                        exploitation[freq.split('.').first] = row3['freq']
                    else
                        exploitation[freq.split('.').first] += row3['freq']
                    end
                end
            end
            semclass =row['semclass'] || ''
            babelurl = 'null'
            if (not babelnet[row['entry_id']].nil?) and (not babelnet[row['entry_id']][1].nil?)
              babelurl,freq=maxvalkey(babelnet[row['entry_id']][1])
            end
            turtle_senses(row['entry_id'],1,header,norm['1'],exploitation['1'],semclass,megadict,babelurl,examples_link)
            for i in 2..patcount
                babelurl = 'null'
                if (not babelnet[row['entry_id']].nil?) and (not babelnet[row['entry_id']][i].nil?)
                  babelurl,freq=maxvalkey(babelnet[row['entry_id']][i])
                end
                turtle_senses(row['entry_id'],i,header,norm[i.to_s],exploitation[i.to_s],semclass,megadict,babelurl,examples_link)
            end
        }
        #patterns
        q = @dbh.execute(db_query)
        q.each{|row|
            db_query2 = "SELECT COUNT(*) FROM patterns "\
                    "WHERE entry_id='#{row['entry_id']}'"
            q2 = @dbh.select_one(db_query2)
            patcount = q2[0].to_i
            samplesize = row['sample_size'].to_s
            samplesize = 'all' if samplesize.downcase.include?('all') or samplesize == ''
            url = 'http://www.fanuk.net/bonito/run.cgi/lngroupinfo?q=q[lempos="'+row['entry_id']+'-v"];corpname='+@corpus+';annotconc='+row['entry_id']+'-v;format=json'
            url += ';q=r'+samplesize if samplesize != 'all'
            map = JSON.parse(open(URI.escape(url), :http_basic_authentication=>['guest', 'GG0we5t']).read)
            norm={}
            exploitation={}
            for i in 1..patcount
                norm[i.to_s]=0
                exploitation[i.to_s]=0
            end
            map['LineGroups'].each do |row3|
                freq = row3['name'].to_s
                norm[freq] += row3['freq'] if freq =~ /^\d+$/
                #a, e, w, f, s are exploitations
                exploitation[freq.split('.').first] += row3['freq'] if freq =~ /^\d+\.[aefs]$/
            end
            semclass = row['semclass'] || ''
            turtle_patterns(row['entry_id'],1,header,norm['1'],exploitation['1'],semclass,megadict,cor[@pdv+'l'])
            for i in 2..patcount
                turtle_patterns(row['entry_id'],i,header,norm[i.to_s],exploitation[i.to_s],semclass,megadict,cor[@pdv+'l'])
            end
        }
        $stderr.puts @pdv+"-lemon-progress: finished"
        total_time = Time.new - start_time
        $stderr.puts "total= #{total_time.to_s} seconds"
    end

    #examples
    def examples(entry,samplesize)
        db_query2 = "SELECT COUNT(*) FROM patterns WHERE entry_id='#{entry}'"
        q2 = @dbh.select_one(db_query2)
        patcount = q2[0].to_i
        samplesize = 'all' if samplesize.downcase.include?('all') or samplesize == ''
        examples_link = Hash.new
        for i in 1..patcount
            ['n','a'].each{|ex_type|
                if @language =='en'
                    url = 'http://www.fanuk.net/bonito/run.cgi/view?q=q[lempos="' + entry + '-v"];q=M' + entry + '-v+'+ex_type+'+' + i.to_s + ';corpname='+@corpus+';annotconc=' + entry + '-v;viewmode=sen;format=json;pagesize=5;'#gdex_enabled=1 bugs
                else
                    url = 'http://www.fanuk.net/bonito/run.cgi/view?q=q[lemma="' + entry + '"];q=M' + entry + '+'+ex_type+'+' + i.to_s + ';corpname='+@corpus+';annotconc=' + entry + ';viewmode=sen;format=json;pagesize=5;'
                end
                url += ';q=r'+samplesize if samplesize != 'all'
                map = JSON.parse(open(URI.escape(url), :http_basic_authentication=>['guest', 'GG0we5t']).read)
                if (not map['Lines'].nil?)
                    map['Lines'].each do |ex|
                        left = ''#ex['Left'][0]['str']
                        right = ''#ex['Right'][0]['str']
                        kwic = ''#ex['Kwic'][0]['str']
                        toknum = ex['toknum'].to_s
                        ex['Left'].each do |l|
                            left += ' '+l['str']
                        end
                        ex['Right'].each do |l|
                            right += ' '+l['str']
                        end
                        ex['Kwic'].each do |l|
                            kwic += ' '+l['str']
                        end
                        left = left.gsub(/ +/, ' ')
                        right = right.gsub(/ +/, ' ')
                        kwic = kwic.gsub(/ +/, ' ')
                        words = []
                        words = nif_extract(left.gsub(/ +/, ' '),words,0)
                        words = nif_extract(kwic.gsub(/ +/, ' '),words,1)
                        words = nif_extract(right.gsub(/ +/, ' '),words,0)
                        sentence = left+kwic+right
                        sentence = sentence.gsub(/ +/, ' ')
                        sent_output,new_ex = nif_generate(sentence,words,entry,i,toknum)
                        if examples_link[i].nil?
                            examples_link[i]=Hash.new
                        end
                        if examples_link[i][ex_type].nil?
                            examples_link[i][ex_type]=Array.new
                        end
                        examples_link[i][ex_type].push(new_ex)
                        exfile = File.open(@nif_file, 'a')
                        exfile.write(sent_output)
                        exfile.close
                    end
                end
            }
        end
        return examples_link
    end

    def nif_generate(sentence,words,verb,number,toknum)#deal with toknum...
        sent_url = "<p:char=#{words[0]['beg']},#{words[-1]['end']}>"
        sents={'url'=>sent_url,'form'=>sentence.gsub('"','\"'),'beg'=>words[0]['beg'],'end'=>words[-1]['end']}
        sent_struct = "	a	nif:Sentence , nif:Context , nif:RFC5147String, lemon:UsageExample ;
	nif:anchorOf	\"#{sents['form']}\" ;
	nif:isString	\"#{sents['form']}\" ;
	lemon:value	\"#{sents['form']}\" ;
	nif:beginIndex	\"#{sents['beg']}\" ;
	nif:endIndex	\"#{sents['end']}\" ;
	nif:firstWord	#{words[0]['url']} ;
	nif:lastWord	#{words[-1]['url']} ;
	nif:referenceContext	#{sents['url']} ;
	nif:word	"+words.map{|h| h['url']}.join(" , ")+" .\n"
        words.each_with_index do |val,w|
            pdev_link = ''
            if words[w]['kwic'] == 1
                pdev_link = "	kwic	<#{@pdvlink}#{@pdv.upcase}_LexicalSense_#{verb}_#{number}>;\n"
            end
            next_word = ""
            previous_word = ""
            if w == 0
                if w != (words.length - 1)
                    next_word = "	nif:nextWord	#{words[w+1]['url']};\n"
                end
            elsif w == (words.length - 1)
                previous_word = "	nif:previousWord	#{words[w-1]['url']};\n"
            else
                next_word = "	nif:nextWord	#{words[w+1]['url']}\n"
                previous_word = "	nif:previousWord	#{words[w-1]['url']};\n"
            end
            word_struct = "	a	nif:Word , nif:RFC5147String ;
	nif:anchorOf	\"#{words[w]['form']}\" ;
	nif:beginIndex	\"#{words[w]['beg']}\" ;
	nif:endIndex	\"#{words[w]['end']}\" ;\n"+pdev_link+previous_word+next_word+"	nif:referenceContext	#{sents['url']} ;
	nif:sentence	#{sents['url']} .\n"
            word_output = words[w]['url']+word_struct
            exfile = File.open(@nif_file, 'a')
            exfile.write(word_output)
            exfile.close
        end
        sent_output = sents['url']+sent_struct
        return sent_output,sents['url']
    end

    def nif_extract(sentence,words,kwic)
        sentence.split(' ').each do |token|
            offset_begin = @nif_wordcount+1 #space
            offset_end = @nif_wordcount+token.length
            @nif_wordcount+=1+token.length
            word_url = "<p:char=#{offset_begin},#{offset_end}>"
            wordhash = {'form'=>token.gsub('"','\"'),'url'=>word_url,'beg'=>offset_begin,'end'=>offset_end,'kwic'=>kwic}
            words.push(wordhash)
        end
        return words
    end

    def printme (prefix,filename,header,content)
        mainpath = @destpath
        cor={@pdv+'l'=>mainpath,@pdv+'l-o'=>mainpath+@pdv+'lemon-CPASO/',@pdv+'l-p'=>mainpath+@pdv+'lemon-PatSenses/',@pdv+'l-s'=>mainpath+@pdv+'lemon-structure/',@pdv+'l-t'=>mainpath+@pdv+'lemon-CoRoTaxo/',@pdv+'l-r'=>mainpath+@pdv+'lemon-register/',@pdv+'l-d'=>mainpath+@pdv+'lemon-domain/'}

        entryfile = File.open(cor[prefix]+filename, 'w')
        entryfile.write(header+content)
        entryfile.close
        entryfile = File.open(cor[@pdv+'l']+@pdv+'lemon-all.'+prefix, 'a')
        entryfile.write(content)
        entryfile.close
    end

    def build_prepositions(megadict)
        megadict['Prepset'] = Hash.new
        res = {}
        query = "select distinct(headword) from objects,entries where entries.entry_id = objects.entry_id and #{@complete} order by headword ASC"
        q = @dbh.execute(query)
        id=1
        q.each{|row|
            lex_ar = []
            if (not row['headword'].nil?) and row['headword'] != ''
                if row['headword'] =~ /[,|;.]+/
                    lex_ar=row['headword'].split(/[,|;.]+/)
                else
                    lex_ar << row['headword'].strip.gsub(/[ \/\[\]]+/, '_').gsub(/\}\{\(\)/,'')
                end
                for i in lex_ar
                    if (not i.nil?) and i != ''  and (not i.strip.nil?)
                        val = i.strip.gsub(/[ \/\[\]]+/, '_').gsub(/\}\{\(\)/,'')
                        res[val] = 1
                        if not megadict['Prepset'].include?(val)
                            megadict['Prepset'][val]=id
                            id +=1
                        end
                    end
                end
            end
        }
        return megadict,res
    end

    def build_verb_variants(variantfile)
        hasvar = Hash.new
        var = Hash.new
        if File.exists?(variantfile)
            File.open(variantfile,"r") do |handle|
                handle.each_line do |line|
                    result = line.strip.split(/\t/)
                    hasvar[result[1]] = Hash.new
                    hasvar[result[1]][result[0]]=1
                    var[result[0]]=1
                end
            end
        end
        return hasvar,var
    end

    def build_lexical_set(megadict)
        megadict['Lexset'] = Hash.new
        res = {}
        query = "select distinct(lexset_items) from objects,entries where entries.entry_id = objects.entry_id and #{@complete} order by lexset_items ASC"
        q = @dbh.execute(query)
        id = 1
        q.each{|row|
            lex_ar = []
            if row['lexset_items'] != ''
                if row['lexset_items'] =~ /[,|;.]+/
                    lex_ar=row['lexset_items'].split(/[,|;.]+/)
                else
                    lex_ar << row['lexset_items']
                end
                for i in lex_ar
                    if (not i.nil?) and i != ''  and (not i.strip.nil?)
                        val =i.strip.gsub(/[ \/\[\]]+/, '_').gsub(/\}\{\(\)=/,'')
                        res[val] = 1
                        if not megadict.include?(val)
                            megadict['Lexset'][val]=id
                            id+=1
                        end
                    end
                end
            end
        }
        return megadict,res
    end
    #create PatSense.ttl file
    def build_turtle_PatSense(header,path)
        filename = path+@pdv+"-lemon-PatSenses"
        onto = %Q[#{@pdv}l-p:PatSense rdf:type rdfs:Class, owl:Class ;
    rdfs:comment "a PatSense is a concept which contains at least one primary implicature]
        onto+=%Q[ and matches a lemon:Frame pattern. It is a reference of a lemon:LexicalSense." ;
    rdfs:label "Sense of a Pattern" .\n]
        printme(@pdv+'l-p','PatSense',header,onto) 
        onto =%Q[#{@pdv}l-p:primaryImplicature rdf:type owl:DatatypeProperty ;
    rdfs:label "Primary Implicature" ;
    rdfs:comment "Primary implicature value of a PatSense" ;
    rdfs:range xsd:string ;
    rdfs:domain #{@pdv}l-p:PatSense .\n]
        printme(@pdv+'l-p','primaryImplicature',header,onto)       
        onto =%Q[#{@pdv}l-p:secondaryImplicature rdf:type owl:DatatypeProperty ;
    rdfs:label "Secondary Implicature" ;
    rdfs:comment "Primary implicature value of a PatSense" ;
    rdfs:range xsd:string ;
    rdfs:domain #{@pdv}l-s:PatSense .\n]
        printme(@pdv+'l-p','secondaryImplicature',header,onto)       
        onto =%Q[#{@pdv}l-p:semanticClass rdf:type owl:DatatypeProperty ;
    rdfs:label "Semantic Class" ;
    rdfs:comment "Semantic Class value of a PatSense" ;
    rdfs:range xsd:string ;
    rdfs:domain #{@pdv}l-s:PatSense .\n]
        printme(@pdv+'l-p','semanticClass',header,onto)       
        onto =%Q[#{@pdv}l-p:frameNetSense rdf:type owl:DatatypeProperty ;
    rdfs:label "reference to FrameNet sense" ;
    rdfs:comment "FrameNet sense url reference of a PatSense" ;
    rdfs:range rdf:resource ;
    rdfs:domain #{@pdv}l-s:PatSense .\n]
        printme(@pdv+'l-p','frameNetSense',header,onto)
    end

    def build_turtle_CPASO(header)
        megadict = Hash.new
        megadict['CPASO'] = Hash.new
        onto = %Q[#{@pdv}l-o:#{@pdv.capitalize}SemanticType rdf:type rdfs:Class,owl:Class ;
    rdfs:label "#{@pdv.upcase} Semantic Type"@en ;
    rdfs:comment "Semantic Type defined in #{@pdv.upcase}"@en .\n]
        printme(@pdv+'l-o',@pdv.capitalize+'SemanticType',header,onto)
        onto=%Q[#{@pdv}l-o:STvalue rdf:type rdf:Property, owl:DatatypeProperty ;
    rdfs:label "Semantic Type string"@en ;
    rdfs:comment "string of Semantic Type defined in #{@pdv.upcase}"@en ;
    rdfs:domain #{@pdv}l-o:#{@pdv.capitalize}SemanticType ;
    rdfs:range xsd:string .\n]
        printme(@pdv+'l-o',@pdv.capitalize+'STvalue',header,onto)
        onto=%Q[#{@pdv}l-o:hasParent rdf:type rdf:Property, owl:ObjectProperty ;
    rdfs:label "Parent of "@en ;
    rdfs:comment "Parent of Semantic Type defined in #{@pdv.upcase}"@en ;
    rdfs:domain #{@pdv}l-o:#{@pdv.capitalize}SemanticType ;
    rdfs:range #{@pdv}l-o:#{@pdv.capitalize}SemanticType .\n]
        printme(@pdv+'l-o','hasParent',header,onto)

#<!--List of Semantic Types-->\n\n]
#     rdfs:comment "Lexicographical Comment: #{row['comment'].to_s.gsub(/"/,"'")}"@en ;
        query1 = "SELECT * from ontology;"
        q1 = @dbh.execute(query1)
        q1.each{|row|
            if megadict['CPASO'].include?(row['term'])
                megadict['CPASO'][row['term'].strip.gsub(/ +/, '_')]+=','+row['tid'].to_s
            else
                megadict['CPASO'][row['term'].strip.gsub(/ +/, '_')]=row['tid'].to_s
            end
            if row['term'] =='Anything'
                 onto=%Q[#{@pdv}l-o:#{@pdv.capitalize}SemanticType_#{row['tid']} rdf:type #{@pdv}l-o:#{@pdv.capitalize}SemanticType, owl:NamedIndividual ;
    rdfs:label "#{row['term']}"@en ;
    #{@pdv}l-o:STvalue "#{row['term']}"@en .\n]
                    printme(@pdv+'l-o',@pdv.capitalize+"SemanticType_#{row['tid']}",header,onto)
            else
                query2 = "SELECT hypernym FROM ontology_links where term='#{row['tid']}'"
                q2 = @dbh.execute(query2)
                q2.each{|row2|
                    onto=%Q[#{@pdv}l-o:#{@pdv.capitalize}SemanticType_#{row['tid']} rdf:type #{@pdv}l-o:#{@pdv.capitalize}SemanticType, owl:NamedIndividual ;
    rdfs:label "#{row['term']}"@en ;
    #{@pdv}l-o:STvalue "#{row['term']}"@en ;
    #{@pdv}l-o:hasParent #{@pdv}l-o:#{@pdv.capitalize}SemanticType_#{row2['hypernym']} .\n]
                    printme(@pdv+'l-o',@pdv.capitalize+"SemanticType_#{row['tid']}",header,onto)
                }
            end
        }
        return megadict
    end

    def build_turtle_DomainTaxonomy(header, megadict)
        megadict['Domain'] = Hash.new
        query = "select distinct(domain) from patterns,entries where entries.entry_id = patterns.entry_id and #{@complete} order by domain ASC;"
        q = @dbh.execute(query)
        id = 1
        onto=%Q[#{@pdv}l-d:#{@pdv.capitalize}Domain rdf:type lemon:SenseContext, rdfs:Class,owl:Class ;
    rdfs:label "#{@pdv.upcase} Domain"@en ;
    rdfs:comment "Domain defined in #{@pdv.upcase}"@en .\n]
        printme(@pdv+'l-d',@pdv.capitalize+'Domain',header,onto)
        onto= %Q[#{@pdv}l-d:Domainvalue rdf:type rdf:Property, owl:DatatypeProperty ;
    rdfs:label "Domain string"@en ;
    rdfs:comment "string of Domain defined in #{@pdv.upcase}"@en ;
    rdfs:domain #{@pdv}l-d:#{@pdv.capitalize}Domain ;
    rdfs:range xsd:string .\n]
        printme(@pdv+'l-d','Domainvalue',header,onto)
        q.each{|row|
            if row['domain'].to_s != ''
                dom_ar = Array.new
                if row['domain'].include?(';')
                    dom_ar=row['domain'].split(';')
                else
                    dom_ar << row['domain']
                end
                dom_ar.each{|d|
                    if not megadict['Domain'].include?(d.to_s)
                        megadict['Domain'][d.to_s] = id
                        onto=%Q[#{@pdv}l-d:#{@pdv.capitalize}Domain_#{id} rdf:type #{@pdv}l-d:#{@pdv.capitalize}Domain, owl:NamedIndividual ;
    rdfs:label "#{d.to_s}"@#{@language} ;
    rdfs:comment "#{@pdv.upcase} Domain concept #{d.to_s}"@#{@language} ;
    #{@pdv}l-d:Domainvalue "#{d.to_s}"@#{@language} .\n]
                        printme(@pdv+'l-d',@pdv.capitalize+"Domain_#{id}",header,onto)
                        id+=1
                    end
                }
            end
        }
        return megadict
    end

    def build_turtle_RegisterTaxonomy(header,megadict,path)
        megadict['Register'] = Hash.new
        filename = path+@pdv+"-lemon-register"
        query = "select distinct(register) from patterns,entries where entries.entry_id = patterns.entry_id and #{@complete} order by register ASC;"
        q = @dbh.execute(query)
        id = 1
        onto=%Q[#{@pdv}l-r:#{@pdv.capitalize}Register rdf:type lexinfo:Register, rdfs:Class,owl:Class ;
    rdfs:label "#{@pdv.upcase} Register"@en ;
    rdfs:comment "Register defined in #{@pdv.upcase}"@en .\n]
        printme(@pdv+'l-r',@pdv.capitalize+'Register',header,onto)
        onto=%Q[#{@pdv}l-r:Registervalue rdf:type rdf:Property, owl:DatatypeProperty ;
    rdfs:label "Register string"@en ;
    rdfs:comment "string of Register defined in #{@pdv.upcase}"@en ;
    rdfs:domain #{@pdv}l-r:#{@pdv.capitalize}Register ;
    rdfs:range xsd:string .\n]
        printme(@pdv+'l-r','Registervalue',header,onto)
        q.each{|row|
            if row['register'].to_s != ''
                reg_ar=Array.new
                if row['register'].include?(';')
                    reg_ar=row['register'].split(';')
                else
                    reg_ar << row['register']
                end
                reg_ar.each{|r|
                    if not megadict['Register'].include?(r.to_s)
                        megadict['Register'][r.to_s] = id
                        onto=%Q[#{@pdv}l-r:#{@pdv.capitalize}Register_#{id} rdf:type #{@pdv}l-r:#{@pdv.capitalize}Register, owl:NamedIndividual ;
    rdfs:label "#{r.to_s}"@#{@language} ;
    rdfs:comment "Register concept #{r.to_s}"@#{@language} ;
    #{@pdv}l-r:Registervalue "#{r.to_s}"@#{@language} .\n]
                        printme(@pdv+'l-r',@pdv.capitalize+"Register_#{id}",header,onto)
                        id+=1
                    end
                }
            end
        }
        return megadict
    end

    def build_turtle_ContextualRolesTaxonomy(header,megadict,path)
        megadict['CoRoTaxo'] = Hash.new
        filename = path+@pdv+"-lemon-CoRoTaxo"
        onto=%Q[#{@pdv}l-t:#{@pdv.capitalize}ContextualRole rdf:type rdfs:Class,owl:Class ;
    rdfs:label "#{@pdv.upcase} Contextual Role"@en ;
    rdfs:comment "Contextual Role defined in #{@pdv.upcase}"@en .\n]
        printme(@pdv+'l-t',@pdv.capitalize+"ContextualRole",header,onto)
#<!--List of Contextual Roles-->\n]
        query =  "select distinct(semantic_role) from objects,entries where entries.entry_id = objects.entry_id and #{@complete} order by semantic_role ASC;"
        q = @dbh.execute(query)
        id = 1
        cr_ar = []
        q.each{|row|
            if row['semantic_role'].to_s != ''
                if row['semantic_role'] =~ /[,|;.]+/
                    cr_ar=row['semantic_role'].split(/[,|;.]+/)
                else
                    cr_ar << row['semantic_role'].strip
                end
                for i in cr_ar
                    if (not i.nil?) and i != ''  and (not i.strip.nil?)
                        cr = i.strip.gsub(/[ \/\[\]]+/, '_')
                        if not megadict['CoRoTaxo'].include? cr
                            megadict['CoRoTaxo'][cr] = id
                            onto=%Q[#{@pdv}l-t:#{@pdv.capitalize}ContextualRole_#{id} rdf:type #{@pdv}l-t:#{@pdv.capitalize}ContextualRole, owl:NamedIndividual ;
    rdfs:label "Contextual Role #{cr}"@#{@language} ;
    rdfs:comment "Contextual Role #{cr} defined in #{@pdv.upcase}"@#{@language} .\n]
                            printme(@pdv+'l-t',@pdv.capitalize+"ContextualRole_#{id}",header,onto)
                            id += 1
                        end
                    end
                end
            end
        }
        return megadict
    end

    def get_secondary_implicature(entry_id, pattern_id)
        if entry_id == '' or entry_id == nil or pattern_id == '' or pattern_id == nil
            return ''
        end
        q = "SELECT implicature FROM implicatures WHERE entry_id=?"\
            " AND pattern_id=?"
        q2 = @dbh.execute(q, entry_id, pattern_id).fetch_all().
                map{|si| si.to_s.strip.gsub(/&/, '&amp;')}.to_a.join("||")
        return q2.to_s
    end


    #create turtle format lemon senses
    def turtle_senses(ei,num,header,norm,exploit,semclass,megadict,babelurl,examples_link)
        db_query3 = "SELECT domain,register,idiom,phrasal,framenet,primary_implicature,semclass,pattern_id FROM patterns "\
                "WHERE entry_id='#{ei}' and number=#{num}"
        q3 = @dbh.execute(db_query3)
        sense=''
        q3.each{|row|
            patsense = ''
            ok_framenet = 0
            sec_impl = get_secondary_implicature(ei,row['pattern_id'])
            sense+=%Q[#{@pdv}l:#{@pdv.upcase}_LexicalSense_#{ei}_#{num} rdf:type lemon:LexicalSense, owl:NamedIndividual ;
    lemon:reference #{@pdv}l-p:#{@pdv.upcase}_PatSense_#{ei}_#{num} ;\n]
            patsense+=%Q[#{@pdv}l-p:#{@pdv.upcase}_PatSense_#{ei}_#{num} rdf:type #{@pdv}l-p:PatSense, owl:NamedIndividual ;\n]
            if sec_impl.to_s != ''
                patsense+="    #{@pdv}l-p:secondaryImplicature \"#{sec_impl.to_s.gsub(/"/, "'")}\" ;\n"
            end
            if  (not semclass.nil?) and semclass != ""
                semclass_dict = Array.new
                if semclass=~/,/
                    semclass_dict = semclass.split(/,/)
                    semclass_dict.each{|sc|
                        if (not sc.nil?) 
                            patsense+=%Q[    #{@pdv}l-p:semanticClass "#{sc.strip}" ;\n]
                        end
                    }
                else
                    patsense+=%Q[    #{@pdv}l-p:semanticClass "#{semclass.strip}" ;\n]
                end
            end
            if babelurl.include?('http')
              patsense +=%Q[    owl:sameAs <#{babelurl}>;\n]
            end
            fn = row['framenet']
            if (not fn.nil?) and (not fn.strip.nil?) and (not fn.include? "20") and (not fn.include? "NOT") and (not fn.include? "?") and (not fn.include? ")") and (not fn.include? "]") and (not fn.downcase.include? "not in") and (not fn.downcase.include? "should") and (not fn.include? " the ")
                fndict = Array.new
                if fn.include?(',')
                    fndict = fn.split(/,/)
                else
                    fndict << fn
                end
                ok_framenet = 1
                for i in 1..fndict.size
                    patsense +=%Q[    #{@pdv}l-p:frameNetSense <https://framenet2.icsi.berkeley.edu/fnReports/data/frame/#{fndict[i-1].strip.gsub(' ','_')}.xml?banner=> ;\n]
                end
            end
            #no definition in LEMON term: the implicature will be put as a property of the SemPat ontology concept
            if row['primary_implicature'] != ''
                p_i =row['primary_implicature'].to_s
                p_i=p_i.gsub('"', "'")
                patsense+=%Q[    #{@pdv}l-p:primaryImplicature "#{p_i}" .\n]
            end
            if row['domain'].to_s != ''
                dom_ar = Array.new
                if row['domain'].to_s.include?(';')
                    dom_ar = row['domain'].to_s.split(/;/)
                else
                    dom_ar << row['domain'].to_s
                end
                dom_ar.each{|d|
                    sense +=%Q[    #{@pdv}l-s:domain #{@pdv}l-d:#{@pdv.capitalize}Domain_#{megadict['Domain'][d.to_s]} ;\n]
                }
            end
            if row['register'].to_s != ''
                reg_ar = Array.new
                if row['register'].to_s.include?(';')
                    reg_ar = row['register'].to_s.split(/;/)
                else
                    reg_ar << row['register'].to_s
                end
                reg_ar.each{|r|
                    sense +=%Q[    #{@pdv}l-s:registerRef #{@pdv}l-r:#{@pdv.capitalize}Register_#{megadict['Register'][r.to_s]} ;\n]
                }
            end
            if norm != "" and (!examples_link[num].nil?) and (!examples_link[num]['n'].nil?)
                examples_link[num]['n'].each{|link|
                    sense += "    lemon:example #{link} ;\n"
                }
            end
            if exploit != "" and (!examples_link[num].nil?) and (!examples_link[num]['a'].nil?)
                examples_link[num]['a'].each{|link|
                    sense += "    lemon:example #{link} ;\n"
                }
            end
            sense+="    "+@pdv+"l-s:frameSenseOf "+@pdv+"l:"+@pdv.upcase+"_Pattern_#{ei}_#{num} .\n"
            printme(@pdv+'l',@pdv.upcase+"_LexicalSense_#{ei}_#{num}",header,sense)
            printme(@pdv+'l-p',@pdv.upcase+"_PatSense_#{ei}_#{num}",header,patsense)
        }
    end

    def turtle_patterns(ei,num,header,norm,exploit,semclass,megadict,path)
        db_query1 = "SELECT * FROM patterns WHERE entry_id='#{ei}' and number=#{num}"
        q1 = @dbh.execute(db_query1)
        # First build all arguments
        iter=0
        q1.each{|row|
            pat_id = row['pattern_id']
            #Deliver SPOCA MODEL each being a lemon:Argument
            #subject
            res,iter=turtle_argument(ei,pat_id,'S',header,num,megadict,false,iter,path)
            #verb/predicator
            res,iter=turtle_argument(ei,pat_id,'P',header,num,megadict,false,iter,path)
            #indirect object
            res,iter=turtle_argument(ei,pat_id,'I',header,num,megadict,false,iter,path)
            #object
            res,iter=turtle_argument(ei,pat_id,'O',header,num,megadict,false,iter,path)
            #complement
            res,iter=turtle_argument(ei,pat_id,'C',header,num,megadict,false,iter,path)
            #adverbial
            #clausals are adverbials, iter reinit for future use
            iter=0
            res,iter=turtle_argument(ei,pat_id,'A',header,num,megadict,false,iter,path)
            #dont forget clausals
            query="SELECT claus_to,claus_ing,claus_that,claus_wh,claus_quote,claus_opt,ocl_to,ocl_ing,ocl_that,ocl_wh,ocl_quote,ocl_opt FROM patterns "\
                    "WHERE entry_id='#{ei}' and number=#{num}"
            q3 = @dbh.execute(query)
            position = ''
            miniheader = @pdv+"l:"+@pdv.upcase+"_Arg_Pattern_#{ei}_#{num} rdf:type lemon:Argument, owl:NamedIndividual ;\n"
            opt=false
            q3.each{|row|
                if row['claus_opt'] == true or row['ocl_opt']==true
                    opt=true
                end
                if row['claus_to'] == true or row['ocl_to']==true
                    position = 'A' if position == 'M'
                    position = 'M'if position == ''
                    pos=position
                    pos += '-'+iter.to_s if row['type'] =='A'
                    name = @pdv.upcase+"_Arg_A_#{pos}_#{ei}_#{num}"
                    argstr=miniheader+"    "+@pdv+"l-s:syntacticCategory "+@pdv+"l-s:ToInf ;\n"
                    argstr+="    lemon:optional true ;\n" if opt ==true
                    argstr+="    "+@pdv+"l-s:argStatus "+@pdv+"l-s:Prototypical .\n" if position =='M'
                    argstr+="    "+@pdv+"l-s:argStatus "+@pdv+"l-s:Alternative .\n" if position =='A'
                    printme(@pdv+'l',name,header,argstr)
                    iter+=1
                end
                if row['claus_ing'] == true or row['ocl_ing']==true
                    position = 'A' if position == 'M'
                    position = 'M'if position == ''
                    pos=position
                    pos += '-'+iter.to_s if row['type'] =='A'
                    name = @pdv.upcase+"_Arg_A_#{pos}_#{ei}_#{num}"
                    argstr=miniheader+"    "+@pdv+"l-s:syntacticCategory "+@pdv+"l-s:Ing ;\n"
                    argstr+="    lemon:optional true ;\n" if opt ==true
                    argstr+="    "+@pdv+"l-s:argStatus "+@pdv+"l-s:Prototypical .\n" if position =='M'
                    argstr+="    "+@pdv+"l-s:argStatus "+@pdv+"l-s:Alternative .\n" if position =='A'
                    filename =path+"#{name}"
                    printme(@pdv+'l',name,header,argstr)
                    iter+=1
                end
                if row['claus_that'] == true or row['ocl_that']==true
                    position = 'A' if position == 'M'
                    position = 'M'if position == ''
                    pos=position
                    pos += '-'+iter.to_s if row['type'] =='A'
                    name = @pdv.upcase+"_Arg_A_#{pos}_#{ei}_#{num}"
                    argstr=miniheader+"    "+@pdv+"l-s:syntacticCategory "+@pdv+"l-s:That ;\n"
                    argstr+="    lemon:optional true ;\n" if opt ==true
                    argstr+="    "+@pdv+"l-s:argStatus "+@pdv+"l-s:Prototypical .\n" if position =='M'
                    argstr+="    "+@pdv+"l-s:argStatus "+@pdv+"l-s:Alternative .\n" if position =='A'
                    printme(@pdv+'l',name,header,argstr)
                    iter+=1
                end
                if row['claus_wh'] == true or row['ocl_wh']==true
                    position = 'A' if position == 'M'
                    position = 'M'if position == ''
                    pos=position
                    pos += '-'+iter.to_s if row['type'] =='A'
                    name = @pdv.upcase+"_Arg_A_#{pos}_#{ei}_#{num}"
                    argstr=miniheader+"    "+@pdv+"l-s:syntacticCategory "+@pdv+"l-s:Wh ;\n"
                    argstr+="    lemon:optional true ;\n" if opt ==true
                    argstr+="    "+@pdv+"l-s:argStatus "+@pdv+"l-s:Prototypical .\n" if position =='M'
                    argstr+="    "+@pdv+"l-s:argStatus "+@pdv+"l-s:Alternative .\n" if position =='A'
                    printme(@pdv+'l',name,header,argstr)
                    iter+=1
                end
                if row['claus_quote'] == true or row['ocl_quote']==true
                    position = 'A' if position == 'M'
                    position = 'M'if position == ''
                    pos=position
                    pos += '-'+iter.to_s if row['type'] =='A'
                    name = @pdv.upcase+"_Arg_A_#{pos}_#{ei}_#{num}"
                    argstr=miniheader+"    "+@pdv+"l-s:syntacticCategory "+@pdv+"l-s:Quote ;\n"
                    argstr+="    lemon:optional true ;\n" if opt ==true
                    argstr+="    "+@pdv+"l-s:argStatus "+@pdv+"l-s:Prototypical .\n" if position =='M'
                    argstr+="    "+@pdv+"l-s:argStatus "+@pdv+"l-s:Alternative .\n" if position =='A'
                    printme(@pdv+'l',name,header,argstr)
                    iter+=1
                end
            }
        }
        q1 = @dbh.execute(db_query1)
        q1.each{|row|
            pat_id = row['pattern_id']
            pattern=%Q[#{@pdv}l:#{@pdv.upcase}_Pattern_#{ei}_#{num} rdf:type lemon:Frame, owl:NamedIndividual ;\n]
            pattern+="    "+@pdv+"l-s:isPhrasal \"true\" ;\n" if row['phrasal']
            pattern+="    "+@pdv+"l-s:isNoObj \"true\" ;\n" if row['object_none'] == true
            pattern+="    "+@pdv+"l-s:isNoAdv \"true\" ;\n" if row['advl_none'] == true
            pattern+="    "+@pdv+"l-s:isIdiom \"true\" ;\n" if row['idiom']
            pattern+="    "+@pdv+"l-s:senseFrequencyOf "+@pdv+"l:"+@pdv.upcase+"_Freq_norm_#{ei}_#{num} ;\n" if norm != ""
            pattern+="    "+@pdv+"l-s:senseFrequencyOf "+@pdv+"l:"+@pdv.upcase+"_Freq_exploitation_#{ei}_#{num} ;\n" if exploit != ""
            #Deliver SPOCA MODEL each being a lemon:Argument
            #subject
            iter=0
            res,iter=turtle_argument(ei,pat_id,'S',header,num,megadict,true,iter,path)
            pattern+=res
            #verb/predicator
            iter=0
            res,iter=turtle_argument(ei,pat_id,'P',header,num,megadict,true,iter,path)
            pattern+=res
            #indirect object
            iter=0
            res,iter=turtle_argument(ei,pat_id,'I',header,num,megadict,true,iter,path)
            pattern+=res
            #object
            iter=0
            res,iter=turtle_argument(ei,pat_id,'O',header,num,megadict,true,iter,path)
            pattern+=res
            #complement
            iter=0
            res,iter=turtle_argument(ei,pat_id,'C',header,num,megadict,true,iter,path)
            pattern+=res
            #adverbial
            #clausals are adverbials, iter reinit for future use
            iter=0
            res,iter=turtle_argument(ei,pat_id,'A',header,num,megadict,true,iter,path)
            pattern+=res
            #dont forget clausals
            query="SELECT claus_to,claus_ing,claus_that,claus_wh,claus_quote,claus_opt,ocl_to,ocl_ing,ocl_that,ocl_wh,ocl_quote,ocl_opt FROM patterns "\
                    "WHERE entry_id='#{ei}' and number=#{num}"
            q3 = @dbh.execute(query)
            position = ''
            miniheader = @pdv+"l:"+@pdv.upcase+"_Arg_Pattern_#{ei}_#{num} rdf:type lemon:Argument, owl:NamedIndividual ;\n"
            opt=false
            q3.each{|row|
                if row['claus_opt'] == true or row['ocl_opt']==true
                    opt=true
                end
                if row['claus_to'] == true or row['ocl_to']==true
                    position = 'A' if position == 'M'
                    position = 'M'if position == ''
                    pos=position
                    pos += '-'+iter.to_s if row['type'] =='A'
                    name = @pdv.upcase+"_Arg_A_#{pos}_#{ei}_#{num}"
                    pattern += "    "+@pdv+"l-s:adverbial "+@pdv+"l:#{name} ;\n"
                    iter+=1
                end
                if row['claus_ing'] == true or row['ocl_ing']==true
                    position = 'A' if position == 'M'
                    position = 'M'if position == ''
                    pos=position
                    pos += '-'+iter.to_s if row['type'] =='A'
                    name = @pdv.upcase+"_Arg_A_#{pos}_#{ei}_#{num}"
                    pattern += "    "+@pdv+"l-s:adverbial "+@pdv+"l:#{name} ;\n"
                    iter+=1
                end
                if row['claus_that'] == true or row['ocl_that']==true
                    position = 'A' if position == 'M'
                    position = 'M'if position == ''
                    pos=position
                    pos += '-'+iter.to_s if row['type'] =='A'
                    name = @pdv.upcase+"_Arg_A_#{pos}_#{ei}_#{num}"
                    pattern += "    "+@pdv+"l-s:adverbial "+@pdv+"l:#{name} ;\n"
                    iter+=1
                end
                if row['claus_wh'] == true or row['ocl_wh']==true
                    position = 'A' if position == 'M'
                    position = 'M'if position == ''
                    pos=position
                    pos += '-'+iter.to_s if row['type'] =='A'
                    name = @pdv.upcase+"_Arg_A_#{pos}_#{ei}_#{num}"
                    pattern += "    "+@pdv+"l-s:adverbial "+@pdv+"l:#{name} ;\n"
                    iter+=1
                end
                if row['claus_quote'] == true or row['ocl_quote']==true
                    position = 'A' if position == 'M'
                    position = 'M'if position == ''
                    pos=position
                    pos += '-'+iter.to_s if row['type'] =='A'
                    name = @pdv.upcase+"_Arg_A_#{pos}_#{ei}_#{num}"
                    pattern += "    "+@pdv+"l-s:adverbial "+@pdv+"l:#{name} ;\n"
                    iter+=1
                end
            }
            pattern+=%Q[    #{@pdv}l:frameSense #{@pdv}l:#{@pdv.upcase}_LexicalSense_#{ei}_#{num} .\n]
            printme(@pdv+'l',@pdv.upcase+"_Pattern_#{ei}_#{num}",header,pattern)
            if norm != ""
                pattern=%Q[#{@pdv}l:#{@pdv.upcase}_Freq_norm_#{ei}_#{num} rdf:type #{@pdv}l-s:Frequency, owl:NamedIndividual ;
    #{@pdv}l-s:ofCorpus "#{@corpus.upcase}" ;
    #{@pdv}l-s:frequencyValue #{norm} .\n]
                printme(@pdv+'l',@pdv.upcase+"_Freq_norm_#{ei}_#{num}",header,pattern)
            end
            if exploit != ""
                pattern = %Q[#{@pdv}l:#{@pdv.upcase}_Freq_exploitation_#{ei}_#{num} rdf:type #{@pdv}l-s:Frequency, owl:NamedIndividual ;
    #{@pdv}l-s:ofCorpus "#{@corpus.upcase}" ;
    #{@pdv}l-s:frequencyValue #{exploit} .\n]
                printme(@pdv+'l',@pdv.upcase+"_Freq_exploitation_#{ei}_#{num}",header,pattern)
            end
        }
    end

    def turtle_argument(ei,pat_id,type,header,num,megadict,computepat,iter,path)
        corr ={"S"=>@pdv+"l-s:subject","C"=>@pdv+"l-s:complement","I"=>@pdv+"l-s:indirectObject","O"=>@pdv+"l-s:directObject","A"=>@pdv+"l-s:adverbial"}
        arglinks = ''
        if ["S","I","O","C"].include?(type)
            db_query = "SELECT * FROM objects WHERE entry_id='#{ei}' AND pattern_id = #{pat_id} AND object_type='#{type}' AND position IN ('M', 'A') ORDER BY position DESC"
            q = @dbh.select_all(db_query)
            if q.length > 0
                q.each{|row|
                    advgroup = ''
                    allopt = false
                    arglink,iter=make_turtle_object(header,ei, pat_id, type,row,num,allopt,megadict,computepat,advgroup,iter,'',path)
                    arglinks += arglink
                }
            end
        end
        if type == 'P'
            q = "SELECT * FROM patterns WHERE entry_id=? AND pattern_id=?"
            q2 = @dbh.execute(q, ei, pat_id)
            row = q2.fetch_all()[0]
            if row['verb_form'] != ''
                arglinks = "    "+@pdv+"l-s:Predicator \"#{row['verb_form']}\" ;\n"
            else
                arglinks = "    "+@pdv+"l-s:Predicator \"#{ei}\" ;\n"
            end
        elsif type == 'A'
            db_query = "SELECT DISTINCT advl_group, advl_function, advl_main_opt FROM objects WHERE entry_id='#{ei}' AND pattern_id = #{pat_id} AND object_type='A' ORDER BY advl_group ASC"
            q = @dbh.select_all(db_query)
            if q.length > 0
                q.each{|row_m|
                    db_query2 = "SELECT * FROM objects WHERE entry_id='#{ei}' AND pattern_id = #{pat_id} AND object_type='A' AND advl_group=#{row_m['advl_group']} AND position<>'X' ORDER BY position DESC"
                    q2 = @dbh.select_all(db_query2)
                    q2.each{|row|
                        allopt=true if row_m['advl_main_opt'] == true
                        advgroup = '-'+row_m['advl_group'].to_s
                        #duplicate prepositions
                        if row['headword'] != '' 
                            if row['headword'] =~ /[,|;.]+/
                                lex_ar = Array.new
                                lex_ar=row['headword'].split(/[,|;.]+/)
                                for i in lex_ar
                                    if (not i.nil?) and i != '' and (not i.strip.nil?)
                                        allopt=true if i.include?'('
                                        prep=megadict['Prepset'][i.strip.gsub(/[ \/\[\]]+/, '_').gsub(/\}\{\(\)=/,'')]
                                        arglink,iter=make_turtle_object(header,ei, pat_id, type,row,num,allopt,megadict,computepat,advgroup,iter,prep,path)
                                        arglinks += arglink
                                    end
                                end
                            else
                                if (not row['headword'].nil?) and row['headword'] != '' and (not row['headword'].strip.nil?)
                                    allopt=true if row['headword'].include?'('
                                    prep=megadict['Prepset'][row['headword'].strip.gsub(/[ \/\[\]]+/, '_').gsub(/\}\{\(\)=/,'')]
                                    arglink,iter=make_turtle_object(header,ei, pat_id, type,row,num,allopt,megadict,computepat,advgroup,iter,prep,path)
                                    arglinks += arglink
                                end
                            end
                        else
                            arglink,iter=make_turtle_object(header,ei, pat_id, type,row,num,allopt,megadict,computepat,advgroup,iter,'',path)
                            arglinks += arglink
                        end
                    }
                }
            end
        end
        return arglinks,iter
    end

    #prepare object/subject text
    def make_turtle_object(header,ei, pat_id, type, row,num,allopt,megadict,computepat,advgroup,iter,prep,path)
        corr ={"S"=>@pdv+"l-s:subject","C"=>@pdv+"l-s:complement","I"=>@pdv+"l-s:indirectObject","O"=>@pdv+"l-s:directObject","A"=>@pdv+"l-s:adverbial"}
        position = row['position']
        pos = position
        arglink=''
        string = ''
        if (row['semantic_type'].to_s != '' or row['lexset_items'].to_s != '') and type !='A'
            string +="    "+@pdv+"l-s:syntacticCategory "+@pdv+"l-s:NounPhrase ;\n"
        end
        if type =='C'
            string+="    "+@pdv+"l-s:preposition "+@pdv+"l:"+@pdp.upcase+"_LexicalEntry_as ;\n" if row['complement_as']
            string+="    "+@pdv+"l-s:determiner \"#{row['complement_det'].to_s}\" ;\n" if row['complement_det'].to_s != ''
        end
        if type =='A'
            string+="    "+@pdv+"l-s:preposition "+@pdv+"l:"+@pdp.upcase+"_LexicalEntry_#{prep} ;\n" if prep != ""
            string+="    "+@pdv+"l-s:syntacticCategory "+@pdv+"l-s:PrepositionalPhrase ;\n" if row['headword'].to_s != ""
            string+="    "+@pdv+"l-s:adverbialFunction "+@pdv+"l-s:AF_#{row['advl_function'].gsub(/ +/,'_')} ;\n" if row['advl_function'].to_s != ""
        end
        string+="    "+@pdv+"l-s:syntacticCategory "+@pdv+"l-s:ToInf ;\n"if row['ocl_to'] == true
        string+="    "+@pdv+"l-s:syntacticCategory "+@pdv+"l-s:Ing ;\n"if row['ocl_ing'] == true
        string+="    "+@pdv+"l-s:syntacticCategory "+@pdv+"l-s:That ;\n"if row['ocl_that'] == true
        string+="    "+@pdv+"l-s:syntacticCategory "+@pdv+"l-s:Quote ;\n"if row['ocl_quote'] == true
        string+="    "+@pdv+"l-s:syntacticCategory "+@pdv+"l-s:Wh ;\n"if row['ocl_wh'] == true
        string+="    "+@pdv+"l-s:determiner \"#{row['det_quant'].to_s}\" ;\n" if row['det_quant'].to_s != ''
        if row['semantic_type'].to_s != ''
            st = megadict['CPASO'][row['semantic_type'].to_s.split(/[0-9]/)[0].strip.gsub(/ +/, '_')] #strip number...but database needs cleaning
            # exclude malformations from pdev
            if (not st.nil?) and (not st.strip.nil?)
                st_ar = Array.new
                if st =~ /[,|;.]+/
                    st_ar=st.split(/[,|;.]+/)
                else
                    st_ar << st
                end
                st_ar.each{|st|
                    string+="    "+@pdv+"l-s:SemanticType "+@pdv+"l-o:"+@pdv.capitalize+"SemanticType_#{st.strip} ;\n"
                }
            end
        end
        if row['semantic_role'].to_s != ''
            cr_ar = []
            if row['semantic_role'] =~ /[,|;.]+/
                cr_ar=row['semantic_role'].split(/[,|;.]+/)
            else
                cr_ar << row['semantic_role'].strip
            end
            for i in cr_ar
                if (not i.nil?) and i != ''  and (not i.strip.nil?)
                    cr = i.strip.gsub(/[ \/\[\]]+/, '_')
                    ncr = megadict['CoRoTaxo'][cr]
                    string+="    "+@pdv+"l-s:ContextualRole "+@pdv+"l-t:"+@pdv.capitalize+"ContextualRole_#{ncr} ;\n"
                end
            end
        end
        if allopt ==true or row['optional'] == true
            string+="    lemon:optional true ;\n"
        end
        if row['lexset_items'] != '' and row['semantic_type']==''
            if row['lexset_items'] =~ /[,|;.]+/
                lex_ar = Array.new
                lex_ar=row['lexset_items'].split(/[,|;.]+/)
                for i in lex_ar
                    if (not i.nil?) and i != '' and (not i.strip.nil?)
                        pos = position
                        pos += advgroup 
                        pos += '-'+iter.to_s if position =='A'
                        name = @pdv.upcase+"_Arg_#{type}_#{pos}_#{ei}_#{num}"
                        arglink += "    #{corr[type]} "+@pdv+"l:#{name} ;\n"
                        argstr = @pdv+"l:#{name} rdf:type lemon:Argument, owl:NamedIndividual ;\n"
                        argstr+=string
                        argstr+="    "+@pdv+"l-s:LexicalSet "+@pdv+"l:"+@pdn.upcase+"_LexicalEntry_#{megadict['Lexset'][i.strip.gsub(/[ \/\[\]]+/, '_').gsub(/\}\{\(\)=/,'')]} ;\n"
                        iter+=1
                        argstr+="    "+@pdv+"l-s:argStatus "+@pdv+"l-s:Prototypical .\n" if position =='M'
                        argstr+="    "+@pdv+"l-s:argStatus "+@pdv+"l-s:Alternative .\n" if position =='A'
                        filename = path+"#{name}"
                        if computepat == false
                            printme(@pdv+'l',name,header,argstr)
                        end
                    end
                end
            else
                i=row['lexset_items']
                if (not i.nil?) and i != ''  and (not i.strip.nil?)
                    pos = position
                    pos += advgroup
                    pos += '-'+iter.to_s if position =='A'
                    name = @pdv.upcase+"_Arg_#{type}_#{pos}_#{ei}_#{num}"
                    arglink += "    #{corr[type]} "+@pdv+"l:#{name} ;\n"
                    argstr = @pdv+"l:#{name} rdf:type lemon:Argument, owl:NamedIndividual ;\n"
                    argstr += string
                    argstr +="    "+@pdv+"l-s:LexicalSet "+@pdv+"l:"+@pdn.upcase+"_LexicalEntry_#{megadict['Lexset'][i.strip.gsub(/[ \/\[\]]+/, '_').gsub(/\}\{\(\)=/,'')]} ;\n"
                    iter+=1
                    argstr+="    "+@pdv+"l-s:argStatus "+@pdv+"l-s:Prototypical .\n" if position =='M'
                    argstr+="    "+@pdv+"l-s:argStatus "+@pdv+"l-s:Alternative .\n" if position =='A'
                    filename = path+"#{name}"
                    if computepat == false
                        printme(@pdv+'l',name,header,argstr)
                    end
                end
            end
        else
            pos = position
            pos += advgroup
            pos += '-'+iter.to_s if position =='A'
            name = @pdv.upcase+"_Arg_#{type}_#{pos}_#{ei}_#{num}"
            arglink += "    #{corr[type]} "+@pdv+"l:#{name} ;\n"
            argstr = @pdv+"l:#{name} rdf:type lemon:Argument, owl:NamedIndividual ;\n"
            argstr+=string
            iter+=1
            argstr+="    "+@pdv+"l-s:argStatus "+@pdv+"l-s:Prototypical .\n" if position =='M'
            argstr+="    "+@pdv+"l-s:argStatus "+@pdv+"l-s:Alternative .\n" if position =='A'
            filename = path+"#{name}"
            if computepat == false
                printme(@pdv+'l',name,header,argstr)
            end
        end
        return arglink,iter
    end
end
