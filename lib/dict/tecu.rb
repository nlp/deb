require 'dict/dict-sedna'

class TeCu < DictSedna
  attr_accessor :hypers

  def initialize(collection, journal_path, key_path='', database = 'deb', admindict = nil)
    super
    @servlet = TecuServlet
    @hypers = get_hypers
  end

  def get_doc(id)
    xml = get(id)
    if xml != ''
      doc = load_xml_string(xml)
      #termin pro hyperonyma
      doc.find('/entry/hyper').each{|h|
        h['term'] = get_term(h['id'])
      }
      #cesty
      paths = get_path(id)
      xmlpaths = XML::Node.new('paths')
      doc.root << xmlpaths
      paths.each{|path|
        xmlpath = XML::Node.new('path')
        xmlpaths << xmlpath
        path.each{|n|
          if n != '0'
            entry = XML::Node.new('path_node')
            entry['id'] = n
            entry['term'] = get_term(n)
            xmlpath << entry
          end
        }
      }
      #klikatelne see, also
      doc.find('/entry/sees/see[not(@entry-id)]|/entry/alsos/also[not(@entry-id)]').each{|see|
        query = 'for $x in collection("tecu")/entry[terms/term="'+see.content+'"] return data($x/@id)'
        @sedna.query(query).each{|si|
          see['entry-id'] = si
        }
      }
      xml = doc.to_s
      $stderr.puts xml
    end
    return xml
  end

  def highlight_def(id, xml, servername, timestamp)
    doc = load_xml_string(xml)
    doc.find('/entry/defs/def').each{|df|
      result = `/var/www/termcheck/run.cgi "#{df.content}" "/tezk"`
      result = result.gsub('( ', '(').gsub(/ ([,;\)\.])/, '\1')
      if result != ""
        newdf = '<def num="'+df['num']+'">'+result+'</def>'
        df.parent << XML::Document.string(newdf).root.copy(true)
        df.remove!
      end
    }
    xmlold = get(id)
    docold = load_xml_string(xmlold)
    if doc.root['timestamp'] == timestamp
      $stderr.puts 'SAVE HIGHLIGHT'
      update(id, doc.to_s)
    end
  end

  def get_term(id)
    xml = get(id)
    term = ''
    if xml != ''
      doc = load_xml_string(xml)
      term = doc.find('/entry/terms/term[@lang="cs"]').to_a.first.content.to_s unless doc.find('/entry/terms/term[@lang="cs"]').to_a.first.nil?
    end
    return term
  end

  def search(search, status='')
    res = []
    status_query = ''
    status_query = '$x/@status="'+status+'" and' if status != ''
    query = 'for $x in collection("tecu")/entry where '+status_query+' (some $t in $x/terms/term satisfies fn:matches(lower-case($t), "(^| |\-|\()'+search.downcase+'")) return <a>{$x/terms/term[1]/text()}|{data($x/@id)}|{data($x/@status)}</a>'
    @sedna.query(query).each{|e|
      ear = e.gsub(/<\/?a>/,'').split('|')
      res << {'id'=>ear[1], 'head'=>ear[0], 'status'=>ear[2], 'highlight'=>ear[0].gsub(search, '<span class="search-highlight">'+search+'</span>')}
    }
    re_search = /#{search}/u
    res = res.sort{|a, b|
      sort1 = (((a['head']=~re_search).nil?)?1000:(a['head']=~re_search)) <=> (((b['head']=~re_search).nil?)?1000:(b['head']=~re_search))
      (sort1 == 0)? a['head']<=>b['head'] : sort1
    }
    return res
  end

  def get_deleted
    res = []
    query = 'for $x in collection("tecudel")/entry return <a>{$x/terms/term[1]/text()}|{data($x/@id)}|{data($x/@status)}</a>'
    @sedna.query(query).each{|e|
      ear = e.gsub(/<\/?a>/,'').split('|')
      res << {'id'=>ear[1], 'head'=>ear[0], 'status'=>0}
    }
    return res.sort{|x,y| x['head']<=>y['head']}
  end

  def get_hypers
    res = []
    query = 'for $x in collection("tecu")/entry/hyper return data($x/@id)'
    @sedna.query(query).each{|e|
      res << e
    }
    return res.uniq
  end

  def get_subtree(id, hypers)
    res = []
    query = 'for $x in collection("tecu")/entry[hyper/@id="'+id+'"] return <a>{data($x/@id)};{$x/terms/term[1]/text()}</a>'
    #query = '[entry[hyper/@id="'+id+'"]]'
    #xquery_to_hash(query).each{|id, xml|
    @sedna.query(query).each{|r|
      #doc = load_xml_string(xml.to_s)
      id, head, expend = r.sub('<a>','').sub('</a>','').split(';')
      new = {'id'=>id, 'head'=>head, 'expandable'=>0}
      new['expandable'] = 1 if @hypers.include?(id)
      #new['head'] = doc.find('terms/term').first.content.to_s unless doc.find('terms/term').first.nil?
      #new['expandable'] = xquery_to_array('[entry[hyper/@id="'+id+'"]]').length 
      res << new
    }
    return res.sort{|x,y| x['head'].to_s.downcase<=>y['head'].to_s.downcase}
  end

  def export_tree_struct(hypers, up, level)
    res = ''
    if up == '0'
      get_top.each{|h|
        res += '* ' + h['id'] + ': ' + h['head'] + "\n"
        res += export_tree_struct(hypers, h['id'], level+1) if h['expandable'] > 0
      }
    else
      get_subtree(up, hypers).each{|h|
        res += ' '*level + '* ' + h['id'] + ': ' + h['head'] + "\n"
        res += export_tree_struct(hypers, h['id'], level+1) if h['expandable'] > 0 and level < 15
      }
    end
    return res
  end

  def get_export_subtree(id, hypers, to_export)
    get_subtree(id, hypers).each{|h|
      unless to_export.include?(h['id'])
        to_export << h['id']
        if h['expandable'] > 0
          to_export = get_export_subtree(h['id'], hypers, to_export)
        end
      end
    }
    return to_export
  end

  def export_subtree(id, hypers)
    to_export = [id]
    to_export = get_export_subtree(id, hypers, to_export)
    to_csv = []
    to_export.each{|eid|
      xml = get(eid)
      if xml != ''
        doc = load_xml_string(xml)
        rows = [ [eid,doc.root['status'],'','','','','','','','','',''] ]
        ri = 0
        doc.find('terms/term').each{|t|
          rows[ri] = ['','','','','','','','','','','',''] if rows[ri].nil?
          rows[ri][2] = t.content
          rows[ri][3] = t['lang']
          ri += 1
        }
        ri = 0
        doc.find('defs/def').each{|t|
          rows[ri] = ['','','','','','','','','','','',''] if rows[ri].nil?
          rows[ri][4] = t.content
          ri += 1
        }
        ri = 0
        doc.find('domains/dom').each{|t|
          rows[ri] = ['','','','','','','','','','','',''] if rows[ri].nil?
          rows[ri][5] = t.content
          ri += 1
        }
        ri = 0
        doc.find('refs/ref').each{|t|
          rows[ri] = ['','','','','','','','','','','',''] if rows[ri].nil?
          rows[ri][6] = t.content
          ri += 1
        }
        ri = 0
        doc.find('hyper').each{|t|
          rows[ri] = ['','','','','','','','','','','',''] if rows[ri].nil?
          rows[ri][7] = get_term(t['id'])
          ri += 1
        }
        ri = 0
        doc.find('sees/see').each{|t|
          rows[ri] = ['','','','','','','','','','','',''] if rows[ri].nil?
          rows[ri][8] = t.content
          rows[ri][9] = t['lang']
          ri += 1
        }
        ri = 0
        doc.find('alsos/also').each{|t|
          rows[ri] = ['','','','','','','','','','','',''] if rows[ri].nil?
          rows[ri][10] = t.content
          rows[ri][11] = t['lang']
          ri += 1
        }
      end
      to_csv += rows
    }
    return to_csv
  end

  def get_top
    res = []
    query = '[entry[not(hyper)]]'
    xquery_to_hash(query).each{|id, xml|
      doc = load_xml_string(xml.to_s)
      new = {'id'=>id}
      next if doc.find('terms/term').first.nil?
      new['head'] = doc.find('terms/term').first.content.to_s unless doc.find('terms/term').first.nil?
      new['expandable'] = xquery_to_array('[entry[hyper/@id="'+id+'"]]').length 
      res << new 
    }
    return res.sort{|x,y| x['head'].downcase<=>y['head'].downcase}
  end

  def get_path(id)
    path = []
    xml = get(id)
    if xml != ''
      path = get_up(id, [[]], 0, 0)
    end
    return path.sort{|a,b| b.length<=>a.length}
  end
  def get_up(id, path_array, path_num, max_ar)
    xml = get(id)
    return path_array if xml == ''
    doc = load_xml_string(xml)
    paths = {}
    offset = 0
    doc.root.find('//hyper').each{|h|
      if offset == 0
        paths[h['id']] = path_num
      else
        max_ar += 1
        path_array[max_ar] = path_array[path_num].clone
        paths[h['id']] = max_ar
      end
      offset += 1
    }
    if doc.root.find('//hyper').length > 0
      doc.root.find('//hyper').each{|h|
        act_path = paths[h['id']]
        next if path_array[act_path].include?(h['id'])
        path_array[act_path] << h['id']
        path_array = get_up(h['id'].to_s, path_array, act_path, max_ar)
      }
    else
      path_array[path_num] << '0'
    end
    return path_array
  end

  def get_perm(user, dict=@dictcode)
    xml = @admindict.get('user'+user)
    doc = load_xml_string(xml.to_s)
    perm_ar = doc.find("//service[@code='tecu']/dict[@code='"+dict+"']").to_a
    if perm_ar.size > 0
      perm = perm_ar.first['perm'].to_s
      return perm
    else
      return nil
    end
  end

  def get_stats
    stat = {}
    #pocet hesel
    stat['entries'] = @sedna.query('count(collection("tecu")/entry)')[0]
    #pocet hesel podle statusu
    stat['status_pouz'] = @sedna.query('count(collection("tecu")/entry[@status=2])')[0]
    stat['status_auto'] = @sedna.query('count(collection("tecu")/entry[@status=3 or @status=5])')[0]
    stat['status_term'] = @sedna.query('count(collection("tecu")/entry[@status=1])')[0]
    #pocet prekladu podle jazyku
    stat['lang'] = []
    @sedna.query('for $x in distinct-values(data(collection("tecu")/entry/terms/term/@lang)) return <a>{$x};{count(collection("tecu")/entry/terms/term[@lang=$x])}</a>').each{|x|
      langstat = x.sub('<a>','').sub('</a>','').split(';')
      stat['lang'] << {'lang'=>langstat[0], 'count'=>langstat[1]}
    }
    #pocet definic
    stat['defs'] = @sedna.query('count(collection("tecu")/entry/defs/def)')[0]
    #pocet polozek obor
    stat['doms'] = []
    @sedna.query('for $x in distinct-values(collection("tecu")/entry/domains/dom) return <a>{$x};{count(collection("tecu")/entry[domains/dom=$x])}</a>').each{|x|
      langstat = x.sub('<a>','').sub('</a>','').split(';')
      stat['doms'] << {'dom'=>langstat[0], 'count'=>langstat[1]}
    }
    #pocet see
    stat['sees'] = @sedna.query('count(collection("tecu")/entry/sees/see)')[0]
    #pocet also
    stat['alsos'] = @sedna.query('count(collection("tecu")/entry/alsos/also)')[0]
    #seznam references
    stat['refs'] = []
    @sedna.query('for $x in distinct-values(collection("tecu")/entry/refs/ref) return <a>{$x}#{count(collection("tecu")/entry[refs/ref=$x])}</a>').each{|x|
      langstat = x.sub('<a>','').sub('</a>','').split('#')
      stat['refs'] << {'ref'=>langstat[0], 'count'=>langstat[1]}
    }
    return stat
  end

  def get_history
    query = '<notes>{subsequence(for $x in collection("tecu")/entry order by $x/notes/note[last()]/@time descending return <last id="{data($x/@id)}" term="{$x/terms/term[1]/text()}">{$x/notes/note[last()]}</last>,0,30)}</notes>'
    return @sedna.query(query)[0]
  end

  def get_all
    query = 'for $x in collection("tecu")/entry return $x'
    res = '<tezk>'
    @sedna.query(query).each{|xml|
      res += xml.to_s.sub('<?xml version="1.0" standalone="yes"?>','')
      res += "\n"
    }
    res += '</tezk>'
  end
end
