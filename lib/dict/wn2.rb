require 'dict/dict-sedna'

class WordNet2 < DictSedna
  attr_accessor :rel_types
  attr_accessor :prefix
  attr_accessor :dict_prefix
  attr_accessor :user_queries
  attr_accessor :lock_timeout
  def initialize(db_path, database, key_path='', env = nil, admindict = nil)
    super
    @servlet = JSWordnetServlet
    @max_result = 100
    @rel_types = []
    if self.instance_of?(WordNet2)
      @rel_types = update_types
    #  if database[0,3] == 'gwg'
    #    prefixes = count_prefix
    #    version = prefixes.sort[0][0].gsub(/[^0-9]/, '').to_s
    #    @prefix = 'PWN ' + version[0,1].to_s + '.' + version[1..-1].to_s
    #  end
    end
    @user_queries = []
    @dict_prefix = @admindict.dict_prefix
    admindict.lock_timeout = 15*60
  end

  #parse one condition
  def parse_one_condition(query, wn_array, attributes = {}, casesens = false)
      top = @key_path.split('/')[1]
      $stderr.puts "QUERY"
      $stderr.puts top
      $stderr.puts query
   
      if query[0..(top.length-1)] == top and top != 'cdb_synset' and top != 'cdb_lu'
        what = query[(top.length+1)..(query.index("=")-1)]
      else
        what =  query[0..(query.index("=")-1)]
      end
      search = query[(query.rindex("=")+1)..-1]
      what.gsub!('.','/')
      $stderr.puts what
      if search.index(':') != nil
        #lookup by data from other dictionary
        other_dict, other_id, other_element  = search.split(':')
        other_element.gsub!('.', '/')
        $stderr.puts other_dict + other_id + other_element
        other_xml = wn_array[other_dict].get(other_id)
        $stderr.puts other_xml.to_s
        odoc = REXML::Document.new(other_xml.to_s)
        vals = []
        REXML::XPath.each( odoc, '/'+other_element ) { |el|
      	  $stderr.puts el
          if el.is_a?(REXML::Element)
            vals << "$p='#{el.text.to_s}'"
          else 
            vals << "$p='#{el.to_s}'"
          end
        }
        search_query = vals.join(' or ')
      elsif search.index('.*') != nil
        if search.scan('.*').length == 2
          search_query = "contains($p, '" + search[2..-3] + "')"
        elsif search.index('.*') == 0
          search_query = "ends-with($p, '" + search[2..-1] + "')"
        else
          search_query = "starts-with($p, '" + search[0..(search.index('.*')-1)] + "')"
        end
        if casesens
          search_query = "dbxml:contains($p, '" + search.gsub('.*', '') + "')"
        end
      else
        search_query = "$p='#{search}'"
        if casesens
          search_query = "match($p, '#{search}')"
        end
      end
      unless attributes.nil? or attributes[top+'/'+what].nil?
        what = "#{what}[#{attributes[top+'/'+what]['name']}='#{attributes[top+'/'+what]['val']}']"
      end
      if search == 'NULL'
        search_string = "not($x/#{what})"
      else
        search_string = "some $p in $x/#{what} satisfies (#{search_query})"
      end
    return search_string
  end

  #parse conditions
  def parse_conditions(query, wn_array, attributes = nil, casesens = false)
    query_ar = query.split(' ')
    query_ar.map!{|qa|
      if ['or', 'OR', 'and', 'AND'].include?(qa)
        qa.downcase
      else
        '(' + parse_one_condition(qa, wn_array, attributes, casesens) + ')'
      end
    }
    search_string = query_ar.join(' ')
    return search_string
  end

  def parse_attributes(attributes)
    attr_hash = {}
    attr_ar = attributes.split(' ')
    attr_ar.each{|at|
      next if ['or', 'OR', 'and', 'AND'].include?(at)
      el = at[0..at.index('@')-2].gsub('.','/')
      name = at[at.index('@')..at.index('=')-1]
      val = at[at.index('=')+1..-1].gsub("'", "&apos;")
      attr_hash[el] = {'name'=>name, 'val'=>val}
    }
    return attr_hash
  end

  #return list of synset IDs
  def get_list(query, wn_array = nil, show_def = false, max_result = @max_result, showmany = false, attributes = nil, casesens = false)
    what = ''
    r = {}
    synsets = {}
    db_query = ''
    query ||= ''

    #check if query is ID
    xml = get(query.to_s)
    if xml.to_s != ''
      return parse_synset_for_list(xml, synsets, nil, nil, show_def)
    end

    top = @key_path.split('/')[1]
    rest = @key_path.split('/')[2..-1].join('/')

    attr_hash = nil
    attr_hash = parse_attributes(attributes) unless attributes.nil?

    begin
      if query == '.*' or query == '/.*/'
        @container.each{|doc|
          synsets = parse_synset_for_list(doc, synsets, nil, nil)
        }
      else
        if query[0,1] == '/'
          re = query.gsub(/\//, '')
          db_query = "for $x in collection('#{@collection}')/#{top} where some $p in $x/SYNONYM/LITERAL satisfies (matches($p,'#{re}')) return $x"
        elsif query[0,top.length] == top or query.include?"="
          search_string = parse_conditions(query, wn_array, attr_hash, casesens)
          db_query = "for $x in collection('#{@collection}')/#{top} where #{search_string} return $x"
        else
          lit = query.split(':')[0]
          sen = query.split(':')[1]
          pos = query.index(':')

          if (pos == nil)
            db_query = "collection('#{@collection}')/SYNSET[SYNONYM/WORD/.='#{lit}' or SYNONYM/LITERAL/.='#{lit}']"
          else
            db_query = "collection('#{@collection}')/SYNSET[SYNONYM/LITERAL/.='#{lit}']"
          end
          if casesens
            db_query = "collection('#{@collection}')/SYNSET[match(SYNONYM/LITERAL, '#{lit}')]"
          end
        end
        $stderr.puts db_query
        $stderr.puts "start " + Time.now.to_s
        count = 0

        query(db_query).each do |s|
          #allow displaying all results for BCS queries
          if count == max_result and not(query[(top.length+1),3] == 'BCS' or query[(top.length+1), 2] == 'ID')
            synsets['...'] = {'pos'=>'?', 'def'=>'...', 'literals'=>[{'literal'=>'...','sense'=>'0'}]} if showmany
            break
          end
          count = count + 1
          synsets = parse_synset_for_list(s, synsets, lit, sen, show_def)
        end
      end

    rescue => e
      $stderr.puts e.to_s + "\n" + e.backtrace.join("\n")
      return e
    end
    
    $stderr.puts "end " + Time.now.to_s
    $stderr.puts synsets.to_s
    synsets 
  end

  def parse_synset_for_list(dbxmldoc, synsets, literal=nil, sense=nil, show_def=false)
    doc = load_xml_string(dbxmldoc.to_s)
    if doc.find('//RIGIDITY').size == 0
      re = XML::Node.new('RIGIDITY')
      doc.root << re
      re['rigid'] = 'unknown'
      re['rigidScore'] = ''
      re['nonRigidScore'] = ''
    end
    show = false
    literals = []
    doc.find('/SYNSET/SYNONYM/LITERAL').each{|el|
      if (sense == nil || (sense.to_s == el['sense'].to_s && literal.to_s == el.content.to_s))
        show = true
      end
      ls = {'literal'=>el.content.to_s.strip, 'sense'=>el['sense'].to_s}
      literals << ls
    }
    if (show)
      id = doc.find('/SYNSET/ID').to_a.first.content.to_s
      synsets[id] = {}
      synsets[id]['pos'] = doc.find('/SYNSET/POS').to_a.first.content.to_s unless doc.find('/SYNSET/POS').to_a.first.nil?
      synsets[id]['def'] = doc.find('/SYNSET/DEF').to_a.first.content.to_s if not doc.find('/SYNSET/DEF').to_a.first.nil? and show_def
      if show_def
        rig = doc.find('/SYNSET/RIGIDITY').to_a.first
        synsets[id]['RIGIDITY'] = {'@rigid'=>rig['rigid'], '@rigidScore'=>rig['rigidScore'],'@nonRigidScore'=>rig['nonRigidScore']}
      end
      synsets[id]['g_syn'] = 'false'
      synsets[id]['g_syn'] = doc.find('/SYNSET/G_SYN').to_a.first.content.to_s.downcase unless doc.find('/SYNSET/G_SYN').to_a.first.nil?
      synsets[id]['literals'] = literals
      lockuser = @admindict.who_locked(@dictcode, id)
      synsets[id]['lockstatus'] = lockuser
    end

    return synsets
  end

  #return xml for one entry
  def get_heslo(id)
    return get(id)
  end

  #expands ilr links to full literals
  def follow_ilr(xmldoc)
    res = ''
    doc = load_xml_string(xmldoc)
    doc.find('/SYNSET/ILR').each{|el|
      ilr = el.content.to_s
      data = get_literals_object(ilr)
      el.content = data['text']
      el['def'] = data['def'].to_s
      el['link'] = ilr
      data['literal'].each{|lit|
        el << plit = XML::Node.new('lit')
        plit['sense'] = lit['sense']
        plit['literal'] = lit['text']
      }
    }
    doc.find('/SYNSET/RILR').each{|el|
      ilr = el.content.to_s
      data = get_literals_object(ilr)
      el.content = data['text']
      el['def'] = data['def'].to_s
      el['link'] = ilr
      data['literal'].each{|lit|
        el << plit = XML::Node.new('lit')
        plit['sense'] = lit['sense']
        plit['literal'] = lit['text']
      }
    }
    return doc.to_s
  end

  #count revers ilr
  def get_rilr(xmldoc)
    doc = load_xml_string(xmldoc)
    doc.find('/SYNSET/ID').each{|el|
      id = el.content.to_s
      $stderr.puts 'RILR'+id
      xquery_to_hash('[SYNSET[ILR="'+id+'"]]').each{|id2,xml2|
        doc2 = load_xml_string(xml2.to_s)
        ilr = doc2.find('/SYNSET/ID').to_a.first.content.to_s
        $stderr.puts ilr
        link = doc2.find("/SYNSET/ILR[.='"+id+"']").first
        el2 = XML::Node.new('RILR', ilr)
        el2['type'] = link['type'].to_s
        el2['literal'] = link['literal'].to_s
        doc.root << el2
      }
    }
    return doc.to_s
  end

  def get_tree(xmldoc, dir = 'ILR', details = 0)
    res = []
    previous = ''
    begin
      $stderr.puts dir
      $stderr.puts previous
      ar = get_up_tree(xmldoc, dir, details)
      if previous == ar[0]
        ar[0] = 'koren'
      else
        previous = ar[0]
      end
      res << ar
      $stderr.puts ar[0]
      if ar[0] != 'koren' 
        #$stderr.puts "collection('#{@collection}')/SYNSET[ID='#{ar[0]}']"
        s = get(ar[0].to_s)
        #@sedna.query("collection('#{@collection}')/SYNSET[ID='#{ar[0]}']") { |s|
          if dir == 'RILR'
            xmldoc = get_rilr(s.to_s)
          else
            xmldoc = s.to_s
          end
        #}
        if xmldoc == ''
          $stderr.puts 'baf'
        end
      end
    end while ar[0] != 'koren'
    return res
  end

  def get_up_tree(xmldoc, dir = 'ILR', details = 0)
    $stderr.puts 'uptree'
    doc = load_xml_string(xmldoc)
    if doc.find("#{dir}[@type='hypernym']").to_a.first.nil?
      upilr = 'koren'
    else
      upilr = doc.find("#{dir}[@type='hypernym']").to_a.first.content.to_s
    end
    id = doc.find('ID').to_a.first.content.to_s
    text = get_literals(id)
    color = ''
    g_syn = ''
    defin = ''
    liter = []
    if details > 0
      relxml = get(id)
      if relxml != ''
        reldoc = load_xml_string(relxml)
        g_syn = 'false'
        g_syn = reldoc.find('G_SYN').to_a.first.content.to_s.downcase unless reldoc.find('G_SYN').to_a.first.nil?
        defin = reldoc.find('DEF').to_a.first.content.to_s unless reldoc.find('DEF').to_a.first.nil?
        if details > 1
          reldoc.find('SYNONYM/LITERAL').each{|lit|
            liter << {'$'=>lit.content.to_s, '@sense'=>lit['sense'].to_s}
          }
        end
      end
    end
    res = [upilr, id, text, color, 'hypernym', g_syn, defin, liter]
    return res  
  end

  def get_subtree(xmldoc, dir = 'ILR', limit_type = '', details = 0)
    res = []
    doc = REXML::Document.new(xmldoc)
    return res if doc.elements['SYNSET/ID'].nil?
    origId = doc.elements['SYNSET/ID'].text
    derivtypes = ['eng_derivative', 'deriv-g', 'deriv-na', 'deriv-ger', 'deriv-dvrb', 'deriv-pas', 'deriv-pos', 'deriv-aad', 'deriv-an', 'deriv-ag', 'deriv-dem']
    query = "/SYNSET/#{dir}"
    query = "/SYNSET/#{dir}[@type='#{limit_type}']" if limit_type != ''
    REXML::XPath.each(doc, query) {|el|
      id = el.text
      type = el.attributes['type']
      text = get_literals(id)
      text = '[' + type + '] ' + text if type != 'hypernym'
      color = ''
      color = 'BLUE' if type != 'hypernym'
      g_syn = ''
      defin = ''
      liter = []
      if details > 0
        relxml = get_heslo(id)
        if relxml != ''
          reldoc = load_xml_string(relxml)
          g_syn = 'false'
          g_syn = reldoc.find('G_SYN').to_a.first.content.to_s.downcase unless reldoc.find('G_SYN').to_a.first.nil?
          defin = reldoc.find('DEF').to_a.first.content.to_s unless reldoc.find('DEF').to_a.first.nil?
          if details > 1
            reldoc.find('SYNONYM/LITERAL').each{|lit|
              liter << {'$'=>lit.content.to_s, '@sense'=>lit['sense'].to_s}
            }
            rig = reldoc.find('//RIGIDITY').to_a.first
            rigid = {'@rigid'=>rig['rigid'], '@rigidScore'=>rig['rigidScore'],'@nonRigidScore'=>rig['nonRigidScore']}
          end
        end
      end
      res << [origId, id, text, color, type, g_syn, defin, liter, rigid]
    }
    return res
  end
  
  def get_full_subtree(xmldoc, dir= 'ILR', limit_type = '', details = 0)
    res = get_subtree(xmldoc, dir, limit_type, details)

    #get subtree for each new id recursively
    res.each{|ar|
      xml = get_heslo(ar[1])
      if dir == 'RILR'
        xml = get_rilr(xml)
      end
      newres = get_full_subtree(xml, dir, limit_type, details)
      res = res.concat(newres)
    }
    return res
  end

  #return literals for synset
  def get_literals(id)
    words = []
    begin
      xml = get(id)
      doc = load_xml_string(xml.to_s)
      doc.find('/SYNSET/SYNONYM/LITERAL').each{|el|
        words.push el.content.to_s + ':' + el['sense'].to_s
      }
    rescue
    end
    text = words.join(', ')
    return text
  end
  def get_literals_object(id)
    words = []
    words2 = []
    result = {}
    begin
      xml = get(id)
      doc = load_xml_string(xml.to_s)
      doc.find('/SYNSET/SYNONYM/LITERAL').each{|el|
        words << {'text'=>el.content.to_s,'sense'=> el['sense'].to_s}
        words2 << el.content.to_s + ':' + el['sense'].to_s
      }
      result['def'] = doc.find('/SYNSET/DEF').first.content unless doc.find('/SYNSET/DEF').first.nil?
    rescue
    end
    result['text'] = words2.join(', ')
    result['literal'] = words
    return result
  end

  #delete also ILR pointing to synset
  def delete_synset(id)
    delete(id)
    xquery_to_hash("/SYNSET[ILR='#{id}']").each{|id2,xml|
      doc = REXML::Document.new(xml)
      doc.elements.delete_all("/SYNSET/ILR[.='#{id}']")
      update(id2, doc.to_s)
    }
    return true
  end

  def get_notes
    return xpath2hash("collection('#{@collection}')/SYNSET/SNOTE/text()")
  end

  def update_types
    types = ['also_see', 'be_in_state', 'category_domain', 'causes', 'holo_member', 'holo_part', 'holo_portion', 'hypernym', 'near_antonym', 'similar_to', 'subevent', 'verb_group', 'deriv-ag', 'deriv-g', 'deriv-dem', 'deriv-na', 'deriv-ger', 'deriv-dvrb', 'deriv-pas', 'deriv-pos', 'deriv-aad', 'deriv-an']
    @sedna.query("for $type in distinct-values(collection('#{@collection}')/SYNSET/ILR/@type) return $type").each{|s|
      types << s
    }
    types.uniq!
    types.sort!
    return types
  end

  def count_prefix
    prefcount = {}
    @container.each{|xml|
      id = xml.to_document.get_metadata('http://www.sleepycat.com/2002/dbxml', 'name')
      pref = id[0..(id.index('-').to_i-1)]
      prefcount[pref] = 0 if prefcount[pref] == nil
      prefcount[pref] += 1 
    }
    return prefcount
  end

  def check_lock(dictionary, entry, user)
    who_lock = @admindict.who_locked(dictionary, entry)
    if (who_lock == false or who_lock == user)
      return true
    else
      return {'action'=>'error','message'=>'locked by '+who_lock, 'user'=>who_lock}
    end
  end

  def lock(dictionary, entry, user, time=nil)
    @admindict.lock_entry(dictionary, entry, user, time)
  end
  def unlock(dictionary, entry, user)
    @admindict.unlock_entry(dictionary, entry, user)
  end
  def unlock_time(dictionary, entry, user)
    @admindict.unlock_time(dictionary, entry, user)
  end

  def first_run(dictionary, dictarray)
    return true
  end

  def update_links_id(origId, newId)
    query = "/SYNSET[ILR/.='#{origId}']"
    $stderr.puts "UPDATE SYNSET links query "+query
    list = xquery_to_hash(query)
    list.each{|key,val|
      $stderr.puts "  update #{key}"
      doc = REXML::Document.new(val.to_s)
      doc.elements.to_a("/SYNSET/ILR[.='#{origId}']").each{|el|
        el.text = newId
      }
      update(key, doc.to_s)
    }
  end

  def add_meta_info(xml, metainfo, pathinfo=nil)
    doc = load_xml_string(xml)
    if doc != nil
      doc.root << meta = XML::Node.new('meta')
      metainfo.each{|hash|
        meta << dic = XML::Node.new('dict')
        dic['code'] = hash['code']
        dic['name'] = hash['name']
      }
      if pathinfo != nil
        meta << path = XML::Node.new('path')
        pathinfo.each{|a|
          path << pp = XML::Node.new('part')
          pp['up'] = a[0]
          pp['id'] = a[1]
          pp['text'] = a[2]
          pp['def'] = a[6] unless a[6].nil?
          unless a[7].nil?
            a[7].each{|lit|
              pp << plit = XML::Node.new('lit')
              plit['sense'] = lit['@sense']
              plit['literal'] = lit['$']
            }
          end
        }
      end
    end
    return doc.to_s
  end

  def topmost_entries
    count = 0
    topmost = {}
    top = @key_path.split('/')[1]
    xquery_to_hash("/#{top}[not(ILR) and POS='n']").each {|id,xml|
      topmost[id] = ''
      count += 1
      break if count > 20
    }
    return topmost
  end

  def get_next_sense(literal)
    sense = 0
    @sedna.query("collection('#{@collection}')/SYNSET/SYNONYM/LITERAL[.='#{literal}']/@sense").each{|s|
      s_nr = s.to_s.gsub(/[^0-9]/,'').to_i
      sense = s_nr if s_nr > sense
    }
    return (sense+1)
  end

  def test_gsyn(id)
    xml = get_heslo(id).to_s
    return false if xml == ''
    doc = load_xml_string(xml)
    return false if doc.nil?
    if doc.find('/SYNSET/G_SYN').to_a.first.nil?
      return false
    else
      if doc.find('/SYNSET/G_SYN').to_a.first.content.to_s.downcase == 'true'
        return true
      else
        return false
      end
    end
  end

  def expand_rels_editor(xml)
    doc = load_xml_string(xml)
    doc.find('ILR').each{|ilr|
      rid = ilr.content.to_s
      relxml = get_heslo(rid)
      reldoc = load_xml_string(relxml)
      $stderr.puts '####'+rid
      g_syn = 'false'
      g_syn = 'true' if not reldoc.find('G_SYN').to_a.first.nil? and reldoc.find('G_SYN').to_a.first.content.to_s.downcase == 'true'
      definition = ''
      definition = reldoc.find('DEF').to_a.first.content.to_s unless reldoc.find('DEF').to_a.first.nil? 
      ilr['rel_g_syn'] = g_syn
      ilr['rel_definition'] = definition
      reldoc.find('SYNONYM/LITERAL').each{|syn|
        rellit = XML::Node.new('rel_literal')
        ilr << rellit
        rellit.content = syn.content.to_s
        rellit['sense'] = syn['sense']
      }
    }
    xml = doc.to_s.gsub('<?xml version="1.0" encoding="utf-8"?>','')
    $stderr.puts xml
    return xml
  end

  def dump(root=nil, code=nil)
    ret = (root==nil)? '<'+@collection.to_s+'>' : '<'+root.to_s+'>'
    unless code.nil?
      begin
        ret += @admindict.get('lmfinfo'+code).to_s
      rescue
      end
    end
    @sedna.query("for $x in collection('#{@collection}') return $x").each{|doc|
      ret += doc.to_s
    }
    ret += (root==nil)? '</'+@collection.to_s+'>' : '</'+root.to_s+'>'
  end

  def find_relr(target_id)
    xmls = []
    types = ['eq_synonym', 'eq_near_synonym', 'eq_has_hyperonym', 'eq_has_hyponym']
    query = "/SYNSET[ELR/.='#{target_id}']"
    list = xquery_to_hash(query)
    list.each{|key,val|
      doc = load_xml_string(val.to_s)
      elr = doc.find("/SYNSET/ELR[.='#{target_id}']").to_a.first
      if types.include?(elr['type'].downcase)
        xmls << val.to_s
      end
    }
    return xmls
  end

  def get_kyotoowl(id)
    $stderr.puts 'KYOTO '+id
    gotowl = false
    kyotoowl = ''
    jumps = -1
    begin
      $stderr.puts jumps.to_s+'--'+id+'--'+gotowl.to_s+'--'
      jumps += 1
      xml = get_heslo(id).to_s
      doc = load_xml_string(xml)
      if doc.find('/SYNSET/kyotoowl').to_a.first.nil?
        if doc.find('/SYNSET/ILR[@type="hypernym"]').to_a.first.nil?
          return nil,nil,nil
        else
          id = doc.find('/SYNSET/ILR[@type="hypernym"]').to_a.first.content.to_s
        end
      else
        if doc.find('/SYNSET/kyotoowl').to_a.first.content.to_s != ''
          gotowl = true
          kyotoowl = doc.find('/SYNSET/kyotoowl').to_a.first.content.to_s
        end
      end
    end while not gotowl
    return id, kyotoowl, jumps
  end
  
  def get_modified(version)
    query = "collection('#{@collection}')/SYNSET[VERSION>='#{version}']/ID"
    ids = []
    $stderr.puts query
    @sedna.query(query).each{|s|
      id = s.to_s.gsub('<ID>','').gsub('</ID>','')
      ids << id
    }
    return ids
  end

  def translate_synsets(wn_array, query, target)
    $stderr.puts 'translate'
    $stderr.puts 'get ELR'
    synsets = get_list(query, wn_array, false)
    elrs = []
    synsets.each{|id, info|
      $stderr.puts id
      synset = get(id)
      doc = load_xml_string(synset)
      doc.find('//ELR').each{|elr|
        elrs << elr.content.to_s if elr['system'].to_s == ''
      }
    }
    elrs.uniq!
    $stderr.puts 'get target'
    tsynsets = {}
    elrs.each{|elr|
      #$stderr.puts elr
      tsyn = wn_array[target].get_list('ELR='+elr, wn_array, false)
      tsyn.each{|tid, ts|
        unless tsynsets.has_key?(tid)
          tsynsets[tid] = ts
        end
      }
      $stderr.puts tsyn
    }
    $stderr.puts elrs
    return {'original'=>synsets,'translated'=>tsynsets}
  end

  def load_info_relations(type='__all', order='desc', maxres=10)
    info_path = @db_path + '/' + @collection + '.info'
    sort = '-r'
    sort = '' if order == 'asc'
    $stderr.puts info_path
    if File.exists?(info_path)
      rel_info = []
      $stderr.puts "grep ';#{type};' #{info_path}|sort -t';' -n -k3 #{sort}|head -#{maxres}"
      output = IO.popen("grep ';#{type};' #{info_path}|sort -t';' -n -k3 #{sort}|head -#{maxres}")
      output.readlines.each{|line|
        line.strip!
        ar = line.split(';')
        rel_info << {'synset_id'=>ar[0], 'synset_pos'=>ar[1], 'relation_type'=>ar[2], 'relation_count'=>ar[3].to_i}
      }
      return rel_info
    else
      return ['error']
    end
  end
  
  def load_info_modifications(user='', synset='', maxres=10)
    info_path = @db_path + '/' + @collection + '.mods'
    search = ''
    if user != '' and synset != ''
      search = '$4 == "'+user+'" && $2 == "'+synset+'"'
    elsif user != ''
      search = '$4 == "'+user+'"'
    elsif synset != ''
      search = '$2 == "'+synset+'"'
    end
    $stderr.puts info_path
    if File.exists?(info_path)
      mod_info = []
      $stderr.puts "awk -F\\; '#{search} {print $0}' #{info_path}|sort -t';' -n -k3 -r | head -#{maxres}"
      output = IO.popen("awk -F\\; '#{search} {print $0}' #{info_path}|sort -t';' -n -k3 -r |head -#{maxres}")
      output.readlines.each{|line|
        line.strip!
        ar = line.split(';')
        mod_info << {'action'=>ar[0], 'synset'=>ar[1], 'time'=>Time.at(ar[2].to_i).strftime('%Y-%m-%d %H:%M'), 'user'=>ar[3], 'version'=>ar[4].to_i}
      }
      return mod_info
    else
      return ['error']
    end
  end

  def load_info_edit(month, user='', synset='')
    info_path = @db_path + '/' + @collection + '.stat'
    search = '$3 == "'+month+'"'
    search += ' && $1 == "'+user+'"' if user != ''
    search += ' && $2 == "'+synset+'"' if synset != ''
    $stderr.puts info_path
    if File.exists?(info_path)
      edit_info = []
      $stderr.puts "awk -F\\; '#{search} {print $0}' #{info_path}"
      output = IO.popen("awk -F\\; '#{search} {print $0}' #{info_path}")
      output.readlines.each{|line|
        line.strip!
        ar = line.split(';')
        edit_info << {'user'=>ar[0], 'synset'=>ar[1], 'month'=>ar[2], 'action_count'=>ar[3]}
      }
      return edit_info
    else
      return ['error']
    end
  end
  
  def load_info_journal(user='', synset='', datefrom='', dateto='', maxres=10)
    journal_path = @db_path + '/' + @collection + '.journal'
    mod_path = @db_path + '/' + @collection + '.mods'
    $stderr.puts "grep '<update\\|delete' #{journal_path} |sed -e \"s#<\\(update\\|delete\\) key='\\([^']*\\)' timestamp='\\([^']*\\)' thread='[^']*' user='\\([^']*\\)'\\([^>]*\\).*#\\1;\\2;\\3;\\4;\\5#\"|sed -e \"s/ version='\\([0-9]*\\)'/\\1/\" > #{mod_path}"
    system("grep '<update\\|delete' #{journal_path} |sed -e \"s#<\\(update\\|delete\\) key='\\([^']*\\)' timestamp='\\([^']*\\)' thread='[^']*' user='\\([^']*\\)'\\([^>]*\\).*#\\1;\\2;\\3;\\4;\\5#\"|sed -e \"s/ version='\\([0-9]*\\)'/\\1/\" > #{mod_path}")
    
    search = []
    search << '$4 == "'+user+'"' if user != ''
    search << '$2 == "'+synset+'"' if synset != ''
    if datefrom != ''
      tsfrom = Time.parse(datefrom).to_i
      search << '$3 >= '+tsfrom.to_s
    end
    if dateto != ''
      tsto = Time.parse(datefrom+' 23:59:59').to_i
      search << '$3 <= '+tsto.to_s
    end
    search_c = ''
    search_c = search.join(' && ')
    if File.exists?(mod_path)
      mod_info = []
      $stderr.puts "awk -F\\; '#{search_c} {print $0}' #{mod_path}|sort -t';' -n -k3 -r | head -#{maxres}"
      output = IO.popen("awk -F\\; '#{search_c} {print $0}' #{mod_path}|sort -t';' -n -k3 -r |head -#{maxres}")
      output.readlines.each{|line|
        line.strip!
        ar = line.split(';')
        mod_info << {'action'=>ar[0], 'synset'=>ar[1], 'time'=>Time.at(ar[2].to_i).strftime('%Y-%m-%d %H:%M'), 'user'=>ar[3], 'version'=>ar[4].to_i}
      }
      return mod_info
    else
      return ['error']
    end
  end
  #DEBDict
  def list_starts_with( key, dict, limit=0, searchall=false )
    synsets = get_list(key, @wn_array, false)
    if synsets.is_a?(Hash)
      syn_ar = []
      count = 0
      synsets.each {|id,info|
        count += 1
        break if limit != 0 and count > limit
        prio = 1000
        label = ''
        literals = []
        info['literals'].each {|h|
          prio = h['sense'].to_i if h['literal'] == key
          literals << h['literal'] + ':' + h['sense']
        }
        label += '[' + info['pos'] + '] ' + literals.join(', ')
        syn_ar << {'id'=>id, 'label'=>label, 'prio'=>prio}
      }
      syns = []
      syn_ar.sort{|x,y| [x['prio'],x['label']]<=>[y['prio'],y['label']]}.each{|h|
        syns << h['id']+'|'+h['label']
      }
      return syns.join("\n")
    else
      return ''
    end

  end

  def get_freq(name,id)
    return ''
  end

  def get_all(format='')
    query = 'for $x in collection("'+@collection+'")/SYNSET order by $x/SYNONYM/LITERAL[1] return $x'
    $stderr.puts query
    case format
    when 'lmf','lemon'
      res = '<WN>'
      owner = ''
      label = @collection
      owner, label = @collection.split('_wn_') if @collection.include?('_wn_')
      res += '<INFO label="'+label+'" owner="'+owner+'"/>'
    else
      res = '<'+@collection+">\n"
    end
    @sedna.query(query).each{|xml|
      res += xml.to_s.sub('<?xml version="1.0" standalone="yes"?>','')
      res += "\n"
    }
    case format
    when 'lmf','lemon'
      res += '</WN>'
    else
      res += '</'+@collection+">\n"
    end
    return res
  end
end
