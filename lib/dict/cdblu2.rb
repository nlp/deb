require 'dict/dict-sedna'
require 'dict/wn2'

class CdbLU < WordNet2
  def initialize( db_path, database, key_path='', env = nil, admindict = nil )
    super
    @servlet = CdbLUServlet
    @id_regexp = /[cdr]_([avn]-)?[0-9]+/ #REGEXP FOR LU ID!
  end

  def get_literal(id)
    begin
      xml = get(id)
      doc = REXML::Document.new( xml.to_s )
      id = REXML::XPath.first( doc, '/cdb_lu/@c_lu_id' ).to_s
      seq_nr = doc.root.attributes['c_seq_nr'].to_s
      form = doc.elements['cdb_lu/form'].attributes['form-spelling'].to_s
      pos = doc.elements['cdb_lu/form'].attributes['form-cat'].to_s
    rescue
      form = 'not_found'
      seq_nr = '0'
      pos = ''
    end
    return {'form'=>form, 'seq_nr'=>seq_nr, 'pos'=>pos}
  end

  def get_list(query, array=nil)
    $stderr.puts "cdb_lu:get_list start "+time_stamp()
    synsets = {}
    db_query = ''
    query ||= ''
    query.gsub!(/&(?!(\w+|#\d+);)/, '&amp;')
    query.gsub!(/'/, '&quot;')
    
    top = @key_path.split('/')[1]
    rest = @key_path.split('/')[2..-1].join('/')
    
    if query[0,top.length] == top or query.include?"="
      search_string = parse_conditions(query, array)
      #db_query = "for $x in collection('#{@collection}')/#{top} where #{search_string} return $x"
      db_query = "for $x in collection('#{@collection}') where #{search_string} return document-uri($x)"
    else
      ar = query.split(':')
      query = ar[0]
      sense = (ar[1] == nil)? '' : 'and $x/cdb_lu/@c_seq_nr="' + ar[1].to_s + '"'
      sensexp = (ar[1] == nil)? '' : 'and @c_seq_nr="' + ar[1].to_s + '"'
      
      if query =~ @id_regexp
        #db_query = "collection('#{@collection}')/#{top}[#{rest}='#{query}']"
        db_query = "for $x in collection('#{@collection}') where $x/#{top}[#{rest}='#{query}'] return document-uri($x)"
      elsif (pos = query.index('*')) != nil
        search = query.gsub('*','')
        if pos == 0 and query.count('*') > 1
          #db_query = "for $x in collection('#{@collection}')/#{top} where some $p in $x/form/@form-spelling|$x/form/@form-spelvar satisfies (contains($p, '#{search}')) #{sense} return $x"
          db_query = "for $x in collection('#{@collection}') where some $p in $x/cdb_lu/form/@form-spelling|$x/cdb_lu/form/@form-spelvar satisfies (contains($p, '#{search}')) #{sense} return document-uri($x)"
        elsif pos == 0
          #db_query = "for $x in collection('#{@collection}')/#{top} where some $p in $x/form/@form-spelling|$x/form/@form-spelvar satisfies (ends-with($p, '#{search}')) #{sense} return $x"
          db_query = "for $x in collection('#{@collection}') where some $p in $x/cdb_lu/form/@form-spelling|$x/cdb_lu/form/@form-spelvar satisfies (ends-with($p, '#{search}')) #{sense} return document-uri($x)"
        else
          #db_query = "for $x in collection('#{@collection}')/#{top} where some $p in $x/form/@form-spelling|$x/form/@form-spelvar satisfies (starts-with($p, '#{search}')) #{sense} return $x"
          db_query = "for $x in collection('#{@collection}') where some $p in $x/cdb_lu/form/@form-spelling|$x/cdb_lu/form/@form-spelvar satisfies (starts-with($p, '#{search}')) #{sense} return document-uri($x)"
        end
      else
        #db_query = "for $x in collection('#{@collection}')/#{top} where some $p in $x/form/@form-spelling|$x/form/@form-spelvar satisfies ($p='#{query}') #{sense} return $x"
        db_query = "for $x in collection('#{@collection}') where some $p in $x/cdb_lu/form/@form-spelling|$x/form/@form-spelvar satisfies ($p='#{query}') #{sense} return document-uri($x)"
      end
    end
    $stderr.puts db_query
    $stderr.puts "QUERY START " + time_stamp()
    count = 0
    query(db_query).each{|id|
      count += 1
      s = get(id)
      $stderr.puts '  '+count.to_s + ' - '+ time_stamp() + ' entry '+id
      res = strip_redundant(s.to_s, true)
      doc = REXML::Document.new(res.to_s)
      synsets[id] = {}
      synsets[id]['seq_nr'] = REXML::XPath.first( doc, '/cdb_lu/@c_seq_nr' ).to_s
      synsets[id]['form'] = doc.elements['cdb_lu/form'].attributes['form-spelling'].to_s
      synsets[id]['pos'] = doc.elements['cdb_lu/form'].attributes['form-cat'].to_s
      synsets[id]['domain'] = doc.elements['cdb_lu/pragmatics/prag-domain'].text.to_s if doc.elements['cdb_lu/pragmatics/prag-domain']
      synsets[id]['resume'] = doc.elements['//sem-resume'].text.to_s if doc.elements['//sem-resume']
      lockuser = @admindict.who_locked(@dict_prefix+'cdb_lu', id)
      synsets[id]['lockstatus'] = lockuser
      break if count > @max_result
    }
    
    $stderr.puts "QUERY END " + time_stamp()
    synsets 
  end

  def get_form_seq(lu_id)
    form = ''
    seq_nr = ''
    begin
      xml = get(lu_id)
      doc = REXML::Document.new(xml.to_s)
      form = doc.elements['cdb_lu/form'].attributes['form-spelling'].to_s
      pos = doc.elements['cdb_lu/form'].attributes['form-cat'].upcase.to_s
      seq_nr = doc.elements['cdb_lu'].attributes['c_seq_nr'].to_s
    rescue => e 
      $stderr.puts "exception #{e.class.to_s}: #{e.message}"
      $stderr.puts e.backtrace.join("\n")
    end
    return form, seq_nr, pos
  end

  #find redundant information
  def follow_redundant(xml, reddb)
    res = ''
    doc = REXML::Document.new(xml)
    c_lu_id = doc.root.attributes['c_lu_id'].to_s
    if c_lu_id != '' and not reddb.nil?
      query = "/rbn_entry[@c_lu_id='#{c_lu_id}']"
      $stderr.puts "REDUNDANT query "+query
      redlist = reddb.xquery_to_hash(query)
      redlist.each {|key, val|
        reddoc = REXML::Document.new(val.to_s)
        doc.root.elements['sem-definition'] = reddoc.root.elements['sem-definition'] if reddoc.root.elements['sem-definition'] != nil
        doc.root.elements['sem-hypernyms'] = reddoc.root.elements['sem-hypernyms'] if reddoc.root.elements['sem-hypernyms'] != nil
        doc.root.elements['sem-synonyms'] = reddoc.root.elements['sem-synonyms'] if reddoc.root.elements['sem-synonyms'] != nil
        doc.write(res, 1)
        return res
      }
    end
    doc.to_s
  end
  def strip_redundant(xml, check_access=false)
    if check_access and @admindict.check_perm(Thread.current[:auth_user], @dict_prefix+'cdb_red2')
      return xml
    end
    return xml if xml.nil? or xml.to_s == ''
    doc = REXML::Document.new(xml.to_s)
    doc.root.elements.delete('sem-definition')
    doc.root.elements.delete('sem-hypernyms')
    doc.root.elements.delete('sem-synonyms')
    $stderr.puts doc.to_s
    return doc.to_s
  end

  #update CID records after LU change
  def update_cids(ciddb, lu_id, form, seqnr, pos)
    query = "[cid[@c_lu_id='#{lu_id}']]"
    $stderr.puts "UPDATE CIDs query "+query
    list = ciddb.xquery_to_hash(query)
    list.each{|key,val|
      $stderr.puts "  update #{key}"
      doc = REXML::Document.new(val.to_s)
      doc.root.attributes['pos'] = pos
      doc.root.attributes['seq_nr'] = seqnr
      doc.root.attributes['form'] = form
      ciddb.update(key, doc.to_s)
    }
  end

  #update synset preview
  def update_synset_preview(syndb, luid, luform, luseq)
    query = "[cdb_synset[synonyms/synonym/@c_lu_id='#{luid}']]"
    $stderr.puts "UPDATE SYNSETs query "+query
    list = syndb.xquery_to_hash(query)
    list.each{|key,val|
      $stderr.puts "  update #{key}"
      doc = REXML::Document.new(val.to_s)
      syid = doc.root.attributes['c_sy_id'].to_s
      #synset previewtext
      prev_ar = []
      doc.elements.to_a('/cdb_synset/synonyms/synonym').each{|el|
        el.attributes['c_lu_id-previewtext'] = luform+':'+luseq if el.attributes['c_lu_id'] == luid and luform != ''
        prev_ar << el.attributes['c_lu_id-previewtext']
      }
      preview = prev_ar.join(', ')
      $stderr.puts preview

      #update link previews
      #@manager.query("for $x in collection('#{@collection}')/cdb_synset where some $p in $x/wn_internal_relations/relation satisfies ($p/@target='#{syid}') return $x") { |s|
      syquery = "[cdb_synset[wn_internal_relations/relation/@target='#{syid}']]"
      sylist = syndb.xquery_to_hash(syquery)
      sylist.each{|sykey,syval|
        doc2 = REXML::Document.new( syval.to_s )
        doc2.elements.to_a("//wn_internal_relations/relation[@target='#{syid}']").each{|el|
          el.attributes['target-previewtext'] = preview
        }
        syndb.update(sykey, doc2.to_s, false)
        $stderr.puts "SAVE PREVIEW for links in #{sykey}"
      }
      syndb.update(syid, doc.to_s, false)
    }
  end
  
  def first_run(dictionary, dictarray)
    start = time_stamp()
    $stderr.puts "FIRST RUN START " + start
    @sedna.query("for $x in collection('#{@collection}') return document-uri($x)").each{|key|
      res = get(key)
      res = follow_redundant(res, dictarray[@dict_prefix+'cdb_red2'])
      $stderr.puts '*Updating LU '+key.to_s
      update(key, res.to_s, false)
    }
    $stderr.puts "FIRST RUN END" + start + '>' + time_stamp()
  end
end
