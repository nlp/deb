require 'dict/dict-sedna'
require 'dict/wn2'

class WordNetAdmin < AdminDict
  #get the list of dictionary packages
  #user: user login, if provided returs only dicts. allowed for the user
  #      with his permissions
  #old_api: use old api
  def list_packages(user='', old_api = false)
    res = {}
    userdicts = user_perms(user)
   
    list_dicts(@service_name).each {|code,dname|
      xml = get('dict'+code).to_s
      #$stderr.puts xml
      doc = load_xml_string(xml.to_s)
      key = doc.find('/dict/key').to_a.first.content.to_s.gsub!('/', '.')[1..-1]
      eqtags = {}
      doc.find('/dict/eqtags/eqtag').each {|el|
        tag = el.content.to_s
        dics = el['dics'].to_s.split(',')
        dics.map!{|x| x.strip}
        eqtags[tag] = {'tag'=>tag, 'dics'=>dics, 'name'=>el['name'].to_s, 'search_element'=>el['elem'].to_s}
      }

      indexes = []
      #doc.find('/dict/indexes/index').each{|eli|
      #  $stderr.puts eli
      #  $stderr.puts eli.doc
      #  $stderr.puts eli.class
      #  $stderr.puts eli.context
      #  element = el.find('//element')
#.to_a#.first.content.to_s
      #  $stderr.puts element
      #  type = el.find('type').to_a.first.content.to_s
      #  if type =~ /-attribute-/
      #    indexes << '@' + element
      #  else
      #    indexes << element
      #  end
      #}
      indexes.uniq!

      lookup = []
      doc.find('/dict/lookup').each{|el|
        lookup = el.content.to_s.split(',') if el.content.to_s != nil
      }
      lookup.map!{|x| x.strip}
      reload = []
      doc.find('/dict/reload').each{|el|
        reload = el.content.to_s.split(',') if el.content.to_s != nil
      }
      reload.map!{|x| x.strip}

      user_queries = []
      user_queries = @array[code].user_queries if @array[code].respond_to?('user_queries')

      doc.find('/dict/packages/package').each{|el|
        id = el.find('code').to_a.first.content.to_s
        name = el.find('name').to_a.first.content.to_s
        lookup_merge = lookup#[id]+lookup
        lookup_merge.uniq!
        if old_api
          if user == ''
            res[id] = {'id'=>id, 'dict'=>code, 'nazov'=>name, 'prava'=>'w', 'key'=>key, 'eqtags'=>eqtags, 'dicslookup'=>lookup_merge, 'dicsreload'=>reload, 'indexes'=>indexes, 'user_queries' => user_queries}
          else
            if userdicts[code] != nil
              res[id] = {'id'=>id, 'dict'=>code, 'nazov'=>name, 'prava'=>userdicts[code], 'key'=>key, 'eqtags'=>eqtags, 'dicslookup'=>lookup_merge, 'dicsreload'=>reload, 'indexes'=>indexes, 'user_queries' => user_queries}
            end
          end
        else
          if user == ''
            res[id] = {'id'=>id, 'code'=>code, 'name'=>name, 'access'=>'w', 'key'=>key, 'eqtags'=>eqtags, 'dicslookup'=>lookup_merge, 'dicsreload'=>reload, 'indexes'=>indexes, 'user_queries' => user_queries}
          else
            if userdicts[code] != nil
              res[id] = {'id'=>id, 'code'=>code, 'name'=>name, 'access'=>userdicts[code], 'key'=>key, 'eqtags'=>eqtags, 'dicslookup'=>lookup_merge, 'dicsreload'=>reload, 'indexes'=>indexes, 'user_queries' => user_queries}
            end
          end
        end
      }
    }
    $stderr.puts res.to_s
    return res
  end

  def user_perms(user)
    userdicts = {}
    if user != ''
      xml = get('user'+user)
      #$stderr.puts xml
      doc = load_xml_string(xml.to_s)
      doc.find("/user/services/service[@code='#{@service_name}']/dict").each {|el|
        #$stderr.puts '*' + el['code'] + ':' + el['perm'].to_s
        userdicts[el['code'].to_s] = el['perm'].to_s
      }
    end
    return userdicts
  end

  def check_perm(user, dict, perm=nil)
    $stderr.puts "check " + user + "-" + dict + "-" + perm.to_s
    hash = user_perms(user)
    #$stderr.puts hash
    if (perm != nil and hash[dict] == perm) or (perm == nil and hash[dict] != nil and hash[dict] != '')
      return true
    else
      return false
    end
  end
end
