require 'dict/dict-sedna'

class Edna < DictSedna
  attr_accessor :letters
  attr_accessor :count
  attr_accessor :last_update
  def initialize(collection, journal_path, key_path='', database = 'deb', admindict = nil)
    super
    @servlet = EdnaServlet
    @letters = get_letters
    @count = get_count
    @last_update = Time.new
  end

  def get_count
    res = @sedna.query('count(collection("edna")/entry[@publish="true"])')
    return res.to_s
  end

  def get_letters
    @last_update = Time.new
    letters = {}
    letters['-'] = {}
    (('a'..'z').to_a+['ch','_']).each{|l|
      if xquery_to_array('[entry[@publish="true" and ('+letter_query(l)+')]]').size == 0
        letters['-']['aktiv_'+l] = 'ne-aktiv'
      else
        letters['-']['aktiv_'+l] = 'aktiv'
      end
    }
    letters['kats'] = {}
    ['vytvar', 'odev', 'obchod', 'doprava', 'stavit', 'rukod', 'zemed', 'lid_divadlo', 'f_detsky', 'f_tanecni', 'f_hudebni', 'f_slovesny', 'magie', 'nabozenstvi', 'rod_vztah', 'obci', 'rodinne_ob', 'vyrocni_ob', 'nazvy_inst', 'zak_pojmy', 'geo_nazvy', 'pramen','ostatni_ob'].each{|k|
      letters[k] = {}
      if xquery_to_array('[entry[@publish="true" and head/klas="'+k+'"]]').size == 0
        letters['kats']['aktiv_'+k] = 'ne-publish'
      else
        letters['kats']['aktiv_'+k] = 'publish'
      end
      (('a'..'z').to_a+['ch','_']).each{|l|
        if xquery_to_array('[entry[@publish="true" and ('+letter_query(l)+') and head/klas="'+k+'"]]').size == 0
          letters[k]['aktiv_'+l] = 'ne-aktiv'
        else
          letters[k]['aktiv_'+l] = 'aktiv'
        end
      }
    }
    return letters
  end

  def letter_query(letter)
    case letter
    when 'a'
      query = 'starts-with(lower-case(head/lemma), "a") or starts-with(lower-case(head/lemma), "á")'
    when 'c'
      query = '(starts-with(lower-case(head/lemma), "c") or starts-with(lower-case(head/lemma), "č")) and not(starts-with(lower-case(head/lemma), "ch"))'
    when 'd'
      query = 'starts-with(lower-case(head/lemma), "d") or starts-with(lower-case(head/lemma), "ď")'
    when 'e'
      query = 'starts-with(lower-case(head/lemma), "e") or starts-with(lower-case(head/lemma), "é") or starts-with(lower-case(head/lemma), "ě")'
    when 'i'
      query = 'starts-with(lower-case(head/lemma), "i") or starts-with(lower-case(head/lemma), "í")'
    when 'n'
      query = 'starts-with(lower-case(head/lemma), "n") or starts-with(lower-case(head/lemma), "ň")'
    when 'o'
      query = 'starts-with(lower-case(head/lemma), "o") or starts-with(lower-case(head/lemma), "ó")'
    when 'r'
      query = 'starts-with(lower-case(head/lemma), "r") or starts-with(lower-case(head/lemma), "ř")'
    when 's'
      query = 'starts-with(lower-case(head/lemma), "s") or starts-with(lower-case(head/lemma), "š")'
    when 't'
      query = 'starts-with(lower-case(head/lemma), "t") or starts-with(lower-case(head/lemma), "ť")'
    when 'u'
      query = 'starts-with(lower-case(head/lemma), "u") or starts-with(lower-case(head/lemma), "ú") or starts-with(lower-case(head/lemma), "ů")'
    when 'y'
      query = 'starts-with(lower-case(head/lemma), "y") or starts-with(lower-case(head/lemma), "ý")'
    when 'z'
      query = 'starts-with(lower-case(head/lemma), "z") or starts-with(lower-case(head/lemma), "ž")'
    when '_'
      query = 'fn:matches(lower-case(head/lemma), "^[^a-z^(áčďéěíňóřšťúůž)]")'
    else
      query = 'starts-with(lower-case(head/lemma), "'+letter+'")'
    end
  return query
  end

  def get_list(search = '', kat = '', letter = '', search_entry='', search_trans='', search_kol='', search_der='', edit='0')
    list = []
    if edit == '1'
      if kat == '-'
        query = '[entry]'
      elsif kat != ''
        query = '[entry[(head/klas="'+kat+'")]]'
      elsif search != ''
        query = '[entry[(fn:matches(lower-case(head/lemma), "(^| )'+search.downcase+'"))]]'
      else
        return list
      end
    else
      if letter != '' and kat != ''
        query_in = '(('+letter_query(letter)+') and head/klas="'+kat+'")'
      elsif letter != ''
        query_in = '('+letter_query(letter)+')'
      elsif search != ''
        query_ar = []
        query_ar << '(fn:matches(lower-case(head/lemma), "(^| )'+search.downcase+'"))' if search_entry == 'on'
        query_ar << '(some $t in trans satisfies fn:matches(lower-case($t), "(^| )'+search.downcase+'"))' if search_trans == 'on'
        query_ar << '(some $d in deriv/deriv|deriv/full satisfies fn:matches(lower-case($d), "(^| )'+search.downcase+'"))' if search_der == 'on'
        query_ar << '(some $k in kolok/kolok|kolok/trans satisfies fn:matches(lower-case($k), "(^| )'+search.downcase+'"))' if search_kol == 'on'
        query_in = query_ar.join(' or ')
      elsif kat != ''
        query_in = '(head/klas="'+kat+'")'
      else
        return list
      end
      query = '[entry[@publish="true" and ('+query_in+')]]'
    end

    xquery_to_hash(query).each {|id,xml|
      doc = load_xml_string(xml.to_s)
      root = doc.root
      publish = 'N'
      publish = 'A' if root['publish'] == 'true'
      lemma = root.find('head/lemma').to_a.first.content.to_s
      gram = root.find('head/gram').to_a.first.content.to_s
      author = root.find('metadata/author').to_a.first.content.to_s unless root.find('metadata/author').to_a.first.nil?
      last = ''
      unless root.find('metadata/history/changes').to_a.last.nil?
        lc = root.find('metadata/history/changes').to_a.last
        last = lc['user'] + '/' + lc['time']
      end
      hash = {'id'=>id, 'lemma'=>lemma, 'author'=>author, 'last'=>last, 'gram'=>gram, 'publish'=>publish}
      list << hash
    }
    return list
  end
  
  def get_perm(user)
    xml = @admindict.get('user'+user)
    doc = REXML::Document.new(xml.to_s)
    perm_el = doc.root.elements["//service[@code='edna']/dict[@code='edna']"]
    if perm_el != nil
      perm = perm_el.attributes['perm'].to_s
      return perm
    else
      return nil
    end
  end

  def check_links(xml)
    doc = load_xml_string(xml)
    doc.root.find('//refer|//kolok/ref').each{|r|
      res =xquery_to_hash('[entry[head/lemma="'+r.content.to_s+'"]]')
      if res.size > 0
        r['link'] = URI.escape(res.keys.first)
      end
    }
    return doc.to_s
  end

  def publish(id)
    xml = get(id)
    return if xml == ''
    doc = load_xml_string(xml)
    doc.root['publish'] = 'true'
    update(id, doc.to_s)
    return true
  end
end
