#!/usr/bin/env ruby
# for Ruby to include the cpa-sql.rb properly
$LOAD_PATH.unshift('/var/lib/deb-server/lib/')
$LOAD_PATH.unshift('/var/lib/deb-server/xslt/')

require "dict/lemon.rb"
require "dbi"
require 'open-uri'
require 'json'
require 'webrick'
require 'cobravsmongoose'
require 'rubygems'
require 'net/smtp'
require 'uri'
require 'set'
require 'builder'
require 'fileutils'


if __FILE__ == $0
    #as of 09 August 2015, cpait takes 100 minutes, cpa, 138 minutes, cpasp, 15 minutes to be processed.
    service = 'cpasp' # MANDATORY = language (cpa cpait cpasp)
    cpalemon = CPALEMON.new(service) # init  Database
    destpath='/home/ymaarouf/lemon'+service+'/'# MANDATORY = Where to write linked dictionary
    FileUtils::mkdir_p destpath unless File.exists?(destpath)
    example_mode = 1 # MANDATORY = should the systtem produce examples for each CPA entry
    babelnetfile = '/home/ymaarouf/pattern_dictionary_babelnet.txt'# OPTIONAL = additional links between Babelnet and PDEV with format:
    #token  verb    pattern_number  example babelnet_synsetId       babelNet_url    score   POS
    #the system maps verb+pattern to babelNet url with sameAs link
    variantfile = '/home/ymaarouf/lemoncpa/verbmap' # OPTIONAL = list of variant forms of the same verb. e.g. organize/organise
    cpalemon.turtle_verblist_local(destpath,example_mode)
#    cpalemon.turtle_verblist_local(destpath,example_mode,variantfile,babelnetfile) # ARG MAX = 4
end
 
