require 'dict/dict-sedna'

class Verbalex < DictSedna
  def initialize(collection, journal_path, key_path='', database = 'deb', admindict = nil)
    super
    @servlet = VerbalexServlet
  end

  def get_verb_list(search)
    list = []
    query = '[verb[some $p in lemmas/lemma satisfies starts-with($p, "'+search+'")]]'
    xquery_to_hash(query).each {|id,xml|
      doc = load_xml_string(xml.to_s)
      root = doc.root
      lemmas = []
      root.find('lemmas/lemma').to_a.each{|l|
        lemmas << l.content.to_s + ':' + l['sense']
      }
      lemma = lemmas.join(', ')
      author = root.find('metadata/author').to_a.first.content.to_s unless root.find('metadata/author').to_a.first.nil?
      last = ''
      unless root.find('metadata/history/changes').to_a.last.nil?
        lc = root.find('metadata/history/changes').to_a.last
        last = lc['user'] + '/' + lc['time']
      end
      hash = {'id'=>id, 'lemma'=>lemma, 'author'=>author, 'last'=>last}
      list << hash
    }
    return list
  end
  
  def get_perm(user)
    xml = @admindict.get('user'+user)
    doc = REXML::Document.new(xml.to_s)
    perm_el = doc.root.elements["//service[@code='verbalex']/dict[@code='verbalex']"]
    if perm_el != nil
      perm = perm_el.attributes['perm'].to_s
      return perm
    else
      return nil
    end
  end

end
