require 'dict/dict-sedna'

class Fni < DictSedna
  attr_reader :lock_timeout
  def initialize(collection, journal_path, key_path='', database = 'deb', admindict = nil)
    super
    @servlet = FniServlet
    admindict.lock_timeout = 60*60 #one hour
  end

  def get_entry(entry_id)
    xml = ''
    begin
      xml = get(entry_id).to_s.strip
      if xml != ''
        varinfo = var_lang(entry_id)
        unless varinfo.nil?
          doc = load_xml_string(xml)
          doc.find('/entry/me').each{|v|
            v['language'] = varinfo[v.content.to_s] if varinfo[v.content.to_s] != nil
          }
          xml = doc.to_s
        end
      end
    rescue => e
      xml = ''
    end
    return xml.to_s
  end


  def get_all_entries(search, parse=true)
    list = []
    first = true
    @container.each{|xml|
      id = xml.to_document.get_metadata('http://www.sleepycat.com/2002/dbxml', 'name')
      if parse
        doc = load_xml_string(xml.to_s)
        root = doc.root
        cz = root.find('word_cz').to_a.first.to_s
        en = root.find('word_en').to_a.first.to_s
        de = root.find('word_de').to_a.first.to_s
        fr = root.find('word_fr').to_a.first.to_s
        final = 'final' + root['final'].to_s
        kats = []
        root.find('domain').to_a.each{|k|
          kats << {'kat' => k.to_s}
        }
        list << {'id' => id,'cz' => cz.to_s, 'en'=> en.to_s, 'fr' => fr.to_s, 'de' => de.to_s, 'kat' => kats, 'final' => final} if cz != nil
      else
        list << {'id' => id}
      end
    }
    return list
  end

  def get_status(xmldoc)
    root = xmldoc.root
    status = ''
    if root['me'].to_s == 'true' and root['xref'].to_s != 'true'
      status = 'ME'
    end
    senses_ar = root.find('//sense').to_a
    is_me_only = true
    if root['xref'].to_s == 'true' or senses_ar.length > 1
      st_ar = []
      senses_ar.each{|xrs|
        s_nr = ''
        s_nr = xrs.find('s').to_a.first.content.to_s if senses_ar.length > 1 and not xrs.find('s').to_a.first.nil?
        exp_ar = []
        xrs.find('expl/ppkk').each{|exp|
          xrsa = exp.find('me').to_a
          if xrsa.length > 0
            xr_ar = []
            xrsa.each{|xr|
              xr_ar << xr.content.to_s
            }
            exp_ar << 'var. of ' + xr_ar.join(' or ')
          else
            if root['me'].to_s == 'true'
              exp_ar << 'ME' unless exp_ar.include?('ME')
            else
              exp_ar << 'var'
            end
          end
        }
        st_t = exp_ar.join('; ')
        st_t = 'RI, ' + st_t if xrs['ri'] == 'true'
        is_me_only = false if st_t != 'ME'
        st_ar << s_nr + ' ' + st_t

      }
      status = st_ar.sort.join('; ')
      status = 'ME' if is_me_only
    end
    
    if root['me'].to_s == '' and root['xref'].to_s == ''
      if not(root.find('sense/expl/ppkk').to_a.first.nil?) and root.find('sense/expl/ppkk').to_a.first.content != '.' and root.find('sense/expl/ppkk').to_a.first.content != ''
        if root.find('sense/expl/ppkk/me').to_a.length == 0
          #status = 'ME in Reaney'
          status = 'NYS'
        else
          status = 'var. in Reaney'
        end
      end
    end

    if root['quarantine'] == 'true'
      status = 'Q'
    end

    status = 'NEW' if status == ''

    if not status.include?('RI') and xmldoc.find('//sense[@ri="true"]').to_a.length > 0
      status = 'RI, ' + status
    end

    return status
  end

  def get_progress(xmldoc, ed)
    root = xmldoc.root

    first_ed = ''
    first_ed_time = ''
    last_ed = ''
    last_ed_time = ''
    last_ir_ed = ''
    last_ir_ed_time = ''
    last_sc_ed = ''
    last_sc_ed_time = ''
    progress = 'NYS'
    complete_time = ''
    if not root.find('meta/edit').to_a.first.nil?
      progress = 'WIP'
      if ed
        x = xmldoc.find('meta/edit').to_a
        edit_a = xmldoc.find('meta/edit[not(@author="dcole" or @author="liam" or @author="kmuhr" or @author="hammond" or @author="jmsscherr" or @author="turpie" or @author="deb" or @author="kilpatrick")]').to_a
        GC.start
        GC.disable
        edit = edit_a.sort{|x,y| x['time']<=>y['time']}
        GC.enable
        unless edit.first.nil?
          first_ed = edit.first['author']
          first_ed = 'hanks' if first_ed == 'patrick'
          first_ed_time = edit.first['time'].to_s[0..9]
          last_ed = edit.last['author']
          last_ed = 'hanks' if last_ed == 'patrick'
          last_ed_time = edit.last['time'].to_s[0..9]
        end
        #irish edit
        edit_ai = xmldoc.find('meta/edit[@author="liam" or @author="kmuhr"]').to_a
        GC.start
        GC.disable
        edit_i = edit_ai.sort{|x,y| x['time']<=>y['time']}
        GC.enable
        unless edit_i.first.nil?
          last_ir_ed = edit_i.last['author']
          last_ir_ed_time = edit_i.last['time'].to_s[0..9]
        end
        #scottish edit
        edit_ai = xmldoc.find('meta/edit[@author="hammond" or @author="turpie"]').to_a
        GC.start
        GC.disable
        edit_i = edit_ai.sort{|x,y| x['time']<=>y['time']}
        GC.enable
        unless edit_i.first.nil?
          last_sc_ed = edit_i.last['author']
          last_sc_ed_time = edit_i.last['time'].to_s[0..9]
        end
      end
    end
    if root['progress_r'] == 'true'
      progress = 'R'
      complete_time = ''
      complete_time = root['complete_time'][0..9] unless root['complete_time'].nil?
      progress += ', Q' if root['progress_q'] == 'true'
      progress += ', EB' if root['progress_eb'] == 'true'
    end
    progress += ', GBLoc' if root['progress_loc'].to_s == 'true' and (progress.include?('WIP') or root['progress_r'].to_s == 'true')
    progress += ', IreLoc' if root['progress_ireloc'].to_s == 'true' and (progress.include?('WIP') or root['progress_r'].to_s == 'true')
    progress += ', LocEtym' if root['progress_locetym'].to_s == 'true' and (progress.include?('WIP') or root['progress_r'].to_s == 'true')
    progress += ', RelEtym' if root['progress_reletym'].to_s == 'true' and (progress.include?('WIP') or root['progress_r'].to_s == 'true')
    if root['progress_p'] == 'true'
      progress = 'P'
    end

    return progress, first_ed, first_ed_time, last_ed, last_ed_time, complete_time, last_ir_ed, last_ir_ed_time, last_sc_ed, last_sc_ed_time
  end

  def prepare_entry(id, xml)
    doc = load_xml_string(xml.to_s)
    return nil if doc.nil?
    root = doc.root
    hw = root.find('hw').to_a.first.content.to_s
    $stderr.puts hw
    freq_uk = 0
    freq_roi = 0
    freq_1881 = 0
    freq_gb_2011 = 0
    freq_ir_2008 = 0
    freq_ir_1911 = 0
    freq_uk = root.find('freq_uk').to_a.first.content.to_s unless root.find('freq_uk').to_a.first.nil? or root.find('freq_uk').to_a.first.content.to_s == ''
    freq_roi = root.find('freq_roi').to_a.first.content.to_s unless root.find('freq_roi').to_a.first.nil? or root.find('freq_roi').to_a.first.content.to_s == ''
    freq_1881 = root.find('freq_uk_1881').to_a.first.content.to_s unless root.find('freq_uk_1881').to_a.first.nil? or root.find('freq_uk_1881').to_a.first.content.to_s == ''
    freq_gb_2011 = root.find('freq_gb_2011').to_a.first.content.to_s unless root.find('freq_gb_2011').to_a.first.nil? or root.find('freq_gb_2011').to_a.first.content.to_s == ''
    freq_ir_2008 = root.find('freq_ir_2008').to_a.first.content.to_s unless root.find('freq_ir_2008').to_a.first.nil? or root.find('freq_ir_2008').to_a.first.content.to_s == ''
    freq_ir_1911 = root.find('freq_ir_1911').to_a.first.content.to_s unless root.find('freq_ir_1911').to_a.first.nil? or root.find('freq_ir_1911').to_a.first.content.to_s == ''

    status = get_status(doc)
    progress, first_ed, first_ed_time, last_ed, last_ed_time, complete_time, last_ir_ed, last_ir_ed_time, last_sc_ed, last_sc_ed_time = get_progress(doc, true)

    #km_ed_time = nil
    #if last_ed != 'kmuhr'
    #  km = doc.find('meta/edit[@author="kmuhr"]').to_a.sort{|x,y| x['time']<=>y['time']}
    #  km_ed_time = km.last['time'][0..9] unless km.last.nil?
    #end
    pr = doc.root['pr'].to_s.upcase

    varsa = [URI.encode(URI.encode(hw))]
    doc.find('me').each{|me|
      varsa << URI.encode(URI.encode(me.content.to_s))
    }
    vars = varsa.join('%21')
    
    checkoup = ''
    begin
      oupxml = @admindict.array['oup'].get(id)
      checkoup = 'oup' if oupxml.to_s != ''
    rescue => e
      $stderr.puts e
    end


    return {'id' => id,'hw' => hw.to_s, 'freq_uk'=>freq_uk, 'freq_roi'=>freq_roi, 'status'=>status, 'first_ed'=>first_ed, 'first_ed_time'=>first_ed_time, 'last_ed'=>last_ed, 'last_ed_time'=>last_ed_time, 'progress'=>progress, 'complete_time'=>complete_time, 'last_ir_ed'=>last_ir_ed, 'last_ir_ed_time'=>last_ir_ed_time, 'pr'=>pr, 'vars'=>vars, 'freq_1881'=>freq_1881, 'last_sc_ed'=>last_sc_ed, 'last_sc_ed_time'=>last_sc_ed_time, 'freq_gb_2011'=>freq_gb_2011, 'freq_ir_2008'=>freq_ir_2008, 'freq_ir_1911'=>freq_ir_1911, 'check_'+checkoup=>'1'} if hw != nil
  end

  def build_query_part(query_array)
    queries = []
    if query_array['hw'].to_s != ''
      case query_array['where_hw']
        when 'cont'
          func = 'contains'
        when 'end'
          func = 'ends-with'
        when 'regexp'
          func = 'matches'
        else
          func = 'starts-with'
      end
      queries << "#{func}(hw, '#{query_array['hw'].gsub("'", "&apos;")}')"
    end

    if query_array['search'].to_s != '' and query_array['where_search'].to_s != ''
      search = query_array['search'].to_s.gsub("'", "&apos;")
      case query_array['where_search']
        when 'src'
          queries << "//src='#{search}'"
        when 'eb'
          #queries << "contains(sense/eb/arch, '#{search}')"
          queries << "(some $p in sense/eb/arch satisfies contains($p, '#{search}'))"
        when 'expl'
          #queries << "contains(sense/expl/ppkk, '#{search}')"
          queries << "(some $p in sense/expl/ppkk satisfies contains($p, '#{search}'))"
        when 'comment'
          queries << "contains(comment, '#{search}')"
        when 'musings'
          queries << "contains(musings, '#{search}')"
        when 'heartland_gb'
          queries << "contains(heartland_gb, '#{search}')"
        when 'heartland_ir'
          queries << "contains(heartland_ir, '#{search}')"
        when 'editors'
          queries << "contains(editors, '#{search}')"
        when 'other_info'
          #queries << "contains(sense/other_info, '#{search}')"
          queries << "(some $p in sense/other_info satisfies contains($p, '#{search}'))"
        when 'lang'
          if search == 'NULL'
            queries << "(some $p in sense/@language satisfies $p='')"
          else
            queries << "(some $p in sense/@language satisfies contains(lower-case($p), lower-case('#{search}')))"
          end
        when 'typology'
          queries << "(some $p in sense/expl/ppkk/@category satisfies contains(lower-case($p), lower-case('#{search}')))"
        when 'all'
          queries << "(contains(musings, '#{search}') or contains(comment, '#{search}') or (some $p in sense/eb/arch satisfies contains($p, '#{search}')) or (some $p in sense/expl/ppkk satisfies contains($p, '#{search}')) or contains(heartland_gb, '#{search}') or contains(heartland_gb_comment, '#{search}') or contains(heartland_ir, '#{search}') or (some $p in sense/other_info satisfies contains($p, '#{search}')) or (some $p in sense/references satisfies contains($p, '#{search}')) or (some $p in sense/editors satisfies contains($p, '#{search}')) or (some $p in sense/@language satisfies contains(lower-case($p), lower-case('#{search}'))) or (some $p in sense/expl/ppkk/@category satisfies contains(lower-case($p), lower-case('#{search}'))) or (contains(editors, '#{search}')) )"
      end
    end

    if query_array['pr'].to_s != ''
      queries << "@pr = '#{query_array['pr']}'"
    end

    if query_array['ri'].to_s == 'true'
      queries << "sense/@ri = 'true'"
    end

    if query_array['q'].to_s == 'true'
      queries << "@progress_q = 'true'"
    end

    if query_array['r'].to_s == 'true'
      queries << "@progress_r = 'true'"
    end

    case query_array['status']
      when 'me'
        #queries << "(@me ='true' and not(@xref='true'))"
        queries << "(@me ='true' )"
      when 'mevar'
        queries << "(@me ='true' or @xref='true')"
      when 'new'
        queries << "(not(@me='true') and not(@xref='true') and count(sense/expl/ppkk)=0 and not(sense/@ri='true'))"
      when 'ri'
        queries << "sense/@ri='true'"
      when 'reaney'
        queries << "(not(@me='true') and not(@xref='true') and count(sense/expl/ppkk/me)>0)"
      when 'wip'
        queries << "not(@progress_r = 'true')"
      when 'q'
        queries << "@progress_q = 'true'"
      when 'nys'
        #queries << "count(meta/edit) = 0"
        queries << "(not(@me != '') and not(@xref != '') and sense/expl/ppkk and sense/expl/ppkk != '' and sense/expl/ppkk != '.' and count(sense/expl/ppkk/me)=0)"
      when 'var'
        queries << "(not(@me != '') and not(@xref != '') and sense/expl/ppkk and sense/expl/ppkk != '' and sense/expl/ppkk != '.' and count(sense/expl/ppkk/me)>0 and not(@quarantine='true'))"
    end

    query_part = '(' + queries.join(' and ') + ')'
    query_part = '(not ' + query_part + ')' if query_array['not'].to_s == 'true'
    if query_array['andor'] != ''
      if query_array['andor'] == 'or'
        query_part = 'or '+query_part
      else
        query_part = 'and '+query_part
      end
    end
    $stderr.puts 'QUERY ' + query_part
    return query_part
  end

  def query_to_array(query_array)
    queries = []

    qpart = {}
    qpart['andor'] = ''
    qpart['hw'] = query_array['hw']
    qpart['where_hw'] = query_array['where_hw']
    qpart['search'] = query_array['search']
    qpart['where_search'] = query_array['where_search']
    qpart['pr'] = query_array['pr']
    qpart['ri'] = query_array['ri']
    qpart['q'] = query_array['q']
    qpart['r'] = query_array['r']
    qpart['status'] = query_array['status']
    queries << qpart

    part_count = 2
    while not query_array['hw'+part_count.to_s].nil?
      qpart = {}
      qpart['andor'] = query_array['andor'+part_count.to_s]
      qpart['not'] = query_array['not'+part_count.to_s]
      qpart['hw'] = query_array['hw'+part_count.to_s]
      qpart['where_hw'] = query_array['where_hw'+part_count.to_s]
      qpart['search'] = query_array['search'+part_count.to_s]
      qpart['where_search'] = query_array['where_search'+part_count.to_s]
      qpart['pr'] = query_array['pr'+part_count.to_s]
      qpart['ri'] = query_array['ri'+part_count.to_s]
      qpart['q'] = query_array['q'+part_count.to_s]
      qpart['r'] = query_array['r'+part_count.to_s]
      qpart['status'] = query_array['status'+part_count.to_s]
      qpart['hw'] = '' if qpart['hw'] == 'surname...'
      qpart['search'] = '' if qpart['search'] == 'search...'
      queries << qpart

      part_count += 1 
    end
    $stderr.puts queries
    return queries
  end

  def build_query2(query_array)
    query_param = query_to_array(query_array)
    queries = []
    query_param.each{|qrow|
      queries << build_query_part(qrow)
    }
    $stderr.puts queries.join(' ')
    return queries.join(' ')
  end

  def build_query(query_array)
    queries = []
    if query_array['hw'].to_s != ''
      case query_array['where_hw']
        when 'cont'
          func = 'contains'
        when 'end'
          func = 'ends-with'
        else
          func = 'starts-with'
      end
      queries << "#{func}(hw, '#{query_array['hw'].gsub("'", "&apos;")}')"
    end

    if query_array['search'].to_s != ''
      search = query_array['search'].to_s.gsub("'", "&apos;")
      case query_array['where_search']
        when 'src'
          queries << "//src='#{search}'"
        when 'eb'
          #queries << "contains(sense/eb/arch, '#{search}')"
          queries << "(some $p in sense/eb/arch satisfies contains($p, '#{search}'))"
        when 'expl'
          #queries << "contains(sense/expl/ppkk, '#{search}')"
          queries << "(some $p in sense/expl/ppkk satisfies contains($p, '#{search}'))"
        when 'comment'
          queries << "contains(comment, '#{search}')"
        when 'heartland_gb'
          queries << "contains(heartland_gb, '#{search}')"
        when 'other_info'
          #queries << "contains(sense/other_info, '#{search}')"
          queries << "(some $p in sense/other_info satisfies contains($p, '#{search}'))"
        when 'lang'
          queries << "(some $p in sense/@language satisfies contains(lower-case($p), lower-case('#{search}')))"
        when 'typology'
          queries << "(some $p in sense/expl/ppkk/@category satisfies contains(lower-case($p), lower-case('#{search}')))"
        when 'all'
          queries << "(contains(comment, '#{search}') or (some $p in sense/eb/arch satisfies contains($p, '#{search}')) or (some $p in sense/expl/ppkk satisfies contains($p, '#{search}')) or contains(heartland_gb, '#{search}') or contains(heartland_gb_comment, '#{search}') or contains(heartland_ir, '#{search}') or (some $p in sense/other_info satisfies contains($p, '#{search}')) or (some $p in sense/references satisfies contains($p, '#{search}')) or (some $p in sense/editors satisfies contains($p, '#{search}')) or (some $p in sense/@language satisfies contains(lower-case($p), lower-case('#{search}'))) or (some $p in sense/expl/ppkk/@category satisfies contains(lower-case($p), lower-case('#{search}'))) )"
      end
    end

    if query_array['pr'].to_s != ''
      queries << "@pr = '#{query_array['pr']}'"
    end

    if query_array['ri'].to_s == 'true'
      queries << "sense/@ri = 'true'"
    end

    if query_array['q'].to_s == 'true'
      queries << "@progress_q = 'true'"
    end

    case query_array['status']
      when 'me'
        queries << "(@me ='true' and not(@xref='true'))"
      when 'mevar'
        queries << "(@me ='true' or @xref='true')"
      when 'new'
        queries << "(not(@me='true') and not(@xref='true') and count(sense/expl/ppkk)=0 and not(sense/@ri='true'))"
      when 'ri'
        queries << "sense/@ri='true'"
      when 'reaney'
        queries << "(not(@me='true') and not(@xref='true') and count(sense/expl/ppkk/me)>0)"
    end

    conditions = queries.join(' and ')
    $stderr.puts 'QUERY '+conditions
    return conditions
  end

  def get_entries(query={}, parse=true)
    #search_cond = build_query(query)
    #$stderr.puts build_query2(query)
    search_cond = build_query2(query)
    $stderr.puts search_cond
    list = []
    query = "[entry[#{search_cond}]]"
    $stderr.puts query
    xquery_to_hash(query).each {|id,xml|
      if parse 
        list << prepare_entry(id, xml)
      else
        list << {'id' => id}
      end
    }
    return list
  end

  
  def get_perm(user, dict=@dictcode)
    xml = @admindict.get('user'+user)
    doc = load_xml_string(xml.to_s)
    $stderr.puts dict
    perm_ar = doc.find("//service[@code='fni']/dict[@code='"+dict+"']").to_a
    $stderr.puts perm_ar
    if perm_ar.size > 0
      perm = perm_ar.first['perm'].to_s
      return perm
    else
      return nil
    end
  end

  def get_for_edit(id)
    begin
      xml = get_entry(id)
    rescue
      return ''
    end
    return '' if xml.to_s == ''
    $stderr.puts '----'+id
    doc = load_xml_string(xml.to_s)
    $stderr.puts doc.root['entry_id']
    doc.root['entry_id'] = doc.root['entry_id'].gsub('&apos;', "__")
    $stderr.puts doc.root['entry_id']
    if doc.find('/entry/eb').to_a.size == 0
      neb = XML::Node.new('eb')
      nar = XML::Node.new('arch')
      neb << nar
      doc.root << neb
    end
    if doc.find('/entry/expl').to_a.size == 0
      neb = XML::Node.new('expl')
      nar = XML::Node.new('ppkk')
      neb << nar
      doc.root << neb
    end
    if doc.find('/entry/sense').size == 0
      ns = XML::Node.new('sense')
      ne = XML::Node.new('expl')
      ns << ne
      ne << XML::Node.new('ppkk')
      doc.root << ns
    end
    doc.find('//sense[not(expl/ppkk)]').each{|el|
      ne = XML::Node.new('expl')
      ne << XML::Node.new('ppkk')
      el << ne
    }
    doc.find('//ex|//cit|//arch|//med|//igi|//other_info|//ppkk|//dafn|//references').each{|el|
      next if el.content.to_s == ''
      elt = el.to_s
      #elt.gsub!('<xref>', '&lt;xref&gt;')
      #elt.gsub!('</xref>', '&lt;/xref&gt;')
      elt.gsub!('<cit>', '')
      elt.gsub!('</cit>', '')
      elt.gsub!('<cit/>', '')
      elt.gsub!('<ex>', '')
      elt.gsub!('</ex>', '')
      elt.gsub!('<ex/>', '')
      elt.gsub!(/<arch type="[^"]*">/, '')
      elt.gsub!('<arch>', '')
      elt.gsub!('<arch/>', '')
      elt.gsub!('</arch>', '')
      elt.gsub!('<med>', '')
      elt.gsub!('</med>', '')
      elt.gsub!('<med/>', '')
      elt.gsub!('<igi>', '')
      elt.gsub!('</igi>', '')
      elt.gsub!('<igi/>', '')
      elt.gsub!('<other>', '')
      elt.gsub!('</other>', '')
      elt.gsub!('<other/>', '')
      elt.gsub!('<other_info>', '')
      elt.gsub!('</other_info>', '')
      elt.gsub!('<other_info/>', '')
      elt.gsub!(/<ppkk category="[^"]*">/, '')
      elt.gsub!(/<ppkk category="[^"]*" language="[^"]*">/, '')
      elt.gsub!(/<ppkk language="[^"]*">/, '')
      elt.gsub!('<ppkk>', '')
      elt.gsub!('</ppkk>', '')
      elt.gsub!('<ppkk/>', '')
      elt.gsub!('<dafn>', '')
      elt.gsub!('</dafn>', '')
      elt.gsub!('<dafn/>', '')
      elt.gsub!('<references>', '')
      elt.gsub!('</references>', '')
      elt.gsub!('<references/>', '')
      el.content = elt
    }
    xml = doc.to_s
    xml.gsub!('<i>', '&lt;i&gt;')
    xml.gsub!('</i>', '&lt;/i&gt;')
    xml.gsub!('<cit/>', '<cit></cit>')
    xml.gsub!('<ex/>', '<ex></ex>')
    $stderr.puts xml
    return xml
  end

  def get_edits(id)
    edits = []
    begin
      xml = get(id)
      doc = load_xml_string(xml.to_s)
      doc.find('/entry/meta/edit').each{|el|
        author = el['author'].to_s
        time = el['time'].to_s
        edits << {'time'=>time, 'author'=>author}
      }
    rescue => e
      $stderr.puts e
    end
    return edits
  end

  def entry_exists(id)
    begin
      get(id)
      return true
    rescue => e
      return false
    end
  end

  def add_meta(xml, edits)
    doc = load_xml_string(xml)
    doc.root << meta = XML::Node.new('meta')
    edits.each{|edit|
      meta << ed = XML::Node.new('edit')
      ed['time'] = edit['time']
      ed['author'] = edit['author']
    }    
    return doc.to_s
  end

  def load_sources(leave_sub=false)
    src = []
    srcf = File.new('/var/lib/deb-server/files/surnames/abbrevs_revised.txt')
    srcf.each{|line|
      sra = line.split('#')
      #sra[1].gsub!('<', '&lt;')
      #sra[1].gsub!('>', '&gt;')
      unless leave_sub and sra[4].to_s.strip == 'sub'
        src << {'id'=>sra[0].to_s, 'abbr'=>sra[1].to_s, 'short'=>sra[2], 'full'=>sra[3].to_s.strip, 'type'=>sra[4].to_s.strip} if sra[2] != '' and sra[2] != nil
      end
    }
    srcf.close
    src.sort!{|x,y| x['short'] <=> y['short']}
    src2 = []
    srcf = File.new('/var/lib/deb-server/files/surnames/abbrevs2_revised.txt')
    srcf.each{|line|
      sra = line.split('#')
      #sra[1].gsub!('<', '&lt;')
      #sra[1].gsub!('>', '&gt;')
      src2 << {'abbr'=>sra[0].to_s, 'full'=>sra[1]} if sra[1] != '' and sra[1] != nil
    }
    srcf.close
    return src, src2
  end

  def write_source(file, doc)
    if file == 'abbr'
      File.rename('/var/lib/deb-server/files/surnames/abbrevs_revised.txt', '/var/lib/deb-server/files/surnames/abbrevs_revised.txt-'+Time.now.to_i.to_s)
      srcf = File.open('/var/lib/deb-server/files/surnames/abbrevs_revised.txt', 'w')
    end
    if file == 'abbr2'
      File.rename('/var/lib/deb-server/files/surnames/abbrevs2_revised.txt', '/var/lib/deb-server/files/surnames/abbrevs2_revised.txt-'+Time.now.to_i.to_s)
      srcf = File.open('/var/lib/deb-server/files/surnames/abbrevs2_revised.txt', 'w')
    end
    srcf.write(doc)
    srcf.close
    return
  end

  def var_info(id)
    begin
      xml = get(id)
      doc = load_xml_string(xml.to_s)
      if doc.root['quarantine'].to_s == 'true'
        return 'Q'
      else
        uk = ''
        roi = ''
        uk = doc.find('freq_gb_2011').to_a.first.content.to_s unless doc.find('freq_gb_2011').to_a.first.nil?
        roi = doc.find('freq_ir_2008').to_a.first.content.to_s unless doc.find('freq_ir_2008').to_a.first.nil?
        return uk + '/' + roi
      end
    rescue
      return ''
    end
  end

  def var_lang(id)
    begin
      xml = get(id)
      doc = load_xml_string(xml.to_s)
      if doc.find('//sense').size > 1
        lastlang = nil
        addinfo = false
        doc.find('//sense').each{|s|
          addinfo = true if lastlang != nil and s['language'] != lastlang
          lastlang = s['language']
        }
        #2+ sense, different languages
        if addinfo
          info_ar = {}
          query = 'for $x in collection("surnames")/entry/sense[@ri="true"] where some $p in $x//ppkk/me satisfies $p="'+doc.find('//hw').first.content.to_s+'" return <a>{$x/../hw/text()};{data($x/@language)}</a>'
          query(query).each{|qr|
            qr.gsub!(/<\/?a>/,'')
            qra = qr.split(';')
            info_ar[qra[0]] = qra[1]
          }
          return info_ar
        end
      end
      return nil
    rescue
      return nil
    end
  end

  def prepare_source_file(array)
    strings = []
    array.each{|h|
      strings << h['id'].to_s + '#' + h['abbr'].to_s + '#' + h['short'].to_s + '#' + h['full'].to_s + '#' + h['type'].to_s
    }
    return strings.join("\n")
  end

  def add_source(short, full, type)
    src_b, src_o = load_sources
    newid = 0
    src_b.each{|h|
      newid = h['id'].to_i if h['id'].to_i > newid
    }
    newid += 1
    src_b << {'id'=>newid.to_s, 'abbr'=>'', 'short'=>short, 'full'=>full, 'type'=>type}
    text = prepare_source_file(src_b)
    write_source('abbr', text)
    return true
  end

  def update_source(id, new, full=nil, type=nil)
    #update source file
    src_b, src_o = load_sources
    orig = ''
    src_b.each{|h|
      if h['id'] == id
        orig = h['short']
        h['short'] = new
        h['full'] = full unless full.nil?
        h['type'] = type
      end
    }
    text = prepare_source_file(src_b)
    write_source('abbr', text)

    if orig != ''
      #update db entries
      search_orig = orig.gsub('<i>', '')
      search_orig.gsub!('</i>', '')
      query = "[entry[//src='#{search_orig}']]"
      $stderr.puts query
      xquery_to_hash(query).each{|key,entry|
        $stderr.puts key
        #$stderr.puts entry
        new_entry = entry.to_s.gsub('<src>'+orig+'</src>', '<src>'+new+'</src>')
        #$stderr.puts new_entry
        #$stderr.puts '******'
        update(key, new_entry, false)
      }
    end
    return true
  end

  def delete_source(id)
    #delete from source file
    src_b, src_o = load_sources
    short = ''
    src_b.each{|h|
      if h['id'].to_i == id.to_i
        short = h['short']
      end
    }
    src_b.delete_if{|h| h['id'].to_i == id.to_i}
    text = prepare_source_file(src_b)
    write_source('abbr', text)

    entries = []
    if short != ''
      #find db entries
      search_orig = short.gsub('<i>', '')
      search_orig.gsub!('</i>', '')
      query = "[entry[//src='#{search_orig}']]"
      $stderr.puts query
      xquery_to_list(query).each{|key|
        $stderr.puts key
        entries << {'id'=>key}
      }
    end
    return entries
  end

  def get_cluster(entry_id)
    $stderr.puts '*-'+entry_id
    refs = {}
    main = ''
    xml = get(entry_id)
    doc = load_xml_string(xml.to_s)
    $stderr.puts xml
    xrs = []
    doc.find('me').each{|xr|
      xref = xr.content.to_s
      xrs << xref
    }
    doc.find('sense/ex').each{|se|
      text = se.to_s.gsub('&lt;','<').gsub('&gt;','>')
      text.scan(/[Ss]ee <me>([^<]*)<\/me>/).each{|m|
        xrs << m.to_s
      }
      text.scan(/[Vv]ariant of <me>([^<]*)<\/me>/).each{|m|
        xrs << m.to_s
      }
    }

    xrs.uniq.each{|xref|
      xref = URI.escape(xref)
      $stderr.puts '****'+xref
      refxml = get_for_edit(xref) 
      if refxml != ''
        refdoc = load_xml_string(refxml)
        freq = 0
        freq = refdoc.find('freq_uk').to_a.first.content.to_i unless refdoc.find('freq_uk').to_a.first.nil?
        refs[xref] = freq + 0
        if refdoc.root['xref'] != 'true'
          main = xref
          refdoc.find('me').each{|rxr|
            rxref = rxr.content.to_s
            vxml = get_for_edit(rxref)
            if vxml != ''
              vdoc = load_xml_string(vxml)
              freq = 0
              freq = vdoc.find('freq_uk').to_a.first.content.to_i unless vdoc.find('freq_uk').to_a.first.nil?
              refs[rxref] = freq + 0
            end
          }
        end
      end
    }
    
    keys = refs.sort{|a,b| a[0]<=>b[0]}.map{|a| a[0]}

    #keys = refs.keys
    keys.unshift(main) if main != ''
    keys.unshift(entry_id)

    return keys.uniq
  end

  def get_cluster_html(refs,dict_path='')
    data = ''
    showdafn = ''
    showigi = ''
    refs.uniq.each{|id|
      if id == '__new__'
        doc = "<entry entry_id='__new__'><hw></hw><sense><s>1</s><expl><ppkk/></expl><eb><arch/></eb></sense><dafn/><igi/><med/></entry>"
      else
        xml = get_for_edit(id)
        begin
          dafxml = @admindict.array['dafn2'].get(id)
          showdafn = 'true' if dafxml.to_s != ''
        rescue => e
          $stderr.puts e
        end
        showigi = 'true' if File.exists?('/var/lib/deb-server/files/surnames/igi/'+id[0,1]+'/'+id+'.xls')
        $stderr.puts '/var/lib/deb-server/files/surnames/igi/'+id[0,1]+'/'+id+'.xls'
        doc = load_xml_string(xml.to_s)
        doc.root['entry_id'] = id.gsub("'", '__')
        doc.root['entry_id'] = id.gsub("&apos;", '__')
      end
      data += apply_transform('edit-single', doc.to_s, [ ['path', '"'+dict_path+'"'], ['showdafn','"'+showdafn+'"'], ['showigi','"'+showigi+'"'] ])
    }
    return data
  end

  def find_xrefs(doc)
    names = []
    doc.find('//me').each{|x|
      names << x.content.to_s
    }
    return names.join(', ')
  end

  def add_var(entry_id, xref)
    xml = get(entry_id)
    if xml != ''
      doc = load_xml_string(xml.to_s)
      vars = doc.find('//me[text()="'+URI.unescape(xref)+'"]').to_a
      if vars.length == 0
        nx = XML::Node.new('me')
        nx.content = URI.unescape(xref)
        doc.root << nx
        update(entry_id, doc.to_s)
      end
    end
  end

  def add_var_sense(entry_id, xref)
    xml = get(entry_id)
    if xml != ''
      doc = load_xml_string(xml.to_s)
      vars = doc.find('//me[text()="'+URI.unescape(xref)+'"]').to_a
      if vars.length == 0
        senses = doc.find('/entry/sense').to_a
        nr = senses.size + 1
        nsense = '<sense><s>'+nr.to_s+'</s><expl><ppkk>see <me>'+URI.unescape(xref)+'</me>.</ppkk></expl></sense>'
        ns = load_xml_string(nsense)
        doc.root << ns.root.copy(true)
        doc.root['xref'] = 'true'
        update(entry_id, doc.to_s)
      end
    end
  end

  def add_var_check(entry_id, xref, reserve, res1881, ren)
    entry_id.gsub!('__',"'")
    xml = get_entry(entry_id)
    $stderr.puts entry_id
    $stderr.puts xref
    if xml != ''
      doc = load_xml_string(xml.to_s)
      vars = doc.find('//me[text()="'+URI.unescape(xref.gsub('__','&apos;'))+'"]').to_a
      if vars.length == 0
        senses = doc.find('/entry/sense').to_a
        nr = senses.size + 1
        nsense = '<sense><s>'+nr.to_s+'</s><expl><ppkk>see <me>'+URI.unescape(xref.gsub('__',"'"))+'</me>.</ppkk></expl></sense>'
        ns = load_xml_string(nsense)
        doc.root << ns.root.copy(true)
        doc.root['xref'] = 'true'
        update(entry_id, doc.to_s)
      end
    end
    xmlr = ''
    if xml == ''
      xmlr = reserve.get_entry(entry_id)
      if xmlr != ''
        doc = load_xml_string(xmlr.to_s)
        vars = doc.find('//me[text()="'+URI.unescape(xref.gsub('__','&apos;'))+'"]').to_a
        if vars.length == 0
          senses = doc.find('/entry/sense').to_a
          nr = senses.size + 1
          nsense = '<sense><s>'+nr.to_s+'</s><expl><ppkk>see <me>'+URI.unescape(xref.gsub('__',"'"))+'</me>.</ppkk></expl></sense>'
          ns = load_xml_string(nsense)
          doc.root << ns.root.copy(true)
          doc.root['xref'] = 'true'
        end
        update(entry_id, doc.to_s)
        reserve.delete(entry_id)
      end
      xmlr1 = res1881.get_entry(entry_id)
      if xmlr1 != ''
        doc = load_xml_string(xmlr1.to_s)
        vars = doc.find('//me[text()="'+URI.unescape(xref.gsub('__','&apos;'))+'"]').to_a
        if vars.length == 0
          senses = doc.find('/entry/sense').to_a
          nr = senses.size + 1
          nsense = '<sense><s>'+nr.to_s+'</s><expl><ppkk>see <me>'+URI.unescape(xref.gsub('__',"'"))+'</me>.</ppkk></expl></sense>'
          ns = load_xml_string(nsense)
          doc.root << ns.root.copy(true)
          doc.root['xref'] = 'true'
        end
        update(entry_id, doc.to_s)
        res1881.delete(entry_id)
      end
      xmlre = ren.get_entry(entry_id)
      if xmlre != ''
        doc = load_xml_string(xmlre.to_s)
        vars = doc.find('//me[text()="'+URI.unescape(xref.gsub('__','&apos;'))+'"]').to_a
        if vars.length == 0
          senses = doc.find('/entry/sense').to_a
          nr = senses.size + 1
          nsense = '<sense><s>'+nr.to_s+'</s><expl><ppkk>see <me>'+URI.unescape(xref.gsub('__',"'"))+'</me>.</ppkk></expl></sense>'
          ns = load_xml_string(nsense)
          doc.root << ns.root.copy(true)
          doc.root['xref'] = 'true'
        end
        update(entry_id, doc.to_s)
        ren.delete(entry_id)
      end
    end
    result = ''
    result = 'in main' if xml != ''
    result = 'in reserve' if xml == '' and xmlr != ''
    result = 'in res1881' if xml == '' and xmlr1 != ''
    result = 'in ren' if xml == '' and xmlre != ''
    result = 'not found' if xml == '' and xmlr == '' and xmlr1 == '' and xmlre == ''
    return result
  end

  def get_statistics
    freqs = {'uk_100'=>0, 'uk_200'=>0, 'uk_500'=>0, 'uk_1000'=>0, 'ir_100'=>0, 'ir_200'=>0, 'ir_500'=>0, 'ir_1000'=>0}
    weeks = {}
    ts_me = 0
    ts_var = 0
    ts_ri = 0
    ts_q = 0
    ts_new = 0
    tp_r = 0
    tp_nys = 0
    tp_wip = 0
    tp_q = 0
    tp_eb = 0
    tp_loc = 0
    tp_p = 0

    auth_comp = {}
    auth_comp2 = {}
    entrytype = {
      'total'=>{'r'=>0,'nys'=>0,'wip'=>0,'q'=>0,'eb'=>0,'loc'=>0,'p'=>0},
      'me'=>{'r'=>0,'nys'=>0,'wip'=>0,'q'=>0,'eb'=>0,'loc'=>0,'p'=>0},
      'var'=>{'r'=>0,'nys'=>0,'wip'=>0,'q'=>0,'eb'=>0,'loc'=>0,'p'=>0},
      'ri'=>{'r'=>0,'nys'=>0,'wip'=>0,'q'=>0,'eb'=>0,'loc'=>0,'p'=>0},
      'q'=>{'r'=>0,'nys'=>0,'wip'=>0,'q'=>0,'eb'=>0,'loc'=>0,'p'=>0},
      'new'=>{'r'=>0,'nys'=>0,'wip'=>0,'q'=>0,'eb'=>0,'loc'=>0,'p'=>0},
      'xrr'=>{'r'=>0,'nys'=>0,'wip'=>0,'q'=>0,'eb'=>0,'loc'=>0,'p'=>0},
      'mer'=>{'r'=>0,'nys'=>0,'wip'=>0,'q'=>0,'eb'=>0,'loc'=>0,'p'=>0}
    }
    r = xquery_to_array('/entry')
    total = r.size 
    r.each{|xml|
      #break if ts_ri >= 2 and ts_me >= 200
      doc = load_xml_string(xml.to_s)
      if doc.nil?
        $stderr.puts xml
        next
      end
      $stderr.puts doc.find('hw').to_a.first.content
      fr_uk = 0
      fr_ir = 0
      #fr_uk = doc.find('freq_uk').to_a.first.content.to_i unless doc.find('freq_uk').to_a.first.nil?
      fr_ir = doc.find('freq_roi').to_a.first.content.to_i unless doc.find('freq_roi').to_a.first.nil?
      #freqs['uk_100'] += 1 if fr_uk >= 100
      #freqs['uk_200'] += 1 if fr_uk >= 200
      #freqs['uk_500'] += 1 if fr_uk >= 500
      #freqs['uk_1000'] += 1 if fr_uk >= 1000
      freqs['ir_100'] += 1 if fr_ir >= 100
      freqs['ir_200'] += 1 if fr_ir >= 200
      freqs['ir_500'] += 1 if fr_ir >= 500
      freqs['ir_1000'] += 1 if fr_ir >= 1000

      status = get_status(doc)
      status.gsub!('RI, ', 'ri; ')
      progress, first_ed, first_ed_time, last_ed, last_ed_time, complete_time = get_progress(doc,false)

      status_a = status.split('; ')
      status_a.map! {|x| 
        if x.include?('var.') or x.include?(' var')
          x='var'
        elsif x.include?('ME')
          x='me'
        elsif x.include?('XR in R')
          x='xrr'
        elsif x.include?('ME in R')
          x='mer'
        elsif x.include?('RI')
          x='ri'
        else
          x=x.downcase.strip
        end
      }
      status_a << 'total'
      progress_a = progress.split(', ')
      progress_a.map! {|x| x.strip.downcase!}

      if status_a.include?('me')
        ts_me += 1
      end
      if status_a.include?('var')
        ts_var += 1
      end
      if status_a.include?('ri')
        ts_ri += 1
      end
      if status_a.include?('q')
        ts_q += 1
      end
      if status_a.include?('new')
        ts_new += 1
      end

      status_a.each{|st|
        progress_a.each{|pr|
          entrytype[st][pr] += 1 unless entrytype[st].nil? or entrytype[st][pr].nil?
        }
      }

      if doc.root['progress_r'] == 'true' and doc.root['complete_time'].to_s != ''
        ctime = Time.parse(doc.root['complete_time'])
        user = ''
        diff = nil
        doc.find('//edit').each{|m|
          mt = Time.parse(m['time'])
          if user == ''
            diff = (ctime.to_i - mt.to_i).abs
            user = m['author']
          end
          diffa = (ctime.to_i - mt.to_i).abs
          if diffa < diff
            diff = diffa
            user = m['author']
          end
        }
        user = 'hanks' if user == 'patrick'
        auth_comp[user] = 0 if auth_comp[user].nil?
        auth_comp[user] += 1
        week = ctime.strftime('%Y_%W')
        weeks[week] = {} if weeks[week].nil?
        weeks[week][user] = 0 if weeks[week][user].nil?
        weeks[week][user] += 1
      end

      #if doc.root['complete'] == 'true'
      #  complete += 1
      #  entrytype[actent]['c'] += 1

      #  #d_auth = []
      #  #doc.find('/entry/meta/edit').each{|ed|
      #  #  d_auth << ed['author'].to_s
      #  #}
      #  #d_auth.uniq.each{|author|
      #  #  auth_comp[author] = 0 if auth_comp[author].nil?
      #  #  auth_comp[author] += 1
      #  #  auth_comp2[author] = {'xref'=>0,'xrefnew'=>0,'me'=>0,'menew'=>0,'quar'=>0}  if auth_comp2[author].nil?
      #  #  auth_comp2[author][actent] += 1
      #  #}
      #end

      #if doc.find('meta/edit').to_a.size > 0
      #  wip += 1 unless doc.root['complete'] == 'true'
      #  entrytype[actent]['w'] += 1 unless doc.root['complete'] == 'true'
      #  w_auth = []
      #  doc.find('meta/edit').to_a.each{|edit|
      #    #$stderr.puts edit['author'] + edit['time']
      #    date = Date.parse(edit['time'].to_s[0..9])
      #    wy = date.year.to_s + '_' + "%02d" % date.cweek
      #    w_auth << wy +'|' + edit['author'].to_s
      #  }
      #  w_auth.uniq.each{|wa|
      #    waa = wa.split('|')
      #    wy = waa[0]
      #    aut = waa[1]
      #    weeks[wy] = {} if weeks[wy].nil?
      #    weeks[wy][aut] = 0 if weeks[wy][aut].nil?
      #    weeks[wy][aut] += 1
      #  }
      #end
    }

    statfile = File.open('/var/lib/deb-server/files/fni/stats.txt','w')
    statfile.puts 'total;'+total.to_s
    statfile.puts 'ts-me;'+ts_me.to_s
    statfile.puts 'ts-var;'+ts_var.to_s
    statfile.puts 'ts-ri;'+ts_ri.to_s
    statfile.puts 'ts-q;'+ts_q.to_s
    statfile.puts 'ts-new;'+ts_new.to_s
    entrytype.each{|st,stinfo|
      stinfo.each{|pr,prcount|
        statfile.puts 'count-'+st+'-'+pr+';'+prcount.to_s
      }
    }
    auth_comp.each{|a,c|
      statfile.puts 'comp_'+a+';'+c.to_s unless a =='deb'
    }
    #auth_comp2.each{|a,h|
    #  statfile.puts 'comp2_'+a+'_me;'+h['me'].to_s
    #  statfile.puts 'comp2_'+a+'_menew;'+h['menew'].to_s
    #  statfile.puts 'comp2_'+a+'_xref;'+h['xref'].to_s
    #  statfile.puts 'comp2_'+a+'_xrefnew;'+h['xrefnew'].to_s
    #  statfile.puts 'comp2_'+a+'_quar;'+h['quar'].to_s
    #}
    freqs.each{|fr,c|
      statfile.puts 'freq_'+fr+';'+c.to_s
    }
    weeks.each{|w,aut|
      aut.each{|a,c|
        statfile.puts 'week_'+w+'_'+a+';'+c.to_s unless a == 'deb'
      }
    }
    statfile.close
    return
  end

  def replace(find, replace, where)
    ##update db entries
    #search_orig = orig.gsub('<i>', '')
    #search_orig.gsub!('</i>', '')
    case where
    when 'cit'
      query = "[entry[(some $p in sense/eb/arch satisfies (contains($p,'#{find.gsub("'",'&apos;')}')))]]"
    when 'expl'
      query = "[entry[(some $p in sense/expl/ppkk satisfies (contains($p,'#{find.gsub("'",'&apos;')}')))]]"
    when 'comment'
      query = "[entry[(some $p in comment satisfies (contains($p,'#{find.gsub("'",'&apos;')}')))]]"
    when 'musings'
      query = "[entry[(some $p in musings satisfies (contains($p,'#{find.gsub("'",'&apos;')}')))]]"
    when 'other'
      query = "[entry[(some $p in other_info satisfies (contains($p,'#{find.gsub("'",'&apos;')}')))]]"
    when 'references'
      query = "[entry[(some $p in references satisfies (contains($p,'#{find.gsub("'",'&apos;')}')))]]"
    when 'heartland'
      query = "[entry[(some $p in heartland_gb satisfies (contains($p,'#{find.gsub("'",'&apos;')}')))]]"
    when 'heartland_ir'
      query = "[entry[(some $p in heartland_ir satisfies (contains($p,'#{find.gsub("'",'&apos;')}')))]]"
    when 'lang'
      query = "[entry[(some $p in sense satisfies (contains($p/@language, '#{find.gsub("'",'&apos;')}')))]]"
    else
      query = "[entry[(some $p in comment satisfies (contains($p,'#{find.gsub("'",'&apos;')}'))) or (some $p in sense/eb/arch satisfies (contains($p,'#{find.gsub("'",'&apos;')}'))) or (some $p in sense/expl/ppkk satisfies (contains($p,'#{find.gsub("'",'&apos;')}'))) or (some $p in sense satisfies (contains($p/@language, '#{find.gsub("'",'&apos;')}'))) or (some $p in musings satisfies (contains($p,'#{find.gsub("'",'&apos;')}'))) or (some $p in other_info satisfies (contains($p,'#{find.gsub("'",'&apos;')}'))) or (some $p in heartland_gb satisfies (contains($p,'#{find.gsub("'",'&apos;')}'))) or (some $p in heartland_ir satisfies (contains($p,'#{find.gsub("'",'&apos;')}')))]]"
    end
    $stderr.puts query
    count = 0
    count_r = 0
    xquery_to_hash(query).each{|key,xml|
      $stderr.puts key 
      #$stderr.puts xml
      doc = load_xml_string(xml.to_s)
      if where == 'lang' or where == 'all'
        doc.find("//sense[contains(@language,\"#{find.gsub('"','&quot;')}\")]").each{|c|
          c['language'] = c['language'].to_s.gsub(find, replace)
          count_r += 1 
        }
      end
      if where == 'cit' or where == 'all'
      #$stderr.puts "//arch[contains(.,\"#{find.gsub('"','&quot;')}\")]"
        doc.find("//arch[contains(.,\"#{find.gsub('"','&quot;')}\")]").each{|c|
          #$stderr.puts c.to_s
          newt = c.to_s.gsub(find, replace).gsub(/<arch[^>]*>/,'').gsub('</arch>','')
          #$stderr.puts newt
          c.content = newt
          count_r += 1 
        }
      end
      if where == 'expl' or where == 'all'
        doc.find("//ppkk[contains(.,\"#{find.gsub('"','&quot;')}\")]").each{|c|
          #$stderr.puts c.to_s
          newt = c.to_s.gsub(find, replace).gsub(/<ppkk[^>]*>/,'').gsub('</ppkk>','')
          #$stderr.puts newt
          c.content = newt
          count_r += 1 
        }
      end
      if where == 'comment' or where == 'all'
        doc.find("//comment[contains(.,\"#{find.gsub('"','&quot;')}\")]").each{|c|
          #$stderr.puts c.to_s
          newt = c.to_s.gsub(find, replace).gsub('<comment>','').gsub('</comment>','')
          #$stderr.puts newt
          c.content = newt
          count_r += 1 
        }
      end
      if where == 'musings' or where == 'all'
        doc.find("//musings[contains(.,\"#{find.gsub('"','&quot;')}\")]").each{|c|
          #$stderr.puts c.to_s
          newt = c.to_s.gsub(find, replace).gsub('<musings>','').gsub('</musings>','')
          #$stderr.puts newt
          c.content = newt
          count_r += 1 
        }
      end
      if where == 'references' or where == 'all'
        doc.find("//references[contains(.,\"#{find.gsub('"','&quot;')}\")]").each{|c|
          #$stderr.puts c.to_s
          newt = c.to_s.gsub(find, replace).gsub('<references>','').gsub('</references>','')
          #$stderr.puts newt
          c.content = newt
          count_r += 1 
        }
      end
      if where == 'other' or where == 'all'
        doc.find("//other_info[contains(.,\"#{find.gsub('"','&quot;')}\")]").each{|c|
          #$stderr.puts c.to_s
          newt = c.to_s.gsub(find, replace).gsub('<other_info>','').gsub('</other_info>','')
          #$stderr.puts newt
          c.content = newt
          count_r += 1 
        }
      end
      if where == 'heartland' or where == 'all'
        doc.find("//heartland_gb[contains(.,\"#{find.gsub('"','&quot;')}\")]").each{|c|
          #$stderr.puts c.to_s
          newt = c.to_s.gsub(find, replace).gsub('<heartland_gb>','').gsub('</heartland_gb>','')
          #$stderr.puts newt
          c.content = newt
          count_r += 1 
        }
      end
      if where == 'heartland_ir' or where == 'all'
        doc.find("//heartland_ir[contains(.,\"#{find.gsub('"','&quot;')}\")]").each{|c|
          #$stderr.puts c.to_s
          newt = c.to_s.gsub(find, replace).gsub('<heartland_ir>','').gsub('</heartland_ir>','')
          #$stderr.puts newt
          c.content = newt
          count_r += 1 
        }
      end

      data = doc.root.to_s
      data.gsub!('&lt;i&gt;', '<i>')
      data.gsub!('&lt;/i&gt;', '</i>')
      data.gsub!('&lt;b&gt;', '<b>')
      data.gsub!('&lt;/b&gt;', '</b>')
      data.gsub!('&lt;sn&gt;', '<sn>')
      data.gsub!('&lt;/sn&gt;', '</sn>')
      data.gsub!('&lt;me&gt;', '<me>')
      data.gsub!('&lt;/me&gt;', '</me>')
      data.gsub!('&lt;var&gt;', '<var>')
      data.gsub!('&lt;/var&gt;', '</var>')
      data.gsub!('&lt;X&gt;', '<var>')
      data.gsub!('&lt;/X&gt;', '</var>')
      data.gsub!('&lt;src&gt;', '<src>')
      data.gsub!('&lt;/src&gt;', '</src>')
      data.gsub!('&lt;xr&gt;', '<xr>')
      data.gsub!('&lt;/xr&gt;', '</xr>')
      #$stderr.puts data
      
      update(key, data, false)
      count += 1
    }
    return count, count_r
  end

  def move_entry(fromdb, todb, entry_id, force=false)
    begin
      entryxml = fromdb.get(entry_id)
    rescue
      $stderr.puts 'Error: entry '+entry_id+' not existing in source database'
      return 'Error: entry '+entry_id+' not existing in source database'
    end

    if force == false
    begin
      toxml = todb.get(entry_id)
      if toxml.to_s != ''
        $stderr.puts 'Error: entry '+entry_id+' exists in target database'
        #return 'Error: entry '+entry_id+' exists in target database'
        return 'in target'
      end
    rescue
    end
    end

    begin
      $stderr.puts 'save to target'
      todb.update(entry_id, entryxml)
      $stderr.puts 'saved'
      $stderr.puts 'delete from source'
      fromdb.delete(entry_id)
      $stderr.puts 'deleted'
    rescue => e
      $stderr.puts 'Error: unexpected error: '+e.message+"\n"+e.backtrace.join("\n")
      return 'Error: unexpected error: '+e.message
    end

    return 'entry moved'
  end

  def get_progress_report(namefrom, nameto, dict="surnames")
    query = "for $x in collection('#{dict}')/entry where $x/hw>='#{namefrom.gsub("'","&apos;")}' and $x/hw<='#{nameto.gsub("'","&apos;")}' return $x"
    ecount = 0
    sensecounts = {}
    statcounts = {'me'=>0, 'mer'=>0, 'meri'=>0, 'var'=>0, 'varri'=>0, 'xrr'=>0, 'new'=>0, 'q'=>0, 'other'=>0}
    progresscounts = {'r'=>0, 'q'=>0, 'nys'=>0, 'wip'=>0, 'other'=>0, 'qri'=>0, 'wipri'=>0, 'inv'=>0, 'res'=>0}
    query(query).each {|entry|
      doc = load_xml_string(entry.to_s)
      ecount += 1
      scount = doc.find('sense').to_a.size
      if scount > 1
        sensecounts[scount] = 0 if sensecounts[scount].nil?
        sensecounts[scount] += 1
      end
      status = get_status(doc)
      progress, first_ed, first_ed_time, last_ed, last_ed_time, complete_time = get_progress(doc,false)
        if status.include?('ME')
          sx='me'
        elsif status.include?('var.') or status.include?(' var')
          sx='var'
        elsif status.include?('XR in R')
          sx='xrr'
        elsif status.include?('ME in R')
          sx='mer'
        else
          sx=status.downcase.strip
        end
        if status.include?('RI') and (sx == 'me' or sx == 'var')
          statcounts[sx+'ri'] += 1
        end
        if statcounts[sx].nil?
          statcounts['other'] += 1
        else 
          statcounts[sx] += 1
        end
      
      progress_a = progress.split(', ')
      progress_a.map! {|x| x.strip.downcase!}
      if progress_a.include?('q')
        progresscounts['q'] += 1
        progresscounts['qri'] += 1 if status.include?('RI')
      end
      if progress_a.include?('r')
        progresscounts['r'] += 1
      elsif progress_a.include?('wip')
        progresscounts['wip'] += 1
        progresscounts['wipri'] += 1 if status.include?('RI')
      elsif progress_a.include?('nys')
        progresscounts['nys'] += 1
      else
        progresscounts['other'] += 1
      end

      unless doc.find('comment').to_a.first.nil?
        if doc.find('comment').to_a.first.content.to_s.include?('INVESTIGATE')
          progresscounts['inv'] += 1
        end
        if doc.find('comment').to_a.first.content.to_s.include?('RESEARCH')
          progresscounts['res'] += 1
        end
      end
    }
    result = {'entrycount'=>ecount, 'moresense'=>0, 'sensecounts'=>[]}
    sensecounts.sort.each{|s_data|
      result['moresense'] += s_data[1]
      result['sensecounts'] << {'senses'=>s_data[0], 'count'=>s_data[1]}
    }
    statcounts.each{|st,c|
      result['status_'+st] = c
    }
    progresscounts.each{|pr,c|
      result['progress_'+pr] = c
    }
    return result
  end

  def change_id(old_eid, new_id)
    $stderr.puts 'RENAME'
    old_id = old_eid.gsub('__', "'")
    new_eid = new_id.gsub("'", '__')
    xml = get(old_id)
    if xml != ''
      $stderr.puts '-delete '+old_id
      delete(old_id)
      $stderr.puts '-save '+new_id
      xml.to_s.gsub!(' entry_id="'+old_eid+'"', ' entry_id="'+new_eid+'"')
      update(new_id, xml)
    end
    return true
  end
end
