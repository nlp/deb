require 'dict/dict-sedna'
require 'mime/types'

class WriteDict < DictSedna
  attr_accessor :schema

  def initialize(collection, journal_path, key_path='', database = 'deb', admindict = nil)
    super
    @servlet = WriteServlet
    dict_info =  get_dict_info(collection)
    @title = dict_info['dict_name']
    @schema = dict_info['schema']
  end

  def get_dict_info(dictcode)
    xml = @admindict.get('dict'+dictcode)
    doc = load_xml_string(xml)
    name = ''
    schema = ''
    code = doc.find('//dict/code').first.content.to_s
    name = doc.find('//dict/name').first.content.to_s unless doc.find('//dict/name').first.nil?
    schema = doc.find('//dict/schema').first.content.to_s unless doc.find('//dict/schema').first.nil?
    dict_info = {'dict_code'=>code,'dict_name'=>name, 'schema'=>schema}
    return dict_info
  end
 
  def get_entries(search='', perm='', templates=[], htemplates=[])
    setting = JSON.parse(@schema)
    entries = []
    if search != '' and setting['key'].to_s != ''
      query = '[entry[starts-with('+setting['key']+', "'+search+'")]]'
    else
      query = '[entry]'
    end
    $stderr.puts query
    xquery_to_hash(query).each{|id,xml|
      doc = load_xml_string(xml)
      head = id
      if setting['key'].to_s != ''
        head = doc.find('//entry/'+setting['key'].to_s).first.content.to_s unless doc.find('//entry/'+setting['key'].to_s).first.nil?
      end
      entry = {'id'=>id, 'head'=>head, 'templates'=>[], 'htemplates'=>[]}
      templates.each{|h|
        entry['templates'] << {'name'=>h['name'], 'code'=>h['code'], 'id'=>id}
      }
      htemplates.each{|h|
        entry['htemplates'] << {'name'=>h['name'], 'code'=>h['code'], 'id'=>id}
      }
      entry['is_manager'] = '1' if perm == 'm'
      entry['is_editor'] = '1' if perm == 'm' or perm == 'w'
      unless doc.root.find('meta/edit').first.nil?
        edit_a = doc.root.find('meta/edit').to_a
        edit = edit_a.sort{|x,y| x['time']<=>y['time']}
        unless edit.first.nil?
          entry['author'] = edit.first['user']
          entry['last_edit'] = edit.last['user'] + '/' + edit.last['time'].to_s[0..9]
        end
      end
      entries << entry
    }
    return entries.sort{|a,b| a['head']<=>b['head']}
  end

  def headword_rec(cont_array)
    cont_array.each{|el|
      return el['element'] if el['headword'] == 'true'
      if el['type'] == 'container'
        res = headword_rec(el['containers'])
        return el['element']+'/'+res unless res.nil?
      end
    }
    return nil
  end

  def get_headword
    setting = JSON.parse(@schema)
    return headword_rec(setting['containers'])
  end

  def find_entries(search='', perm='')
    return [] if perm == ''
    hw = get_headword
    entries = []
    if search != '' and hw.to_s != ''
      query = '[entry[starts-with('+hw+', "'+search+'")]]'
    else
      query = '[entry]'
    end
    $stderr.puts query
    xquery_to_hash(query).each{|id,xml|
      doc = load_xml_string(xml)
      head = id
      if hw.to_s != ''
        head = doc.find('//entry/'+hw.to_s).first.content.to_s unless doc.find('//entry/'+hw.to_s).first.nil?
      end
      entry = {'id'=>id, 'head'=>head}
      if perm == 'm' or perm == 'a'
        unless doc.root.find('meta/edit').first.nil?
          edit_a = doc.root.find('meta/edit').to_a
          edit = edit_a.sort{|x,y| x['time']<=>y['time']}
          unless edit.first.nil?
            entry['author'] = edit.first['user']
            entry['last_edit'] = edit.last['user'] + '/' + edit.last['time'].to_s[0..9]
          end
        end
      end
      entries << entry
    }
    return entries.sort{|a,b| a['head']<=>b['head']}
  end

  def get_perm(user)
    xml = @admindict.get('user'+user)
    doc = REXML::Document.new(xml.to_s)
    perm_el = doc.root.elements["//service[@code='write']/dict[@code='"+@collection+"']"]
    if perm_el != nil
      perm = perm_el.attributes['perm'].to_s
      return perm
    else
      return nil
    end
  end

  def add_meta(xml, user, id)
    doc = load_xml_string(xml)
    oldxml = get(id.to_s)
    if oldxml != ''
      olddoc = load_xml_string(oldxml)
      doc.root << olddoc.root.find('meta').first.copy(true) unless olddoc.root.find('meta').first.nil?
    end
    doc.root << XML::Node.new('meta') if doc.root.find('meta').first.nil?
    newedit = XML::Node.new('edit')
    newedit['user'] = user
    newedit['time'] = Time.now.strftime('%Y-%m-%d %H:%M:%S') 
    doc.root.find('meta').first << newedit
    return doc
  end

  def detect_mime(xml, dictcode, filepath)
    doc = load_xml_string(xml.to_s)
    doc.find('//*[@elem_type="file"]').each{|el|
      file = filepath + '/' + dictcode + '/' + el.content
      mime = MIME::Types.type_for(file)[0]
      el['mime'] = mime.to_s
    }
    return doc.to_s
  end

  def get_all
    setting = JSON.parse(@schema)
    if setting['key'].to_s != ''
      sort = 'order by $x/'+setting['key']
    else
      sort = ''
    end
    query = 'for $x in collection("'+@collection+'")/entry '+sort+' return $x'
    $stderr.puts query
    res = '<'+@collection+'>'
    @sedna.query(query).each{|xml|
      res += xml.to_s.sub('<?xml version="1.0" standalone="yes"?>','')
      res += "\n"
    }
    res += '</'+@collection+'>'
  end

  def get_multi(ids)
    setting = JSON.parse(@schema)
    key = headword_rec(setting['containers'])
    if not key.nil?
      sort = 'order by $x/'+key
    else
      sort = ''
    end
    where = ''
    if ids.split(',').length>0
      where = 'where ('+ids.split(',').map{|x| '$x/@id="'+x.to_s.gsub('"','')+'"'}.join(' or ')+')'
    end
    query = 'for $x in collection("'+@collection+'")/entry '+where+' '+sort+' return $x'
    $stderr.puts query
    res = '<'+@collection+'>'
    @sedna.query(query).each{|xml|
      res += xml.to_s.sub('<?xml version="1.0" standalone="yes"?>','')
      res += "\n"
    }
    res += '</'+@collection+'>'
  end
end
