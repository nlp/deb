require 'rubygems'
require 'net/smtp'
require 'open-uri'
require 'uri'
require 'set'
require 'builder'
require 'json'

class CPASQL
  attr_accessor :xslt_sheets
  attr_accessor :dbh
  attr_accessor :corpora
  attr_accessor :main_freq_corp
  attr_accessor :service_name

  def initialize(service_name=nil, dbc=nil)
    @service_name = service_name
    @xslt_sheets = Hash.new
    if dbc
      @dbh = dbc
    else
      @dbh = db_connect
    end
    @status = {'R' => 'ready', 'W' => 'WIP', 'C' => 'complete',
        '1' => '1st check', '2' => '2nd check', 'O' => 'old',
        'N' => 'NYS', 'A' => 'AUX', 'M' => 'MODAL', 'Q' => 'Q',
        'V'=>'VALID', 'S' => 'Auto-cpa', 'G' => 'G'}
    @serv_lang = {'cpa' => 'en', 'cpait' => 'it', 'cpasp' => 'sp'}
    @language = @serv_lang[@service_name]
  end

  def apply_transform(name, xml_data, params=nil)
    if xml_data != nil and xml_data != ''
      xslt_doc = XML::Document.file(get_transform(name))
      xslt = LibXSLT::XSLT::Stylesheet.new(xslt_doc)
      xml_doc = XML::Document.string(xml_data.to_s)
      if params != nil
        if params.is_a?(Array)
          parameters = {}
          params.each{|a|
            if a[1][0,1] == '"'
              parameters[a[0]] = "'"+a[1][1..-2]+"'" unless a[1][1..-2] == ''
            else
              parameters[a[0]] = "'"+a[1]+"'"
            end
          }
        else
          parameters = params
        end
      end
      s = xslt.apply(xml_doc, parameters)
    end
    return s.to_s
  end

  def get_transform(name)
    if ! @xslt_sheets.has_key?(name)
      raise "no such transformation #{name}"
    end
    sheet = @xslt_sheets[ name ]
    return sheet 
  end

  def semclass_used_verbs(semclass, freqs, filter_out_verbs)
    res = Array.new
    q = "SELECT objects.entry_id, group_concat(DISTINCT "\
      "cast(number as text)) AS numbers FROM objects,patterns WHERE "\
      "objects.pattern_id=patterns.pattern_id AND "\
      "objects.entry_id = patterns.entry_id AND "\
      "TRIM(SUBSTRING(semantic_type, '^[a-zA-Z_]*')) = "\
      "? GROUP BY objects.entry_id"
    res2 = @dbh.execute(q, semclass)
    res2.each{|row|
      next if filter_out_verbs.include? row['entry_id'].to_s
      if freqs
        freq = 0
        number_array = row['numbers'].split(',')
        q2 = "SELECT sample_size from entries where entry_id=?";
        res3 = @dbh.execute(q2,row['entry_id'])
        s_s=""
        res3.each{|row2|
          s_s = row2['sample_size']
        }
        url = 'http://www.fanuk.net/bonito/run.cgi/lngroupinfo?q=q[lempos="'+row['entry_id']+'-v"];corpname=bnc50;annotconc='+row['entry_id']+'-v;format=json;'
        if s_s == '' or s_s == nil or s_s.downcase.include? "all"
          s_s = ""
        else
          s_s='&q=r'+s_s
          url+=s_s
        end
        map = JSON.parse(open(URI.escape(url), :http_basic_authentication=>['guest', 'GG0we5t']).read)
        map['LineGroups'].each{|row3|
          if number_array.include? row3['name'].to_s
            freq += row3['freq']
          end
        }
        res << row['entry_id'].to_s + ';' + row['numbers'].to_s + ';' + freq.to_s ;
      else
        res << row['entry_id'].to_s + ';' + row['numbers'].to_s;
      end
    }
    return res.join("\n") + "\n"
  end

  def get_bare_verbs()
      res = ""
      q = "SELECT entry_id, status FROM entries ORDER BY entry_id"
      q2 = @dbh.execute(q)
      q2.each{|row|
          res += row['entry_id'] + "\t" + row['status'] + "\n"
      }
      return res
  end

    def get_bare_patterns(entry_id)
        res = Array.new
        q = "SELECT * FROM patterns WHERE entry_id=?"
        q2 = @dbh.execute(q, entry_id)
        q2.each{|row|
            res << row['pattern_id'].to_s +
                "\t" + row['number'].to_s +
                "\t" + row['primary_implicature'].to_s +
                "\t" + row['domain'].to_s +
                "\t" + row['register'].to_s +
                "\t" + (row['idiom'] ? 'idiom' : '') +
                (row['phrasal'] ? ' pv' : '') +
                "\t" + row['pattern_string'].to_s +
                "\t" + row['template_comment'].to_s.gsub("\n", "==NEWLINE==").gsub("\"", "==QUOTE==") +
                "\t" + row['corpus'].to_s;
        }
        return res.join("\n") + "\n"
    end

    def get_status(entry_id)
        q = "SELECT status FROM entries WHERE entry_id=?"
        q2 = @dbh.select_one(q, entry_id)
        return q2.to_s
    end

    def pattern_aux_info(v, pid)
        q = "SELECT number, pattern_id, idiom, phrasal FROM patterns "\
            "WHERE entry_id = ? AND pattern_id = ?"
        r = @dbh.select_one(q, v, pid)
        return {
          'number' => r[0].to_i,
          'pattern_id' => r[1].to_i,
          'secondary_implicature' => get_secondary_implicature(v, pid),
          'string' => json_pattern(v, id2num(v, pid)).to_json,
          'idiom' => r[2].to_s,
          'phrasal' => r[3].to_s
        }
    end

    def match_query(column, v)
        q = "SELECT patterns.entry_id, patterns.number, objects.#{column} "\
            "FROM patterns, objects "\
            "WHERE patterns.entry_id = objects.entry_id "\
            "AND patterns.pattern_id = objects.pattern_id "\
            "AND objects.#{column} ILIKE '%#{v}%'"
        return @dbh.execute(q).fetch_all().map{|i| i.to_a}.to_a
    end

    def match_primimpl(p)
        q = "SELECT entry_id, number, primary_implicature FROM patterns "\
            "WHERE primary_implicature ILIKE '%#{p}%'"
        return @dbh.execute(q).fetch_all().map{|i| i.to_a}.to_a
    end

    def match_secimpl(p)
        q = "SELECT patterns.entry_id, patterns.number, implicatures.implicature "\
            "FROM patterns, implicatures "\
            "WHERE patterns.entry_id = implicatures.entry_id "\
            "AND patterns.pattern_id = implicatures.pattern_id "\
            "AND implicatures.implicature ILIKE '%#{p}%'"
        return @dbh.execute(q).fetch_all().map{|i| i.to_a}.to_a
    end

    def match_domain(p)
        q = "SELECT entry_id, number, domain FROM patterns "\
            "WHERE domain ILIKE '%#{p}%'"
        return @dbh.execute(q).fetch_all().map{|i| i.to_a}.to_a
    end

    def match_register(p)
        q = "SELECT entry_id, number, register FROM patterns "\
            "WHERE register ILIKE '%#{p}%'"
        return @dbh.execute(q).fetch_all().map{|i| i.to_a}.to_a
    end

    def match_comment(p)
        q = "SELECT entry_id,number,trim(template_comment || '<br/>' || other_comment) FROM patterns "\
            "WHERE template_comment ILIKE '%#{p}%' or other_comment ILIKE '%#{p}%'"
        return @dbh.execute(q).fetch_all().map{|i| i.to_a}.to_a
    end

    def get_secondary_implicature(entry_id, pattern_id)
        if entry_id == '' or entry_id == nil or pattern_id == '' or pattern_id == nil
            return ''
        end
        q = "SELECT implicature FROM implicatures WHERE entry_id=?"\
            " AND pattern_id=?"
        q2 = @dbh.execute(q, entry_id, pattern_id).fetch_all().
                map{|si| si.to_s.strip.gsub(/&/, '&amp;')}.to_a.join("||")
        return q2.to_s
    end

    def get_pattern_string_from_db(entry_id, pattern_id)
        q = "SELECT pattern_string FROM patterns WHERE entry_id=? AND pattern_id=?"
        p = @dbh.select_one(q, entry_id, pattern_id)[0]
        return p.to_s
    end

    def get_pattern_string(entry_id, pattern_id)
        q = "SELECT * FROM patterns WHERE entry_id=? AND pattern_id=?"
        q2 = @dbh.execute(q, entry_id, pattern_id)
        row = q2.fetch_all()[0]
        if row.nil?
            return ''
        end
        patstring = ''
        patstring += make_sub_conj(entry_id, pattern_id)
        patstring += make_object3(entry_id, pattern_id, 'S')
        if row['verb_form'] != ''
            patstring += ' ' + row['verb_form'] + ' '
        else
            patstring += ' ' + entry_id + ' '
        end
        indobj_string = ''
        opt_iobj = is_optional(entry_id, pattern_id, 'O', 'I')
        obj_ind = make_object3(entry_id, pattern_id, 'O', 'I')
        if obj_ind != ''
            indobj_string += '(' if opt_iobj
            indobj_string += obj_ind
            indobj_string += ')' if opt_iobj
            indobj_string += ' '
        end
        obj_string = ''
        if row['object_none'] == true
            obj_string += '[NO OBJ]'
        elsif not is_empty(entry_id, pattern_id, 'O', 'M')
            opt_obj = is_optional(entry_id, pattern_id, 'O', 'M')
            obj_string += '(' if opt_obj
            obj_string += make_object3(entry_id, pattern_id, 'O')
            obj_string += ')' if opt_obj
        end
        if @language == 'it'
            patstring += obj_string
            patstring += ' a ' if indobj_string != ''
            patstring += indobj_string
        else
            patstring += indobj_string
            patstring += ' ' if indobj_string != ''
            patstring += obj_string
        end
        oclaus_string = make_clausal_object(entry_id, pattern_id, row, 'K')
        patstring += ' ' + oclaus_string + '' if oclaus_string != ''
        patstring += ' ' + make_comp_object(entry_id, pattern_id)
        if row['advl_none'] == true
            patstring += ' [NO ADVL]'
        else
            tmpstring = make_adverbial_object(entry_id, pattern_id,row)
            if not tmpstring.match(/^\s*$/)
                patstring += ' (' if row['advl_opt'] == true
                patstring += ' ' if row['advl_opt'] != true
                patstring += tmpstring
                patstring += ')' if row['advl_opt'] == true
            end
        end
        return patstring.gsub('&', '&amp;')
    end

    #simplified entries list in RDF
    def rdf_verblist()
        res = %Q[<RDF:RDF xmlns:RDF="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
        xmlns:d="http://pdev.org.uk/rdf#"
        xmlns:nc="http://home.netscape.com/NC-rdf#">\n]
        db_query = "SELECT entry_id, to_char(last_edit, 'YYYY-MM-DD') AS modified, "\
          "bnc_freq, bnc50_freq, oec_freq, status "\
          "FROM entries WHERE status='C' ORDER BY entry_id"
        q = @dbh.execute(db_query)
        q.each{|row|
            db_query2 = "SELECT COUNT(*) FROM patterns "\
                    "WHERE entry_id='#{row['entry_id']}'"
            q2 = @dbh.select_one(db_query2)
            patcount = q2[0].to_i
            status = @status[row['status'].to_s]
            res += %Q[<RDF:Description RDF:about="http://pdev.org.uk/proxy.php?action=rdf_verb;query=#{row['entry_id']}">
\t<d:entry_id>#{row['entry_id']}</d:entry_id>
\t<d:last_edit RDF:datatype="Date">#{row['modified']}</d:last_edit>
\t<d:bnc_freq RDF:datatype="Integer">#{row['bnc_freq']}</d:bnc_freq>
\t<d:bnc50_freq RDF:datatype="Integer">#{row['bnc50_freq']}</d:bnc50_freq>
\t<d:oec_freq RDF:datatype="Integer">#{row['oec_freq']}</d:oec_freq>
\t<d:patterns RDF:datatype="Integer">#{patcount}</d:patterns>
\t<d:status>#{status}</d:status>
</RDF:Description>\n]
            }
        res += %q[</RDF:RDF>]
        return res
    end

    #entries list in RDF
    def to_rdf(query, group, perm, filter = '-')
        ids = []
        hw = {}
        res = %Q[<RDF:RDF xmlns:RDF="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
        xmlns:d="http://nlp.fi.muni.cz/cpa/rdf#"
        xmlns:nc="http://home.netscape.com/NC-rdf#">\n]
        res += %Q[<!-- data -->\n]
        where_perm = (perm == 'r')? "WHERE status='C'" : ''
        if filter == 'public'
            db_query = "SELECT entry_id, to_char(create_time, 'YYYY-MM-DD') "\
          "AS create, to_char(last_edit, 'YYYY-MM-DD') AS modified, "\
          "bnc_freq, bnc50_freq, oec_freq, create_by, edit_by, status, "\
          "core FROM entries WHERE status='C' ORDER BY entry_id"
        elsif query.to_s == '*' or group
            db_query = "SELECT entry_id, to_char(create_time, 'YYYY-MM-DD') "\
          "AS create, to_char(last_edit, 'YYYY-MM-DD') AS modified, bnc_freq, "\
          "bnc50_freq, oec_freq, create_by, edit_by, status, core FROM entries"\
          " #{where_perm} ORDER BY entry_id"
        elsif query.length == 0 and filter == '-'
            where_perm = (perm == 'r')? "WHERE status='C'" : "WHERE status='W'"
            db_query = "SELECT entry_id, to_char(create_time, 'YYYY-MM-DD') "\
          "AS create, to_char(last_edit, 'YYYY-MM-DD') AS modified, bnc_freq, "\
          "bnc50_freq, oec_freq, create_by, edit_by, status, core FROM entries"\
          " #{where_perm} ORDER BY last_edit DESC LIMIT 30"
        else
            conds = []
            conds << "status = 'C'" if (perm == 'r')
            conds << "status='#{filter}'" if (filter != '-' and filter != 'all')
            if query.length > 0
                if query[0,1] == '-'
                    conds << "entry_id ILIKE '%#{query[1..-1]}'"
                else
                    conds << "entry_id ILIKE '#{query}%'"
                end
            end
            where = ''
            where = 'WHERE ' + conds.join(' AND ') if (conds.size > 0)
            db_query = "SELECT entry_id, to_char(create_time, 'YYYY-MM-DD') "\
          "AS create, to_char(last_edit, 'YYYY-MM-DD') AS modified, bnc_freq, "\
          "bnc50_freq, oec_freq, create_by, edit_by, status, core FROM entries"\
          " #{where} ORDER BY entry_id"
        end
        if not group
            q = @dbh.execute(db_query)
            q.each{|row|
                db_query2 = "SELECT COUNT(*) FROM patterns "\
                        "WHERE entry_id='#{row['entry_id']}'"
                q2 = @dbh.select_one(db_query2)
                patcount = q2[0].to_i
                db_query3 = "SELECT COUNT(*) FROM patterns WHERE "\
                        "entry_id='#{row['entry_id']}' AND "\
                        "(framenet LIKE 'NOT YET IN%' OR framenet = '' "\
                        "OR framenet IS NULL)"
                q3 = @dbh.select_one(db_query3)
                nofmc = q3[0].to_i
                status = @status[row['status'].to_s]
                patcount = 0 if (row['status'].to_s == 'N'\
                        and nofmc == patcount) or row['status'].to_s == 'M'\
                        or row['status'].to_s == 'Q' or row['status'].to_s=='A'
                coreclass = (row['core'])? 'core' : ''
                res += %Q[<RDF:Description
about="urn:xmlslov:cpa:#{row['entry_id']}">
\t<d:entry_id>#{row['entry_id']}</d:entry_id>
\t<d:entry_label>#{row['entry_id']}</d:entry_label>
\t<d:create>#{row['create']}</d:create>
\t<d:last_edit>#{row['modified']}</d:last_edit>
\t<d:bnc_freq nc:parseType="Integer">#{row['bnc_freq']}</d:bnc_freq>
\t<d:bnc50_freq nc:parseType="Integer">#{row['bnc50_freq']}</d:bnc50_freq>
\t<d:oec_freq nc:parseType="Integer">#{row['oec_freq']}</d:oec_freq>
\t<d:edit_by>#{row['edit_by']}</d:edit_by>
\t<d:create_by>#{row['create_by']}</d:create_by>
\t<d:patterns nc:parseType="Integer">#{patcount}</d:patterns>
\t<d:status>#{status}</d:status>
\t<d:coreclass>#{coreclass}</d:coreclass>
</RDF:Description>\n]
                ids << row['entry_id']
            }
        end
        if group
            groups = {}
            where_perm = (perm == 'r' or filter == 'C')? " AND status='C'" : ''
            db_query = "SELECT DISTINCT TRIM(SUBSTRING(semantic_type, "\
                    "'^[a-zA-Z ]*')) AS type FROM objects,entries WHERE "\
                    "entries.entry_id=objects.entry_id AND object_type IN "\
                    "('O','S','A') AND semantic_type != '' #{where_perm}"
            q = @dbh.execute(db_query)
            q.each{|row|
                type_enc = CGI::escape(row['type'])
                groups[type_enc] = []
                db_query2 = "SELECT  objects.entry_id, group_concat(DISTINCT "\
                        "cast(number as text)) AS numbers FROM objects,patterns WHERE "\
                        "objects.pattern_id=patterns.pattern_id AND "\
                        "objects.entry_id = patterns.entry_id AND "\
                        "TRIM(SUBSTRING(semantic_type, '^[a-zA-Z ]*')) = "\
                        "? GROUP BY objects.entry_id"
                q2 = @dbh.execute(db_query2, row['type'])
                q2.each{|row2|
                    groups[type_enc] << row2['entry_id']+type_enc
                    db_query3 = "SELECT entry_id, to_char(create_time, "\
                            "'YYYY-MM-DD HH24:MI:SS') AS create, to_char(last_edit, 'YYYY-MM-DD HH24:MI:SS') AS modified, bnc_freq, bnc50_freq, oec_freq, create_by, edit_by, status FROM entries WHERE entry_id=? #{where_perm}"
                    q3 = @dbh.execute(db_query3, row2['entry_id'])
                    q3.each{|row3|
                        db_query4 = "SELECT COUNT(*) FROM patterns WHERE entry_id='#{row3['entry_id']}'"
                        q4 = @dbh.select_one(db_query4)
                        patcount = q4[0].to_i
                        status = @status[row3['status'].to_s]
                        coreclass = (row['core'])? 'core' : ''
                        num_ar = row2['numbers'].split(',')
                        numbers = num_ar.sort{|x,y| x.to_i <=> y.to_i}.join(',')
                        res += %Q[<RDF:Description about="urn:xmlslov:cpa:#{row2['entry_id']}#{type_enc}">
\t<d:entry_id>#{row3['entry_id']}</d:entry_id>
\t<d:entry_label>#{row3['entry_id']}: #{numbers}</d:entry_label>
\t<d:create>#{row3['create']}</d:create>
\t<d:last_edit>#{row3['modified']}</d:last_edit>
\t<d:bnc_freq nc:parseType="Integer">#{row3['bnc_freq']}</d:bnc_freq>
\t<d:bnc50_freq nc:parseType="Integer">#{row3['bnc50_freq']}</d:bnc50_freq>
\t<d:oec_freq nc:parseType="Integer">#{row3['oec_freq']}</d:oec_freq>
\t<d:edit_by>#{row3['edit_by']}</d:edit_by>
\t<d:create_by>#{row3['create_by']}</d:create_by>
\t<d:patterns nc:parseType="Integer"></d:patterns>
\t<d:status>#{status}</d:status>
\t<d:coreclass>#{coreclass}</d:coreclass>
</RDF:Description>\n]
                        ids << row['entry_id']
                    }
                }
                db_query_o = "SELECT COUNT(*) FROM ontology WHERE term=?"
                qo = @dbh.select_one(db_query_o, row['type'])
                in_ont = (qo[0].to_i == 0)? "notin" : "in"
                res += %Q[<RDF:Description about="urn:xmlslov:cpa:xgroup#{type_enc}" group="true">
\t<d:in_ontology>#{in_ont}</d:in_ontology>
\t<d:entry_id>* #{row['type']}</d:entry_id>
\t<d:entry_label>* #{row['type']}</d:entry_label>
\t<d:create></d:create>
\t<d:last_edit></d:last_edit>
\t<d:bnc_freq nc:parseType="Integer"></d:bnc_freq>
\t<d:bnc50_freq nc:parseType="Integer"></d:bnc50_freq>
\t<d:oec_freq nc:parseType="Integer"></d:oec_freq>
\t<d:edit_by></d:edit_by>
\t<d:create_by></d:create_by>
\t<d:patterns nc:parseType="Integer">#{groups[type_enc].size.to_s}</d:patterns>
\t<d:status></d:status>
</RDF:Description>\n]
            }
        end
        res += %Q[<!-- tree -->\n]
        res += %Q[<RDF:Seq about="urn:xmlslov:cpa:entries">\n]
        if group
            groups.each{|group, array|
                res += %Q[\t<RDF:li><RDF:Seq about="urn:xmlslov:cpa:xgroup#{group}">\n]
                array.each{|id|
                    res += %Q[\t\t<RDF:li resource="urn:xmlslov:cpa:#{id}"/>\n]
                }
                res += %Q[\t</RDF:Seq></RDF:li>\n]
            }
        else
            ids.each{ |id| res += %Q[\t<RDF:li resource="urn:xmlslov:cpa:#{id}"/>\n] }
        end
        res += %Q[</RDF:Seq>\n]
        res += %q[</RDF:RDF>]
        return res
    end

    def get_public_verbs(query, status)
        res = ""
        if status == 'C'
            where = "status='C' AND"
        elsif status == 'W'
            where = "(status='W' OR status='R') AND"
        elsif status == 'N'
            where = "status='N' AND"
        elsif status == 'L'
            where = "status='S' AND"
        else
            where = ""
        end
        db_query = "SELECT entry_id, bnc_freq, bnc50_freq, oec_freq, status "\
            "FROM entries WHERE #{where} entry_id ILIKE '#{query}%' "\
            "ORDER BY entry_id"
        q = @dbh.execute(db_query)
        q.each{|row|
            next if row['status'] == 'G'
            next if row['status'] == 'Q'
            q2 = "SELECT COUNT(*) FROM patterns WHERE entry_id = '#{row['entry_id']}'"
            qq2 = @dbh.select_one(q2)
            pc = qq2[0].to_i
            status = @status[row['status'].to_s]
            res += row['entry_id'] + ';' +
                pc.to_s + ';' +
                row['bnc_freq'].to_s + ';' +
                row['bnc50_freq'].to_s + ';' +
                row['oec_freq'].to_s+';' +
                status.to_s+"\n"
        }
        return res
    end

    #entries list in CSV
    def to_csv(query, perm, filter = '-', group = false)
        ids = []
        hw = {}
        res = "entry_id;create;modified;bnc_freq;bnc50_freq;oec_freq;edit_by;create_by;patterns;status;sample_size;real_sample_size\n"
        where_perm = (perm == 'r')? "WHERE status='C'" : ''

        if filter == 'public'
            db_query = "SELECT entry_id, to_char(create_time, 'YYYY-MM-DD HH24:MI:SS') AS create, to_char(last_edit, 'YYYY-MM-DD HH24:MI:SS') AS modified, bnc_freq, bnc50_freq, oec_freq, create_by, edit_by, status, core, sample_size FROM entries WHERE status='C' ORDER BY entry_id"
        elsif query.to_s == '*'
            db_query = "SELECT entry_id, to_char(create_time, 'YYYY-MM-DD HH24:MI:SS') AS create, to_char(last_edit, 'YYYY-MM-DD HH24:MI:SS') AS modified, bnc_freq, bnc50_freq, oec_freq, create_by, edit_by, status, core, sample_size FROM entries #{where_perm} ORDER BY entry_id"
        elsif query.length == 0 and filter == '-'
            db_query = "SELECT entry_id, to_char(create_time, 'YYYY-MM-DD HH24:MI:SS') AS create, to_char(last_edit, 'YYYY-MM-DD HH24:MI:SS') AS modified, bnc_freq, bnc50_freq, oec_freq, create_by, edit_by, status, core, sample_size FROM entries #{where_perm} ORDER BY last_edit DESC LIMIT 30"
        else
            conds = []
            conds << "status = 'C'" if (perm == 'r')
            conds << "status='#{filter}'" if (filter != '-' and filter != 'all')
            if query.length > 0
                if query[0,1] == '-'
                    conds << "entry_id ILIKE '%#{query[1..-1]}'"
                else
                    conds << "entry_id ILIKE '#{query}%'"
                end
            end
            where = ''
            where = 'WHERE ' + conds.join(' AND ') if (conds.size > 0)
            db_query = "SELECT entry_id, to_char(create_time, 'YYYY-MM-DD HH24:MI:SS') AS create, to_char(last_edit, 'YYYY-MM-DD HH24:MI:SS') AS modified, bnc_freq, bnc50_freq, oec_freq, create_by, edit_by, status, core, sample_size FROM entries #{where} ORDER BY entry_id"
        end
        if not group
            res = "entry_id;create;modified;bnc_freq;bnc50_freq;oec_freq;edit_by;create_by;patterns;status;sample_size;real_sample_size;framenet\n"
            q = @dbh.execute(db_query)
            q.each{|row|
                db_query2 = "SELECT COUNT(*) FROM patterns WHERE entry_id='#{row['entry_id']}'"
                q2 = @dbh.select_one(db_query2)
                patcount = q2[0].to_i
                #db_query3 = "SELECT COUNT(*) FROM patterns WHERE entry_id='#{row['entry_id']}' AND (framenet LIKE 'NOT YET IN%' OR framenet = '' OR framenet IS NULL)"
                db_query3 = "SELECT COUNT(*) FROM patterns WHERE entry_id='#{row['entry_id']}' AND NOT (framenet LIKE 'NOT YET IN%' OR framenet = '' OR framenet IS NULL)"
                q3 = @dbh.select_one(db_query3)
                db_query4="SELECT COUNT(*) FROM framenet WHERE entry_id='#{row['entry_id']}'"
                q4 = @dbh.select_one(db_query4)
                nofmc = q3[0]+q4[0]
                status = @status[row['status'].to_s]
                #patcount = 0 if (row['status'].to_s == 'N' and nofmc == patcount) or row['status'].to_s == 'M' or row['status'].to_s == 'Q' or row['status'].to_s == 'A'
                patcount = 0 if row['status'].to_s == 'N' or row['status'].to_s == 'M' or row['status'].to_s == 'Q' or row['status'].to_s == 'A'
                coreclass = (row['core'])? 'core' : ''
                samplesize = row['sample_size']
                if samplesize == '' or samplesize == nil or samplesize.downcase.include? 'all' or samplesize.include? '=':
                  samplesize = row['bnc50_freq']
                end
                res += row['entry_id']+';'+row['create'].to_s+';'+row['modified'].to_s+';'+row['bnc_freq'].to_s+';'+row['bnc50_freq'].to_s+';'+row['oec_freq'].to_s+';'+row['edit_by'].to_s+';'+row['create_by'].to_s+';'+patcount.to_s+';'+status.to_s+';'+row['sample_size'].to_s+';'+samplesize.to_s+';'+nofmc.to_s+"\n"
            }
        end
        if group
            groups = {}
            where_perm = (perm == 'r' or filter == 'C')? " AND status='C'" : ''

            db_query = "SELECT DISTINCT TRIM(SUBSTRING(semantic_type, '^[a-zA-Z ]*')) AS type FROM objects,entries WHERE entries.entry_id=objects.entry_id AND object_type IN ('O','S','A') AND semantic_type != '' #{where_perm}"
            q = @dbh.execute(db_query)
            q.each{|row|
                type_enc = CGI::escape(row['type'])
                groups[type_enc] = []
                db_query2 = "SELECT  objects.entry_id, group_concat(DISTINCT cast(number as text)) AS numbers FROM objects,patterns WHERE objects.pattern_id=patterns.pattern_id AND objects.entry_id = patterns.entry_id AND TRIM(SUBSTRING(semantic_type, '^[a-zA-Z ]*')) = ? GROUP BY objects.entry_id"
                q2 = @dbh.execute(db_query2, row['type'])
                q2.each{|row2|
                    groups[type_enc] << row2['entry_id'] + ':' + row2['numbers']
                    db_query3 = "SELECT entry_id, to_char(create_time, 'YYYY-MM-DD HH24:MI:SS') AS create, to_char(last_edit, 'YYYY-MM-DD HH24:MI:SS') AS modified, bnc_freq, bnc50_freq, oec_freq, create_by, edit_by, status FROM entries WHERE entry_id=? #{where_perm}"
                    q3 = @dbh.execute(db_query3, row2['entry_id'])
                    q3.each{|row3|
                        db_query4 = "SELECT COUNT(*) FROM patterns WHERE entry_id='#{row3['entry_id']}'"
                        q4 = @dbh.select_one(db_query4)
                        patcount = q4[0].to_i
                        status = @status[row3['status'].to_s]
                        coreclass = (row['core'])? 'core' : ''
                        num_ar = row2['numbers'].split(',')
                        numbers = num_ar.sort{|x,y| x.to_i <=> y.to_i}.join(',')
                        ids << row['entry_id']
                    }
                }
                db_query_o = "SELECT COUNT(*) FROM ontology WHERE term=?"
                qo = @dbh.select_one(db_query_o, row['type'])
                in_ont = (qo[0].to_i == 0)? "notin" : "in"
            }
            groups.each{|group, array|
                res += "#{group};"
                array.each{|id|
                    res += "#{id};"
                }
                res += "\n";
            }
        end
        return res
    end

    #build RDF tree with differences between ontology and groups
    def difference_tree()
        ids = []
        hw = {}
        res = %Q[<RDF:RDF xmlns:RDF="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
         xmlns:d="http://nlp.fi.muni.cz/cpa/rdf#"
         xmlns:nc="http://home.netscape.com/NC-rdf#">\n]

        res += %Q[<!-- data -->\n]

        db_query = "SELECT DISTINCT patterns.entry_id, number, semantic_type FROM entries, patterns, objects WHERE entries.entry_id=patterns.entry_id AND patterns.entry_id=objects.entry_id AND patterns.pattern_id=objects.pattern_id AND semantic_type IN (SELECT TRIM(SUBSTRING(semantic_type, '^[a-zA-Z ]*')) AS t FROM objects EXCEPT SELECT term FROM ontology ORDER BY t) AND semantic_type != '' AND status!='O' AND object_type IN ('S', 'O', 'A')  ORDER BY semantic_type"
        q = @dbh.execute(db_query)
        q.each{|row|
            eid = (row['entry_id'] + '-' + row['number'].to_s + '-' + row['semantic_type']).gsub(/[^A-Z^0-9^a-z^:^-]/, '_')
            res += %Q[<RDF:Description about="urn:xmlslov:cpa:#{eid}">
\t<d:entry_id>#{row['entry_id']}</d:entry_id>
\t<d:entry_label>#{row['entry_id']}</d:entry_label>
\t<d:number>#{row['number']}</d:number>
\t<d:type>#{row['semantic_type']}</d:type>
</RDF:Description>\n]
            ids << eid
        }

        res += %Q[<!-- tree -->\n]
        res += %Q[<RDF:Seq about="urn:xmlslov:cpa:diff">\n]
        ids.each{ |id| res += %Q[\t<RDF:li resource="urn:xmlslov:cpa:#{id}"/>\n] }
        res += %Q[</RDF:Seq>\n]
        res += %q[</RDF:RDF>]

        return res
    end

    #build RDF ontology tree
    def ontology_tree(search='')
        ids = []
        tree = {}
        res = %Q[<RDF:RDF xmlns:RDF="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
        xmlns:d="http://nlp.fi.muni.cz/cpa/rdf#"
        xmlns:nc="http://home.netscape.com/NC-rdf#">\n]
        res += %Q[<!-- data -->\n]
        db_query = "SELECT * FROM ontology"
        q = @dbh.execute(db_query)
        q.each{|row|
            prop = (search.to_s != '' and row['term'].downcase.include?(search.downcase)) ? 'highlight' : ''
            comment = row['comment']
            if comment.nil?
                comment = ''
            end
            hyp_query = "SELECT ontology.term AS hyper FROM ontology, ontology_links WHERE ontology.tid=ontology_links.hypernym AND ontology_links.term=?"
            hypers = []
            hq = @dbh.execute(hyp_query, row['tid'])
            hq.each{|hyp|
                hypers << hyp['hyper']
            }
            comment = '[' + hypers.join(', ') + '] ' + comment if hypers.size > 1
            res += %Q[<RDF:Description about="urn:xmlslov:cpa:term#{row['tid']}">
\t<d:tid>#{row['tid']}</d:tid>
\t<d:term>#{row['term']}</d:term>
\t<d:comment>#{comment.gsub('"','\"')}</d:comment>
\t<d:prop>#{prop}</d:prop>
</RDF:Description>
]
            tid = row['tid'].to_i
            tree[tid] = []
            tree_query = "SELECT * FROM ontology_links WHERE hypernym=?"
            tq = @dbh.execute(tree_query, row['tid'])
            tq.each{|link|
                tree[tid] << link['term'].to_i
            }
        }
        res += %Q[<RDF:Description about="urn:xmlslov:cpa:term0">
\t<d:tid>0</d:tid>
\t<d:term>Not connected</d:term>
\t<d:comment>Terms not connected to anything</d:comment>
</RDF:Description>
]
        res += %Q[<!-- tree -->\n]
        res += %Q[<RDF:Seq about="urn:xmlslov:cpa:ontology">\n]
        res += ontotree_level(tree, 1)
        res += %Q[\t<RDF:li><RDF:Seq about="urn:xmlslov:cpa:term0">\n]
        tree_query = "SELECT tid, hypernym FROM ontology LEFT JOIN ontology_links ON ontology_links.term=ontology.tid WHERE hypernym IS NULL AND tid != 1"
        tq = @dbh.execute(tree_query)
        tq.each{|r|
            res += %Q[\t\t<RDF:li resource="urn:xmlslov:cpa:term#{r['tid']}"/>\n]
        }
        res += %Q[</RDF:Seq></RDF:li>\n]

        res += %Q[</RDF:Seq>\n]

        res += %q[</RDF:RDF>]

        return res
    end

    def ontotree_level(tree, tid)
        res = ''
        if not tree[tid].nil? and tree[tid].size > 0
            res += %Q[\t<RDF:li><RDF:Seq about="urn:xmlslov:cpa:term#{tid}">\n]
            tree[tid].each{|link|
                res += ontotree_level(tree, link)
            }
            res += %Q[</RDF:Seq></RDF:li>\n]
        else
            res += %Q[\t\t<RDF:li resource="urn:xmlslov:cpa:term#{tid}"/>\n]
        end
        return res
    end

    def semtyp_hyper()
      ontology_map = {} # id => (term, comment)
      query1 = 'SELECT tid, term, comment from ontology'
      result1 = @dbh.execute(query1)
      result1.each{|row|
          ontology_map[row['tid'].to_i] = [row['term'].to_s, row['comment'].to_s]
      }
      query2 = 'SELECT term, hypernym from ontology_links'
      result2 = @dbh.execute(query2)
      result2.each{|row|
        begin
          ontology_map[row['term'].to_i].insert(1, ontology_map[row['hypernym'].to_i][0])
        rescue Exception=>e
        end
      }
      return ontology_map
    end

    def make_sub_conj(entry_id, pattern_id)
        q = "SELECT sub_conj FROM patterns WHERE entry_id=? AND pattern_id=?"
        r = @dbh.select_one(q, entry_id, pattern_id)
        if r[0].to_s.strip != ""
            return "{#{r[0].to_s}} "
        else
            return ""
        end
    end

  #prepare object/subject/adverbial text
  def make_object3(entry_id, pattern_id, type, position=nil)
    string = ''
    if position != nil
      db_query = "SELECT * FROM objects WHERE entry_id='#{entry_id}' AND pattern_id = #{pattern_id} AND object_type='#{type}' AND position='#{position}'"
    else
      db_query = "SELECT * FROM objects WHERE entry_id='#{entry_id}' AND pattern_id = #{pattern_id} AND object_type='#{type}' AND position IN ('M', 'A') ORDER BY position DESC"
    end
    q = @dbh.select_all(db_query)
    complement_optional = false
    if q.length > 0
      semtypes_ar = []
      lexsets_ar = []
      first = ''
      q.each{|row|
        comp_prefix = ''
        if row['complement_as']
          if row['complement_as_optional']
            comp_prefix = '(as) '
          else
            comp_prefix = 'as '
          end
        end
        if row['optional']
          complement_optional = true
        end
        position = row['position']
        string = ''
        if row['semantic_type'].to_s != ''
          string = row['det_quant'].to_s+' ' if row['det_quant'].to_s != ''
          if row['semantic_role'] != '' and row['semantic_role'] != nil
            string += '{' if q.length > 1
            string += row['semantic_type']
            string += ' = ' + row['semantic_role'].gsub(/&/, '&amp;')
            string += '}' if q.length > 1
          else
            string += row['semantic_type']
          end
          first = 's' if position == 'M'
          classes = []
          classes << 'to/INF [V '+string+']' if row['ocl_to'] == true
          classes << '-ING' if row['ocl_ing'] == true
          classes << 'that-CLAUSE' if row['ocl_that'] == true
          classes << 'WH-CLAUSE' if row['ocl_wh'] == true
          classes << 'QUOTE = '+string if row['ocl_quote'] == true and string != ''
          classes << 'QUOTE' if row['ocl_quote'] == true and string == ''
          if (row['ocl_wh'] == true or row['ocl_that'] == true) and string != '' and row['ocl_quote'] != true
            classes << string
          end
          if classes.uniq.size > 1
            string = '{[' + classes.uniq.join(' | ') + ']}'
          elsif classes.uniq.size > 0
            string = '{' + classes.uniq.join(' | ') + '}'
          end
          semtypes_ar << comp_prefix + string
        end
        if row['lexset_items'].to_s != '' and row['semantic_type'].to_s == ''
          string = ''
          string = row['det_quant'].to_s+' ' if row['det_quant'].to_s != ''
          string += normlist(row['lexset_items'], row['lexset_show'] != 'true')
          first = 'l' if position == 'M'
          classes = []
          classes << 'to/INF [V '+string+']' if row['ocl_to'] == true
          classes << '-ING' if row['ocl_ing'] == true
          classes << 'that-CLAUSE' if row['ocl_that'] == true
          classes << 'WH-CLAUSE' if row['ocl_wh'] == true
          classes << 'QUOTE = '+string if row['ocl_quote'] == true and string != ''
          classes << 'QUOTE' if row['ocl_quote'] == true and string == ''
          if (row['ocl_wh'] == true or row['ocl_that'] == true) and string != '' and row['ocl_quote'] != true
            classes << string
          end
          if classes.uniq.size > 1
            string = '{[' + classes.uniq.join(' | ') + ']}'
          elsif classes.uniq.size > 0
            string = '{' + classes.uniq.join(' | ') + '}'
          end
          lexsets_ar << comp_prefix + string
        end
        if row['lexset_items'].to_s == '' and row['semantic_type'].to_s == ''
          string = ''
          string = row['det_quant'].to_s+' ' if row['det_quant'].to_s != ''
          first = 'l' if position == 'M'
          classes = []
          classes << 'to/INF [V '+string+']' if row['ocl_to'] == true
          classes << '-ING' if row['ocl_ing'] == true
          classes << 'that-CLAUSE' if row['ocl_that'] == true
          classes << 'WH-CLAUSE' if row['ocl_wh'] == true
          classes << 'QUOTE = '+string if row['ocl_quote'] == true and string != ''
          classes << 'QUOTE' if row['ocl_quote'] == true and string == ''
          if (row['ocl_wh'] == true or row['ocl_that'] == true) and string != '' and row['ocl_quote'] != true
            classes << string
          end
          if classes.uniq.size > 1
            string = classes.uniq.join(' | ') 
          elsif classes.uniq.size > 0
            string = classes.uniq.join(' | ') 
          end
          lexsets_ar << comp_prefix + string
        end
      }
      if semtypes_ar.length > 0 and lexsets_ar.length > 0
        string = '{[[' + semtypes_ar.join(' | ') + ']] | {' + lexsets_ar.join(' | ') +'}}' if first == 's'
        string = '{{' + lexsets_ar.join(' | ') +'} | [[' + semtypes_ar.join(' | ') + ']]}' if first == 'l'
      elsif semtypes_ar.length > 0
        string = '[[' + semtypes_ar.join(' | ') + ']]'
      else
        string = '{' + lexsets_ar.join(' | ') +'}'
      end
    end
    if type == 'C' and complement_optional
      string = '(' + string + ')'
    end
    return string
  end

    def make_object(entry_id, pattern_id, type, position)
        string = ''
        db_query = "SELECT * FROM objects WHERE entry_id='#{entry_id}' AND pattern_id = #{pattern_id} AND object_type='#{type}' AND position='#{position}'"
        q = @dbh.select_all(db_query)

        if q.length > 0
            q.each{|row|
                if position == 'M'
                    string += ''
                else
                    string += ' | '
                end

                if row['semantic_type'] != ''
                    if row['semantic_role'] != '' and row['semantic_role'] != nil
                        string += '{' if position == 'A'
                        string += row['semantic_type']
                        string += ' = ' + row['semantic_role'].gsub(/&/, '&amp;')
                        string += '}' if position == 'A'
                    else
                        string += row['semantic_type']
                    end
                else
                    lex_ar = row['lexset_items'].split(',')
                    string += normlist(lex_ar, row['lexset_show'] != 'true')
                end

            }
        end
        return string
    end

  #prepare adverbial text
  def make_adverbial_object(entry_id, pattern_id,spec_row)
    strings = []
    db_query = "SELECT DISTINCT advl_group, advl_function, advl_main_opt "\
        "FROM objects WHERE entry_id='#{entry_id}' AND "\
        "pattern_id = #{pattern_id} AND object_type='A' ORDER BY advl_group ASC"
    q = @dbh.select_all(db_query)
    if q.length > 0
      q.each{|row_m|
        brackets = false
        string = ''
        string += '(' if row_m['advl_main_opt'] == true
        string += '[Adv['+row_m['advl_function']+']]' if row_m['advl_function'].to_s != ''
        db_query2 = "SELECT * FROM objects WHERE entry_id='#{entry_id}' AND "\
            "pattern_id = #{pattern_id} AND object_type='A' AND "\
            "advl_group=#{row_m['advl_group']} AND position<>'X' "\
            "ORDER BY position DESC"
        q2 = @dbh.select_all(db_query2)
        q2.each{|row|
          if row['position'] == 'M' and row_m['advl_function'].to_s != ''
            string += ' '
          end
          if row['position'] == 'A' or row_m['advl_function'].to_s != ''
            string += ' | '
          end
          string += '(' if row['optional'] == true
          if(row['headword'] != nil and row['headword'] != '')
            string += row['headword'] + ' '
            brackets = true
          end
          string += '(' if row['adv_comp_opt'] == true
          if row['semantic_type'].to_s != ''
            brackets = true
            string += ' ' if row['headword'] != nil and row['headword'] != ''
            string += '[[' + row['semantic_type'].to_s
            if row['semantic_role'] != '' and row['semantic_role'] != nil
              string += ' = ' + row['semantic_role'].gsub(/&/, '&amp;')
            end
            string += ']]'
          elsif row['advl_adverbs'].to_s != ''
            string += '{'
            string += normlist(row['advl_adverbs'], row['lexset_show'] != 'true')
            string += '}'
          elsif row['lexset_items'].to_s != ''
            string += '{'
            string += normlist(row['lexset_items'], row['lexset_show'] != 'true')
            string += '}'
          end
          string.strip!
          string += ')' if row['adv_comp_opt'] == true
          string += ')' if row['optional'] == true
        }
        string += ')' if row_m['advl_main_opt'] == true
        string = '{' + string + '}' if brackets and not row_m['advl_main_opt']
        $stderr.puts "###" + string.to_s + "###"
        strings << string
      }
    end
    claus_string = make_clausal_object(entry_id, pattern_id, spec_row, 'L')
    strings << claus_string if claus_string != ''
    return strings.join(' ')
  end

    def make_comp_object(entry_id, pattern_id)
        if @language == 'en':
          mystr = make_object3(entry_id,pattern_id,'C')
          return mystr
        end
        string = ''
        string_comp = ''
        db_query = "SELECT * FROM objects WHERE entry_id='#{entry_id}' "\
            "AND pattern_id = #{pattern_id} AND object_type='C' "\
            "ORDER BY position DESC"
        q = @dbh.select_all(db_query)

        if q.length > 0
            q.each{|row|
                if row['complement_as_optional']
                    string_comp += "("
                end
                if row['complement_as']
                    string_comp += 'as'
                end
                if row['complement_as_optional']
                    string_comp += ") "
                elsif row['complement_as']
                    string_comp += ' '
                end
                if row['position'] == 'A'
                    string += ' | '
                end
                if @language == 'it'
                    string += '[['
                else
                    string += '{'
                end
                string += string_comp + row['complement_det'].to_s + ' '
                if row['semantic_type'].to_s != ''
                    string += row['semantic_type'].to_s
                    if row['semantic_role'].to_s != ''
                        string += " = " + row['semantic_role'].to_s
                    end
                elsif row['lexset_items'].to_s != ''
                    string += '{'
                    string += normlist(row['lexset_items'], row['lexset_show'] != 'true')
                    string += '}'
                end
                if @language == 'it'
                    string += ']]'
                else
                    string += '}'
                end
            }
        end
        return string
    end

    def make_clausal_object(entry_id, pattern_id, patrow, type)
        string = ''
        string += '(' if ((patrow['claus_opt'] == true && type == 'L') || (patrow['ocl_opt'] == true && type == 'K'))
        db_query = "SELECT * FROM objects WHERE entry_id='#{entry_id}' AND pattern_id = #{pattern_id} AND object_type='#{type}' ORDER BY position DESC"
        q = @dbh.select_all(db_query)
        cl_string = ''

        if q.length > 0
            cl_rows = []
            q.each{|row|
                if row['semantic_type'] != ''
                    cl_row = row['semantic_type']
                    if row['semantic_role'] != '' and row['semantic_role'] != nil
                        cl_row = '{' + cl_row + ' = ' + row['semantic_role'].gsub(/&/, '&amp;')+'}'
                    end
                else
                    lex_ar = row['lexset_items'].split(',')
                    cl_row = '' + lex_ar[0].to_s + '' if lex_ar != nil and lex_ar[0].to_s != ''
                end
                cl_rows << cl_row
            }
            cl_string = '[' + cl_rows.join(' | ') + ']'
        end

        classes = []
        if type == 'L'
            classes << 'to/INF [V'+cl_string+']' if patrow['claus_to'] == true
            classes << '-ING' if patrow['claus_ing'] == true
            classes << 'that-CLAUSE' if patrow['claus_that'] == true
            classes << 'WH-CLAUSE' if patrow['claus_wh'] == true
            classes << 'QUOTE = '+cl_string if patrow['claus_quote'] == true and cl_string != ''
            classes << 'QUOTE' if patrow['claus_quote'] == true and cl_string == ''
            if (patrow['claus_wh'] == true or patrow['claus_that'] == true) and cl_string != '' and patrow['claus_quote'] != true
                classes << cl_string
            end
        end
        if type == 'K'
            classes << 'to/INF [V'+cl_string+']' if patrow['ocl_to'] == true
            classes << '-ING' if patrow['ocl_ing'] == true
            classes << 'that-CLAUSE' if patrow['ocl_that'] == true
            classes << 'WH-CLAUSE' if patrow['ocl_wh'] == true
            classes << 'QUOTE = '+cl_string if patrow['ocl_quote'] == true and cl_string != ''
            classes << 'QUOTE' if patrow['ocl_quote'] == true and cl_string == ''
            if (patrow['ocl_wh'] == true or patrow['ocl_that'] == true) and cl_string != '' and patrow['ocl_quote'] != true
                classes << cl_string
            end
        end
        if classes.uniq.size > 1
            string += '{[' + classes.uniq.join(' | ') + ']}'
        elsif classes.uniq.size > 0
            string += '{' + classes.uniq.join(' | ') + '}'
        end

        string += ')' if ((patrow['claus_opt'] == true && type == 'L') || (patrow['ocl_opt'] == true && type == 'K'))
        return string
    end

    def is_none(entry_id, pattern_id, type, position)
        string = ''
        db_query = "SELECT * FROM objects WHERE entry_id='#{entry_id}' AND pattern_id = #{pattern_id} AND object_type='#{type}' AND position='#{position}'"
        q = @dbh.select_all(db_query)

        if q.length > 0
            q.each{|row|
                if row['none'] == true
                    return true
                end
            }
        end
        return false
    end

    def is_empty(entry_id, pattern_id, type, position)
        string = ''
        db_query = "SELECT * FROM objects WHERE entry_id='#{entry_id}' AND pattern_id = #{pattern_id} AND object_type='#{type}' AND position='#{position}'"
        q = @dbh.select_all(db_query)

        if q.length > 0
            q.each{|row|
                if row['semantic_type'] == '' and row['lexset_items'] == '' and row['ocl_that'] == '' and row['ocl_to'] == '' and row['ocl_ing'] == '' and row['ocl_wh'] == '' and row['ocl_quote'] == ''
                    return true
                end
            }
        else
            return true
        end
        return false
    end

    def is_lexset(entry_id, pattern_id, type, position)
        string = ''
        db_query = "SELECT * FROM objects WHERE entry_id='#{entry_id}' AND pattern_id = #{pattern_id} AND object_type='#{type}' AND position='#{position}'"
        q = @dbh.select_all(db_query)

        if q.length > 0
            q.each{|row|
                if row['semantic_type'] == '' and row['lexset_items'] != ''
                    return true
                end
            }
        end
        return false
    end

    def is_optional(entry_id, pattern_id, type, position)
        string = ''
        db_query = "SELECT * FROM objects WHERE entry_id='#{entry_id}' AND pattern_id = #{pattern_id} AND object_type='#{type}' AND position='#{position}'"
        q = @dbh.select_all(db_query)
        if q.length > 0
            q.each{|row|
                if row['optional'] == true
                    return true
                end
            }
        end
        return false
    end

    #patterns list for entry, in RDF
    def rdf_patterns(entry_id, newlines=true)
        pats = []
        res = %Q[<RDF:RDF xmlns:RDF="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
         xmlns:d="http://nlp.fi.muni.cz/cpa/rdf#"
         xmlns:nc="http://home.netscape.com/NC-rdf#">\n]

        res += %Q[<!-- data -->\n]

        db_query = "SELECT * FROM patterns WHERE entry_id='#{entry_id}'"
        q = @dbh.execute(db_query)
        q.each{|row|
            patid = row['pattern_id'].to_s
            patstring = row['pattern_string']
            if patstring.nil?
                patstring = ''
            end
            secimpl = get_secondary_implicature(entry_id, patid)
            pvidiom = ''
            pvidiom += 'idiom' if row['idiom']
            pvidiom += ' pv' if row['phrasal']
            comment = row['template_comment'].to_s
            if comment != nil
              comment=URI.escape(row['template_comment'].to_s)
            end
            res += %Q[<RDF:Description about="urn:xmlslov:cpa:pattern#{row['number']}">
\t<d:prop>main</d:prop>
\t<d:patid>#{patid}</d:patid>
\t<d:num nc:parseType="Integer">#{row['number']}</d:num>
\t<d:string>#{patstring.gsub('&','&amp;')}</d:string>
\t<d:primary>#{row['primary_implicature'].to_s.gsub(/&/,'&amp;')}</d:primary>
\t<d:chid>ch_#{entry_id}_#{patid}</d:chid>
\t<d:secimp>#{secimpl}</d:secimp>
\t<d:pvidiom>#{pvidiom}</d:pvidiom>
\t<d:domain>#{row['domain']}</d:domain>
\t<d:register>#{row['register']}</d:register>
\t<d:comment>#{comment}</d:comment>
\t<d:corpus>#{row['corpus']}</d:corpus>
</RDF:Description>]
            pats << row['number']
        }
        res += %Q[<!-- tree -->\n]
        res += %Q[<RDF:Seq about="urn:xmlslov:cpa:patterns">\n]
        pats.sort.each { |n| res += %Q[\t<RDF:li resource="urn:xmlslov:cpa:pattern#{n}"/>\n] }
        res += %Q[</RDF:Seq>\n]

        res += %q[</RDF:RDF>]
    end

    #prepare object/subject/adverbial text
    def make_xml_object(xml, entry_id, pattern_id, type, position)
        xo = Builder::XmlMarkup.new(:target => xml)
        db_query = "SELECT * FROM objects WHERE entry_id='#{entry_id}' AND pattern_id = #{pattern_id} AND object_type='#{type}' AND position='#{position}'"
        q = @dbh.select_all(db_query)

        if q.length > 0
            q.each{|row|
                type = (row['optional'] == true)? "optional":""
                none = (row['none'] == true)? "true":""
                ocl_quote = (row['ocl_quote'] == true)? "true":""
                ocl_to = (row['ocl_to'] == true)? "true":""
                ocl_ing = (row['ocl_ing'] == true)? "true":""
                ocl_that = (row['ocl_that'] == true)? "true":""
                ocl_wh = (row['ocl_wh'] == true)? "true":""

                xo.argspec("type"=>type, "none"=>none,
                        "det_quant"=>row['det_quant'], "headword"=>row['det_quant'],
                        "ocl_to"=>ocl_to, "ocl_ing"=>ocl_ing, "ocl_quote"=>ocl_quote,
                        "ocl_that"=>ocl_that, "ocl_wh"=>ocl_wh) {
                    xo.BSO_type("name"=>row['semantic_type'])

                    xo.subspec {
                        if (row['lexset_items'] != nil and row['lexset_items'] != '')
                            xo.Lexset("name"=>'') {
                                xo.item(row['lexset_items'])
                                xo.show(row['lexset_show'])
                            }
                        end
                        if row['semantic_role'] != '' and row['semantic_role'] != nil
                            xo.Role("name"=>row['semantic_role'])
                        end
                        if row['polarity'] != '' and row['polarity'] != nil
                            xo.Polarity("name"=>row['polarity'])
                        end
                        if row['attributes'].to_s != ''
                            xo.attributes(row['attributes'])
                        end
                    }
                }
            }
        end
    end

    def make_json_object(entry_id, pattern_id, type, position)
        obj = {}
        db_query = "SELECT * FROM objects WHERE entry_id='#{entry_id}' AND pattern_id = #{pattern_id} AND object_type='#{type}' AND position='#{position}'"
        q = @dbh.select_all(db_query)
        q.each{|row|
            obj['argspec'] = {}
            a = obj['argspec']
            a['@type'] = (row['optional'] == true)? "optional":""
            a['@none'] = (row['none'] == true)? "true":""
            a['@det_quant'] = row['det_quant'].to_s
            a['@headword'] = row['det_quant'].to_s
            a['@ocl_to'] = (row['ocl_to'] == true)? "true":""
            a['@ocl_ing'] = (row['ocl_ing'] == true)? "true":""
            a['@ocl_quote'] = (row['ocl_quote'] == true)? "true":""
            a['@ocl_that'] = (row['ocl_that'] == true)? "true":""
            a['@ocl_wh'] = (row['ocl_wh'] == true)? "true":""
            a['BSO_type'] = {}
            a['BSO_type']['@name'] = row['semantic_type'].to_s
            a['subspec'] = {}
            s = a['subspec']
            if (row['lexset_items'] != nil and row['lexset_items'] != '')
                s['Lexset'] = {}
                s['Lexset']['@name'] = ''
                s['Lexset']['item'] = {}
                s['Lexset']['item']['$'] = row['lexset_items'].to_s
                s['Lexset']['show'] = {}
                if row['lexset_show'].to_s != ''
                    s['Lexset']['show']['$'] = row['lexset_show'].to_s
                end
            end
            if row['semantic_role'] != '' and row['semantic_role'] != nil
                s['Role'] = {}
                s['Role']['@name'] = row['semantic_role']
            end
            if row['polarity'] != '' and row['polarity'] != nil
                s['Polarity'] = {}
                s['Polarity']['@name'] = row['polarity'].to_s
            end
            if row['attributes'].to_s != ''
                s['attributes'] = {}
                s['attributes']['$'] = row['attributes'].to_s
            end
        }
        return obj
    end

    #prepare alternate object/subject
    def make_xml_alt_object(xml, entry_id, pattern_id, type, position)
        xo = Builder::XmlMarkup.new(:target => xml)
        db_query = "SELECT * FROM objects WHERE entry_id='#{entry_id}' AND pattern_id = #{pattern_id} AND object_type='#{type}' AND position='#{position}'"
        q = @dbh.select_all(db_query)
        if q.length > 0
            q.each{|row|
                xo.alternation {
                    type = (row['optional'] == true)? "optional":""
                    ocl_quote = (row['ocl_quote'] == true)? "true":""
                    ocl_to = (row['ocl_to'] == true)? "true":""
                    ocl_ing = (row['ocl_ing'] == true)? "true":""
                    ocl_that = (row['ocl_that'] == true)? "true":""
                    ocl_wh = (row['ocl_wh'] == true)? "true":""

                    xo.argspec("type"=>type, "det_quant"=>row['det_quant'],
                            "headword"=>row['det_quant'], "ocl_to"=>ocl_to,
                            "ocl_ing"=>ocl_ing, "ocl_quote"=>ocl_quote,
                            "ocl_that"=>ocl_that, "ocl_wh"=>ocl_wh) {
                        xo.BSO_type("name"=>row['semantic_type'])

                        xo.subspec {
                            if row['lexset_items'] != ''
                                xo.Lexset("name"=>'') {
                                    xo.item(row['lexset_items'])
                                    xo.show(row['lexset_show'])
                                }
                            end
                            if row['polarity'] != '' and row['polarity'] != nil and row['semantic_role'] != '' and row['semantic_role'] != nil
                                xo.and {
                                    xo.Role("name"=>row['semantic_role'])
                                    xo.Polarity("name"=>row['polarity'])
                                }
                            end
                            if row['semantic_role'] != '' and row['semantic_role'] != nil
                                xo.Role("name"=>row['semantic_role'])
                            end
                            if row['polarity'] != '' and row['polarity'] != nil
                                xo.Polarity("name"=>row['polarity'])
                            end
                            if row['attributes'].to_s != ''
                                xo.attributes(row['attributes'])
                            end
                        }
                    }
                }
            }
        end
    end

    def make_json_alt_object(entry_id, pattern_id, type, position)
        l = []
        db_query = "SELECT * FROM objects WHERE entry_id='#{entry_id}' AND pattern_id = #{pattern_id} AND object_type='#{type}' AND position='#{position}'"
        q = @dbh.select_all(db_query)
        if q.length < 1
            return {}
        end
        q.each{|row|
            obj = {}
            obj['argspec'] = {}
            a = obj['argspec']
            a['@type'] = (row['optional'] == true)? "optional":""
            a['@det_quant'] = row['det_quant'].to_s
            a['@headword'] = row['det_quant'].to_s
            a['@ocl_to'] = (row['ocl_to'] == true)? "true":""
            a['@ocl_ing'] = (row['ocl_ing'] == true)? "true":""
            a['@ocl_quote'] = (row['ocl_quote'] == true)? "true":""
            a['@ocl_that'] = (row['ocl_that'] == true)? "true":""
            a['@ocl_wh'] = (row['ocl_wh'] == true)? "true":""
            a['BSO_type'] = {}
            a['BSO_type']['@name'] = row['semantic_type'].to_s
            a['subspec'] = {}
            s = a['subspec']
            if (row['lexset_items'] != nil and row['lexset_items'] != '')
                s['Lexset'] = {}
                s['Lexset']['@name'] = ''
                s['Lexset']['item'] = {}
                if row['lexset_items'].to_s != ''
                    s['Lexset']['item']['$'] = row['lexset_items'].to_s
                end
                s['Lexset']['show'] = {}
                if row['lexset_show'].to_s != ''
                    s['Lexset']['show']['$'] = row['lexset_show'].to_s
                end
            end
            if row['semantic_role'] != '' and row['semantic_role'] != nil
                s['Role'] = {}
                s['Role']['@name'] = row['semantic_role']
            end
            if row['polarity'] != '' and row['polarity'] != nil
                s['Polarity'] = {}
                s['Polarity']['@name'] = row['polarity'].to_s
            end
            if row['attributes'].to_s != ''
                s['attributes'] = {}
                s['attributes']['$'] = row['attributes'].to_s
            end
            if q.length == 1
                return obj
            else
                l << obj
            end
        }
        return l
    end

    #indirect object
    def make_xml_ind_object(xml, entry_id, pattern_id)
        xo = Builder::XmlMarkup.new(:target => xml)
        db_query = "SELECT * FROM objects WHERE entry_id='#{entry_id}' AND pattern_id = #{pattern_id} AND object_type='O' AND position='I'"
        q = @dbh.select_all(db_query)

        if q.length > 0
            q.each{|row|
                xo.indirect {
                    type = (row['optional'] == true)? "optional":""
                    xo.argspec("headword"=>row['det_quant'], 'type'=>type) {
                        xo.BSO_type("name"=>row['semantic_type'])

                        xo.subspec {
                            if row['lexset_items'] != ''
                                xo.Lexset("name"=>'') {
                                    xo.item(row['lexset_items'])
                                    xo.show(row['lexset_show'])
                                }
                            end
                            if row['semantic_role'] != '' and row['semantic_role'] != nil
                                xo.Role("name"=>row['semantic_role'])
                            end
                            if row['polarity'] != '' and row['polarity'] != nil
                                xo.Polarity("name"=>row['polarity'])
                            end
                            if row['attributes'].to_s != ''
                                xo.attributes(row['attributes'])
                            end
                        }
                    }
                }
            }
        end
    end

    def make_json_ind_object(entry_id, pattern_id)
        l = []
        db_query = "SELECT * FROM objects WHERE entry_id='#{entry_id}' AND pattern_id = #{pattern_id} AND object_type='O' AND position='I'"
        q = @dbh.select_all(db_query)
        q.each{|row|
            o = {}
            o['argspec'] = {}
            a = o['argspec']
            a['@type'] = (row['optional'] == true)? "optional":""
            a['@headword'] = row['det_quant'].to_s
            a['BSO_type'] = {}
            a['BSO_type']['@name'] = row['semantic_type'].to_s
            a['subspec'] = {}
            s = a['subspec']
            if (row['lexset_items'] != nil and row['lexset_items'] != '')
                s['Lexset'] = {}
                s['Lexset']['@name'] = ''
                s['Lexset']['item'] = {}
                s['Lexset']['item']['$'] = row['lexset_items'].to_s
                s['Lexset']['show'] = {}
                s['Lexset']['show']['$'] = row['lexset_show'].to_s
            end
            if row['semantic_role'] != '' and row['semantic_role'] != nil
                s['Role'] = {}
                s['Role']['@name'] = row['semantic_role']
            end
            if row['polarity'] != '' and row['polarity'] != nil
                s['Polarity'] = {}
                s['Polarity']['@name'] = row['polarity'].to_s
            end
            if row['attributes'].to_s != ''
                s['attributes'] = {}
                s['attributes']['$'] = row['attributes'].to_s
            end
            if q.to_a.length > 1
                l << o
            else
                return o
            end
        }
        return l
    end

    def make_xml_adverbial(xml, entry_id, pattern_id)
        xo = Builder::XmlMarkup.new(:target => xml)
        db_query = "SELECT DISTINCT advl_group, advl_function, advl_main_opt FROM objects WHERE entry_id='#{entry_id}' AND pattern_id = #{pattern_id} AND object_type='A' ORDER BY advl_group ASC"
        q = @dbh.select_all(db_query)

        if q.length > 0
            q.each{|row_m|
                main_opt = (row_m['advl_main_opt'] == true)? "true":"false"
                xo.adverbial("func"=>row_m['advl_function'], "optional"=>main_opt) {
                    db_query2 = "SELECT * FROM objects WHERE entry_id='#{entry_id}' AND pattern_id = #{pattern_id} AND object_type='A' AND advl_group=#{row_m['advl_group']} AND position<>'X' ORDER BY position DESC"
                    q2 = @dbh.select_all(db_query2)
                    q2.each{|row|
                        type = (row['optional'] == true)? "optional":""
                        type2 = (row['adv_comp_opt'] == true)? "true":"false"
                        xo.alternation("type"=>type, "adv_comp_opt"=>type2) {
                            xo.ptag("value"=>"PP", "headword"=>row['headword']) {
                                xo.argspec {
                                    xo.adverbs(row['advl_adverbs'].to_s)
                                    xo.BSO_type("name"=>row['semantic_type'])
                                    xo.subspec {
                                        if row['lexset_items'] != ''
                                            xo.Lexset("name"=>'') {
                                            xo.item(row['lexset_items'])
                                            xo.show(row['lexset_show'])
                                            }
                                        end
                                        if row['semantic_role'] != '' and row['semantic_role'] != nil
                                            xo.Role("name"=>row['semantic_role'])
                                        end
                                        if row['polarity'] != '' and row['polarity'] != nil
                                            xo.Polarity("name"=>row['polarity'])
                                        end
                                        if row['attributes'].to_s != ''
                                            xo.attributes(row['attributes'])
                                        end
                                    }
                                }
                            }
                        }
                    }
                }
            }
        end
    end

    def make_json_adverbial(entry_id, pattern_id)
        l = []
        db_query = "SELECT DISTINCT advl_group, advl_function, advl_main_opt FROM objects WHERE entry_id='#{entry_id}' AND pattern_id = #{pattern_id} AND object_type='A' ORDER BY advl_group ASC"
        q = @dbh.select_all(db_query)
        q.each{|row_m|
            a = {}
            a['@func'] = row_m['advl_function']
            a['@optional'] = (row_m['advl_main_opt'] == true)? "true":"false"
            a['alternation'] = []
            db_query2 = "SELECT * FROM objects WHERE entry_id='#{entry_id}' AND pattern_id = #{pattern_id} AND object_type='A' AND advl_group=#{row_m['advl_group']} AND position<>'X' ORDER BY position DESC"
            q2 = @dbh.select_all(db_query2)
            q2.each{|row|
                alt_obj = {}
                alt_obj['@type'] = (row['optional'] == true)? "optional":""
                alt_obj['@adv_comp_opt'] = (row['adv_comp_opt'] == true)? "true":"false"
                alt_obj['ptag'] = {}
                ptag = alt_obj['ptag']
                ptag['@value'] = 'PP'
                ptag['@headword'] = row['headword'].to_s
                ptag['argspec'] = {}
                arg = ptag['argspec']
                arg['adverbs'] = {}
                if row['advl_adverbs'].to_s != ''
                    arg['adverbs']['$'] = row['advl_adverbs'].to_s
                end
                arg['BSO_type'] = {}
                arg['BSO_type']['@name'] = row['semantic_type'].to_s
                arg['subspec'] = {}
                s = arg['subspec']
                if row['lexset_items'] != ''
                    s['Lexset'] = {}
                    s['Lexset']['@name'] = ''
                    s['Lexset']['item'] = {}
                    if row['lexset_items'].to_s != ''
                        s['Lexset']['item']['$'] = row['lexset_items'].to_s
                    end
                    s['Lexset']['show'] = {}
                    if row['lexset_show'].to_s != ''
                        s['Lexset']['show']['$'] = row['lexset_show'].to_s
                    end
                end
                if row['semantic_role'] != '' and row['semantic_role'] != nil
                    s['Role'] = {}
                    s['Role']['@name'] = row['semantic_role'].to_s
                end
                if row['polarity'] != '' and row['polarity'] != nil
                    s['Polarity'] = {}
                    s['Polarity']['@name'] = row['polarity'].to_s
                end
                if row['attributes'].to_s != ''
                    s['attributes'] = {}
                    s['attributes']['$'] = row['attributes'].to_s
                end
                if q2.to_a.length > 1
                    a['alternation'] << alt_obj
                else
                    a['alternation'] = alt_obj
                    break
                end
            }
            if q2.to_a.length < 1
                a.delete('alternation')
            end
            if q.to_a.length > 1
                l << a
            else
                return a
            end
        }
        return l
    end

    def make_xml_complement(xml, entry_id, pattern_id)
        xo = Builder::XmlMarkup.new(:target => xml)
        db_query = "SELECT * FROM objects WHERE entry_id='#{entry_id}' AND pattern_id = #{pattern_id} AND object_type='C' ORDER BY position DESC"
        q = @dbh.select_all(db_query)

        if q.length > 0
            q.each{|row|
                xo.comp(row['semantic_type'],
                        "role"=>row['semantic_role'],
                        "lexset"=>row['lexset_items'],
                        "lexset_show"=>row['lexset_show'],
                        "type"=>row['complement_type'],
                        "optional"=>row['optional'],
                        "complement_as"=>row['complement_as'],
                        "complement_as_optional"=>row['complement_as_optional'],
                        "complement_det"=>row['complement_det'])
            }
        end
    end

    def make_json_complement(entry_id, pattern_id)
        l = []
        db_query = "SELECT * FROM objects WHERE entry_id='#{entry_id}' AND pattern_id = #{pattern_id} AND object_type='C' ORDER BY position DESC"
        q = @dbh.select_all(db_query)
        q.each{|row|
            c = {}
            if row['semantic_type'].to_s != ''
                c['$'] = row['semantic_type'].to_s
            end
            c['@role'] = row['semantic_role'].to_s
            c['@lexset'] = row['lexset_items'].to_s
            c['@lexset_show'] = row['lexset_show'].to_s
            c['@type'] = row['complement_type'].to_s
            c['@optional'] = row['optional'].to_s
            c['@complement_as'] = row['complement_as'].to_s
            c['@complement_as_optional'] = row['complement_as_optional'].to_s
            c['@complement_det'] = row['complement_det'].to_s
            if q.length > 1
                l << c
            else
                return c
            end
        }
        return l
    end

    def make_xml_clausal(xml, entry_id, pattern_id, type)
        xo = Builder::XmlMarkup.new(:target => xml)
        db_query = "SELECT * FROM objects WHERE entry_id='#{entry_id}' AND pattern_id = #{pattern_id} AND object_type='#{type}' ORDER BY position DESC"
        q = @dbh.select_all(db_query)

        if q.length > 0
            q.each{|row|
                xo.clausal {
                    type = (row['optional'] == true)? "optional":""
                    xo.argspec("headword"=>row['det_quant'], "type"=>type) {
                        xo.BSO_type("name"=>row['semantic_type'])

                        xo.subspec {
                            if row['lexset_items'] != ''
                                xo.Lexset("name"=>'') {
                                    xo.item(row['lexset_items'])
                                    xo.show(row['lexset_show'])
                                }
                            end
                            if row['semantic_role'] != '' and row['semantic_role'] != nil
                                xo.Role("name"=>row['semantic_role'])
                            end
                            if row['polarity'] != '' and row['polarity'] != nil
                                xo.Polarity("name"=>row['polarity'])
                            end
                            if row['attributes'].to_s != ''
                                xo.attributes(row['attributes'])
                            end
                        }
                    }
                }
            }
        end
    end

    def make_json_clausal(entry_id, pattern_id, type)
        l = []
        db_query = "SELECT * FROM objects WHERE entry_id='#{entry_id}' AND pattern_id = #{pattern_id} AND object_type='#{type}' ORDER BY position DESC"
        q = @dbh.select_all(db_query)
        q.each{|row|
            o = {}
            o['argspec'] = {}
            a = o['argspec']
            a['@type'] = (row['optional'] == true)? "optional":""
            a['@headword'] = row['det_quant'].to_s
            a['BSO_type'] = {}
            a['BSO_type']['@name'] = row['semantic_type'].to_s
            a['subspec'] = {}
            s = a['subspec']
            if (row['lexset_items'] != nil and row['lexset_items'] != '')
                s['Lexset'] = {}
                s['Lexset']['@name'] = ''
                s['Lexset']['item'] = {}
                s['Lexset']['item']['$'] = row['lexset_items'].to_s
                s['Lexset']['show'] = {}
                s['Lexset']['show']['$'] = row['lexset_show'].to_s
            end
            if row['semantic_role'] != '' and row['semantic_role'] != nil
                s['Role'] = {}
                s['Role']['@name'] = row['semantic_role']
            end
            if row['polarity'] != '' and row['polarity'] != nil
                s['Polarity'] = {}
                s['Polarity']['@name'] = row['polarity'].to_s
            end
            if row['attributes'].to_s != ''
                s['attributes'] = {}
                s['attributes']['$'] = row['attributes'].to_s
            end
            if q.to_a.length > 1
                l << o
            else
                return o
            end
        }
        return l
    end

    #get document
    def get(entry_id)
        res = ""
        xml = Builder::XmlMarkup.new(:target => res)
        xml.pattern_set do
            xml.verb_stem(entry_id)
            db_query = "SELECT * FROM patterns WHERE entry_id='#{entry_id}' ORDER BY number"
            q = @dbh.execute(db_query)
            q.each{|row|
                advl_none = (row['advl_none'] == true)? "true":""
                advl_opt = (row['advl_opt'] == true)? "true":""
                object_none = (row['object_none'] == true)? "true":""
                claus_opt = (row['claus_opt'] == true)? "true":""
                claus_quote = (row['claus_quote'] == true)? "true":""
                claus_to = (row['claus_to'] == true)? "true":""
                claus_ing = (row['claus_ing'] == true)? "true":""
                claus_that = (row['claus_that'] == true)? "true":""
                claus_wh = (row['claus_wh'] == true)? "true":""
                oclaus_opt = (row['ocl_opt'] == true)? "true":""
                oclaus_quote = (row['ocl_quote'] == true)? "true":""
                oclaus_to = (row['ocl_to'] == true)? "true":""
                oclaus_ing = (row['ocl_ing'] == true)? "true":""
                oclaus_that = (row['ocl_that'] == true)? "true":""
                oclaus_wh = (row['ocl_wh'] == true)? "true":""
                impl_idiom = (row['idiom'] == true)? "true":""
                impl_pv = (row['phrasal'] == true)? "true":""
                semclass = (row['semclass'] == true)? "true":""
                xml.pattern("id"=>row['pattern_id'], "num"=>row['number']) {
                    xml.template("adverbial_class"=>row['advl_class'],
                                 "adverbial_opt"=>advl_opt,
                                 "adverbial_none"=>advl_none,
                                 'object_none'=>object_none,
                                 'auxiliary'=>row['auxiliary']) {
                        xml.verb_form(row['verb_form'])
                        xml.sub_conj(row['sub_conj'])
                        xml.subject {
                            make_xml_object(xml, entry_id, row['pattern_id'], 'S', 'M')
                            if count_alternations(entry_id, row['pattern_id'], 'S') > 0
                                make_xml_alt_object(xml, entry_id, row['pattern_id'], 'S', 'A')
                            end
                        }
                        xml.object {
                            make_xml_object(xml, entry_id, row['pattern_id'], 'O', 'M')
                            if count_alternations(entry_id, row['pattern_id'], 'O') > 0
                                make_xml_alt_object(xml, entry_id, row['pattern_id'], 'O', 'A')
                            end
                            if count_alternations(entry_id, row['pattern_id'], 'O', 'I') > 0
                                make_xml_ind_object(xml, entry_id, row['pattern_id'])
                            end
                        }
                        make_xml_complement(xml, entry_id, row['pattern_id'])
                        make_xml_adverbial(xml, entry_id, row['pattern_id'])
                        xml.clausal(row['clausal'])
                        xml.oclausals("optional"=>oclaus_opt, "to"=>oclaus_to,
                                      "ing"=>oclaus_ing, "that"=>oclaus_that,
                                      "wh"=>oclaus_wh, 'quote'=>oclaus_quote) {
                            make_xml_clausal(xml, entry_id, row['pattern_id'], 'K')
                        }
                        xml.clausals("optional"=>claus_opt, "to"=>claus_to,
                                     "ing"=>claus_ing, "that"=>claus_that,
                                     "wh"=>claus_wh, 'quote'=>claus_quote) {
                            make_xml_clausal(xml, entry_id, row['pattern_id'], 'L')
                        }
                        xml.comment(row['template_comment'], "type"=>row['template_comment_type'])
                    }
                    xml.primary_implicature(row['primary_implicature'].to_s.gsub(/&/,'&amp;'),
                            'idiom'=>impl_idiom, 'phrasal'=>impl_pv)
                    db_query2 = "SELECT * FROM implicatures WHERE entry_id='#{entry_id}'"\
                            " AND pattern_id=#{row['pattern_id']}"
                    q2 = @dbh.execute(db_query2)
                    q2.each{|row2|
                        xml.secondary_implicature(row2['implicature'].to_s.gsub(/&/,'&amp;'))
                    }
                    xml.framenet(row['framenet'])
                    xml.domain(row['domain'])
                    xml.register(row['register'])
                    xml.corpus(row['corpus'])
                    xml.comment(row['other_comment'], "type"=>row['other_comment_type'])
                    xml.exploitation(row['exploit_comment'])
                    xml.semclass(row['semclass'])
                }
            }
        end
        return res
    end

    def getonto(tid)
        res = ""
        xml = Builder::XmlMarkup.new(:target => res)
        xml.onto_entry do
            xml.tid(tid)
            db_query = "SELECT * FROM ontology WHERE tid='#{tid}'"
            q = @dbh.execute(db_query)
            q.each{|row|
                xml.term(row['term'])
                xml.comment(row['comment'])
            }
            db_query = "SELECT ontology_links.*,ontology.term AS text FROM ontology_links,ontology WHERE ontology_links.hypernym=ontology.tid AND ontology_links.term=#{tid}"
            q = @dbh.execute(db_query)
            q.each{|row|
                xml.hypernym(row['hypernym'], "text"=>row['text'])
            }
        end
        return res
    end

    def count_alternations(entry_id, pattern_id, type, pos='A')
        db_query = "SELECT * FROM objects WHERE entry_id='#{entry_id}' AND pattern_id = #{pattern_id} AND object_type='#{type}' AND position='#{pos}'"
        q = @dbh.select_all(db_query)
        return q.length
    end

    #modifies one pattern in xml
    def store_pattern(entry_id, pattern_id, new_data, user='')
        xmldoc = REXML::Document.new(new_data)
        pat = xmldoc.root
        dbh['AutoCommit'] = false
        begin
            db_query = "DELETE FROM objects WHERE entry_id='#{entry_id}' AND pattern_id=#{pattern_id}"
            dbh.do(db_query)
            db_query = "DELETE FROM implicatures WHERE entry_id='#{entry_id}' AND pattern_id=#{pattern_id}"
            dbh.do(db_query)
            number = pat.attributes['num'].to_s
            sub_conj = ''
            sub_conj = pat.elements['template/sub_conj'].text.to_s if pat.elements['template/sub_conj'] != nil
            verb_form = pat.elements['template/verb_form'].text.to_s if pat.elements['template/verb_form'] != nil
            clausal = ''
            if pat.elements['template/clausals'].attributes['optional'] == 'true'
                claus_opt = 't'
            else
                claus_opt = 'f'
            end
            if pat.elements['template/clausals'].attributes['to'] == 'true'
                claus_to = 't'
            else
                claus_to = 'f'
            end
            if pat.elements['template/clausals'].attributes['ing'] == 'true'
                claus_ing = 't'
            else
                claus_ing = 'f'
            end
            if pat.elements['template/clausals'].attributes['that'] == 'true'
                claus_that = 't'
            else
                claus_that = 'f'
            end
            if pat.elements['template/clausals'].attributes['wh'] == 'true'
                claus_wh = 't'
            else
                claus_wh = 'f'
            end
            if pat.elements['template/clausals'].attributes['quote'] == 'true'
                claus_quote = 't'
            else
                claus_quote = 'f'
            end
            if pat.elements['template/oclausals'].attributes['optional'] == 'true'
                oclaus_opt = 't'
            else
                oclaus_opt = 'f'
            end
            if pat.elements['template/oclausals'].attributes['to'] == 'true'
                oclaus_to = 't'
            else
                oclaus_to = 'f'
            end
            if pat.elements['template/oclausals'].attributes['ing'] == 'true'
                oclaus_ing = 't'
            else
                oclaus_ing = 'f'
            end
            if pat.elements['template/oclausals'].attributes['that'] == 'true'
                oclaus_that = 't'
            else
                oclaus_that = 'f'
            end
            if pat.elements['template/oclausals'].attributes['wh'] == 'true'
                oclaus_wh = 't'
            else
                oclaus_wh = 'f'
            end
            if pat.elements['template/oclausals'].attributes['quote'] == 'true'
                oclaus_quote = 't'
            else
                oclaus_quote = 'f'
            end

            template_comment = pat.elements['template/comment'].text.to_s if pat.elements['template/comment'] != nil
            template_comment_type = pat.elements['template/comment'].attributes['type'].to_s if pat.elements['template/comment'] != nil
            prim_imp = pat.elements['primary_implicature'].text.to_s if pat.elements['primary_implicature'] != nil
            if pat.elements['primary_implicature'] != nil and pat.elements['primary_implicature'].attributes['idiom'] == 'true'
                impl_idiom = 't'
            else
                impl_idiom = 'f'
            end
            if pat.elements['primary_implicature'] != nil and pat.elements['primary_implicature'].attributes['phrasal'] == 'true'
                impl_phrasal = 't'
            else
                impl_phrasal = 'f'
            end

            other_comment = pat.elements['comment'].text.to_s if pat.elements['comment'] != nil
            other_comment_type = pat.elements['comment'].attributes['type'].to_s if pat.elements['comment'] != nil
            framenet = pat.elements['framenet'].text.to_s if pat.elements['framenet'] != nil
            domain = pat.elements['domain'].text.to_s if pat.elements['domain'] != nil
            register = pat.elements['register'].text.to_s if pat.elements['register'] != nil
            corpus = pat.elements['corpus'].text.to_s if pat.elements['corpus'] != nil
            exploit_comment = pat.elements['exploitation'].text.to_s if  pat.elements['exploitation'] != nil
            advl_class = pat.elements['template'].attributes['adverbial_class'].to_s if  pat.elements['template'].attributes['adverbial_class'] != nil
            semclass = pat.elements['semclass'].text.to_s if pat.elements['semclass'] != nil
            if pat.elements['template'].attributes['adverbial_none'] == 'true'
                advl_none = 't'
            else
                advl_none = 'f'
            end
            advl_opt = 'f'
            if pat.elements['template'].attributes['adverbial_opt'] == 'true'
                advl_opt = 't'
            else
                advl_opt = 'f'
            end
            auxiliary = ''
            if pat.elements['template'].attributes['object_none'] == 'true'
                object_none = 't'
                if pat.elements['template'].attributes['auxiliary'] != ''
                    auxiliary = pat.elements['template'].attributes['auxiliary']
                end
            else
                object_none = 'f'
            end
            db_query = "UPDATE patterns SET sub_conj=?, verb_form=?, clausal=?,"\
                "template_comment=?, template_comment_type=?, primary_implicature=?,"\
                "idiom=?, phrasal=?, other_comment=?, other_comment_type=?,"\
                "domain=?, register=?, corpus=?, exploit_comment=?, advl_class=?,"\
                "advl_none=?, advl_opt=?, claus_to=?, claus_ing=?,"\
                "claus_that=?, claus_wh=?, claus_quote=?, claus_opt=?,"\
                "object_none=?, framenet=?, semclass=?, ocl_opt=?, ocl_to=?,"\
                "ocl_ing=?, ocl_that=?, ocl_wh=?, ocl_quote=?, auxiliary=? "\
                "WHERE entry_id=? AND pattern_id=?"
            dbh.do(db_query, sub_conj, verb_form, clausal, template_comment,
                template_comment_type, prim_imp, impl_idiom, impl_phrasal,
                other_comment, other_comment_type, domain, register, corpus,
                exploit_comment, advl_class, advl_none, advl_opt, claus_to,
                claus_ing, claus_that, claus_wh, claus_quote, claus_opt,
                object_none, framenet, semclass, oclaus_opt, oclaus_to,
                oclaus_ing, oclaus_that, oclaus_wh, oclaus_quote, auxiliary,
                entry_id, pattern_id)
            if pat.elements['template/subject/argspec/BSO_type'] != nil
                sem_type = ''
                sem_role = ''
                polarity = ''
                lex_show = ''
                lex_items = ''
                det_quant = ''

                sem_type = pat.elements['template/subject/argspec/BSO_type'].attributes['name'].to_s
                det_quant = pat.elements['template/subject/argspec'].attributes['det_quant'].to_s

                if pat.elements['template/subject/argspec/subspec/and'] != nil
                    sem_role = pat.elements['template/subject/argspec/subspec/and/Role'].attributes['name'].to_s if pat.elements['template/subject/argspec/subspec/and/Role'] != nil
                else
                    sem_role = pat.elements['template/subject/argspec/subspec/Role'].attributes['name'].to_s if pat.elements['template/subject/argspec/subspec/Role'] != nil
                end

                if pat.elements['template/subject/argspec/subspec/Lexset'] != nil
                    if pat.elements['template/subject/argspec/subspec/Lexset/show'] != nil
                      lex_show =pat.elements['template/subject/argspec/subspec/Lexset/show'].text.to_s
                    end
                    lex_items = ''
                    REXML::XPath.match(pat, 'template/subject/argspec/subspec/Lexset/item').each {|el|
                        lex_items += el.text.to_s
                    }
                end

                attributes = pat.elements['template/subject/argspec/subspec/attributes'].text.to_s if pat.elements['template/subject/argspec/subspec/attributes'] != nil

                i = 0
                sem_type.split("|").each{|st|
                    st.strip!
                    position = (i==0)? 'M':'A'

                    if st != ''
                        db_query = "INSERT INTO objects (entry_id, pattern_id,"\
                            " position, object_type, semantic_type, semantic_role,"\
                            " polarity, lexset_show, lexset_items, attributes, det_quant) "\
                            "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
                        dbh.do(db_query, entry_id, pattern_id, position, 'S',
                            st, sem_role, polarity, lex_show, lex_items,
                            attributes, det_quant)
                        i += 1
                    end
                }
                if i == 0 and lex_items != ''
                    db_query = "INSERT INTO objects (entry_id, pattern_id, "\
                        "position, object_type, semantic_type, semantic_role, "\
                        "polarity, lexset_show, lexset_items, attributes, det_quant) "\
                        "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
                    dbh.do(db_query, entry_id, pattern_id, 'M', 'S', '', sem_role,
                        polarity, lex_show, lex_items, attributes, det_quant)
                    end
                end
                if pat.elements['template/subject/alternation/argspec'] != nil
                    REXML::XPath.match(pat, 'template/subject/alternation/argspec').each{|el|
                        if el.elements['BSO_type'] != nil
                            sem_type = ''
                            sem_role = ''
                            polarity = ''
                            lex_show = ''
                            lex_items = ''
                            det_quant = ''

                            sem_type = el.elements['BSO_type'].attributes['name'].to_s
                            det_quant = el.attributes['det_quant'].to_s

                            if el.elements['subspec/and'] != nil
                                sem_role = el.elements['subspec/and/Role'].attributes['name'].to_s if el.elements['subspec/and/Role'] != nil
                            else
                                sem_role = el.elements['subspec/Role'].attributes['name'].to_s if el.elements['subspec/Role'] != nil
                            end

                            if el.elements['subspec/Lexset'] != nil
                              if el.elements['subspec/Lexset/show'] != nil
                                lex_show =el.elements['subspec/Lexset/show'].text.to_s
                              end
                              lex_items = ''
                              REXML::XPath.match(el, 'subspec/Lexset/item').each {|eli|
                                    lex_items += eli.text.to_s
                                }
                            end

                            attributes = el.elements['subspec/attributes'].text.to_s if el.elements['subspec/attributes'] != nil

                            i = 0
                            sem_type.split("|").each{|st|
                                st.strip!
                                db_query = "INSERT INTO objects (entry_id, pattern_id, "\
                                    "position, object_type, semantic_type, semantic_role, "\
                                    "polarity, lexset_show, lexset_items, attributes, det_quant) "\
                                    "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
                                dbh.do(db_query, entry_id, pattern_id, 'A',
                                    'S', st, sem_role, polarity, lex_show,
                                    lex_items, attributes, det_quant)
                                i += 1
                            }
                            if i == 0 and lex_items != ''
                                db_query = "INSERT INTO objects (entry_id, "\
                                    "pattern_id, position, object_type, "\
                                    "semantic_type, semantic_role, polarity, "\
                                    "lexset_show, lexset_items, attributes, det_quant) "\
                                    "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
                                dbh.do(db_query, entry_id, pattern_id, 'A',
                                    'S', '', sem_role, polarity, lex_show,
                                    lex_items, attributes, det_quant)
                            end
                        end
                    }
                end
                elem = pat.elements['template/object/argspec/']
                if pat.elements['template/object/argspec/BSO_type'] != nil || (elem!= nil && ( elem.attributes['to']=='true' ||  elem.attributes['ing']=='true' ||  elem.attributes['wh']=='true' ||  elem.attributes['quote']=='true' ||  elem.attributes['that']=='true'))
                    sem_type = ''
                    sem_role = ''
                    polarity = ''
                    lex_show = ''
                    lex_items = ''
                    headword = ''
                    if elem.attributes['to'] == 'true'
                        oclaus_to = 't'
                    else
                        oclaus_to = 'f'
                    end
                    if elem.attributes['ing'] == 'true'
                        oclaus_ing = 't'
                    else
                        oclaus_ing = 'f'
                    end
                    if elem.attributes['wh'] == 'true'
                        oclaus_wh = 't'
                    else
                        oclaus_wh = 'f'
                    end
                    if elem.attributes['quote'] == 'true'
                        oclaus_quote = 't'
                    else
                        oclaus_quote = 'f'
                    end
                    if elem.attributes['that'] == 'true'
                        oclaus_that = 't'
                    else
                        oclaus_that = 'f'
                    end

                    det_quant = pat.elements['template/object/argspec'].attributes['det_quant'].to_s
                    sem_type = pat.elements['template/object/argspec/BSO_type'].attributes['name'].to_s  if pat.elements['template/object/argspec/BSO_type'].attributes['name'] != ''

                    if pat.elements['template/object/argspec/subspec/and'] != nil
                        sem_role = pat.elements['template/object/argspec/subspec/and/Role'].attributes['name'].to_s if pat.elements['template/object/argspec/subspec/and/Role'] != nil
                    else
                        sem_role = pat.elements['template/object/argspec/subspec/Role'].attributes['name'].to_s if pat.elements['template/object/argspec/subspec/Role'] != nil
                    end

                    attributes = pat.elements['template/object/argspec/subspec/attributes'].text.to_s if pat.elements['template/object/argspec/subspec/attributes'] != nil

                    if pat.elements['template/object/argspec'].attributes['type'] == 'optional'
                        optional = 't'
                    else
                        optional = 'f'
                    end

                    if pat.elements['template/object/argspec'].attributes['none'] == 'true'
                        none = 't'
                    else
                        none = 'f'
                    end

                    if pat.elements['template/object/argspec/subspec/Lexset'] != nil
                        if pat.elements['template/subject/argspec/subspec/Lexset/show'] != nil
                          lex_show =pat.elements['template/subject/argspec/subspec/Lexset/show'].text.to_s
                        end
                        lex_items = ''
                        REXML::XPath.match(pat, 'template/object/argspec/subspec/Lexset/item').each {|el|
                            lex_items += el.text.to_s
                        }
                    end

                    i = 0
                    sem_type.split("|").each{|st|
                        st.strip!
                        position = (i == 0)? 'M':'A'

                        if st != ''
                            db_query = "INSERT INTO objects (entry_id, pattern_id, position, object_type, semantic_type, semantic_role, polarity, lexset_show, lexset_items, optional, none, det_quant, attributes, ocl_to, ocl_ing, ocl_wh, ocl_quote, ocl_that) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
                            dbh.do(db_query, entry_id, pattern_id, position, 'O', st, sem_role, polarity, lex_show, lex_items, optional, none, det_quant, attributes, oclaus_to, oclaus_ing, oclaus_wh, oclaus_quote, oclaus_that)
                            i += 1
                            end
                    }
                    if i == 0 and (lex_items != '' || oclaus_that == 't'|| oclaus_ing == 't'|| oclaus_to == 't'|| oclaus_wh == 't'|| oclaus_quote == 't')
                        db_query = "INSERT INTO objects (entry_id, pattern_id, position, object_type, semantic_type, semantic_role, polarity, lexset_show, lexset_items, optional, none, det_quant, attributes, ocl_to, ocl_ing, ocl_wh, ocl_quote, ocl_that) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
                        dbh.do(db_query, entry_id, pattern_id, 'M', 'O', '', sem_role, polarity, lex_show, lex_items, optional, none, det_quant, attributes, oclaus_to, oclaus_ing, oclaus_wh, oclaus_quote, oclaus_that)
                        end
                    end
                    if pat.elements['template/object/alternation/argspec'] != nil
                        REXML::XPath.match(pat, 'template/object/alternation/argspec').each {|el|
                            if el.elements['BSO_type'] != nil ||el.attributes['to']=='true' || el.attributes['to']=='true' || el.attributes['that']=='true' ||el.attributes['wh']=='true' || el.attributes['ing']=='true' ||el.attributes['quote']=='true'
                                sem_type = ''
                                sem_role = ''
                                polarity = ''
                                lex_show = ''
                                lex_items = ''
                                if el.attributes['to'] == 'true'
                                    oclaus_to = 't'
                                else
                                    oclaus_to = 'f'
                                end
                                if el.attributes['ing'] == 'true'
                                    oclaus_ing = 't'
                                else
                                    oclaus_ing = 'f'
                                end
                                if el.attributes['wh'] == 'true'
                                    oclaus_wh = 't'
                                else
                                    oclaus_wh = 'f'
                                end
                                if el.attributes['quote'] == 'true'
                                    oclaus_quote = 't'
                                else
                                    oclaus_quote = 'f'
                                end
                                if el.attributes['that'] == 'true'
                                    oclaus_that = 't'
                                else
                                    oclaus_that = 'f'
                                end

                                sem_type = el.elements['BSO_type'].attributes['name'].to_s

                                if el.elements['subspec/and'] != nil
                                    sem_role = el.elements['subspec/and/Role'].attributes['name'].to_s if el.elements['subspec/and/Role'] != nil
                                else
                                    sem_role = el.elements['subspec/Role'].attributes['name'].to_s if el.elements['subspec/Role'] != nil
                                end

                                attributes = el.elements['subspec/attributes'].text.to_s if el.elements['subspec/attributes'] != nil

                                if el.attributes['type'] == 'optional'
                                    optional = 't'
                                else
                                    optional = 'f'
                                end

                                headword = el.attributes['headword'].to_s
                                det_quant = el.attributes['det_quant'].to_s

                                if el.elements['subspec/Lexset'] != nil
                                  if el.elements['subspec/Lexset/show'] != nil
                                    lex_show =el.elements['subspec/Lexset/show'].text.to_s
                                  end
                                  lex_items = ''
                                  REXML::XPath.match(el, 'subspec/Lexset/item').each {|eli|
                                        lex_items += eli.text.to_s
                                    }
                                end

                                i = 0
                                sem_type.split("|").each{|st|
                                    st.strip!
                                    if st != ''
                                        db_query = "INSERT INTO objects (entry_id, pattern_id, position, object_type, semantic_type, semantic_role, polarity, lexset_show, lexset_items, optional, none, det_quant, attributes, ocl_to, ocl_ing, ocl_wh, ocl_quote, ocl_that) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
                                        dbh.do(db_query, entry_id, pattern_id, 'A', 'O', st, sem_role, polarity, lex_show, lex_items, optional, none, det_quant, attributes, oclaus_to, oclaus_ing, oclaus_wh, oclaus_quote, oclaus_that)
                                        i += 1
                                    end
                                }

                                if i == 0 and (lex_items != '' || oclaus_that == 't'|| oclaus_ing == 't'|| oclaus_to == 't'|| oclaus_wh == 't'|| oclaus_quote == 't')
                                    db_query = "INSERT INTO objects (entry_id, pattern_id, position, object_type, semantic_type, semantic_role, polarity, lexset_show, lexset_items, optional, none, det_quant, attributes, ocl_to, ocl_ing, ocl_wh, ocl_quote, ocl_that) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
                                    dbh.do(db_query, entry_id, pattern_id, 'A', 'O', '', sem_role, polarity, lex_show, lex_items, optional, none, det_quant, attributes, oclaus_to, oclaus_ing, oclaus_wh, oclaus_quote, oclaus_that)
                                end
                            end
                        }
                    end
                    if pat.elements['template/object/indirect/argspec'] != nil
                        REXML::XPath.match(pat, 'template/object/indirect/argspec').each {|el|
                            if el.elements['BSO_type'] != nil
                                sem_type = ''
                                sem_role = ''
                                polarity = ''
                                lex_show = ''
                                lex_items = ''

                                sem_type = el.elements['BSO_type'].attributes['name'].to_s

                                if el.elements['subspec/and'] != nil
                                    sem_role = el.elements['subspec/and/Role'].attributes['name'].to_s if el.elements['subspec/and/Role'] != nil
                                else
                                    sem_role = el.elements['subspec/Role'].attributes['name'].to_s if el.elements['subspec/Role'] != nil
                                end

                                attributes = el.elements['subspec/attributes'].text.to_s if el.elements['subspec/attributes'] != nil

                                if el.attributes['type'] == 'optional'
                                    optional = 't'
                                else
                                    optional = 'f'
                                end

                                headword = el.attributes['headword'].to_s
                                det_quant = el.attributes['det_quant'].to_s

                                if el.elements['subspec/Lexset'] != nil
                                  if el.elements['subspec/Lexset/show'] != nil
                                    lex_show =el.elements['subspec/Lexset/show'].text.to_s
                                  end
                                  lex_items = ''
                                  REXML::XPath.match(el, 'subspec/Lexset/item').each {|eli|
                                        lex_items += eli.text.to_s
                                    }
                                end

                                i = 0
                                sem_type.split("|").each{|st|
                                    st.strip!
                                    if st != ''
                                        db_query = "INSERT INTO objects (entry_id, pattern_id, position, object_type, semantic_type, semantic_role, polarity, lexset_show, lexset_items, optional, none, det_quant, attributes) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
                                        dbh.do(db_query, entry_id, pattern_id, 'I', 'O', st, sem_role, polarity, lex_show, lex_items, optional, none, det_quant, attributes)
                                        i += 1
                                    end
                                }
                                if i == 0 and lex_items != ''
                                    db_query = "INSERT INTO objects (entry_id, pattern_id, position, object_type, semantic_type, semantic_role, polarity, lexset_show, lexset_items, optional, none, det_quant, attributes) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
                                    dbh.do(db_query, entry_id, pattern_id, 'I', 'O', '', sem_role, polarity, lex_show, lex_items, optional, none, det_quant, attributes)
                                end
                            end
                        }
                    end
                    i = 0
                    if pat.elements['template/comp'] != nil
                        REXML::XPath.match(pat, 'template/comp').each {|elc|
                            comp = ''
                            comp_type = ''
                            comp_role = ''
                            comp_optional = 'f'
                            comp_as = 'f'
                            comp_as_opt = 'f'
                            comp_det = ''

                            comp = elc.text.to_s
                            comp_type = elc.attributes['type'].to_s if elc.attributes['type'] != nil
                            comp_role = elc.attributes['role'].to_s if elc.attributes['role'] != nil
                            comp_lexset = elc.attributes['lexset'].to_s if elc.attributes['lexset'] != nil
                            comp_lexset_show = elc.attributes['lexset_show'].to_s if elc.attributes['lexset_show'] != nil
                            comp_optional = elc.attributes['optional'].to_s if elc.attributes['optional'] != nil
                            comp_as = elc.attributes['complement_as'].to_s if elc.attributes['complement_as'] != nil
                            comp_as_opt = elc.attributes['complement_as_optional'].to_s if elc.attributes['complement_as_optional'] != nil
                            comp_det = elc.attributes['complement_det'].to_s if elc.attributes['complement_det'] != nil

                            position = (i == 0)? 'M':'A'
                            i += 1
                            db_query = "INSERT INTO objects (entry_id, pattern_id, position, object_type, semantic_type, semantic_role, lexset_items, optional, complement_type, complement_as, complement_det, complement_as_optional,lexset_show) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?)"
                            dbh.do(db_query, entry_id, pattern_id, position,
                                   'C', comp, comp_role, comp_lexset,
                                   comp_optional, comp_type, comp_as, comp_det,
                                   comp_as_opt,comp_lexset_show)
                        }
                    end
                    advl_group = 0
                    if pat.elements['template/adverbial'] != nil
                        REXML::XPath.match(pat, 'template/adverbial').each {|ela|
                            advl_group += 1
                            advl_func = ela.attributes['func'].to_s
                            if ela.attributes['optional'] == 'true'
                                advl_main_opt = 't'
                            else
                                advl_main_opt = 'f'
                            end
                            i = 0
                            adv_comp_opt = 'f'
                            REXML::XPath.match(ela, 'alternation').each {|alt_el|
                                sem_type = ''
                                sem_role = ''
                                polarity = ''
                                lex_show = ''
                                lex_items = ''
                                headword = ''

                                if alt_el.attributes['type'] == 'optional'
                                    optional = 't'
                                else
                                    optional = 'f'
                                end
                                adv_comp_opt = 'f'
                                adv_comp_opt = 't' if alt_el.attributes['adv_comp_opt'] == 'true'

                                headword = alt_el.elements['ptag'].attributes['headword'].to_s if alt_el.elements['ptag'] != nil

                                el = alt_el.elements['ptag/argspec']
                                if el.elements['BSO_type'] != nil
                                    sem_type = el.elements['BSO_type'].attributes['name'].to_s

                                    sem_role = el.elements['subspec/Role'].attributes['name'].to_s if el.elements['subspec/Role'] != nil

                                    attributes = el.elements['subspec/attributes'].text.to_s if el.elements['subspec/attributes'] != nil

                                    if el.elements['subspec/Lexset'] != nil
                                      if el.elements['subspec/Lexset/show'] != nil
                                        lex_show =el.elements['subspec/Lexset/show'].text.to_s
                                      end
                                      lex_items = ''
                                      REXML::XPath.match(el, 'subspec/Lexset/item').each {|eli|
                                            lex_items += eli.text.to_s
                                        }
                                    end
                                end
                                if el.elements['adverbs'] != nil
                                    adverbs = el.elements['adverbs'].text.to_s
                                end

                                sem_type.split("|").each{|st|
                                    st.strip!
                                    position = (i == 0)? 'M':'A'

                                    if st != ''
                                        db_query = "INSERT INTO objects (entry_id, pattern_id, position, object_type, semantic_type, semantic_role, polarity, lexset_show, lexset_items, optional, headword, advl_adverbs, advl_function, advl_group, advl_main_opt, attributes, adv_comp_opt) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
                                        dbh.do(db_query, entry_id, pattern_id, position, 'A', st, sem_role, polarity, lex_show, lex_items, optional, headword, adverbs, advl_func, advl_group, advl_main_opt, attributes, adv_comp_opt)
                                        i += 1
                                        end
                                }
                                if sem_type == '' and (adverbs != '' or lex_items != '' or headword != '')
                                    position = (i == 0)? 'M':'A'
                                    db_query = "INSERT INTO objects (entry_id, pattern_id, position, object_type, semantic_type, semantic_role, polarity, lexset_show, lexset_items, optional, headword, advl_adverbs, advl_function, advl_group, advl_main_opt, attributes, adv_comp_opt) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
                                    dbh.do(db_query, entry_id, pattern_id, position, 'A', '', sem_role, polarity, lex_show, lex_items, optional, headword, adverbs, advl_func, advl_group, advl_main_opt, attributes, adv_comp_opt)
                                    i += 1
                                end
                            }
                            if i == 0
                                db_query = "INSERT INTO objects (entry_id, pattern_id, position, object_type, advl_function, advl_group, advl_main_opt, adv_comp_opt) VALUES (?, ?, ?, ?, ?, ?, ?, ?)"
                                dbh.do(db_query, entry_id, pattern_id, 'X', 'A', advl_func, advl_group, advl_main_opt, adv_comp_opt)
                            end
                        }
                    end
                    if pat.elements['template/oclausals/clausal/argspec'] != nil
                        REXML::XPath.match(pat, 'template/oclausals/clausal/argspec').each {|el|
                            if el.elements['BSO_type'] != nil
                                sem_type = ''
                                sem_role = ''
                                polarity = ''
                                lex_show = ''
                                lex_items = ''

                            if el.attributes['type'] == 'optional'
                                optional = 't'
                            else
                                optional = 'f'
                            end

                            det_quant = el.attributes['det_quant'].to_s
                            sem_type = el.elements['BSO_type'].attributes['name'].to_s

                            if el.elements['subspec/and'] != nil
                                sem_role = el.elements['subspec/and/Role'].attributes['name'].to_s if el.elements['subspec/and/Role'] != nil
                            else
                                sem_role = el.elements['subspec/Role'].attributes['name'].to_s if el.elements['subspec/Role'] != nil
                            end

                            attributes = el.elements['subspec/attributes'].text.to_s if el.elements['subspec/attributes'] != nil

                            if el.elements['subspec/Lexset'] != nil
                              if el.elements['subspec/Lexset/show'] != nil
                                lex_show =el.elements['subspec/Lexset/show'].text.to_s
                              end
                              lex_items = ''
                              REXML::XPath.match(el, 'subspec/Lexset/item').each {|eli|
                                    lex_items += eli.text.to_s
                                }
                            end

                            i = 0
                            sem_type.split("|").each{|st|
                                st.strip!
                                position = (i == 0)? 'M':'A'
                                if st != ''
                                    db_query = "INSERT INTO objects (entry_id, pattern_id, position, object_type, semantic_type, semantic_role, polarity, lexset_show, lexset_items, det_quant, optional, attributes) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
                                    dbh.do(db_query, entry_id, pattern_id, position, 'K', st, sem_role, polarity, lex_show, lex_items, det_quant, optional, attributes)
                                    i += 1
                                end
                            }
                            if i == 0 and lex_items != ''
                                db_query = "INSERT INTO objects (entry_id, pattern_id, position, object_type, semantic_type, semantic_role, polarity, lexset_show, lexset_items, det_quant, optional, attributes) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
                                dbh.do(db_query, entry_id, pattern_id, 'M', 'K', '', sem_role, polarity, lex_show, lex_items, det_quant, optional, attributes)
                            end
                        end
                    }
                end
                if pat.elements['template/clausals/clausal/argspec'] != nil
                    REXML::XPath.match(pat, 'template/clausals/clausal/argspec').each {|el|
                        if el.elements['BSO_type'] != nil
                            sem_type = ''
                            sem_role = ''
                            polarity = ''
                            lex_show = ''
                            lex_items = ''

                            if el.attributes['type'] == 'optional'
                                optional = 't'
                            else
                                optional = 'f'
                            end

                            headword = el.attributes['det_quant'].to_s


                            sem_type = el.elements['BSO_type'].attributes['name'].to_s

                            if el.elements['subspec/and'] != nil
                                sem_role = el.elements['subspec/and/Role'].attributes['name'].to_s if el.elements['subspec/and/Role'] != nil
                            else
                                sem_role = el.elements['subspec/Role'].attributes['name'].to_s if el.elements['subspec/Role'] != nil
                            end

                            attributes = el.elements['subspec/attributes'].text.to_s if el.elements['subspec/attributes'] != nil

                            if el.elements['subspec/Lexset'] != nil
                              if el.elements['subspec/Lexset/show'] != nil
                                lex_show = el.elements['subspec/Lexset/show'].text.to_s
                              end
                              lex_items = ''
                              REXML::XPath.match(el, 'subspec/Lexset/item').each {|eli|
                                    lex_items += eli.text.to_s
                                }
                            end

                            i = 0
                            sem_type.split("|").each{|st|
                                st.strip!
                                position = (i == 0)? 'M':'A'

                                if st != ''
                                    db_query = "INSERT INTO objects (entry_id, pattern_id, position, object_type, semantic_type, semantic_role, polarity, lexset_show, lexset_items, det_quant, optional, attributes) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
                                    dbh.do(db_query, entry_id, pattern_id, position, 'L', st, sem_role, polarity, lex_show, lex_items, det_quant, optional, attributes)
                                    i += 1
                                end
                            }
                            if i == 0 and lex_items != ''
                                db_query = "INSERT INTO objects (entry_id, pattern_id, position, object_type, semantic_type, semantic_role, polarity, lexset_show, lexset_items, det_quant, optional, attributes) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
                                dbh.do(db_query, entry_id, pattern_id, 'M', 'L', '', sem_role, polarity, lex_show, lex_items, det_quant, optional, attributes)
                            end
                        end
                    }
                end

                if pat.elements['secondary_implicature'] != nil
                    REXML::XPath.match(pat, '//secondary_implicature').each {|el|
                        db_query = "INSERT INTO implicatures (entry_id, pattern_id, implicature) VALUES (?, ?, ?)"
                        dbh.do(db_query, entry_id, pattern_id, el.text.to_s)
                    }
                end
                db_query = "SELECT status FROM entries WHERE entry_id=?"
                q = @dbh.select_one(db_query, entry_id)
                if q[0].to_s == 'N'
                    db_query = "UPDATE entries SET create_time=CURRENT_TIMESTAMP, create_by=?, status=? WHERE entry_id=?"
                    dbh.do(db_query, user, 'W', entry_id)
                end

                # save current pattern string!
                patstr = get_pattern_string(entry_id, pattern_id)
                patq = "UPDATE patterns SET pattern_string=? WHERE entry_id=? AND pattern_id=?"
                dbh.do(patq, patstr, entry_id, pattern_id)
                dbh.commit

                # update TIMESTAMP
                db_query = "UPDATE entries SET last_edit=CURRENT_TIMESTAMP, edit_by=? WHERE entry_id=?"
                dbh.do(db_query, user, entry_id)
                dbh.commit
            rescue=>e
                $stderr.puts e
                $stderr.puts e.backtrace.join("\n")
                dbh.rollback
            end
        end

    def renumber(key, data, use_num=false, user='')
        dbh['AutoCommit'] = false
        ske_map = ''
        begin
            mapdata = {}
            data.each{|id, n|
                next if id == '' or n == ''
                qid = id
                if use_num
                    db_query = "SELECT pattern_id FROM patterns WHERE entry_id=? AND number=?"
                    q = @dbh.select_one(db_query, key, id)
                    qid = q[0].to_i
                    ske_map += "#{qid}:#{n}-"
                    mapdata[qid] = n
                else
                    dbq1 = "SELECT number FROM patterns WHERE entry_id=? AND pattern_id=?"
                    dbq1q = @dbh.select_one(dbq1, key, id)
                    tmp_n = dbq1q[0].to_i.to_s
                    ske_map += "#{tmp_n}:#{n}-"
                    db_query = "UPDATE patterns SET number=? WHERE entry_id=? AND pattern_id=?"
                    dbh.do(db_query, n, key, id)
                end
            }
            if use_num
                mapdata.each{|id, n|
                    db_query = "UPDATE patterns SET number=? WHERE entry_id=? AND pattern_id=?"
                    dbh.do(db_query, n, key, id)
                }
            end
            db_query = "UPDATE entries SET last_edit=CURRENT_TIMESTAMP, edit_by=? WHERE entry_id=?"
            dbh.do(db_query, user, key)
            dbh.commit
            return ske_map
        rescue => e
            $stderr.puts e
            dbh.rollback
            return 'error'
        end
    end

    def get_info_id(entry, num, corpus)
      if @language == 'en'
          lpos = '-v'
      else
          lpos = ''
      end
      url = $ske_url + 'getlngroupmap?corpname=' + corpus + '&annotconc=' + entry + lpos
      map = JSON.parse(open(URI.escape(url), :http_basic_authentication=>['guest', 'GG0we5t']).read)
      map['map'].each{|key, value|
          return key if value == num
      }
      return 'Not found!'
    end

    def new_pattern(entry_id, number, user='', newentry=false)
      dbh['AutoCommit'] = false
      begin
        db_query = "SELECT * FROM entries WHERE entry_id='#{entry_id}'"
        q = @dbh.select_all(db_query)
        if q.length == 0 and newentry
          if @main_freq_corp == 'bnc'
            oec = `echo "#{entry_id}-v"|lsclex -f -s oec lempos|awk '{print $3}'`
            oec = oec.strip.to_i + 0
            bnc = `echo "#{entry_id}-v"|lsclex -f -s bnc lempos|awk '{print $3}'`
            bnc = bnc.strip.to_i + 0
            bnc50 = `echo "#{entry_id}-v"|lsclex -f -s bnc50 lempos|awk '{print $3}'`
            bnc50 = bnc50.strip.to_i + 0
          else
            oec = 0
            bnc50 = 0
            bnc = `echo "#{entry_id}"|lsclex -f -s #{@main_freq_corp} lemma|awk '{print $3}'`
            bnc = bnc.strip.to_i + 0
          end
          if @language == 'en'
            db_query2 = "INSERT INTO entries (entry_id, create_time, last_edit,"\
                " edit_by, create_by, oec_freq, bnc_freq, bnc50_freq) VALUES "\
                "(?, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, ?, ?, ?, ?)"
            dbh.do(db_query2, entry_id, user, user, oec, bnc, bnc50)
          else
            db_query2 = "INSERT INTO entries (entry_id, create_time, last_edit, "\
                "edit_by, create_by) VALUES "\
                "(?, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, ?, ?)"
            dbh.do(db_query2, entry_id, user, user)
          end
        end
        if (q.length > 0 and not newentry) or (q.length == 0 and newentry)
          max = get_max_id(entry_id)+1
          first_free = get_lowest_unused(entry_id)
          db_query3 = "INSERT INTO patterns (entry_id, pattern_id, number, verb_form) VALUES (?, ?, ?, ?)"
          dbh.do(db_query3, entry_id, max, first_free, entry_id)
          db_query = "INSERT INTO objects (entry_id, pattern_id, position, object_type, semantic_type) VALUES (?, ?, ?, ?, ?)"
          dbh.do(db_query, entry_id, max, 'M', 'S', 'Human')
          db_query = "UPDATE entries SET last_edit=CURRENT_TIMESTAMP, edit_by=? WHERE entry_id=?"
          dbh.do(db_query, user, entry_id)
          db_query = "SELECT framenet FROM patterns WHERE entry_id=? AND number=?"
          qf = dbh.select_one(db_query, entry_id, 1)
          fnet = ''
          fnet = qf[0].to_s unless qf.nil?
          if fnet == 'NOT YET IN. 20 June 2009.'
            db_query = "UPDATE patterns SET framenet=? WHERE entry_id=? AND pattern_id=?"
            dbh.do(db_query, 'NOT YET IN. '+Time.now.strftime("%d %B  %Y."), entry_id, max)
          end
          dbh.commit
          patstr = get_pattern_string(entry_id, max)
          patq = "UPDATE patterns SET pattern_string=? WHERE entry_id=? AND pattern_id=?"
          @dbh.do(patq, patstr, entry_id, max)
          @dbh.commit
        else
          dbh.commit
        end
      rescue => e
        $stderr.puts e
        $stderr.puts e.backtrace
        dbh.rollback
        return 'error'
      end
      return max
    end

    def get_max_id(entry_id)
        db_query = "SELECT MAX(pattern_id) FROM patterns WHERE entry_id='#{entry_id}'"
        q = @dbh.select_one(db_query)
        return q[0].to_i
    end

    def get_max_number(entry_id)
        db_query = "SELECT MAX(number) FROM patterns WHERE entry_id='#{entry_id}'"
        q = @dbh.select_one(db_query)
        return q[0].to_i
    end

    def get_lowest_unused(entry_id)
        db_query = "SELECT MAX(number) FROM patterns WHERE entry_id='#{entry_id}'"
        q = @dbh.select_one(db_query)
        max_number = q[0].to_i
        for i in 1..max_number
            db_query = "SELECT number FROM patterns WHERE entry_id='#{entry_id}' and number = #{i}"
            q = @dbh.select_all(db_query)
            if q.length == 0
                return i
            end
        end
        return max_number+1
    end

    def get_numbers(entry_id)
        nums = {}
        db_query = "SELECT * FROM patterns WHERE entry_id='#{entry_id}' ORDER BY number"
        q = dbh.execute(db_query)
        q.each{|row|
            id = row['pattern_id']
            num = row['number']
            nums[id] = num
        }
        return nums
    end

    def delete(entry_id)
        dbh['AutoCommit'] = false
        begin
            db_query = "DELETE FROM objects WHERE entry_id='#{entry_id}'"
            dbh.do(db_query)
            db_query = "DELETE FROM implicatures WHERE entry_id='#{entry_id}'"
            dbh.do(db_query)
            db_query = "DELETE FROM patterns WHERE entry_id='#{entry_id}'"
            dbh.do(db_query)
            db_query = "DELETE FROM entries WHERE entry_id='#{entry_id}'"
            dbh.do(db_query)
            dbh.commit
            return 'deleted'
            rescue => e
                $stderr.puts e
                dbh.rollback
                return 'error'
        end
    end

    def delete_pattern(entry_id, pattern_id, renumber=nil, user='', use_num=false)
      if use_num
        db_q1 = "SELECT pattern_id from patterns where entry_id=? and number=?"
        pn = @dbh.select_one(db_q1, entry_id, pattern_id)
        if pn.nil?
          return 'error'
        end
        pattern_id = pn[0].to_i
      end
      dbh['AutoCommit'] = false
      begin
        if renumber != nil
          renstat = renumber(entry_id, renumber, use_num, user)
          if renstat == 'error'
            return 'error'
          end
        end
        db_query = "DELETE FROM objects WHERE entry_id='#{entry_id}' "\
            "AND pattern_id=#{pattern_id}"
        dbh.do(db_query)
        db_query = "DELETE FROM implicatures WHERE entry_id='#{entry_id}' "\
            "AND pattern_id=#{pattern_id}"
        dbh.do(db_query)
        db_query = "DELETE FROM patterns WHERE entry_id='#{entry_id}' "\
            "AND pattern_id=#{pattern_id}"
        dbh.do(db_query)
        db_query = "UPDATE entries SET last_edit=CURRENT_TIMESTAMP, edit_by=?"\
            " WHERE entry_id='#{entry_id}'"
        dbh.do(db_query, user)
        dbh.commit
        return 'deleted'
      rescue => e
        $stderr.puts e
        dbh.rollback
        return 'error'
      end
    end

    def set_status(entry_id, status, user)
        dbh['AutoCommit'] = false
        begin
            db_query = "UPDATE entries SET last_edit=CURRENT_TIMESTAMP, status=?, edit_by=? WHERE entry_id=?"
            db_check_s = "SELECT status FROM entries WHERE entry_id=?"
            q_check = @dbh.select_one(db_check_s, entry_id)
            dbh.do(db_query, status, user, entry_id)
            dbh.commit
            rescue=>e
                $stderr.puts e
                dbh.rollback
        end    
        return 'ok'
    end

    def is_trainee(admin, user)
        xml = admin.get('user'+user)
        doc = REXML::Document.new(xml.to_s)
        el = doc.root.elements["trainee"]
        if el != nil and el.first == 'true'
            return true
        end
        return false
    end

    def get_perm(admin, user, entry='')
        xml = admin.get('user'+user)
        doc = REXML::Document.new(xml.to_s)
        perm_el = doc.root.elements["//service[@code='#{admin.service_name}']/dict[@code='cpa']"]
        if perm_el != nil
            perm = perm_el.attributes['perm'].to_s
            if entry != '' and perm == 'w'
                db_query = "SELECT status FROM entries WHERE entry_id=?"
                q = @dbh.select_one(db_query, entry)
                if q != nil and (q[0].to_s != 'C' || (user == 'patrick' || user =='ymaarouf' || user == 'saramoze'))
                    return 'w'
                else
                    return 'r'
                end
            end
            return perm
        else
            return nil
        end
    end

    def copy_pattern(fromentry, pattern, toentry, user='')
      dbh['AutoCommit'] = false
      user = Thread.current[:auth_user]
      begin
        first_free = get_lowest_unused(toentry)
        max = 0
        db_query = "SELECT * FROM patterns WHERE entry_id=? AND pattern_id=?"
        q = dbh.execute(db_query, fromentry, pattern)
        q.each{|patrow|
          max = get_max_id(toentry)+1
          first_free = get_lowest_unused(toentry)
          db_query2 = "INSERT INTO patterns (entry_id, pattern_id, "\
            "number, verb_form, pattern_string, clausal, template_comment, "\
            "template_comment_type, primary_implicature, other_comment, "\
            "other_comment_type, domain, register, corpus, exploit_comment, "\
            "advl_class, advl_none, advl_opt, claus_to, claus_ing, "\
            "claus_that, claus_wh, claus_quote, claus_opt, object_none, "\
            "framenet, semclass, sub_conj) "\
            "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, "\
            "?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
          dbh.do(db_query2, toentry, max, first_free,
            patrow['verb_form'], '', patrow['clausal'],
            patrow['template_comment'],
            patrow['template_comment_type'],
            patrow['primary_implicature'], patrow['other_comment'],
            patrow['other_comment_type'], patrow['domain'],
            patrow['register'], patrow['corpus'], patrow['exploit_comment'],
            patrow['advl_class'], patrow['advl_none'],
            patrow['advl_opt'], patrow['claus_to'],
            patrow['claus_ing'], patrow['claus_that'],
            patrow['claus_wh'], patrow['claus_quote'],
            patrow['claus_opt'], patrow['object_none'],
            patrow['framenet'], patrow['semclass'],
            patrow['sub_conj'])
          db_query2 = "SELECT * FROM implicatures WHERE entry_id=? AND pattern_id=?"
          q2 = dbh.execute(db_query2, fromentry, pattern)
          q2.each{|improw|
            db_query3 = "INSERT INTO implicatures (entry_id, pattern_id, implicature) "\
                "VALUES (?, ?, ?)"
            dbh.do(db_query3, toentry, max, improw['implicature'])
          }
          db_query2 = "SELECT * FROM objects WHERE entry_id=? AND pattern_id=?"
          q2 = dbh.execute(db_query2, fromentry, pattern)
          q2.each{|objrow|
            db_query3 = "INSERT INTO objects (entry_id, pattern_id, position, "\
                "object_type, semantic_type, semantic_role, polarity, "\
                "lexset_show, lexset_items, optional, headword, none, "\
                "advl_function, advl_adverbs, advl_group, advl_main_opt, "\
                "attributes, det_quant, complement_as, complement_det, "\
                "complement_as_optional, adv_comp_opt) "\
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, "\
                "?, ?, ?, ?, ?)"
            dbh.do(db_query3, toentry, max, objrow['position'],
            objrow['object_type'], objrow['semantic_type'],
            objrow['semantic_role'], objrow['polarity'],
            objrow['lexset_show'], objrow['lexset_items'],
            objrow['optional'], objrow['headword'],
            objrow['none'], objrow['advl_function'],
            objrow['advl_adverbs'], objrow['advl_group'],
            objrow['advl_main_opt'], objrow['attributes'],
            objrow['det_quant'], objrow['complement_as'],
            objrow['complement_det'],
            objrow['complement_as_optional'],
            objrow['adv_comp_opt'])
          }
          db_query2 = "UPDATE entries SET last_edit=CURRENT_TIMESTAMP, edit_by=? WHERE entry_id=?"
          dbh.do(db_query2, user, toentry)
          dbh.commit
          # add patstring to DB
          patstr = get_pattern_string(toentry, max)
          patq = "UPDATE patterns SET pattern_string=? WHERE entry_id=? AND pattern_id=?"
          dbh.do(patq, patstr, toentry, max)
          dbh.commit
        }
        return max.to_s
      rescue => e
        $stderr.puts e
        dbh.rollback
        return 'error: ' + e
      end
    end

    def export_corpora
        result = ''
        db_query = "SELECT * FROM patterns ORDER BY entry_id ASC, number ASC"
        q = dbh.execute(db_query)
        q.each{|patrow|
            sub_types = []
            db_query2 = "SELECT * FROM objects WHERE entry_id=? AND pattern_id=? AND object_type='S' ORDER BY position DESC"
            q2 = dbh.execute(db_query2, patrow['entry_id'], patrow['pattern_id'])
            q2.each{|row|
                sub_types << row['semantic_type'] if row['semantic_type'].to_s != ''
            }
            obj_types = []
            db_query2 = "SELECT * FROM objects WHERE entry_id=? AND pattern_id=? AND object_type='O' ORDER BY position DESC"
            q2 = dbh.execute(db_query2, patrow['entry_id'], patrow['pattern_id'])
            q2.each{|row|
                obj_types << row['semantic_type'] if row['semantic_type'].to_s != ''
            }

            result += patrow['entry_id'] + "\t" + patrow['number'].to_s + "\t" + sub_types.join('|') + "\t" + obj_types.join('|') + "\n"
        }
        return result
    end

    def search_onto(word)
        terms = []
        db_query = "SELECT * FROM ontology WHERE term ILIKE '#{word}%'"
        q = dbh.execute(db_query)
        q.each{|row|
            terms << row['tid'].to_s + ':' + row['term']
        }
        return terms.sort
    end

    def store_onto(tid, new_data)
        tid = nil if tid == 'null' or tid == ''
        xmldoc = REXML::Document.new(new_data)
        entry = xmldoc.root
        dbh['AutoCommit'] = false
        begin
            term = entry.elements['term'].text.to_s
            comment = entry.elements['comment'].text.to_s
            if tid != nil
                db_query = "DELETE FROM ontology_links WHERE term=?"
                dbh.do(db_query, tid)
                db_query = "UPDATE ontology SET term=?, comment=? WHERE tid=?"
                dbh.do(db_query, term, comment, tid)
            else
                db_query = "SELECT MAX(tid) FROM ontology"
                q = @dbh.select_one(db_query)
                tid = q[0].to_i + 1
                db_query = "INSERT INTO ontology (tid, term, comment) VALUES (?, ?, ?)"
                dbh.do(db_query, tid, term, comment)
            end

            REXML::XPath.match(xmldoc, '//hypernym').each {|el|
                db_query = "INSERT INTO ontology_links (term, hypernym) VALUES (?, ?)"
                hyper = el.text.to_s
                dbh.do(db_query, tid, hyper)
            }

            dbh.commit
                rescue=>e
                    $stderr.puts e
                    dbh.rollback
        end
        return tid
    end

    def add_hypo(data, hyper_id)
        db_query = "SELECT MAX(tid) FROM ontology"
        q = @dbh.select_one(db_query)
        tid = q[0].to_i + 1
        ar = data.split(":")
        term = ar[0]
        comment = ar[1]
        db_query = "INSERT INTO ontology (tid, term, comment) VALUES (?, ?, ?)"
        dbh.do(db_query, tid, term, comment)
        db_query = "INSERT INTO ontology_links (term, hypernym) VALUES (?, ?)"
        dbh.do(db_query, tid, hyper_id)
        return 'ok'
    end

    def add_hyper(data, hypo_id)
        db_query = "SELECT MAX(tid) FROM ontology"
        q = @dbh.select_one(db_query)
        tid = q[0].to_i + 1
        ar = data.split(":")
        term = ar[0]
        comment = ar[1]
        db_query = "INSERT INTO ontology (tid, term, comment) VALUES (?, ?, ?)"
        dbh.do(db_query, tid, term, comment)
        db_query = "SELECT hypernym FROM ontology_links WHERE term=?"
        q = @dbh.select_one(db_query, hypo_id)
        hyper_id = q[0].to_i
        db_query = "DELETE FROM ontology_links WHERE term=?"
        dbh.do(db_query, hypo_id)
        db_query = "INSERT INTO ontology_links (term, hypernym) VALUES (?, ?)"
        dbh.do(db_query, hypo_id, tid)
        dbh.do(db_query, tid, hyper_id)
        return 'ok'
    end

    def change_hyper(data, hypo_id)
        ar = data.split(":")
        hyper_id = ar[0]
        db_query = "UPDATE ontology_links SET hypernym=? WHERE term=?"
        dbh.do(db_query, hyper_id, hypo_id)
        return 'ok'
    end

    def reportpoponto(data)
        entryfile = File.open('/var/lib/deb-server/files/pop-onto-report.txt', 'a')
        entryfile.write(data+"\n")
        entryfile.close
        return('ok');
    end

    def get_nouns(data, constraints)
        res = []
        param = constraints.split(":")
        type = param[0]
        bncdoc = param[1]
        entry_id = param[2]
        pattern_id = param[3]
        semantic_type = param[4]
        stan_rel = param[5]
        cpa_rel = param[6]
        argument = param[7]
        whereclause = []
        whereclause << " semantic_type ='#{semantic_type}'" if (semantic_type != '' and semantic_type != nil)
        whereclause << " bnc_doc = '#{bncdoc}'" if (bncdoc !='' and bncdoc != nil)
        whereclause << " entry_id = '#{entry_id}'" if (entry_id !='' and entry_id != nil)
        whereclause << " pattern_id = '#{pattern_id}'" if (pattern_id !='' and pattern_id != nil)
        whereclause << " stanford_synt_rel = '#{stan_rel}'" if (stan_rel !='' and stan_rel != nil)
        whereclause << " cpa_synt_rel = '#{cpa_rel}'" if (cpa_rel !='' and cpa_rel != nil)
        whereclause << " (argument LIKE '#{argument}\\_%' or argument = '#{argument}')" if (argument !='' and argument != nil)
        whereclause << " type = '#{type}'" if (type !='' and type != nil)
        #semeval = ['continue', 'operate','crush','apprehend','appreciate','decline','undertake','adapt', 'afflict', 'ask', 'avert', 'begrudge', 'bludgeon', 'boo', 'breeze', 'teeter', 'totter', 'advise', 'ascertain', 'attain', 'avoid', 'belch', 'bluff', 'brag', 'sue', 'tense', 'wing']
        #semeval_clause = "AND entry_id !='"+semeval.join("' AND entry_id !='")+"' "
        #where_clause = " WHERE "+whereclause.join(' AND ')+" "+semeval_clause
        where_clause = " WHERE "+whereclause.join(' AND ')+" "
        if where_clause !~ /=/
          return ''
        end
        $stderr.puts where_clause
        if data == 'syntax'
            db_query = "SELECT argument,cpa_synt_rel,count(*) as frequency FROM pop_ontology #{where_clause} group by argument,cpa_synt_rel order by frequency DESC"
            q=dbh.execute(db_query)
            q.each{|row|
                resrow = row['argument'].to_s + ':' + row['cpa_synt_rel'].to_s + ':' + row['frequency'].to_s
                res << resrow
            }
        elsif data == 'all'
            db_query = "SELECT argument,count(*) as frequency FROM pop_ontology #{where_clause} group by argument order by frequency DESC"
            q=dbh.execute(db_query)
            q.each{|row|
                resrow = row['argument'].to_s + ':' + row['frequency'].to_s
                res << resrow
            }
        elsif data == 'sem'
            db_query = "SELECT semantic_type,count(*) as frequency FROM pop_ontology #{where_clause} group by semantic_type order by frequency DESC"
            q=dbh.execute(db_query)
            q.each{|row|
                resrow = row['semantic_type'].to_s + ':' + row['frequency'].to_s
                res << resrow
            }
        end
        return  res.join("\n") + "\n"
    end

    def delete_onto(tid)
        dbh['AutoCommit'] = false
        begin
            db_query = "SELECT hypernym FROM ontology_links WHERE term=?"
            q = @dbh.select_one(db_query, tid)
            if q.nil?
                db_query = "DELETE FROM ontology_links WHERE hypernym=?"
                dbh.do(db_query, tid)
            else
                hypernym = q[0].to_i
                db_query = "UPDATE ontology_links SET hypernym=? WHERE hypernym=?"
                dbh.do(db_query, hypernym, tid)
            end
            db_query = "DELETE FROM ontology_links WHERE term=?"
            dbh.do(db_query, tid)
            db_query = "DELETE FROM ontology WHERE tid=?"
            dbh.do(db_query, tid)
            dbh.commit
            return 'deleted'
        rescue=>e
            $stderr.puts e
            dbh.rollback
            return 'error'
        end
    end

    def dump_all
        result = "<cpa>\n"
        db_query = "SELECT * FROM entries ORDER BY entry_id ASC"
        q = dbh.execute(db_query)
        q.each{|row|
            result += get(row['entry_id'])
        }
        result += "\n</cpa>"
        return result
    end

    def set_verb_info(entry_id, user, samplesize, semclass, aspclass, comment, difficulty, compilation_time)
        db_query = "SELECT status FROM entries WHERE entry_id=?"
        q = @dbh.select_one(db_query, entry_id)
        if q[0].to_s == 'N'
            db_query = "UPDATE entries SET create_time = CURRENT_TIMESTAMP, create_by=?, status=? WHERE entry_id=?"
            dbh.do(db_query, user, 'W', entry_id)
        end
        dbh['AutoCommit'] = false
        begin
            db_query = "UPDATE entries SET last_edit=CURRENT_TIMESTAMP, "\
                "sample_size=?, semclass=?, aspclass=?, edit_by=?, comment=?, "\
                "difficulty=?, compilation_time=? WHERE entry_id=?"
            dbh.do(db_query, samplesize, semclass, aspclass, user, comment, difficulty, compilation_time, entry_id)
            dbh.commit
        rescue=>e
            $stderr.puts e
            dbh.rollback
        end
        return 'ok'
    end

    def get_verb_info(entry_id)
        db_query = "SELECT * FROM entries WHERE entry_id=?"
        patterns = getnofpats(entry_id)
        q = dbh.execute(db_query, entry_id)
        status = ''
        samplesize = ''
        semclass = ''
        aspclass = ''
        erlangen = false
        comment = ''
        difficulty = ''
        compilation_time = ''
        created_by = ''
        last_edit_by = ''
        created_time = ''
        last_edit_time = ''
        bnc50_freq = ''
        q.each{|row|
            status = row['status'].to_s
            samplesize = row['sample_size'].to_s
            semclass = row['semclass']
            aspclass = row['aspclass']
            erlangen = row['erlangen']
            comment = row['comment']
            difficulty = row['difficulty']
            compilation_time = row['compilation_time']
            created_by = row['create_by']
            last_edit_by = row['edit_by']
            created_time = row['create_time']
            last_edit_time = row['last_edit']
            bnc50_freq = row['bnc50_freq']
        }
        return status, samplesize, semclass, aspclass, erlangen, comment,\
                patterns, difficulty, compilation_time, created_by,\
                created_time, last_edit_by, last_edit_time, bnc50_freq
    end

    def getnofpats(entry_id)
        pat_q = "SELECT COUNT(*) FROM patterns WHERE entry_id=?"
        return @dbh.select_one(pat_q, entry_id)[0].to_i.to_s
    end

    def get_distinct_values(table, column)
        q = "select distinct(#{column}) from #{table}"
        q = dbh.execute(q)
        r = Array.new
        q.each{|row|
            r << row[column]
        }
        return r.join("\n") + "\n"
    end

    def get_hypernyms(term)
        db_query = "SELECT ontology.term as hyper FROM ontology_links, ontology WHERE hypernym=tid AND ontology_links.term=(SELECT tid FROM ontology WHERE term=?)"
        q = dbh.execute(db_query, term)
        ret = []
        q.each{|row|
            ret << row['hyper']
        }
        return ret
    end

    def db_stat()
        cq = "SELECT COUNT(*) FROM entries WHERE status='C'"
        cc = @dbh.select_one(cq)[0].to_i.to_s
        wq = "SELECT COUNT(*) FROM entries WHERE status in ('W', 'R')"
        wc = @dbh.select_one(wq)[0].to_i.to_s
        nq = "SELECT COUNT(*) FROM entries WHERE status='N'"
        nc = @dbh.select_one(nq)[0].to_i.to_s
        lq = "SELECT COUNT(*) FROM entries WHERE status='S'"
        lc = @dbh.select_one(lq)[0].to_i.to_s
        aq = "SELECT COUNT(*) FROM entries"
        ac = @dbh.select_one(aq)[0].to_i.to_s
        return [cc, wc, nc, lc, ac].join("\t")
    end

    def get_entry_stat(query='', filter='-')
        count = {}
        db_query = "SELECT COUNT(*) FROM entries WHERE status NOT IN ('M', 'A', 'Q')"
        q = @dbh.select_one(db_query)
        count['total_entry'] = q[0].to_i
        db_query = "SELECT COUNT(patterns.pattern_id) FROM patterns,entries WHERE entries.entry_id=patterns.entry_id AND status NOT IN ('M','A','Q')"
        q = @dbh.select_one(db_query)
        count['total_pattern'] = q[0].to_i
        db_query = "SELECT COUNT(*) FROM entries WHERE status='C'"
        q = @dbh.select_one(db_query)
        count['comp_entry'] = q[0].to_i
        db_query = "SELECT COUNT(pattern_id) FROM patterns,entries WHERE entries.entry_id=patterns.entry_id AND status='C'"
        q = @dbh.select_one(db_query)
        count['comp_pattern'] = q[0].to_i
        db_query = "SELECT COUNT(*) FROM entries WHERE status IN ('W', 'R')"
        q = @dbh.select_one(db_query)
        count['draft_entry'] = q[0].to_i
        db_query = "SELECT COUNT(pattern_id) FROM patterns,entries WHERE entries.entry_id=patterns.entry_id AND status IN ('R', 'W')"
        q = @dbh.select_one(db_query)
        count['draft_pattern'] = q[0].to_i
        conds = []
        conds << "status='#{filter}'" if (filter != '-' and filter != 'all') 
        conds << "entry_id ILIKE '#{query}%'"  if (query.length > 0) 
        where = ''
        where = 'WHERE ' + conds.join(' AND ') if (conds.size > 0)
        db_query = "SELECT COUNT(*) FROM entries #{where}"
        q = @dbh.select_one(db_query)
        count['filter_entry'] = q[0].to_i

        return count
    end

    def json_pattern(verb, number)
        obj = {}
        obj['pattern'] = {}
        p = obj['pattern']
        p['@id'] = num2id(verb, number).to_s
        p['@num'] = number.to_s
        p['template'] = {}
        t = p['template']
        db_query = "SELECT * FROM patterns WHERE entry_id='#{verb}' AND number=#{number}"
        q = @dbh.select_all(db_query)
        row = q[0]
        p['semclass'] = {}
        if row['semclass'].to_s != ''
            p['semclass']['$'] = row['semclass'].to_s
        end
        p['primary_implicature'] = {}
        if row['primary_implicature'].to_s.gsub(/&/, '&amp;') != ''
            p['primary_implicature']['$'] = row['primary_implicature'].to_s.gsub(/&/, '&amp;')
        end
        p['primary_implicature']['@idiom'] = (row['idiom'] == true)? "true":""
        p['primary_implicature']['@phrasal'] = (row['phrasal'] == true)? "true":""
        t['@adverbial_class'] = row['advl_class'].to_s
        t['@adverbial_opt'] = (row['advl_opt'] == true)? "true":""
        t['@adverbial_none'] = (row['advl_none'] == true)? "true":""
        t['@object_none'] = (row['object_none'] == true)? "true":""
        t['@auxiliary'] = row['auxiliary'].to_s
        t['verb_form'] = {}
        t['verb_form']['$'] = row['verb_form'].to_s
        t['sub_conj'] = {}
        if row['sub_conj'].to_s != ''
            t['sub_conj']['$'] = row['sub_conj'].to_s
        end
        t['subject'] = make_json_object(verb, row['pattern_id'], 'S', 'M')
        if count_alternations(verb, row['pattern_id'], 'S') > 0
            t['subject']['alternation'] = make_json_alt_object(verb, row['pattern_id'], 'S', 'A')
        end
        t['object'] = make_json_object(verb, row['pattern_id'], 'O', 'M')
        if count_alternations(verb, row['pattern_id'], 'O') > 0
            t['object']['alternation'] = make_json_alt_object(verb, row['pattern_id'], 'O', 'A')
        end
        if count_alternations(verb, row['pattern_id'], 'O', 'I') > 0
            t['object']['indirect'] = make_json_ind_object(verb, row['pattern_id'])
        end
        t['clausal'] = {}
        if row['clausal'].to_s != ''
            t['clausal']['$'] = row['clausal'].to_s
        end
        t['oclausals'] = {}
        t['oclausals']['clausal'] = make_json_clausal(verb, row['pattern_id'], 'K')
        if t['oclausals']['clausal'].empty?
            t['oclausals'].delete('clausal')
        end
        t['oclausals']['@optional'] = (row['ocl_opt'] == true)? "true":""
        t['oclausals']['@to'] = (row['ocl_to'] == true)? "true":""
        t['oclausals']['@ing'] = (row['ocl_ing'] == true)? "true":""
        t['oclausals']['@that'] = (row['ocl_that'] == true)? "true":""
        t['oclausals']['@wh'] = (row['ocl_wh'] == true)? "true":""
        t['oclausals']['@quote'] = (row['ocl_quote'] == true)? "true":""
        t['clausals'] = {}
        t['clausals']['clausal'] = make_json_clausal(verb, row['pattern_id'], 'L')
        if t['clausals']['clausal'].empty?
            t['clausals'].delete('clausal')
        end
        t['clausals']['@optional'] = (row['claus_opt'] == true)? "true":""
        t['clausals']['@to'] = (row['claus_to'] == true)? "true":""
        t['clausals']['@ing'] = (row['claus_ing'] == true)? "true":""
        t['clausals']['@that'] = (row['claus_that'] == true)? "true":""
        t['clausals']['@wh'] = (row['claus_wh'] == true)? "true":""
        t['clausals']['@quote'] = (row['claus_quote'] == true)? "true":""
        t['comp'] = make_json_complement(verb, row['pattern_id'])
        if t['comp'].empty?
            t.delete('comp')
        end
        t['adverbial'] = make_json_adverbial(verb, row['pattern_id'])
        if t['adverbial'].empty?
            t.delete('adverbial')
        end
        t['comment'] = {}
        if row['template_comment'].to_s != ''
            t['comment']['$'] = row['template_comment'].to_s
        end
        t['comment']['@type'] = row['template_comment_type'].to_s

        # pattern
        db_query2 = "SELECT * FROM implicatures WHERE entry_id='#{verb}' AND pattern_id=#{row['pattern_id']}"
        q2 = @dbh.execute(db_query2)
        if q2.rows > 1
            p['secondary_implicature'] = []
            q2.each{|row2|
                if row2['implicature'].to_s.gsub(/&/,'&amp;') != ''
                    p['secondary_implicature'] << {'$' => row2['implicature'].to_s.gsub(/&/,'&amp;')}
                end
            }
        elsif q2.rows == 1
            p['secondary_implicature'] = {}
            q2.each{|row2|
                if row2['implicature'].to_s.gsub(/&/,'&amp;') != ''
                    p['secondary_implicature']['$'] = row2['implicature'].to_s.gsub(/&/, '&amp;')
                end
            }
        end
        p['comment'] = {}
        if row['other_comment'].to_s != ''
            p['comment']['$'] = row['other_comment'].to_s
        end
        p['comment']['@type'] = row['other_comment_type'].to_s
        p['domain'] = {}
        if row['domain'].to_s != ''
            p['domain']['$'] = row['domain'].to_s
        end
        p['exploitation'] = {}
        if row['exploit_comment'].to_s != ''
            p['exploitation']['$'] = row['exploit_comment'].to_s
        end
        p['framenet'] = {}
        if row['framenet'].to_s != ''
            p['framenet']['$'] = row['framenet'].to_s
        end
        p['register'] = {}
        if row['register'].to_s != ''
            p['register']['$'] = row['register'].to_s
        end
        p['corpus'] = {}
        if row['corpus'].to_s != ''
            p['corpus']['$'] = row['corpus'].to_s
        end
        return obj
    end

    def gdex_get(verb,patnum)
      q1 = "SELECT json_string from gdex where verb='#{verb}' and pattern='#{patnum}' order by date DESC"
      res = @dbh.select_one(q1)
      return res.to_s
    end

    def gdex_save(verb,patnum,json_string,username,mode)
        auth_users = ['patrick','marc','sara']
        q1 = "SELECT * from gdex where verb='#{verb}' and pattern='#{patnum}'"
        res = @dbh.execute(q1)
        if res.rows==0 or auth_users.include? username #automatically fill if empty
            q1 = dbh.prepare("INSERT into gdex (json_string,username,date,verb,pattern) VALUES (?,?,CURRENT_TIMESTAMP,?,?)")
        else
            return 'not enough privileges to save'
        end
        q1.execute(json_string,username,verb,patnum)
        q1.finish
        entryfile = File.open('/var/log/deb-server/pdev_gdex.log', 'a')
        entryfile.write(Time.now.strftime("%d %B  %Y. %H:%M.")+"\t"+username+"\t"+verb+"\t"+patnum.to_s+"\t"+json_string+"\n")
        entryfile.close
        return dbh.commit
    end

    def get_frames(entry_id)
        q1 = "SELECT frame from framenet where entry_id = '#{entry_id}'"
        res = @dbh.execute(q1)
        ret = []
        res.each{|row|
            ret << row['frame']
        }
        return ret
    end

    def get_types_stat
        count = {}
        db_query = "SELECT DISTINCT semantic_type FROM entries, patterns, "\
                "objects WHERE entries.entry_id=patterns.entry_id AND "\
                "patterns.entry_id=objects.entry_id AND patterns.pattern_id="\
                "objects.pattern_id AND semantic_type IN (SELECT "\
                "TRIM(SUBSTRING(semantic_type, '^[a-zA-Z ]*')) AS t FROM "\
                "objects EXCEPT SELECT term FROM ontology ORDER BY t) AND "\
                "semantic_type != '' AND status!='O' AND object_type IN "\
                "('S', 'O', 'A')"
        q = @dbh.execute(db_query)
        count['notin'] = q.fetch_all.size
        db_query = "SELECT DISTINCT TRIM(SUBSTRING(semantic_type, "\
                "'^[a-zA-Z ]*')) AS type FROM objects,entries WHERE "\
                "entries.entry_id=objects.entry_id AND object_type IN "\
                "('O','S','A') AND semantic_type != ''"
        q = @dbh.execute(db_query)
        count['in'] = q.fetch_all.size - count['notin']

        return count
    end

    def iterate_save_pop_onto
      q1 = "SELECT entry_id FROM entries WHERE status='C' ORDER BY entry_id"
      res = @dbh.execute(q1)
      res.each{|row|
        $stderr.puts row['entry_id']
        save_pop_onto(row['entry_id'], 'bnc50')
      }
    end

    def save_pop_onto(entry_id,corpus)
        #store pdev data in dict
        pdev = {}
        num2rel = {}
        relmap = {} #pp_into: A
        q = "SELECT pattern_id, number from patterns where entry_id ='"+entry_id+"'"
        patterns = @dbh.execute(q)
        patterns.each{|pat|
          #collect arguments
          num = pat['number'].to_s
          q2 = "SELECT * FROM objects WHERE entry_id=? AND pattern_id=? AND object_type IN ('S','O','A') ORDER BY position DESC"
          objs = dbh.execute(q2, entry_id, pat['pattern_id'])
            num2rel[num] = []
            pdev[num] = Hash.new { |hash, key| hash[key] = [] }
            objs.each{|obj|
              if obj['semantic_type'] != nil
                st = obj['semantic_type'].gsub(/[0-9]+/,'').strip
                if obj['object_type'] == 'O' and st != ''
                  pdev[num]['object'].push st
                  num2rel[num] << 'object'
                  relmap['object'] = 'O'
                elsif obj['object_type'] == 'S' and st != ''
                  pdev[num]['subject'].push st
                  num2rel[num] << 'subject'
                  relmap['subject'] = 'S'
                elsif obj['object_type'] == 'A' and obj['headword'] != nil and obj['headword'] != '' and st != ''
                  obj['headword'].split("|").each{|prep|
                    prep.strip!
                    pdev[num]['pp_'+prep.gsub(' ','_')+'-p'].push st
                    num2rel[num] << 'pp_'+prep.gsub(' ','_')+'-p'
                    relmap['pp_'+prep.gsub(' ','_')+'-p'] = 'A'
                  }
                end
              end
            }
        }
        #remove any existing triples
        q3 = ('delete from pop_ontology where entry_id=?')
        dbh.execute(q3, entry_id)

        #get wordsketch and insert into db where relevant
        $ske_url = 'http://www.fanuk.net/bonito/run.cgi/'
        url = $ske_url + 'wsketch?corpname='+ corpus+'&lemma='+entry_id+'&lpos=-v&minfreq=1&minscore=0.0&maxitems=1&sort_ws_columns=f&clustercolls=0&structured=1&showmwlink=0&minsim=0.15&selgrlist=object&selgrlist=pp_%25s&selgrlist=subject;format=json'
        ws = JSON.parse(open(url, :http_basic_authentication=>['guest', 'GG0we5t']).read)
        if ws['Gramrels'] != nil
          ws['Gramrels'].each{|row|
            seek = row['seek']
            ws_rel = row['name']
            urlseek = $ske_url + 'viewattrsx?q=w-'+seek.to_s+';q=g'+entry_id+'-v;pagesize=1000;corpname='+corpus+';lemma='+entry_id+';lpos=-v;annotconc='+entry_id+'-v;setrefs=%3Dbncdoc.id;setattrs=lemma;allpos=all;format=json'
            conc = JSON.parse(open(urlseek, :http_basic_authentication=>['guest', 'GG0we5t']).read)
            conc['Lines'].each{|line|
             #no semantic alternations
              if line['linegroup']=~ /^\d+$/ and num2rel.include? line['linegroup'] and num2rel[line['linegroup']].include? ws_rel and pdev[line['linegroup']][ws_rel].length == 1
                toknum = line['toknum']
                bncdoc = line['Tbl_refs'][0]
                argument =''
                [line['Right'],line['Left']].each{|span|
                  span.each{|words|
                    if words['class'] == 'coll'
                      argument = words['str']
                    end
                  }
                }
                if argument != ''
                  #insert
                  q3 = ('insert into pop_ontology values(\'UNI\',?,?,?,?,?,?,?,?)')
                  dbh.execute(q3, bncdoc, toknum, entry_id, line['linegroup'], pdev[line['linegroup']][ws_rel][0],ws_rel,relmap[ws_rel],argument)
                end
              end
            }
          }
        end
    end

    def normlist(list, shorten=true)
      l = list.to_s.split(/[,|]/)
      s = ''
      if l != nil
        if l.size == 1
          s = l[0].to_s.strip
        elsif l.size == 2
          s = l[0].to_s + ' | ' + l[1].to_s
        elsif shorten
          s = l[0].to_s + ' | ' + l[1].to_s + ' | ...'
        else
          s = l.join(' | ')
        end
      end
      return s
    end

    def num2id(verb, number)
        db_query = "SELECT pattern_id FROM patterns WHERE entry_id=? AND number=?"
        q = @dbh.select_one(db_query, verb, number)
        if q.nil? or q[0].nil?
            return -1
        end
        return q[0].to_i
    end

    def id2num(verb, id)
        db_query = "SELECT number FROM patterns WHERE entry_id=? AND pattern_id=?"
        q = @dbh.select_one(db_query, verb, id)
        if q.nil? or q[0].nil?
            return -1
        end
        return q[0].to_i
    end

end
