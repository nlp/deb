require 'dict/dict-sedna'

class WriteAdmin < AdminDict
  def get_user_dicts(user)
    dicts = {'dict_m'=>[], 'dict_w'=>[], 'dict_r'=>[], 'dict_a'=>[]}
    codes = []
    xquery_to_hash('[dict[starts-with(code,"'+user+'_")]]').each {|id,xml|
      doc = load_xml_string(xml)
      name = ''
      code = doc.find('//dict/code').first.content.to_s
      name = doc.find('//dict/name').first.content.to_s unless doc.find('//dict/name').first.nil?
      dicts['dict_m'] << {'id'=>id, 'code'=>code, 'name'=>name}
      codes << code
    }
    userxml = get('user'+user)
    userdoc = load_xml_string(userxml)
    userdoc.find('/user/services/service[@code="write"]/dict[not(starts-with(@code,"'+user+'_"))]').each{|d|
      id = 'dict'+d['code']
      doc = load_xml_string(get(id))
      name = ''
      code = doc.find('//dict/code').first.content.to_s
      name = doc.find('//dict/name').first.content.to_s unless doc.find('//dict/name').first.nil?
      dicts['dict_'+d['perm']] = [] if dicts['dict_'+d['perm']].nil?
      dicts['dict_'+d['perm']] << {'id'=>id, 'code'=>code, 'name'=>name}
      codes << code
    }
    if not userdoc.find('/user/admin').first.nil? and userdoc.find('/user/admin').first.content == 'true'
      xml = get('servicewrite')
      doc = load_xml_string(xml)
      doc.find('/service/dicts/dict').each{|d|
        dc = d['code']
        id = 'dict'+d['code']
        unless codes.include?(dc)
          xmld = get('dict'+dc)
          docd = load_xml_string(xmld)
          name = ''
          code = docd.find('//dict/code').first.content.to_s
          name = docd.find('//dict/name').first.content.to_s unless docd.find('//dict/name').first.nil?
          dicts['dict_a'] << {'id'=>id, 'code'=>code, 'name'=>name}
        end
      }
    end
    return dicts
  end

  def get_dict_users(dictcode)
    users = []
    xquery_to_hash('[user[services/service[@code="write"]/dict[@code="'+dictcode+'"]]]').each{|id,xml|
      doc = load_xml_string(xml)
      name = ''
      email = ''
      login = doc.find('/user/login').first.content.to_s
      name = doc.find('/user/name').first.content.to_s unless doc.find('/user/name').first.nil?
      email = doc.find('/user/email').first.content.to_s unless doc.find('/user/email').first.nil?
      perm = doc.find('/user/services/service[@code="write"]/dict[@code="'+dictcode+'"]').first['perm']
      users << {'login'=>login, 'name'=>name, 'perm'=>perm, 'email'=>email}
    }
    return users
  end

  def get_dict_info(dictcode)
    xml = get('dict'+dictcode)
    return nil if xml == ''
    doc = load_xml_string(xml)
    name = ''
    schema = ''
    code = doc.find('//dict/code').first.content.to_s
    name = doc.find('//dict/name').first.content.to_s unless doc.find('//dict/name').first.nil?
    schema = doc.find('//dict/schema').first.content.to_s unless doc.find('//dict/schema').first.nil?
    dict_info = {'dict_code'=>code,'dict_name'=>name, 'schema'=>schema}
    return dict_info
  end

  def add_dict_to_service(dictcode)
    xml = get('servicewrite')
    doc = load_xml_string(xml)
    if doc.find('/service/dicts/dict[@code="'+dictcode+'"]').size == 0
      newdict = XML::Node.new('dict')
      newdict['code'] = dictcode
      doc.find('/service/dicts').first << newdict
      update('servicewrite', doc.to_s)
    end
  end

  def remove_dict_from_service(dictcode)
    xml = get('servicewrite')
    doc = load_xml_string(xml)
    if doc.find('/service/dicts/dict[@code="'+dictcode+'"]').size > 0
      doc.find('/service/dicts/dict[@code="'+dictcode+'"]').each{|d|
        d.remove!
      }
      update('servicewrite', doc.to_s)
    end
  end

  def add_user_to_dict(user, dictcode, type='r')
    xml = get('user'+user)
    doc = load_xml_string(xml)
    doc.find('/user/services/service[@code="write"]/dict[@code="'+dictcode+'"]').each{|d|
      d.remove!
    }
    doc.root << XML::Node.new('services') if doc.find('/user/services').size == 0
    if doc.find('/user/services/service[@code="write"]').size == 0
      nservice = XML::Node.new('service')
      nservice['code'] = 'write'
      doc.find('/user/services').first << nservice
    end
    ndict = XML::Node.new('dict')
    ndict['code'] = dictcode
    ndict['perm'] = type
    doc.find('/user/services/service[@code="write"]').first << ndict
    update('user'+user, doc.to_s)
  end

  def remove_user_from_dict(user, dictcode)
    xml = get('user'+user)
    doc = load_xml_string(xml)
    doc.find('/user/services/service[@code="write"]/dict[@code="'+dictcode+'"]').each{|d|
      d.remove!
    }
    update('user'+user, doc.to_s)
  end

  def remove_all_users_from_dict(dictcode)
    xquery_to_hash('[user[services/service[@code="write"]/dict[@code="'+dictcode+'"]]]').each{|id,xml|
      $stderr.puts id
      doc = load_xml_string(xml)
      doc.find('/user/services/service[@code="write"]/dict[@code="'+dictcode+'"]').each{|d|
        d.remove!
      }
      update(id, doc.to_s)
    }
  end

  def template_save(dictcode, name, code, template, default, xslt_path)
    fxsl = File.new(xslt_path+'/'+dictcode+'-'+code+'.xsl', 'w')
    fxsl.puts(template)
    fxsl.close
    xml = get('dict'+dictcode)
    doc = load_xml_string(xml)
    doc.find('/dict/xslts/xslt[file="'+dictcode+'-'+code+'.xsl"]').each{|d|
      d.remove!
    }
    nxslt = XML::Node.new('xslt')
    nxslt << XML::Node.new('name', code)
    nxslt << XML::Node.new('label', name)
    nxslt << XML::Node.new('file', dictcode+'-'+code+'.xsl')
    if default == 'true'
      nxslt['default'] = 'true'
      doc.find('/dict/xslts/xslt|/dict/handlebars/hb').each{|d|
        d['default'] = 'false'
      }
    end
    doc.root << XML::Node.new('xslts') if doc.find('/dict/xslts').first.nil?
    doc.find('/dict/xslts').first << nxslt
    update('dict'+dictcode, doc.to_s)
  end

  def template_remove(dictcode, code, xslt_path)
    File.delete(xslt_path+'/'+dictcode+'-'+code+'.xsl')
    xml = get('dict'+dictcode)
    doc = load_xml_string(xml)
    doc.find('/dict/xslts/xslt[name="'+code+'"]').each{|d|
      d.remove!
    }
    update('dict'+dictcode, doc.to_s)
  end

  def htemplate_save(dictcode, name, code, template, default, xslt_path)
    fxsl = File.new(xslt_path+'/'+dictcode+'-'+code+'.hb', 'w')
    fxsl.puts(template)
    fxsl.close
    xml = get('dict'+dictcode)
    doc = load_xml_string(xml)
    doc.find('/dict/handlebars/hb[file="'+dictcode+'-'+code+'.hb"]').each{|d|
      d.remove!
    }
    nxslt = XML::Node.new('hb')
    nxslt << XML::Node.new('name', code)
    nxslt << XML::Node.new('label', name)
    nxslt << XML::Node.new('file', dictcode+'-'+code+'.hb')
    if default == 'true'
      nxslt['default'] = 'true'
      doc.find('/dict/xslts/xslt|/dict/handlebars/hb').each{|d|
        d['default'] = 'false'
      }
    end
    doc.root << XML::Node.new('handlebars') if doc.find('/dict/handlebars').first.nil?
    doc.find('/dict/handlebars').first << nxslt
    update('dict'+dictcode, doc.to_s)
  end

  def htemplate_remove(dictcode, code, xslt_path)
    File.delete(xslt_path+'/'+dictcode+'-'+code+'.hb')
    xml = get('dict'+dictcode)
    doc = load_xml_string(xml)
    doc.find('/dict/handlebars/hb[name="'+code+'"]').each{|d|
      d.remove!
    }
    update('dict'+dictcode, doc.to_s)
  end

  def get_dict_templates(dictcode, xslt_path)
    templates = []
    xml = get('dict'+dictcode)
    doc = load_xml_string(xml)
    doc.find('/dict/xslts/xslt').each{|xsl|
      xslt = {'code'=>xsl.find('name').first.content}
      xslt['name'] = xsl.find('label').first.content unless xsl.find('label').first.nil?
      file = xsl.find('file').first.content.to_s
      xslt['template'] = File.read(xslt_path+'/'+file)
      xslt['default'] = (xsl['default'].to_s == 'true')? 'true':'false'
      templates << xslt
    }
    return templates
  end
  def get_dict_htemplates(dictcode, xslt_path)
    htemplates = []
    xml = get('dict'+dictcode)
    doc = load_xml_string(xml)
    doc.find('/dict/handlebars/hb').each{|xsl|
      xslt = {'code'=>xsl.find('name').first.content}
      xslt['name'] = xsl.find('label').first.content unless xsl.find('label').first.nil?
      file = xsl.find('file').first.content.to_s
      xslt['template'] = File.read(xslt_path+'/'+file)
      xslt['default'] = (xsl['default'].to_s == 'true')? 'true':'false'
      htemplates << xslt
    }
    return htemplates
  end

  def copy_templates(dictcode, xml)
    oldxml = get('dict'+dictcode)
    if oldxml != ''
      olddoc = load_xml_string(oldxml)
      doc = load_xml_string(xml)
      doc.root << olddoc.find('//xslts').first.copy(true) unless olddoc.find('//xslts').first.nil?
      doc.root << olddoc.find('//handlebars').first.copy(true) unless olddoc.find('//handlebars').first.nil?
      xml = doc.to_s
    end
    return xml
  end

  def get_perm(user, dict)
    return 'm' if dict[0,dict.index('_')] == user
    xml = get('user'+user)
    doc = load_xml_string(xml.to_s)
    return 'm' if doc.root.find('admin[.="true"]').size > 0
    perm_el = doc.root.find("//service[@code='write']/dict[@code='"+dict+"']").first
    if perm_el != nil
      perm = perm_el['perm'].to_s
      return perm 
    else
      return ''
    end
  end

  def find_users(search)
    users = []
    if search.nil?
      query = '[user]'
    else
      search.gsub!('"','')
      search.gsub!("'",'')
      query = '[user[starts-with(login,"'+search+'") or starts-with(email,"'+search+'") or contains(name, "'+search+'")]]'
    end
    xquery_to_hash(query).each{|id,xml|
      doc = load_xml_string(xml)
      name = ''
      email = ''
      login = doc.find('/user/login').first.to_s
      email = doc.find('/user/email').first.to_s unless doc.find('/user/email').first.nil?
      name = doc.find('/user/name').first.to_s unless doc.find('/user/name').first.nil?
      users << {'login'=>login, 'name'=>name, 'email'=>email}
    }
    return users
  end

end
