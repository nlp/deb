require 'dict/dict-sedna'

class Dafn2 < DictSedna
  def initialize(collection, journal_path, key_path='', database = 'deb', admindict = nil)
    super
    @servlet = Dafn2Servlet
    admindict.lock_timeout = 12*60*60 #12 hours
  end

  def build_query_part(query_array)
    queries = []
    if query_array['hw'].to_s != ''
      case query_array['where_hw']
      when 'cont'
        func = 'contains'
      when 'end'
        func = 'ends-with'
      when 'regexp'
        func = 'matches'
      else
        func = 'starts-with'
      end
      queries << "#{func}(name, '#{query_array['hw'].gsub("'", "&apos;")}')"
    end

    if query_array['search'].to_s != ''
      search = query_array['search'].to_s.gsub("'", "&apos;").gsub('(','\(').gsub(')','\)')
      case query_array['where_search']
      when 'prelim'
        queries << "(some $p in defblock/prelim satisfies fn:matches($p, '#{search}'))"
      when 'expl'
        queries << "(some $p in defblock/sense satisfies fn:matches($p, '#{search}'))"
      when 'comment'
        queries << "fn:matches(comment, '#{search}')"
      when 'musings'
        queries << "fn:matches(musings, '#{search}')"
      when 'editors'
        queries << "fn:matches(editors, '#{search}')"
      when 'fore'
        queries << "fn:matches(forenames, '#{search}')"
      when 'history'
        queries << "(some $p in history satisfies fn:matches($p, '#{search}'))"
      when 'all'
        queries << "(fn:matches(musings, '#{search}') or fn:matches(comment, '#{search}') or fn:matches(editors, '#{search}') or (some $p in defblock/sense satisfies fn:matches($p, '#{search}')) or (fn:matches(forenames, '#{search}')) or (some $p in history satisfies fn:matches($p, '#{search}')) or (some $p in defblock/prelim satisfies fn:matches($p, '#{search}')))"
      end
    end

    if query_array['contrib'].to_s != ''
      queries << "(some $a in meta/edit satisfies $a/@author='#{query_array['contrib']}')"
    end
    if query_array['approved'].to_s == 'true'
      queries << "(@approved='true')"
    end
    if query_array['mmcheck'].to_s == 'true'
      queries << "(@mmcheck='true')"
    end

    query_part = '(' + queries.join(' and ') + ')'
    query_part = '(not ' + query_part + ')' if query_array['not'].to_s == 'true'
    if query_array['andor'] != ''
      if query_array['andor'] == 'or'
        query_part = 'or '+query_part
      else
        query_part = 'and '+query_part
      end
    end
    $stderr.puts 'QUERY ' + query_part
    return query_part    
  end

  def get_list(query={})
    authors = {
      'patrick' => 'pwh',
      'kilpatrick' => 'kak',
      'brouwer' => 'lsb',
      'dcole' => 'djc',
      'beider' => 'axb',
      'gentry' => 'exg',
      'freeman' => 'jxf',
      'horace' => 'cxc',
      'kremer' => 'dxk',
      'ryman' => 'lxr',
      'veka' => 'oxv',
      'caffarelli' => 'exc',
      'moldanova' => 'dxm',
      'tomescu' => 'dxt',
      'durco' => 'pxd',
      'lenarcic' => 'sxl',
      'brozovic' => 'dxb',
      'cieslikowa' => 'axc',
      'picard' => 'mxp',
      'marshall' => 'rxm',
      'rcampbell' => 'rjc',
      'taylor' => 'pjt',
      'brady' => 'fxb',
      'marcato' => 'cxm',
      'swoboda' => 'pxs',
      'parkin' => 'dhp',
      'wilmotte' => 'kmw',
      'munro' => 'mxm',
      'boullon' => 'aib',
      'bramwell' => 'esb',
      'salaberri' => 'psz',
      'hardcastle' => 'kph',
      'crook' => 'alc',
      'chetwood' => 'jac',
      'gregory' => 'rkg',
      'berkel' => 'gvb',
      'urvashi' => 'urv',
    }
    search_cond = build_query2(query)
    $stderr.puts search_cond
    list = []
    query = '[entry['+search_cond+']]'
    xquery_to_hash(query).each {|id,xml|
      doc = load_xml_string(xml.to_s)
      root = doc.root
      name = root.find('name').to_a.first.content.to_s
      count = root.find('count').to_a.first.content.to_s unless root.find('count').to_a.first.nil?
      fr_2000 = root.find('freq_2000').to_a.first.content.to_s unless root.find('freq_2000').to_a.first.nil?
      fr_2010 = root.find('freq_2010').to_a.first.content.to_s unless root.find('freq_2010').to_a.first.nil?
      fr_2007 = root.find('freq_2007').to_a.first.content.to_s unless root.find('freq_2007').to_a.first.nil?
      #author = root.find('meta/edit').to_a.first['author'] unless root.find('meta/edit').to_a.first.nil?
      last = ''
      first = ''
      fau = ''
      unless root.find('meta/edit').to_a.first.nil?
        lc = root.find('meta/edit').to_a.first
        if lc['author'].to_s == ''
          fau = 'pwh'
        elsif authors[lc['author'].to_s] != nil
          fau = authors[lc['author'].to_s]
        else
          fau = lc['author'].to_s
        end
        first = fau + '/' + lc['time'].to_s[0..10]
      end
      unless root.find('meta/edit').to_a.last.nil?
        lc = root.find('meta/edit').to_a.last
        if lc['author'].to_s == ''
          lau = 'pwh'
        elsif authors[lc['author'].to_s] != nil
          lau = authors[lc['author'].to_s]
        else
          lau = lc['author'].to_s
        end
        last = lau + '/' + lc['time'].to_s[0..10] unless lau == fau
      end
      appr = 'N'
      appr = 'Y' if root['approved'] == 'true'
      appr = 'Q' if root.find('comment').first != nil and root.find('comment').first.content.to_s.length > 0
      mmch = 'N'
      mmch = 'Y' if root['mmcheck'] == 'true'
      hash = {'id'=>id, 'name'=>name, 'first'=>first, 'count'=>count, 'last'=>last, 'status'=>root['status'], 'fr_2007'=>fr_2007, 'approved'=>appr,  'fr_2000'=>fr_2000, 'mmcheck'=>mmch, 'fr_2010'=>fr_2010}
      list << hash
    }
    $stderr.puts list
    return list
  end

  def get_for_edit(id)
    xml = get(id)
    xml.gsub!('<form>','<b>')
    xml.gsub!('</form>','</b>')
    xml.gsub!('<xref>','<xr>')
    xml.gsub!('</xref>','</xr>')
    doc = load_xml_string(xml.to_s)
    doc.find('//metadata').each{|el|
      el.remove!
    }
    if doc.find('//sense').size == 0
      ndf = XML::Node.new('defblock')
      doc.root << ndf
      ndf << XML::Node.new('sense')
    end
    annot = []
    doc.find('//prelim').each{|el|
      elt = el.to_s
      elt.gsub!(/<prelim[^>]*>/, '')
      elt.gsub!('</prelim>', '')
      el.content = elt.gsub("\n", " ")
    }
    doc.find('//sense').each{|el|
      unless el.find('annotation').to_a.first.nil?
        annot <<  el.find('annotation').to_a.first.content
        el.find('annotation').to_a.first.remove!
      end
      elt = el.to_s
      elt.gsub!(/<sense[^>]*>/, '')
      elt.gsub!('</sense>', '')
      el.content = elt.gsub("\n", " ")
    }
    if doc.find('/entry/annot').first.nil?
      doc.root << XML::Node.new('annot', annot.join(', '))
    end
    doc.find('//forenames|//history').each{|el|
      elt = el.to_s
      elt.gsub!('<forenames>', '')
      elt.gsub!('</forenames>', '')
      elt.gsub!('<forenames/>', '')
      elt.gsub!(/<history[^>]*>/, '')
      elt.gsub!('</history>', '')
      elt.gsub!('&amp;lt;probability&amp;gt;','<prob>')
      elt.gsub!('&amp;lt;/probability&amp;gt;','</prob>')
      elt.gsub!('&lt;probability&gt;','<prob>')
      elt.gsub!('&lt;/probability&gt;','</prob>')
      elt.gsub!('</prob><lang>','</prob> <lang>')
      elt.gsub!('</lang><fn>','</lang> <fn>')
      el.content = elt.gsub("\n", " ")
    }
    xml = doc.to_s
    xml.gsub!('<?xml version="1.0" encoding="UTF-8"?>','')
    $stderr.puts xml
    return xml
  end
  
  def get_perm(user)
    xml = @admindict.get('user'+user)
    doc = REXML::Document.new(xml.to_s)
    perm_el = doc.root.elements["//service[@code='dafn2']/dict[@code='dafn2']"]
    if perm_el != nil
      perm = perm_el.attributes['perm'].to_s
      return perm
    else
      return nil
    end
  end

  def get_cluster(entry_id)
    $stderr.puts '*-'+entry_id
    xml = get(entry_id)

    return [entry_id]
  end

  def get_cluster_html(refs, dict_path='', user='')
    data = ''
    refs.uniq.each{|id|
      if id == '__new__'
        doc = "<entry entry_id='__new__'><name></name><defblock><sense></sense></defblock></entry>"
      else
        xml = get_for_edit(id)
        doc = load_xml_string(xml.to_s)
        doc.root['sortkey'] = id.gsub("'", '__')
        doc.root['sortkey'] = id.gsub("&apos;", '__')
      end
      data += apply_transform('edit-single', doc.to_s, [ ['path', '"'+dict_path+'"'], ['user', '"'+user+'"']  ])
    }
    return data
  end


  def get_edits(id)
    edits = []
    begin
      xml = get(id)
      doc = load_xml_string(xml.to_s)
      doc.find('/entry/meta/edit').each{|el|
        author = el['author'].to_s
        time = el['time'].to_s
        edits << {'time'=>time, 'author'=>author}
      }
    rescue => e
      $stderr.puts e
    end
    return edits
  end

  def add_meta(xml, edits)
    doc = load_xml_string(xml)
    doc.root << meta = XML::Node.new('meta')
    edits.each{|edit|
      meta << ed = XML::Node.new('edit')
      ed['time'] = edit['time']
      ed['author'] = edit['author']
    }    
    return doc.to_s
  end


  def move_entry(fromdb, todb, entry_id, force=false)
    begin
      entryxml = fromdb.get(entry_id)
    rescue
      $stderr.puts 'Error: entry '+entry_id+' not existing in source database'
      return 'Error: entry '+entry_id+' not existing in source database'
    end

    if force == false
      begin
        toxml = todb.get(entry_id)
        if toxml.to_s != ''
          $stderr.puts 'Error: entry '+entry_id+' exists in target database'
          #return 'Error: entry '+entry_id+' exists in target database'
          return 'in target'
        end
      rescue
      end
    end

    begin
      $stderr.puts 'save to target'
      todb.update(entry_id, entryxml)
      $stderr.puts 'saved'
      $stderr.puts 'delete from source'
      fromdb.delete(entry_id)
      $stderr.puts 'deleted'
    rescue => e
      $stderr.puts 'Error: unexpected error: '+e.message+"\n"+e.backtrace.join("\n")
      return 'Error: unexpected error: '+e.message
    end

    return 'entry moved'
  end

  def replace(find, replace, where)
    #update db entries
    search = find.to_s.gsub("'", '&apos;')
    case where
      when 'expl'
        query = "[entry[(some $p in defblock/sense satisfies contains($p, '#{search}'))]]"
      when 'comment'
        query = "[entry[contains(comment, '#{search}')]]"
      when 'musings'
        query = "[entry[contains(musings, '#{search}')]]"
      when 'editors'
        query = "[entry[contains(editors, '#{search}')]]"
      when 'fore'
        query = "[entry[contains(forenames, '#{search}')]]"
      when 'history'
        query = "[entry[(some $p in history satisfies contains($p, '#{search}'))]]"
    else
      query = "[entry[(some $p in defblock/sense satisfies contains($p, '#{search}')) or contains(comment, '#{search}') or contains(musings, '#{search}') or contains(editors, '#{search}') or contains(forenames, '#{search}') or (some $p in history satisfies contains($p, '#{search}'))]]"
    end
    $stderr.puts query
    count = 0
    count_r = 0
    xquery_to_hash(query).each{|key,xml|
      $stderr.puts key 
      #$stderr.puts xml
      doc = load_xml_string(xml.to_s)
      search = find.to_s.gsub('"','&quot;')
      if where == 'expl' or where == 'all'
        doc.find("//defblock/sense[contains(.,\"#{search}\")]").each{|c|
          newt = c.to_s.gsub(find, replace).gsub(/<sense[^>]*>/,'').gsub('</sense>','')
          c.content = newt
          count_r += 1 
        }
      end
      if where == 'comment' or where == 'all'
        doc.find("//comment[contains(.,\"#{search}\")]").each{|c|
          newt = c.to_s.gsub(find, replace).gsub(/<comment[^>]*>/,'').gsub('</comment>','')
          c.content = newt
          count_r += 1 
        }
      end
      if where == 'musings' or where == 'all'
        doc.find("//musings[contains(.,\"#{search}\")]").each{|c|
          newt = c.to_s.gsub(find, replace).gsub(/<musings[^>]*>/,'').gsub('</musings>','')
          c.content = newt
          count_r += 1 
        }
      end
      if where == 'editors' or where == 'all'
        doc.find("//editors[contains(.,\"#{search}\")]").each{|c|
          newt = c.to_s.gsub(find, replace).gsub(/<editors[^>]*>/,'').gsub('</editors>','')
          c.content = newt
          count_r += 1 
        }
      end
      if where == 'fore' or where == 'all'
        doc.find("//forenames[contains(.,\"#{search}\")]").each{|c|
          newt = c.to_s.gsub(find, replace).gsub(/<forenames[^>]*>/,'').gsub('</forenames>','')
          c.content = newt
          count_r += 1 
        }
      end
      if where == 'history' or where == 'all'
        doc.find("//history[contains(.,\"#{search}\")]").each{|c|
          newt = c.to_s.gsub(find, replace).gsub(/<history[^>]*>/,'').gsub('</history>','')
          c.content = newt
          count_r += 1 
        }
      end

      data = doc.root.to_s
        data.gsub!('&lt;i&gt;', '<i>')
        data.gsub!('&lt;/i&gt;', '</i>')
        data.gsub!('&lt;b&gt;', '<b>')
        data.gsub!('&lt;/b&gt;', '</b>')
        data.gsub!('&lt;bi&gt;', '<bi>')
        data.gsub!('&lt;/bi&gt;', '</bi>')
        data.gsub!('&lt;xr&gt;', '<xr>')
        data.gsub!('&lt;/xr&gt;', '</xr>')
        data.gsub!(/&lt;xref refurl=&quot;([0-9]*)&quot;&gt;/, '<xref refurl="\1">')
        data.gsub!(/&lt;render img="([0-9]*)"\/&gt;/, '<render img="\1"/>')
        data.gsub!('&lt;/xref&gt;', '</xref>')
        data.gsub!('&lt;xref&gt;', '<xref>')
        data.gsub!('&lt;form&gt;', '<form>')
        data.gsub!('&lt;/form&gt;', '</form>')
        data.gsub!('&lt;probability&gt;', '<probability>')
        data.gsub!('&lt;/probability&gt;', '</probability>')
        data.gsub!('&lt;forename&gt;', '<forename>')
        data.gsub!('&lt;/forename&gt;', '</forename>')
        data.gsub!('&lt;annotation&gt;', '<annotation>')
        data.gsub!('&lt;/annotation&gt;', '</annotation>')
        data.gsub!('&lt;regionalStats&gt;', '<regionalStats>')
        data.gsub!('&lt;/regionalStats&gt;', '</regionalStats>')
        data.gsub!('&lt;in&gt;', '<in>')
        data.gsub!('&lt;/in&gt;', '</in>')
        data.gsub!('&lt;ini&gt;', '<ini>')
        data.gsub!('&lt;/ini&gt;', '</ini>')
        data.gsub!('&lt;su&gt;', '<su>')
        data.gsub!('&lt;/su&gt;', '</su>')
        data.gsub!('&lt;sui&gt;', '<sui>')
        data.gsub!('&lt;/sui&gt;', '</sui>')
        data.gsub!('&lt;sc&gt;', '<sc>')
        data.gsub!('&lt;/sc&gt;', '</sc>')
        data.gsub!('&lt;fr&gt;', '<fr>')
        data.gsub!('&lt;/fr&gt;', '</fr>')
        data.gsub!('&lt;fn&gt;', '<fn>')
        data.gsub!('&lt;/fn&gt;', '</fn>')
        data.gsub!('&lt;prob&gt;', '<prob>')
        data.gsub!('&lt;/prob&gt;', '</prob>')
        data.gsub!('&lt;lang&gt;', '<lang>')
        data.gsub!('&lt;/lang&gt;', '</lang>')
      $stderr.puts data
      
      update(key, data, false)
      count += 1
    }
    return count, count_r
  end

  def get_xr_from(id)
    xml = get(id)
    doc = load_xml_string(xml.to_s)
    xar = []
    doc.root.find('//sense//xref|//sense//xr').each{|x|
      xar << x.content.to_s
    }
    xar.uniq!
    xr = {}
    xar.each{|x|
      xr[x] = ''
      xquery_to_hash('[entry[name="'+x+'"]]').each{|id,xml|
        xr[x] = id
      }
    }
    $stderr.puts xr
    return xr
  end
  def get_xr_to(id)
    xml = get(id)
    doc = load_xml_string(xml.to_s)
    name = doc.find('//entry/name').first.content.to_s
    xr = {}
    xquery_to_hash('[entry[.//sense//xref="'+name+'" or .//sense//xr="'+name+'"]]').each{|eid,xmlx|
      docx = load_xml_string(xmlx)
      namex = docx.find('//entry/name').first.content.to_s
      xr[namex] = eid
    }
    $stderr.puts xr
    return xr
  end


  def query_to_array(query_array)
    queries = []

    qpart = {}
    qpart['andor'] = ''
    qpart['hw'] = query_array['hw']
    qpart['where_hw'] = query_array['where_hw']
    qpart['search'] = query_array['search']
    qpart['where_search'] = query_array['where_search']
    qpart['contrib'] = query_array['contrib']
    qpart['approved'] = query_array['approved']
    qpart['mmcheck'] = query_array['mmcheck']
    queries << qpart

    part_count = 2
    while not query_array['hw'+part_count.to_s].nil?
      qpart = {}
      qpart['andor'] = query_array['andor'+part_count.to_s]
      qpart['not'] = query_array['not'+part_count.to_s]
      qpart['hw'] = query_array['hw'+part_count.to_s]
      qpart['where_hw'] = query_array['where_hw'+part_count.to_s]
      qpart['search'] = query_array['search'+part_count.to_s]
      qpart['where_search'] = query_array['where_search'+part_count.to_s]
      qpart['contrib'] = query_array['contrib'+part_count.to_s]
      qpart['approved'] = query_array['approved'+part_count.to_s]
      qpart['mmcheck'] = query_array['mmcheck'+part_count.to_s]
      qpart['hw'] = '' if qpart['hw'] == 'surname...'
      qpart['search'] = '' if qpart['search'] == 'search...'
      queries << qpart

      part_count += 1 
    end
    return queries
  end
  def build_query2(query_array)
    query_param = query_to_array(query_array)
    queries = []
    query_param.each{|qrow|
      queries << build_query_part(qrow)
    }
    $stderr.puts queries.join(' ')
    return queries.join(' ')
  end

  def approve(entryid)
    xml = get(entryid)
    doc = load_xml_string(xml.to_s)
    if doc.root['approved'] != 'true' and (doc.root.find('comment').first.nil? or doc.root.find('comment').first.content == '')
      doc.root['approved'] = 'true'
      $stderr.puts doc
      update(entryid, doc.to_s)
      return 1
    end
    return 0
  end
end
