require 'dict/dict-sedna'

class CZJ < DictSedna
  attr_accessor :loaded_cookie
  attr_accessor :locale

  def initialize(collection, journal_path, key_path='', database = 'deb', admindict = nil)
    super
    @servlet = CZJServlet
    @loaded_cookie = []
    @locale = parse_locale
    @slovni_druhy = {
      'subst'=>'podstatné jméno',
      'adj'=>'přídavné jméno',
      'pron'=>'zájmeno',
      'num'=>'číslovka',
      'verb'=>'sloveso',
      'adv'=>'příslovce',
      'prep'=>'předložka',
      'ust'=>'ustálený předložkový výraz',
      'kon'=>'spojka',
      'par'=>'částice',
      'int'=>'citoslovce',
      'ustalene'=>'ustálené slovní spojení',
      'prislovi'=>'přísloví',
      'porekadlo'=>'pořekadlo',
      'frazem'=>'frazém',
      'sprezka'=>'příslovečná spřežka',
      'prirovnani'=>'přirovnání',
    }
    @slovni_druhy_en = {
      'subst'=>'noun',
      'adj'=>'adjective',
      'pron'=>'pronoun',
      'num'=>'number',
      'verb'=>'verb',
      'adv'=>'adverb',
      'prep'=>'preposition',
      'ust'=>'set prepositional collocation',
      'kon'=>'conjunction',
      'par'=>'particle',
      'int'=>'interjection',
      'ustalene'=>'set collocation',
      'prislovi'=>'proverb',
      'porekadlo'=>'adage',
      'frazem'=>'phraseme',
      'sprezka'=>'adverbial compound',
      'prirovnani'=>'simile',
    }
  end

  def get_signs(swstring)
    swa = swstring.split('_')
    swa.map!{|e| e.gsub(/\([^\)]*\)/,'')}
    return swa.sort!
  end

  def from_fsw(fswstring, symboldict)
    swa = []
    maxx = 0
    maxy = 0
    match = /M([0-9]*)x([0-9]*)(S.*)/.match(fswstring)
    unless (match.nil? or match[1].nil? or match[2].nil? or match[3].nil?)
      maxx = match[1].to_i
      maxy = match[2].to_i
      match[3].split('S').each{|fs|
        next if fs == ''
        bs = fs[0..2].to_i(16)
        fil = fs[3..3].to_i(16)+1
        rot = fs[4..4].to_i(16)+1
        $stderr.puts fs.to_s + ' = ' + bs.to_s + '=' + fil.to_s + '=' + rot.to_s
        res = symboldict.xquery_to_hash('[symbol[@bs_code="'+bs.to_s+'" and @fill="'+fil.to_s+'" and @rot="'+rot.to_s+'"]]')
        next if res.first.nil?
        swid, xml = res.first
        $stderr.puts swid
        match2 = /([0-9]*)x([0-9]*)/.match(fs[5..-1])
        x = match2[1].to_i - 500
        y = match2[2].to_i - 500
        swpos = ''
        if x != 0 or y != 0
          swpos = '('
          swpos += 'x'+x.to_s if x != 0
          swpos += 'y'+y.to_s if y != 0
          swpos += ')'
        end
        swa << swid.to_s + swpos
      }
    end
    swstring = swa.join('_')
    return swstring
  end

  def get_fsw(swstring, symboldict)
    fsw = 'M500x500'
    swa = []
    swstring.split('_').each{|e|
      match = /([0-9]*)(\(.*\))?/.match(e)
      unless match[1].nil?
        info = {'id'=>match[1], 'x'=>0, 'y'=>0}
        unless match[2].nil?
          if match[2].include?('x') and match[2].include?('y')
            match2 = /\(x([\-0-9]*)y([\-0-9]*)\)/.match(match[2])
            info['x'] = match2[1].to_i
            info['y'] = match2[2].to_i
          elsif match[2].include?('x')
            info['x'] = match[2].gsub(/[^0-9^-]/,'').to_i
          else
            info['y'] = match[2].gsub(/[^0-9^-]/,'').to_i
          end
        end
        swa << info
      end
    }
    swa.each{|info|
      xml = symboldict.get(info['id'])
      doc = load_xml_string(xml).root
      $stderr.puts info
      $stderr.puts doc['bs_code'] + '-' + doc['fill'] + '-' + doc['rot']
      $stderr.puts doc['bs_code'].to_i.to_s(16) + '-' + (doc['fill'].to_i-1).to_s(16) + '-' + (doc['rot'].to_i-1).to_s(16)
      fsw += 'S' + doc['bs_code'].to_i.to_s(16) + (doc['fill'].to_i-1).to_s(16) + (doc['rot'].to_i-1).to_s(16) + (info['x']+500).to_s + 'x' + (info['y']+500).to_s
    }
    return fsw
  end

  def translate_allcs(search_term, search_czj, czjdict, perm='ro', page=0, user_skupiny='', is_public=false, deklin=true, target="czj", extend_translation=false)
    page_entries = 20
    list = []
    if extend_translation
      source_query = 'for $x in collection("'+@dictcode+'")/entry where ($x/lemma/completeness!="1") and $x/lemma/lemma_type!="collocation"
      and count($x/meanings/meaning/relation[@target="'+target+'" and @status="published" and ((@auto_complete="1" and @front_video="published") or not(@auto_complete))])>0
      return <e><i>{data($x/@id)}</i><d>'+@dictcode+'</d><s>{lower-case($x/lemma/title)}</s><t>lemma</t></e>'
    else
      source_query = 'for $x in collection("'+@dictcode+'")/entry where ($x/lemma/completeness!="1") and $x/lemma/lemma_type!="collocation"
      and count($x/meanings/meaning/relation[@target="'+target+'" and @status="published" and @auto_complete="1" and @front_video="published"])>0
      return <e><i>{data($x/@id)}</i><d>'+@dictcode+'</d><s>{lower-case($x/lemma/title)}</s><t>lemma</t></e>'
    end
    usg_query = 'for $x in collection("'+@dictcode+'")/entry where ($x/lemma/completeness!="1") and (some $r in $x/meanings/meaning/usages/usage satisfies (($r/../is_translation_unknown!="1" or not($r/../is_translation_unknown)) and  $r/relation/@type="translation" and $r/relation/@target="'+target+'" and $r/status="published")) order by lower-case($x/lemma/title) return <e><i>{data($x/@id)}</i><d>'+@dictcode+'</d><s>{($x/meanings/meaning/usages/usage[relation[@type="translation" and @target="'+target+'"] and (../is_translation_unknown!="1" or not(../is_translation_unknown)) and text and status="published"]/text/text())}</s><t>usg</t></e>'
    text_query = 'for $x in collection("'+target+'")/entry where ($x/lemma/lemma_type!="collocation" or not($x/lemma/lemma_type)) and ($x/lemma/@auto_complete="1" or $x/lemma/completeness="2" or $x/lemma/completeness="100" or $x/media/file[location=$x/lemma/video_front and status="published"]) and (some $r in $x/meanings/meaning/relation[@type="translation" and @status="published" and @target="'+@dictcode+'"] satisfies (($r/../is_translation_unknown!="1" or not($r/../is_translation_unknown)) and  not(fn:matches($r/@meaning_id,"^[0-9]*-[0-9]+(_us[0-9]*)?$")) )) return <e><i>{data($x/@id)}</i><d>'+target+'</d><s>{data($x/meanings/meaning/relation[@type="translation" and @target="'+@dictcode+'" and @status="published" and  not(fn:matches(@meaning_id,"^[0-9]*-[0-9]+(_us[0-9]*)?$"))]/@meaning_id)}</s><t>text</t></e>'

    if extend_translation
      search_query = 'let $dsource:='+source_query+'
      let $dtext:='+text_query+'
      for $entry in subsequence(for $p in (($dsource,$dtext)) order by lower-case($p/s) return $p, '+(1+page*page_entries).to_s+', '+page_entries.to_s+') return $entry'
      $stderr.puts search_query
      count_query = 'count(let $dsource:='+source_query+'
      let $dtext:='+text_query+'
      for $p in ($dsource,$dtext) return $p)'
    else
      search_query = 'let $dsource:='+source_query+'
      for $entry in subsequence(for $p in (($dsource)) order by lower-case($p/s) return $p, '+(1+page*page_entries).to_s+', '+page_entries.to_s+') return $entry'
      $stderr.puts search_query
      count_query = 'count(let $dsource:='+source_query+'
      for $p in ($dsource) return $p)'
    end
    count = @sedna.query(count_query).first.to_i
    
    query = search_query
    $stderr.puts query
    ids = []
    words = {}
    @sedna.query(query).each{|entry|
      $stderr.puts entry
      id = /<i>(\d*)<\/i>/.match(entry.to_s)[1]
      dict = /<d>(\w*)<\/d>/.match(entry.to_s)[1]
      type = /<t>(\w*)<\/t>/.match(entry.to_s)[1]
      next if ids.include?(dict+id)
      xml = @admindict.array[dict.to_s].get(id.to_s)
      doc = load_xml_string(xml)
      ids << dict+id
      if dict == @dictcode and type == 'lemma'
        info = {'id'=>id.to_s}
        info['title'] = doc.root.find('/entry/lemma/title').to_a.first.content.to_s
        info['title_var'] = doc.root.find('/entry/lemma/title_var').to_a.first.content.to_s if doc.root.find('/entry/lemma/title_var').first != nil and doc.root.find('/entry/lemma/title_var').to_a.first.content.to_s != ''
        if info['title_var'] != nil and not info['title'].start_with?(search_term) and info['title_var'].start_with?(search_term)
          tem = info['title']
          info['title'] = info['title_var']
          info['title_var'] = tem
        end
        words[info['title']] = 0 if words[info['title']].nil?
        words[info['title']] += 1
        variants = doc.root.find('/entry/lemma/grammar_note/variant|/entry/lemma/style_note/variant')
        variants.each{|v|
          info['variant'] = [] if info['variant'].nil?
          hv = {'var'=>v['title']}
          hv['last'] = '1' if v == variants.last
          info['variant'] << hv
        }
        info['lemma_type'] = 'single'
        info['lemma_type'] = doc.root.find('/entry/lemma/lemma_type').first.content.to_s unless doc.root.find('/entry/lemma/lemma_type').first.nil?
        info['is_colloc'] = '1' if info['lemma_type'] == 'collocation'
        info['region'] = doc.root.find('/entry/lemma/grammar_note').first['region'].to_s unless doc.root.find('/entry/lemma/grammar_note').first.nil?
        info['region_sort'] = (info['region']=='cr'?3:0) + (info['region']=='cechy'?2:0) + (info['region']=='morava'?1:0) 
        info['slovni_druh'] = @slovni_druhy[doc.root.find('/entry/lemma/grammar_note').first['slovni_druh'].to_s].to_s unless doc.root.find('/entry/lemma/grammar_note').first.nil?
        info['slovni_druh_en'] = @slovni_druhy_en[doc.root.find('/entry/lemma/grammar_note').first['slovni_druh'].to_s].to_s unless doc.root.find('/entry/lemma/grammar_note').first.nil?
        info['auto_complete'] = '1' if doc.root.find('/entry/lemma').first['auto_complete'] == '1'
        trans_words = []
        $stderr.puts id
        doc.root.find('/entry/meanings/meaning/relation[@target="'+target+'" and @type="translation" and (@completeness!="1" or not(@completeness))]').each {|tran|
          info['translations'] = [] if info['translations'].nil?
          # info['translations'] << {'trid'=>tran['meaning_id']}
          if tran['lemma_id'].to_s != ''
            if target == 'cs' or target == 'en'
              info2 = {'id'=>tran['lemma_id']}
              info2['title'] = tran.find('title').first.content.to_s unless tran.find('title').first.nil?
              info2['title_dia'] = tran.find('title_dia').first.content.to_s unless tran.find('title_dia').first.nil?
              info2['is_text'] = '1'
              trans_words << info2['title']
            else
              info2, coll2 = parse_entry_search(tran['lemma_id'], load_xml_string(@admindict.array[target].get(tran['lemma_id'])), 'admin', {}, 'text')
              info2['is_sign'] = '1'
            end
            info2['preklad_neznamy'] = '1' if not tran.parent.find('is_translation_unknown').first.nil? and tran.parent.find('is_translation_unknown').first.content == '1'
            info2['dict'] = target
            info2['text_link'] = '1' if perm == 'ro' and not tran['auto_complete'] == '1'
            info['translations'] << info2
          elsif not(tran['meaning_id'].to_s =~ /[0-9]*-[0-9]/) and not trans_words.include?(tran['meaning_id'].to_s)
            info['translations'] << {'text_link'=>'1', 'title'=>tran['meaning_id'].to_s, 'is_text'=>'1'}
          end
        }
        info['translations'].uniq! unless info['translations'].nil?
        info['czj'] = 0
        list << info
      end
      if dict == @dictcode and type == 'usg'
        entry_ac = doc.find('/entry/lemma').first['auto_complete']
        doc.find('/entry/meanings/meaning/usages/usage/relation[@type="translation" and @target="'+target+'"]').each{|usg|
          next if usg.find('../text').first.nil?
          usgtran = usg.find('../text').first.content.to_s
          info = {'title'=>usgtran, 'id'=>id, 'translations'=>[]}
          if usg['lemma_id'].to_s != ''
            if target == 'cs' or target == 'en'
              info2 = {'id'=>tran['lemma_id']}
              info2['title'] = usg.find('translation').first.content.to_s unless usg.find('translation').first.nil?
              info2['title_dia'] = strip_dia(info2['title'])
              info2['is_text'] = '1'
            else
              info2, coll2 = parse_entry_search(usg['lemma_id'], load_xml_string(@admindict.array[target].get(usg['lemma_id'])), 'admin', {}, 'text')
              info2['is_sign'] = '1'
            end
            info2['dict'] = target
            info2['text_link'] = '1' if perm == 'ro' and (not usg['auto_complete'] == '1')
            info['translations'] << info2
            info['text_link'] = '1'
          end
          if list.select{|l| l['title'] == info['title']}.size == 0
            list << info
          else
            info_up = list.select{|l| l['title'] == info['title']}.first
            info_up['translations'] << info2
            info_up['translations'].uniq!
          end
        }
      end
      if dict == target
        entry_ac = doc.find('/entry/lemma').first['auto_complete']
        doc.find('/entry/meanings/meaning/relation[@type="translation" and @target="'+@dictcode+'"]').each{|rel|
          next if rel['meaning_id'].to_s =~ /[0-9]*-[0-9]/
          next unless rel['meaning_id'].downcase_utf8 =~ /(^| )#{search_term.downcase_utf8}/
          info = {'title'=>rel['meaning_id'].to_s, 'text_link'=>'1'}
          info['preklad_neznamy'] = '1' if not rel.parent.find('is_translation_unknown').first.nil? and rel.parent.find('is_translation_unknown').first.content == '1'
          if target == 'cs' or target == 'en'
            info2 = {'id'=>id}
            info2['title'] = doc.find('/entry/lemma/title').first.content.to_s unless doc.find('/entry/lemma/title').first.nil?
            info2['title_dia'] = doc.find('/entry/lemma/title_dia').first.content.to_s unless doc.find('/entry/lemma/title_dia').first.nil?
            #info2['title'] = tran.find('title').first.content.to_s unless tran.find('title').first.nil?
            info2['is_text'] = '1'
          else
            info2, coll2 = parse_entry_search(id, load_xml_string(xml), 'admin', {}, 'text')
            info2['is_sign'] = '1'
          end
          info2['dict'] = target
          info2['text_link'] = '1' if perm == 'ro' and not rel['auto_complete'] == '1' and not entry_ac == '1'
          info['translations'] = [info2]
          if list.select{|l| l['title'] == info['title']}.size == 0
            list << info
          else
            info_up = list.select{|l| l['title'] == info['title']}.first
            info_up['translations'] = [] if info_up['translations'].nil?
            info_up['translations'] << info2
            info_up['translations'].uniq!
          end
        }
      end
    }
    list.each{|h|
      if words[h['title']] != nil and words[h['title']] == 1
        h.delete('slovni_druh')
      end
    }
    return list,count,count
  end

  def translate_cs(search_term, search_czj, czjdict, perm='ro', page=0, user_skupiny='', is_public=false, deklin=true, target="czj")
    page_entries = 20
    list = []
    search_parts = [
      'fn:matches(lower-case($x/lemma/title), "(^| )'+search_term.downcase_utf8+'")',
      'fn:matches($x/lemma/title_var, "(^| )'+(search_term.downcase_utf8)+'")',
      'fn:matches($x/lemma/title_dia, "(^| )'+(search_term.downcase_utf8)+'")',
      '(some $v in $x/lemma/grammar_note/variant|$x/lemma/style_note/variant satisfies lower-case($v)="'+search_term.downcase_utf8+'")'
    ]
    usg_search = '(some $r in $x/meanings/meaning/usages/usage satisfies $r/relation/@type="translation" and $r/relation/@target="'+target+'" and fn:matches(lower-case($r/text), "(^| )'+search_term.downcase_utf8+'"))'
    search_parts << '(some $g in $x/lemma/gram/form satisfies fn:matches(lower-case($g), "(^| )'+search_term.downcase_utf8+'"))' if deklin
    tranunknown = '($r/../is_translation_unknown!="1" or not($r/../is_translation_unknown)) and '
    if perm == 'ro' and not user_skupiny.include?('all')
      source_query = 'for $x in collection("'+@dictcode+'")/entry where ($x/lemma/completeness!="1") 
      and (count($x/meanings/meaning/relation[@target="'+target+'"])>0
      and ('+search_parts.join(' or ')+'))
      return <e><i>{data($x/@id)}</i><d>'+@dictcode+'</d><s>{lower-case($x/lemma/title)}</s><t>lemma</t></e>'
      usg_query = 'for $x in collection("'+@dictcode+'")/entry where ($x/lemma/completeness!="1") and (some $r in $x/meanings/meaning/usages/usage satisfies '+tranunknown+' $r/relation/@type="translation" and $r/relation/@target="'+target+'" and fn:matches(lower-case($r/text), "(^| )'+search_term.downcase_utf8+'")) order by lower-case($x/lemma/title) return <e><i>{data($x/@id)}</i><d>'+@dictcode+'</d><s>{($x/meanings/meaning/usages/usage[relation[@type="translation" and @target="'+target+'" and fn:matches(lower-case(../text), "(^| )'+search_term.downcase_utf8+'")]]/text/text())}</s><t>usg</t></e>' 
      text_query = 'for $x in collection("'+target+'")/entry where ($x/lemma/@auto_complete="1" or $x/lemma/completeness="2" or $x/lemma/completeness="100" or $x/media/file[location=$x/lemma/video_front and status="published"]) and (some $r in $x/meanings/meaning/relation[@type="translation" and @status="published" and @target="'+@dictcode+'"] satisfies ('+tranunknown+' not(fn:matches($r/@meaning_id,"^[0-9]*-[0-9]$")) and fn:matches(lower-case($r/@meaning_id), "(^| )'+search_term.downcase_utf8+'"))) return <e><i>{data($x/@id)}</i><d>'+target+'</d><s>{data($x/meanings/meaning/relation[@type="translation" and @target="'+@dictcode+'" and fn:matches(lower-case(@meaning_id), "(^| )'+search_term.downcase_utf8+'")]/@meaning_id)}</s><t>text</t></e>'
    else
      source_query = 'for $x in collection("'+@dictcode+'")/entry where 
      (count($x/meanings/meaning/relation[@target="'+target+'"])>0
      and ('+search_parts.join(' or ')+'))
      return <e><i>{data($x/@id)}</i><d>'+@dictcode+'</d><s>{lower-case($x/lemma/title)}</s><t>lemma</t></e>'
      usg_query = 'for $x in collection("'+@dictcode+'")/entry where (some $r in $x/meanings/meaning/usages/usage satisfies '+tranunknown+' $r/relation/@type="translation" and $r/relation/@target="'+target+'" and fn:matches(lower-case($r/text), "(^| )'+search_term.downcase_utf8+'")) order by lower-case($x/lemma/title) return <e><i>{data($x/@id)}</i><d>'+@dictcode+'</d><s>{($x/meanings/meaning/usages/usage[relation[@type="translation" and @target="'+target+'" and fn:matches(lower-case(../text), "(^| )'+search_term.downcase_utf8+'")]]/text/text())}</s><t>usg</t></e>' 
      text_query = 'for $x in collection("'+target+'")/entry where (some $r in $x/meanings/meaning/relation[@type="translation" and @target="'+@dictcode+'"] satisfies ('+tranunknown+' not(fn:matches($r/@meaning_id,"^[0-9]*-[0-9]$")) and fn:matches(lower-case($r/@meaning_id), "(^| )'+search_term.downcase_utf8+'"))) return <e><i>{data($x/@id)}</i><d>'+target+'</d><s>{data($x/meanings/meaning/relation[@type="translation" and @target="'+@dictcode+'" and fn:matches(lower-case(@meaning_id), "(^| )'+search_term.downcase_utf8+'")]/@meaning_id)}</s><t>text</t></e>'
    end

    search_query = 'let $dsource:='+source_query+'
    let $dusg:='+usg_query+'
    let $dtext:='+text_query+'
    for $p in (($dsource,$dusg,$dtext)) order by $p/t,lower-case($p/s) return $p'
    
    query = search_query
    $stderr.puts query
    ids = []
    words = {}
    @sedna.query(query).each{|entry|
      $stderr.puts entry
      id = /<i>(\d*)<\/i>/.match(entry.to_s)[1]
      dict = /<d>(\w*)<\/d>/.match(entry.to_s)[1]
      type = /<t>(\w*)<\/t>/.match(entry.to_s)[1]
      next if ids.include?(dict+id)
      xml = @admindict.array[dict.to_s].get(id.to_s)
      doc = load_xml_string(xml)
      ids << dict+id
        $stderr.puts '---'+id
      if dict == @dictcode and type == 'lemma'
        info = {'id'=>id.to_s}
        info['title'] = doc.root.find('/entry/lemma/title').to_a.first.content.to_s
        info['title_var'] = doc.root.find('/entry/lemma/title_var').to_a.first.content.to_s if doc.root.find('/entry/lemma/title_var').first != nil and doc.root.find('/entry/lemma/title_var').to_a.first.content.to_s != ''
        if info['title_var'] != nil and not info['title'].start_with?(search_term.force_encoding('UTF-8')) and info['title_var'].start_with?(search_term.force_encoding('UTF-8'))
          tem = info['title']
          info['title'] = info['title_var']
          info['title_var'] = tem
        end
        words[info['title']] = 0 if words[info['title']].nil?
        words[info['title']] += 1
        variants = doc.root.find('/entry/lemma/grammar_note/variant|/entry/lemma/style_note/variant')
        variants.each{|v|
          info['variant'] = [] if info['variant'].nil?
          hv = {'var'=>v['title']}
          hv['last'] = '1' if v == variants.last
          info['variant'] << hv
        }
        info['lemma_type'] = 'single'
        info['lemma_type'] = doc.root.find('/entry/lemma/lemma_type').first.content.to_s unless doc.root.find('/entry/lemma/lemma_type').first.nil?
        info['is_colloc'] = '1' if info['lemma_type'] == 'collocation'
        info['region'] = doc.root.find('/entry/lemma/grammar_note').first['region'].to_s unless doc.root.find('/entry/lemma/grammar_note').first.nil?
        info['region_sort'] = (info['region']=='cr'?3:0) + (info['region']=='cechy'?2:0) + (info['region']=='morava'?1:0) 
        info['slovni_druh'] = @slovni_druhy[doc.root.find('/entry/lemma/grammar_note').first['slovni_druh'].to_s].to_s unless doc.root.find('/entry/lemma/grammar_note').first.nil?
        info['slovni_druh_en'] = @slovni_druhy_en[doc.root.find('/entry/lemma/grammar_note').first['slovni_druh'].to_s].to_s unless doc.root.find('/entry/lemma/grammar_note').first.nil?
        info['auto_complete'] = '1' if doc.root.find('/entry/lemma').first['auto_complete'] == '1'
        trans_words = []
        $stderr.puts id
        doc.root.find('/entry/meanings/meaning/relation[@target="'+target+'" and @type="translation" and (@completeness!="1" or not(@completeness))]').each {|tran|
          next if perm == 'ro' and (tran['status'] != 'published' or tran['front_video'] != 'published')
          info['translations'] = [] if info['translations'].nil?
          # info['translations'] << {'trid'=>tran['meaning_id']}
          if tran['lemma_id'].to_s != ''
            if target == 'cs' or target == 'en'
              info2 = {'id'=>tran['lemma_id']}
              info2['title'] = tran.find('title').first.content.to_s unless tran.find('title').first.nil?
              info2['title_dia'] = tran.find('title_dia').first.content.to_s unless tran.find('title_dia').first.nil?
              info2['is_text'] = '1'
              trans_words << info2['title']
            else
              $stderr.puts tran
              info2, coll2 = parse_entry_search(tran['lemma_id'], load_xml_string(@admindict.array[target].get(tran['lemma_id'])), 'admin', {}, 'text')
              info2['is_sign'] = '1'
            end
            info2['preklad_neznamy'] = '1' if not tran.parent.find('is_translation_unknown').first.nil? and tran.parent.find('is_translation_unknown').first.content == '1'
            info2['dict'] = target
            info2['text_link'] = '1' if perm == 'ro' and not tran['auto_complete'] == '1'
            info['translations'] << info2
          elsif not(tran['meaning_id'].to_s =~ /[0-9]*-[0-9]/) and not trans_words.include?(tran['meaning_id'].to_s)
            info['translations'] << {'text_link'=>'1', 'title'=>tran['meaning_id'].to_s, 'is_text'=>'1'}
          end
        }
        info['translations'].uniq! unless info['translations'].nil?
        info['czj'] = 0
        info['is_editor'] = '1' if perm.include?('edit') or perm.include?('admin')
        list << info
      end
      if dict == @dictcode and type == 'usg'
        entry_ac = doc.find('/entry/lemma').first['auto_complete']
        doc.find('/entry/meanings/meaning/usages/usage/relation[@type="translation" and @target="'+target+'"]').each{|usg|
          next if usg.find('../text').first.nil?
          usgtran = usg.find('../text').first.content.to_s
          next unless usgtran.clone.downcase_utf8 =~ /(^| )#{search_term.downcase_utf8}/
          next if perm == 'ro' and (usg['completeness'] == '1' or (usg['auto_complete'] == '0' and usg['front_video'].to_s != 'published'))
          next if perm == 'ro' and (usg.find('../status').first.nil?  or usg.find('../status').first.content != 'published')
          info = {'title'=>usgtran, 'id'=>id, 'translations'=>[]}
          if usg['lemma_id'].to_s != ''
            if target == 'cs' or target == 'en'
              info2 = {'id'=>tran['lemma_id']}
              info2['title'] = usg.find('translation').first.content.to_s unless usg.find('translation').first.nil?
              info2['title_dia'] = strip_dia(info2['title'])
              info2['is_text'] = '1'
            else
              info2, coll2 = parse_entry_search(usg['lemma_id'], load_xml_string(@admindict.array[target].get(usg['lemma_id'])), 'admin', {}, 'text')
              info2['is_sign'] = '1'
            end
            info2['dict'] = target
            info2['text_link'] = '1' if perm == 'ro' and (not usg['auto_complete'] == '1')
            info['translations'] << info2
            #info['text_link'] = '1' if perm == 'ro' and (not entry_ac == '1' or (not usg.find('../status').first.nil? and usg.find('../status').first.content != 'published'))
            info['text_link'] = '1'
            #info['auto_complete'] = '1' if entry_ac == '1'
            info['is_editor'] = '1' if perm.include?('edit') or perm.include?('admin')
          end
          if list.select{|l| l['title'] == info['title']}.size == 0
            list << info
          else
            info_up = list.select{|l| l['title'] == info['title']}.first
            info_up['translations'] << info2
            info_up['translations'].uniq!
          end
        }
      end
      if dict == target
        entry_ac = doc.find('/entry/lemma').first['auto_complete']
        doc.find('/entry/meanings/meaning/relation[@type="translation" and @target="'+@dictcode+'"]').each{|rel|
          next if rel['meaning_id'].to_s =~ /[0-9]*-[0-9]/
          next if perm == 'ro' and rel['status'] != 'published'
          next unless rel['meaning_id'].downcase_utf8 =~ /(^| )#{search_term.downcase_utf8}/
          info = {'title'=>rel['meaning_id'].to_s, 'text_link'=>'1'}
          info['preklad_neznamy'] = '1' if not rel.parent.find('is_translation_unknown').first.nil? and rel.parent.find('is_translation_unknown').first.content == '1'
          if target == 'cs' or target == 'en'
            info2 = {'id'=>id}
            info2['title'] = doc.find('/entry/lemma/title').first.content.to_s unless doc.find('/entry/lemma/title').first.nil?
            info2['title_dia'] = doc.find('/entry/lemma/title_dia').first.content.to_s unless doc.find('/entry/lemma/title_dia').first.nil?
            #info2['title'] = tran.find('title').first.content.to_s unless tran.find('title').first.nil?
            info2['is_text'] = '1'
          else
            info2, coll2 = parse_entry_search(id, load_xml_string(xml), 'admin', {}, 'text')
            info2['is_sign'] = '1'
          end
          info2['dict'] = target
          info2['text_link'] = '1' if perm == 'ro' and not rel['auto_complete'] == '1' and not entry_ac == '1'
          info['translations'] = [info2]
          info['is_editor'] = '1' if perm.include?('edit') or perm.include?('admin')
          if list.select{|l| l['title'] == info['title']}.size == 0
            list << info
          else
            info_up = list.select{|l| l['title'] == info['title']}.first
            info_up['translations'] = [] if info_up['translations'].nil?
            info_up['translations'] << info2
            info_up['translations'].uniq!
          end
        }
      end
    }
    
    list.delete_if{|l| l['translations'].nil? or l['translations'].size==0}
    trancount = list.inject(0){|sum, e| sum + e['translations'].size}

    top = []
    list.select{|h| h['title'].downcase == search_term.downcase}.each{|h|
      top << h
      list.delete(h)
    }
    col = TwitterCldr::Collation::Collator.new(:cs)
    list.sort!{|x,y|
      pocetx = (x['title'].count(' ') > 0) ? 1 : 0
      pocety = (y['title'].count(' ') > 0) ? 1 : 0
      sort_pocetslov = (pocetx <=> pocety)
      sort_poradislov = (x['title'].clone.downcase_utf8.force_encoding('UTF-8').split(' ').index{|w|w.start_with?(search_term.downcase_utf8.force_encoding('UTF-8'))}.to_i <=> y['title'].clone.downcase_utf8.force_encoding('UTF-8').split(' ').index{|w|w.start_with?(search_term.downcase_utf8.force_encoding('UTF-8'))}.to_i)
      sort_alfa = (col.compare(x['title'].clone.downcase_utf8.force_encoding('UTF-8'), y['title'].clone.downcase_utf8.force_encoding('UTF-8')))
      (sort_pocetslov == 0 ?  (sort_poradislov == 0 ? sort_alfa : sort_poradislov) : sort_pocetslov)
    }
    newlist = top + list

    resultlist = []
    count = newlist.size
    entrycount = -1
    newlist.each{|entry|
      entrycount +=1
      next if entrycount < page*page_entries
      break if entrycount >= page*page_entries+page_entries
      resultlist << entry 
    }
    
    resultlist.each{|h|
      if words[h['title']] != nil and words[h['title']] == 1
        h.delete('slovni_druh')
      end
      if target == 'en' or target == 'cs' or target == 'sj'
        h['translations'].sort!{|x,y|
          x['title'] <=> y['title']
        }
      else
        h['translations'].sort!{|x,y|
          y['region_sort'].to_i <=> x['region_sort'].to_i
        }
      end
    }

    return resultlist, count, trancount
  end

  def search_cs(search_term, search_czj, czjdict, perm='ro', page=0, user_skupiny='', is_public=false, deklin=true, diak=false, spojeni=false)
    page_entries = 20
    list = []
    deklin_query = ''
    diak_query = ''
    col_query2 = ''
    deklin_query = ' or (some $g in lemma/gram/form satisfies fn:matches(lower-case($g), "(^| )'+search_term.downcase_utf8+'"))' if deklin
    diak_query = ' or starts-with(lemma/title_dia, "'+(search_term.downcase_utf8)+'")' if diak
    if perm == 'ro' and not user_skupiny.include?('all')
        col_query = 'let $col := for $x in collection("'+@dictcode+'")/entry[(lemma/@auto_complete="1" or lemma/completeness="2" or lemma/completeness="100") and (lemma/lemma_type="collocation") and (fn:matches(lower-case(lemma/title), "(^| )'+search_term.downcase_utf8+'") or starts-with(lemma/title_var, "'+(search_term.downcase_utf8)+'") or (some $v in lemma/grammar_note/variant|lemma/style_note/variant satisfies starts-with(lower-case($v),"'+search_term.downcase_utf8+'")) ' + diak_query + deklin_query + ')] return $x' 
        #search_query = col_query+"\n"+'for $x in collection("'+@dictcode+'")/entry[@id=$col/collocations/colloc/@lemma_id or ((lemma/@auto_complete="1" or lemma/completeness="2" or lemma/completeness="100") and ((lemma/lemma_type="single" or lemma/lemma_type="predpona" or not(lemma/lemma_type)) and (starts-with(lower-case(lemma/title), "'+search_term.downcase_utf8+'") or starts-with(lemma/title_var, "'+(search_term.downcase_utf8)+'") or (some $v in lemma/grammar_note/variant|lemma/style_note/variant satisfies starts-with(lower-case($v),"'+search_term.downcase_utf8+'")) ' + diak_query + deklin_query + ')))] order by lower-case($x/lemma/title) return data($x/@id)' 
        part_term = search_term
        part_term = search_term.cut_utf8(0,3) if search_term.length_utf8 > 4
        col_query2 = '@id=$col/collocations/colloc/@lemma_id or ' if spojeni
        part_query = 'subsequence(for $x in collection("'+@dictcode+'")/entry[(lemma/@auto_complete="1" or not(lemma/@auto_complete) or lemma/completeness="2" or lemma/completeness="100") and ((lemma/lemma_type="single" or lemma/lemma_type="predpona" or not(lemma/lemma_type)) and (starts-with(lower-case(lemma/title), "'+part_term.downcase_utf8+'") or starts-with(lemma/title_var, "'+(part_term.downcase_utf8)+'") or (some $v in lemma/grammar_note/variant|lemma/style_note/variant satisfies starts-with(lower-case($v),"'+part_term.downcase_utf8+'")) ' + diak_query + deklin_query + '))] order by lower-case($x/lemma/title) return data($x/@id), 1, 50)' 
        full_query = 'for $x in collection("'+@dictcode+'")/entry['+col_query2+'((lemma/@auto_complete="1" or not(lemma/@auto_complete) or lemma/completeness="2" or lemma/completeness="100") and ((lemma/lemma_type="single" or lemma/lemma_type="predpona" or not(lemma/lemma_type)) and (lower-case(lemma/title)="'+search_term.downcase_utf8+'" or lemma/title_var="'+(search_term.downcase_utf8)+'" or (some $v in lemma/grammar_note/variant|lemma/style_note/variant satisfies lower-case($v)="'+search_term.downcase_utf8+'") ' + diak_query + deklin_query + ')))] order by lower-case($x/lemma/title) return data($x/@id)' 
        search_query = 'let $dfull:='+full_query+'
        let $dpart:='+part_query+'
        for $p in distinct-values(($dfull,$dpart)) return $p'
        search_query = col_query + "\n" + search_query if spojeni
    else
        col_query2 = '@id=$col/collocations/colloc/@lemma_id or ' if spojeni
        col_query = 'let $col := for $x in collection("'+@dictcode+'")/entry[(lemma/lemma_type="collocation") and (fn:matches(lower-case(lemma/title), "(^| )'+search_term.downcase_utf8+'") or starts-with(lemma/title_var, "'+(search_term.downcase_utf8)+'") or starts-with(@id,"'+search_term+'") or (some $v in lemma/grammar_note/variant|lemma/style_note/variant satisfies starts-with(lower-case($v),"'+search_term.downcase_utf8+'")) ' + diak_query + deklin_query + ')] return $x' 
        search_query = 'for $x in collection("'+@dictcode+'")/entry['+col_query2+'((lemma/lemma_type="single" or lemma/lemma_type="predpona" or not(lemma/lemma_type)) and (starts-with(lower-case(lemma/title), "'+search_term.downcase_utf8+'") or starts-with(lemma/title_var, "'+(search_term.downcase_utf8)+'") or starts-with(@id,"'+search_term+'") or (some $v in lemma/grammar_note/variant|lemma/style_note/variant satisfies starts-with(lower-case($v),"'+search_term.downcase_utf8+'")) ' + diak_query + deklin_query + '))] order by lower-case($x/lemma/title) return data($x/@id)' 
        search_query = col_query + "\n" + search_query if spojeni
    end
    countquery = 'count('+search_query+')'
    $stderr.puts search_query
    count = @sedna.query(countquery).first.to_i
    query = 'subsequence(' + search_query + ', ' + (1+page*page_entries).to_s + ', ' + page_entries.to_s + ')'
    #query = search_query
    $stderr.puts query
    ids = []
    words = {}
    @sedna.query(query).each{|id|
      next if ids.include?(id)
      xml = get(id.to_s)
      doc = load_xml_string(xml)
      #next if perm == 'ro' and doc.find('/entry/lemma/status').first != nil and doc.find('/entry/lemma/status').first.content.to_s == 'hidden' and doc.find('/entry/lemma/completeness').first != nil and (doc.find('/entry/lemma/completeness').first.content != '100' and doc.find('/entry/lemma/completeness').first.content != '2')
      #next if perm == 'ro' and (doc.find('/entry/lemma/completeness').first.nil? or doc.find('/entry/lemma/completeness').first.content == '1')
      #next if perm == 'ro' and doc.find('/entry/lemma').first['auto_complete'] == '0'
      ids << id
      info = {'id'=>id.to_s}
      info['title'] = doc.root.find('/entry/lemma/title').to_a.first.content.to_s
      info['title_var'] = doc.root.find('/entry/lemma/title_var').to_a.first.content.to_s if doc.root.find('/entry/lemma/title_var').first != nil and doc.root.find('/entry/lemma/title_var').to_a.first.content.to_s != ''
      words[info['title']] = 0 if words[info['title']].nil?
      words[info['title']] += 1
      variants = doc.root.find('/entry/lemma/grammar_note/variant|/entry/lemma/style_note/variant')
      variants.each{|v|
        info['variant'] = [] if info['variant'].nil?
        hv = {'var'=>v['title']}
        hv['last'] = '1' if v == variants.last
        info['variant'] << hv
      }
      info['lemma_type'] = 'single'
      info['lemma_type'] = doc.root.find('/entry/lemma/lemma_type').first.content.to_s unless doc.root.find('/entry/lemma/lemma_type').first.nil?
      info['region'] = doc.root.find('/entry/lemma/grammar_note').first['region'].to_s unless doc.root.find('/entry/lemma/grammar_note').first.nil?
      info['region_sort'] = (info['region']=='cr'?3:0) + (info['region']=='cechy'?2:0) + (info['region']=='morava'?1:0) 
      info['slovni_druh'] = @slovni_druhy[doc.root.find('/entry/lemma/grammar_note').first['slovni_druh'].to_s].to_s unless doc.root.find('/entry/lemma/grammar_note').first.nil?
      info['slovni_druh_en'] = @slovni_druhy_en[doc.root.find('/entry/lemma/grammar_note').first['slovni_druh'].to_s].to_s unless doc.root.find('/entry/lemma/grammar_note').first.nil?
      if spojeni
        if perm == 'ro'
          query2 = 'for $x in collection("'+@dictcode+'")/entry[(lemma/@auto_complete="1" or lemma/completeness="2" or lemma/completeness="100") and collocations/colloc/@lemma_id="'+id.to_s+'"] order by $x/lemma/title return <a>{data($x/@id)};{$x/lemma/title/text()};{data($x/lemma//variant/@title)}</a>'
        else
          query2 = 'for $x in collection("'+@dictcode+'")/entry[collocations/colloc/@lemma_id="'+id.to_s+'"] order by $x/lemma/title return <a>{data($x/@id)};{$x/lemma/title/text()};{data($x/lemma//variant/@title)}</a>'
        end
        @sedna.query(query2).each{|qres|
          qar = qres.gsub('<a>','').gsub('</a>','').split(';')
          search_term = search_term.force_encoding('UTF-8')
          #$stderr.puts search_term.encoding
          #$stderr.puts qar[1].encoding
          #$stderr.puts strip_dia(qar[1]).encoding
          sptitle = qar[1]
          if sptitle.include?(search_term) or strip_dia(qar[1].dup).include?(search_term) or qar[2].to_s.include?(search_term) or strip_dia(qar[2].to_s.dup).include?(search_term)
            info['spojeni'] = [] if info['spojeni'].nil?
            spojeni = {'id'=>qar[0], 'title'=>sptitle}
            spojeni['var'] = qar[2].to_s if qar[2].to_s != ''
            info['spojeni'] << spojeni
          end
        }
      end
      info['czj'] = 0
      info['is_editor'] = '1' if perm.include?('edit') or perm.include?('admin')
      list << info
    }
    if search_czj
      query = 'for $x in collection("czj")/entry where some $r in $x/meanings/meaning/relation[@type="translation"] satisfies fn:matches(lower-case($r/@meaning_id), "(^| )'+search_term.downcase_utf8+'") return data($x/@id)'
      $stderr.puts query
      @sedna.query(query).each{|id|
        xml = czjdict.get(id.to_s)
        $stderr.puts xml
        doc = load_xml_string(xml)
        info = {'id'=>id.to_s, 'czjlink'=>'1'}
        titles = []
        doc.find('/entry/meanings/meaning/relation[@type="translation"]').each{|rel|
          titles << rel['meaning_id'].to_s unless rel['meaning_id'].to_s =~ /[0-9]*-[0-9]/
        }
        next if titles.size == 0
        count += 1
        info['title'] = titles.uniq.join(', ')
        info['czj'] = 1
        list << info
      }
    end
    list.each{|h|
      if words[h['title']] != nil and words[h['title']] == 1
        h.delete('slovni_druh')
      end
    }
    top = []
    list.select{|h| h['title'].downcase == search_term.downcase.force_encoding('UTF-8')}.each{|h|
      top << h
      list.delete(h)
    }
    list_start = []
    list_col = []
    list_col_start = []
    list.select{|h| h['title'].downcase.start_with?(search_term.downcase.force_encoding('UTF-8'))}.each{|h|
      list_start << h
      list.delete(h)
    }
    #col = TwitterCldr::Collation::Collator.new(:cs)
    #list_start.sort!{|x,y|
    #  #$stderr.puts x['title'].downcase_utf8.encoding
    #  #$stderr.puts y['title'].downcase_utf8.encoding
    #  col.compare(x['title'].downcase_utf8.force_encoding('UTF-8'), y['title'].downcase_utf8.force_encoding('UTF-8'))
    #}
    #list.sort!{|x,y|
    #  col.compare(x['title'].downcase_utf8.force_encoding('UTF-8'), y['title'].downcase_utf8.force_encoding('UTF-8'))
    #}
    resultlist = top + list_start + list

    #resultlist = []
    #count = newlist.size
    #entrycount = -1
    #newlist.each{|entry|
    #  entrycount +=1
    #  next if entrycount < page*page_entries
    #  break if entrycount >= page*page_entries+page_entries
    #  resultlist << entry 
    #}
    return resultlist, count
  end

  def search_allcs(search_term, search_czj, czjdict, perm='ro', page=0, user_skupiny='', is_public=false, deklin=true)
    page_entries = 20
    list = []
    search_query = 'for $x in collection("'+@dictcode+'")/entry[((lemma/@auto_complete="1" or lemma/completeness="2" or lemma/completeness="100") and ((lemma/lemma_type="single" or lemma/lemma_type="predpona" or not(lemma/lemma_type)) ))] order by lower-case($x/lemma/title) return data($x/@id)' 
    $stderr.puts search_query
    countquery = 'count('+search_query+')'
    count = @sedna.query(countquery).first.to_i
    ids = []
    words = {}
    query = 'for $entry in subsequence(for $p in ('+search_query+') return $p, '+(1+page*page_entries).to_s+', '+page_entries.to_s+') return $entry'
    $stderr.puts query
    @sedna.query(query).each{|id|
      next if ids.include?(id)
      xml = get(id.to_s)
      doc = load_xml_string(xml)
      ids << id
      info = {'id'=>id.to_s}
      info['title'] = doc.root.find('/entry/lemma/title').to_a.first.content.to_s
      info['title_var'] = doc.root.find('/entry/lemma/title_var').to_a.first.content.to_s if doc.root.find('/entry/lemma/title_var').first != nil and doc.root.find('/entry/lemma/title_var').to_a.first.content.to_s != ''
      words[info['title']] = 0 if words[info['title']].nil?
      words[info['title']] += 1
      variants = doc.root.find('/entry/lemma/grammar_note/variant|/entry/lemma/style_note/variant')
      variants.each{|v|
        info['variant'] = [] if info['variant'].nil?
        hv = {'var'=>v['title']}
        hv['last'] = '1' if v == variants.last
        info['variant'] << hv
      }
      info['lemma_type'] = 'single'
      info['lemma_type'] = doc.root.find('/entry/lemma/lemma_type').first.content.to_s unless doc.root.find('/entry/lemma/lemma_type').first.nil?
      info['region'] = doc.root.find('/entry/lemma/grammar_note').first['region'].to_s unless doc.root.find('/entry/lemma/grammar_note').first.nil?
      info['region_sort'] = (info['region']=='cr'?3:0) + (info['region']=='cechy'?2:0) + (info['region']=='morava'?1:0) 
      info['slovni_druh'] = @slovni_druhy[doc.root.find('/entry/lemma/grammar_note').first['slovni_druh'].to_s].to_s unless doc.root.find('/entry/lemma/grammar_note').first.nil?
      info['slovni_druh_en'] = @slovni_druhy_en[doc.root.find('/entry/lemma/grammar_note').first['slovni_druh'].to_s].to_s unless doc.root.find('/entry/lemma/grammar_note').first.nil?
      if perm == 'ro'
        query2 = 'for $x in collection("'+@dictcode+'")/entry[(lemma/@auto_complete="1" or lemma/completeness="2" or lemma/completeness="100") and collocations/colloc/@lemma_id="'+id.to_s+'"] order by $x/lemma/title return <a>{data($x/@id)};{$x/lemma/title/text()};{data($x/lemma//variant/@title)}</a>'
      else
        query2 = 'for $x in collection("'+@dictcode+'")/entry[collocations/colloc/@lemma_id="'+id.to_s+'"] order by $x/lemma/title return <a>{data($x/@id)};{$x/lemma/title/text()};{data($x/lemma//variant/@title)}</a>'
      end
      info['czj'] = 0
      info['is_editor'] = '1' if perm.include?('edit') or perm.include?('admin')
      list << info
    }
    list.each{|h|
      if words[h['title']] != nil and words[h['title']] == 1
        h.delete('slovni_druh')
      end
    }

    return list, count
  end

  def translate_all(search_term, type, perm='ro', page=0, user_skupiny='', is_public=false, sort_order='misto', target='cs')
    page_entries = 10
    list = {}
    list_to_sort = []
    #hledani
    search_query = 'for $y in collection("'+@dictcode+'")/entry let $size:= if (count($y/lemma/swmix/sw)=0) then 1000 else if ($y/lemma/swmix/sw[@primary="true"]) then 1 else count($y/lemma/swmix/sw) where (not($y/lemma/completeness) or $y/lemma/completeness!="1") and (count($y/meanings/meaning/relation[@target="'+target+'"])>0) and ((some $m in $y/meanings/meaning/relation satisfies (($m/../is_translation_unknown!="1" or not($m/../is_translation_unknown)) and  $m/@target="'+target+'" and $m/@type="translation" and $m/@status="published" )) ) order by $y/lemma/lemma_type descending,$size,string-length($y/lemma/swmix/sw[1]/@fsw) return data($y/@id)'

    query = 'for $entry in subsequence('+search_query+', '+(1+page*page_entries).to_s+', '+page_entries.to_s+') return $entry'
    $stderr.puts query

    count_query = 'count('+search_query+')'
    count = @sedna.query(count_query).first.to_i
    ids = []
    collocs = {}
    resultlist = []
    @sedna.query(query).each{|id|
      xml = get( id.to_s)
      doc = load_xml_string(xml)
      info, collocs = parse_entry_search(id, doc, perm, collocs, type)
      info['dict'] = @dictcode
      info['auto_complete'] = '1' if doc.root.find('/entry/lemma').first['auto_complete'] == '1'
      trans_words = []
      doc.root.find('/entry/meanings/meaning/relation[@target="'+target+'" and @type="translation" and not(@completeness="1") and @status="published"]').each{|tran|
        info['translations'] = [] if info['translations'].nil?
        if tran['lemma_id'].to_s != ''
          if target == 'cs' or target == 'en'
            info2 = {'id'=>tran['lemma_id']}
            info2['title'] = tran.find('title').first.content.to_s unless tran.find('title').first.nil?
            info2['title_dia'] = strip_dia(info2['title'])
            info2['is_text'] = '1'
            info2['is_colloc'] = '1' if tran['lemma_type'] == 'collocation'
            trans_words << info2['title']
          else
            info2, coll2 = parse_entry_search(tran['lemma_id'], load_xml_string(@admindict.array[target].get(tran['lemma_id'])), 'admin', {}, 'text')
            info2['is_sign'] = '1'
          end
          info2['dict'] = target
          info2['text_link'] = '1' if not tran['auto_complete'] == '1'
          info2['text_link'] = '1' if tran['lemma_type'] == 'usage'
          info2['preklad_neznamy'] = '1' if not tran.parent.find('is_translation_unknown').first.nil? and tran.parent.find('is_translation_unknown').first.content == '1'
          info['translations'] << info2
        elsif not(tran['meaning_id'].to_s =~ /[0-9]*-[0-9]/) and not trans_words.include?(tran['meaning_id'].to_s)
          info2 = {'text_link'=>'1', 'title'=>tran['meaning_id'].to_s, 'is_text'=>'1'}
          info2['title_dia'] = strip_dia(info2['title'])
          info2['preklad_neznamy'] = '1' if not tran.parent.find('is_translation_unknown').first.nil? and tran.parent.find('is_translation_unknown').first.content == '1'
          info['translations'] << info2
        end
      }
      info['translations'].uniq! unless info['translations'].nil?
      resultlist << info
    }
    return resultlist, count, true, count
  end

  def translate(search_term, type, perm='ro', page=0, user_skupiny='', is_public=false, sort_order='misto', target='cs')
    page_entries = 10
    list = {}
    list_to_sort = []
    #hledani
    case type
    when 'text'
      trantarget = 'cs'
      trantarget = 'en' if dictcode == 'asl' or dictcode == 'is'
      tranunknown = '($m/../is_translation_unknown!="1" or not($m/../is_translation_unknown)) and '
      if perm == 'ro'
        #presna shoda
        query_full = 'for $y in collection("'+@dictcode+'")/entry where (not($y/lemma/completeness) or $y/lemma/completeness!="1") and (count($y/meanings/meaning/relation[@target="'+target+'"])>0) and ((some $m in $y/meanings/meaning/relation satisfies ('+tranunknown+' $m/@target="'+trantarget+'" and $m/@status="published" and $m/@type="translation" and (lower-case($m/title)="'+search_term.downcase_utf8+'" or $m/title_dia="'+(search_term.downcase_utf8)+'" or (some $tv in $m/title_var satisfies $tv="'+search_term.downcase_utf8+'") or (some $f in $m/form satisfies $f="'+search_term.downcase_utf8+'")))) or $y/@id="'+search_term+'" or (some $l in $y/media/file[location=$y/lemma/video_front]/label satisfies $l="'+search_term+'")) return data($y/@id)' 
        #zacatek slova
        query_part = 'for $y in collection("'+@dictcode+'")/entry where (not($y/lemma/completeness) or $y/lemma/completeness!="1") and (count($y/meanings/meaning/relation[@target="'+target+'"])>0) and (some $m in $y/meanings/meaning/relation satisfies ('+tranunknown+' $m/@target="'+trantarget+'" and $m/@status="published" and $m/@type="translation" and (fn:matches(lower-case($m/title), "(^| )'+search_term.downcase_utf8+'") or fn:matches($m/title_dia, "(^| )'+(search_term.downcase_utf8)+'") or (some $tv in $m/title_var satisfies fn:matches($tv, "(^| )'+search_term.downcase_utf8+'")) or (some $f in $m/form satisfies fn:matches($f, "(^| )'+search_term.downcase_utf8+'")) ))) return data($y/@id)' 
      else
        #presna shoda
        query_full = 'for $y in collection("'+@dictcode+'")/entry where (not($y/lemma/completeness) or $y/lemma/completeness!="1") and (count($y/meanings/meaning/relation[@target="'+target+'"])>0) and ((some $m in $y/meanings/meaning/relation satisfies ('+tranunknown+' $m/@target="'+trantarget+'" and $m/@type="translation" and (lower-case($m/title)="'+search_term.downcase_utf8+'" or $m/title_dia="'+(search_term.downcase_utf8)+'" or (some $tv in $m/title_var satisfies $tv="'+search_term.downcase_utf8+'") or (some $f in $m/form satisfies $f="'+search_term.downcase_utf8+'")))) or $y/@id="'+search_term+'" or (some $l in $y/media/file[location=$y/lemma/video_front]/label satisfies $l="'+search_term+'")) return data($y/@id)' 
        #zacatek slova
        query_part = 'for $y in collection("'+@dictcode+'")/entry where (not($y/lemma/completeness) or $y/lemma/completeness!="1") and (count($y/meanings/meaning/relation[@target="'+target+'"])>0) and (some $m in $y/meanings/meaning/relation satisfies ('+tranunknown+' $m/@target="'+trantarget+'" and $m/@type="translation" and (fn:matches(lower-case($m/title), "(^| )'+search_term.downcase_utf8+'") or fn:matches($m/title_dia, "(^| )'+(search_term.downcase_utf8)+'") or (some $tv in $m/title_var satisfies fn:matches($tv, "(^| )'+search_term.downcase_utf8+'")) or (some $f in $m/form satisfies fn:matches($f, "(^| )'+search_term.downcase_utf8+'")) ))) return data($y/@id)' 
      end
      
      query = 'let $full := ' + query_full + "\nlet $starting := " + query_part + "\nfor $p in distinct-values(($full, subsequence($starting, 1, 200))) return $p"
      query = 'let $full := ' + query_full + "\nlet $starting := " + query_part + "\nfor $p in distinct-values(($full, subsequence($starting, 1, 500))) return $p" if is_public
    when 'sw'
      search_ar = search_term.split('_')  
      search_ar.map!{|e| e='fn:matches($r, "(^|_)'+e.gsub(/\([^\)]*\)/,'')+'([^0-9]|$)")'}
      search_query = search_ar.join(' and ')
      query = 'for $y in collection("'+@dictcode+'")/entry where $y/lemma/completeness!="1" and (count($y/meanings/meaning/relation[@target="'+target+'"])>0) and (some $r in $y/lemma/sw|$y/lemma/swmix/sw satisfies ('+search_query+')) return data($y/@id)'
    when 'img'
      search_args = search_term.split('|')
      search_shape = search_args[0].split(',') #tvary
      search_asymetric = []
      search_symetric = []
      search_jedno = []
      search_obe_ruzne = []
      search_obe_stejne = []
      search_shape.each{|e|
        #jednorucni, rotace jen 0-7
        search_jedno << '(fn:matches($r/@fsw, "'+e+'[0-5][0-7]") and not(fn:matches($r/@fsw, "S1[0-9a-f][0-9a-f][0-5][89a-f]")) and not(fn:matches($r/@fsw, "S20[0-4][0-5][89a-f]")))'
        #dve ruce, stejne rotace 0-7 + 8-f
        search_obe_stejne << '(fn:matches($r/@fsw, "'+e+'[0-5][0-7]") and fn:matches($r/@fsw, "'+e+'[0-5][89a-f]"))'
        #dve ruce, ruzne, hledana 0-7 a jina 8-f, nebo hledana 8-f a jina 0-7
        search_obe_ruzne << '((fn:matches($r/@fsw, "'+e+'[0-5][0-7]") and not(fn:matches($r/@fsw, "'+e+'[0-5][89a-f]")) and (fn:matches($r/@fsw, "S1[0-9a-f][0-9a-f][0-5][89a-f]") or fn:matches($r/@fsw, "S20[0-4][0-5][89a-f]"))) or (fn:matches($r/@fsw, "'+e+'[0-5][89a-f]") and not(fn:matches($r/@fsw, "'+e+'[0-5][0-7]")) and (fn:matches($r/@fsw, "S1[0-9a-f][0-9a-f][0-5][0-7]") or fn:matches($r/@fsw, "S20[0-4][0-5][0-7]"))))'
      }
      if search_args[1].to_s != ''
        #pridame umisteni
        search_loc = search_args[1].split(',')
        search_loc.map!{|e| e='contains($r/@misto,"'+e+'")'}
        search_jedno.map!{|e| e = '(' + e + ' and ('+search_loc.join(' or ')+'))'}
        search_obe_stejne.map!{|e| e = '(' + e + ' and ('+search_loc.join(' or ')+'))'}
        search_obe_ruzne.map!{|e| e = '(' + e + ' and ('+search_loc.join(' or ')+'))'}
      end
      if search_args[4].to_s != ''
        #aktivni ruce
        if search_args[4].to_s == 'jedna'
          #obsahuje sipku s 0
          search_jedno.map!{|e| e = '(' + e + ' and (fn:matches($r/@fsw, "S2[2-9a-f][0-9a-f]0") and not(fn:matches($r/@fsw, "S2[2-9a-f][0-9a-f][12]"))))'}
          search_obe_stejne.map!{|e| e = '(' + e + ' and (fn:matches($r/@fsw, "S2[2-9a-f][0-9a-f]0") and not(fn:matches($r/@fsw, "S2[2-9a-f][0-9a-f][12]"))))'}
          search_obe_ruzne.map!{|e| e = '(' + e + ' and (fn:matches($r/@fsw, "S2[2-9a-f][0-9a-f]0") and not(fn:matches($r/@fsw, "S2[2-9a-f][0-9a-f][12]"))))'}
        end
        if search_args[4].to_s == 'dve'
          #obsahuje sipku s 1 a vic
          search_jedno.map!{|e| e = '(' + e + ' and (fn:matches($r/@fsw, "S2[2-9a-f][0-9a-f]2") or (fn:matches($r/@fsw, "S2[2-9a-f][0-9a-f]0") and fn:matches($r/@fsw, "S2[2-9a-f][0-9a-f]1")) or (not(fn:matches($r/@fsw, "S2[2-9a-f][0-9a-f]")) and (fn:matches($r/@fsw, "S20[567bcd]") or fn:matches($r/@fsw, "S21[123]")) ) ))'}
          search_obe_ruzne.map!{|e| e = '(' + e + ' and (fn:matches($r/@fsw, "S2[2-9a-f][0-9a-f]2") or (fn:matches($r/@fsw, "S2[2-9a-f][0-9a-f]0") and fn:matches($r/@fsw, "S2[2-9a-f][0-9a-f]1")) or (not(fn:matches($r/@fsw, "S2[2-9a-f][0-9a-f]")) and (fn:matches($r/@fsw, "S20[567bcd]") or fn:matches($r/@fsw, "S21[123]")) ) ))'}
          search_obe_stejne.map!{|e| e = '(' + e + ' and (fn:matches($r/@fsw, "S2[2-9a-f][0-9a-f]2") or (fn:matches($r/@fsw, "S2[2-9a-f][0-9a-f]0") and fn:matches($r/@fsw, "S2[2-9a-f][0-9a-f]1")) or (not(fn:matches($r/@fsw, "S2[2-9a-f][0-9a-f]")) and (fn:matches($r/@fsw, "S20[567bcd]") or fn:matches($r/@fsw, "S21[123]")) ) ))'}
        end
      end
      if search_args[2].to_s == 'j'
        #jednorucni
        search_query = search_jedno.join(' or ')
      elsif search_args[2].to_s == 's'
        #obe stejne
        search_query = search_obe_stejne.join(' or ')
      else
        #obe ruzne
        search_query = search_obe_ruzne.join(' or ')
      end
      #jen misto bez ruky
      if search_query == ''
        search_query = search_loc.join(' or ')
      end

      if search_args[3].to_s == 'jednoduchy'
        #jednoduchy znak
        query = 'for $y in collection("'+@dictcode+'")/entry where $y/lemma/lemma_type="single" and (count($y/meanings/meaning/relation[@target="'+target+'"])>0) and $y/lemma/completeness!="1" and (some $r in $y/lemma/sw|$y/lemma/swmix/sw satisfies ('+search_query+')) return data($y/@id)'
      elsif search_args[3].to_s == 'slozeny'
        #slozeny znak
        query = 'for $y in collection("'+@dictcode+'")/entry where $y/lemma/lemma_type!="single" and (count($y/meanings/meaning/relation[@target="'+target+'"])>0) and $y/lemma/completeness!="1" and (some $r in $y/lemma/sw|$y/lemma/swmix/sw satisfies ('+search_query+')) return data($y/@id)'
      else
        query = 'for $y in collection("'+@dictcode+'")/entry where $y/lemma/completeness!="1" and (count($y/meanings/meaning/relation[@target="'+target+'"])>0) and (some $r in $y/lemma/sw|$y/lemma/swmix/sw satisfies ('+search_query+')) return data($y/@id)'
      end
    end

    $stderr.puts query
    ids = []
    collocs = {}
    dbresult = @sedna.query(query)
    moreresults = false
    if type == 'text'
      origcount = dbresult.size
      moreresults = true if perm == 'ro' and origcount >= 500
      moreresults = true if perm != 'ro' and origcount >= 200
    end
    origcount = dbresult.size
    resultlist = []
    entrycount = -1
    dbresult.each{|id|
      $stderr.puts id
      next if ids.include?(id)
      xml = get( id.to_s)
      doc = load_xml_string(xml)
      $stderr.puts 'PERM='+perm
      $stderr.puts 'public='+is_public.to_s
      $stderr.puts 'skup='+user_skupiny
      $stderr.puts 'skup='+doc.find('/entry/lemma/pracskupina').first.content unless doc.find('/entry/lemma/pracskupina').first.nil?
      skup_eq = true
      entry_com = ''
      entry_com = doc.find('/entry/lemma/completeness').first.content.to_s unless doc.find('/entry/lemma/completeness').first.nil?
      translate_complete = false
      translate_complete = true if doc.find('/entry/lemma').first['auto_complete'] == '1'
      translate_complete = true if doc.find('/entry/media/file[location=/entry/lemma/video_front and status="published"]').first != nil
      $stderr.puts 'trans_comp='+translate_complete.to_s
      if doc.find('/entry/lemma/pracskupina').first != nil and (user_skupiny == '' or (not user_skupiny.include?(doc.find('/entry/lemma/pracskupina').first.content.to_s))) and not user_skupiny.include?('all')  #pokud ma heslo a uzivatel jine skupiny, delaji se dalsi kontroly. (stejna skupina = nepreskakuje se nezverejnene heslo)
        skup_eq = false
        #next if perm == 'ro' and doc.find('/entry/lemma/status').first != nil and doc.find('/entry/lemma/status').first.content.to_s == 'hidden' and doc.find('/entry/lemma/completeness').first != nil and (doc.find('/entry/lemma/completeness').first.content != '100' and doc.find('/entry/lemma/completeness').first.content != '2')
        next if perm == 'ro' and not translate_complete and doc.find('/entry/lemma/completeness').first != nil and (doc.find('/entry/lemma/completeness').first.content != '100' and doc.find('/entry/lemma/completeness').first.content != '2')
        next if perm == 'ro' and (doc.find('/entry/lemma/completeness').first.nil? or doc.find('/entry/lemma/completeness').first.content == '1')
        #next if perm == 'ro' and doc.find('/entry/lemma').first['auto_complete'] == '0' and doc.find('/entry/meanings/meaning/relation[@target="'+target+'" and @status="published"]').size == 0
        next if perm == 'ro' and not translate_complete and doc.find('/entry/meanings/meaning/relation[@target="'+target+'" and @status="published"]').size == 0
      end
      #next if doc.find('/entry/lemma').first['auto_complete'] == '0' and doc.find('/entry/lemma/pracskupina').first != nil and not perm.include?('admin') and not user_skupiny.include?('all') and not user_skupiny.include?(doc.find('/entry/lemma/pracskupina').first.content.to_s) #kontrola skupiny hesla a uzivatele
      next if not translate_complete and doc.find('/entry/lemma/pracskupina').first != nil and not perm.include?('admin') and not user_skupiny.include?('all') and not user_skupiny.include?(doc.find('/entry/lemma/pracskupina').first.content.to_s) #kontrola skupiny hesla a uzivatele
      ids << id
      info, collocs = parse_entry_search(id, doc, perm, collocs, type)
      info['dict'] = @dictcode
      info['auto_complete'] = '1' if doc.root.find('/entry/lemma').first['auto_complete'] == '1'
      trans_words = []
      doc.root.find('/entry/meanings/meaning/relation[@target="'+target+'" and @type="translation" and not(@completeness="1")]').each{|tran|
        next if perm == 'ro' and not skup_eq and tran['status'] != 'published' and entry_com != '2'
        info['translations'] = [] if info['translations'].nil?
       # info['translations'] << {'trid'=>tran['meaning_id']}
        if tran['lemma_id'].to_s != ''
          if target == 'cs' or target == 'en'
            info2 = {'id'=>tran['lemma_id']}
            info2['title'] = tran.find('title').first.content.to_s unless tran.find('title').first.nil?
            info2['title_dia'] = strip_dia(info2['title'])
            info2['is_text'] = '1'
            info2['is_colloc'] = '1' if tran['lemma_type'] == 'collocation'
            trans_words << info2['title']
          else
            info2, coll2 = parse_entry_search(tran['lemma_id'], load_xml_string(@admindict.array[target].get(tran['lemma_id'])), 'admin', {}, 'text')
            info2['is_sign'] = '1'
          end
          info2['dict'] = target
          info2['text_link'] = '1' if not skup_eq and not tran['auto_complete'] == '1'
          info2['text_link'] = '1' if tran['lemma_type'] == 'usage'
          info2['preklad_neznamy'] = '1' if not tran.parent.find('is_translation_unknown').first.nil? and tran.parent.find('is_translation_unknown').first.content == '1'
          info['translations'] << info2
        elsif not(tran['meaning_id'].to_s =~ /[0-9]*-[0-9]/) and not trans_words.include?(tran['meaning_id'].to_s)
          info2 = {'text_link'=>'1', 'title'=>tran['meaning_id'].to_s, 'is_text'=>'1'}
          info2['title_dia'] = strip_dia(info2['title'])
          info2['preklad_neznamy'] = '1' if not tran.parent.find('is_translation_unknown').first.nil? and tran.parent.find('is_translation_unknown').first.content == '1'
          info['translations'] << info2
        end
      }
      info['translations'].uniq! unless info['translations'].nil?
      $stderr.puts info['translations']
      if not info['translations'].nil? and info['translations'].size>0
        list[info['id']] = info 
        list_to_sort << info
      end
    }
    $stderr.puts query
    $stderr.puts ids
    count = list_to_sort.size

    #sort
    #setridime jen single hesla
    case type
    when 'text'
      top = []
      list_to_sort.select{|h| h['trans_sort'].include?(search_term.downcase)}.each{|h|
        top << h
        list_to_sort.delete(h)
      }
      $stderr.puts list_to_sort
      top.sort!{|x,y|
        y['region_sort'].to_i <=> x['region_sort'].to_i
      }
      list_to_sort.each{|x|
        x_translation = ''
        x_translation_dia = ''
        if x['translations'].size == 1
          x_translation = x['translations'].first['title']
          x_translation_dia = strip_dia(x_translation)
        else
          x['translations'].sort!{|a,b| 
            sort_mezer = a['title'].count(' ') <=> b['title'].count(' ')
            slovoa = a['title'].downcase_utf8.split(' ').index{|w|w.start_with?(search_term.downcase_utf8)}
            slovoa = 1000 if slovoa.nil?
            slovob = b['title'].downcase_utf8.split(' ').index{|w|w.start_with?(search_term.downcase_utf8)}
            slovob = 1000 if slovob.nil?
            sort_poradislov = (slovoa <=> slovob)
            dslovoa = a['title_dia'].downcase_utf8.split(' ').index{|w|w.start_with?(search_term.downcase_utf8)}
            dslovoa = 1000 if dslovoa.nil?
            dslovob = b['title_dia'].downcase_utf8.split(' ').index{|w|w.start_with?(search_term.downcase_utf8)}
            dslovob = 1000 if dslovob.nil?
            sort_dporadislov = (dslovoa <=> dslovob)
            sort_delka = (a['title'].length_utf8 <=> b['title'].length_utf8)
            (sort_poradislov == 0 ? (sort_mezer == 0 ? (sort_dporadislov == 0 ? sort_delka : sort_dporadislov) : sort_mezer) : sort_poradislov)
          }
          x_translation = x['translations'].first['title']
          x_translation_dia = x['translations'].first['title_dia']
        end
        x['sort_pocetslov'] = (x_translation.count(' ') == 0) ? (x_translation.to_s.downcase_utf8.start_with?(search_term.downcase_utf8) ? 0 : 1) : 2
        x['sort_poradislov'] = x_translation.downcase_utf8.split(' ').index{|w|w.start_with?(search_term.downcase_utf8)}
        x['sort_poradislov'] = 1000 if x['sort_poradislov'].nil?
        x['sort_dporadislov'] = x_translation_dia.downcase_utf8.split(' ').index{|w|w.start_with?(search_term.downcase_utf8)}
        x['sort_dporadislov'] = 1000 if x['sort_dporadislov'].nil?
      }
      $stderr.puts list_to_sort
      col = TwitterCldr::Collation::Collator.new(:cs)
      list_to_sort.sort!{|x,y|
        sort_pocetslov = (x['sort_pocetslov'] <=> y['sort_pocetslov'])
        sort_poradislov = (x['sort_poradislov'] <=> y['sort_poradislov'])
        sort_dporadislov = (x['sort_dporadislov'] <=> y['sort_dporadislov'])
        sort_alfa = (col.compare(x['translation'].downcase_utf8.force_encoding('UTF-8'), y['translation'].downcase_utf8.force_encoding('UTF-8')))
        sort_region = (y['region_sort'].to_i <=> x['region_sort'].to_i)
        (sort_pocetslov == 0 ?  (sort_poradislov == 0 ? (sort_dporadislov == 0 ? (sort_alfa == 0 ? sort_region : sort_alfa) : sort_dporadislov) : sort_poradislov) : sort_pocetslov)
      }
      newlist = top + list_to_sort
    when 'sw','img'
      top = []
      top_ids = []
      newlist = []
      $stderr.puts 'search='+search_term
      $stderr.puts 'search='+get_signs(search_term).to_s if type == 'sw'
      list_to_sort.map!{|h|
        h['sw_sort_size'] = h['sw_search'].length
        h['sw_sort_size'] = 1000 if h['sw_search'].length == 0
        h['sw_sort_size'] = 1 if h['sw_primary'] != ''
        if h['sw_search'].length == 1 or h['sw_primary'] != ''
          if h['sw_primary'] != ''
            sw_to_check = h['sw_primary']
            fsw_to_check = h['fsw_primary']
          else
            sw_to_check = h['sw_search'].first['sw']
            fsw_to_check = h['sw_search'].first['fsw']
          end
          if type=='sw' and h['lemma_type'] == 'single' and get_signs(sw_to_check) == get_signs(search_term) and h['art_misto'] == 'neutral'
            top << h
            top_ids << h['id']
          end
          if type == 'img' and h['art_misto'] == 'neutral' and h['lemma_type'] == 'single'
            search_shape.each{|shape|
              if fsw_to_check.include?(shape) 
                top << h
                top_ids << h['id']
                break
              end
            }
          end
        end
        h
      }

      list_to_sort.delete_if{|x| top_ids.include?(x['id'])}

      $stderr.puts list_to_sort
      list_to_sort.sort!{|x,y|
        typecomp = (y['lemma_type'] <=> x['lemma_type'])
        if sort_order == 'misto'
          comp = (y['art_misto_sort'] <=> x['art_misto_sort'])
          typecomp.zero? ? (comp.zero? ? (x['sw_sort_size'] <=> y['sw_sort_size']) : comp) : typecomp
        else 
          comp = (x['sw_sort_size'] <=> y['sw_sort_size'])
          typecomp.zero? ? (comp.zero? ? (y['art_misto_sort'] <=> x['art_misto_sort']) : comp) : typecomp
        end
      }
      $stderr.puts 'TOP='
      top.each{|t| $stderr.puts t['id']}
      $stderr.puts 'REST='
      list_to_sort.each{|t| $stderr.puts t['id']+'-'+t['lemma_type']+'-'+t['sw_sort_size'].to_s+'a'+t['art_misto_sort'].to_s+'='+t['art_misto']}
      newlist = top + list_to_sort
    end

    ids_used = []
    #pridame slovni spojeni
    resultlist = []
    entrycount = -1
    newlist.each{|entry|
      eid = entry['id'].to_s
      next if ids_used.include?(eid)
      ids_used << eid
      entrycount +=1
      next if entrycount < page*page_entries
      break if entrycount >= page*page_entries+page_entries
      #$stderr.puts '---S'+eid.to_s
      resultlist << entry
      #unless collocs[eid].nil?
      #  collocs[eid].each{|cid|
      #    ids_used << cid
      #    resultlist << list[cid]
      #  }
      #end
    }

    #pridame zbytek
    list.each{|eid,entry|
      entrycount +=1
      next if ids_used.include?(eid)
      next if entrycount < page*page_entries
      break if entrycount >= page*page_entries+page_entries
      resultlist << entry 
    }
    trancount = newlist.inject(0){|sum, e| sum + e['translations'].size}
    resultlist.each{|h|
      if target == 'en' or target == 'cs'
        h['translations'].sort!{|x,y|
          x['title'] <=> y['title']
        }
      else
        h['translations'].sort!{|x,y|
          y['region_sort'].to_i <=> x['region_sort'].to_i
        }
      end
    }
    return resultlist, count, moreresults, trancount
  end


  def search(search_term, type, perm='ro', page=0, user_skupiny='', is_public=false, sort_order='misto', slovni_druh='')
    page_entries = 5
    list = {}
    list_to_sort = []
    #hledani
    case type
    when 'text'
      trantarget = 'cs'
      trantarget = 'en' if dictcode == 'asl' or dictcode == 'is'
      trantarget = 'sj' if dictcode == 'spj'
      tranunknown = '($m/../is_translation_unknown!="1" or not($m/../is_translation_unknown)) and '
      tranunknown = '' if slovni_druh.to_s == 'klf' or slovni_druh.to_s == 'spc'
      if slovni_druh.to_s != ''
        #presna shoda
        query_full = 'for $y in collection("'+@dictcode+'")/entry where ($y/lemma/grammar_note/@slovni_druh="'+slovni_druh+'") and (not($y/lemma/completeness) or $y/lemma/completeness!="1") and ((some $m in $y/meanings/meaning/relation satisfies ('+tranunknown+' $m/@target="'+trantarget+'" and $m/@type="translation" and (lower-case($m/title)="'+search_term.downcase_utf8+'" or $m/title_dia="'+(search_term.downcase_utf8)+'" or (some $tv in $m/title_var satisfies $tv="'+search_term.downcase_utf8+'") or (some $f in $m/form satisfies $f="'+search_term.downcase_utf8+'")))) or $y/@id="'+search_term+'" or (some $l in $y/media/file[location=$y/lemma/video_front]/label satisfies $l="'+search_term+'")) return data($y/@id)' 
        #zacatek slova
        query_part = 'for $y in collection("'+@dictcode+'")/entry where ($y/lemma/grammar_note/@slovni_druh="'+slovni_druh+'") and (not($y/lemma/completeness) or $y/lemma/completeness!="1") and (some $m in $y/meanings/meaning/relation satisfies ('+tranunknown+' $m/@target="'+trantarget+'" and $m/@type="translation" and (fn:matches(lower-case($m/title), "(^| )'+search_term.downcase_utf8+'") or fn:matches($m/title_dia, "(^| )'+(search_term.downcase_utf8)+'") or (some $tv in $m/title_var satisfies fn:matches($tv, "(^| )'+search_term.downcase_utf8+'")) or (some $f in $m/form satisfies fn:matches($f, "(^| )'+search_term.downcase_utf8+'")) ))) return data($y/@id)' 
      else
        #presna shoda
        query_full = 'for $y in collection("'+@dictcode+'")/entry where (not($y/lemma/completeness) or $y/lemma/completeness!="1") and ((some $m in $y/meanings/meaning/relation satisfies ('+tranunknown+' $m/@target="'+trantarget+'" and $m/@type="translation" and (lower-case($m/title)="'+search_term.downcase_utf8+'" or $m/title_dia="'+(search_term.downcase_utf8)+'" or (some $tv in $m/title_var satisfies $tv="'+search_term.downcase_utf8+'") or (some $f in $m/form satisfies $f="'+search_term.downcase_utf8+'")))) or $y/@id="'+search_term+'" or (some $l in $y/media/file[location=$y/lemma/video_front]/label satisfies $l="'+search_term+'")) return data($y/@id)' 
        #zacatek slova
        query_part = 'for $y in collection("'+@dictcode+'")/entry where (not($y/lemma/completeness) or $y/lemma/completeness!="1") and (some $m in $y/meanings/meaning/relation satisfies ('+tranunknown+' $m/@target="'+trantarget+'" and $m/@type="translation" and (fn:matches(lower-case($m/title), "(^| )'+search_term.downcase_utf8+'") or fn:matches($m/title_dia, "(^| )'+(search_term.downcase_utf8)+'") or (some $tv in $m/title_var satisfies fn:matches($tv, "(^| )'+search_term.downcase_utf8+'")) or (some $f in $m/form satisfies fn:matches($f, "(^| )'+search_term.downcase_utf8+'")) ))) return data($y/@id)' 
      end
      
      query = 'let $full := ' + query_full + "\nlet $starting := " + query_part + "\nfor $p in distinct-values(($full, subsequence($starting, 1, 200))) return $p"
      query = 'let $full := ' + query_full + "\nlet $starting := " + query_part + "\nfor $p in distinct-values(($full, subsequence($starting, 1, 500))) return $p" if is_public
      #query = 'let $full := ' + query_full + "\nlet $starting := " + query_part + "\nfor $p in distinct-values(($full, $starting)) return $p"
    when 'sw'
      search_ar = search_term.split('_')  
      search_ar.map!{|e| e='fn:matches($r, "(^|_)'+e.gsub(/\([^\)]*\)/,'')+'([^0-9]|$)")'}
      search_query = search_ar.join(' and ')
      if slovni_druh != ''
        if search_query != ''
          query = 'for $y in collection("'+@dictcode+'")/entry where ($y/lemma/grammar_note/@slovni_druh="'+slovni_druh+'") and $y/lemma/completeness!="1" and (some $r in $y/lemma/sw|$y/lemma/swmix/sw satisfies ('+search_query+')) return data($y/@id)'
        else
          query = 'for $y in collection("'+@dictcode+'")/entry where ($y/lemma/grammar_note/@slovni_druh="'+slovni_druh+'") and $y/lemma/completeness!="1" return data($y/@id)'
        end
      else
        query = 'for $y in collection("'+@dictcode+'")/entry where $y/lemma/completeness!="1" and (some $r in $y/lemma/sw|$y/lemma/swmix/sw satisfies ('+search_query+')) return data($y/@id)'
      end
    when 'img'
      search_args = search_term.split('|')
      search_shape = search_args[0].to_s.split(',') #tvary
      search_asymetric = []
      search_symetric = []
      search_jedno = []
      search_obe_ruzne = []
      search_obe_stejne = []
      search_shape.each{|e|
        #jednorucni, rotace jen 0-7
        search_jedno << '(fn:matches($r/@fsw, "'+e+'[0-5][0-7]") and not(fn:matches($r/@fsw, "S1[0-9a-f][0-9a-f][0-5][89a-f]")) and not(fn:matches($r/@fsw, "S20[0-4][0-5][89a-f]")))'
        #dve ruce, stejne rotace 0-7 + 8-f
        search_obe_stejne << '(fn:matches($r/@fsw, "'+e+'[0-5][0-7]") and fn:matches($r/@fsw, "'+e+'[0-5][89a-f]"))'
        #dve ruce, ruzne, hledana 0-7 a jina 8-f, nebo hledana 8-f a jina 0-7
        search_obe_ruzne << '((fn:matches($r/@fsw, "'+e+'[0-5][0-7]") and not(fn:matches($r/@fsw, "'+e+'[0-5][89a-f]")) and (fn:matches($r/@fsw, "S1[0-9a-f][0-9a-f][0-5][89a-f]") or fn:matches($r/@fsw, "S20[0-4][0-5][89a-f]"))) or (fn:matches($r/@fsw, "'+e+'[0-5][89a-f]") and not(fn:matches($r/@fsw, "'+e+'[0-5][0-7]")) and (fn:matches($r/@fsw, "S1[0-9a-f][0-9a-f][0-5][0-7]") or fn:matches($r/@fsw, "S20[0-4][0-5][0-7]"))))'
        ##symetricke, rotace je 0-7 + 8-f
        ##bud neobsahuje sipku S2[2-f][0-f] nebo je tam jedna sipka S2[2-f][0-f]2 nebo dve sipky S2[2-f][0-f]0 + S2[2-f][0-f]1
        #search_symetric << '(fn:matches($r/@fsw, "'+e+'[0-5][0-7]") and fn:matches($r/@fsw, "'+e+'[0-5][89a-f]") and (not(fn:matches($r/@fsw, "S2[2-9a-f][0-9a-f]")) or (fn:matches($r/@fsw, "S2[2-9a-f][0-9a-f]2")) or (fn:matches($r/@fsw, "S2[2-9a-f][0-9a-f]0") and fn:matches($r/@fsw, "S2[2-9a-f][0-9a-f]1"))))'
        ##asymetricke, jen obsahuje znak
        #search_asymetric << '(fn:matches($r/@fsw, "'+e+'"))'
      }
      if search_args[1].to_s != ''
        #pridame umisteni
        search_loc = search_args[1].split(',')
        search_loc.map!{|e| e='contains($r/@misto,"'+e+'")'}
        search_jedno.map!{|e| e = '(' + e + ' and ('+search_loc.join(' or ')+'))'}
        search_obe_stejne.map!{|e| e = '(' + e + ' and ('+search_loc.join(' or ')+'))'}
        search_obe_ruzne.map!{|e| e = '(' + e + ' and ('+search_loc.join(' or ')+'))'}
        #search_symetric.map!{|e| e = '(' + e + ' and ('+search_loc.join(' or ')+'))'}
        #search_asymetric.map!{|e| e = '(' + e + ' and ('+search_loc.join(' or ')+'))'}
      end
      if search_args[4].to_s != ''
        #aktivni ruce
        if search_args[4].to_s == 'jedna'
          #obsahuje sipku s 0
          search_jedno.map!{|e| e = '(' + e + ' and (fn:matches($r/@fsw, "S2[2-9a-f][0-9a-f]0") and not(fn:matches($r/@fsw, "S2[2-9a-f][0-9a-f][12]"))))'}
          search_obe_stejne.map!{|e| e = '(' + e + ' and (fn:matches($r/@fsw, "S2[2-9a-f][0-9a-f]0") and not(fn:matches($r/@fsw, "S2[2-9a-f][0-9a-f][12]"))))'}
          search_obe_ruzne.map!{|e| e = '(' + e + ' and (fn:matches($r/@fsw, "S2[2-9a-f][0-9a-f]0") and not(fn:matches($r/@fsw, "S2[2-9a-f][0-9a-f][12]"))))'}
        end
        if search_args[4].to_s == 'dve'
          #obsahuje sipku s 1 a vic
          search_jedno.map!{|e| e = '(' + e + ' and (fn:matches($r/@fsw, "S2[2-9a-f][0-9a-f]2") or (fn:matches($r/@fsw, "S2[2-9a-f][0-9a-f]0") and fn:matches($r/@fsw, "S2[2-9a-f][0-9a-f]1")) or (not(fn:matches($r/@fsw, "S2[2-9a-f][0-9a-f]")) and (fn:matches($r/@fsw, "S20[567bcd]") or fn:matches($r/@fsw, "S21[123]")) ) ))'}
          search_obe_ruzne.map!{|e| e = '(' + e + ' and (fn:matches($r/@fsw, "S2[2-9a-f][0-9a-f]2") or (fn:matches($r/@fsw, "S2[2-9a-f][0-9a-f]0") and fn:matches($r/@fsw, "S2[2-9a-f][0-9a-f]1")) or (not(fn:matches($r/@fsw, "S2[2-9a-f][0-9a-f]")) and (fn:matches($r/@fsw, "S20[567bcd]") or fn:matches($r/@fsw, "S21[123]")) ) ))'}
          search_obe_stejne.map!{|e| e = '(' + e + ' and (fn:matches($r/@fsw, "S2[2-9a-f][0-9a-f]2") or (fn:matches($r/@fsw, "S2[2-9a-f][0-9a-f]0") and fn:matches($r/@fsw, "S2[2-9a-f][0-9a-f]1")) or (not(fn:matches($r/@fsw, "S2[2-9a-f][0-9a-f]")) and (fn:matches($r/@fsw, "S20[567bcd]") or fn:matches($r/@fsw, "S21[123]")) ) ))'}
        end
      end
      if search_args[2].to_s == 'j'
        #jednorucni
        search_query = search_jedno.join(' or ')
      elsif search_args[2].to_s == 's'
        #obe stejne
        search_query = search_obe_stejne.join(' or ')
      else
        #obe ruzne
        search_query = search_obe_ruzne.join(' or ')
      end
      #jen misto bez ruky
      if search_query == '' and not search_loc.nil?
        search_query = search_loc.join(' or ') 
      end
      search_query = '1=1' if search_query == ''

      #if search_args[2].to_s == 's' or search_args[1].to_s.include?('ruka')
      #  #symetricke nebo umisteni ruka
      #  search_query = search_symetric.join(' or ')
      #elsif search_args[2].to_s == 'j'
      #  #jednorucni
      #  search_query = search_jedno.join(' or ')
      #else
      #  #asymetricke = zbytek
      #  search_query = '(' + search_asymetric.join(' or ') + ') and not('+search_jedno.join(' or ')+') and not('+search_symetric.join(' or ')+')'
      #end
      if slovni_druh != ''
        if search_args[3].to_s == 'jednoduchy'
          #jednoduchy znak
          query = 'for $y in collection("'+@dictcode+'")/entry where ($y/lemma/grammar_note/@slovni_druh="'+slovni_druh+'") and $y/lemma/lemma_type="single" and $y/lemma/completeness!="1" and (some $r in $y/lemma/sw|$y/lemma/swmix/sw satisfies ('+search_query+')) return data($y/@id)'
        elsif search_args[3].to_s == 'slozeny'
          #slozeny znak
          query = 'for $y in collection("'+@dictcode+'")/entry where ($y/lemma/grammar_note/@slovni_druh="'+slovni_druh+'") and $y/lemma/lemma_type!="single" and $y/lemma/completeness!="1" and (some $r in $y/lemma/sw|$y/lemma/swmix/sw satisfies ('+search_query+')) return data($y/@id)'
        else
          query = 'for $y in collection("'+@dictcode+'")/entry where ($y/lemma/grammar_note/@slovni_druh="'+slovni_druh+'") and $y/lemma/completeness!="1" and (some $r in $y/lemma/sw|$y/lemma/swmix/sw satisfies ('+search_query+')) return data($y/@id)'
        end
      else
        if search_args[3].to_s == 'jednoduchy'
          #jednoduchy znak
          query = 'for $y in collection("'+@dictcode+'")/entry where $y/lemma/lemma_type="single" and $y/lemma/completeness!="1" and (some $r in $y/lemma/sw|$y/lemma/swmix/sw satisfies ('+search_query+')) return data($y/@id)'
        elsif search_args[3].to_s == 'slozeny'
          #slozeny znak
          query = 'for $y in collection("'+@dictcode+'")/entry where $y/lemma/lemma_type!="single" and $y/lemma/completeness!="1" and (some $r in $y/lemma/sw|$y/lemma/swmix/sw satisfies ('+search_query+')) return data($y/@id)'
        else
          query = 'for $y in collection("'+@dictcode+'")/entry where $y/lemma/completeness!="1" and (some $r in $y/lemma/sw|$y/lemma/swmix/sw satisfies ('+search_query+')) return data($y/@id)'
        end
      end
    end

    $stderr.puts query
    ids = []
    collocs = {}
    dbresult = @sedna.query(query)
    moreresults = false
    if type == 'text'
      origcount = dbresult.size
      moreresults = true if perm == 'ro' and origcount >= 500
      moreresults = true if perm != 'ro' and origcount >= 200
    end
    origcount = dbresult.size

    dbresult.each{|id|
      $stderr.puts id
      next if ids.include?(id)
      #xml = get_follow(@dictcode, id.to_s)
      xml = get( id.to_s)
      #$stderr.puts xml
      doc = load_xml_string(xml)
      $stderr.puts 'PERM='+perm
      $stderr.puts 'public='+is_public.to_s
      $stderr.puts 'skup='+user_skupiny
      $stderr.puts 'skup='+doc.find('/entry/lemma/pracskupina').first.content unless doc.find('/entry/lemma/pracskupina').first.nil?

      if doc.find('/entry/lemma/pracskupina').first != nil and (user_skupiny == '' or (not user_skupiny.include?(doc.find('/entry/lemma/pracskupina').first.content.to_s))) and not user_skupiny.include?('all')  #pokud ma heslo a uzivatel jine skupiny, delaji se dalsi kontroly. (stejna skupina = nepreskakuje se nezverejnene heslo)
        #next if perm == 'ro' and doc.find('/entry/lemma/status').first != nil and doc.find('/entry/lemma/status').first.content.to_s == 'hidden' and doc.find('/entry/lemma/completeness').first != nil and (doc.find('/entry/lemma/completeness').first.content != '100' and doc.find('/entry/lemma/completeness').first.content != '2')
        next if perm == 'ro' and (doc.find('/entry/lemma/completeness').first.nil? or doc.find('/entry/lemma/completeness').first.content == '1')
        next if perm == 'ro' and doc.find('/entry/lemma').first['auto_complete'] == '0'
      end
      next if doc.find('/entry/lemma').first['auto_complete'] == '0' and doc.find('/entry/lemma/pracskupina').first != nil and not perm.include?('admin') and not user_skupiny.include?('all') and not user_skupiny.include?(doc.find('/entry/lemma/pracskupina').first.content.to_s) #kontrola skupiny hesla a uzivatele
      ids << id
      info, collocs = parse_entry_search(id, doc, perm, collocs, type)
      include_colloc = false
      if info['is_colloc'] == '1'
        cid = info['colloc_parts'][0]
        if cid != nil and not ids.include?(cid)
          xml = get(cid)
          doc = load_xml_string(xml)
          unless doc.nil? 
            next if perm == 'ro' and doc.find('/entry/lemma').first['auto_complete'] == '0'
            ids << cid
            info2, collocs = parse_entry_search(cid, doc, perm, collocs, type)
            list[info2['id']] = info2
            list_to_sort << info2 
            include_colloc = true
          end
        end
      end
      list[info['id']] = info if not info['is_colloc'] == '1' or include_colloc
      list_to_sort << info if info['lemma_type'] != 'collocation'
    }
    $stderr.puts query
    $stderr.puts ids
    count = list_to_sort.size
    

    #sort
    #setridime jen single hesla
    case type
    when 'text'
      top = []
      list_to_sort.select{|h| h['trans_sort'].include?(search_term.downcase_utf8)}.each{|h|
        top << h
        list_to_sort.delete(h)
      }
      top.sort!{|x,y|
        y['region_sort'].to_i <=> x['region_sort'].to_i
      }
      list_to_sort.sort!{|x,y|
        pocetx = (x['translation'].count(' ') == 0) ? (x['translation'].to_s.downcase_utf8.start_with?(search_term.downcase_utf8) ? 0 : 1) : 2
        pocety = (y['translation'].count(' ') == 0) ? (y['translation'].to_s.downcase_utf8.start_with?(search_term.downcase_utf8) ? 0 : 1) : 2
        sort_pocetslov = (pocetx <=> pocety)
        slovox = x['translation'].downcase_utf8.split(' ').index{|w|w.start_with?(search_term.downcase_utf8)}
        slovox = 1000 if slovox.nil?
        slovoy = y['translation'].downcase_utf8.split(' ').index{|w|w.start_with?(search_term.downcase_utf8)}
        slovoy = 1000 if slovoy.nil?
        sort_poradislov = (slovox <=> slovoy)
        sort_alfa = (x['translation'].downcase_utf8 <=> y['translation'].downcase_utf8)
        sort_region = (y['region_sort'].to_i <=> x['region_sort'].to_i)
        (sort_pocetslov == 0 ?  (sort_poradislov == 0 ? (sort_alfa == 0 ? sort_region : sort_alfa) : sort_poradislov) : sort_pocetslov)
      }
      newlist = top + list_to_sort
    when 'sw','img'
      top = []
      top_ids = []
      newlist = []
      $stderr.puts 'search='+search_term
      $stderr.puts 'search='+get_signs(search_term).to_s if type == 'sw'
      list_to_sort.map!{|h|
        h['sw_sort_size'] = h['sw_search'].length
        h['sw_sort_size'] = 1000 if h['sw_search'].length == 0
        h['sw_sort_size'] = 1 if h['sw_primary'] != ''
        if h['sw_search'].length == 1 or h['sw_primary'] != ''
          if h['sw_primary'] != ''
            sw_to_check = h['sw_primary']
            fsw_to_check = h['fsw_primary']
          else
            sw_to_check = h['sw_search'].first['sw']
            fsw_to_check = h['sw_search'].first['fsw']
          end
          if type=='sw' and get_signs(sw_to_check) == get_signs(search_term) and h['art_misto'] == 'neutral'
            top << h
            top_ids << h['id']
          end
          if type == 'img' and h['art_misto'] == 'neutral' 
            search_shape.each{|shape|
              if fsw_to_check.include?(shape) 
                top << h
                top_ids << h['id']
                break
              end
            }
          end
        end
        h
      }

      list_to_sort.delete_if{|x| top_ids.include?(x['id'])}

      $stderr.puts list_to_sort
      list_to_sort.sort!{|x,y|
        if sort_order == 'misto'
          comp = (y['art_misto_sort'] <=> x['art_misto_sort'])
          comp.zero? ? (x['sw_sort_size'] <=> y['sw_sort_size']) : comp
        else 
          comp = (x['sw_sort_size'] <=> y['sw_sort_size'])
          comp.zero? ? (y['art_misto_sort'] <=> x['art_misto_sort']) : comp
        end
      }
      $stderr.puts 'TOP='
      top.each{|t| $stderr.puts t['id']}
      $stderr.puts 'REST='
      list_to_sort.each{|t| $stderr.puts t['id']+'-'+t['sw_sort_size'].to_s+'a'+t['art_misto_sort'].to_s+'='+t['art_misto']}
      newlist = top + list_to_sort
    end

    ids_used = []
    #pridame slovni spojeni
    resultlist = []
    entrycount = -1
    newlist.each{|entry|
      eid = entry['id'].to_s
      #$stderr.puts entrycount.to_s+'<'+(page*page_entries).to_s+'>'+(page*page_entries+page_entries).to_s+'---'+eid.to_s
      #$stderr.puts ids_used.to_s
      next if ids_used.include?(eid)
      ids_used << eid
      entrycount +=1
      next if entrycount < page*page_entries
      break if entrycount >= page*page_entries+page_entries
      #$stderr.puts '---S'+eid.to_s
      resultlist << entry
      unless collocs[eid].nil?
        collocs[eid].each{|cid|
          ids_used << cid
          resultlist << list[cid] unless list[cid].nil?
        }
      end
    }

    #pridame zbytek
    list.each{|eid,entry|
      entrycount +=1
      next if ids_used.include?(eid)
      next if entrycount < page*page_entries
      break if entrycount >= page*page_entries+page_entries
      resultlist << entry 
    }

    return resultlist, count, moreresults
  end

  def search_all(search_term, type, perm='ro', page=0, user_skupiny='', is_public=false, sort_order='misto', slovni_druh='')
    page_entries = 5
    list = {}
    list_to_sort = []
    #hledani
    search_query = 'for $y in collection("'+@dictcode+'")/entry let $size:= if (count($y/lemma/swmix/sw)=0) then 1000 else if ($y/lemma/swmix/sw[@primary="true"]) then 1 else count($y/lemma/swmix/sw) where $y/lemma/lemma_type != "collocation" and ($y/lemma/completeness!="1" and $y/lemma/@auto_complete="1") order by $y/lemma/lemma_type descending,$size,string-length($y/lemma/swmix/sw[1]/@fsw) return data($y/@id)'

    query = 'for $entry in subsequence('+search_query+', '+(1+page*page_entries).to_s+', '+page_entries.to_s+') return $entry'
    $stderr.puts query

    count_query = 'count('+search_query+')'
    count = @sedna.query(count_query).first.to_i

    $stderr.puts query
    ids = []
    list = []

    @sedna.query(query).each{|id|
      $stderr.puts id
      next if ids.include?(id)
      #xml = get_follow(@dictcode, id.to_s)
      xml = get( id.to_s)
      doc = load_xml_string(xml)
      ids << id
      info, collocs = parse_entry_search(id, doc, perm, collocs, type)
      list << info if info['lemma_type'] != 'collocation'
    }
    
    return list, count, count
  end


  def parse_entry_search(entryid, doc, perm, collocs, type)
    info = {'id'=>entryid.to_s}
    info['lemma_type'] = 'single'
    info['lemma_type'] = doc.root.find('/entry/lemma/lemma_type').first.content.to_s unless doc.root.find('/entry/lemma/lemma_type').first.nil?
    info['region'] = doc.root.find('/entry/lemma/grammar_note').first['region'].to_s unless doc.root.find('/entry/lemma/grammar_note').first.nil?
    info['region_sort'] = (info['region']=='cr'?3:0) + (info['region']=='cechy'?2:0) + (info['region']=='morava'?1:0) 
    if info['lemma_type'] == 'collocation'
      info['colloc_parts'] = []
      selected_colloc = nil
      doc.root.find('/entry/collocations/colloc').each{|col|
        if col['lemma_id'].to_s != ''
          info['colloc_parts'] << col['lemma_id'].to_s
          selected_colloc = col['lemma_id'].to_s if selected_colloc.nil? and col['auto_complete'] == '1'
          collocs[col['lemma_id'].to_s] = [] if collocs[col['lemma_id'].to_s].nil?
          collocs[col['lemma_id'].to_s] << entryid
        end
      }
      info['is_colloc'] = '1' 
      selected_colloc = info['colloc_parts'][0] if selected_colloc.nil? and info['colloc_parts'].size > 0
      info['selected_colloc'] = selected_colloc
    end
    unless doc.root.find('/entry/lemma/video_front').to_a.first.nil?
      info['video'] = doc.root.find('/entry/lemma/video_front').to_a.first.content.to_s 
      info['video_type'] = (info['video'][-3,3] == 'mp4')? 'video/mp4':'video/flash'
      info['is_flv'] = '1' if info['video'][-3,3] == 'flv'
      info['is_mp4'] = '1' if info['video'][-3,3] == 'mp4'
    end
    info['preklad_neznamy'] = '1' if doc.root.find('/entry/meanings/meaning[is_translation_unknown="1"]').size > 0
    info['hamnosys'] = doc.root.find('/entry/lemma/hamnosys').to_a.first.content.to_s unless doc.root.find('/entry/lemma/hamnosys').to_a.first.nil?
    info['sw'] = []
    info['art_misto'] = ''
    info['sw_primary'] = ''
    info['fsw_primary'] = ''
    info['sw_search'] = []
    doc.root.find('/entry/lemma/swmix/sw').each{|sw|
      info['art_misto'] = sw['misto'].to_s if info['lemma_type'] == 'kompozitum' and info['art_misto'] == '' and sw['misto'].to_s != ''
      info['sw'] << {'sw'=>sw.content.to_s, 'fsw'=>sw['fsw'].to_s} unless sw.content.to_s == ''
    }
    doc.root.find('/entry/lemma/sw').each{|sw|
      info['art_misto'] = sw['misto'].to_s if (info['art_misto'] == '' or sw['primary'] == 'true') and sw['misto'].to_s != ''
      info['sw_search'] << {'sw'=>sw.content.to_s, 'fsw'=>sw['fsw'].to_s} unless sw.content.to_s == ''
      info['sw_primary'] = sw.content.to_s if sw['primary'] == 'true'
      info['fsw_primary'] = sw['fsw'].to_s if sw['primary'] == 'true'
    }
    info['sw_search'] = info['sw'] if info['lemma_type'] == 'kompozitum'
    info['art_misto_sort'] = (info['art_misto']=='neutral'?17:0) + (info['art_misto']=='hlava'?16:0) + (info['art_misto']=='oblicej'?15:0) + (info['art_misto']=='temeno'?14:0) + (info['art_misto']=='celo'?13:0) + (info['art_misto']=='oci'?12:0) + (info['art_misto']=='nos'?11:0) + (info['art_misto']=='usi'?10:0) + (info['art_misto']=='tvare'?9:0) + (info['art_misto']=='usta'?8:0) + (info['art_misto']=='brada'?7:0) + (info['art_misto']=='krk'?6:0) + (info['art_misto']=='hrud'?5:0) + (info['art_misto']=='paze'?4:0) + (info['art_misto']=='ruka'?3:0) + (info['art_misto']=='pas'?2:0) + (info['art_misto']=='dolni'?1:0)
    if doc.root.find('/entry/lemma/grammar_note/variant|/entry/lemma/style_note/variant').size > 0
      info['variants'] = []
      varcount = 0
      doc.root.find('/entry/lemma/grammar_note/variant|/entry/lemma/style_note/variant').each{|var|
        varcount += 1
        variant = {'label'=>'V'+varcount.to_s, 'varid'=>var.content.to_s}
        doc.root.find('//file[@id="'+var.content.to_s+'"]').each{|ve|
          variant['file'] = ve.find('location').first.content.to_s unless ve.find('location').first.nil?
          variant['video_type'] = (variant['file'][-3,3] == 'mp4')? 'video/mp4':'video/flash'
          variant['is_flv'] = '1' if variant['file'][-3,3] == 'flv'
          variant['is_mp4'] = '1' if variant['file'][-3,3] == 'mp4'
          variant['sw'] = []
          ve.find('main_for_entry/swmix/sw').each{|vsw|
            variant['sw'] << {'sw'=>vsw.content.to_s} unless vsw.content.to_s == ''
          }
        }
        info['variants'] << variant
      }
    end
    if type == 'text'
      trans = []
      trans_sort = []
      transdia = []
      doc.root.find('//relation[@type="translation"]/title').each{|sw|
        trans << sw.content.to_s
        trans_sort << sw.content.to_s.downcase_utf8
      }
      doc.root.find('//relation[@type="translation"]/title_dia').each{|sw|
        transdia << sw.content.to_s
      }
      info['translation'] = trans.uniq.join(', ')
      info['translation_dia'] = transdia.uniq.join(', ')
      info['trans_sort'] = trans_sort
      langs = []
      doc.root.find('//relation[@type="translation"]').each{|tr|
        langs << tr['target'] unless tr['target'].to_s == ''
      }
      langs.uniq.each{|lang|
        trans = []
        doc.root.find('//relation[@type="translation" and @target="'+lang+'"]/title').each{|sw|
          trans << sw.content.to_s
        }
        info['translation_'+lang] = trans.uniq.join(', ')
      }
    end
    info['is_editor'] = '1' if perm.include?('edit') or perm.include?('admin')
    if info['video']
      lab = []
      doc.root.find('//file[location="'+info['video']+'"]').each{|ve|
        lab << ve.find('label').first.content.to_s unless ve.find('label').first.nil?
      }
      doc.root.find('//variant').each{|va|
        doc.root.find('//file[@id="'+va.content.to_s+'"]').each{|ve|
          lab << ve.find('label').first.content.to_s unless ve.find('label').first.nil?
        }
      }
      info['label'] = lab.uniq.join(', ')
    end
    return info, collocs
  end

  def full_sw()
  end

  def get_follow(nowdict, id, colloc_only=false, xml=nil, perm='ro')
    $stderr.puts 'FOLLOW'+id.to_s
    xml = get(id) if xml.nil?
    return if xml.to_s.strip == ''
    doc = load_xml_string(xml.to_s)
    return if doc.nil?
    doc.root.find('//swmix').each{|sw|
      sw.remove!
    }
    doc.root.find('//revcolloc').each{|sw|
      sw.remove!
    }
    unless colloc_only
      #more info for relations
      doc.root.find('//relation').each{|re|
        #$stderr.puts re
        if re['type'] == 'translation' or re['type'] == 'translation_colloc'
          target = re['target'].to_s if re['target'] != ''
          target = (nowdict != 'cs')?'cs':'czj' if target == ''
        else
          target = nowdict
        end
        $stderr.puts target
        follow_relation(target, re)
        $stderr.puts re
      }
      #find collocations for id
      doc.root << XML::Node.new('collocations') if doc.root.find('//collocations').first.nil?
      colel = doc.root.find('collocations').first
      if perm == 'ro'
        colquery = 'collection("'+nowdict+'")/entry[collocations/colloc/@lemma_id="'+id+'" and collocations/@status!="hidden"]/data(@id)'
      else
        colquery = 'collection("'+nowdict+'")/entry[collocations/colloc/@lemma_id="'+id+'"]/data(@id)'
      end
      @sedna.query(colquery).each{|e|
        $stderr.puts 'rev>'+e.to_s
        newrev = XML::Node.new('revcolloc')
        newrev['lemma_id'] = e
        colel << newrev
      }
    end
    #fill info for colloc
    doc.root.find('//colloc|//revcolloc').each{|re|
      follow_relation(nowdict, re) unless id == re['lemma_id']
    }
    #fill info for variant
    if nowdict == 'cs' or nowdict == 'en' or nowdict == 'sj'
      doc.root.find('//variant').each{|re|
        follow_variant(nowdict, re)
      }
    end
    #heslo bez diakritiky
    unless doc.root.find('/entry/lemma/title').first.nil?
      doc.root.find('/entry/lemma/title_dia').first.remove! unless doc.root.find('/entry/lemma/title_dia').first.nil?
      doc.root.find('/entry/lemma').first << XML::Node.new('title_dia', strip_dia(doc.root.find('/entry/lemma/title').first.content))
    end
    #combine SW
    newsw = XML::Node.new('swmix')
    doc.root.find('lemma').first << newsw
    if not doc.root.find('lemma/lemma_type').first.nil? and (doc.root.find('lemma/lemma_type').first.content.to_s == 'derivat' or doc.root.find('lemma/lemma_type').first.content.to_s == 'collocation' or doc.root.find('lemma/lemma_type').first.content.to_s == 'kompozitum' or doc.root.find('lemma/lemma_type').first.content.to_s == 'fingerspell')
      collar = doc.root.find('collocations/colloc').to_a
      swar = doc.root.find('lemma/sw').to_a
      $stderr.puts 'swcompos'
      if doc.root.find('collocations/swcompos').first.nil? or doc.root.find('collocations/swcompos').first.content.to_s == ''
        $stderr.puts 'sw prazdne'
        collar.each{|ce|
          if ce.find('swmix').first.nil?
            ce.find('sw').each{|swel|
              newsw << swel.copy(true)
            }
          else
            ce.find('swmix/sw').each{|swel|
              newsw << swel.copy(true)
            }
          end
        }
      else
        doc.root.find('collocations/swcompos').first.content.to_s.split(',').each{|swid|
          swid.strip!
          $stderr.puts 'sw part '+swid
          if swid[0,2].upcase == 'SW'
            #copy from this entry
            swn = swid[2..-1].to_i-1
            newsw << swar[swn].copy(true) unless swar[swn].nil?
          elsif swid.upcase =~ /^[A-Z]$/
            #copy from this entry
            $stderr.puts 'get SW char from this entry ' + swid + ' = ' + (swid[0].ord-65).to_s
            swn = swid[0].ord-65
            newsw << swar[swn].copy(true) unless swar[swn].nil?
          else
            #copy from part
            if swid.upcase =~ /[A-Z]/
              #copy one char
              match = /([0-9]+)([A-Z]+)/.match(swid.upcase)
              unless match.nil?
                $stderr.puts 'copy char '+swid+' ('+(match[1].to_i-1).to_s+':'+(match[2][0].ord-65).to_s+')'
                unless collar[match[1].to_i-1].nil?
                  unless collar[match[1].to_i-1].find('sw').first.nil?
                    newsw << collar[match[1].to_i-1].find('sw').to_a[match[2][0].ord-65].copy(true) unless collar[match[1].to_i-1].find('sw').to_a[match[2][0].ord-65].nil?
                  else
                    newsw << collar[match[1].to_i-1].find('swmix/sw').to_a[match[2][0].ord-65].copy(true) unless collar[match[1].to_i-1].find('swmix/sw').to_a[match[2][0].ord-65].nil?
                  end
                end
              end
            else
              #copy full
              $stderr.puts 'copy full '+swid
              unless collar[swid.to_i-1].nil?
                if collar[swid.to_i-1].find('swmix').first.nil?
                  collar[swid.to_i-1].find('sw').each{|swel|
                    newsw << swel.copy(true)
                  }
                else
                  collar[swid.to_i-1].find('swmix/sw').each{|swel|
                    newsw << swel.copy(true)
                  }
                end
              end
            end
          end
        }
      end
    else
      if doc.root.find('lemma/sw[@primary="true"]').size == 0
        doc.root.find('lemma/sw').each{|swel|
          newsw << swel.copy(true)
        }
      else
        newsw << doc.root.find('lemma/sw[@primary="true"]').first.copy(true)
      end
    end

    files = []
    #varianty SW
    collar = doc.root.find('collocations/colloc').to_a if collar.nil?
    swar = doc.root.find('lemma/sw').to_a if swar.nil?
    doc.root.find('/entry/lemma/grammar_note/variant|/entry/lemma/style_note/variant').each{|gvar|
      gvar.find('swmix').each{|sw|
        sw.remove!
      }
      #if doc.root.find('//media/file[@id="'+gvar.content.to_s+'"]').first.nil?
        files << gvar.content.to_s
      #end
      if gvar['sw'] != ''
        newsw = XML::Node.new('swmix')
        gvar << newsw
        gvar['sw'].to_s.split(',').each{|swid|
          swid.strip!
          $stderr.puts 'sw part '+swid
          if swid[0,2].upcase == 'SW'
            #copy from this entry
            swn = swid[2..-1].to_i-1
            newsw << swar[swn].copy(true) unless swar[swn].nil?
          elsif swid.upcase =~ /^[A-Z]$/
            #copy from this entry
            $stderr.puts 'get SW char from this entry ' + swid + ' = ' + (swid[0].ord-65).to_s
            swn = swid[0].ord-65
            newsw << swar[swn].copy(true) unless swar[swn].nil?
          else
            #copy from part
            if swid.upcase =~ /[A-Z]/
              #copy one char
              match = /([0-9]+)([A-Z]+)/.match(swid.upcase)
              unless match.nil?
                $stderr.puts 'copy char '+swid+' ('+(match[1].to_i-1).to_s+':'+(match[2][0].ord-65).to_s+')'
                unless collar[match[1].to_i-1].nil?
                  unless collar[match[1].to_i-1].find('sw').first.nil?
                    newsw << collar[match[1].to_i-1].find('sw').to_a[match[2][0].ord-65].copy(true) unless collar[match[1].to_i-1].find('sw').to_a[match[2][0].ord-65].nil?
                  else
                    newsw << collar[match[1].to_i-1].find('swmix/sw').to_a[match[2][0].ord-65].copy(true) unless collar[match[1].to_i-1].find('swmix/sw').to_a[match[2][0].ord-65].nil?
                  end
                end
              end
            else
              #copy full
              $stderr.puts 'copy full '+swid
              unless collar[swid.to_i-1].nil?
                if collar[swid.to_i-1].find('swmix').first.nil?
                  collar[swid.to_i-1].find('sw').each{|swel|
                    newsw << swel.copy(true)
                  }
                else
                  collar[swid.to_i-1].find('swmix/sw').each{|swel|
                    newsw << swel.copy(true)
                  }
                end
              end
            end
          end
        }
      end
    }

    #add file metadata
    doc.root.find('/entry/media/file').each{|me|
      me.remove!
    }
    doc.root.find('//video_front|//video_side').each{|file|
      $stderr.puts 'find media '+@dictcode+' '+file.content.to_s
      @admindict.array[@dictcode+'_media'].xquery_to_hash('[media[location="'+file.content.to_s+'"]]').each{|mid,mx|
        if doc.root.find('//media/file[@id="'+mid+'"]').first.nil?
          files << mid
        end
      }
    }
    doc.root.find('//file[@media_id!=""]').each{|file|
      $stderr.puts 'find media'+file['media_id'].to_s
      if doc.root.find('//media/file[@id="'+file['media_id']+'"]').first.nil?
        files << file['media_id'].to_s
      end
    }
    #variant
    usedvars = []
    doc.root.find('//style_note/variant').each{|file|
      if usedvars.include?(file.content.to_s)
        file.remove!
      else
        usedvars << file.content.to_s
      end
    }
    usedvars = []
    doc.root.find('//grammar_note/variant').each{|file|
      if usedvars.include?(file.content.to_s)
        file.remove!
      else
        usedvars << file.content.to_s
      end
    }
    files.uniq.each{|file|
      next if @admindict.array[@dictcode+'_media'].nil?
      res = @admindict.array[@dictcode+'_media'].get(file)
      next if res == ''
      res = res.sub('<media ', '<file ').sub('</media>', '</file>')
      docf = load_xml_string(res)
      unless docf.find('//location').first.nil?
        loc = docf.find('//location').first.content.to_s
        xquery_to_hash('[entry[(lemma/video_front="'+loc+'" or lemma/video_side="'+loc+'") ]]').each{|eid,exml|
          next if eid == id
          docrel = load_xml_string(get(eid))
          maine = XML::Node.new('main_for_entry')
          maine['lemma_id'] = eid
          maine['auto_complete'] = ''
          maine['auto_complete'] = docrel.root.find('lemma').first['auto_complete'].to_s unless docrel.root.find('lemma').first.nil?
          maine['completeness'] = ''
          maine['completeness'] = docrel.root.find('lemma/completeness').first.content.to_s unless docrel.root.find('lemma/completeness').first.nil?
          maine['lemma_type'] = ''
          maine['lemma_type'] = docrel.root.find('lemma/lemma_type').first.content.to_s unless docrel.root.find('lemma/lemma_type').first.nil?
          maine['lemma_region'] = docrel.root.find('lemma/grammar_note').first['region'].to_s unless docrel.root.find('lemma/grammar_note').first.nil?
          docf.root << maine
          docrel.root.find('lemma/hamnosys|lemma/sw|lemma/swmix').each{|h|
            maine << h.copy(true)
          }

        }
      end
      newme = doc.import(docf.root)
      doc.root << XML::Node.new('media') if doc.root.find('/entry/media').first.nil?
      doc.root.find('/entry/media').first << newme
    }
    doc.root.find('/entry/media[position() > 1]/file').each{|fi|
      doc.root.find('/entry/media').first << fi.copy(true)
      fi.remove!
    }

    #doplnit region k variantam
    doc.root.find('//grammar_note/variant|//style_note/variant').each{|var|
      ventry = doc.root.find('//file[@id="'+var.content.to_s+'" and main_for_entry]/main_for_entry').first
      unless ventry.nil?
        var['lemma_region'] = ventry['lemma_region'].to_s
      end
    }

    #odkazy v prikladech
    doc.root.find('//usage[@colloc_link!="" and not(relation)]').each{|us|
      target = (nowdict != 'cs')?'cs':'czj'
      us.find('relation').each{|ur| ur.remove!}
      ur = XML::Node.new('relation')
      ur['meaning_id'] = us['colloc_link']
      ur['type'] = 'translation'
      ur['target'] = target
      us << ur
      follow_relation(target, ur)
    }

    #kontrola uplnosti, auto_complete =0 neuplne, =1 uplne
    complete_video_front = false
    complete_video_side = false
    complete_trans = false
    complete_vyklad = false
    complete_preklad = false
    complete_priklad = false
    #obe videa?
    vf = doc.root.find('/entry/lemma/video_front')
    if vf.size > 0
      vfv = vf.first.content.to_s
      unless doc.root.find('/entry/media/file[location="'+vfv+'"]').first.nil?
        complete_video_front = true if doc.root.find('/entry/media/file[location="'+vfv+'"]').first.find('status').first.content == 'published'
      end
    end
    vs = doc.root.find('/entry/lemma/video_side')
    if vs.size > 0
      vsv = vs.first.content.to_s
      unless doc.root.find('/entry/media/file[location="'+vsv+'"]').first.nil?
        complete_video_side = true if doc.root.find('/entry/media/file[location="'+vsv+'"]').first.find('status').first.content == 'published'
      end
    end
    #transkripce?
    complete_trans = true if (doc.root.find('/entry/lemma/sw').size > 0 or doc.root.find('/entry/lemma/swmix').size > 0) and (doc.find('/entry/lemma').first['swstatus'] == 'published' or (doc.root.find('/entry/lemma/lemma_type').first != nil and doc.root.find('/entry/lemma/lemma_type').first.content != 'single'))
    complete_trans = true if doc.root.find('/entry/lemma/hamnosys').size > 0 and doc.root.find('/entry/lemma/hamnosys').first['status'] == 'published'
    #vyklad?
    complete_vyklad = true if doc.root.find('/entry/meanings/meaning[status="published"]').size > 0
    #preklad?
    complete_preklad = true if doc.root.find('/entry/meanings/meaning[status="published"]/relation[@type="translation" and @status="published"]').size > 0
    complete_preklad = true if doc.root.find('/entry/meanings/meaning[status="published" and is_translation_unknown="1"]').size > 0
    #priklad?
    complete_priklad = true if doc.root.find('/entry/meanings/meaning[status="published"]/usages/usage[status="published" and (text!="" or text/file)]').size > 0

    auto_complete = '0'
    auto_complete = '1' if (nowdict=='czj' or nowdict=='is' or nowdict=='asl' or nowdict=='spj') and complete_video_front and complete_video_side and complete_trans and complete_vyklad and complete_preklad and complete_priklad
    auto_complete = '1' if (nowdict=='cs' or nowdict=='en' or nowdict=='sj') and complete_vyklad and complete_priklad
    auto_complete = '1' if doc.root.find('/entry/lemma/completeness').first != nil and (doc.root.find('/entry/lemma/completeness').first.content.to_s == '100' or doc.root.find('/entry/lemma/completeness').first.content.to_s == '2')
    doc.root.find('/entry/lemma').first['auto_complete'] = auto_complete

    #komentare
    doc.root.find('/entry/comments').first.remove! unless doc.root.find('/entry/comments').first.nil?
    doc.root << XML::Node.new('comments')
    comroot = doc.root.find('/entry/comments').first
    @admindict.array['czj_koment'].get_comments(nowdict, '', id, '').each{|ch|
      comroot['last'] = ch['user'] if comroot['last'].to_s == ''
      if comroot.find('box[@box="'+ch['box']+'"]').size == 0
        nb = XML::Node.new('box')
        nb['box'] = ch['box']
        nb['last'] = ch['user']
        comroot << nb
      end
      cn = XML::Node.new('comment')
      cn['user'] = ch['user']
      cn['box'] = ch['box']
      comroot.find('box[@box="'+ch['box']+'"]').first << cn
    }
    
    return doc.to_s
  end

  def follow_variant(target_dict, variant)
    xmlrel = @admindict.array[target_dict].get(variant.content).to_s
    if xmlrel != ''
      docrel = load_xml_string(xmlrel.to_s)
      variant['lemma_id'] = variant.content
      variant['title'] = docrel.find('lemma/title').to_a.first.content unless docrel.find('lemma/title').to_a.first.nil?
      unless docrel.find('lemma/grammar_note').first.nil?
        variant['lemma_region'] = docrel.find('lemma/grammar_note').first['region'].to_s
      end
    else
      variant['title'] = variant.content
    end
  end

  def follow_relation(target_dict, relation)
    $stderr.puts relation
    $stderr.puts 'TARGET ' + target_dict + relation['lemma_id'].to_s + relation['type'].to_s
    colloc_link = false
    if relation['lemma_id'].nil?
      xmlrel = ''
      query = '[entry[meanings/meaning/@id="'+relation['meaning_id'].to_s.gsub('"','&quot;')+'"]]'
      res = @admindict.array[target_dict].xquery_to_hash(query)
      #xmlrel = get_follow(target_dict, res.keys[0], true, res.values[0].to_s) unless res.first.nil?
      #zkusime priklad
      if res.first.nil? #and (target_dict == 'cs' or target_dict == 'en')
        query = '[entry[meanings/meaning/usages/usage/@id="'+relation['meaning_id'].to_s.gsub('"','&quot;')+'"]]'
        res = @admindict.array[target_dict].xquery_to_hash(query)
        if relation['meaning_id'].include?('_us') and res.size == 0 
          relation.remove!
          return
        end
        colloc_link = true
      end
      xmlrel = @admindict.array[target_dict].get(res.keys[0]) unless res.first.nil?
      $stderr.puts xmlrel
    else
      #xmlrel = @admindict.array[target_dict].get_follow(target_dict, relation['lemma_id'], true).to_s
      xmlrel = @admindict.array[target_dict].get(relation['lemma_id']).to_s
    end
    relation['title_only'] = ''
    if xmlrel.to_s == '' and relation['meaning_id'].to_s != '' and relation['type'] == 'translation' and (target_dict == 'cs' or target_dict == 'en' or target_dict == 'sj')
      relation.find('title').each{|rt| rt.remove!}
      relation.find('title_dia').each{|rt| rt.remove!}
      relation.find('title_var').each{|rt| rt.remove!}
      relation.find('form').each{|rt| rt.remove!}
      relation << XML::Node.new('title', relation['meaning_id'])
      relation << XML::Node.new('title_dia', strip_dia(relation['meaning_id']))
      relation['title_only'] = 'true'
      #relation['meaning_id'] = ''
    end
    if xmlrel.to_s == '' and relation['meaning_id'].to_s != '' and relation['type'] != 'translation' and (@dictcode == 'cs' or @dictcode == 'en')
      relation.find('title').each{|rt| rt.remove!}
      relation.find('title_dia').each{|rt| rt.remove!}
      relation.find('title_var').each{|rt| rt.remove!}
      relation.find('form').each{|rt| rt.remove!}
      relation << XML::Node.new('title', relation['meaning_id'])
      #relation['meaning_id'] = ''
      relation << XML::Node.new('title_dia', strip_dia(relation['meaning_id']))
      relation['title_only'] = 'true'
    end
    if xmlrel.to_s != ''
      #$stderr.puts '**************XMLREL'
      #$stderr.puts xmlrel
      #$stderr.puts '**************XMLREL!'
      docrel = load_xml_string(xmlrel.to_s)
      relation['completeness'] = docrel.find('lemma/completeness').first.content unless docrel.find('lemma/completeness').first.nil?
      relation['auto_complete'] = ''
      relation['auto_complete'] = docrel.find('lemma').first['auto_complete'].to_s unless docrel.find('lemma').first.nil?
      relation['front_video'] = ''
      relation['front_video'] = 'published' if docrel.find('/entry/media/file[location=/entry/lemma/video_front and status="published"]').first != nil
      unless docrel.find('lemma/grammar_note').first.nil?
        relation['lemma_region'] = docrel.find('lemma/grammar_note').first['region'].to_s
      end
      if relation['meaning_id'].to_s != '' and not relation['meaning_id'].to_s.include?('_us') and not docrel.find('meanings/meaning[@id="'+relation['meaning_id'].to_s+'"]').first.nil?
        relation['meaning_region'] = docrel.find('meanings/meaning[@id="'+relation['meaning_id'].to_s+'"]').first['style_region'].to_s
      end
      if target_dict == 'cs' or target_dict == 'en'
        relation.find('title').each{|rt| rt.remove!}
        relation.find('title_dia').each{|rt| rt.remove!}
        relation.find('title_var').each{|rt| rt.remove!}
        relation.find('form').each{|rt| rt.remove!}
        relation['lemma_id'] = docrel.root['id']
        relation['lemma_type'] = docrel.find('/entry/lemma/lemma_type').first.content.to_s unless docrel.find('/entry/lemma/lemma_type').first.nil?
        if colloc_link or relation['meaning_id'].to_s.include?('_us')
          usgrel = docrel.find('meanings/meaning/usages/usage[@id="'+relation['meaning_id'].to_s+'"]/text').to_a.first
          if usgrel.nil?
            relation.remove!
          else
            relation << XML::Node.new('title', usgrel.content) 
            relation['title_only'] = 'true'
            relation['lemma_type'] = 'usage'
          end
        else
          relation << docrel.find('lemma/title').to_a.first.copy(true) unless docrel.find('lemma/title').to_a.first.nil?
          relation << docrel.find('lemma/title_dia').to_a.first.copy(true) unless docrel.find('lemma/title_dia').to_a.first.nil?
          docrel.find('lemma/grammar_note/variant|lemma/style_note/variant').each{|rt|
            relation << XML::Node.new('title_var', rt['title'])
            relation << XML::Node.new('title_var', strip_dia(rt['title']))
          }
          docrel.find('lemma/gram/form').each{|rt|
            relation << rt.copy(true)
          }
          if relation['meaning_id'] != ''
            relme = docrel.find('meanings/meaning[@id="'+relation['meaning_id'].to_s+'"]').to_a.first
            relation['meaning_nr'] = relme['number'] if relme != nil and relme['number'].to_s != ''
            relation['meaning_count'] = docrel.find('meanings/meaning').size.to_s
          end
        end
      else
        relation.children.each{|rt| rt.remove!} 
        relation['lemma_id'] = docrel.root['id']
        relation['lemma_type'] = docrel.root.find('lemma/lemma_type').first.content.to_s unless docrel.root.find('lemma/lemma_type').first.nil?
        if relation['meaning_id'].to_s != ''
          relme = docrel.find('meanings/meaning[@id="'+relation['meaning_id'].to_s+'"]').to_a.first
          relation['meaning_nr'] = relme['number'] if relme != nil and relme['number'].to_s != ''
          relation['meaning_count'] = docrel.find('meanings/meaning').size.to_s
          relation['auto_complete'] = '0' if relme.nil? or relme.find('status').first.nil? or relme.find('status').first.content != 'published'
        end
        if relation['meaning_id'].to_s.include?('_us')
          usf = docrel.find('//usage[@id="'+relation['meaning_id'].to_s+'"]/text/file').first
          if not usf.nil? and usf['media_id'].to_s != ''
            docrel.find('media/file[@id="'+usf['media_id'].to_s+'"][1]').each{|h|
              relation << h.copy(true)
            }
          end
          relation['auto_complete'] = '0'
        else
          docrel.find('lemma/hamnosys|lemma/sw|lemma/swmix').each{|h|
            relation << h.copy(true)
          }
          if not docrel.root.find('lemma/video_front').first.nil? and docrel.root.find('lemma/video_front').first.content != ''
            docrel.find('media/file[location="'+docrel.root.find('lemma/video_front').first.content.to_s+'"][1]').each{|h|
              relation << h.copy(true)
            }
          end
        end
        #find translation
        ##for $x in collection("czj")//meaning[@id="2261"]/relation[@type="translation"] return collection("cs")/entry[meanings/meaning/@id=$x/@meaning_id]/lemma/title
        trans = []
        docrel.find('//relation[@type="translation" and (@target="en" or @target="cs")]').each{|rt|
          target = rt['target'].to_s
          target = 'cs' if target == ''
          follow_relation(target, rt)
          $stderr.puts rt
          trans << rt.find('title').to_a.first.content unless rt.doc.nil? or rt.find('title').to_a.first.nil?
        }
        relation << XML::Node.new('translation', trans.join(', '))
      end
    end
  end

  def get_video_info(location)
    query = '/media[location="'+location+'"]'
    $stderr.puts query
    res = @admindict.array[@dictcode+'_media'].xquery_to_array(query).first
    $stderr.puts res
    return res.to_s
  end

  def get_max_meaning
    return @sedna.query('max(for $x in collection("czj")//meaning return $x/@id)').first.to_s
  end
  def get_max_usage
    return @sedna.query('max(for $x in collection("czj")//usage return $x/@id)').first.to_s
  end

  def get_file_list(query, mediadict)
    list = []
    mediadict.xquery_to_array(query).each {|xml|
      docm = load_xml_string(xml.to_s)
      hash = {}
      hash['location'] = docm.root.find('location').to_a.first.content.to_s unless docm.root.find('location').to_a.first.nil?
      hash['id'] = docm.root['id']
      hash['type'] = docm.root.find('type').to_a.first.content.to_s unless docm.root.find('type').to_a.first.nil?
      hash['author'] = docm.root.find('id_meta_author').to_a.first.content.to_s unless docm.root.find('id_meta_author').to_a.first.nil?
      hash['copyright'] = docm.root.find('id_meta_copyright').to_a.first.content.to_s unless docm.root.find('id_meta_copyright').to_a.first.nil?
      hash['admin_comment'] = docm.root.find('admin_comment').to_a.first.content.to_s unless docm.root.find('admin_comment').to_a.first.nil?
      hash['source'] = docm.root.find('id_meta_source').to_a.first.content.to_s unless docm.root.find('id_meta_source').to_a.first.nil?
      hash['original'] = docm.root.find('original_file_name').to_a.first.content.to_s unless docm.root.find('original_file_name').to_a.first.nil?
      hash['orient'] = docm.root.find('orient').to_a.first.content.to_s unless docm.root.find('orient').to_a.first.nil?
      hash['status'] = docm.root.find('status').to_a.first.content.to_s unless docm.root.find('status').to_a.first.nil?
      #if File.exists?('/var/lib/deb-server/files/czj/media/'+entryid.to_s+'/'+hash['id'].to_s+'_thumb.jpg')
      #  hash['thumb'] = hash['id']+'_thumb.jpg'
      #end
      list << hash
    }
    return list
  end

  def find_files(search, mediadict, type)
    if search.length > 1
      case type
      when 'AB'
        query = '/media[(contains(location, "'+search+'") or contains(original_file_name, "'+search+'")) and (type="sign_front" or type="sign_side")]'
      when 'A'
        query = '/media[(contains(location, "'+search+'") or contains(original_file_name, "'+search+'")) and (type="sign_front")]'
      when 'K'
        query = '/media[(contains(location, "'+search+'") or contains(original_file_name, "'+search+'")) and (type="sign_usage_example")]'
      when 'D'
        query = '/media[(contains(location, "'+search+'") or contains(original_file_name, "'+search+'")) and (type="sign_definition")]'
      else
        query = '/media[contains(location, "'+search+'") or contains(original_file_name, "'+search+'")]'
      end
      return get_file_list(query, mediadict)
    end
    return []
  end

  def find_rel_cs(search)
    list = []
      xquery_to_array('/entry[lemma/completeness!="1" and (starts-with(lower-case(lemma/title), "'+search.downcase_utf8+'") or starts-with(lower-case(lemma/title_dia), "'+(search.downcase_utf8)+'"))]').each{|xml|
        doc = load_xml_string(xml.to_s)
        title = doc.find('//lemma/title').first.content.to_s
        doc.find('//meaning').each{|m|
          hash = {'title'=>title, 'number'=>m['number'].to_s, 'id'=>m['id']}
          hash['def'] = m.find('text').first.content.to_s unless m.find('text').first.nil?
          list << hash
        }
      }
    return list.sort_by{|x| [x['title'], x['number'].to_i]}
  end
  def find_link_cs(search)
    list = []
    if search.length > 2
      xquery_to_array('/entry[lemma/completeness!="1" and (starts-with(lower-case(lemma/title), "'+search.downcase_utf8+'") or starts-with(lower-case(lemma/title_dia), "'+(search.downcase_utf8)+'"))]').each{|xml|
        doc = load_xml_string(xml.to_s)
        title = doc.find('//lemma/title').first.content.to_s
        hash = {'title'=>title, 'id'=>doc.root['id']}
        list << hash
      }
    end
    return list.sort_by{|x| [x['title']]}
  end

  def find_link(search)
    list = []
    if search.length > 2
      #xquery_to_array('/entry[lemma/@auto_complete="1" and ((some $l in media/file/label satisfies contains($l, "'+search.downcase+'")) or @id="'+search+'")]').each{|xml|
      xquery_to_array('/entry[lemma/completeness!="1" and ((some $l in media/file satisfies ((contains($l/label, "'+search.downcase+'") or $l/label="'+search+'") and ($l/type="sign_side" or $l/type="sign_front" or $l/type="sign_definition"))) or @id="'+search+'" or (some $r in meanings/meaning/relation[@type="translation"] satisfies (starts-with($r/title, "'+search+'") or starts-with($r/title_dia, "'+search+'"))))]').each{|xml|
        doc = load_xml_string(xml.to_s)
        id = doc.root['id']
        label = ''
        label = doc.find('/entry/media/file/label[contains(., "'+search.downcase+'")]').first.content.to_s unless doc.find('/entry/media/file/label[contains(., "'+search.downcase+'")]').first.nil?
        front = ''
        front = doc.find('/entry/lemma/video_front').first.content unless doc.find('/entry/lemma/video_front').first.nil?
        tr = []
        #doc.find('//title').each{|t|
        #  tr << t.content
        #}
        list << {'id'=>id, 'title'=>tr.join('; '), 'label'=>label, 'loc'=>front}
      }
    end
    return list.sort_by{|x| [x['title'], x['label']]}
  end

  def find_rel(search, mediadict)
    list = []
    xquery_to_array('/entry[lemma/completeness!="1" and ((some $l in media/file satisfies ((contains($l/label, "'+search.downcase+'") or $l/label="'+search+'") and ($l/type="sign_side" or $l/type="sign_front" or $l/type="sign_definition"))) or @id="'+search+'" or (some $r in meanings/meaning/relation[@type="translation"] satisfies (starts-with($r/title, "'+search+'") or starts-with($r/title_dia, "'+search+'"))))]').each{|xml|
      #xml = get(search).to_s
      doc = load_xml_string(xml.to_s)
      label = doc.root['id'].to_s
      labelen = doc.find('/entry/media/file[location=/entry/lemma/video_front]/label').first
      label = labelen.content.to_s + ' ('+label+')' unless labelen.nil?
      front = ''
      front = doc.find('/entry/lemma/video_front').first.content unless doc.find('/entry/lemma/video_front').first.nil?
      doc.find('//meaning').each{|m|
        hash = {'title'=>label, 'number'=>m['number'].to_s, 'id'=>m['id'], 'def'=>'', 'target'=>@dictcode, 'front'=>front}
        if m.find('text//file').size > 0
          #xmlm = mediadict.get(m.find('text//file').first['media_id'].to_s)
          doc.find('/entry/media/file[@id="'+m.find('text//file').first['media_id'].to_s+'"]').each{|xmlm|
            #docm = load_xml_string(xmlm)
            hash['def'] = m.find('text//file').first['media_id'].to_s
            hash['loc'] = xmlm.find('location').to_a.first.content.to_s unless xmlm.find('location').to_a.first.nil?
          }
        end
        list << hash
      }
    }
    return list.sort_by{|x| [x['title'], x['number'].to_i]}
  end

  def get_entry_files(entryid, mediadict)
    list = []
    xml = get(entryid)
      folder = ''
      files = []
      aquery = ''
    if xml != ''
      doc = load_xml_string(xml.to_s)
      folder = doc.root.find('//lemma/media_folder_id').first.content.to_s unless doc.root.find('//lemma/media_folder_id').first.nil?
      doc.root.find('//file').each{|f|
        files << "@id='"+f['media_id']+"'" if f['media_id'].to_s != ''
      }
      doc.root.find('//media/file').each{|f|
        files << "@id='"+f['id']+"'" if f['id'].to_s != ''
      }
      doc.root.find('//variant').each{|f|
        files << "@id='"+f.content.to_s+"'" if f.content.to_s != ''
      }
      unless doc.root.find('lemma/video_front').first.nil?
        aquery += " or location='"+doc.root.find('lemma/video_front').first.content.to_s+"'"
      end
      unless doc.root.find('lemma/video_side').first.nil?
        aquery += " or location='"+doc.root.find('lemma/video_side').first.content.to_s+"'"
      end
    end
      query = "entry_folder='#{entryid}'"
      query += " or media_folder_id='#{folder}'" unless folder == ''
      if files.size > 0
        query += ' or ('
        query += files.join(' or ')
        query += ')'
      end
      query += aquery
      query = "/media[#{query}]"
      $stderr.puts query
      return get_file_list(query, mediadict)
  end
  
  def get_perm(user, dict)
    xml = dict.get('user'+user)
    doc = load_xml_string(xml.to_s)
    perms = []
    doc.find('//editor|//revizor').each{|e|
      perms << e.name + '_' + e['perm']
    }
    if not doc.find('/user/admin').first.nil? and doc.find('/user/admin').first.content.to_s == 'true'
      perms << 'admin'
    end
    if perms.size > 0
      return perms
    else
      return 'ro'
    end
  end
  def get_user_info(user, dict)
    xml = dict.get('user'+user)
    doc = load_xml_string(xml.to_s)
    perms = []
    doc.find('//skupina').each{|e|
      perms << e.content.to_s
    }
    copy = ''
    copy = doc.find('/user/copy').first.content.to_s unless doc.find('/user/copy').first.nil?
    zdroj = ''
    zdroj = doc.find('/user/zdroj').first.content.to_s unless doc.find('/user/zdroj').first.nil?
    autor = ''
    autor = doc.find('/user/autor').first.content.to_s unless doc.find('/user/autor').first.nil?
    return perms.join(','), copy, zdroj, autor
  end

  def get_set_rel(user, dict)
    xml = dict.get('user'+user)
    doc = load_xml_string(xml.to_s)
    set_rel = 'all'
    set_rel = doc.find('/user/set_rel').first.content.to_s unless doc.find('/user/set_rel').first.nil?
    return set_rel.to_s
  end

  def remove_file(media_id, entry_id)
    xml = get(media_id)
    doc = load_xml_string(xml.to_s)
    doc.find('//entry_folder[.="'+entry_id+'"]').each{|e|
      e.remove!
    }
    doc.find('//media_folder_id').each{|e|
      e.remove!
    }
    update(media_id, doc.to_s)
  end
  def attach_file(location, entry_id, metadata)
    query = '[media[location="'+location+'"]]'
    result = xquery_to_hash(query)
    if result.size > 0
      result.each{|id,xml|
        doc = load_xml_string(xml.to_s)
        doc.root << XML::Node.new('entry_folder', entry_id)
        $stderr.puts doc.to_s
        update(id, doc.to_s)
      }
    else
      mediaid = @admindict.get_next_id(@dictcode).to_s
      xml = "<media id='#{mediaid}'>
      <location>#{location}</location>
      <entry_folder>#{entry_id}</entry_folder>
    <id_meta_copyright>#{metadata['@id_meta_copyright']}</id_meta_copyright>
    <id_meta_author>#{metadata['@id_meta_author']}</id_meta_author>
    <id_meta_source>#{metadata['@id_meta_source']}</id_meta_source>
    <admin_comment>#{metadata['@admin_comment']}</admin_comment>
    <type>#{metadata['@type']}</type>
    <status>#{metadata['@status']}</status>
    <orient>#{metadata['@orient']}</orient>
    <created_at>#{Time.now.strftime("%Y-%m-%d %H:%M:%S")}</created_at>
      </media>"
      $stderr.puts xml
        update(mediaid, xml.to_s)
    end
  end

  def update_media_info(media_info)
    if media_info['@id'].to_s != ''
       xml = get(media_info['@id'].to_s)
      if xml == ''
        doc = load_xml_string('<media id="'+media_info['@id'].to_s+'"></media>')
      else
        doc = load_xml_string(xml.to_s)
      end
      if doc.root.find('id').first.nil?
        doc.root << XML::Node.new('id', media_info['@id'].to_s)
      end
      if doc.root.find('location').first.nil?
        doc.root << XML::Node.new('location', media_info['@location'].to_s)
      else
        doc.root.find('location').first.content = media_info['@location'].to_s
      end
      if doc.root.find('id_meta_author').first.nil?
        doc.root << XML::Node.new('id_meta_author', media_info['@id_meta_author'].to_s)
      else
        doc.root.find('id_meta_author').first.content = media_info['@id_meta_author'].to_s
      end
      if doc.root.find('id_meta_copyright').first.nil?
        doc.root << XML::Node.new('id_meta_copyright', media_info['@id_meta_copyright'].to_s)
      else
        doc.root.find('id_meta_copyright').first.content = media_info['@id_meta_copyright'].to_s
      end
      if doc.root.find('id_meta_source').first.nil?
        doc.root << XML::Node.new('id_meta_source', media_info['@id_meta_source'].to_s)
      else
        doc.root.find('id_meta_source').first.content = media_info['@id_meta_source'].to_s
      end
      if doc.root.find('admin_comment').first.nil?
        doc.root << XML::Node.new('admin_comment', media_info['@admin_comment'].to_s)
      else
        doc.root.find('admin_comment').first.content = media_info['@admin_comment'].to_s
      end
      if doc.root.find('status').first.nil?
        doc.root << XML::Node.new('status', media_info['@status'].to_s)
      else
        doc.root.find('status').first.content = media_info['@status'].to_s
      end
      if doc.root.find('orient').first.nil?
        doc.root << XML::Node.new('orient', media_info['@orient'].to_s)
      else
        doc.root.find('orient').first.content = media_info['@orient'].to_s
      end
      if doc.root.find('type').first.nil?
        doc.root << XML::Node.new('type', media_info['@type'].to_s)
      else
        doc.root.find('type').first.content = media_info['@type'].to_s
      end
      $stderr.puts doc.to_s
      update(media_info['@id'].to_s, doc.to_s)
    end
  end

  def get_relations(meaning_id, type)
    relations = []
    if type == 'synonym'
      type_query = '@type="synonym"'
    elsif type == 'translation'
      type_query = '@type="translation"'
    else
      type_query = '@type="translation" or @type="synonym"'
    end
    query = 'for $r in collection("'+@collection+'")/entry/meanings/meaning[@id="'+meaning_id+'"]/relation['+type_query+'] return <r>{data($r/@type)};{data($r/@target)};{data($r/@meaning_id)};{$r/file/label/text()}{$r/title/text()}</r>'
    @sedna.query(query).each{|rel|
      rar = rel.to_s.sub('<r>','').sub('</r>','').split(';')
      relations << {'type'=>rar[0], 'target'=>rar[1], 'meaning_id'=>rar[2], 'title'=>rar[3]}
    }

    return relations
  end

  def get_relation_info(meaning_id)
    info = ''
    query = 'collection("'+@collection+'")/entry[meanings/meaning/@id="'+meaning_id+'"]/lemma/title/text()'
    @sedna.query(query).each{|title|
      info = 'T:'+title.to_s
    }
    query = 'collection("'+@collection+'")/entry[meanings/meaning/@id="'+meaning_id+'"]/lemma/video_front/text()'
    @sedna.query(query).each{|title|
      info = 'V:'+title.to_s
    }

    return info
  end

  def remove_all_relations(source, entry_id)
    @admindict.array.each{|code,dict|
      query = 'update delete collection("' + code + '")/entry/meanings/meaning/relation[@target="' + source + '" and starts-with(@meaning_id,"' + entry_id + '-")]'
      $stderr.puts query
      @sedna.query(query)
    }
  end

  def remove_relation(meaning_id, rel_meaning_id, rel_type, target='')
    $stderr.puts meaning_id, rel_meaning_id, rel_type, target
    target = @dictcode if target == ''
    xquery_to_hash('[entry[meanings/meaning/@id="'+rel_meaning_id.gsub('"','&quot;')+'" or meanings/meaning/usages/usage/@id="'+rel_meaning_id.gsub('"','&quot;')+'"]]').each{|id,xml|
      doc = load_xml_string(xml)
      doc.find('//meaning[@id="'+rel_meaning_id.gsub('"','&quot;')+'"]/relation[@meaning_id="'+meaning_id+'" and @type="'+rel_type+'" and @target="'+target+'"]').each{|r|
        $stderr.puts r
        r.remove!
      }
      doc.find('//usage[@id="'+rel_meaning_id.gsub('"','&quot;')+'"]/relation[@meaning_id="'+meaning_id+'" and @type="'+rel_type+'" and @target="'+target+'"]').each{|r|
        if r.parent.find('relation').size == 1
          r.parent['type'] = 'sentence'
        end
        r.remove!
      }
      update(id.to_s, doc.to_s)
    }
  end
  
  def remove_colloc(target, source)
    xml = get(source)
    if xml != ''
      doc = load_xml_string(xml)
      doc.find('/entry/collocations/colloc[@lemma_id="'+target+'"]').each{|col|
        col.remove!
      }
      update(source, doc.to_s)
    end
  end

  def add_relation(meaning_id, rel_meaning_id, rel_type, rel_status, target='')
    target = @dictcode if target == ''
    rel_type = 'synonym' if rel_type == 'synonym_strategie'
    xquery_to_hash('[entry[meanings/meaning/@id="'+rel_meaning_id.gsub('"','&quot;')+'"]]').each{|id,xml|
      doc = load_xml_string(xml)
      if rel_type == 'synonym'
        rquery = '//meaning[@id="'+rel_meaning_id.gsub('"','&quot;')+'"]/relation[@meaning_id="'+meaning_id+'" and (@type="synonym" or @type="synonym_strategie") and @target="'+target+'"]'
      else
        rquery = '//meaning[@id="'+rel_meaning_id.gsub('"','&quot;')+'"]/relation[@meaning_id="'+meaning_id+'" and @type="'+rel_type+'" and @target="'+target+'"]'
      end
      if doc.find(rquery).size == 0
        newrel = XML::Node.new('relation')
        newrel['type'] = rel_type
        newrel['status'] = rel_status
        newrel['meaning_id'] = meaning_id
        newrel['target'] = target
        doc.find('//meaning[@id="'+rel_meaning_id+'"]').first << newrel
      else
        if rel_type == 'synonym'
          rquery2 = '//meaning[@id="'+rel_meaning_id.gsub('"','&quot;')+'"]/relation[@meaning_id="'+meaning_id+'" and (@type="synonym" or @type="synonym_strategie")]'
        else
          rquery2 = '//meaning[@id="'+rel_meaning_id.gsub('"','&quot;')+'"]/relation[@meaning_id="'+meaning_id+'" and @type="'+rel_type+'"]'
        end
        doc.find(rquery2).each{|rel|
          rel['status'] = rel_status
        }
      end
      update(id.to_s, doc.to_s)
    }
    xquery_to_hash('[entry[meanings/meaning/usages/usage/@id="'+rel_meaning_id.gsub('"','&quot;')+'"]]').each{|id,xml|
      doc = load_xml_string(xml)
      if rel_type == 'synonym'
        rquery = '//usage[@id="'+rel_meaning_id+'"]/relation[@meaning_id="'+meaning_id+'" and (@type="synonym" or @type="synonym_strategie") and @target="'+target+'"]'
      else
        rquery = '//usage[@id="'+rel_meaning_id+'"]/relation[@meaning_id="'+meaning_id+'" and @type="'+rel_type+'" and @target="'+target+'"]'
      end
      if doc.find(rquery).size == 0
        newrel = XML::Node.new('relation')
        newrel['type'] = rel_type
        newrel['status'] = rel_status
        newrel['meaning_id'] = meaning_id
        newrel['target'] = target
        doc.find('//usage[@id="'+rel_meaning_id+'"]').first << newrel
      else
        if rel_type == 'synonym'
          rquery2 = '//usage[@id="'+rel_meaning_id+'"]/relation[@meaning_id="'+meaning_id+'" and (@type="synonym" or @type="synonym_strategie")]'
        else
          rquery2 = '//usage[@id="'+rel_meaning_id+'"]/relation[@meaning_id="'+meaning_id+'" and @type="'+rel_type+'"]'
        end
        doc.find(rquery2).each{|rel|
          rel['status'] = rel_status
        }
      end
      update(id.to_s, doc.to_s)
    }
  end

  def add_variant_cs(entry_id, target_id, type)
    xml = get(target_id)
    if xml.to_s != ''
      doc = load_xml_string(xml.to_s)
      doc.find('//entry/lemma').to_a.first << XML::Node.new(type) if doc.find('//entry/lemma/'+type).to_a.first.nil?
      if doc.find('//entry/lemma/'+type+'/variant[.="'+entry_id+'"]').length == 0
        doc.find('//entry/lemma/'+type).to_a.first << XML::Node.new('variant', entry_id)
      end
      update(target_id, doc.to_s)
    end
  end


  def remove_variant_cs(entry_id, target_id, type)
    xml = get(target_id)
    if xml.to_s != ''
      doc = load_xml_string(xml.to_s)
      doc.find('//entry/lemma/'+type+'/variant[.="'+entry_id+'"]').each{|v|
        v.remove!
      }
      update(target_id, doc.to_s)
    end
  end

  def add_variant(dictmedia, target_video, source_vidid, vartype)
    to_cache = []
    target_vidid = ''
    dictmedia.xquery_to_hash('[media[location="'+target_video+'"]]').each{|id,xml|
      target_vidid = id
    }
    $stderr.puts 'add variant, target='+target_vidid
    xmlmedia = dictmedia.get(source_vidid)
    if xmlmedia != '' and target_vidid != ''
      docmedia = load_xml_string(xmlmedia)
      source_loc = docmedia.find('//location').first.content.to_s
      xquery_to_hash('[entry[lemma/video_front="'+source_loc+'"]]').each{|id,xml|
        doc = load_xml_string(xml)
        doc.find('/entry/lemma').first << XML::Node.new(vartype) if doc.find('//'+vartype).size == 0
        if doc.find('//'+vartype+'/variant[.="'+target_vidid+'"]').size == 0
          doc.find('//'+vartype).first << XML::Node.new('variant', target_vidid)
        end
        to_cache << id.to_s
        update(id.to_s, doc.to_s)
      }
    end
    return to_cache
  end

  def get_comments(dictcode, user, entry, box, exact=true)
    result = []
    if box == ''
      query = '/comment[dict="'+dictcode+'" and entry="'+entry+'"]'
    else
      if exact
        query = '/comment[dict="'+dictcode+'" and entry="'+entry+'" and box="'+box+'"]'
      else
        query = '/comment[dict="'+dictcode+'" and entry="'+entry+'" and contains(box,"'+box+'")]'
      end
    end
    xquery_to_array(query).each{|xml|
      doc = load_xml_string(xml.to_s)
      hash = {'entry'=>entry, 'box'=>box, 'dict'=>dictcode, 'user'=>'', 'text'=>'', 'time'=>''}
      hash['time'] = doc.find('//time').first.content.to_s unless doc.find('//time').first.nil?
      hash['user'] = doc.find('//user').first.content.to_s unless doc.find('//user').first.nil?
      hash['text'] = doc.find('//text').first.content.to_s unless doc.find('//text').first.nil?
      hash['box'] = doc.find('//box').first.content.to_s unless doc.find('//box').first.nil?
      hash['id'] = doc.find('//id').first.content.to_s unless doc.find('//id').first.nil?
      result << hash
    }
    result.sort!{|x,y| y['time']<=>x['time']}
    return result
  end

  def add_comment(dictcode, user, entry, box, text)
    cid = @admindict.get_next_id('czj_koment').to_s
    xml = '<comment><id>'+cid+'</id>'
    xml += '<user>'+user+'</user>'
    xml += '<time>'+Time.new.strftime('%Y-%m-%d %H:%M')+'</time>'
    xml += '<text>'+text+'</text>'
    xml += '<entry>'+entry+'</entry>'
    xml += '<box>'+box+'</box>'
    xml += '<dict>'+dictcode+'</dict>'
    xml += '</comment>'
    update(cid, xml)
  end

  def del_comment(id)
    delete(id)
  end

  def report_video(dictmedia, dictkom, page=0, type_a=false, type_b=false, type_d=false, type_k=false, def_skup='', author='', source='', status='')
    report = []
    type_cond = []
    type_cond << 'type="sign_front"' if type_a
    type_cond << 'type="sign_side"' if type_b
    type_cond << 'type="sign_definition"' if type_d
    type_cond << 'type="sign_usage_example"' if type_k
    #cond_a = ['status!="published"']
    cond_a = []
    cond_a << 'status="published"' if status == 'published'
    cond_a << 'status!="published"' if status == 'hidden'
    cond_a << '@id' if status == 'all'

    cond_a << '(' + type_cond.join(' or ') + ')' if type_cond.size > 0
    cond_a << 'id_meta_author="'+author+'"'if author != ''
    cond_a << 'id_meta_source="'+source+'"'if source != ''
    if def_skup != ''
      cond_a << '(@id=$vd or location=$vf or location=$vs)'
      cond = cond_a.join(' and ')
      query = 'subsequence(let $vd:=collection("czj")/entry[lemma/pracskupina="'+def_skup+'"]/meanings/meaning/text//file/@media_id
      let $vf:=collection("czj")/entry[lemma/pracskupina="'+def_skup+'"]/lemma/video_front  
      let $vs:=collection("czj")/entry[lemma/pracskupina="'+def_skup+'"]/lemma/video_side
      for $x in collection("czj_media")/media['+cond+'] order by $x/label, $x/location return <a>{data($x/@id)};{$x/location/text()};{$x/label/text()};{$x/original_file_name/text()}</a>, '+(1+page*10).to_s+', 10)'
      countquery = 'count(let $vd:=collection("czj")/entry[lemma/pracskupina="'+def_skup+'"]/meanings/meaning/text//file/@media_id
      let $vf:=collection("czj")/entry[lemma/pracskupina="'+def_skup+'"]/lemma/video_front  
      let $vs:=collection("czj")/entry[lemma/pracskupina="'+def_skup+'"]/lemma/video_side
      for $x in collection("czj_media")/media['+cond+'] order by $x/label, $x/location return <a>{data($x/@id)};{$x/location/text()};{$x/label/text()};{$x/original_file_name/text()}</a>)'
    else
      cond = cond_a.join(' and ')
      query = 'subsequence(for $x in collection("czj_media")/media['+cond+'] order by $x/label, $x/location return <a>{data($x/@id)};{$x/location/text()};{$x/label/text()};{$x/original_file_name/text()}</a>, '+(1+page*10).to_s+', 10)'
      countquery = 'count(for $x in collection("czj_media")/media['+cond+'] order by $x/label, $x/location return <a>{data($x/@id)};{$x/location/text()};{$x/label/text()};{$x/original_file_name/text()}</a>)'
    end
    count = @sedna.query(countquery).first.to_s
    $stderr.puts query
    @sedna.query(query).each{|res|
      $stderr.puts res
      id, loc, label, filename = res.to_s.sub('<a>','').sub('</a>','').split(';')
      vh = {'id'=>id, 'location'=>loc, 'label'=>label, 'filename'=>filename}
      vh['is_flv'] = '1' if loc[-3,3] == 'flv'
      vh['is_mp4'] = '1' if loc[-3,3] == 'mp4'
      entries = []
      xquery_to_hash('[entry[lemma/video_front="'+loc+'" or lemma/video_side="'+loc+'" or //file/@media_id="'+id+'"]]').each{|eid,xml|
          eh = {'eid'=>eid,'mid'=>id, 'location'=>loc}
          car = dictkom.get_comments('czj', '', eid, 'video'+loc)
          if car.size > 0
            eh['comment'] = car[0]['text'] + ', ' + car[0]['user']
          end
          entries << eh
      }
      vh['entries'] = entries if entries.size > 0
      report << vh
    }
    return report, count
  end

  def get_video_author(selected=nil)
    res = []
    query = 'distinct-values(collection("czj_media")/media[status!="published"]/id_meta_author)'
    @sedna.query(query).each{|r|
      h = {'author'=>r, 'authore'=>CGI.escape(r)}
      h['selected'] = 1 if selected != nil and selected == r
      res << h
    }
    return res
  end

  def get_video_source(selected=nil)
    res = []
    query = 'distinct-values(collection("czj_media")/media[status!="published"]/id_meta_source)'
    @sedna.query(query).each{|r|
      h = {'source'=>r, 'sourcee'=>CGI.escape(r)}
      h['selected'] = 1 if selected != nil and selected == r
      res << h
    }
    return res
  end

  def publish_video(id)
    xml = get(id)
    doc = load_xml_string(xml.to_s)
    doc.find('//status').each{|st|
      st.remove!
    }
    doc.root << XML::Node.new('status', 'published')
    $stderr.puts doc
    @admindict.array['czj'].xquery_to_hash('[entry[media/file/@id="'+id+'"]]').each{|id2,xml2|
      doc2 = load_xml_string(xml2)
      doc2.find('/entry/media/file[@id="'+id+'"]').each{|m|
        m.find('status').each{|st| st.remove!}
        m << XML::Node.new('status', 'published')
      }
      @admindict.array['czj'].update(id2, doc2.to_s)
    }
    update(id, doc.to_s)
  end

  def publish_relation(entry, meaning, relation, type, publish_more=true)
    return [] if entry.to_s == ''
    xml = get(entry.to_s)
    rels = []
    doc = load_xml_string(xml.to_s)
    doc.find('//meaning[@id="'+meaning+'"]/relation[@type="'+type+'" and @meaning_id="'+relation+'"]').each{|r|
      r['status'] = 'published'
      target_dict = @dictcode
      if type == 'translation'
        target_dict = r['target'].to_s
        target_dict = (@dictcode == 'czj')?'cs':'czj'  if target_dict == ''
      end
      rels << {'target'=>target_dict, 'entry_id'=>r['lemma_id']} if r['lemma_id'] != '' and publish_more
    }
    doc.find('//usage[@id="'+meaning+'"]/relation[@type="'+type+'" and @meaning_id="'+relation+'"]').each{|r|
      r['status'] = 'published'
      r.parent.find('status').first.remove!
      r.parent << XML::Node.new('status')
      r.parent.find('status').first.content = 'published'
      target_dict = @dictcode
      if type == 'translation'
        target_dict = r['target'].to_s
        target_dict = (@dictcode == 'czj')?'cs':'czj'  if target_dict == ''
      end
      rels << {'target'=>target_dict, 'entry_id'=>r['lemma_id']} if r['lemma_id'] != '' and publish_more
    }
    #$stderr.puts doc
    update(entry, doc.to_s)
    return rels
  end

  def publish_usage(entry, meaning, usage, mediadict)
    xml = get(entry)
    doc = load_xml_string(xml.to_s)
    doc.find('//meaning[@id="'+meaning+'"]/usages/usage[@id="'+usage+'"]').each{|r|
      r << XML::Node.new('status') if r.find('status').size == 0
      r.find('status').first.content = 'published'
      r.find('text/file').each{|mf|
        $stderr.puts 'MF='+mf.to_s
        mediadict.xquery_to_hash('[media[@id="'+mf['media_id']+'"]]').each{|mid,mx|
          mdoc = load_xml_string(mx.to_s)
          mdoc.root << XML::Node.new('status') if mdoc.find('//status').size == 0
          mdoc.find('//status').first.content = 'published'
          mediadict.update(mid, mdoc.to_s)
          doc.find('//media/file[@id="'+mf['media_id']+'"]').each{|file|
            file << XML::Node.new('status') if file.find('status').size == 0
            file.find('status').first.content = 'published'
          }
        }
      }
    }
    $stderr.puts doc
    update(entry, doc.to_s)
  end

  def publish_meaning(entry, meaning, mediadict)
    xml = get(entry)
    doc = load_xml_string(xml.to_s)
    doc.find('//meaning[@id="'+meaning+'"]').each{|r|
      r << XML::Node.new('status') if r.find('status').size == 0
      r.find('status').first.content = 'published'
      r.find('text/file').each{|mf|
        mediadict.xquery_to_hash('[media[@id="'+mf['media_id']+'"]]').each{|mid,mx|
          mdoc = load_xml_string(mx.to_s)
          mdoc.root << XML::Node.new('status') if mdoc.find('//status').size == 0
          mdoc.find('//status').first.content = 'published'
          mediadict.update(mid, mdoc.to_s)
          doc.find('//media/file[@id="'+mf['media_id']+'"]').each{|file|
            file << XML::Node.new('status') if file.find('status').size == 0
            file.find('status').first.content = 'published'
          }
        }
      }
    }
    $stderr.puts doc
    update(entry, doc.to_s)
  end

  def publish(entry, type, mediadict)
    xml = get(entry)
    doc = load_xml_string(xml.to_s)
    if type == 'entry'
      doc.find('/entry/lemma').first << XML::Node.new('status')  if doc.find('/entry/lemma/status').size == 0
      doc.find('/entry/lemma/status').first.content = 'published'
      doc.find('/entry').first << XML::Node.new('collocations') if doc.find('/entry/collocations').size == 0
      doc.find('/entry/collocations').first['status'] = 'published'
    end
    if type == 'sw'
      doc.find('/entry/lemma').first['swstatus'] = 'published'
    end
    if type == 'hns'
      doc.find('/entry/lemma/hamnosys').first['status'] = 'published' unless doc.find('/entry/lemma/hamnosys').first.nil?
    end
    if type == 'gram'
      doc.find('/entry/lemma/grammar_note').first['status'] = 'published' unless doc.find('/entry/lemma/grammar_note').first.nil?
    end
    if type == 'style'
      doc.find('/entry/lemma/style_note').first['status'] = 'published' unless doc.find('/entry/lemma/style_note').first.nil?
    end
    if type == 'video_front' or type == 'video_side'
      unless doc.find('/entry/lemma/'+type).first.nil?
        loc = doc.find('/entry/lemma/'+type).first.content.to_s
        mediadict.xquery_to_hash('[media[location="'+loc+'"]]').each{|mid,mx|
          mdoc = load_xml_string(mx.to_s)
          mdoc.root << XML::Node.new('status') if mdoc.find('//status').size == 0
          mdoc.find('//status').first.content = 'published'
          mediadict.update(mid, mdoc.to_s)
        }
      end
    end
    $stderr.puts doc
    update(entry, doc.to_s)
    #$stderr.puts @dictcode
    #newdoc = get_follow(@dictcode, entry, false, nil, 'admin')
    #update(entry, newdoc.to_s)
  end

  def publish_all_cs(entry_id)
    xml = get(entry_id)
    doc = load_xml_string(xml.to_s)
    doc.find('/entry/lemma').first << XML::Node.new('status')  if doc.find('/entry/lemma/status').size == 0
    doc.find('/entry/lemma/status').first.content = 'published'
    doc.find('/entry/lemma/grammar_note').first['status'] = 'published' unless doc.find('/entry/lemma/grammar_note').first.nil?
    doc.find('/entry/lemma/style_note').first['status'] = 'published' unless doc.find('/entry/lemma/style_note').first.nil?
    doc.find('//meaning/relation').each{|r|
      r['status'] = 'published'
    }
    doc.find('//meaning').each{|r|
      r << XML::Node.new('status') if r.find('status').size == 0
      r.find('status').first.content = 'published'
    }
    doc.find('//meaning/usages/usage').each{|r|
      r << XML::Node.new('status') if r.find('status').size == 0
      r.find('status').first.content = 'published'
    }
    update(entry_id, doc.to_s)
  end

  def preprocess_cs(xml)
    xml.gsub!(/<html>.*<\/html>/m, '')
    xml.gsub!(/([ (\n])\[([1-9][-,0-9]*)\]/, '\1<link>\2</link>')
    xml.gsub!(/<text>\[([1-9][-,0-9]*)\]/, '<text><link>\1</link>')
    xml.gsub!('&lt;i&gt;', '<i>')
    xml.gsub!('&lt;/i&gt;', '</i>')
    xml.gsub!('&lt;b&gt;', '<b>')
    xml.gsub!('&lt;/b&gt;', '</b>')
    xml.gsub!('&lt;sub&gt;', '<sub>')
    xml.gsub!('&lt;/sub&gt;', '</sub>')
    xml.gsub!('&lt;sup&gt;', '<sup>')
    xml.gsub!('&lt;/sup&gt;', '</sup>')
    xml.gsub!('&amp;lt;', '&lt;')
    xml.gsub!('&amp;gt;', '&gt;')
    $stderr.puts xml
    doc = load_xml_string(xml)
    doc.find('//meanings/meaning').each{|mean|
      rels = mean.find('relation').to_a
      mean.find('relation').each{|r| r.remove!}
      rels.sort{|x,y| x['meaning_id']<=>y['meaning_id'] }.each{|rel|
        mean << rel
      }
    }
    doc.find('//link').each{|l|
      $stderr.puts l
      if l.content.include?(',')
        # vice vyznamu
        lid,lm = l.content.to_s.split('-')
        lms = lm.split(',')
        title = ''
        doc2 = load_xml_string(get(lid))
        doc2.find('//lemma/title').each{|t|
          title = t.content.to_s
        }
        l['lemma_id'] = lid
        l.content = title
        l['auto_complete'] = doc2.find('//lemma').first['auto_complete'].to_s
        lms.each{|lmean|
          nm = XML::Node.new('link_mean')
          d2m = doc2.find('//meaning[@id="'+lid+'-'+lmean+'"]').first
          nm['number'] = d2m['number'].to_s
          nm['meaning_id'] = lid+'-'+lmean
          nm['status'] = 'hidden'
          nm['status'] = d2m.find('status').first.content.to_s unless d2m.find('status').first.nil?
          l << nm
        }
      elsif l.content.include?('-')
        # je zadane id vyznamu?
        query = '[entry[meanings/meaning/@id="'+l.content.to_s+'"]]'
        xquery_to_hash(query).each{|id, xml2|
          $stderr.puts id
          title = ''
          doc2 = load_xml_string(xml2)
          doc2.find('//lemma/title').each{|t|
            title = t.content.to_s
          }
          l['lemma_id'] = id
          l['auto_complete'] = doc2.find('//lemma').first['auto_complete'].to_s
          lid = l.content.to_s
          l.content = title
          if doc2.find('//meaning').to_a.size > 1
            nm = XML::Node.new('link_mean')
            d2m = doc2.find('//meaning[@id="'+lid+'"]').first
            nm['number'] = d2m['number'].to_s
            nm['status'] = 'hidden'
            nm['status'] = d2m.find('status').first.content.to_s unless d2m.find('status').first.nil?
            nm['meaning_id'] = lid
            l << nm
          end
        }
      else
        #je zadane id hesla?
        xml2 = get(l.content.to_s)
        if xml2 != ''
          title = ''
          doc2 = load_xml_string(xml2)
          doc2.find('//lemma/title').each{|t|
            title = t.content.to_s
          }
          l['lemma_id'] = l.content.to_s
          l['auto_complete'] = doc2.find('//lemma').first['auto_complete'].to_s
          l.content = title
        end
      end
    }
    $stderr.puts doc.to_s
    return doc.to_s
  end

  def update_all
    log = File.new('/tmp/update.log','w')
    log.puts 'run one ' + Time.new.to_s 
    @sedna.query('data(collection("czj")/entry[(lemma/lemma_type="single" or not(lemma/lemma_type))]/@id)').each{|id|
      log.puts id
      update(id, get_follow('czj', id, false, nil, 'admin'))
    }
    log.puts 'run two ' + Time.new.to_s
    @sedna.query('data(collection("czj")/entry[(lemma/lemma_type!="single")]/@id)').each{|id|
      log.puts id
      update(id, get_follow('czj', id, false, nil, 'admin'))
    }
    log.puts 'run three ' + Time.new.to_s
    @sedna.query('data(collection("czj")/entry[(lemma/lemma_type="single" or not(lemma/lemma_type))]/@id)').each{|id|
      log.puts id
      update(id, get_follow('czj', id, false, nil, 'admin'))
    }
    log.close
  end

  def update_allcs
    log = File.new('/tmp/updatecs.log','w')
    @sedna.query('data(collection("cs")/entry/@id)').each{|id|
      log.puts id
      update(id, get_follow('cs', id, false, nil, 'admin'))
    }
    log.close
  end

  def get_report(dict, dictcode, page=0, query={}, pocet=10, perm='', skupiny='', sort='id', user='')
    report = []
    search_cond = []
    if perm == 'ro'
      scond = ''
      scond = skupiny.split(',').map{|sk| '($x/lemma/pracskupina="'+sk+'")'}.join(' or ')
      #cond = '$x/lemma/@auto_complete="1"'
      #cond = '$x/lemma/completeness!="1"'
      cond = '((not($x/lemma/completeness) or $x/lemma/completeness!="1") and (count($x/meanings/meaning/relation)>0) and ((some $m in $x/meanings/meaning/relation satisfies (($m/../is_translation_unknown!="1" or not($m/../is_translation_unknown)) and $m/@type="translation" and $m/@status="published" )) ))'
      cond += ' or ' + scond unless scond == ''
      search_cond <<  '('+cond+')'
    end
    query.each{|k,v|
      cond = ''
      case k
      when 'bez_sw'
        cond = '(not($x/lemma/sw) and not($x/collocations/swcompos) and not($x/lemma/swmix/sw))'
      when 'nes_sw'
        cond = '(($x/lemma/@swstatus="hidden" or not($x/lemma/@swstatus) or $x/lemma/@swstatus="") and ( not($x/collocations/swcompos) and not($x/collocations/colloc/sw)))'
      when 'bez_hns'
        cond = '(not($x/lemma/hamnosys) or $x/lemma/hamnosys="")'
      when 'nes_hns'
        cond = '($x/lemma/hamnosys/@status="hidden" or not($x/lemma/hamnosys/@status) or $x/lemma/hamnosys/@status="")'
      when 'rucne'
        cond = '($x/lemma/completeness="1")'
      when 'relpub'
        cond = '(every $r in $x//relation[@type="translation"] satisfies ($r/@auto_complete and $r/@auto_complete!="1"))'
      when 'vztahy'
        cond = '(some $r in $x//relation satisfies ($r/@status="hidden" or not($r/@status) or $r/@status=""))'
      when 'pubtrans'
        cond = '(some $r in $x//relation[@type="translation"] satisfies ($r/@status="hidden" or not($r/@status) or $r/@status=""))'
      when 'pubtranscs'
        cond = '((count($x//relation[@type="translation" and @target="cs"])=0) or (some $r in $x//relation[@type="translation" and @target="cs"] satisfies ($r/@status="hidden" or not($r/@status) or $r/@status="")))'
      when 'pubtransczj'
        cond = '((count($x//relation[@type="translation" and @target="czj"])=0) or (some $r in $x//relation[@type="translation" and @target="czj"] satisfies ($r/@status="hidden" or not($r/@status) or $r/@status="")))'
      when 'pubtransen'
        cond = '((count($x//relation[@type="translation" and @target="en"])=0) or (some $r in $x//relation[@type="translation" and @target="en"] satisfies ($r/@status="hidden" or not($r/@status) or $r/@status="")))'
      when 'pubtransasl'
        cond = '((count($x//relation[@type="translation" and @target="asl"])=0) or (some $r in $x//relation[@type="translation" and @target="asl"] satisfies ($r/@status="hidden" or not($r/@status) or $r/@status="")))'
      when 'pubtransis'
        cond = '((count($x//relation[@type="translation" and @target="is"])=0) or (some $r in $x//relation[@type="translation" and @target="is"] satisfies ($r/@status="hidden" or not($r/@status) or $r/@status="")))'
      when 'videa'
        cond = '(not($x/lemma/video_front) or not($x/lemma/video_side) or $x/lemma/video_front="" or $x/lemma/video_side="")'
      when 'videa2'
        cond = '((not($x/lemma/video_front) or $x/lemma/video_front="") and (not($x/lemma/video_side) or $x/lemma/video_side=""))'
      when 'schvcelni'
        cond = '(not($x/media/file[location=$x/lemma/video_front]/status="published"))'
      when 'schvbocni'
        cond = '(not($x/media/file[location=$x/lemma/video_side]/status="published"))'
      when 'noupdate'
        cond = '((not($x/lemma/updated_at) or $x/lemma/updated_at=""))'
      when 'translation'
        cond = '(every $r in $x/meanings/meaning/relation[@type="translation"] satisfies ((fn:matches($r/@meaning_id, "^[-0-9]*$"))))'
      when 'translationczj'
        cond = '((count($x/meanings/meaning//relation[@type="translation" and @target="czj"])>0) and (every $r in $x/meanings/meaning//relation[@type="translation" and @target="czj"] satisfies ((fn:matches($r/@meaning_id, "^[-0-9]*(_us[0-9]*)?$")))))'
      when 'translationcs'
        cond = '((count($x/meanings/meaning//relation[@type="translation" and @target="cs"])>0) and (every $r in $x/meanings/meaning//relation[@type="translation" and @target="cs"] satisfies ((fn:matches($r/@meaning_id, "^[-0-9]*(_us[0-9]*)?$")))))'
      when 'translationasl'
        cond = '((count($x/meanings/meaning//relation[@type="translation" and @target="asl"])>0) and (every $r in $x/meanings/meaning//relation[@type="translation" and @target="asl"] satisfies ((fn:matches($r/@meaning_id, "^[-0-9]*(_us[0-9]*)?$")))))'
      when 'translationis'
        cond = '((count($x/meanings/meaning//relation[@type="translation" and @target="is"])>0) and (every $r in $x/meanings/meaning//relation[@type="translation" and @target="is"] satisfies ((fn:matches($r/@meaning_id, "^[-0-9]*(_us[0-9]*)?$")))))'
      when 'translationen'
        cond = '((count($x/meanings/meaning//relation[@type="translation" and @target="en"])>0) and (every $r in $x/meanings/meaning//relation[@type="translation" and @target="en"] satisfies ((fn:matches($r/@meaning_id, "^[-0-9]*(_us[0-9]*)?$")))))'
      when 'texttranslationen'
        cond = '((count($x/meanings/meaning//relation[@type="translation" and @target="en"])>0) and (some $r in $x/meanings/meaning//relation[@type="translation" and @target="en"] satisfies (not(fn:matches($r/@meaning_id, "^[-0-9]*(_us[0-9]*)?$")))))'
      when 'vyznam'
        cond = '(some $m in $x/meanings/meaning satisfies ((not($m/status) or $m/status="hidden" or $m/status="") or not($m/text//file)))'
      when 'vyznamvid'
        cond = '(some $m in $x/meanings/meaning satisfies (not($m/text//file)))'
      when 'vyznamcs'
        cond = '(some $m in $x/meanings/meaning satisfies ((not($m/status) or $m/status="hidden" or $m/status="") or $m/text=""))'
      when 'usage'
        cond = '((some $m in $x/meanings/meaning satisfies not($m/usages/usage)) or (some $m in $x/meanings/meaning/usages/usage satisfies ((not($m/status) or $m/status="hidden" or $m/status="") or not($m/text//file))))'
      when 'usagevid'
        cond = '((some $m in $x/meanings/meaning satisfies not($m/usages/usage)) or (some $m in $x/meanings/meaning/usages/usage satisfies (not($m/text//file))))'
      when 'usagecs'
        cond = '(some $m in $x/meanings/meaning/usages/usage satisfies ((not($m/status) or $m/status="hidden" or $m/status="") or $m/text=""))'
      when 'artik'
        cond = '(some $r in $x/lemma/sw satisfies $r/@misto="")'
      when 'skup'
        cond = '($x/lemma/pracskupina="'+query['def_skup'].to_s+'")'
      when 'sldruh'
        cond = '($x/lemma/grammar_note/@slovni_druh="'+query['slovni_druh'].to_s+'")'
      when 'typhesla'
        cond = '($x/lemma/lemma_type="'+query['seltyphesla'].to_s+'")'
      when 'coll'
        cond = '$x/lemma/lemma_type!="collocation"'
      when 'koment'
        if query['komentbox'].to_s == ''
          if query['koment_moje'] == 'on'
            cond = '($x/comments/box)'
          else
            cond = '($x/comments[@last!="'+user+'"])'
          end
        else
          if query['komentbox'].to_s == 'video'
            if query['koment_moje'] == 'on'
              cond = '(some $b in $x/comments/box satisfies (starts-with($b/@box, "video")))'
            else
              cond = '(some $b in $x/comments/box[@last!="'+user+'"] satisfies (starts-with($b/@box, "video")))'
            end
          else
            if query['koment_moje'] == 'on'
              cond = '(some $b in $x/comments/box satisfies (contains($b/@box, "'+query['komentbox'].to_s+'")))'
            else
              cond = '(some $b in $x/comments/box[@last!="'+user+'"] satisfies (contains($b/@box, "'+query['komentbox'].to_s+'")))'
            end
          end
        end
      when 'completeness'
        if query['completenessbox'].to_s != ''
          cond = '($x/lemma/completeness="'+query['completenessbox'].to_s+'")'
        else
          cond = '($x/lemma/completeness)'
        end
      when 'autocomp'
        if query['autocompbox'].to_s != ''
          cond = '($x/lemma/@auto_complete="'+query['autocompbox'].to_s+'")'
        end
      end
      if cond != '' and v != ''
        if v == 'ano'
          cond = '(not('+cond+'))'
        end
        if k == 'coll'
          cond = '$x/lemma/lemma_type="collocation"' if v == 'ano'
          cond = 'not($x/lemma/lemma_type="collocation")' if v == 'ne'
        end
        if k == 'relpub' and v == 'ano'
          cond = '((count($x//relation[@type="translation"])>0) and (every $r in $x//relation[@type="translation"] satisfies ($r/@auto_complete and $r/@auto_complete="1")))'
        end
        cond = '($x/meanings/meaning/relation[@type="translation"] and '+cond+')' if k == 'translation'
        search_cond << cond
      end
    }
    if search_cond.size > 0
      case sort
      when 'front'
        sortby = '$x/lemma/video_front[1]'
      when 'preklad'
        sortby = 'string-join($x//title/text(),"")'
      when 'lemma'
        sortby = '$x/lemma/title'
      else
        sortby = 'number($x/@id)'
      end
      #if query['koment'].to_s != ''
      #  #if query['komentbox'].to_s == 'video' 
      #  #  komquery = 'let $med := collection("'+dictcode+'_media")/media
      #  #  let $kom := for $k in collection("czj_koment")/comment where $k/dict="'+dictcode+'" and starts-with($k/box,"video") and $med[location=fn:substring($k/box,6)] return $k'
      #  #else
      #    if query['koment_moje'] == 'on'
      #      komquery = 'let $kom := collection("czj_koment")/comment[dict="'+dictcode+'"]'
      #    else
      #      komquery = 'let $kom := collection("czj_koment")/comment[dict="'+dictcode+'" and user!="'+user+'"]'
      #    end
      #  #end
      #  dbquery = 'subsequence('+komquery+'
      #  for $x in collection("'+dictcode+'")/entry where '+search_cond.join(' and ')+' order by '+sortby+' return <a>{data($x/@id)};{$x/lemma/video_front/text()};{string-join($x//title/text(),", ")};{$x/lemma/title/text()}</a>, '+(1+page*pocet).to_s+', '+pocet.to_s+')'
      #  countquery = 'count('+komquery+'
      #  for $x in collection("'+dictcode+'")/entry where '+search_cond.join(' and ')+' return $x)'
      #else
        dbquery = 'subsequence(for $x in collection("'+dictcode+'")/entry where '+search_cond.join(' and ')+' order by '+sortby+' return <a>{data($x/@id)};{$x/lemma/video_front/text()};{string-join($x//title/text(),", ")};{$x/lemma/title/text()};{for $r in $x/meanings/meaning/relation[@type="translation" and @target!="cs" and @target!="sj" and @target!="en"] return <r>{data($r/@target)}:{data($r/@lemma_id)}:{$r/file/location/text()},</r>}</a>, '+(1+page*pocet).to_s+', '+pocet.to_s+')'
        countquery = 'count(for $x in collection("'+dictcode+'")/entry where '+search_cond.join(' and ')+' return $x)'
      #end

      $stderr.puts dbquery
      count = @sedna.query(countquery).first.to_s
      @sedna.query(dbquery).each{|res|
        $stderr.puts res
        id, loc, title, titlecs, rels = res.to_s.sub('<a>','').sub('</a>','').gsub('<r>','').gsub('</r>','').split(';')
        vh = {'id'=>id, 'location'=>loc, 'title'=>title, 'titlecs'=>titlecs, 'rels'=>[]}
        rels.to_s.split(',').each{|rel|
          rtar, rid, rloc = rel.split(':')
          vh['rels'] << {'target'=>rtar, 'lemma_id'=>rid, 'file'=>rloc}
        }
        vh['is_flv'] = '1' if loc and loc[-3,3] == 'flv'
        vh['is_mp4'] = '1' if loc and loc[-3,3] == 'mp4'
        if query['koment'].to_s == 'ne'
          koms = @admindict.array['czj_koment'].get_comments(dictcode, '', id, query['komentbox'].to_s, false)
          kom_view = []
          koms.each{|kom|
            kom_view << kom['text'] + '(' + kom['time'] + ', ' + kom['user'] + ')'
          }
          vh['koment'] = kom_view.join("<br/>")
        end
        if query['vztahy'].to_s == 'ne' or query['nes_sw'].to_s == 'ne' or query['nes_hns'].to_s == 'ne'
          xml = dict.get(id)
          doc = load_xml_string(xml)
          if query['vztahy'].to_s == 'ne'
            rels = []
            doc.find('//relation[@status="hidden" or not(@status) or @status=""]').each{|r|
              if r['meaning_id'] =~ /^[-0-9]*$/
                target = (r['type'] == 'translation')? r['target']:dictcode
                rels << {'type'=>r['type'], 'rmid'=>r['meaning_id'], 'rlid'=>r['lemma_id'], 'target'=>target, 'tr'=>((target=='czj')?'preview6':'preview'), 'meaning_id'=>r.parent['id'], 'entry_id'=>id}
              else
                rels << {'type'=>r['type'], 'rmid'=>r['meaning_id'], 'meaning_id'=>r.parent['id'], 'entry_id'=>id}
              end
            }
            vh['relations'] = rels
          end
          if query['nes_sw'].to_s == 'ne' 
            sws = []
            doc.find('/entry/lemma/sw').each{|sw|
              sws << {'sw'=>sw.content.to_s}
            }
            vh['sw'] = sws
          end
          if query['nes_hns'].to_s == 'ne'
            vh['hns'] = doc.find('/entry/lemma/hamnosys').first.content.to_s unless doc.find('/entry/lemma/hamnosys').first.nil?
          end
        end
        report << vh
      }
    end
    return report, count
  end

  def strip_dia(string)
    return '' if string.nil?
    string = string.dup.to_s.downcase_utf8.force_encoding('UTF-8')
    string.gsub!('č','c')
    string.gsub!('á','a')
    string.gsub!('ď','d')
    string.gsub!('é','e')
    string.gsub!('ě','e')
    string.gsub!('í','i')
    string.gsub!('ň','n')
    string.gsub!('ó','o')
    string.gsub!('ř','r')
    string.gsub!('š','s')
    string.gsub!('ť','t')
    string.gsub!('ú','u')
    string.gsub!('ů','u')
    string.gsub!('ý','y')
    string.gsub!('ř','r')
    string.gsub!('ž','z')
    return string
  end

  def parse_locale
    doc = XML::Document.file('/var/lib/deb-server/xslt/czj-locale.xslt', :encoding => XML::Encoding::UTF_8)
    locale = {}
    doc.find('//xsl:variable[1]/xsl:if').each{|i|
      lang = i['test'].sub(/.*'([a-z]*)'.*/, '\1')
      locale[lang] = {} if locale[lang].nil?
    }
    doc.find('//xsl:variable[starts-with(@name, "web")]').each{|v|
      v.find('xsl:if').each{|i|
        lang = i['test'].sub(/.*'([a-z]*)'.*/, '\1')
        locale[lang]['loc-' + v['name']] = i.content.to_s
      }
    }
    return locale
  end

  def get_counts
    counts = {}
    @admindict.array.each{|code,hash|
      if hash.xslt_sheets.length > 0
        if hash.xslt_sheets['preview'].include?('write')
          search_query = 'for $x in collection("'+code+'")/entry[((lemma/@auto_complete="1" or lemma/completeness="2" or lemma/completeness="100") and ((lemma/lemma_type="single" or lemma/lemma_type="predpona" or not(lemma/lemma_type)) ))] return data($x/@id)' 
        else
          search_query = 'for $y in collection("'+code+'")/entry where $y/lemma/lemma_type != "collocation" and ($y/lemma/completeness!="1" and $y/lemma/@auto_complete="1") return data($y/@id)'
        end
        countquery = 'count('+search_query+')'
        count = @sedna.query(countquery).first.to_i
        counts['entrycount_'+code] = count
      end
    }
    return counts
  end
end
