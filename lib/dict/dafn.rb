require 'dict/dict-sedna'

class Dafn < DictSedna
  def initialize(collection, journal_path, key_path='', database = 'deb', admindict = nil)
    super
    @servlet = DafnServlet
  end

  def get_list(search)
    list = []
    query = '[entry[starts-with(name, "'+search+'")]]'
    xquery_to_hash(query).each {|id,xml|
      doc = load_xml_string(xml.to_s)
      root = doc.root
      name = root.find('name').to_a.first.content.to_s
      count = root.find('count').to_a.first.content.to_s unless root.find('count').to_a.first.nil?
      fr_2007 = root.find('freq_2007').to_a.first.content.to_s unless root.find('freq_2007').to_a.first.nil?
      author = root.find('metadata/author').to_a.first.content.to_s unless root.find('metadata/author').to_a.first.nil?
      last = ''
      unless root.find('metadata/history/changes').to_a.last.nil?
        lc = root.find('metadata/history/changes').to_a.last
        last = lc['user'] + '/' + lc['time'][0..10]
      end
      hash = {'id'=>id, 'name'=>name, 'author'=>author, 'count'=>count, 'last'=>last, 'status'=>root['status'], 'fr_2007'=>fr_2007}
      list << hash
    }
    $stderr.puts list
    return list
  end

  def get_for_edit(id)
    xml = get(id)
    doc = load_xml_string(xml.to_s)
    doc.find('//sense').each{|el|
      unless el.find('annotation').to_a.first.nil?
        el['annotation'] = el.find('annotation').to_a.first.content
        el.find('annotation').to_a.first.remove!
      end
      elt = el.to_s
      elt.gsub!(/<sense[^>]*>/, '')
      elt.gsub!('</sense>', '')
      el.content = elt.gsub("\n", " ")
    }
    doc.find('//forenames|//history').each{|el|
      elt = el.to_s
      elt.gsub!('<forenames>', '')
      elt.gsub!('</forenames>', '')
      elt.gsub!(/<history[^>]*>/, '')
      elt.gsub!('</history>', '')
      el.content = elt.gsub("\n", " ")
    }
    xml = doc.to_s
    xml.gsub!('<?xml version="1.0" encoding="UTF-8"?>','')
    $stderr.puts xml
    return xml
  end
  
  def get_perm(user)
    xml = @admindict.get('user'+user)
    doc = REXML::Document.new(xml.to_s)
    perm_el = doc.root.elements["//service[@code='dafn']/dict[@code='dafn']"]
    if perm_el != nil
      perm = perm_el.attributes['perm'].to_s
      return perm
    else
      return nil
    end
  end

end
