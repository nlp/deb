require 'slovnik/slovnik'
require 'slovnik/admin'

class JournalDict < AdminDict
  attr_reader :journal_hash

  def initialize( db_path, database, key_path='', env=nil )
    super
    @last_entry = {}
    @last_update = {}
    @journal_hash = {}
  end

  def list_dictfiles(service = nil)
    return list_dicts(service, true)
  end

  def update_hash
    start = Time.now.to_s
    list_dictfiles.each{|code,dbfile|
      @last_update[code] = Time.at(0) if @last_update[code] == nil
      @last_entry[code] = 0 if @last_entry[code] == nil
      journalfile = @db_path + '/' + dbfile + '.journal'
      $stderr.puts journalfile
      mtime = File::mtime(journalfile)

      if mtime > @last_update[code]
        $stderr.puts ' update'
        entry = ''
        time = 0
        File.open(journalfile).each{|line|
          if line =~ /<(update)?(add)?(delete)? key=/
            #start new
            entry = line
            match = /<(update)?(add)?(delete)? key='[^']+' timestamp='([0-9]+)'.*/.match(line)
            time = match[4].to_i
          else
            entry += line
          end
          if line =~ /<\/(update)?(add)?(delete)?>/ or line =~ /<(update)?(add)?(delete)? [^<]*\/>/
            #parse
            if entry != '' and time > @last_entry[code]
              doc = REXML::Document.new(entry)
              timestamp = doc.root.attributes['timestamp'].to_i
              @last_entry[code] = timestamp
              @journal_hash[timestamp] = [] if @journal_hash[timestamp] == nil
              @journal_hash[timestamp] << {
                'dictionary' => code,
                'user' => doc.root.attributes['user'],
                'key' => doc.root.attributes['key'],
                'thread' => doc.root.attributes['thread'],
                'action' => doc.root.name,
                'xmldoc' => doc.root.elements[1]
              }
            end
            entry = ''
          end
        }
        @last_update[code] = Time.now
      end
    }
    $stderr.puts start + '>' + Time.now.to_s
    return @journal_hash
  end
 
  def get_latest(dictionary, key)
    limited = @journal_hash.select{|k,a|
      la = a.select{|v| v['dictionary'] == dictionary and v['key'] == key}
      la.size > 0
    }
    time = 0
    last = nil
    limited.each{|t,a|
      if t > time
        la = a.select{|v| v['dictionary'] == dictionary and v['key'] == key}
        last = la[0]
        la[0]['time'] = t
        time = t
      end
    }
    return last
  end
end
