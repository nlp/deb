require 'dict/dict-sedna'
require 'dict/wn2'

class Sumo < WordNet2
  def initialize( db_path, database, key_path='', env = nil, admindict = nil )
    super
    @servlet = SumoServlet
  end
  
  def get_list(query, array=nil)
    synsets = {}
    db_query = ''
    query ||= ''

    top = @key_path.split('/')[1]
    rest = @key_path.split('/')[2..-1].join('/')
   
   
    if query[0,top.length] == top
      search_string = parse_conditions(query, array)
      db_query = "for $x in collection('#{@collection}')/#{top} where #{search_string} return $x"
    else
      if (pos = query.index('*')) != nil
        $stderr.puts pos
        $stderr.puts query.length
        search = query.gsub('*','')
        if pos == 0 and query.count('*') > 1
          db_query = "for $x in collection('#{@collection}')/#{top} where contains($x/#{rest}, '#{search}') return $x"
        elsif pos == 0
          db_query = "for $x in collection('#{@collection}')/#{top} where ends-with($x/#{rest}, '#{search}') return $x"
        else
          db_query = "for $x in collection('#{@collection}')/#{top} where starts-with($x/#{rest}, '#{search}') return $x"
        end
      else
        #db_query = "for $x in collection('#{@collection}')/#{top} where some $p in $x/#{rest} satisfies ($p='#{query}') return $x"
        db_query = "for $x in collection('#{@collection}')/#{top} where $x/#{rest}='#{query}' return $x"
      end
    end
    $stderr.puts db_query
    $stderr.puts "start " + Time.now.to_s
    query(db_query).each{|s|
      #s = get(id)
      $stderr.puts s 
      doc = load_xml_string(s.to_s)
      id = doc.root.find('/CONCEPT/ID').first.content.to_s
      synsets[id] = {}
      synsets[id]['form'] = id
    }
    
    $stderr.puts "end " + Time.now.to_s
    $stderr.puts synsets.to_s
    synsets 
  end

  #count revers ilr
  def get_rilr(xmldoc)
    res = ''
    doc = load_xml_string(xmldoc)
    doc.find('/CONCEPT/ID').each{|el|
      id = el.content.to_s
      xquery_to_hash('[CONCEPT[ILR="'+id+'"]]').each{|id2, xml2|
        doc2 = load_xml_string(xml2.to_s)
        ilr = doc2.find('/CONCEPT/ID').to_a.first.content.to_s
        link = doc2.find("/CONCEPT/ILR[.='"+id+"']").first
        el2 = XML::Node.new('RILR', ilr)
        el2['type'] = link['type'].to_s
        doc.root << el2
      }
    }
    return doc.to_s
  end
  
   def get_tree(xmldoc, dir = 'ILR')
    res = []
    previous = ''
    begin
      $stderr.puts dir
      $stderr.puts previous
      ar = get_up_tree(xmldoc, dir)
      if previous == ar[0]
        ar[0] = 'koren'
      else
        previous = ar[0]
      end
      res << ar
      $stderr.puts ar[0]
      if ar[0] != 'koren'
        #$stderr.puts "collection('#{@collection}')/CONCEPT[ID='#{ar[0]}']"
        #@manager.query("collection('#{@collection}')/CONCEPT[ID='#{ar[0]}']") { |s|
        s = get(ar[0].to_s)
          if dir == 'RILR'
            xmldoc = get_rilr( s.to_s )
          else
            xmldoc = s.to_s
          end
        #}
        if xmldoc == ''
          $stderr.puts 'baf'
        end
      end
    end while ar[0] != 'koren'
    return res
  end

  def get_up_tree(xmldoc, dir = 'ILR')
    $stderr.puts 'uptree'
    doc = REXML::Document.new( xmldoc )
    upilr = REXML::XPath.first( doc, "/CONCEPT/#{dir}[@type='subclass']/text()").to_s
    if upilr == '' or upilr == 'Entity'
      upilr = 'koren'
    end
    id = REXML::XPath.first( doc, '/CONCEPT/ID/text()').to_s
    res = [upilr, id, id]
    return res  
  end

  def get_subtree(xmldoc, dir= 'ILR')
    res = []
    doc = REXML::Document.new( xmldoc )
    origId = doc.elements['CONCEPT/ID'].text
    REXML::XPath.each( doc, "/CONCEPT/#{dir}[@type='subclass']") { |el|
      id = el.text
      res << [origId, id, id]
    }
    return res
  end
  
  def get_full_subtree(xmldoc, dir= 'ILR')
    res = []
    doc = REXML::Document.new( xmldoc )
    origId = doc.elements['CONCEPT/ID'].text
    new = []
    REXML::XPath.each( doc, "/CONCEPT/#{dir}[@type='subclass']") { |el|
      id = el.text
      res << [origId, id, id]
      new << id
    }

    #get subtree for each new id recursively
    new.each{|id|
      xml = get(id)
      if dir == 'RILR'
        xml = get_rilr(xml)
      end
      newres = get_full_subtree(xml, dir)
      res = res.concat(newres)
    }
    return res
  end

  #return literals for synset
  def get_literals(id)
    begin
      xml = get(id)
      doc = load_xml_string(xml.to_s)
      text = doc.find('/CONCEPT/ID').to_a.first.content.to_s
    rescue
      text = ''
    end
    return text
  end

  def topmost_entries
    count = 0
    topmost = {}
    top = @key_path.split('/')[1]
    xquery_to_hash("/#{top}[not(ILR)]").each {|id,xml|
      topmost[id] = ''
      count += 1
      break if count > 20
    }
    return topmost
  end
end
