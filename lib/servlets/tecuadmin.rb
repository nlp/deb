require 'template'
require 'convert'
require 'dump'
require 'import'
require 'pp'

class TecuAdminServlet < WEBrick::HTTPServlet::AbstractServlet
  def initialize( server, infodict, base_path, template_path, xslt_path )
    @infodict = infodict
    @server = server

    @servername = nil
    @template_path = template_path
    @base_path = base_path
    @xslt_path = xslt_path
  end

  def get_servername(server, request)
    servername = 'https://' + request.host
  end

  #load dictionary class names from lib/modules/dicts*
  def load_dict_names
    dicts = []
    path = @base_path + '/lib/modules'
    Dir.glob(path + '/dicts*'){|filename|
      File.open(filename).each{|line|
        dicts << {'class' => line.strip} if line.strip != ''
      }
    }
    return dicts.sort{|a,b| a['class']<=>b['class']}
  end

  def checkadmin(user)
    if @server[:Authenticator].userdb.check_permissions(user, 'admin', true)
      return true
    else
      return false
    end
  end

  def allowed_dirs
    allow_dir = ['files', 'files/wn', 'xpi']
    @infodict.list_dicts.each{ |dcode,name|
      #allow_dir << 'files/'+dcode
      allow_dir << 'xpi/'+dcode
    }
    @infodict.list_services.each{|scode,name|
      allow_dir << 'files/'+scode
    }
    return allow_dir
  end

  def save_uploaded_file(filedata, filename, dir)
    allow_dir = allowed_dirs
    dirpath = nil
    $stderr.puts dir + filename
    case dir
      when 'xslt'
        dirpath = @xslt_path
      when 'template'
        dirpath = @template_path
      else
        if allow_dir.include?(dir)
          dirpath = @base_path + '/' + dir
        else
          return false
        end
    end
    if dirpath != nil
      if not File.exists?(dirpath)
        Dir.mkdir(dirpath, 0775)
        File.new(dirpath).chmod(0775)
      end
      f = File.new(dirpath+'/'+filename, 'w')
      f.syswrite(filedata)
      f.chmod(0664)
    else
      return false
    end
  end

  #convert from LMF to DEBVisDic format
  def convert_from_lmf(src)
    xslt = XML::XSLT.new()
    xslt.xslfile = @xslt_path + 'kyoto-lmf2deb.xslt'
    xslt.xml = src
    s = xslt.serve()
    temp = Tempfile.new('lmfconv')
    temp.write(s)
    temp.flush
    temp.close
    return temp.path
  end

  #unmount old dicts, mount new
  def remount
    @infodict.array.each_key{ |code|
      @server.umount('/'+code)
    }
    @infodict.array = {}
    text = ''
    @infodict.xquery_to_array('/dict').sort.each{ |xml|
      code = ''
      file = ''
      doc = @infodict.load_xml_string(xml)
      code = doc.find('/dict/code').first.content.to_s
      keypath = doc.find('/dict/key').first.content.to_s
      classname = doc.find('/dict/class').first.content.to_s
      serverclass = get_class(classname)
      $stderr.puts code+':'+file
      text += code+':'+file + "\n"
      if code != '' and file != '' and File.exists?(@infodict.db_path + '/' + file)
        @infodict.array[code] = serverclass.new( @infodict.db_path, file, keypath )
        @infodict.array[code].xslt_sheets = {}
        doc.find('/dict/xslts/xslt').each{|el|
          xname = el.find('name').first.content
          xfile = el.find('file').first.content
          @infodict.array[code].xslt_sheets[xname] = @xslt_path + '/' + xfile
        }
      end
    }
    @infodict.array.each_key { |code|
      @server.mount '/'+code, SlovServlet, @infodict.array[code]
    }

    text
  end

  #update htpasswd file for services
  def update_htpasswd(userservs=nil)
    @infodict.list_services(true).each{|scode, htfile|
      if htfile.to_s != '' and File.file?(htfile) and (not userservs.nil? and userservs.include?(scode))
        $stderr.puts "update htpassd for #{scode}-#{htfile}"
        htf = File.new(htfile, 'w')
        @infodict.get_passwd(scode).each{|h|
          $stderr.puts h['user']+':'+h['pass']
          htf.puts h['user']+':'+h['pass']
        }
        htf.close
      end
    }
  end

  def do_GET(request, response)
    user = request.attributes[:auth_user].to_s
    @servername = get_servername(@server, request) if @servername == nil
    $stderr.puts @servername
    case request.query['action']
    when 'listusers'
      testperm = checkadmin(user)
      if testperm != true
        response['Content-Type'] = 'text/html; charset=utf-8'
        response.body = HTMLTemplate.new({'noaccess'=>1}).output(@template_path + '/listusers.tmpl')
        return response.body
      end
      response['Content-Type'] = 'text/html; charset=utf-8'
      users = {'no_service'=>[]}
      cssclass = 1

      #edit one user
      edituser = []
      if request.query['edituser'] != nil and request.query['edituser'] != ''
        begin
          xml = @infodict.get('user'+request.query['edituser'].to_s)
          $stderr.puts xml
          doc = @infodict.load_xml_string(xml)
          login = doc.find('/user/login').first.content.to_s
          name = doc.find('/user/name').first.content.to_s unless doc.find('/user/name').first.nil?
          email = doc.find('/user/email').first.content.to_s unless doc.find('/user/email').first.nil?
          org = doc.find('/user/org').first.content.to_s unless doc.find('/user/org').first.nil?
          addr = doc.find('/user/addr').first.content.to_s unless doc.find('/user/addr').first.nil?
          comment = doc.find('/user/comment').first.content.to_s unless doc.find('/user/comment').first.nil?
          admin =  (doc.find('/user/admin').first != nil and doc.find('/user/admin').first.content == 'true')? true:false
          perm = @server[:Authenticator].userdb.get_perms[login]['tecu_tecu'].to_s
          edituser << {'login'=>login, 'name'=>name, 'email'=>email, 'org'=>org, 'addr'=>addr, 'comment'=>comment, 'admin'=>admin, 'cssclass'=>cssclass, 'perm'=>perm, 'perm_'+perm=>'1'}
        rescue => e
          $stderr.puts e
        end
      end

      #user list
      userlist = []
      query = '/user[services/service/@code="tecu"]' 

      @infodict.xquery_to_array(query).sort.each{ |xml|
        doc = @infodict.load_xml_string(xml)
        login = doc.find('/user/login').first.content.to_s
        name = ''
        name = doc.find('/user/name').first.content.to_s unless doc.find('/user/name').first.nil?
        if request.query['detail'].to_s == '1'
          email = doc.find('/user/email').first.content.to_s unless doc.find('/user/email').first.nil?
          org = doc.find('/user/org').first.content.to_s unless doc.find('/user/org').first.nil?
          addr = doc.find('/user/addr').first.content.to_s unless doc.find('/user/addr').first.nil?
          comment = doc.find('/user/comment').first.content.to_s unless doc.find('/user/comment').first.nil?
          admin =  (doc.find('/user/admin').first != nil and doc.find('/user/admin').first.content == 'true')? true:false
          perm = @server[:Authenticator].userdb.get_perms[login]['tecu_tecu'].to_s
        end
        if request.query['detail'].to_s == '1'
          userlist << {'login'=>login, 'name'=>name, 'email'=>email, 'org'=>org, 'addr'=>addr, 'comment'=>comment, 'admin'=>admin, 'cssclass'=>cssclass, 'perm'=>perm}
        else
          userlist << {'login'=>login, 'name'=>name}
        end
      }


      tmpl_params = {'users'=>userlist, 'edituser'=>edituser}
      tmpl_params['deluser'] = request.query['delete'].to_s if request.query['delete'] != nil and request.query['delete'].to_s != '' 

      api = request.query['api'].to_s
      callback = request.query['callback'].to_s

      if api == '1'
        if callback != ''
          response['Content-Type'] = 'text/html; charset=utf-8'
        else
          response['Content-Type'] = 'application/json; charset=utf-8'
        end
        responsetext = userlist.to_json
        responsetext = callback + '(' + responsetext + ');' if callback != ''
        response.body = responsetext
        return response.body
      end

      response.body = HTMLTemplate.new(tmpl_params).output(@template_path + '/listusers.tmpl')
      
    when 'edituser'
      testperm = checkadmin(user)
      if testperm != true
        response['Content-Type'] = 'text/html; charset=utf-8'
        response.body = HTMLTemplate.new({'noaccess'=>1}).output(@template_path + '/listusers.tmpl')
        return response.body
      end
      newuser = false
      api = request.query['api'].to_s
      callback = request.query['callback'].to_s
      
      response['Content-Type'] = 'text/html; charset=utf-8'      
      if request.query['login'] != nil and request.query['login'] != ''
        login =request.query['login']
        
        #new user - check if login exists
        if request.query['new'] != nil and request.query['new'] == '1'
          if @infodict.get('user'+login).to_s != ''
            responsetext = 'login exists'
            responsetext = responsetext.to_json if api == '1'
            responsetext = callback + '(' + responsetext + ');' if callback != ''
            response.body = responsetext
            return response.body
          end
        end
        
        #if password is entered, crypt it
        #if new user, generate new password
        #else use the existing one
        if request.query['pass'] != nil and request.query['pass'].to_s != ''
          newpass = request.query['pass']
          pass = newpass.crypt(WEBrick::Utils::random_string(2))
        elsif request.query['new'] != nil and request.query['new'] == '1'
          newpass = WEBrick::Utils::random_string(8)
          pass = newpass.crypt(WEBrick::Utils::random_string(2))
          newuser = true
        else
          pass = @server[:Authenticator].userdb[login]
        end

        email = request.query['email'].to_s
        PP::pp(request.query, $stderr)
        $stderr.puts request.query

        xml = '<user>'
        xml += '<login>'+login+'</login>'
        xml += '<name>'+request.query['name'].to_s+'</name>'
        xml += '<email>'+email+'</email>'
        xml += '<org>'+request.query['org'].to_s+'</org>'
        xml += '<addr>'+request.query['addr'].to_s+'</addr>'
        xml += '<comment>'+request.query['comment'].to_s+'</comment>'
        xml += '<pass>'+pass+'</pass>'
        xml += '<admin>true</admin>' if request.query['admin'] != nil and request.query['admin'] == 'on'
        xml += '<services><service code="tecu">'
        xml += '<dict code="tecu" perm="'+request.query['perm'].to_s+'"/>'
        xml += '</service></services>'
        xml += '</user>'

        @infodict.update('user'+login, xml)
        @server[:Authenticator].userdb.reload(true)
        update_htpasswd(['tecu'])
        #@server[:Authenticator].userdb.load_permissions()
        
        #send email
        if newuser
          message = IO.read(@template_path + '/mailnew.tmpl')
        elsif newpass !=nil
          message = IO.read(@template_path + '/mailnewpass.tmpl')
        else
          message = IO.read(@template_path + '/mailchange.tmpl')
        end
        message.gsub!('#{user}', login)
        message.gsub!('#{email}', email)
        message.gsub!('#{pass}', newpass) if newpass != nil
        message.gsub!('#{server}', @servername)
        $stderr.puts message
        if email != ''
          mail_send(email, 'deb@aurora.fi.muni.cz', message, 'debucty@aurora.fi.muni.cz')
        end

      end
      
      if api == '1'
        responsetext = 'user created'.to_json 
        responsetext = callback + '(' + responsetext + ');' if callback != ''
        response.body = responsetext
        return response.body
      end

      response.set_redirect(WEBrick::HTTPStatus::SeeOther, @servername + '/tezk/admin?action=listusers#' + login)

    when 'genpass'
      testperm = checkadmin(user)
      if testperm != true
        response['Content-Type'] = 'text/html; charset=utf-8'
        response.body = HTMLTemplate.new({'noaccess'=>1}).output(@template_path + '/listusers.tmpl')
        return response.body
      end

      api = request.query['api'].to_s
      callback = request.query['callback'].to_s

      if request.query['login'] != nil and request.query['login'] != ''
        login = request.query['login']
        newpass = WEBrick::Utils::random_string(8)
        newcrypt = newpass.crypt(WEBrick::Utils::random_string(2))
        begin
          xml = @infodict.get('user'+login)
        rescue =>e
          if api == '1'
            responsetext = 'invalid login'.to_json 
            responsetext = callback + '(' + responsetext + ');' if callback != ''
            response.body = responsetext
            return response.body
          else
            response.body = "invalid login"
            return response.body
          end
        end
        doc = @infodict.load_xml_string(xml.to_s)
        email = doc.find('/user/email').first.content.to_s
        doc.find('/user/pass').first.content = newcrypt
        @infodict.update('user'+login, doc.to_s)
        @server[:Authenticator].userdb.reload(true)

        #send email
        message = IO.read(@template_path + '/mailnewpass.tmpl')
        message.gsub!('#{user}', login)
        message.gsub!('#{email}', email)
        message.gsub!('#{pass}', newpass) if newpass != nil
        message.gsub!('#{server}', @servername)
        $stderr.puts message
        if email != ''
          mail_send(email, 'deb@aurora.fi.muni.cz', message, 'debucty@aurora.fi.muni.cz')
        end

        if api == '1'
          responsetext = 'password reset'.to_json 
          responsetext = callback + '(' + responsetext + ');' if callback != ''
          response.body = responsetext
          return response.body
        end

        response.set_redirect(WEBrick::HTTPStatus::SeeOther, @servername + '/tezk/admin?action=listusers')
        response.body += newcrypt
      else
        if api == '1'
          responsetext = 'invalid login'.to_json 
          responsetext = callback + '(' + responsetext + ');' if callback != ''
          response.body = responsetext
          return response.body
        else
          response.body = "invalid login"
          return response.body
        end
      end
      
    when 'changepass'
      if request.query['new'] != nil and request.query['new'] != ''
        newcrypt = request.query['new'].crypt(WEBrick::Utils::random_string(2))
        xml = @infodict.get('user'+user)
        doc = @infodict.load_xml_string(xml.to_s)
        doc.find('/user/pass').first.content = newcrypt
        @infodict.update('user'+user, doc.to_s)
        @server[:Authenticator].userdb.reload(true)
        update_htpasswd(['cpa'])

        response.set_redirect(WEBrick::HTTPStatus::SeeOther, @servername + '/tezk/admin?action=changepass&set=1')
        response.body += newcrypt
      end
      params = {'user'=>user}
      if request.query['set'] == '1'
        params['set'] = 1
      end
      response['Content-Type'] = 'text/html; charset=utf-8'
      response.body = HTMLTemplate.new(params).output(@template_path + '/changepass.tmpl')
      return response.body

    when 'deluser'
      testperm = checkadmin(user)
      if testperm != true
        response['Content-Type'] = 'text/html; charset=utf-8'
        response.body = HTMLTemplate.new({'noaccess'=>1}).output(@template_path + '/listusers.tmpl')
        return response.body
      end
      login = request.query['login'].to_s
      api = request.query['api'].to_s
      callback = request.query['callback'].to_s
      if login != ''
        begin
          xml = @infodict.get('user'+login)
        rescue=>e
          if api == '1'
            responsetext = 'invalid login'.to_json 
            responsetext = callback + '(' + responsetext + ');' if callback != ''
            response.body = responsetext
            return response.body
          else
            response.body = "invalid login"
            return response.body
          end
        end
        doc = @infodict.load_xml_string(xml.to_s)
        email = doc.find('/user/email').first.content.to_s

        @infodict.delete('user'+login)
        @server[:Authenticator].userdb.reload(true)

        #send email
        message = IO.read(@template_path + '/maildel.tmpl')
        message.gsub!('#{user}', login)
        message.gsub!('#{email}', email)
        $stderr.puts message
        if email != ''
          mail_send(email, 'deb@aurora.fi.muni.cz', message, 'debucty@aurora.fi.muni.cz')
        end
        if api == '1'
          responsetext = 'user deleted'.to_json 
          responsetext = callback + '(' + responsetext + ');' if callback != ''
          response.body = responsetext
          return response.body
        end
      else
        if api == '1'
          responsetext = 'invalid login'.to_json 
          responsetext = callback + '(' + responsetext + ');' if callback != ''
          response.body = responsetext
          return response.body
        end
      end
      response.set_redirect(WEBrick::HTTPStatus::SeeOther, @servername + '/tezk/admin?action=listusers')

    when 'remount'
      text = remount
      response['Content-Type'] = 'text/plain'
      response.body = text

    when 'dump'
      filename = 'tecu_' + Time.now.strftime('%Y%m%d-%H%M') + '.xml'
      outfile = @base_path + '/files/tecu/exports/' + filename
      logfile = '/var/log/deb-server/tecu' + Time.now.strftime('%Y%m%d-%H%M') + '.log'
      Thread.new { dump('tecu', outfile, logfile, false, false, nil, nil, nil) }
      response.set_redirect(WEBrick::HTTPStatus::SeeOther, @servername + '/tezk/admin?action=showlog&type=dump&log=' + logfile + '&dump=' + filename)
      response.body
      

    when 'import'
      res = HTMLTemplate.new().output(@template_path + '/import.tmpl')
      response.body = res
      return response.body
    when 'import_upload'
      if request.query['file'].to_s == ''
        response.body = 'no file data'
        return response.body
      end
      temp = Tempfile.new("import_temp")
      filedata = request.query['file']
      temp.syswrite(filedata)
      temp.close
      srcfile = temp.path
      code = 'tecu'
      root = 'entry'
      key = '/entry/@id'
      logfile = '/var/log/deb-server/' + code + Time.now.strftime('%Y%m%d-%H%M') +'.log'
      delete = false
      delete = true if request.query['delete'].to_s == 'on'
      overwrite = false
      overwrite = true if request.query['overwrite'].to_s == 'on'
      $stderr.puts srcfile.to_s + ';' +  code.to_s + ';' +  logfile.to_s+ ';' +  delete.to_s + ';' +overwrite.to_s + ';' + key + ';' + root
      Thread.new{ import(code, srcfile, logfile, key, root, delete, overwrite) }      
      response.set_redirect(WEBrick::HTTPStatus::SeeOther, @servername + '/tezk/admin?action=showlog&type=import&log=' + logfile)
    when 'showlog'
      logfile = request.query['log'].to_s
      dumpfile = request.query['dump'].to_s
      type = request.query['type'].to_s
      result = %x[tail -n 20 #{logfile}]
      lastline = ''
      result.to_s.each{|line| lastline=line if line != ''}
      $stderr.puts lastline

      response['Content-Type'] = 'text/html; charset=utf-8'
      response.body = '<html><head><meta http-equiv="Refresh" content="5"/></head><body><a href="/tezk/admin">admin</a><pre>'+result.to_s+'</pre>'
      if type == 'dump' and lastline[0,8] == 'DUMP END'
        response.body += '<p>Soubor je připraven ke stažení '+dumpfile
        response.body += ', <a href="/tezk/editor/exports/' + dumpfile + '">download</a>'
        response.body += '</p>'
      end
      if type == 'import' and lastline[0,10] == 'IMPORT END'
        response.body += '<p>Import dokončen.</p>'
      end
      response.body += '</body></html>'


    when 'export'
      begin
        response['Content-Type'] = 'text/plain; charset=utf-8'
        response.body = @infodict.array['tecu'].get_all
      rescue => e
        $stderr.puts e
        $stderr.puts e.backtrace
        response.body = e
      end
      return response.body
      
    else
      testperm = checkadmin(user)
      tmpl_params = {}
      tmpl_params['admin'] = 1 if testperm
      response.body = HTMLTemplate.new(tmpl_params).output(@template_path + '/admin.tmpl')
      
    end
 
  end
  
  alias do_POST do_GET
end
