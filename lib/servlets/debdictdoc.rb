require 'json'

class DebdictDocServlet < WEBrick::HTTPServlet::AbstractServlet
  def initialize(server, dict)
    @dict = dict
  end

  def do_GET(request, response)
    case request.query['action']
    when 'init'
      response['Content-type'] = "text/plain; charset=utf-8"
      user = request.attributes[:auth_user].to_s
      dics = @dict.list_dictionaries(user)
      response.body = dics.join(";")
    when 'get_settings'
      $stderr.puts 'GET settings'
      set = @dict.get_settings(request.attributes[:auth_user].to_s, @dict.service_name)
      response["Content-type"] = "application/json; charset=utf-8"
  		response.body = set.to_json
    when 'save_settings'
      $stderr.puts 'save settings'
      data = URI.unescape(request.query['data']).to_s
      data_hash = JSON.parse(data)
      set = @dict.save_settings(request.attributes[:auth_user].to_s, data_hash, @dict.service_name)
      response["Content-type"] = "application/json; charset=utf-8"
  		response.body = data_hash.to_json
    else
      response.header["Cache-Control"] = "no-cache"
      response['Content-type'] = "text/plain; charset=utf-8"
      response.body += "hello "+request.attributes[:auth_user].to_s
    end
  end
  alias do_POST do_GET
end
