require 'template'
require 'cobravsmongoose'
#require 'unicode'
require 'uri'
require 'fileutils'
require 'utf8string'

class FniServlet < DictServlet
 #devel
#def SurnamesServlet.get_instance config, *options
#load __FILE__
#SurnamesServlet.new config, *options
#end

  def initialize( server, dict, array )
    super
    @servername = nil
    @basehref = 'https://deb.fanuk.net:8060/'
  end

  def preparse_xml(xml)
    xml = xml.to_s.gsub(' ib.',' <src>ib.</src>')
    doc = XML::Document.string(xml)
    doc.find('//arch|//ppkk|//other_info|//references').each{|t|
      $stderr.puts '****'+t.content.to_s
      text = t.to_s.gsub("\n",'<br/>')
      text.gsub!('<cit>', '')
      text.gsub!('</cit>', '')
      text.gsub!('<cit/>', '')
      text.gsub!('<ex>', '')
      text.gsub!('</ex>', '')
      text.gsub!('<ex/>', '')
      text.gsub!(/<arch type="[^"]*">/, '')
      text.gsub!('<arch>', '')
      text.gsub!('<arch/>', '')
      text.gsub!('<arch type=""/>', '')
      text.gsub!('</arch>', '')
      text.gsub!('<med>', '')
      text.gsub!('</med>', '')
      text.gsub!('<med/>', '')
      text.gsub!('<igi>', '')
      text.gsub!('</igi>', '')
      text.gsub!('<igi/>', '')
      text.gsub!('<other>', '')
      text.gsub!('</other>', '')
      text.gsub!('<other/>', '')
      text.gsub!('<other_info>', '')
      text.gsub!('</other_info>', '')
      text.gsub!('<other_info/>', '')
      text.gsub!(/<ppkk category="[^"]*">/, '')
      text.gsub!(/<ppkk category="[^"]*" language="[^"]*">/, '')
      text.gsub!(/<ppkk language="[^"]*">/, '')
      text.gsub!('<ppkk>', '')
      text.gsub!('</ppkk>', '')
      text.gsub!('<ppkk/>', '')
      text.gsub!('<dafn>', '')
      text.gsub!('</dafn>', '')
      text.gsub!('<dafn/>', '')
      text.gsub!('<references>', '')
      text.gsub!('</references>', '')
      text.gsub!('<references/>', '')
      t.content = text
      $stderr.puts '****'+t.content.to_s
    }
    $stderr.puts doc.to_s 
    xml = doc.to_s.gsub('&lt;br/&gt;','<br/>')
    xml.gsub!('&lt;i&gt;', '<i>')
    xml.gsub!('&lt;/i&gt;', '</i>')
    xml.gsub!('&lt;b&gt;', '<b>')
    xml.gsub!('&lt;/b&gt;', '</b>')
    xml.gsub!('&lt;sn&gt;', '<sn>')
    xml.gsub!('&lt;/sn&gt;', '</sn>')
    xml.gsub!('&lt;F&gt;', '<b>')
    xml.gsub!('&lt;/F&gt;', '</b>')
    xml.gsub!('&lt;xr&gt;', '<xr>')
    xml.gsub!('&lt;/xr&gt;', '</xr>')
    xml.gsub!('&lt;me&gt;', '<me>')
    xml.gsub!('&lt;/me&gt;', '</me>')
    xml.gsub!('&lt;var&gt;', '<var>')
    xml.gsub!('&lt;/var&gt;', '</var>')
    xml.gsub!('&lt;X&gt;', '<var>')
    xml.gsub!('&lt;/X&gt;', '</var>')
    xml.gsub!('&lt;src&gt;', '<src>')
    xml.gsub!('&lt;/src&gt;', '</src>')
    xml.gsub!('. <b>', '.<br/><b>')
    xml.gsub!(/<br\/>[[:space:]]*<br\/>/,'<br/>')
    $stderr.puts xml
    return xml
  end

  def gen_src_js
    src_b, src_o = @dict.load_sources
    output = "function get_src_array() {\nvar sources_list = new Array(\n"
    output += src_b.map{|x| '"'+x['short'].gsub('<', '&lt;').gsub('>', '&gt;').strip+'"'}.join(",\n")
    output += ");\nreturn sources_list;\n}\n\nvar got_src_list = '1';\n"
    file = File.new('/var/lib/deb-server/files/fni/srclist.js','w')
    file.write(output)
    file.close
  end

  def do_GET(request, response)
    dict_path = request.path[1..-1]
    #@servername = get_servername(@server, request) if @servername == nil
    @servername = @basehref[0..-2]
    #$stderr.puts @servername
    user = request.attributes[:auth_user].to_s
    request.query['action'] = 'query' if request.query['action'] == nil or request.query['action'] == ''
    case request.query['action']
    when 'progress'
      namefrom = request.query['namefrom'].to_s
      nameto = request.query['nameto'].to_s
      if namefrom == '' or nameto == ''
        tmpl_params = {'basehref'=>@basehref}
      else
        tmpl_params = @dict.get_progress_report(namefrom, nameto, request.query['dict'].to_s)
        tmpl_params['data'] = 1
        tmpl_params['basehref'] = @basehref
        tmpl_params['namefrom'] = namefrom
        tmpl_params['nameto'] = nameto
        tmpl_params['dict'] = 'Main list' if request.query['dict'].to_s == 'fni'
        tmpl_params['dict'] = '1881 reserve' if request.query['dict'].to_s == 'res1881'
      end 
        response['Content-Type'] = 'text/html; charset=utf8'
        res = HTMLTemplate.new(tmpl_params).output(@dict.template_path + '/progress.tmpl')
        response.body = res
      
    when 'stat'
      statfile = '/var/lib/deb-server/files/fni/stats.txt'
      @dict.get_statistics if not File.exists?(statfile) or File::mtime(statfile).to_i < (Time.now.to_i - 12*60*60)
      tmpl_params = {'basehref'=>@basehref, 'week'=>[], 'freq'=>[], 'comp'=>[], 'comp2'=>[], 'time'=>File::mtime(statfile).to_s}
      weeks = {}
      comp2 = {}
      beforeweek = ''
      f = File.open(statfile, 'r')
      f.each{|l|
        la = l.strip.split(';')
        if la[0].include?('_')
          laa = la[0].split('_')
          case laa[0]
            when 'comp'
              tmpl_params['comp'] << {'user'=>laa[1], 'count'=>la[1].to_i}
            when 'comp2'
              comp2[laa[1]] = {} if comp2[laa[1]].nil?
              comp2[laa[1]][laa[2]] = la[1].to_i
            when 'freq'
              tmpl_params[la[0]] = la[1]
            when 'week'
              wy = laa[1]+'_'+laa[2]
              weeks[wy] = [] if weeks[wy].nil?
              weeks[wy] << {'user'=>laa[3], 'count'=>la[1]}
          end
        else
          tmpl_params[la[0]] = la[1]
        end
      }
      #comp2.sort{|x,y| x[0]<=>y[0]}.each{|u,h|
      #  tmpl_params['comp2'] << {'user'=>u, 'me'=>h['me'], 'menew'=>h['menew'], 'xref'=>h['xref'], 'xrefnew'=>h['xrefnew'], 'quar'=>h['quar']}
      #}
      weekcount = 0
      webe = {}
      weeks.sort{|x,y| y[0]<=>x[0]}.each{|a|
        weekcount += 1
        if weekcount == 6
          beforeweek = a[0]
        end
        if weekcount <= 6
          tmpl_params['week'] << {'week'=>a[0], 'users'=>a[1]}
        else
          a[1].each{|h|
            webe[h['user']] = 0 if webe[h['user']].nil?
            webe[h['user']] += h['count'].to_i
          }
        end
      }
      befusers = []
      webe.each{|u,c|
        befusers << {'user'=>u, 'count'=>c.to_s}
      }
      tmpl_params['week'] << {'week'=>'before '+beforeweek, 'users'=>befusers}
      tmpl_params['comp'].sort!{|x,y| y['count']<=>x['count']}

      response['Content-Type'] = 'text/html; charset=utf8'
      res = HTMLTemplate.new(tmpl_params).output(@dict.template_path + '/stats.tmpl')
      response.body = res
    when 'cluster'
      id = request.query['id']
      if id.to_s != ''
        refs = @dict.get_cluster(id.to_s)
        locked = []
        refs.each{|rid|
          lock = @server.info.who_locked('fni', rid)
          if lock != false and lock != user
            locked << {'user'=>lock.to_s, 'entry'=>rid}
          end
        }
        if locked.size > 0
          response.body = ''
          locked.each{|h|
            response.body += 'entry ' + h['entry'] + ' is locked by  ' + h['user'] + "\n"
          }
          return response.body
        else
        $stderr.puts 'locking'
          refs.each{|rid|
          $stderr.puts rid
            @server.info.lock_entry('fni', rid, user)
          }
        end

        text = @dict.get_cluster_html(refs, dict_path)
      else
        text = @dict.get_cluster_html(['__new__'], dict_path)
      end

      case dict_path
      when 'fni':
        pathinfo = 'FNI'
      when 'fnires':
        pathinfo = 'RESERVE LIST'
      end

        out = %Q[<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
  <html xmlns="http://www.w3c.org/1999/xhtml">
  <head>
        <base href="#{@basehref}"/>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <script type="text/javascript" src="files/surnames3.js?#{Time.now.to_i}"></script>
  <script type="text/javascript" src="files/global.js"></script>
  <script type="text/javascript" src="files/sm/subModal/submodalsource.js"></script>
  <script type="text/javascript" src="files/textedit.js"></script>
  <script type="text/javascript" src="files/srclist.js?#{Time.now.to_i}"></script>
  <link rel="stylesheet" href="files/surned.css" type="text/css"/>
  <link rel="stylesheet" href="files/sm/subModal/submodal.css" type="text/css"/>
</head>
<body onload="if (event.target == document) edit_onload();" class="bg#{dict_path}">
<div id="top_info" style="position:fixed;z-index:10;height:33px;width:100%;background:#ABCDEF;top:0;border-bottom: solid 1px black;">
<div id="loading_wait" style="display:none;position: fixed;left: 25%;top: 25%;z-index: 10;background-color: #eee;width: 400px;height: 100px;font-size: 110%;text-align: center; vertical-align: middle; line-height: 100px">loading, please wait...</div>
        <span id="pathinfo">#{pathinfo}</span>
<button onclick="alert(get_all())">test</button>
<button id="but_save" disabled="true" onclick="save_all();reload_main();">Save</button>
<button id="but_save_back" disabled="true" onclick="save_all(true);reload_main();">Save & close</button>
<button onclick="reload_main();unlock(true);">Close</button>
<span id="info_loading" style=""><b><i>Loading...</i></b></span>
<span id="info_saving" style="display:none"><b><i>Saving...</i></b></span>
<span id="info_saved" style="display:none"><b><i>Saved...</i></b></span>
</div>
<div id="inside" style="position:relative;margin-top:38px;">
        ]
        out2 = '</div><script type="text/javascript">var footer_loaded = true;</script></body></html>'
        response['Content-Type'] = 'text/html; charset=utf8'
        response.body = out+text.to_s+out2



    when 'save_cluster'
      perm = @dict.get_perm(user)
      if perm == 'w'
        data = request.query['data']
        data.gsub!('&lt;i&gt;', '<i>')
        data.gsub!('&lt;/i&gt;', '</i>')
        data.gsub!('&lt;b&gt;', '<b>')
        data.gsub!('&lt;/b&gt;', '</b>')
        data.gsub!('&lt;sn&gt;', '<sn>')
        data.gsub!('&lt;/sn&gt;', '</sn>')
        data.gsub!('&lt;F&gt;', '<b>')
        data.gsub!('&lt;/F&gt;', '</b>')
        data.gsub!('&lt;xr&gt;', '<xr>')
        data.gsub!('&lt;/xr&gt;', '</xr>')
        data.gsub!('&lt;me&gt;', '<me>')
        data.gsub!('&lt;/me&gt;', '</me>')
        data.gsub!('&lt;var&gt;', '<var>')
        data.gsub!('&lt;/var&gt;', '</var>')
        data.gsub!('&lt;X&gt;', '<var>')
        data.gsub!('&lt;/X&gt;', '</var>')
        data.gsub!('&lt;src&gt;', '<src>')
        data.gsub!('&lt;/src&gt;', '</src>')
        $stderr.puts data

        edoc = @dict.load_xml_string(data.to_s)
        edoc.find('/data/entry').each{|en|
          id = en['entry_id'].gsub('__', "'")
          id = URI.escape(id) unless id.include?("%")
          $stderr.puts en['entry_id'] + ' '+id
          edits = @dict.get_edits(id)
          $stderr.puts edits
          edits << {'author'=>user, 'time'=>Time.now.strftime('%Y-%m-%d %H:%M:%S')}

          doc = @dict.add_meta(en.to_s, edits)
          xml = doc.to_s.gsub('<?xml version="1.0" encoding="utf-8"?>','')
          @dict.update(id, xml)
          @server.info.unlock_entry('fni', id, user) if request.query['unlock'].to_s == 'true'
        }
        response.body = 'saved cluster' 
      else
        response.body = 'not authorized to save entry'
      end
      return response.body

    when 'addvar'
      entry_id = URI.escape(request.query['entry_id'].to_s)
      xref = URI.escape(request.query['xref'].to_s)
      if entry_id != '' and xref != '' and not entry_id.include?('(obj)')
        @dict.add_var(entry_id, xref)
      end
    when 'addvarsense'
      entry_id = URI.escape(request.query['entry_id'].to_s)
      xref = URI.escape(request.query['xref'].to_s)
      if entry_id != '' and xref != ''
        @dict.add_var_sense(entry_id, xref)
      end
    when 'addvarcheck'
      entry_id = URI.escape(request.query['entry_id'].to_s)
      xref = URI.escape(request.query['xref'].to_s)
      result = ''
      if dict_path == 'surnames' and entry_id != '' and xref != ''
        result = @dict.add_var_check(entry_id, xref, @dict_array['reserve'], @dict_array['res1881'], @dict_array['ren'])
      end
      response['Content-Type'] = 'text/plain; charset=utf8'
      response.body = result

    when 'xml'
      id = request.query['id']
      if id.to_s != ''
        xml = @dict.get_entry(id)
        @dict.var_lang(id)
        xml = @dict.apply_transform(request.query['tr'], xml.to_s) if request.query['tr'].to_s != ''
        response['Content-Type'] = 'text/xml; charset=utf8'
        response.body = xml.to_s
      end
      
    when 'edit'
      id = request.query['id']
      type = request.query['type'].to_s

      #check permissions
      perm = @dict.get_perm(user)
      if perm != 'w' and perm != 's'
        response['Content-Type'] = 'text/plain; charset=utf8'
        response.body = 'not authorized to edit'
        return response.body
      end
      
      if id.to_s != ''
        #check lock
        lock = @server.info.who_locked('fni', id)
        if lock != false and request.query['nolock'].to_s != '1' and lock != user
          response['Content-Type'] = 'text/plain; charset=utf8'
          response.body = 'this entry is locked by ' + lock.to_s
          return response.body
        else
          @server.info.lock_entry('fni', id, user)
        end
        #load
        xml = @dict.get_for_edit(id)
        $stderr.puts xml.to_s
      else
        if type == 'ref'
          xml = "<entry><hw></hw><me></me><author>#{user}</author></entry>"
        else
          xml = "<entry><hw></hw><sense><s>1</s><cit></cit><ex></ex></sense><author>#{user}</author></entry>"
        end
      end
      #response['Content-Type'] = 'application/vnd.mozilla.xul+xml; charset=utf8'
      response['Content-Type'] = 'text/html; charset=utf8'
      response.body = @dict.apply_transform('edit', xml.to_s)
      
    when 'preview'
      id = request.query['id'].to_s
      data = request.query['data'].to_s
      log = request.query['log'].to_s
      if id != '' or data != ''
        if id != ''
          xml = @dict.get_entry(id)
        else
          xml = data
        end
        xml = preparse_xml(xml.to_s)
        response['Content-Type'] = 'text/html; charset=utf8'
        response.body = @dict.apply_transform('preview', xml.to_s, [ ['show_log', '"'+log+'"'],['base', '"'+@basehref+'"'], ['path', '"'+dict_path+'"'] ])
      else
        response.body = ''
      end

    when 'multipreview'
      ids = request.query['ids'].to_s
      title = ''
      if request.query['hw'].to_s != ''
        title = 'Selection '+request.query['hw']
      end
      if request.query['search'].to_s != ''
      where = {'all'=>'all', 'expl'=>'expl.', 'eb'=>'E.B.', 'comment'=>'comment', 'heartland_gb'=> 'GB distrib.', 'other_info'=>'other info'}
        title = request.query['search'] + ' in ' + where[request.query['where']].to_s
      end
      $stderr.puts ids
        #output = %Q[<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
#output = %Q[  <html xmlns="http://www.w3c.org/1999/xhtml">
output = %Q[   <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<html>
  <head>
        <base href="#{@basehref}"/>
        <title>#{title}: FNI Preview</title>
        <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
        <link href="files/surnames.css" rel="stylesheet" type="text/css"/>
        <script src='files/preview.js' type='text/javascript'> </script>
        </head>
        <body class="preview">
            <a class="comment_link" id="comment_link" onclick="show_comments()">show requests</a> 
                / <a class="musings_link" id="musings_link" onclick="show_musings()">show musings</a>

        ]
        xmldata = {'_'=>[]}
        ids.split('!').each{|id|
        $stderr.puts id
          xml = @dict.get_entry(id)
          xml = preparse_xml(xml.to_s)
          lang = '_'
          if request.query['sort'].to_s == 'true'
            xmldoc = @dict.load_xml_string(xml)
            lang = xmldoc.find('//sense[@language!=""]').to_a.first['language'] unless xmldoc.find('//sense[@language!=""]').to_a.first.nil?
          end
          xmldata[lang] = [] if xmldata[lang].nil?
          xmldata[lang] << xml
        }
        xmldata.sort.each{|lang,xmla|
        $stderr.puts lang
          xmla.each{|xml|
            #xmlconv = @dict.apply_transform('multipreview', xml.to_s, [ ['base', '"'+@basehref+'"'], ['path', '"'+dict_path+'"'] ])
            xmlconv = @dict.apply_transform('multipreview', xml.to_s, [ ['path', '"'+dict_path+'"'] ]).sub('<html>','').sub('</html>','')
            output += xmlconv
          $stderr.puts xmlconv
          }
        }
        response['Content-Type'] = 'text/html; charset=utf8'
        output += '</body></html>'
        response.body = output
    
    when 'delete'
      perm = @dict.get_perm(user)
      if perm != 'w' and perm != 's'
        response['Content-Type'] = 'text/plain; charset=utf8'
        response.body = 'not authorized to edit entry'
        return response.body
      end
      
      id = request.query['id']
      hw = request.query['hw']
      search = request.query['search']
      if id.to_s != ''
        #check lock
        lock = @server.info.who_locked('fni', id)
        if lock != false and lock != user
          response['Content-Type'] = 'text/plain; charset=utf8'
          response.body = 'this entry is locked by ' + lock.to_s
          return response.body
        else
          @server.info.lock_entry('fni', id, user)
        end
        xml = @dict.get(id)
        $stderr.puts xml.to_s
        response['Content-Type'] = 'text/html; charset=utf8'
        response.body = @dict.apply_transform('del', xml.to_s, [ ['hw', '"'+hw.gsub("'","%27")+'"' ], ['search', '"'+search.gsub("'","%27")+'"'], ['base', '"'+@basehref+'"'], ['path', '"'+dict_path+'"'] ])
      else
        response.body = ''
      end
      
    when 'dodelete'
      $stderr.puts 'DO DELETE'
      perm = @dict.get_perm(user)
      if perm != 'w' and perm != 's'
        response['Content-Type'] = 'text/plain; charset=utf8'
        response.body = 'not authorized to edit entry'
        return response.body
      end
      
      id = URI.escape(request.query['id'])
      $stderr.puts request.query['id'] + ' ' + id
      hw = request.query['hw']
      search = request.query['search']
      if id.to_s != ''
        #check lock
        lock = @server.info.who_locked('fni', id)
        if lock != false and lock != user
          response['Content-Type'] = 'text/plain; charset=utf8'
          response.body = 'this entry is locked by ' + lock.to_s
          return response.body
        else
          @server.info.lock_entry('fni', id, user)
        end
        @dict.delete(id)
      end
      response.set_redirect(WEBrick::HTTPStatus::SeeOther, @servername + '/' + dict_path + '?action=query&hw=' + hw +'&search=' + search)

    when 'save'
      perm = @dict.get_perm(user)
      new = false
      if perm == 'w'
        id = request.query['id']
        new = request.query['new'].to_s
        #check lock
        lock = @server.info.who_locked('fni', id)
        if lock != false and lock != user
          response['Content-Type'] = 'text/plain; charset=utf8'
          response.body = 'this entry is locked by ' + lock.to_s
          return response.body
        end
        if new == 'true' and @dict.entry_exists(id)
          response['Content-Type'] = 'text/plain; charset=utf8'
          response.body = 'entry ' + id.to_s + ' already in database'
          return response.body
        end

        data = request.query['data']
        data.gsub!('&lt;i&gt;', '<i>')
        data.gsub!('&lt;/i&gt;', '</i>')
        data.gsub!('&lt;b&gt;', '<b>')
        data.gsub!('&lt;/b&gt;', '</b>')
        data.gsub!('&lt;sn&gt;', '<sn>')
        data.gsub!('&lt;/sn&gt;', '</sn>')
        data.gsub!('&lt;var&gt;', '<var>')
        data.gsub!('&lt;/var&gt;', '</var>')
        data.gsub!('&lt;me&gt;', '<me>')
        data.gsub!('&lt;/me&gt;', '</me>')
        data.gsub!('&lt;xr&gt;', '<xr>')
        data.gsub!('&lt;/xr&gt;', '</xr>')
        data.gsub!('&lt;X&gt;', '<var>')
        data.gsub!('&lt;/X&gt;', '</var>')
        data.gsub!('&lt;xref&gt;', '<xr>')
        data.gsub!('&lt;/xref&gt;', '</xr>')
        data.gsub!('&lt;src&gt;', '<src>')
        data.gsub!('&lt;/src&gt;', '</src>')
        data.gsub!('<src><i>', '<src>')
        data.gsub!('</i></src>', '</src>')
        $stderr.puts data
        #get previous edits
        if new == 'true'
          edits = []
        else
          edits = @dict.get_edits(id)
        end
        $stderr.puts edits
        edits << {'author'=>user, 'time'=>Time.now.strftime('%Y-%m-%d %H:%M:%S')}

        doc = @dict.add_meta(data.to_s, edits)
        $stderr.puts doc
        if new == 'true'
          @dict.add(id, doc.to_s)
        else
          @dict.update(id, doc.to_s)
        end
        response.body = 'saved ' + id
      else
        response.body = 'not authorized to save entry'
      end
      response.body

    when 'unlock'
      if request.query['unlock'].to_s != ''
        @server.info.unlock_entry('fni', request.query['unlock'].to_s, user)
      end
      response.body = 'unlocked ' + request.query['unlock'].to_s

    when 'unlock_cluster'
      if request.query['unlock'].to_s != ''
        request.query['unlock'].to_s.split('|').each{|uid|
          @server.info.unlock_entry('fni', uid, user)
        }
      end
      response.body = 'unlocked ' + request.query['unlock'].to_s

    when 'lock'
      if request.query['lock'].to_s != ''
        @server.info.lock_entry('fni', request.query['lock'].to_s, user)
      end
      response.body = 'locked ' + request.query['lock'].to_s


    when 'query2'
      if request.query['unlock'].to_s != ''
        @server.info.unlock_entry('fni', request.query['unlock'], user)
      end
      start = Time.now.to_s
      $stderr.puts 'START '+start
      request.query['search'] = '' if request.query['search'].to_s == 'search...'
      request.query['hw'] = '' if request.query['hw'].to_s == 'surname...'
      if request.query['search'].to_s == '' and request.query['hw'].to_s == ''
        tmpl_params = {
          'basehref'=>@basehref,
          'path'=>dict_path,
          'is_'+dict_path=>1
        }
        response['Content-Type'] = 'text/html; charset=utf8'
        response.body = HTMLTemplate.new(tmpl_params).output(@dict.template_path + '/list.tmpl')
      else
        #col = UCollator.new(:en_GB)
        #entries = @dict.get_entries(request.query, true).sort{|a, b| col.cmp(a['hw'], b['hw'])}
        col = TwitterCldr::Collation::Collator.new(:en)
        entries = @dict.get_entries(request.query, true).sort{|a, b| col.compare(a['hw'], b['hw'])}
        allentr = []
        #prepare for template
        entries.each{|e|
          e['escapeid'] = URI.escape(e['id']).gsub("&apos;", '%27')
          e['escapeid2'] = URI.escape(e['id']).gsub("&apos;", '\%27')
          allentr << e['escapeid']
        }
        tmpl_params = {
          'basehref'=>@basehref,
          'entries'=>entries,
          'count' => entries.size,
          'path' => dict_path,
          'is_'+dict_path=>1
        }
        if request.query['search'].to_s != ''
          tmpl_params['form_search'] = request.query['search']
          tmpl_params['form_search_set'] = 1
          tmpl_params['form_whsearch_'+request.query['where_search'].to_s] =  1
          tmpl_params['form_whsearch'] = request.query['where_search'].to_s
        end
        if request.query['hw'].to_s != ''
          tmpl_params['form_hw'] = request.query['hw'] 
          tmpl_params['form_hw_set'] = 1
          tmpl_params['form_whhw_'+request.query['where_hw'].to_s] =  1
          tmpl_params['form_whhw'] = request.query['where_hw'].to_s
        end
        tmpl_params['form_pr_'+request.query['pr'].to_s] = 1 if request.query['pr'].to_s != ''
        tmpl_params['form_pr'] = request.query['pr'].to_s
        tmpl_params['form_status_'+request.query['status'].to_s] = 1 if request.query['status'].to_s != ''
        tmpl_params['form_status'] = request.query['status'].to_s
        tmpl_params['form_ri_set'] = 1 if request.query['ri'].to_s == 'true'
        tmpl_params['form_q_set'] = 1 if request.query['q'].to_s == 'true'
        tmpl_params['form_r_set'] = 1 if request.query['r'].to_s == 'true'
        tmpl_params['allentries'] = allentr.join('!')
        response['Content-Type'] = 'text/html; charset=utf8'
        res = HTMLTemplate.new(tmpl_params).output(@dict.template_path + '/listent2.tmpl')
        response.body = res
      end
        $stderr.puts 'END   '+start + ' > '+Time.now.to_s

    when 'query'
      if request.query['unlock'].to_s != ''
        @server.info.unlock_entry('fni', request.query['unlock'], user)
      end
      start = Time.now.to_s
      $stderr.puts 'START '+start
      request.query['search'] = '' if request.query['search'].to_s == 'search...'
      request.query['hw'] = '' if request.query['hw'].to_s == 'surname...'
      if request.query['search'].to_s == '' and request.query['hw'].to_s == '' and request.query['r'].to_s == ''
        tmpl_params = {
          'basehref'=>@basehref,
          'path'=>dict_path,
          'is_'+dict_path=>1
        }
        response['Content-Type'] = 'text/html; charset=utf8'
        response.body = HTMLTemplate.new(tmpl_params).output(@dict.template_path + '/list.tmpl')
      else
        #col = UCollator.new(:en_GB)
        col = TwitterCldr::Collation::Collator.new(:en)
        #entries = @dict.get_entries(request.query, true).sort{|a, b| col.cmp(a['hw'], b['hw'])}
        entries = @dict.get_entries(request.query, true).sort{|a, b| col.compare(a['hw'], b['hw'])}
        allentr = []
        #prepare for template
        entries.each{|e|
          e['escapeid'] = URI.escape(e['id']).gsub("&apos;", '%27')
          e['escapeid2'] = URI.escape(e['id']).gsub("&apos;", '\%27')
          allentr << e['escapeid']
        }
        tmpl_params = {
          'basehref'=>@basehref,
          'entries'=>entries,
          'count' => entries.size,
          'path' => dict_path,
          'is_'+dict_path=>1,
          'query_path'=>[]
        }
        perm = @dict.get_perm(user, 'fni')
        if perm == 'r'
          tmpl_params['readonly'] = '1' 
          tmpl_params['entries'].each{|en| en['readonly'] = '1'}
        end
        request.query.each{|k,v|
          tmpl_params['query_path'] << {'key'=>k,'value'=>v}
        }
        if request.query['search'].to_s != ''
          tmpl_params['form_search'] = request.query['search']
          tmpl_params['search'] = request.query['search']
          tmpl_params['form_search_set'] = 1
          tmpl_params['form_whsearch_'+request.query['where_search'].to_s] =  1
          tmpl_params['form_whsearch'] = request.query['where_search'].to_s
        end
        if request.query['hw'].to_s != ''
          tmpl_params['form_hw'] = request.query['hw'] 
          tmpl_params['cat'] = request.query['hw'] 
          tmpl_params['form_hw_set'] = 1
          tmpl_params['form_whhw_'+request.query['where_hw'].to_s] =  1
          tmpl_params['form_whhw'] = request.query['where_hw'].to_s
        end
        tmpl_params['form_pr_'+request.query['pr'].to_s] = 1 if request.query['pr'].to_s != ''
        tmpl_params['form_pr'] = request.query['pr'].to_s
        tmpl_params['form_status_'+request.query['status'].to_s] = 1 if request.query['status'].to_s != ''
        tmpl_params['form_status'] = request.query['status'].to_s
        tmpl_params['form_ri_set'] = 1 if request.query['ri'].to_s == 'true'
        tmpl_params['form_q_set'] = 1 if request.query['q'].to_s == 'true'
        tmpl_params['form_r_set'] = 1 if request.query['r'].to_s == 'true'
        tmpl_params['allentries'] = allentr.join('!')
        response['Content-Type'] = 'text/html; charset=utf8'
        res = HTMLTemplate.new(tmpl_params).output(@dict.template_path + '/listent.tmpl')
        response.body = res
      end
        $stderr.puts 'END   '+start + ' > '+Time.now.to_s


    
    when 'sources'
      src_b, src_o = @dict.load_sources(true)
      books = []
      other = []
      src_b.each{|h|
        h['short'].gsub!('<', '&lt;')
        h['short'].gsub!('>', '&gt;')
        books << {'short'=>h['short'],'full'=>h['full']}
      }
      books.uniq!
      src_o.each{|h|
        h['full'].gsub!('<', '&lt;')
        h['full'].gsub!('>', '&gt;')
        other << {'full'=>h['full']}
      }
      other.uniq!
      response['Content-Type'] = 'text/html; charset=utf8'
      tmpl_params = {'books'=>books, 'other'=>other, 'basehref'=>@basehref}
      res = HTMLTemplate.new(tmpl_params).output(@dict.template_path + '/sources.tmpl')
      response.body = res

    when 'list_sources'
      perm = @dict.get_perm(user, 'fni')
      #if perm != 'w' and perm != 's'
      #  response['Content-Type'] = 'text/plain; charset=utf8'
      #  response.body = 'not authorized to edit'
      #  return response.body
      #end
      src_b, src_o = @dict.load_sources
      books = []
      other = []
      i = 0
      src_b.each{|h|
        h['short'].gsub!('<', '&lt;')
        h['short'].gsub!('>', '&gt;')
        h['short'].gsub!('"', '&quot;')
        h['short'].gsub!("'", '&apos;')
        h['full'].gsub!('<', '&lt;')
        h['full'].gsub!('>', '&gt;')
        h['full'].gsub!('"', '&quot;')
        h['full'].gsub!("'", '&apos;')
        h['full'].strip!
        h['abbr'].gsub!('<', '&lt;')
        h['abbr'].gsub!('>', '&gt;')
        h['abbr'].gsub!('"', '&quot;')
        h['abbr'].gsub!("'", '&apos;')
        h['abbr'] = '[NOT IN]' if h['abbr'] == ''
        i += 1
        sh = {'id'=>h['id'], 'short'=>h['short'],'full'=>h['full'], 'abbr'=>h['abbr'], 'count'=>i, 'type'=>h['type']}
        sh['authorized'] = 1 if perm == 'w' or perm == 's'
        books << sh
      }
      books.uniq!
      i = 0
      src_o.each{|h|
        h['full'].gsub!('<', '&lt;')
        h['full'].gsub!('>', '&gt;')
        h['full'].gsub!('"', '&quot;')
        h['full'].strip!
        h['abbr'].gsub!('<', '&lt;')
        h['abbr'].gsub!('>', '&gt;')
        h['abbr'].gsub!('"', '&quot;')
        i += 1
        other << {'full'=>h['full'], 'abbr'=>h['abbr'], 'count'=>i}
      }
      other.uniq!
      response['Content-Type'] = 'text/html; charset=utf8'
      tmpl_params = {'books'=>books, 'other'=>other, 'basehref'=>@basehref}
      tmpl_params['new_error'] = 1 if request.query['new_error'].to_s == '1'
      tmpl_params['new_error_full'] = 1 if request.query['new_error'].to_s == '2'
      tmpl_params['new_error_short'] = 1 if request.query['new_error'].to_s == '3'
      tmpl_params['new_saved'] = 1 if request.query['new_saved'].to_s == '1'
      tmpl_params['authorized'] = 1 if perm == 'w' or perm == 's'
      res = HTMLTemplate.new(tmpl_params).output(@dict.template_path + '/sourceslist.tmpl')
      response.body = res

    when 'oup_sources'
      src_b, src_o = @dict.load_sources
      books = []
      tmplbooks = []
      subbooks = {}
      src_b.each{|h|
        h['short'].gsub!('<', '&lt;')
        h['short'].gsub!('>', '&gt;')
        h['short'].gsub!('"', '&quot;')
        h['short'].gsub!("'", '&apos;')
        #h['full'].gsub!('<', '&lt;')
        #h['full'].gsub!('>', '&gt;')
        h['full'].gsub!('"', '&quot;')
        h['full'].gsub!("'", '&apos;')
        h['full'].strip!
        #s = Iconv.iconv("US-ASCII//TRANSLIT",'UTF-8', h['short']).join.gsub(' ', '')
        if request.query['sort'].to_s == '1'
          s = h['short'].asciify_utf8.gsub(' ', '').capitalize
          #$stderr.puts h['short']
          #$stderr.puts s
          #$stderr.puts Iconv.iconv("US-ASCII//TRANSLIT",'UTF-8', h['short']).join.gsub(' ', '')
        else 
          s = h['short']
        end
        if h['type'] != 'sub'
          books << {'short'=>h['short'],'full'=>h['full'],  'type'=>h['type'], 's'=>s}
        else
          ref = h['short'].slice(0..(h['short'].index(',')-1))
          subbooks[ref] = [] if subbooks[ref].nil?
          subbooks[ref] << {'short'=>h['short'],'full'=>h['full'],  'type'=>h['type'], 's'=>s}
        end
      }

      $stderr.puts 'before sort'
      books.each{|h|
        unless subbooks[h['short']].nil?
          h['sub'] = subbooks[h['short']].sort{|a, b| a['s']<=>b['s']}
        end
        tmplbooks << h
      }
      $stderr.puts 'after sort'

      tmplbooks = tmplbooks.sort{|a, b| a['s']<=>b['s']}
      $stderr.puts 'after sort2'

      response['Content-Type'] = 'text/html; charset=utf8'
      tmpl_params = {'books'=>tmplbooks, 'basehref'=>@basehref}
      res = HTMLTemplate.new(tmpl_params).output(@dict.template_path + '/sourcesoup.tmpl')
      response.body = res

    when 'check_hw'
      response['Content-Type'] = 'text/html; charset=utf8'
      id = URI.escape(request.query['check'].to_s)
      entry_id = URI.escape(request.query['entry_id'].to_s)
      $stderr.puts id
      $stderr.puts entry_id
      if id != ''
        xml = @dict.get(id)
        response.body = '-'
        if xml == ''
          if entry_id != ''
            @dict.change_id(entry_id, id)
          end
          response.body = 'ok'
        end
        return response.body
      end
      response.body = '-'

    when 'check_qua'
      response['Content-Type'] = 'text/html; charset=utf8'
      id = URI.escape(request.query['id'].to_s)
      quar = ''
      if id != ''
        quar = @dict.var_info(id)
      end
      response.body = quar

    when 'edit_source', 'del_source'
      orig = request.query['orig'].to_s.split('|')
      id = request.query['id'].to_i.to_s
      perm = @dict.get_perm(user)
      if perm != 'w' and perm != 's'
        response['Content-Type'] = 'text/plain; charset=utf8'
        response.body = 'not authorized to edit'
        return response.body
      end
      src_b, src_o = @dict.load_sources
      books = []
      other = []
      i = 0
      src_b.each{|h|
        if h['id'] == id or orig.include?(h['short'])
          h['short'].gsub!('<', '&lt;')
          h['short'].gsub!('>', '&gt;')
          h['short'].gsub!('"', '&quot;')
          h['short'].gsub!("'", '&apos;')
          h['full'].gsub!('<', '&lt;')
          h['full'].gsub!('>', '&gt;')
          h['full'].gsub!('"', '&quot;')
          h['full'].gsub!("'", '&apos;')
          h['full'].strip!
          h['abbr'].gsub!('<', '&lt;')
          h['abbr'].gsub!('>', '&gt;')
          h['abbr'].gsub!('"', '&quot;')
          h['abbr'].gsub!("'", '&apos;')
          i += 1
          books << {'id'=>h['id'], 'short'=>h['short'],'full'=>h['full'], 'abbr'=>h['abbr'], 'type'=>h['type'], 'count'=>i}
        end
      }
      books.uniq!
      response['Content-Type'] = 'text/html; charset=utf8'
      tmpl_params = {'books'=>books, 'basehref'=>@basehref}
      tmpl_params['redir'] = request.query['redir'].to_s
      template = 'sourcesedit.tmpl'
      template = 'sourcesdel.tmpl' if request.query['action'] == 'del_source'
      $stderr.puts template
      res = HTMLTemplate.new(tmpl_params).output(@dict.template_path + '/' + template)
      response.body = res

    when 'update_source'
      id = request.query['id'].to_i.to_s
      short = request.query['short'].to_s
      full = request.query['full'].to_s.gsub(/[\r\n]/, '')
      type = request.query['type'].to_s
      if id != '' and short != ''
        @dict.update_source(id, short, full, type)
        gen_src_js
      end
      #response.body = orig + '|' + short + '|' + full
      if request.query['redir'].to_s != '1'
        tmpl_params = {'basehref'=>@basehref,'id'=>id, 'short'=>short.gsub("'", "\\\\'"), 'full'=>full}
        response['Content-Type'] = 'text/html; charset=utf8'
        response.body = HTMLTemplate.new(tmpl_params).output(@dict.template_path + '/sourcesedit_suc.tmpl')
      else
        response.set_redirect(WEBrick::HTTPStatus::SeeOther, @servername + '/fni?action=list_sources')
      end

    when 'delete_source'
      id = request.query['id'].to_i.to_s
      short = request.query['short'].to_s
      entries = []
      if id != '' 
        entries = @dict.delete_source(id)
        gen_src_js
      end
      tmpl_params = {'basehref'=>@basehref,'id'=>id, 'short'=>short.gsub("'", "\\\\'"), 'entries'=>entries}
      response['Content-Type'] = 'text/html; charset=utf8'
      response.body = HTMLTemplate.new(tmpl_params).output(@dict.template_path + '/sourcesdel_suc.tmpl')

    when 'add_source'
      short = request.query['short'].to_s
      full = request.query['full'].to_s
      type = request.query['type'].to_s
      if full != '' and short != ''
        src_b, src_o = @dict.load_sources
        can_save = true
        src_b.each{|h|
          if h['full'].strip == full
            can_save = false
            response.set_redirect(WEBrick::HTTPStatus::SeeOther, @servername + '/fni?action=list_sources&new_error=2')
          end
          if h['short'].strip == short
            can_save = false
            response.set_redirect(WEBrick::HTTPStatus::SeeOther, @servername + '/fni?action=list_sources&new_error=3')
          end
        }
        @dict.add_source(short, full, type) if can_save
        gen_src_js
      else
        response.set_redirect(WEBrick::HTTPStatus::SeeOther, @servername + '/fni?action=list_sources&new_error=1')
      end
      if request.query['from_editor'].to_s == '1'
        response.body = 'ok'
      else
        response.set_redirect(WEBrick::HTTPStatus::SeeOther, @servername + '/fni?action=list_sources&new_saved=1')
      end

    when 'replace'
      tmpl_params = {'basehref'=>@basehref}
      tmpl_params['not_all'] = 1 if request.query['not_all'].to_s == '1'
      if request.query['done'].to_s == '1'
        tmpl_params['count_e'] = request.query['count_e'].to_s
        tmpl_params['count_r'] = request.query['count_r'].to_s
        tmpl_params['done'] = 1
      end
      response['Content-Type'] = 'text/html; charset=utf8'
      response.body = HTMLTemplate.new(tmpl_params).output(@dict.template_path + '/replace.tmpl')

    when 'do_replace'
      find = request.query['find'].to_s
      replace = request.query['replace'].to_s
      where = request.query['where'].to_s
      if find != '' #and replace != ''
        count_e1, count_r1 = @dict_array['surnames'].replace(find, replace, where)
        count_e2, count_r2 = @dict_array['reserve'].replace(find, replace, where)
        count_e3, count_r3 = @dict_array['res1881'].replace(find, replace, where)
        count_e4, count_r4 = @dict_array['ren'].replace(find, replace, where)
        count_e = count_e1 + count_e2 + count_e3 + count_e4
        count_r = count_r1 + count_r2 + count_r3 + count_r4
      else
        response.set_redirect(WEBrick::HTTPStatus::SeeOther, @servername + '/surnames?action=replace&not_all=1')
      end
      response.set_redirect(WEBrick::HTTPStatus::SeeOther, @servername + '/surnames?action=replace&done=1&count_e='+count_e.to_s+'&count_r='+count_r.to_s)


    when 'move_entry'
      fromdb = request.query['from'].to_s
      todb = request.query['to'].to_s
      entry_id = URI.escape(request.query['id'].to_s)
      force = false
      force = true if request.query['force'].to_s == 'true'
      if fromdb != '' and todb != '' and entry_id != ''
        $stderr.puts 'MOVE ENTRY '+entry_id+' from '+fromdb+' to '+todb
        result = @dict.move_entry(@dict_array[fromdb], @dict_array[todb], entry_id, force)
        response['Content-Type'] = 'text/html; charset=utf8'
        response.body = result
      else
        response['Content-Type'] = 'text/html; charset=utf8'
        response.body = 'Error: arguments not set'
      end

    when 'src_js_list'
      gen_src_js

      
    end

  end

  
  alias do_POST do_GET
end
