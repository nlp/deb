require 'webrick'
require 'cobravsmongoose'

class JSWordnetServlet < WEBrick::HTTPServlet::AbstractServlet
  def initialize( server, wn, wn_array )
    @wn = wn
    @wn_array = wn_array
    @servername = nil
    @server = server
    @packages = {}
    packages = server.info.list_packages()
    packages.each{|code, hash|
      @packages[hash['dict']] = [] if @packages[hash['dict']] == nil
      @packages[hash['dict']] << code
    }
  end
  
  def get_servername(server, request)
    if server[:SSLEnable]
      servername = 'https://'
    else
      servername = 'http://'
    end
    servername += request['Host'].to_s
  end

  def lock_info(dictionary, id)
    locked_by = @server.info.who_locked(dictionary, id)
    if locked_by == false
      lock_info = {'locked'=>'false','username'=>''}
    else
      lock_info = {'locked'=>'true','username'=>locked_by}
    end
    return lock_info
  end

  def format_synset_list(synsets, show_def=false, show_elr=false, user=nil, dictionary='')
    syn_ar = []
    synsets.each{|id, info|
	lock_info = lock_info(dictionary, id)
      label = ''
      label += (info['lockstatus'] == user)? '[*] ':'[x] '  if info['lockstatus'] != false
      literals = []
      info['literals'].each {|h|
        literals << h['literal'] + ':' + h['sense']
      }
      label += '[' + info['pos'] + '] ' + literals.join(', ')
      label = '...' if id == '...'
      hash = {'value' => id, 'label'=> label, 'g_syn'=>info['g_syn'], 'style'=>'BLACK', 'form'=>info['literals'].first['literal'], 'seq_nr'=>info['literals'].first['sense']}
      hash['definition'] = info['def'] if show_def
      hash['RIGIDITY'] = info['RIGIDITY'] if show_def
      hash['elr'] = info['elr'] if show_elr
	    hash['LOCK'] = lock_info
      syn_ar << hash
    }
    return syn_ar
  end

  def add_jsonp(data, callback)
    return callback + "(" + data + ");"
  end

  def save_entry(id, data_hash, request, dictionary, version=nil)
    #add version tag
    data_hash['SYNSET']['VERSION'] = {}
    data_hash['SYNSET']['VERSION']['$'] = version.to_s
    #new entry
    if id == '_new_id_' or id == ''
      id = @server.info.get_next_id(dictionary)
      data_hash['SYNSET']['ID'] = {}
      data_hash['SYNSET']['ID']['$'] = id
      data_hash['SYNSET']['CREATED'] = {}
      data_hash['SYNSET']['CREATED']['$'] = request.attributes[:auth_user].to_s + ' ' + Time.now.strftime("%Y-%m-%d %H:%M:%S")
    end
    #split literals to words
    words = []
    if data_hash['SYNSET']['SYNONYM']['LITERAL'].is_a?(Array)
      data_hash['SYNSET']['SYNONYM']['LITERAL'].each{|lit|
        lit['$'].split(' ').each {|w|
          words << {'$'=>w}
        }
      }
    end
    data_hash['SYNSET']['SYNONYM']['WORD'] = words
    #update stamp
    data_hash['SYNSET']['STAMP'] = {}
    data_hash['SYNSET']['STAMP']['$'] = request.attributes[:auth_user].to_s + ' ' + Time.now.strftime("%Y-%m-%d %H:%M:%S")
    $stderr.puts data_hash
    xml = CobraVsMongoose.hash_to_xml(data_hash)
    $stderr.puts xml
    @wn.update(id, xml.to_s, true, version)

    #update rel. types
    if data_hash['SYNSET']['ILR'].is_a?(Array)
      data_hash['SYNSET']['ILR'].each{|ilr|
        @wn.rel_types << ilr['@type'] if not @wn.rel_types.include?(ilr['@type'])
      }
      @wn.rel_types.uniq!
      @wn.rel_types.sort!
    end
    return id
  end

  def do_GET(request, response)
    @servername = get_servername(@server, request) if @servername == nil
    dictionary = request.path[1..-1]
    user = request.attributes[:auth_user].to_s
  
    case request.query['action']
    #wordnets list
    when 'getQueryList'
      response["Content-type"] = "text/plain; charset=utf-8"
      dicts = @wn['info'].list_dicts
      dicts.each {|id, name|
        response.body += id + ':' + name + "\n"
      }
      if request.query['callback'].to_s != ''
        response["Content-type"] = "text/plain"
        response.body = add_jsonp(response.body, request.query['callback'].to_s)
      end
      response.body

		when 'logout'
			response["Content-type"] = "text/plain; charset=utf-8"
      name = request.query['name']
			if (name != '')
				response.body += {'ok' => 'ok:' + name}
			end
      if request.query['callback'].to_s != ''
        response["Content-type"] = "text/plain"
        response.body = add_jsonp(response.body, request.query['callback'].to_s)
      end
			response.body

    #get one synset
    when 'runQuery'
      type = request.query['outtype'] #output type
      query = request.query['query']
			arg = request.query['arg'] #tree type
			default = request.query['default'].to_s
			expand = request.query['expand_rels'].to_s
			display_only = request.query['displayonly'].to_s
      if request.query['details'].to_s == ''
        details = 0
      else
        details = request.query['details'].to_i
      end
      if request.query['reldics'].to_s == ''
        reldics = []
      else
        reldics = request.query['reldics'].to_s.split(',')
      end

      if query == '' || type == ''
        $stderr.puts "Outtype or query not set"
        response.body += "Outtype or query not set".to_json
      else
				res = runQuery(@wn, query, type, arg, dictionary, default, expand, details, display_only, reldics)
				$stderr.puts res.to_json
        if request.query['nojson'] == '1'
          response.body += res
          return response.body
        end
        if request.query['eval'] == '1'
          response.body += '<html><head><script>test_object = '+res.to_json+';</script></head><body></body></html>'
          return response.body
        end
        response["Content-type"] = "application/json; charset=utf-8"
  			response.body += res.to_json
      end
      if request.query['callback'].to_s != ''
        response["Content-type"] = "text/plain"
        response.body = add_jsonp(response.body, request.query['callback'].to_s)
      end
    
    #show locked entries
    when 'showlocks'
      response["Content-type"] = "text/plain; charset=utf-8"
      locks = @server.info.get_locks(dictionary)
      if request.query['format'] == 'kyoto'
        out_a = []
        locks.each{|hash|
          out_a << {'username'=>hash['login'], 'time'=>hash['time'], 'id'=>hash['entry']}
        }
        response["Content-type"] = "application/json; charset=utf-8"
        response.body = out_a.to_json
      else
        locks.each{|hash|
          response.body += hash['login']+';'+hash['entry']+';'+hash['time']+"\n"
        }
      end
      if request.query['callback'].to_s != ''
        response["Content-type"] = "text/plain"
        response.body = add_jsonp(response.body, request.query['callback'].to_s)
      end

    when 'lockuser'
      response["Content-type"] = "text/plain; charset=utf-8"
      id = request.query['id'].to_s
      if request.query['format'] == 'kyoto'
        begin
          res = @wn.get(id).to_s
          locked_by = @server.info.who_locked(dictionary, id)
          if locked_by == false
            status = {'action'=>'check', 'lock'=>'false', 'id'=>id, 'username'=>''}
          else
            status = {'action'=>'check', 'lock'=>'true', 'id'=>id, 'username'=>locked_by}
          end
        rescue => e
          $stderr.puts e.to_s
          $stderr.puts e.backtrace.join("\n")
          status = {'username'=>'', 'id'=>id, 'action'=>'error', 'message'=>e.to_s, 'lock'=>'false'}
        end
      else
        lockuser= @server.info.who_locked(dictionary, id)
        if lockuser != false
          status = lockuser
        else
          status = ''
        end
      end
      response.body = status.to_json
      if request.query['callback'].to_s != ''
        response["Content-type"] = "text/plain"
        response.body = add_jsonp(response.body, request.query['callback'].to_s)
      end

    when 'previewMerge'
      address1 = '/doc'
      address2 = '/doc'
      if request.query['doc1'].to_s != ''
        address1 = '/'+dictionary+'?action=runQuery&amp;outtype=html&amp;nojson=1&amp;default=1&amp;query=' + request.query['doc1']
      end
      if request.query['doc2'].to_s != ''
        address2 = '/'+dictionary+'?action=runQuery&amp;outtype=html&amp;nojson=1&amp;default=1&amp;query=' + request.query['doc2']
      end
      response["Content-type"] = "text/html; charset=utf-8"
      response.body = %Q[<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <title>DEBVisDic Merge Preview</title>
  <style type="text/css">
    html,body {
      height: 99%;
    }
    td,iframe,table {
      height: 100%;
    }
    iframe,table {
      width: 100%;
    }
    td {
      width: 50%;
    }
    table {
      margin: 0 auto;
      padding: 0;
    }
  </style>
</head>
<body>
  <table>
    <tr>
      <td>
        <iframe src="#{address1}"></iframe>
      </td>
      <td>
        <iframe src="#{address2}"></iframe>
      </td>
    </tr>
  </table>
</body>
</html>       
]      
      
      
    #vizualizace
    when 'jsonvis'
      query = request.query['query']
      response["Content-type"] = "application/json; charset=utf-8"

      if query == ''
        response.body = 'No query'
      else
        meta = []
        @wn_array.each{|code,data|
          meta << {'code'=>code, 'name'=>data.title} if code != @wn.dictcode and code != 'sumo'
        }
        resxml=@wn.get_heslo(query)
        if resxml != ''
          res = @wn.get_rilr(resxml)
          path = @wn.get_tree(res, 'ILR', 2)
          $stderr.puts path
      		res = @wn.follow_ilr(res)
          res = @wn.add_meta_info(res, meta, path)
            $stderr.puts res
          res = @wn.apply_transform( 'jsonvis', res, [ [ 'slovnik', '"'+dictionary+'"'] ] )
          response.body = '['+res.gsub('<?xml version="1.0" encoding="UTF-8"?>','')+']'
        else
          synsets = @wn.get_list(query, @wn_array)
          synres = []
          synsets.each {|id,info|
            res = @wn.get_rilr(@wn.get_heslo(id))
            path = @wn.get_tree(res, 'ILR', 2)
        		res = @wn.follow_ilr(res)
            res = @wn.add_meta_info(res, meta, path)
            res = @wn.apply_transform( 'jsonvis', res, [ [ 'slovnik', '"'+dictionary+'"'] ] )
            synres << res.gsub('<?xml version="1.0" encoding="UTF-8"?>','')
          }
          response.body = '[' + synres.join(',') + ']'
        end
      end
    #VisualBrowser
    when 'commonQuery'
      query = request.query['query']
      response["Content-type"] = "text/plain; charset=utf-8"

      if query == ''
        response.body = 'No query'
      else
        resxml=@wn.get_heslo(query)
        if resxml != ''
          res = @wn.get_rilr( resxml )
          res = @wn.apply_transform( 'vb', res, [ [ 'slovnik', '"'+dictionary+'"'] ] )
          response.body = res.gsub('&lt;','<').gsub('&gt;','>')
        else
          synsets = @wn.get_list(query, @wn_array)
          synsets.each {|id,info|
            response.body += "<urn:nlp:wn#{dictionary}/#{id}> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <urn:nlp:wn#{dictionary}/Synset> .\n"
            info['literals'].each {|h|
              response.body += "<urn:nlp:wn#{dictionary}/#{id}> <urn:nlp:wn#{dictionary}/LITERAL> <urn:nlp:wn#{dictionary}/#{h['literal']}> .\n"
              response.body += "<urn:nlp:wn#{dictionary}/#{h['literal']}> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <urn:nlp:wn#{dictionary}/Literal> .\n"
            }
          }
        end
      end
    #linked data
    when 'linked'
      query = request.query['query']
      response["Content-type"] = "text/plain; charset=utf-8"

      if query == ''
        response.body = 'No query'
      else
        resxml=@wn.get_heslo(query)
        if resxml != ''
          res = @wn.get_rilr( resxml )
          res = @wn.apply_transform( 'ld', res, [ [ 'slovnik', '"'+dictionary+'"'] ] )
          response.body = res.gsub('&lt;','<').gsub('&gt;','>')
        else
          synsets = @wn.get_list(query, @wn_array)
          synsets.each {|id,info|
            response.body += "<urn:nlp:wn#{dictionary}/#{id}> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <urn:nlp:wn#{dictionary}/Synset> .\n"
            info['literals'].each {|h|
              response.body += "<urn:nlp:wn#{dictionary}/#{id}> <urn:nlp:wn#{dictionary}/LITERAL> <urn:nlp:wn#{dictionary}/#{h['literal']}> .\n"
              response.body += "<urn:nlp:wn#{dictionary}/#{h['literal']}> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <urn:nlp:wn#{dictionary}/Literal> .\n"
            }
          }
        end
      end

    #get preview for list of synsets
    when 'listPreview'
      query = request.query['query'].strip

      #if query == ''
      #  response.body = 'No query'
      #else
        synsets = @wn.get_list(query, @wn_array)
        response["Content-type"] = "text/html; charset=utf-8"
        response.body = %Q[<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>#{@wn.title}</title>
<style type="text/css">
  body {
  	font-family: Verdana, Arial, sans-serif;
  	background-color: #ffffff;
  }
  
  a {
  	color: #0000ff;
  	text-decoration: underline;
  }
  .BLUE {
  	color: #0000ff;
  }
  .RED {
  color: #ff0000;
  }
  .GREEN {
  	color: #009900;
  }
  .RED2 {
  	color: #990000;
  }
  .GREEN2 {
  	color: #00ff00;
  }
    .showin-ul {
      margin: 0.1em;
      padding-left: 0.2em;
      list-style-type: none;
    }
    .showin-select {
      float:right;
      border:solid 1px;
      background-color: #fff0de;
    }
    .path {
      background-color: #fff3ae;
    }
      </style>
</head>
<body>
<h1>#{@wn.title}</h1>
<form method="get" action="/#{dictionary}">
New search:
<input type="text" name="query"/>
<input type="hidden" name="action" value="listPreview"/>
<input type="submit" value="search"/>
<a href="/" target="_top">Grid main page</a>
</form>
]
        meta = []
        @wn_array.each{|code,data|
          meta << {'code'=>code, 'name'=>data.title} if code != @wn.dictcode and code != 'sumo'
        }

        #either one entry ID is given
        resxml=@wn.get_heslo(query)
        xsltparams = [
          [ 'dictionary', '"'+dictionary+'"']
        ]
        xsltparams << [ 'frame', '"true"' ] if request.query['frame'].to_s == 'true'
        xsltparams << [ 'gwgdicts', '"'+request.query['gwgdicts'].to_s+'"' ] if request.query['gwgdicts'].to_s != ''
        if resxml != ''
          doc = REXML::Document.new(resxml)
          syninf = @wn.get_literals(query)

          res = @wn.get_rilr(resxml)
          path = @wn.get_tree(res)
          res = @wn.follow_ilr(res)
          res = @wn.add_meta_info(res, meta, path)
          res = @wn.apply_transform( 'single', res, xsltparams )
          response.body += "<h2>Query \"#{syninf}\"</h2>"
          response.body += res
        else
        #or a query
          if synsets.size == 0 and query.to_s == ''
            #no query, get topmost, only for Grid
            if @server.info.service_name == 'gwgrid'
              synsets = @wn.topmost_entries
            query = synsets.size.to_s + ' topmost entries'
            topmost = true
            end
          end
          response.body += "<h2>Query \"#{query}\"</h2>"
          synsets.each {|id,info|
            resxml=@wn.get_heslo(id)
            if resxml != ''
              res = @wn.get_rilr(resxml)
              path = @wn.get_tree(res) if not topmost
              res = @wn.follow_ilr(res)
              res = @wn.add_meta_info(res, meta, path)
              res = @wn.apply_transform( 'single', res, xsltparams )
              response.body += res
            end
          }
        end
        response.body += "</body></html>"
      #end
   
    
    #list of synsets
    when 'queryList'
      query = URI.unescape(request.query['word']).to_s
        response["Content-type"] = "application/json; charset=utf-8"
      
      if query == ''
        $stderr.puts "Query not set"
        response.body += {'action'=>'error', 'message'=>"Query not set"}.to_json
      else
        if request.query['add_def'].to_s == 'true'
          show_def = true
        else
          show_def = false
        end
        maxres = nil
        maxres = request.query['maxres'].to_i if request.query['maxres'].to_i > 0
        showmany = false
        showmany = true if request.query['show_toomany'].to_s == 'true'
        casesens = nil
        casesens = true if request.query['casesensitivity'].to_s == 'true'

        synsets = @wn.get_list(query, @wn_array, show_def, maxres, showmany, request.query['attribute'], casesens)
        if synsets.is_a?(Hash)
          syn_ar = format_synset_list(synsets, show_def, false, user, dictionary)
          response.body += syn_ar.sort!{|x,y| if y['label'] == '...' then -1 elsif x['label'] == '...' then  1 else  x['label']<=>y['label'] end}.to_json
        else
          response.body = {'action'=>'error', 'message'=>"bad result list"}.to_json
        end
      end
      if request.query['callback'].to_s != ''
        response["Content-type"] = "text/plain"
        response.body = add_jsonp(response.body, request.query['callback'].to_s)
      end

    #"translate" synsets to other wordnet
    #run query, get synsets ELR links and find synsets in target WN
    when 'translate'
      query = URI.unescape(request.query['query']).to_s
      target = URI.unescape(request.query['target']).to_s
      response['Content-Type'] = 'application/json; charset=utf-8'
      if query == '' or target == ''
        $stderr.puts "Query or target wordnet not set"
        response.body = {'action'=>'error', 'message'=>"Query not set"}.to_json
      else
        synsets = @wn.translate_synsets(@wn_array, query, target)
        outorig = format_synset_list(synsets['original'], false, true, user, dictionary)
        outtran = format_synset_list(synsets['translated'], false, true, user, dictionary)
        outsyn = {'translated'=>outtran, 'original'=>outorig}
        response.body = outsyn.to_json
      end
                                                                                                          

    #list of synsets for debdict
    when 'list_starts_with'
      query = URI.unescape(request.query['id']).to_s
      response["Content-type"] = "text/plain; charset=utf-8"
      
      if query == ''
        $stderr.puts "Query not set"
        response.body += {'action'=>'error', 'message'=>"Query not set"}.to_json
      else
        response.body += @wn.list_starts_with(query, 'wncz')
      end
      if request.query['callback'].to_s != ''
        response["Content-type"] = "text/plain"
        response.body = add_jsonp(response.body, request.query['callback'].to_s)
      end

    #get for debdict
    when 'getdoc'
      meta = []
      @wn_array.each{|code,data|
        meta << {'code'=>code, 'name'=>data.title} if code != @wn.dictcode and code != 'sumo'
      }
      id = URI.unescape(request.query['id']).to_s
      response["Content-type"] = "text/html; charset=utf-8"
  	  resxml = @wn.get_heslo(id)
      if resxml != ''
    		res = @wn.get_rilr(resxml)
        path = @wn.get_tree(res)
    		res = @wn.follow_ilr(res)
        res = @wn.add_meta_info(res, meta, path)
        res = @wn.apply_transform(dictionary, res, [ [ 'slovnik', '"'+dictionary.to_s+'"'], [ 'servername', '_' ], ['package', '"'+@packages[dictionary.to_s].to_s+'"'] ] )
        response.body = res
      else
        response.body = 'not found'
      end

    when 'listAllID'
        synsets = @wn.get_list('.*', @wn_array, false)
        if synsets.is_a?(Hash)
          syn_ar = []
          synsets.each {|id,info|
            syn_ar << id
          }
          response.body += syn_ar.sort!.to_json
        else
          response['Content-Type'] = 'text/plain'
          response.body = {'action'=>'error', 'message'=>"bad result list"}.to_json
        end
      if request.query['callback'].to_s != ''
        response["Content-type"] = "text/plain"
        response.body = add_jsonp(response.body, request.query['callback'].to_s)
      end


		when 'copyEntryTo'
			#zavolam funkciu na serveri a poviem mu ze chcem skopirovat aktualne entryId
			#zo slovniku dictionaryIdFrom do slovniku dictionaryIdTo, 
			#dalsim parametrom je aktualna zalozka v slovniku kam sa ma kopirovat (rovnake parametre ako pri runQuery t.j. arg a outtype)
			#to co mi server vrati zobrazim v slovniku dictionaryIdTo alebo chybu ak tam uz take
			#existuje. Ak existuje(server vrati '') opytam sa uzivatela ci
			#to chce v slovniku id prepisat a ak ano tak toto zavolam znova s
			#parametrom force = true
			dictionaryIdFrom = request.query['dictionaryIdFrom']
			dictionaryIdTo = request.query['dictionaryIdTo']
			entryId = request.query['entryId']
			force = request.query['force']
			arg = request.query['arg']
			outtype = request.query['outtype']

			if dictionaryIdFrom == '' || dictionaryIdTo == '' || entryId == '' || force == '' || outtype == '' || (outtype == 'tree' && arg == '')
        $stderr.puts "Query not set"
        response["Content-type"] = "text/plain; charset=utf-8"
        response.body = "Query not set".to_json
        return response.body
			elsif dictionaryIdTo == dictionaryIdFrom
        $stderr.puts "The same dictionaries"
        response["Content-type"] = "text/plain; charset=utf-8"
        response.body = "The same dictionaries".to_json
      if request.query['callback'].to_s != ''
        response["Content-type"] = "text/plain"
        response.body = add_jsonp(response.body, request.query['callback'].to_s)
      end
        return response.body
			else
        #exists in idFrom
        resxml = @wn.get_heslo(entryId)
        if resxml == ''
          response["Content-type"] = "text/plain; charset=utf-8"
          response.body = "Not in source".to_json
          return response.body
        end
        if force != 'true'
          res2 = @wn_array[dictionaryIdTo].get_heslo(entryId)
          if res2 != ''
            response["Content-type"] = "text/plain; charset=utf-8"
            response.body = "Exists in target".to_json
      if request.query['callback'].to_s != ''
        response["Content-type"] = "text/plain"
        response.body = add_jsonp(response.body, request.query['callback'].to_s)
      end
            return response.body
          end
        end
				#zatial sa vracia odpoved z povodneho slovniku, pre fungivanie odkomentovat a zmazat duplicity
        @wn_array[dictionaryIdTo].update(entryId, resxml)
        response["Content-type"] = "text/xml; charset=utf-8"
				resnew = runQuery(@wn_array[dictionaryIdTo], entryId, outtype, arg, dictionary)
        response.body = resnew.to_json
      if request.query['callback'].to_s != ''
        response["Content-type"] = "text/plain"
        response.body = add_jsonp(response.body, request.query['callback'].to_s)
      end
        return response.body
			end
    when 'takeIdFrom'
			dictionaryIdFrom = request.query['dictionaryIdFrom']
			dictionaryIdTo = request.query['dictionaryIdTo']
			entryIdFrom = request.query['entryIdFrom']
			entryIdTo = request.query['entryIdTo']
			force = request.query['force']
			arg = request.query['arg']
			outtype = request.query['outtype']
			if dictionaryIdFrom == '' || dictionaryIdTo == '' || entryIdFrom == '' || force == '' || outtype == '' || (outtype == 'tree' && arg == '')
        $stderr.puts "Query not set"
        response["Content-type"] = "text/plain; charset=utf-8"
        response.body = "Query not set".to_json
        return response.body
			elsif dictionaryIdTo == dictionaryIdFrom
        $stderr.puts "Identical dictionaries"
        response["Content-type"] = "text/plain; charset=utf-8"
        response.body = "Identical dictionaries".to_json
      if request.query['callback'].to_s != ''
        response["Content-type"] = "text/plain"
        response.body = add_jsonp(response.body, request.query['callback'].to_s)
      end
        return response.body
			else
        resxml = @wn_array[dictionaryIdFrom].get_heslo(entryIdFrom)
        if resxml == ''
					$stderr.puts "Not in source"
          response["Content-type"] = "text/plain; charset=utf-8"
          response.body = "Not in source".to_json
      if request.query['callback'].to_s != ''
        response["Content-type"] = "text/plain"
        response.body = add_jsonp(response.body, request.query['callback'].to_s)
      end
          return response.body
        end
        if entryIdTo == ''
          res2 = @wn_array[dictionaryIdTo].get_heslo(entryIdFrom)
          if res2 != ''
	  				$stderr.puts "Target exists"
            response["Content-type"] = "text/plain; charset=utf-8"
            response.body = "Target exists".to_json
      if request.query['callback'].to_s != ''
        response["Content-type"] = "text/plain"
        response.body = add_jsonp(response.body, request.query['callback'].to_s)
      end
            return response.body
          end
          @wn_array[dictionaryIdTo].update(entryIdFrom, resxml)
					$stderr.puts "Run takeIdFrom, entryIdTo == null"
          response["Content-type"] = "text/plain; charset=utf-8"
		  		resnew = runQuery(@wn_array[dictionaryIdTo], entryIdFrom, outtype, arg, dictionary)
          response.body = resnew.to_json
      if request.query['callback'].to_s != ''
        response["Content-type"] = "text/plain"
        response.body = add_jsonp(response.body, request.query['callback'].to_s)
      end
          return response.body    
        end

				$stderr.puts "Run takeIdFrom"
        res2 = @wn_array[dictionaryIdTo].get_heslo(entryIdTo)
        begin
          @wn_array[dictionaryIdTo].delete(entryIdTo)
        rescue
          $stderr.puts 'Entry '+entryIdTo+'not in destination dictionary'
        end
        if res2 != ''
          doc = REXML::Document.new(res2.to_s)
        else
          doc = REXML::Document.new(resxml.to_s)
        end
        #update ID
        doc.elements['SYNSET/ID'].text = entryIdFrom.to_s
        $stderr.puts "Runing takeIdFrom: xml new = " + doc.to_s
        @wn_array[dictionaryIdTo].update(entryIdFrom, doc.to_s)
        #update all links
        @wn_array[dictionaryIdTo].update_links_id(entryIdTo.to_s, entryIdFrom.to_s)

        response["Content-type"] = "text/plain; charset=utf-8"
		  	resnew = runQuery(@wn_array[dictionaryIdTo], entryIdFrom, outtype, arg, dictionary)
        response.body = resnew.to_json
      if request.query['callback'].to_s != ''
        response["Content-type"] = "text/plain"
        response.body = add_jsonp(response.body, request.query['callback'].to_s)
      end
        return response.body    
      end
      
    when 'subtree'
      response["Content-type"] = "text/plain; charset=utf-8"
      query = request.query['query']
      arg = request.query['arg']
      limit_type = request.query['rel_type'].to_s
      if request.query['details'].to_s == ''
        details = 0
      else
        details = request.query['details'].to_i
      end
      resxml = @wn.get_heslo(query)
      array = []
  		if arg == ''
    		arg = 'ILR'
  		end
      arg = (arg == 'ILR')? 'RILR':'ILR'
      
      if arg == 'RILR'
        resxml = @wn.get_rilr(resxml)
      end
      tree = @wn.get_subtree(resxml, arg, limit_type, details)
  		tree.each {|ar|
        lock_info = lock_info(dictionary, ar[1])
        case details
          when 0
      			array << {'parent' => ar[0], 'id' => ar[1], 'val' => ar[2], 'color' => ar[3], 'type' => ar[4], 'LOCK'=>lock_info}
          when 1
      			array << {'parent' => ar[0], 'id' => ar[1], 'val' => ar[2], 'color' => ar[3], 'type' => ar[4], 'g_syn' => ar[5], 'definition' => ar[6], 'LOCK'=>lock_info}
          when 2
      			array << {'parent' => ar[0], 'id' => ar[1], 'val' => ar[2], 'color' => ar[3], 'type' => ar[4], 'g_syn' => ar[5], 'definition' => ar[6], 'literal' => ar[7], 'LOCK'=>lock_info, 'RIGIDITY' => ar[8]}
        end
  		}
  		response.body = array.to_json
      if request.query['callback'].to_s != ''
        response["Content-type"] = "text/plain"
        response.body = add_jsonp(response.body, request.query['callback'].to_s)
      end
    when 'fullsubtree'
      response["Content-type"] = "text/plain; charset=utf-8"
      query = request.query['query']
      arg = request.query['arg']
      limit_type = request.query['rel_type'].to_s
      if request.query['details'].to_s == ''
        details = 0
      else
        details = request.query['details'].to_i
      end
      resxml = @wn.get_heslo(query)
      array = []
  		if arg == ''
    		arg = 'ILR'
  		end
      arg = (arg == 'ILR')? 'RILR':'ILR'
      
      if arg == 'RILR'
        resxml = @wn.get_rilr(resxml)
      end
      tree = @wn.get_full_subtree(resxml, arg, limit_type, details)
  		tree.each {|ar|
        lock_info = lock_info(dictionary, ar[1])
        case details
          when 0
      			array << {'parent' => ar[0], 'id' => ar[1], 'val' => ar[2], 'color' => ar[3], 'type' => ar[4], 'LOCK'=>lock_info}
          when 1
      			array << {'parent' => ar[0], 'id' => ar[1], 'val' => ar[2], 'color' => ar[3], 'type' => ar[4], 'g_syn' => ar[5], 'definition' => ar[6], 'LOCK'=>lock_info}
          when 2
      			array << {'parent' => ar[0], 'id' => ar[1], 'val' => ar[2], 'color' => ar[3], 'type' => ar[4], 'g_syn' => ar[5], 'definition' => ar[6], 'literal' => ar[7], 'LOCK'=>lock_info}
        end
  		}
  		response.body = array.to_json
      if request.query['callback'].to_s != ''
        response["Content-type"] = "text/plain"
        response.body = add_jsonp(response.body, request.query['callback'].to_s)
      end
    when 'delfullsubtree'
      response["Content-type"] = "text/plain; charset=utf-8"
      query = request.query['query']
      arg = request.query['arg']
      resxml = @wn.get_heslo(query)
      array = []
  		if arg == ''
    		arg = 'ILR'
  		end
      arg = (arg == 'ILR')? 'RILR':'ILR'
      
      if arg == 'RILR'
        resxml = @wn.get_rilr(resxml)
      end
      tree = @wn.get_full_subtree(resxml, arg)
  		tree.each {|ar|
        $stderr.puts ar[1]
        @wn.delete_synset(ar[1])
  		}
  		response.body = "<OK/>".to_json
      if request.query['callback'].to_s != ''
        response["Content-type"] = "text/plain"
        response.body = add_jsonp(response.body, request.query['callback'].to_s)
      end
    #delete node: zmaze uzol stromu a ak je uzol korenom podstromu,
    #tak presunie jeho potomkov na jeho uroven
    when 'delNode'
      response["Content-type"] = "text/plain; charset=utf-8"
      query = request.query['query']
      resxml = @wn.get_heslo(query)
      resxml = @wn.get_rilr(resxml)
      $stderr.puts resxml.to_s
      up = /<ILR type='hypernym'>([^<]*)<\/ILR>/.match(resxml)
      if up != nil
        upid = up[1]
      end
      $stderr.puts upid.to_s
      array = []
      childs = @wn.get_subtree(resxml, 'RILR')
  		  childs.each {|ar|
          cid = ar[1].to_s
          array << cid
          childr = @wn.get_heslo(cid)
          cdoc = REXML::Document.new(childr.to_s)
          if cdoc.elements['SYNSET/ILR'] != nil
            REXML::XPath.each(cdoc, "/SYNSET/ILR[@type='hypernym' and .='#{query}']") {|ilr|
              if upid != nil
                ilr.text = upid
              else
                cdoc.delete_element "/SYNSET/ILR[@type='hypernym' and .='#{query}']"
              end
            }
            $stderr.puts cdoc.to_s
            @wn.update(cid, cdoc.to_s)
          end
  		  }
      
      @wn.delete_synset(query)
  		#response.body = array.to_s
      response.body = '<OK/>'.to_json
      if request.query['callback'].to_s != ''
        response["Content-type"] = "text/plain"
        response.body = add_jsonp(response.body, request.query['callback'].to_s)
      end
      
    when 'getNotes'
      response["Content-type"] = "text/plain; charset=utf-8"
      dont_display = ['wnpol']
      if dont_display.include?(dictionary)
        notes = []
      else
        ar = @wn.get_notes
        notes = ['']
        ar.each_key{|k|
          notes << k.to_s
        }
      end
      response.body = notes.to_json
      if request.query['callback'].to_s != ''
        response["Content-type"] = "text/plain"
        response.body = add_jsonp(response.body, request.query['callback'].to_s)
      end
    when 'getTypes'
      response["Content-type"] = "text/plain; charset=utf-8"
      ar = @wn.rel_types
      response.body = ar.to_json
      if request.query['callback'].to_s != ''
        response["Content-type"] = "text/plain"
        response.body = add_jsonp(response.body, request.query['callback'].to_s)
      end
    when 'delete'
      id = request.query['id']
      $stderr.puts "DELETE SYNSET #{id}"
      check = @wn.check_lock(dictionary, id, Thread.current[:auth_user])
      if not(check == true)
        response['Content-Type'] = 'text/plain'
        response.body = check.to_json
      if request.query['callback'].to_s != ''
        response["Content-type"] = "text/plain"
        response.body = add_jsonp(response.body, request.query['callback'].to_s)
      end
        return
      end
      if not @server.info.check_perm(Thread.current[:auth_user], dictionary, 'w') and not @server.info.check_perm(Thread.current[:auth_user], dictionary, 'm')
        response["Content-type"] = "text/plain; charset=utf-8"
        response.body = {'action'=>'error', 'message'=>'not authorized'}.to_json
      if request.query['callback'].to_s != ''
        response["Content-type"] = "text/plain"
        response.body = add_jsonp(response.body, request.query['callback'].to_s)
      end
        return response.body
      end
      if @wn.test_gsyn(id)
        response["Content-type"] = "text/plain; charset=utf-8"
        response.body = {'action'=>'error', 'message'=>'cannot save: g_syn=true'}.to_json
      if request.query['callback'].to_s != ''
        response["Content-type"] = "text/plain"
        response.body = add_jsonp(response.body, request.query['callback'].to_s)
      end
        return response.body
      end

      begin
        @wn.delete_synset(id)
        response["Content-type"] = "text/plain; charset=utf-8"
        response.body = {'action'=>'deleted', 'reload'=>@packages[dictionary]}.to_json
      if request.query['callback'].to_s != ''
        response["Content-type"] = "text/plain"
        response.body = add_jsonp(response.body, request.query['callback'].to_s)
      end
      rescue => e
        response['Content-Type'] = 'text/plain'
        $stderr.puts e.message
        $stderr.puts e.backtrace.join("\n")
        response.body = {'action'=>'error', 'message'=>"exception #{e.class.to_s}: #{e.message}"}.to_json
      if request.query['callback'].to_s != ''
        response["Content-type"] = "text/plain"
        response.body = add_jsonp(response.body, request.query['callback'].to_s)
      end
      end

    when 'save'
      id = request.query['id'].to_s
      $stderr.puts "EDIT Synset ID " + id + ' START ' + time_stamp()
      check = @wn.check_lock(dictionary, id, Thread.current[:auth_user])
      if not(check == true)
        response['Content-Type'] = 'text/plain'
        response.body = check.to_json
      if request.query['callback'].to_s != ''
        response["Content-type"] = "text/plain"
        response.body = add_jsonp(response.body, request.query['callback'].to_s)
      end
        return
      end
      if not @server.info.check_perm(Thread.current[:auth_user], dictionary, 'w') and not @server.info.check_perm(Thread.current[:auth_user], dictionary, 'm')
        response["Content-type"] = "text/plain; charset=utf-8"
        response.body = {'action'=>'error', 'message'=>'not authorized'}.to_json
      if request.query['callback'].to_s != ''
        response["Content-type"] = "text/plain"
        response.body = add_jsonp(response.body, request.query['callback'].to_s)
      end
        return response.body
      end
      if @wn.test_gsyn(id)
        response["Content-type"] = "text/plain; charset=utf-8"
        response.body = {'action'=>'error', 'message'=>'cannot save: g_syn=true'}.to_json
      if request.query['callback'].to_s != ''
        response["Content-type"] = "text/plain"
        response.body = add_jsonp(response.body, request.query['callback'].to_s)
      end
        return response.body
      end

      if request.query['data'].to_s == ''
        response["Content-type"] = "text/plain; charset=utf-8"
        response.body = {'action'=>'error', 'message'=>'no data'}.to_json
      if request.query['callback'].to_s != ''
        response["Content-type"] = "text/plain"
        response.body = add_jsonp(response.body, request.query['callback'].to_s)
      end
        return response.body
      end
     
      begin
        data = URI.unescape(request.query['data']).to_s
        data_hash = JSON.parse data if request.query['saveonly'].to_s == ''
        $stderr.puts data
        version = @server.info.get_next_version(dictionary)
        if request.query['saveonly'].to_s != '' and id != '' and id != '_new_id_'
          saveonly = request.query['saveonly'].to_s
          $stderr.puts 'SAVE ONLY ' + request.query['saveonly'].to_s
          resxml = @wn.get_heslo(id)
          data_hash = CobraVsMongoose.xml_to_hash(resxml.to_s)
          case request.query['saveonly'].to_s
            when 'DEF','POS'
              data_hash['SYNSET'][saveonly] = {}
              data_hash['SYNSET'][saveonly]['$'] = data
            when 'ELR', 'ILR'
              data_hash['SYNSET'][saveonly] = [] if data_hash['SYNSET'][saveonly].nil?
              data_sent = JSON.parse data
              data_hash['SYNSET'][saveonly] << data_sent
            when 'LITERAL'
              data_hash['SYNSET']['SYNONYM']['LITERAL'] = [] if data_hash['SYNSET']['SYNONYM']['LITERAL'].nil?
              if data_hash['SYNSET']['SYNONYM']['LITERAL'].class == Hash
                lit = data_hash['SYNSET']['SYNONYM']['LITERAL']
                data_hash['SYNSET']['SYNONYM']['LITERAL'] = []
                data_hash['SYNSET']['SYNONYM']['LITERAL'] << lit
              end
              data_sent = JSON.parse data
              data_hash['SYNSET']['SYNONYM']['LITERAL'] << data_sent
            when 'RIGIDITY'
              data_hash['SYNSET'][saveonly] = {}
              data_sent = JSON.parse data
              data_hash['SYNSET'][saveonly]['@rigid'] = data_sent['@rigid'] 
              data_hash['SYNSET'][saveonly]['@rigidScore'] = data_sent['@rigidScore'] 
              data_hash['SYNSET'][saveonly]['@nonRigidScore'] = data_sent['@nonRigidScore'] 
          end
          $stderr.puts data_hash.to_s
        end
        id = save_entry(id, data_hash, request, dictionary, version)
        response['Content-Type'] = 'application/json'
        response.body = {'action'=>'saved', 'reload'=>@packages[dictionary], 'id'=>id.to_s, 'dictVersion'=>version.to_s}.to_json
        if request.query['callback'].to_s != ''
          response["Content-type"] = "text/plain"
          response.body = add_jsonp(response.body, request.query['callback'].to_s)
        end
      rescue => e
        response['Content-Type'] = 'text/plain'
        $stderr.puts e.message
        $stderr.puts e.backtrace.join("\n")
        response.body = {'action'=>'error', 'message'=>"Error, please contact deb@aurora.fi.muni.cz with the following message: exception #{e.class.to_s}: #{e.message}"}.to_json
        if request.query['callback'].to_s != ''
          response["Content-type"] = "text/plain"
          response.body = add_jsonp(response.body, request.query['callback'].to_s)
        end
      end

    when 'saveTree'
      $stderr.puts 'SAVETREE START ' + time_stamp()
      if not @server.info.check_perm(Thread.current[:auth_user], dictionary, 'w') and not @server.info.check_perm(Thread.current[:auth_user], dictionary, 'm')
        response["Content-type"] = "text/plain; charset=utf-8"
        response.body = {'action'=>'error', 'message'=>'not authorized'}.to_json
        if request.query['callback'].to_s != ''
          response["Content-type"] = "text/plain"
          response.body = add_jsonp(response.body, request.query['callback'].to_s)
        end
        return response.body
      end

      if request.query['synsetsArray'].to_s == ''
        response["Content-type"] = "text/plain; charset=utf-8"
        response.body = {'action'=>'error', 'message'=>'no data'}.to_json
        if request.query['callback'].to_s != ''
          response["Content-type"] = "text/plain"
          response.body = add_jsonp(response.body, request.query['callback'].to_s)
        end
        return response.body
      end
     
      begin
        data = URI.unescape(request.query['synsetsArray']).to_s
        syn_array = JSON.parse(data)
        $stderr.puts data
        version = @server.info.get_next_version(dictionary)
        response_array = []
        if not syn_array.is_a?(Array)
          response["Content-type"] = "text/plain; charset=utf-8"
          response.body = {'action'=>'error', 'message'=>'synsetsArray is not array'}.to_json
          if request.query['callback'].to_s != ''
            response["Content-type"] = "text/plain"
            response.body = add_jsonp(response.body, request.query['callback'].to_s)
          end
          return response.body
        end
        #gather internal IDs
        internal_ids = {} 
        syn_array.each{|synset_hash|
          unless synset_hash['SYNSET']['INTERNAL_ID'].nil?
            internal_ids[synset_hash['SYNSET']['INTERNAL_ID'].to_s] = ''
          end
        }
        #get real IDs
        internal_ids.each{|id, l|
          internal_ids[id] = @server.info.get_next_id(dictionary)
        }
        #replace IDs and save
        syn_array.each{|synset_hash|
          unless synset_hash['SYNSET']['INTERNAL_ID'].nil?
            synset_hash['SYNSET']['ID'] = {}
            synset_hash['SYNSET']['ID']['$'] = internal_ids[synset_hash['SYNSET']['INTERNAL_ID'].to_s]
            synset_hash['SYNSET'].delete('INTERNAL_ID')
          end
          unless synset_hash['SYNSET']['ILR'].nil?
            synset_hash['SYNSET']['ILR'].each{|ilr|
              ilrtarget = ilr['$'].to_s
              unless internal_ids[ilrtarget].nil?
                ilr['$'] = internal_ids[ilrtarget]
              end
            }
          end
          $stderr.puts synset_hash
          save_entry(synset_hash['SYNSET']['ID']['$'].to_s, synset_hash, request, dictionary, version)
          response_array << {'action'=>'saved', 'id'=>synset_hash['SYNSET']['ID']['$'].to_s, 'dictVersion'=>version.to_s, 'SYNONYM'=>synset_hash['SYNSET']['SYNONYM']}
        }
        response['Content-Type'] = 'application/json'
        response.body = response_array.to_json
        if request.query['callback'].to_s != ''
          response["Content-type"] = "text/plain"
          response.body = add_jsonp(response.body, request.query['callback'].to_s)
        end
      rescue => e
        response['Content-Type'] = 'application/json'
        $stderr.puts e.message
        $stderr.puts e.backtrace.join("\n")
        response.body = {'action'=>'error', 'message'=>"Error, please contact deb@aurora.fi.muni.cz with the following message: exception #{e.class.to_s}: #{e.message}"}.to_json
        if request.query['callback'].to_s != ''
          response["Content-type"] = "text/plain"
          response.body = add_jsonp(response.body, request.query['callback'].to_s)
        end
      end
      

    when 'currentversion'
      version = @server.info.get_cur_version(dictionary)
      response['Content-Type'] = 'text/plain'
      response.body = version.to_s
      if request.query['callback'].to_s != ''
        response["Content-type"] = "text/plain"
        response.body = add_jsonp(response.body, request.query['callback'].to_s)
      end
    
    when 'listModified'
      version = request.query['version'].to_s
      if version == ''
        response['Content-Type'] = 'text/plain'
        response.body = {'action'=>'error', 'message'=>'Specify version'}.to_json
      else
        ids = @wn.get_modified(version)
        response['Content-Type'] = 'text/plain'
        response.body = {'ids'=>ids}.to_json
      end
      if request.query['callback'].to_s != ''
        response["Content-type"] = "text/plain"
        response.body = add_jsonp(response.body, request.query['callback'].to_s)
      end


    when 'lock'
      id = request.query['id']
      user = Thread.current[:auth_user]
      if request.query['format'] == 'kyoto'
        begin
          res = @wn.get(id).to_s
          check = @wn.check_lock(dictionary, id, user)
          if check == true
            time = Time.now.to_i
            @wn.lock(dictionary, id, user, time) #lock actual synset
            status = {'action' => 'locked','user' => user, 'id'=>id}
          else
            check['message'].gsub!('locked by ','')
            check['action'] = 'taken'
            status = check
          end
        rescue => e
          $stderr.puts e.to_s
          status = {'user'=>user, 'action'=>'error', 'message'=>e.to_s, 'id'=>id}
        end
      else
        begin
          res = @wn.get(id).to_s
          check = @wn.check_lock(dictionary, id, user)
          if check == true
            time = Time.now.to_i
            @wn.lock(dictionary, id, user, time) #lock actual synset
            status = {'action' => 'locked','user' => user}
          else
            status = check
          end
        rescue => e
          status = {'error' => e.to_s}
        end
      end
      response["Content-type"] = "text/plain; charset=utf-8"
      response.body = status.to_json
      if request.query['callback'].to_s != ''
        response["Content-type"] = "text/plain"
        response.body = add_jsonp(response.body, request.query['callback'].to_s)
      end
      
    when 'unlock'
      id = request.query['id']
      user = Thread.current[:auth_user]
      if request.query['format'] == 'kyoto'
        begin
          res = @wn.get(id).to_s
          #check = @wn.check_lock(dictionary, id, user)
          locked_by = @server.info.who_locked(dictionary, id)
          if locked_by == false
            status = {'action'=>'unlocked', 'lock_released'=>'false', 'id'=>id, 'username'=>''}
          elsif locked_by == user
            status = {'action'=>'unlocked', 'lock_released'=>'true', 'id'=>id, 'username'=>user}
            @wn.unlock_time(dictionary, id, Thread.current[:auth_user])
          else
            status = {'action'=>'taken', 'lock_released'=>'false', 'id'=>id, 'username'=>locked_by}
          end
        rescue => e
          $stderr.puts e.to_s
          $stderr.puts e.backtrace.join("\n")
          status = {'user'=>user, 'id'=>id, 'action'=>'error', 'message'=>e.to_s, 'lock_released'=>'false'}
        end
      else
        @wn.unlock_time(dictionary, id, Thread.current[:auth_user])
        status = {'action' => 'unlocked'}
      end
      response["Content-type"] = "text/plain; charset=utf-8"
      response.body = status.to_json
      if request.query['callback'].to_s != ''
        response["Content-type"] = "text/plain"
        response.body = add_jsonp(response.body, request.query['callback'].to_s)
      end

    when 'nextSense'
      literal = request.query['literal'].to_s
      if literal == ''
        response["Content-type"] = "text/plain; charset=utf-8"
        response.body = 'specify literal'
      else
        sense = @wn.get_next_sense(literal)
        response["Content-type"] = "text/plain; charset=utf-8"
        response.body = {'literal' => literal, 'recommended_sense' => sense}.to_json
      end
      if request.query['callback'].to_s != ''
        response["Content-type"] = "text/plain"
        response.body = add_jsonp(response.body, request.query['callback'].to_s)
      end

    when 'dump'
      if request.query['visdic'] == 'true'
        response['Content-Type'] = 'text/plain'
        xmldump = @wn.dump('WN', dictionary)
        response.body = @wn.apply_transform( 'visdic', xmldump )
      elsif request.query['lmf'] == 'true'
        response['Content-Type'] = 'text/plain'
        xmldump = @wn.dump('WN', dictionary)
        response.body = @wn.apply_transform( 'lmf', xmldump )
      else
        response['Content-Type'] = 'text/xml'
        response.body = @wn.dump('WN', dictionary)
      end
    when 'firstrun'
      @wn.first_run(@wn, @wn_array)
      response['Content-Type'] = 'text/plain'
      response.body = 'ok'
    when 'info'
      case request.query['type']
      when 'relations'
        type = '__all'
        type = request.query['relation'] if request.query['relation'].to_s != ''
        maxres = 10
        maxres = request.query['maxres'].to_i if request.query['maxres'].to_s != ''
        order = 'desc'
        order = 'asc' if request.query['ascending'] == 'true'
        relinfo = @wn.load_info_relations(type, order, maxres)
        response["Content-type"] = "application/json"
        response.body = relinfo.to_json
        response.body = {'action'=>'error', 'error'=>'info file not exists'}.to_json if relinfo[0] == 'error'
        if request.query['callback'].to_s != ''
          response["Content-type"] = "text/plain"
          response.body = add_jsonp(response.body, request.query['callback'].to_s)
        end
      when 'lastmodification'
        maxres = 10
        maxres = request.query['maxres'].to_i if request.query['maxres'].to_s != ''
        modinfo = @wn.load_info_modifications(request.query['user'].to_s, request.query['synset'].to_s, maxres)
        response["Content-type"] = "application/json"
        response.body = modinfo.to_json
        response.body = {'action'=>'error', 'error'=>'info file not exists'}.to_json if modinfo[0] == 'error'
        if request.query['callback'].to_s != ''
          response["Content-type"] = "text/plain"
          response.body = add_jsonp(response.body, request.query['callback'].to_s)
        end
      when 'edit'
        month = Time.mktime(Time.now.year, Time.now.month, 1).strftime('%Y-%m')
        month = request.query['month'] if request.query['month'].to_s != ''
        editinfo = @wn.load_info_edit(month, request.query['user'].to_s, request.query['synset'].to_s)
        response["Content-type"] = "application/json"
        response.body = editinfo.to_json
        response.body = {'action'=>'error', 'error'=>'info file not exists'}.to_json if editinfo[0] == 'error'
        if request.query['callback'].to_s != ''
          response["Content-type"] = "text/plain"
          response.body = add_jsonp(response.body, request.query['callback'].to_s)
        end
      when 'journal'
        maxres = 10
        maxres = request.query['maxres'].to_i if request.query['maxres'].to_s != ''
        editinfo = @wn.load_info_journal(request.query['user'].to_s, request.query['synset'].to_s, request.query['datefrom'].to_s, request.query['dateto'].to_s, maxres)
        response["Content-type"] = "application/json"
        response.body = editinfo.to_json
        response.body = {'action'=>'error', 'error'=>'info file not exists'}.to_json if editinfo[0] == 'error'
        if request.query['callback'].to_s != ''
          response["Content-type"] = "text/plain"
          response.body = add_jsonp(response.body, request.query['callback'].to_s)
        end
      end
    end
  end
  
  def runQuery(dictionary, query, type, arg, dictcode, default='', expand='', tree_details = 0, display_only='', reldics = [])
  	resxml = dictionary.get_heslo(query).gsub('<?xml version="1.0" encoding="UTF-8"?>','')
    if resxml == ''
      return {'action'=>'error', 'error'=>'synset does not exist'}
    end
  	$stderr.puts resxml
  	case type
  	when 'xml'
  		res = dictionary.get_rilr(resxml)
  		res = dictionary.follow_ilr(res)
      res = dictionary.apply_transform('xml', res, [ [ 'slovnik', '"'+dictcode.to_s+'"'], [ 'servername', '"'+@servername.to_s+'"' ] ] )
  		return res
  	when 'html'
  		res = dictionary.get_rilr(resxml)
  		res = dictionary.follow_ilr(res)
      res = dictionary.apply_transform('preview', res, [ [ 'dict', '"'+dictcode.to_s+'"'], [ 'servername', '"'+@servername.to_s+'"' ], ['default', '"'+default.to_s+'"'], ['package', '"'+@packages[dictcode.to_s].to_s+'"'] ] )
  		return res
    when 'plain'
  		res = dictionary.get_rilr(resxml)
  		res = dictionary.follow_ilr(res)
      return res
  	when 'lmf'
  		res = dictionary.get_rilr(resxml)
$stderr.puts res
  		res = dictionary.follow_ilr(res)
$stderr.puts res
      begin
        info = @server.info.get('lmfinfo'+dictcode).to_s
      rescue
        info = ''
      end
      res = '<WN>'+info+res+'</WN>'
      res = dictionary.apply_transform('lmf', res)
  		return res
  	when 'tree'
  		array = []
  		if arg == ''
    		arg = 'ILR'
  		end
  		res = dictionary.get_rilr(resxml)
  		tree = dictionary.get_tree(res, arg, tree_details)
  		tree.each {|ar|
        lock_info = lock_info(dictcode, ar[1])
        case tree_details
          when 0
      			array << {'parent' => ar[0], 'id' => ar[1], 'val' => ar[2], 'color' => ar[3], 'type' => ar[4], 'LOCK'=>lock_info}
          when 1
      			array << {'parent' => ar[0], 'id' => ar[1], 'val' => ar[2], 'color' => ar[3], 'type' => ar[4], 'g_syn' => ar[5], 'definition' => ar[6], 'LOCK'=>lock_info}
          when 2
      			array << {'parent' => ar[0], 'id' => ar[1], 'val' => ar[2], 'color' => ar[3], 'type' => ar[4], 'g_syn' => ar[5], 'definition' => ar[6], 'literal' => ar[7], 'LOCK'=>lock_info}
        end
  		}
      array.reverse!
  		return array
  	when 'editor'
      return {} if query == '' or query == '_new_id_'
      resxml = dictionary.expand_rels_editor(resxml.to_s) if expand == '1'
      hash = CobraVsMongoose.xml_to_hash(resxml.to_s)
      if display_only != ''
        if display_only == 'kyotoowl'
          id, kyoto, jumps = dictionary.get_kyotoowl(query)
          hash = {'SYNSET'=>{'ID'=>{'$'=>id},'kyotoowl'=>{'$'=>kyoto}},'jumps'=>jumps}
        else
          hash = {'SYNSET'=>{'ID'=>hash['SYNSET']['ID'], display_only=>hash['SYNSET'][display_only]}}
        end
      end
      lock_info = lock_info(dictcode, hash['SYNSET']['ID']['$'])
      hash['LOCK'] = lock_info
      begin
        if hash['SYNSET']['USAGE'].is_a?(Hash)
          temp = hash['SYNSET']['USAGE']
          hash['SYNSET']['USAGE'] = [temp]
        end
      rescue
      end
      begin
        if hash['SYNSET']['ILR'].is_a?(Hash)
          temp = hash['SYNSET']['ILR']
          hash['SYNSET']['ILR'] = [temp]
        end
      rescue
      end
      begin
        if hash['SYNSET']['ILR'].is_a?(Hash)
          temp = hash['SYNSET']['ILR']
          hash['SYNSET']['ILR'] = [temp]
        end
      rescue
      end
      begin
        if hash['SYNSET']['VALENCY'].is_a?(Hash)
          temp = hash['SYNSET']['VALENCY']
          hash['SYNSET']['VALENCY'] = [temp]
        end
      rescue
      end
      begin
        hash['SYNSET']['VALENCY'].each{|v|
          if v['FRAME'].is_a?(Hash)
            temp = v['FRAME']
            v['FRAME'] = [temp]
          end
        }
      rescue
      end
      begin
        if hash['SYNSET']['SYNONYM']['LITERAL'].is_a?(Hash)
          temp = hash['SYNSET']['SYNONYM']['LITERAL']
          hash['SYNSET']['SYNONYM']['LITERAL'] = [temp]
        end
      rescue
      end
      #hash['SYNSET']['G_SYN'] = {} if hash['SYNSET']['G_SYN'].nil?
      #hash['SYNSET']['G_SYN']['$'] = 'false' if hash['SYNSET']['G_SYN']['$'].to_s.downcase != 'true'
      return hash
    when 'rels'
      synsets = []
      reldics.each{|targetdict|
        unless @wn_array[targetdict].nil?
          @wn_array[targetdict].find_relr(query).each{|xml|
            hash = CobraVsMongoose.xml_to_hash(xml.to_s)
            hash['SYNSET']['WN_NAME'] = targetdict
          }
        end
      }
      return hash
  	end
  end

  alias do_POST do_GET
end

