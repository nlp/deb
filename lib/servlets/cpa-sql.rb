require 'open-uri'
require 'json'
require 'cobravsmongoose'

class CPASQLServlet < WEBrick::HTTPServlet::AbstractServlet
  def initialize (server, slov, admin)
    @slov = slov
    @admin = admin
    @server = server
    @server.info.lock_timeout = 60*60
  end
 
  def do_GET(request, response)
    $stderr.puts "do_GET > #{request.query['action']}"
    user = request.attributes[:auth_user].to_s
    case request.query['action']
    # list of entries in RDF
    when 'rdf'
      query = request.query['query']
      group = true if request.query['group'] == 'true'
      transform = request.query['tr']
      filter = request.query['filter']
      if query == nil
        query = ''
      end
      perm = @slov.get_perm(@admin, user)
      if transform == 'csv'
        response["Content-Type"] = "text/plain"
        response.body = @slov.to_csv(query, perm, filter, group)
        return response.body
      end
      if transform
        response["Content-Type"] = "text/html; charset=utf-8"
        transform = (group == 'true')? 'rdfgroup' : 'rdf'
        response.body = @slov.apply_transform(transform,
            @slov.to_rdf(query, group, perm, filter))
      else
        response["Content-Type"] = "text/rdf"
        response.body = @slov.to_rdf(query, group, perm, filter)
      end

    when 'turtleverblist'
        verb= ''
        verb = request.query['verb']
        response["Content-Type"] = "text/turtle"
        response.body = @slov.turtle_verblist(verb)

    # list of verbs with basic info
    when 'jsonlist'
      query = request.query['query'] or ''
      transform = request.query['tr']
      filter = request.query['filter']
      language = request.query['language'] or 'en'
      perm = @slov.get_perm(@admin, user)
      response["Content-Type"] = "application/json"
      csva = @slov.to_csv(query, perm, filter, false).split("\n")
      resjson = []
      csva.shift
      csva.each{|l|
        next if l.strip == ''
        ca = l.split(';')
        if language == 'it'
          ce = {
            'entry_id'=>ca[0],
            'created'=>ca[1],
            'modified'=>ca[2],
            'itwac_freq'=>ca[3],
            'edit_by'=>ca[6],
            'create_by'=>ca[7],
            'patterns'=>ca[8],
            'status'=>ca[9]
          }
        else
          ce = {
            'entry_id'=>ca[0],
            'created'=>ca[1],
            'modified'=>ca[2],
            'bnc_freq'=>ca[3],
            'bnc50_freq'=>ca[4],
            'oec_freq'=>ca[5],
            'edit_by'=>ca[6],
            'create_by'=>ca[7],
            'patterns'=>ca[8],
            'status'=>ca[9],
            'sample'=>ca[11],
            'fn_link'=>ca[12]
          }
        end
        resjson << ce
      }
      response.body = resjson.to_json
      
    # list of entries in RDF
    when 'ontordf'
      transform = request.query['tr']
      search = request.query['search'].to_s
      if transform
        response["Content-Type"] = "text/html; charset=utf-8"
        response.body = @slov.apply_transform(transform, @slov.ontology_tree)
      else
        response["Content-Type"] = "text/rdf"
        response.body = @slov.ontology_tree(search)
      end

    # ontology tree, JSON
    when 'json_onto'
      response["Content-Type"] = "application/json"
      xmldecl = '<?xml version="1.0" encoding="UTF-8"?>'
      response.body = @slov.apply_transform('ontojson',
          @slov.ontology_tree(search)).sub(xmldecl,'')

    when 'onto_report'
        response["Content-Type"] = "text/plain; charset=utf-8"
        result = ''
        @slov.semtyp_hyper().to_a.sort!{|a, b| a[1] <=> b[1]}.each{|item|
            result += "||" + item.drop(1).join("||") + "||\n"
        }
        response.body = result

    when 'get_matching_roles'
      response["Content-Type"] = "text/plain; charset=utf-8"
      q = request.query['q']
      response.body = @slov.match_query('semantic_role', q).map{|i|
          i.join("\t")}.join("\n")

    when 'get_matching_adverbs'
      response["Content-Type"] = "text/plain; charset=utf-8"
      q = request.query['q']
      response.body = @slov.match_query('advl_adverbs', q).map{|i|
          i.join("\t")}.join("\n")

    when 'get_matching_particles'
      response["Content-Type"] = "text/plain; charset=utf-8"
      q = request.query['q']
      response.body = @slov.match_query('headword', q).map{|i|
          i.join("\t")}.join("\n")

    when 'get_matching_lexsets'
      response["Content-Type"] = "text/plain; charset=utf-8"
      q = request.query['q']
      response.body = @slov.match_query('lexset_items', q).map{|i|
          i.join("\t")}.join("\n")

    when 'get_matching_semtypes'
      response["Content-Type"] = "text/plain; charset=utf-8"
      q = request.query['q']
      response.body = @slov.match_query('semantic_type', q).map{|i|
          i.join("\t")}.join("\n")

    when 'get_matching_primary_implicatures'
      response["Content-Type"] = "text/plain; charset=utf-8"
      q = request.query['q']
      response.body = @slov.match_primimpl(q).map{|i| i.join("\t")}.join("\n")

    when 'get_matching_secondary_implicatures'
      response["Content-Type"] = "text/plain; charset=utf-8"
      q = request.query['q']
      response.body = @slov.match_secimpl(q).map{|i| i.join("\t")}.join("\n")

    when 'get_matching_domain'
      response["Content-Type"] = "text/plain; charset=utf-8"
      q = request.query['q']
      response.body = @slov.match_domain(q).map{|i|
          i.join("\t")}.join("\n")

    when 'get_matching_register'
      response["Content-Type"] = "text/plain; charset=utf-8"
      q = request.query['q']
      response.body = @slov.match_register(q).map{|i|
          i.join("\t")}.join("\n")

    when 'get_matching_comment'
      response["Content-Type"] = "text/plain; charset=utf-8"
      q = request.query['q']
      response.body = @slov.match_comment(q).map{|i|
          i.join("\t")}.join("\n")

    # list of differences in RDF
    when 'diffrdf'
      transform = request.query['tr']
      if transform
        response["Content-Type"] = "text/html; charset=utf-8"
        response.body = @slov.apply_transform(transform, @slov.difference_tree)
      else
        response["Content-Type"] = "text/rdf"
        response.body = @slov.difference_tree
      end
      
    #list of patterns for entry in RDF
    when 'rdf_pat'
      entry_id = request.query['id']
      if entry_id != nil
        if request.query['tr'].to_s == 'prev'
          response["Content-Type"] = "text/html"
          rdf = @slov.rdf_patterns(entry_id)
          percent = load_percent(entry_id)
          if percent != nil
            rdfdoc = REXML::Document.new(rdf.to_s)
            rdfdoc.root.elements.to_a('//RDF:Description').each{|el|
              pel = el.add_element 'd:perc'
              num = el.elements['d:num'].text.to_i
              if percent[num] != nil
                pel.text = percent[num]
              else
                pel.text = '<1%'
              end
            }
            rdf = rdfdoc.to_s
          end
          response.body = @slov.apply_transform('verbprev',
              rdf, [ ['entry_id','"'+entry_id+'"'] ])
        else
          response["Content-Type"] = "text/rdf"
          response.body = @slov.rdf_patterns(entry_id)        
        end
      end

    when 'json_patterns'
      entry_id = request.query['id']
      if entry_id != nil
        response["Content-Type"] = "application/json"
        rdf = @slov.rdf_patterns(entry_id, false)
        xmldecl = '<?xml version="1.0" encoding="UTF-8"?>'
        response.body = @slov.apply_transform('verbjson',
            rdf, [['entry_id','"'+entry_id+'"']]).sub(xmldecl, '')
      end

    # simple list of patterns in plain text
    when 'bare_patterns'
      entry_id = request.query['id']
      if entry_id != nil
        response["Content-Type"] = "text/plain"
        response.body = @slov.get_bare_patterns(entry_id)
      end

    # simple list of verbs with statuses
    when 'bare_verbs'
      response["Content-Type"] = "text/plain"
      response.body = @slov.get_bare_verbs()
      
    # get entry XML
    when 'getdoc'
      key = request.query['id']
      transform = request.query['tr']
      lang  = request.query['lang']
      type  = request.query['type']
      begin
        a = @slov.get(key)
        if (type == 'html') 
          response["Content-Type"] = "text/html; charset=utf-8"
        else
          response["Content-Type"] = "text/xml; charset=utf-8"
        end
        if transform
          res = @slov.apply_transform(transform, a.to_s,
              [['id', '"'+key+'"' ]])
          response.body += res
        else
          response.body += a
        end
      rescue => e
        $stderr.puts e
        response["Content-Type"] = "text/html; charset=utf-8"
        response.body += %Q[<?xml version="1.0" encoding="utf-8"?>\n]
        response.body += "<html><body><br><br>"
        response.body += "<font color='red'>"
        if lang=='cs'
          response.body += "Slovo '<b>#{key}</b>' nenalezeno"
        else
          response.body += "Headword '<b>#{key}</b>' not found"
        end
        response.body += "</font>"
        response.body += "</body></html>"
      end

    #get single pattern XML
    when 'getpat'
      key = request.query['id']      
      num = request.query['n']
      if key.to_s != '' and num.to_i > 0
        string = @slov.get(key)
        doc = REXML::Document.new(string.to_s)
        pat = REXML::XPath.first(doc, "/pattern_set/pattern[@id='#{num}']")
        response["Content-Type"] = "text/xml; charset=utf-8"
        response.body = pat.to_s
      else
        response["Content-Type"] = "text/html; charset=utf-8"
        response.body += %Q[<?xml version="1.0" encoding="utf-8"?>\n]
        response.body += "<html><body><br><br>"
        response.body += "<font color='red'>"
        response.body += "Headword '<b>#{key}</b>' not found"
        response.body += "</font>"
        response.body += "</body></html>"
      end

    # get single pattern in json
    when 'json_getpat'
        key = request.query['id']
        num = request.query['n']
        if key.to_s != '' and num.to_i > 0 and @slov.id2num(key, num) > 0
            patn = @slov.id2num(key, num)
            response["Content-Type"] = "application/json; charset=utf-8"
            response.body = @slov.json_pattern(key, patn).to_json
        else
            response["Content-Type"] = "text/html; charset=utf-8"
            response.status = 500
            response.body = "#{key} not found"
        end

    # pattern in json, direct way
    when 'pattern'
        verb = request.query['verb']
        number = request.query['number'].to_s
        pattern_id = request.query['pattern_id'].to_s
        response["Content-Type"] = "application/json; charset=utf-8"
        if number == '' and pattern_id == ''
            response.body = {'error' => 'Missing pattern identifier!'}.to_json
            response.status = 500
        end
        if number == ''
            number = @slov.id2num(verb, pattern_id)
        end
        response.body = @slov.json_pattern(verb, number).to_json

    #get onto entry
    when 'getonto'
      tid = request.query['tid']
      response.body = @slov.getonto(tid)

    #get hypernyms
    when 'gethyper'
      term = request.query['term'].to_s
      response.body = @slov.get_hypernyms(term).join(', ')

    when 'get_frames'
      entry_id = request.query['entry_id'].to_s
      response["Content-Type"] = "text/html; charset=utf-8"
      response.body = @slov.get_frames(entry_id).join("\n")

    # dump all XML
    when 'dump'
      response["Content-Type"] = "text/xml; charset=utf-8"
      response.body = @slov.dump_all()

    # get entry statistics
    when 'stat'
      query = request.query['query'].to_s
      filter = request.query['filter'].to_s
      response["Content-Type"] = "text/plain; charset=utf-8"
      count = @slov.get_entry_stat(query, filter)
      response.body = count['total_entry'].to_s + ';' +
          count['total_pattern'].to_s + ';' +
          count['comp_entry'].to_s + ';' +
          count['comp_pattern'].to_s + ';' +
          count['draft_entry'].to_s + ';' +
          count['draft_pattern'].to_s + ';' +
          count['filter_entry'].to_s

    # get types statistics
    when 'groupstat'
      response["Content-Type"] = "text/plain; charset=utf-8"
      count = @slov.get_types_stat
      response.body = count['in'].to_s + ';' + count['notin'].to_s

    #store single pattern
    when 'storepat'
      key = request.query['id']
      patid = request.query['n']
      data = request.query['data']
      perm = @slov.get_perm(@admin, user, key)
      if @slov.is_trainee(@admin, user)
          response.status = 500
          response.body = 'Trainee can not save patterns'
          return response.body
      end
      if perm == 'w' or perm == 's'
        @slov.store_pattern(key, patid, data, user)
      end
      response["Content-Type"] = "text/plain; charset=utf-8"
      response.body = key

    # store single pattern sent in JSON
    when 'json_storepat'
      key = request.query['id']
      patid = request.query['n']
      data = URI.unescape(request.query['data']).to_s
      data_hash = JSON.parse data 
      xml = CobraVsMongoose.hash_to_xml(data_hash)
      perm = @slov.get_perm(@admin, user, key)
      if @slov.is_trainee(@admin, user)
          response.status = 500
          response.body = 'Trainee can not save patterns'
          return response.body
      end
      if perm == 'w' or perm == 's'
        @slov.store_pattern(key, patid, xml, user)
      end
      response["Content-Type"] = "text/plain; charset=utf-8"
      response.body = key
      response.body = xml

    #store ontology
    when 'storeonto'
      id = request.query['id']
      data = request.query['data']
      perm = @slov.get_perm(@admin, user)
      if @slov.is_trainee(@admin, user)
          response.status = 500
          response.body = 'Trainee can not change ontology'
          return response.body
      end
      if perm == 's'
        @slov.store_onto(id, data)
      end
      response["Content-Type"] = "text/plain; charset=utf-8"
      response.body = id

    when 'semclass_verbs'
      semclass = request.query['semclass']
      response["Content-Type"] = "text/plain; charset=utf-8"
      response.body = @slov.semclass_used_verbs(semclass, true, [])

    #renumber patterns for entry
    when 'renumber'
      response["Content-Type"] = "text/plain; charset=utf-8"
      if @slov.is_trainee(@admin, user)
          response.status = 500
          response.body = 'Trainee can not renumber patterns'
          return response.body
      end
      key = request.query['id']
      data = request.query['data']
      hash = {}
      data.split('-').each { |d|
        a = d.split(':')
        hash[a[0]] = a[1]
      }
      perm = @slov.get_perm(@admin, user, key)
      if perm == 'w' or perm == 's'
        renum_res = @slov.renumber(key, hash)
        if renum_res == 'error'
            response.status = 500
            response.body = 'Error: not renumbered'
            return response.body
        else
            response.body = renum_res
            return response.body
        end
      end
      response.body = key
      return response.body
      
    #add new empty entry
    when 'new'
      response["Content-Type"] = "text/plain; charset=utf-8"
      name = request.query['name'].to_s.strip
      perm = @slov.get_perm(@admin, user, '')
      if @slov.is_trainee(@admin, user)
        response.status = 500
        response.body = 'Trainee can not add new patterns'
        return response.body
      end
      if perm == 'w' or perm == 's'
        result = @slov.new_pattern(name, 1, user, true)
      end
      if result == 'error'
        response.status = 500
        response.body = "Error when adding new entry/pattern: #{name}"
      else
        response.body = name
      end

    #add new empty pattern
    when 'newpat'
      name = request.query['name'].to_s
      perm = @slov.get_perm(@admin, user, name)
      if @slov.is_trainee(@admin, user)
          response.status = 500
          response.body = 'Trainee can not add new patterns'
          return response.body
      end
      if perm == 'w' or perm == 's'
        newid = @slov.new_pattern(name, 0, user)
      end
      if newid == 'error'
        response.status = 500
      else
        response["Content-Type"] = "text/plain; charset=utf-8"
        response.body = newid.to_s
      end

    #copy pattern
    when 'copy'
        fromentry = request.query['entry'].to_s
        toentry = request.query['newentry'].to_s
        pat = request.query['pat'].to_s
        perm = @slov.get_perm(@admin, user, toentry)
        if @slov.is_trainee(@admin, user)
            response.status = 500
            response.body = 'Trainee can not copy patterns'
            return response.body
        end
        if perm == 'w' or perm == 's'
            newid = @slov.copy_pattern(fromentry, pat, toentry, user)
        end
        if newid == 'error'
            response.status = 500
        else
            response["Content-Type"] = "text/plain; charset=utf-8"
            response.body = newid.to_s
        end

    #either add new patter or new entry - for web access from corpora
    when 'create_pattern'
        name = request.query['name'].to_s
        num = request.query['num']
        if @slov.is_trainee(@admin, user)
            response.status = 500
            response.body = 'Trainee can not create patterns'
            return response.body
        end
        newid = @slov.new_pattern(name, num, user)
        response["Content-Type"] = "text/plain; charset=utf-8"
        response.body = newid.to_s

    #get ID:number pairs
    when 'get_numbers'
      key = request.query['name'].to_s
      response["Content-Type"] = "text/plain; charset=utf-8"
      response.body = ""
      @slov.get_numbers(key).sort.each {|id,n|
        response.body += id.to_s + ':' + n.to_s + "\n"
      }

    #get seconday implicature
    when 'secondary_implicature'
      entry_id = request.query['entry_id'].to_s
      pattern_id = request.query['pattern_id']
      response["Content-Type"] = "text/plain"
      response.body = @slov.get_secondary_implicature(entry_id, pattern_id)

    #get pattern string
    when 'pattern_string'
      entry_id = request.query['entry_id'].to_s
      pattern_id = request.query['pattern_id']
      response["Content-Type"] = "text/plain"
      response.body = @slov.get_pattern_string_from_db(entry_id, pattern_id)
      
    #get max pattern id
    when 'getmaxid'
      key = request.query['key'].to_s
      response["Content-Type"] = "text/plain; charset=utf-8"
      max = @slov.get_max_id(key)+1
      response.body = max.to_s

    #return CSO types
    when 'getcso'
      word = request.query['word']
      response.body = @slov.search_onto(word).join("\n")
    
    #return BSO types
    when 'getbso'
      word = request.query['word']
      response['Content-Type'] = 'text/plain; charset=utf-8'
      nextline = false
      downtype = ''
      data = open("http://eurydice.cs.brandeis.edu/BSOonline/BSObrowser.py?"\
          "word=#{URI.escape(word)}&db=Lexicon&action=display").each{|line|
        if nextline
          ar = []
          nextline = false
          line.split('&gt;').each { |l|
            type = /<a.*>([^<]*)<\/a>/.match(l)
            ar << type[1] if type != nil
          }
          response.body += "TopType\n"
          i = 0
          ar.each{ |l|
            i += 1
            i.times {response.body += ' '}
            response.body += l + "\n"
          }
          (i+1).times {response.body += ' '}
          response.body += downtype + "\n"
        end
        if line =~ /.*Types:.*/
          downtype = /<a.*>([^<]*)<\/a>/.match(line)[1]
          nextline = true
        end
      }

    #delete entry
    when 'deldoc'
        if @slov.is_trainee(@admin, user)
            response.status = 500
            response.body = 'Trainee can noc delete entries'
            return response.body
        end
        entry_id = request.query['id']
        response["Content-Type"] = "text/plain; charset=utf-8"
        response.body = @slov.delete(entry_id)

    #delete pattern with number not with id
    when 'delpatnum'
        if @slov.is_trainee(@admin, user)
            response.status = 500
            response.body = 'Trainee can not delete patterns'
            return response.body
        end
        entry_id = request.query['entry_id']
        pat_num = request.query['pat_num'].to_i
        renumber = request.query['renumber'].to_s
        hash = {}
        renumber.split('-').each{|d|
            a = d.split(':')
            hash[a[0]] = a[1]
        }
        resp = ''
        perm = @slov.get_perm(@admin, user, entry_id)
        if perm == 'w' or perm == 's'
            resp = @slov.delete_pattern(entry_id, pat_num, hash, user, true)
        end
        if resp == 'error'
            response.status = 500
            response.body = 'Error: pattern not deleted'
        else
            response["Content-Type"] = "text/plain; charset=utf-8"
            response.body = resp
        end

    #delete pattern
    when 'delpat'
      if @slov.is_trainee(@admin, user)
          response.status = 500
          response.body = 'Trainee can not delete patterns'
          return response.body
      end
      entry_id = request.query['entry_id']
      pat_id = request.query['pat_id']
      renumber = request.query['renumber'].to_s
      hash = {}
      renumber.split('-').each {|d|
        a = d.split(':')
        if a[0] == "0"
          hash[newid.to_s] = a[1]
        else
          hash[a[0]] = a[1]
        end
      }
      if renumber == ''
        hash = nil
      end
      resp = ''
      perm = @slov.get_perm(@admin, user, entry_id)
      if perm == 'w' or perm == 's'
        resp = @slov.delete_pattern(entry_id, pat_id, hash, user, false)
      end
      if resp == 'error'
        response.status = 500
        response.body = 'Error: pattern not deleted'
      else
        response["Content-Type"] = "text/plain; charset=utf-8"
        response.body = resp
      end

    #delete onto entry
    when 'delonto'
      if @slov.is_trainee(@admin, user)
          response.status = 500
          response.body = 'Trainee can not delete ontology items'
          return response.body
      end
      onto_id = request.query['id']
      response["Content-Type"] = "text/plain; charset=utf-8"
      response.body = @slov.delete_onto(onto_id)

    #set entry status
    when 'status'
      if @slov.is_trainee(@admin, user)
          response.status = 500
          response.body = 'Trainee can not change verb status'
          return response.body
      end
      entry_id = request.query['id']
      status = request.query['status']
      response["Content-Type"] = "text/plain; charset=utf-8"
      response.body = @slov.set_status(entry_id, status, user)

    #set entry info 
    when 'set_verb_info'
      if @slov.is_trainee(@admin, user)
          response.status = 500
          response.body = 'Trainee can not change verb info'
          return response.body
      end
      entry_id = request.query['id']
      samplesize = request.query['samplesize']
      semclass = request.query['semclass']
      aspclass = request.query['aspclass']
      comment = request.query['comment']
      difficulty = request.query['difficulty']
      compilation_time = request.query['compilation_time']
      response["Content-Type"] = "text/plain; charset=utf-8"
      response.body = @slov.set_verb_info(entry_id, user, samplesize,
                semclass, aspclass, comment, difficulty, compilation_time)

    #get entry info 
    when 'get_verb_info'
      entry_id = request.query['id']
      response["Content-Type"] = "application/json; charset=utf-8"
      status, samplesize, semclass, aspclass, erlangen, comment, patterns,\
            difficulty, compilation_time, created_by, created_time,\
            last_edit_by, last_edit_time, bnc50_freq =\
            @slov.get_verb_info(entry_id)
      r = Hash.new
      r['status'] = status.to_s
      r['samplesize'] = samplesize.to_s
      r['semclass'] = semclass.to_s
      r['aspclass'] = aspclass.to_s
      r['erlangen'] = erlangen.to_s
      r['comment'] = comment.to_s
      r['patterns'] = patterns.to_s
      r['difficulty'] = difficulty.to_s
      r['compilation_time'] = compilation_time.to_s
      r['created_by'] = created_by.to_s
      r['created_time'] = created_time.to_s
      r['last_edit_by'] = last_edit_by.to_s
      r['last_edit_time'] = last_edit_time.to_s
      r['bnc50_freq'] = bnc50_freq.to_s
      response.body = r.to_json

    #get current number of patterns
    when 'check_pats'
      entry_id = request.query['entry_id']
      response["Content-Type"] = "text/plain; charset=utf-8"
      response.body = @slov.getnofpats(entry_id)

    # get list of unique semantic types
    when 'get_semclasses'
      response["Content-Type"] = "text/plain; charset=utf-8"
      response.body = @slov.get_distinct_values('ontology', 'term')

    # get list of Roles
    when 'get_roles'
      response["Content-Type"] = "text/plain; charset=utf-8"
      response.body = @slov.get_distinct_values('objects', 'semantic_role')

    # get list of distinct secondary implicatures
    when 'get_secondary_implicatures'
      response["Content-Type"] = "text/plain; charset=utf-8"
      response.body = @slov.get_distinct_values('implicatures', 'implicature')

    # get list of primary implicatures
    when 'get_primary_implicatures'
      response["Content-Type"] = "text/plain; charset=utf-8"
      response.body = @slov.get_distinct_values('patterns',
          'primary_implicature')

    # get list of distinct lexical sets
    when 'get_lexsets'
      response["Content-Type"] = "text/plain; charset=utf-8"
      response.body = @slov.get_distinct_values('objects', 'lexset_items')

    # get list of distinct particles
    when 'get_particles'
      response["Content-Type"] = "text/plain; charset=utf-8"
      response.body = @slov.get_distinct_values('objects', 'headword')

    # get list of distinct adverbs from adverbials
    when 'get_adverbs'
      response["Content-Type"] = "text/plain; charset=utf-8"
      response.body = @slov.get_distinct_values('objects', 'advl_adverbs')

    #add hyponym in ontology
    when 'add_hypo'
      if @slov.is_trainee(@admin, user)
          response.status = 500
          response.body = 'Trainee can not add hyponyms to ontology'
          return response.body
      end
      data = request.query['data']
      hyper = request.query['hyperid']
      response["Content-Type"] = "text/plain; charset=utf-8"
      result = @slov.add_hypo(data, hyper)
      response.body = result

    #add hypernym to ontology
    when 'add_hyper'
      if @slov.is_trainee(@admin, user)
          response.status = 500
          response.body = 'Trainee can not add hyperonyms to ontology'
          return response.body
      end
      data = request.query['data']
      hypo = request.query['hypoid']
      response["Content-Type"] = "text/plain; charset=utf-8"
      result = @slov.add_hyper(data, hypo)
      response.body = result

    #change hypernym of node
    when 'change_hyper'
      if @slov.is_trainee(@admin, user)
          response.status = 500
          response.body = 'Trainee can not change hypernyms'
          return response.body
      end
      data = request.query['data']
      hypo = request.query['hypoid']
      response["Content-Type"] = "text/plain; charset=utf-8"
      result = @slov.change_hyper(data, hypo)
      response.body = result

    #get nouns from pop_ontology table according to constraints
    when 'get_nouns'
      data = request.query['data']
      constraints = request.query['constraints']
      response["Content-Type"] = "text/plain; charset=utf-8"
      result = @slov.get_nouns(data, constraints)
      response.body = result

    when 'generate_pop_onto'
      entry_id = request.query['entry_id']
      @slov.save_pop_onto(entry_id,'bnc50')

    #report wrong st-noun links from pop_ontology
    when 'reportpoponto'
      data = request.query['data']
      response["Content-Type"] = "text/plain; charset=utf-8"
      result = @slov.reportpoponto(data)
      response.body = result

    #check access rights
    when 'perm'
      entry_id = request.query['id'].to_s
      perm = @slov.get_perm(@admin, user, entry_id)
      response.body = user + ':' + entry_id + ':' + perm.to_s
  
    #export for sketches
    when 'export'
      response["Content-Type"] = "text/plain; charset=utf-8"
      response.body = @slov.export_corpora()

    #lock entry
    when 'lock_entry'
      id = request.query['id'].to_s
      lock = @server.info.who_locked('cpa', id)
      if lock != false and lock != user
        response['Content-Type'] = 'text/plain; charset=utf8'
        response.body = 'this entry is locked by ' + lock.to_s
        return response.body
      else
        @server.info.lock_entry('cpa', id, user)
        response['Content-Type'] = 'text/plain; charset=utf8'
        response.body = 'locked'
        return response.body
      end

    #unlock entry
    when 'unlock_entry'
      id = request.query['id'].to_s
      lock = @server.info.who_locked('cpa', id)
      if lock != false and lock != user
        response['Content-Type'] = 'text/plain; charset=utf8'
        response.body = 'this entry is locked by ' + lock.to_s
        return response.body
      else
        @server.info.unlock_entry('cpa', id, user)
        response['Content-Type'] = 'text/plain; charset=utf8'
        response.body = 'unlocked'
        return response.body
      end

    #check lock
    when 'lock_check'
      id = request.query['id'].to_s
      lock = @server.info.who_locked('cpa', id)
      if lock != false 
        response['Content-Type'] = 'text/plain; charset=utf8'
        response.body = 'this entry is locked by ' + lock.to_s
        return response.body
      else
        response['Content-Type'] = 'text/plain; charset=utf8'
        response.body = 'unlocked'
        return response.body
      end

    when 'get_settings'
      set = @server.info.get_settings(request.attributes[:auth_user].to_s,
          @server.info.service_name)
      response["Content-Type"] = "application/json; charset=utf-8"
      response.body = set.to_json

    when 'save_settings'
      response["Content-Type"] = "application/json; charset=utf-8"
      data = URI.unescape(request.query['data']).to_s
      data_hash = JSON.parse(data)
      set = @server.info.save_settings(request.attributes[:auth_user].to_s,
          data_hash, @server.info.service_name)
      response.body = data_hash.to_json

    else
      super(request, response)
    end
  end

  def load_percent(entry_id)
    status, samplesize, semclass, aspclass, erlangen, comment, patterns,\
        difficulty, compilation_time, created_by, created_time, last_edit_by,\
        last_edit_time, bnc50_freq = @slov.get_verb_info(entry_id)
    percent = nil
    if samplesize.to_s != ''
      if @slov.corpora == 'bnc50'
        url = "http://www.fanuk.net/bonito/run.cgi/lngroupinfo?"\
            "q=q%5Blempos%3D%22" + URI.escape(entry_id) +
            '-v%22%5D;corpname=bnc50;annotconc=' + URI.escape(entry_id) +
            '-v;format=text';
      else
        url = "http://www.fanuk.net/bonito/run.cgi/lngroupinfo?"\
            "q=q%5Blemma%3D%22" + URI.escape(entry_id) + '%22%5D;corpname=' +
            URI.escape(@slov.corpora) + ';annotconc=' + URI.escape(entry_id) +
            ';format=text';
      end
      if samplesize.to_s.downcase != 'all'
        url += '&q=r' + URI.escape(samplesize.to_s)
      end
      samplesize = samplesize.to_i
      sums = []
      total = 0
      first = true
      open(url, :http_basic_authentication=>['guest', 'GG0we5t']).each{|line|
        next if first and line.strip  == ''
        if first and (line.strip != entry_id and line.strip != entry_id+'-v')
          return nil
        end
        if first
          first = false
          next
        end
        la = line.split("\t")
        if la[0] =~ /[0-9]+(.[eaf])?/
          i = la[0].to_i
          sums[i] = 0 if sums[i] == nil
          sums[i] += la[1].to_i
          total += la[1].to_i
        end
        if la[0] == 'u' or la[0] == 'Not assigned'
          total += la[1].to_i
        end
        if samplesize > 0 and la[0] == 'x'
          samplesize -= la[1].to_i
        end
      }
      if samplesize == 0
        samplesize = total
      end
      percent = [samplesize]
      for i in (1..sums.size-1)
        perc = ((sums[i].to_f/samplesize.to_f)*100).round
        if perc < 1
          percent[i] = '<1%'
        else
          percent[i] = perc.to_s + '%'
        end
      end
    end
    return percent
  end

  alias do_POST do_GET
end

