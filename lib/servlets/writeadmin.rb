require 'template'
require 'uri'
require 'tempfile'
require 'import'

class WriteAdminServlet < DictServlet
  def initialize( server, dict, array )
    super
    @servername = nil
  end

  def add_jsonp(data, callback)
    return callback + "(" + data + ");"
  end

  def do_GET(request, response)
    @servername = get_servername(@server, request) if @servername == nil
    request.query['action'] = 'main' if request.query['action'] == nil or request.query['action'] == ''
    user = request.attributes[:auth_user].to_s
    dict_path = request.path[1..-1]

    case request.query['action']
    when 'main'
      tmpl_param = @dict.get_user_dicts(user)
      tmpl_param['my_dict'] = 1 unless (tmpl_param['dict_m']+tmpl_param['dict_w']+tmpl_param['dict_r']).size == 0
      res = HTMLTemplate.new(tmpl_param).output(@dict.template_path + '/main.tmpl')
      response.body = res
    when 'dict_list'
      response.body = @dict.get_user_dicts(user).to_json
    when 'dict_info'
      info = {}
      if request.query['code'].to_s != ''
        info = @dict.get_dict_info(request.query['code'].to_s)
        info['owner'] = request.query['code'][0,request.query['code'].index('_')]
        info['users'] = @dict.get_dict_users(request.query['code'])
        info['templates'] = @dict.get_dict_templates(request.query['code'], @server.info.xslt_path)
        info['htemplates'] = @dict.get_dict_htemplates(request.query['code'], @server.info.xslt_path)
      end
      response.body = info.to_json
    when 'dict_edit'
      tmpl_param = {}
      if request.query['dict'].to_s != ''
        tmpl_param = @dict.get_dict_info(request.query['dict'].to_s)
        tmpl_param['edit'] = 1 
        tmpl_param['exist'] = 1 if request.query['exist'].to_s == '1'
        tmpl_param['owner'] = request.query['dict'][0,request.query['dict'].index('_')]
        tmpl_param['users'] = @dict.get_dict_users(request.query['dict']).to_json
        tmpl_param['templates'] = @dict.get_dict_templates(request.query['dict'], @server.info.xslt_path).to_json
        tmpl_param['htemplates'] = @dict.get_dict_htemplates(request.query['dict'], @server.info.xslt_path).to_json
      end
      res = HTMLTemplate.new(tmpl_param).output(@dict.template_path + '/dictedit.tmpl')
      response.body = res
    when 'dict_save'
      code = request.query['dict_code'].to_s
      code = user + '_' + code if request.query['edit'].to_s == ''
      name = request.query['dict_name'].gsub(/[<>&]/, '')
      $stderr.puts code
      $stderr.puts @dict.get('dict_'+code)
      if request.query['edit'].to_s != '1' and @dict.get('dict_'+code).to_s != ''
        response.body = 'dictionary code '+code+' is already used'
      else
        dictxml = '<dict><code>'+code+'</code><name>'+name+'</name><key>/entry/@id</key><root>entry</root><class>WriteDict</class><schema>'+request.query['setting'].to_s+'</schema></dict>'
        dictxml = @dict.copy_templates(code, dictxml)
        $stderr.puts dictxml
        @dict.update('dict'+code, dictxml)
        @dict.add_dict_to_service(code)
        @dict.add_user_to_dict(user, code, 'm')
        @dict.remount(@server, 'write')
        response.body = 'OK='+code
      end
      return response.body
    when 'dict_del'
      if request.query['code'].to_s != ''
        if request.query['code'][0,user.length] != user
          response.body = 'not allowed to delete dictionary ' + request.query['code']
          return response.body
        end
        @dict.delete('dict'+request.query['code'].to_s)
        @dict.remove_dict_from_service(request.query['code'].to_s)
        @dict.remove_all_users_from_dict(request.query['code'].to_s)
        @dict.remount(@server, 'write')
        response.set_redirect(WEBrick::HTTPStatus::SeeOther, @servername + '/admin')
      end
    when 'user_add'
      if request.query['login'].to_s != '' and request.query['code'].to_s != ''
        perm = 'r'
        perm = request.query['perm'].to_s if request.query['perm'].to_s != ''
        if @dict.get('user'+request.query['login'].to_s) == ''
          response.body = 'user '+request.query['login'].to_s+' not existing'
          return response.body
        else
          @dict.add_user_to_dict(request.query['login'].to_s, request.query['code'].to_s, perm)
          response.body = 'OK'
          return response.body
        end
      else
        response.body = 'unknown login'
        return response.body
      end
    when 'user_remove'
      if request.query['login'].to_s != '' and request.query['code'].to_s != ''
        if @dict.get('user'+request.query['login'].to_s) == ''
          response.body = 'user '+request.query['login'].to_s+' not existing'
          return response.body
        else
          @dict.remove_user_from_dict(request.query['login'].to_s, request.query['code'].to_s)
          response.body = 'OK'
          return response.body
        end
      else
        response.body = 'unknown login'
        return response.body
      end
    when 'template_save'
      begin
        @dict.template_save(request.query['dict'], request.query['name'], request.query['code'], request.query['template'], @server.info.xslt_path)
        @dict.remount(@server, 'write')
        response.body = 'OK'
      rescue => e
        $stderr.puts e
        $stderr.puts e.backtrace
        response.body = e
      end
      return response.body
    when 'template_remove'
      begin
        @dict.template_remove(request.query['dict'], request.query['code'], @server.info.xslt_path)
        @dict.remount(@server, 'write')
        response.body = 'OK'
      rescue => e
        $stderr.puts e
        $stderr.puts e.backtrace
        response.body = e
      end
      return response.body
    when 'htemplate_save'
      begin
        @dict.htemplate_save(request.query['dict'], request.query['name'], request.query['code'], request.query['template'], @server.info.xslt_path)
        #@dict.remount(@server, 'write')
        response.body = 'OK'
      rescue => e
        $stderr.puts e
        $stderr.puts e.backtrace
        response.body = e
      end
      return response.body
    when 'htemplate_remove'
      begin
        @dict.htemplate_remove(request.query['dict'], request.query['code'], @server.info.xslt_path)
        #@dict.remount(@server, 'write')
        response.body = 'OK'
      rescue => e
        $stderr.puts e
        $stderr.puts e.backtrace
        response.body = e
      end
      return response.body
    when 'export'
      code = request.query['code']
      begin
        response['Content-Type'] = 'text/xml; charset=utf-8'
        response.body = @dict.array[code].get_all
      rescue => e
        $stderr.puts e
        $stderr.puts e.backtrace
        response.body = e
      end
      return response.body
    when 'import'
      if request.query['code'].to_s != ''
        tmpl_param = @dict.get_dict_info(request.query['code'].to_s)
        res = HTMLTemplate.new(tmpl_param).output(@dict.template_path + '/import.tmpl')
        response.body = res
      else
        response.body = 'no dictionary code selected'
      end
      return response.body
    when 'import_upload'
      if request.query['code'].to_s == ''
        response.body = 'no dictionary selected'
        return response.body
      end
      if request.query['file'].to_s == ''
        response.body = 'no file data'
        return response.body
      end
      temp = Tempfile.new("import_temp")
      filedata = request.query['file']
      temp.syswrite(filedata)
      temp.close
      srcfile = temp.path
      code = request.query['code'].to_s
      root = 'entry'
      key = '/entry/@id'
      logfile = '/var/log/deb-server/' + code + Time.now.strftime('%Y%m%d-%H%M') +'.log'
      delete = false
      delete = true if request.query['delete'].to_s == 'on'
      overwrite = false
      overwrite = true if request.query['overwrite'].to_s == 'on'
      $stderr.puts srcfile.to_s + ';' +  code.to_s + ';' +  logfile.to_s+ ';' +  delete.to_s + ';' +overwrite.to_s + ';' + key + ';' + root
      Thread.new{ import(code, srcfile, logfile, key, root, delete, overwrite) }      
      response.set_redirect(WEBrick::HTTPStatus::SeeOther, @servername + '/admin?action=showlog&type=import&log=' + logfile)
    when 'showlog'
      logfile = request.query['log'].to_s
      type = request.query['type'].to_s
      result = %x[tail -n 20 #{logfile}]
      lastline = ''
      result.to_s.each{|line| lastline=line}
      response['Content-Type'] = 'text/html; charset=utf-8'
      response.body = '<html><head><meta http-equiv="Refresh" content="5"/></head><body><a href="/admin">admin</a><pre>'+result.to_s+'</pre>'
      response.body += '</body></html>'

    end
      if request.query['callback'].to_s != ''
        response["Content-type"] = "text/plain"
        response.body = add_jsonp(response.body, request.query['callback'].to_s)
      end
  end
  alias do_POST do_GET
end
