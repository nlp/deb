class WriteServlet < DictServlet
  def initialize( server, dict, array )
    super
    @servername = nil
  end
  def do_GET(request, response)
    @servername = get_servername(@server, request) if @servername == nil
    request.query['action'] = 'list' if request.query['action'] == nil or request.query['action'] == ''
    user = request.attributes[:auth_user].to_s
    dict_path = request.path[1..-1]
    perm = @dict.get_perm(user)
    $stderr.puts request.query['action']
    case request.query['action']
    when 'list'
      templates = @server.info.get_dict_templates(dict_path, @server.info.xslt_path)
      htemplates = @server.info.get_dict_htemplates(dict_path, @server.info.xslt_path)
      entries = @dict.get_entries(request.query['search'].to_s, perm, templates, htemplates)
      tmpl_param = {'dict_name'=>@dict.title, 'code'=>dict_path, 'entries'=>entries}
      tmpl_param['search'] = request.query['search'].to_s if request.query['search'].to_s != ''
      tmpl_param['templates'] = templates
      tmpl_param['htemplates'] = htemplates
      tmpl_param['is_manager'] = '1' if perm == 'm'
      tmpl_param['is_editor'] = '1' if perm == 'm' or perm == 'w'
      res = HTMLTemplate.new(tmpl_param).output(@dict.template_path + '/list.tmpl')
      response['Content-Type'] = 'text/html; charset=utf-8'
      response.body = res
    when 'edit'
      if perm != 'm' and perm != 'w'
        response.body = 'not allowed to edit entries'
        return response.body
      end
      tmpl_param = {'dict_name'=>@dict.title, 'schema'=>@dict.schema, 'code'=>dict_path, 'entryid'=>request.query['id'].to_s}
      res = HTMLTemplate.new(tmpl_param).output(@dict.template_path + '/editor.tmpl')
      response['Content-Type'] = 'text/html; charset=utf-8'
      response.body = res
    when 'get'
      if request.query['tr'].to_s != ''
        response["Content-type"] = "text/html; charset=utf-8"
        response.body = @dict.apply_transform(request.query['tr'].to_s, @dict.get(request.query['id'].to_s))
      else
        response['Content-Type'] = 'text/xml; charset=utf-8'
        response.body = @dict.get(request.query['id'].to_s)
      end
      return response.body
    when 'preview_all'
      if request.query['tr'].to_s != ''
        response["Content-type"] = "text/html; charset=utf-8"
        response.body = @dict.apply_transform(request.query['tr'].to_s, @dict.get_all)
      else
        response['Content-Type'] = 'text/xml; charset=utf-8'
        response.body = @dict.get_all
      end
      return response.body

    when 'hbpreview'
      htemplates = @server.info.get_dict_htemplates(dict_path, @server.info.xslt_path)
      hb = ''
      htemplate = htemplates.select{|h| h['code'] == request.query['hb'].to_s}
      unless htemplate[0].nil?
        hb = htemplate[0]['template'].to_s 
      end
      data = CobraVsMongoose.xml_to_hash(@dict.get(request.query['id'].to_s)).to_json
      tmpl_param = {'data'=>data, 'hb'=>hb}
      res = HTMLTemplate.new(tmpl_param).output(@dict.template_path + '/hbpreview.tmpl')
      response['Content-Type'] = 'text/html; charset=utf-8'
      response.body = res
    when 'save'
      $stderr.puts request.query['data']
      $stderr.puts request.query['id']
      if perm != 'm' and perm != 'w'
        response.body = 'not allowed to edit entries'
        return response.body
      end      
      if request.query['data'].to_s == ''
        response.body = 'no XML data'
        return response
      end
      if request.query['id'].to_s == ''
        id = @server.info.get_next_id(dict_path).to_s
        data = request.query['data'].to_s.sub('entry id=""', 'entry id="'+id+'"')
      else
        id = request.query['id'].to_s
        data = request.query['data'].to_s
      end
      data = @dict.add_meta(data, user, id)
      data = @dict.detect_mime(data, dict_path, @server.info.file_path)
      $stderr.puts id
      begin
        @dict.update(id, data)
        response.body = 'OK=' + id
      rescue => e
        $stderr.puts e
        $stderr.puts e.backtrace
        response.body = e
      end
      return response.body
    when 'delete'
      if perm != 'm' and perm != 'w'
        response.body = 'not allowed to edit entries'
        return response.body
      end
      if request.query['id'].to_s == ''
        response.body = 'no ID'
        return response.body
      end
      @dict.delete(request.query['id'].to_s)
      response.body = 'OK'
      return response.body
    when 'upload'
      begin
      file_path = @server.info.file_path + '/' + dict_path 
      filename = file_path + '/' + request.query['data'].filename
      filedata = request.query['data']
        unless File.directory?(file_path)
          Dir.mkdir(file_path)
        end
        $stderr.puts filename
        f = File.new(filename, 'w')
        f.syswrite(filedata)
        f.close
        response.body = 'OK='+request.query['data'].filename
      rescue => e
        $stderr.puts e
        $stderr.puts e.backtrace
        response.body = e
      end
      return response.body
    end
  end
  alias do_POST do_GET
end
