require 'webrick'
require 'cobravsmongoose'

class CdbLUServlet < JSWordnetServlet
def do_GET(request, response)
    dictionary = request.path[1..-1]
    user = request.attributes[:auth_user].to_s
    case request.query['action']
    when 'queryList'
      if not @server.info.check_perm(Thread.current[:auth_user], @wn.dict_prefix+'cdb_lu')
        response.body = [{'value' => '0', 'label'=> 'not authorized'}].to_json
        return response.body
      end

      query = URI.unescape(request.query['word']).to_s
      response["Content-type"] = "text/plain; charset=utf-8"
      
      if query == ''
        $stderr.puts "Query not set"
        response.body += "Query not set".to_json
      else
        lus = @wn.get_list(query, @wn_array)
        lu_ar = []
        lus.each {|id,info|
          label = ''
          label += (info['lockstatus'] == user)? '[*] ':'[x] '  if info['lockstatus'] != false
          label += id + ' ' + info['pos'] + ', ' + info['form'].to_s + ':' + info['seq_nr']
          label += ', ' + info['domain'].to_s.strip if info['domain'].to_s != ''
          label += ', ' + info['resume'].to_s.strip if info['resume'].to_s != ''
          $stderr.puts label
          lu_ar << {'value' => id, 'label' => label, 'seq_nr' => info['seq_nr'].to_i, 'form' => info['form'].to_s, 'pos' => info['pos'].to_s}
        }
        response.body += lu_ar.sort!{|x,y| (x['pos'] <=> y['pos']).nonzero? || (x['form'] <=> y['form']).nonzero? || x['seq_nr'] <=> y['seq_nr']}.to_json
      end
    when 'save'
      id = request.query['id']
      $stderr.puts "EDIT LU ID " + id

      check = @wn.check_lock(dictionary, id, Thread.current[:auth_user])
      if not(check == true)
        response['Content-Type'] = 'text/plain'
        response.body = check.to_json
        return
      end
      if not @server.info.check_perm(Thread.current[:auth_user], @wn.dict_prefix+'cdb_lu', 'w')
        response["Content-type"] = "text/plain; charset=utf-8"
        response.body = {'action'=>'error', 'message'=>'not authorized'}.to_json
        return response.body
      end
      
      begin
        new = false
        if id == '_new_id_' or id == ''
          new = true
          id = @server.info.get_next_id(dictionary)
        end
$stderr.puts 'xx'
        data = URI.unescape(request.query['data']).to_s
        data_hash = JSON.parse data
        $stderr.puts data
        #update data
        data_hash['cdb_lu']['@c_lu_id'] = id
        #examples
        if data_hash['cdb_lu']['examples'] != nil
          if data_hash['cdb_lu']['examples']['example'].is_a? Hash
            if data_hash['cdb_lu']['examples']['example']['@r_ex_id'] == nil
              data_hash['cdb_lu']['examples']['example']['@r_ex_id'] = id + '-1'
            end
          else
            examnr = 1
            data_hash['cdb_lu']['examples']['example'].each{|exam|
              if exam['@r_ex_id'] == nil
                exam['@r_ex_id'] = id + '-' + examnr.to_s
                examnr += 1
              end
            }
          end
        end

        #save
        xml = CobraVsMongoose.hash_to_xml(data_hash)
        xml = @wn.follow_redundant(xml, @wn_array[@wn.dict_prefix+'cdb_red2'])
        $stderr.puts xml
        @wn.update(id, xml.to_s)
        #new LU, create CID
        #if new
        #  newcid = @wn_array[@wn.dict_prefix+'cdb_id'].newcid(id, '', @server.info, @wn_array[@wn.dict_prefix+'cdb_lu'])
        #else
        #  @wn.update_cids(@wn_array[@wn.dict_prefix+'cdb_id'], id, data_hash['cdb_lu']['form']['@form-spelling'].to_s, data_hash['cdb_lu']['@c_seq_nr'].to_s, data_hash['cdb_lu']['form']['@form-cat'].upcase.to_s)
        #end
        #update previews in synsets
        @wn.update_synset_preview(@wn_array[@wn.dict_prefix+'cdb_syn'], id, data_hash['cdb_lu']['form']['@form-spelling'].to_s, data_hash['cdb_lu']['@c_seq_nr'].to_s)
        response['Content-Type'] = 'text/plain'
        response.body = {'action'=>'saved', 'reload'=>['cdblu1'], 'id'=>id}.to_json
      rescue => e
        response['Content-Type'] = 'text/plain'
        $stderr.puts e.message
        $stderr.puts e.backtrace.join("\n")
        response.body = {'action'=>'error', 'message'=>"exception #{e.class.to_s}: #{e}"}.to_json
      end
    when 'newid'
      newid = @server.info.get_next_id(dictionary)
      response["Content-type"] = "text/plain; charset=utf-8"
      response.body = newid.to_s
    when 'delete'
      id = request.query['id']
      force = request.query['force'].to_s
      $stderr.puts "DELETE LU #{id}, force #{force}"
      check = @wn.check_lock(dictionary, id, Thread.current[:auth_user])
      if not(check == true)
        response['Content-Type'] = 'text/plain'
        response.body = check.to_json
        return
      end

      delete_lu = false
      last = false
      begin
        #is this last synonym in synset?
        in_synset = false
        @wn_array[@wn.dict_prefix+'cdb_syn'].xquery_to_hash("[cdb_synset[synonyms/synonym/@c_lu_id='#{id}']]").each{|sy_id,xml|
          in_synset = true
          doc = REXML::Document.new(xml.to_s)
          count = doc.elements.to_a('/cdb_synset/synonyms/synonym').size
          $stderr.puts "synonym count" + count.to_s
          if count == 1
            last = true
            if force == 'true'
              $stderr.puts "REMOVE SYNSET "+sy_id
              @wn_array[@wn.dict_prefix+'cdb_syn'].delete(sy_id)
              delete_lu = true
            else
              response['Content-Type'] = 'text/plain'
              response.body = {'action'=>'in_synset', 'reload'=>[]}.to_json
              return
            end
          else
            #remove LU from synset
            @wn.update_synset_preview(@wn_array[@wn.dict_prefix+'cdb_syn'], id, '', '')
            doc.elements.delete("/cdb_synset/synonyms/synonym[@c_lu_id='#{id}']")
            $stderr.puts "REMOVE FROM SY "+sy_id
            @wn_array[@wn.dict_prefix+'cdb_syn'].update(sy_id, doc.to_s)
            #delete CIDs for LU
            delete_lu = true
          end
        }
        delete_lu = true if not in_synset
        if delete_lu and (not last or force == 'true')
          #@wn_array[@wn.dict_prefix+'cdb_id'].delete_for_lu(id)
          $stderr.puts "REMOVE LU"
          @wn.delete(id)
        end
        @wn.unlock(dictionary, id, Thread.current[:auth_user])
        response['Content-Type'] = 'text/plain'
        response.body = {'action'=>'deleted', 'reload'=>['cdblu1', 'cdbid1']}.to_json
      rescue => e
        response['Content-Type'] = 'text/plain'
        $stderr.puts e.message
        $stderr.puts e.backtrace.join("\n")
        response.body = {'action'=>'error', 'message'=>"exception #{e.class.to_s}: #{e.message}"}.to_json
      end

    when 'lock'
      id = request.query['id']
      begin
    	  res = @wn.get(id).to_s
        check = @wn.check_lock(dictionary, id, Thread.current[:auth_user])
        if check == true
          @wn.lock(dictionary, id, Thread.current[:auth_user])
          status = {'action' => 'locked'}
        else
          status = check
        end
      rescue => e
        status = {'error' => e.to_s}
      end
      response["Content-type"] = "text/plain; charset=utf-8"
      response.body = status.to_json
    when 'unlock'
      id = request.query['id']
      @wn.unlock_time(dictionary, id, Thread.current[:auth_user])
      response["Content-type"] = "text/plain; charset=utf-8"
      response.body = {'action' => 'unlocked'}.to_json
      
    else
      super(request, response)
    end
  end
  
  alias do_POST do_GET 

  def runQuery(dictionary, query, type, arg, dictcode, default='', expand='', tree_details = 0, display_only = '', reldics=[])
    if not @server.info.check_perm(Thread.current[:auth_user], @wn.dict_prefix+'cdb_lu')
      return nil
    end
    
    begin
    	resxml = dictionary.get(query)
    rescue
      return nil
    end
    res = resxml.to_s
  	$stderr.puts resxml
  	case type
    when 'plain'
      res = @wn.strip_redundant(res, true)
      #res = change_multiple(res)
      return res
  	when 'xml'
      res = @wn.strip_redundant(res, true)
      #res = dictionary.apply_transform( 'xml', res, [ [ 'slovnik', '"'+dictcode.to_s+'"'], [ 'servername', '"'+@servername.to_s+'"' ] ] )
      param = {'slovnik'=>dictcode.to_s}
      res = dictionary.apply_transform( 'xml', res, param )
  		return res
  	when 'html'
      transform = 'preview'
      transform += arg if arg.to_s != ''
      res = @wn.strip_redundant(res, true)
      #res = dictionary.apply_transform( transform, res, [ [ 'slovnik', '"'+dictcode.to_s+'"'], [ 'servername', '"'+@servername.to_s+'"' ], ['default', '"'+default.to_s+'"'] ] )
      param = {'slovnik'=>dictcode.to_s}
      param['default'] = default.to_s unless default.to_s == ''
      res = dictionary.apply_transform( transform, res, param)
  		return res
  	when 'tree'
  	when 'editor'
      #res = change_multiple(res)
      res = @wn.strip_redundant(res, true)
      check = dictionary.check_lock(dictcode, query, Thread.current[:auth_user])
      hash = CobraVsMongoose.xml_to_hash(res)
      begin
        if hash['cdb_lu']['morphology_noun']['morpho_plurforms']['morpho_plurform'].is_a?(Hash)
          temp = hash['cdb_lu']['morphology_noun']['morpho_plurforms']['morpho_plurform']
          hash['cdb_lu']['morphology_noun']['morpho_plurforms']['morpho_plurform'] = [temp]
        end
      rescue
      end
      begin
        if hash['cdb_lu']['syntax_noun']['sy-complementation']['sy-comp'].is_a?(Hash)
          temp = hash['cdb_lu']['syntax_noun']['sy-complementation']['sy-comp']
          hash['cdb_lu']['syntax_noun']['sy-complementation']['sy-comp'] = [temp]
        end
      rescue
      end
      begin
        if hash['cdb_lu']['examples']['example'].is_a?(Hash)
          temp = hash['cdb_lu']['examples']['example']
          hash['cdb_lu']['examples']['example'] = [temp]
        end
      rescue
      end
      begin
        hash['cdb_lu']['examples']['example'].each{|h|
          if h['syntax_example']['sy-combi']['sy-combipair'].is_a?(Hash)
            temp = h['syntax_example']['sy-combi']['sy-combipair']
            h['syntax_example']['sy-combi']['sy-combipair'] = [temp]
          end
        }
      rescue
      end
      begin
        if hash['cdb_lu']['syntax_verb']['sy-complementation']['sy-comp'].is_a?(Hash)
          temp = hash['cdb_lu']['syntax_verb']['sy-complementation']['sy-comp']
          hash['cdb_lu']['syntax_verb']['sy-complementation']['sy-comp'] = [temp]
        end
      rescue
      end
      begin
        if hash['cdb_lu']['syntax_verb']['sy-complementation']['sy-compl-text'].is_a?(Hash)
          temp = hash['cdb_lu']['syntax_verb']['sy-complementation']['sy-compl-text']
          hash['cdb_lu']['syntax_verb']['sy-complementation']['sy-compl-text'] = [temp]
        end
      rescue
      end
      begin
        if hash['cdb_lu']['morphology_verb']['flex-conjugation'].is_a?(Hash)
          temp = hash['cdb_lu']['morphology_verb']['flex-conjugation']
          hash['cdb_lu']['morphology_verb']['flex-conjugation'] = [temp]
        end
      rescue
      end
      begin
        if hash['cdb_lu']['semantics_verb']['sem-selrestriction'].is_a?(Hash)
          temp = hash['cdb_lu']['semantics_verb']['sem-selrestriction']
          hash['cdb_lu']['semantics_verb']['sem-selrestriction'] = [temp]
        end
      rescue
      end
      begin
        if hash['cdb_lu']['semantics_verb']['sem-caseframe']['args']['arg'].is_a?(Hash)
          temp = hash['cdb_lu']['semantics_verb']['sem-caseframe']['args']['arg']
          hash['cdb_lu']['semantics_verb']['sem-caseframe']['args']['arg'] = [temp]
        end
      rescue
      end
      begin
        hash['cdb_lu']['semantics_verb']['sem-caseframe']['args']['arg'].each{|h|
          if h['synset_list'].is_a?(Hash)
            temp = h['synset_list']
            h['synset_list'] = [temp]
          end
        }
      rescue
      end
      begin
        if hash['cdb_lu']['semantics_adj']['sem-selrestrictions']['sem-selrestriction'].is_a?(Hash)
          temp = hash['cdb_lu']['semantics_adj']['sem-selrestrictions']['sem-selrestriction']
          hash['cdb_lu']['semantics_adj']['sem-selrestrictions']['sem-selrestriction'] = [temp]
        end
      rescue
      end
      begin
        hash['cdb_lu']['semantics_adj']['sem-selrestrictions']['sem-selrestriction'].each{|h|
          if h['synset_list'].is_a?(Hash)
            temp = h['synset_list']
            h['synset_list'] = [temp]
          end
        }
      rescue
      end

      begin
        if hash['cdb_lu']['syntax_adj']['sy-complementation']['sy-comp'].is_a?(Hash)
          temp = hash['cdb_lu']['syntax_adj']['sy-complementation']['sy-comp']
          hash['cdb_lu']['syntax_adj']['sy-complementation']['sy-comp'] = [temp]
        end
      rescue
      end
      begin
        if hash['cdb_lu']['morphology_adj']['mor-comparis'].is_a?(Hash)
          temp = hash['cdb_lu']['morphology_adj']['mor-comparis']
          hash['cdb_lu']['morphology_adj']['mor-comparis'] = [temp]
        end
      rescue
      end

      return hash
  	end
  end

  #rewrite multiple numbered tags to tree structure
  def change_multiple(xml)
    doc = REXML::Document.new(xml)
    REXML::XPath.each(doc, '//syntax_example/sy-combiword'){|el|
      if el.parent.elements['sy-combiwords'] == nil
        el.parent.add_element('sy-combiwords')
      end
      el.parent.elements['sy-combiwords'].elements << el
    }
    REXML::XPath.each(doc, '//sy-combiword2'){|el|
      el.parent.elements['sy-combiwords'].elements << el
      el.name = 'sy-combiword'
    }
    REXML::XPath.each(doc, '//syntax_example/sy-combicat'){|el|
      if el.parent.elements['sy-combicats'] == nil
        el.parent.add_element('sy-combicats')
      end
      el.parent.elements['sy-combicats'].elements << el
    }
    REXML::XPath.each(doc, '//sy-combicat2'){|el|
      el.parent.elements['sy-combicats'].elements << el
      el.name = 'sy-combicat'
    }

    REXML::XPath.each(doc, '//sy-complementation/*'){|el|
      el.name = 'sy-comp'
    }
    return doc.to_s
  end
end
