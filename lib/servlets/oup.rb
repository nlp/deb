require 'net/http'
require 'uri'

class OUPServlet < DictServlet

#def TNAServlet.get_instance config, *options
#load __FILE__
#TNAServlet.new config, *options
#end

  def initialize( server, dict, array )
    super
    @servername = nil
  end

  def do_GET(request, response)
    dict_path = request.path[1..-1]
    request.query['action'] = 'search' if request.query['action'].to_s == ''
    case request.query['action']
    when 'search'
      name = request.query['name'].to_s
      result = '<html><head><meta content="text/html; charset=utf-8" http-equiv="Content-Type"/><link href="/files/surnames.css" rel="stylesheet" type="text/css"/><title>FaNUK1 OUP: '+name+'</title></head><body class="preview"><h1>FaNUK1 OUP: '+name+'</h1><form method="get" action="/oup"><input type="hidden" name="action" value="search"/><input type="text" name="name"/><input type="submit" value="search"/></form>'
      found = false
      @dict.xquery_to_hash('[entry[//hw="'+name+'"]]').each{|k,x|
        found = true
        result += @dict.apply_transform('preview', x.to_s).gsub('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>','').gsub(/\n,/m,',').gsub(/\n\./m,'.').gsub(/\n;/m,';')
        $stderr.puts k
      }
      result += 'not found' unless found
      result += '</body></html>'
      response.body = result
      response['Content-Type'] = 'text/html; charset=utf-8'
    else
      super(request, response)
    end
  end
end
