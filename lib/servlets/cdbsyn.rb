require 'webrick'
require 'cobravsmongoose'

class CdbSynServlet < JSWordnetServlet
  def do_GET(request, response)
    dictionary = request.path[1..-1]
    user = request.attributes[:auth_user].to_s
    case request.query['action']
    when 'queryList'
      #response["Content-type"] = "text/plain; charset=utf-8"
      response["Content-type"] = "application/json; charset=utf-8"
      if not @server.info.check_perm(Thread.current[:auth_user], @wn.dict_prefix+'cdb_syn')
        response.body = [{'value' => '0', 'label'=> 'not authorized', 'style' => 'BLACK|GREEN|RED'}].to_json
        return response.body
      end
    
      query = URI.unescape(request.query['word']).to_s
      
      if query == ''
        $stderr.puts "Query not set"
        response.body += "Query not set".to_json
      else
        synsets = @wn.get_list(query, @wn_array)
        
        syn_ar = []
        synsets.each {|id,info|
          form = ''
          seq_nr = 0 
          label = ''
          label += (info['lockstatus'] == user)? '[*] ':'[x] '  if info['lockstatus'] != false
          #label += id
          #label += ' [' + info['pos'].to_s + '] '
          #label += info['literals'].join(', ') + ' | ' 
          #label += info['domain'].to_s + ' | '
          #label += info['def'].to_s.strip
          label += id + ' [' + info['pos'].to_s + '] ' + info['literals'].join(', ') + ' | ' + info['domain'].to_s + ' | ' + info['def'].to_s.strip
          form,seq_nr = info['literals'][0].split(':') if info['literals'][0] != nil
          syn_ar << {'value' => id, 'label'=> label, 'style' => 'BLACK|GREEN|RED', 'form' => form, 'seq_nr' => seq_nr.to_i, 'LOCK'=>lock_info(dictionary, id)}
        }
        response.body += syn_ar.sort!{|x,y| (x['form']<=>y['form']).nonzero? || x['seq_nr']<=>y['seq_nr']}.to_json
      end
    when 'subtree'
      response["Content-type"] = "text/plain; charset=utf-8"
      query = request.query['query']
      arg = request.query['arg']
    	resxml = @wn.get(query).to_s
      array = []
  		if arg == nil or arg == '' or arg == 'ILR'
    		arg = 'HAS_HYPONYM'
      else
        arg = 'HAS_HYPERONYM'
  		end
      
      if arg == 'HAS_HYPONYM'
        #resxml = @wn.get_rilr(resxml, @wn_array)
      end
      tree = @wn.get_subtree(resxml, @wn_array, arg)
  		tree.each {|ar|
  			array << {'parent' => ar[0], 'id' => ar[1], 'val' => ar[2]}
  		}
  		response.body = array.to_json
    when 'fullsubtree'
      response["Content-type"] = "text/plain; charset=utf-8"
      query = request.query['query']
      arg = request.query['arg']
    	resxml = @wn.get(query).to_s
      array = []
  		if arg == nil or arg == '' or arg == 'ILR'
    		arg = 'HAS_HYPONYM'
      else
        arg = 'HAS_HYPERONYM'
  		end
      
      if arg == 'HAS_HYPONYM'
        #resxml = @wn.get_rilr(resxml, @wn_array)
      end
      
      tree = @wn.get_full_subtree(resxml, @wn_array, arg)
  		tree.each {|ar|
  			array << {'parent' => ar[0], 'id' => ar[1], 'val' => ar[2]}
  		}
  		response.body = array.to_json
    when 'save'
      id = request.query['id']
      $stderr.puts "EDIT Synset ID " + id + ' START ' + time_stamp()
      check = @wn.check_lock(dictionary, id, Thread.current[:auth_user])
      if not(check == true)
        response['Content-Type'] = 'text/plain'
        response.body = check.to_json
        return
      end
      if not @server.info.check_perm(Thread.current[:auth_user], @wn.dict_prefix+'cdb_syn', 'w')
        response["Content-type"] = "text/plain; charset=utf-8"
        response.body = {'action'=>'error', 'message'=>'not authorized'}.to_json
        return response.body
      end

      begin
        new = false
        if id == '_new_id_' or id == ''
          new = true
          id = @server.info.get_next_id(dictionary)
        end
        data = URI.unescape(request.query['data']).to_s
        data_hash = JSON.parse data
        $stderr.puts data
        #update data
        data_hash['cdb_synset']['@c_sy_id'] = id
        #if existing, check removed/added synonyms
        if not new
          begin
            origxml = @wn.get(id)
            orighash = CobraVsMongoose.xml_to_hash(origxml.to_s)
            $stderr.puts "Original: "+orighash.to_s
            origsyns = []
            if orighash['cdb_synset']['synonyms']['synonym'].is_a?(Array)
              orighash['cdb_synset']['synonyms']['synonym'].each{|syn|
                origsyns << syn['@c_lu_id'].to_s
              }
            else
              origsyns << orighash['cdb_synset']['synonyms']['synonym']['@c_lu_id']
            end
          rescue =>e 
            $stderr.puts "Not new " + e.to_s
          end
        else
          origsyns = []
        end
        newsyns = []
        if data_hash['cdb_synset']['synonyms']['synonym'] != nil
          if data_hash['cdb_synset']['synonyms']['synonym'].is_a?(Array)
            data_hash['cdb_synset']['synonyms']['synonym'].each{|syn|
              newsyns << syn['@c_lu_id'].to_s if syn['@c_lu_id'].to_s != ''
            }
          else
            newsyns << data_hash['cdb_synset']['synonyms']['synonym']['@c_lu_id']
          end
        end
        ##removed
        #(origsyns-newsyns).each{|lu_id|
        #  $stderr.puts "Remove synonym " + lu_id.to_s
        #  #@wn_array[@wn.dict_prefix+'cdb_id'].deselect_lu(lu_id)
        #}
        ##added
        #(newsyns-origsyns).each{|lu_id|
        #  $stderr.puts "Add synonym " + lu_id.to_s
        #  newcid = @wn_array[@wn.dict_prefix+'cdb_id'].newcid(lu_id, id, @server.info, @wn_array[@wn.dict_prefix+'cdb_lu'])
        #  @wn_array[@wn.dict_prefix+'cdb_id'].deselect_lu(lu_id)
        #  @wn_array[@wn.dict_prefix+'cdb_id'].select(newcid)
        #}
        #set editor name to ontology 
        if data_hash['cdb_synset']['sumo_relations']['ont_relation'] != nil
          if data_hash['cdb_synset']['sumo_relations']['ont_relation'].is_a?(Array)
            data_hash['cdb_synset']['sumo_relations']['ont_relation'].each{|ont|
              ont['@name'] = user if ont['@name'].to_s == ''
            }
          else
            data_hash['cdb_synset']['sumo_relations']['ont_relation']['@name'] = user if data_hash['cdb_synset']['sumo_relations']['ont_relation']['@name'].to_s == ''
          end
        end
        #set editor name to domains
        if data_hash['cdb_synset']['wn_domains']['dom_relation'] != nil
          if data_hash['cdb_synset']['wn_domains']['dom_relation'].is_a?(Array)
            data_hash['cdb_synset']['wn_domains']['dom_relation'].each{|dom|
              dom['@name'] = user if dom['@name'].to_s == ''
            }
          else
            data_hash['cdb_synset']['wn_domains']['dom_relation']['@name'] = user if data_hash['cdb_synset']['wn_domains']['dom_relation']['@name'].to_s == ''
          end
        end
        #set editor name to wn, remove empty
        if data_hash['cdb_synset']['wn_equivalence_relations']['relation'] != nil
          if data_hash['cdb_synset']['wn_equivalence_relations']['relation'].is_a?(Array)
            data_hash['cdb_synset']['wn_equivalence_relations']['relation'].delete_if{|rel| rel['@target20'].to_s == '' and rel['@target30'].to_s == ''}
            data_hash['cdb_synset']['wn_equivalence_relations']['relation'].each{|rel|
              rel['author'] = {} if rel['author'].nil?
              if rel['author']['@name'].to_s == ''
                rel['author']['@name'] = user
              end
            }
          end
        end
        
        #save
        $stderr.puts "DATA"
        $stderr.puts data_hash.to_json
        #xml = CobraVsMongoose.hash_to_xml(@wn.delete_previewtext(data_hash))
        xml = CobraVsMongoose.hash_to_xml(data_hash)
        $stderr.puts "follow_previewtext"
        xml = @wn.follow_previewtext(xml, @wn_array)
        $stderr.puts "save_rilr_links"
        xml = @wn.save_rilr_links(xml, @wn_array)
        $stderr.puts "follow_redundant"
        xml = @wn.follow_redundant(xml, @wn_array[@wn.dict_prefix+'cdb_red'])
        @wn.update(id, xml.to_s)
        $stderr.puts "EDIT Synset ID " + id + ' END ' + time_stamp()
        response['Content-Type'] = 'text/plain'
        response.body = {'action'=>'saved', 'reload'=>['cdbsyn1', 'cdbid1'], 'id'=>id}.to_json
      rescue => e
        response['Content-Type'] = 'text/plain'
        $stderr.puts e.message
        $stderr.puts e.backtrace.join("\n")
        response.body = {'action'=>'error', 'message'=>"exception #{e.class.to_s}: #{e.message}"}.to_json
      end

    #delete synset
    when 'delete'
      id = request.query['id']
      $stderr.puts "DELETE SYNSET #{id}"
      if id.to_s == ''
        response['Content-Type'] = 'text/plain'
        response.body = {'action'=>'error', 'message'=>"empty id"}.to_json
        return response
      end
      check = @wn.check_lock(dictionary, id, Thread.current[:auth_user])
      if not(check == true)
        response['Content-Type'] = 'text/plain'
        response.body = check.to_json
        return
      end
      begin
        @wn.delete(id)
        #@wn_array[@wn.dict_prefix+'cdb_id'].delete_synset(id)
        response['Content-Type'] = 'text/plain'
        response.body = {'action'=>'deleted', 'reload'=>['cdbsyn1', 'cdbid1']}.to_json
      rescue => e
        response['Content-Type'] = 'text/plain'
        $stderr.puts e.message
        $stderr.puts e.backtrace.join("\n")
        response.body = {'action'=>'error', 'message'=>"exception #{e.class.to_s}: #{e.message}"}.to_json
      end

    when 'newid'
      newid = @server.info.get_next_id(dictionary)
      response["Content-type"] = "text/plain; charset=utf-8"
      response.body = newid.to_s
    when 'check'
      logfile = '/var/log/deb-server/csyn_check-' + Time.now.strftime('%Y%m%d-%H%M') +'.log'
      log = File.new(logfile, 'w')

      @wn.container.each {|doc|
        xml = REXML::Document.new( doc.to_s )
        sy_id = xml.root.attributes['c_sy_id'].to_s
        log.puts "SYNSET=#{sy_id}"
        xml.elements.each('//synonyms/synonym') { |el|
          lu_id = el.attributes['c_lu_id'].to_s
          begin
            @wn_array[@wn.dict_prefix+'cdb_lu'].get(lu_id)
            log.puts " LU ok: #{lu_id}"
          rescue
            log.puts " LU not found: #{lu_id}"
          end
        }
        xml.elements.each('//wn_internal_relations/relation') { |el|
          rel_id = el.attributes['target'].to_s
          begin
            @wn.get(rel_id)
            log.puts " Relation ok, type #{el.attributes['relation_name'].to_s}, target #{rel_id}"
          rescue
            log.puts " Relation not found, type #{el.attributes['relation_name'].to_s}, target #{rel_id}"
          end
        }
      }
      log.close
      response["Content-type"] = "text/plain; charset=utf-8"
      response.body = "done"
      
    when 'lock'
      id = request.query['id']
      begin
    	  res = @wn.get(id).to_s
        check = @wn.check_lock(dictionary, id, Thread.current[:auth_user])
        if check == true
          time = Time.now.to_i        
          @wn.lock(dictionary, id, Thread.current[:auth_user], time) #lock actual synset
          doc = REXML::Document.new(res)
          #lock synonyms
          REXML::XPath.each(doc, '/cdb_synset/synonyms/synonym'){|el|
            lu_id = el.attributes['c_lu_id'].to_s
            $stderr.puts lu_id
            @wn.lock(@wn.dict_prefix+'cdb_lu', lu_id,  Thread.current[:auth_user], time)
          }
          #lock relations
          REXML::XPath.each(doc, '/cdb_synset/wn_internal_relations/relation'){|el|
            sy_id = el.attributes['target'].to_s
            $stderr.puts sy_id
            @wn.lock(@wn.dict_prefix+'cdb_syn', sy_id,  Thread.current[:auth_user], time)
          }
          status = {'action' => 'locked'}
        else
          status = check
        end
      rescue => e
        status = {'error' => e.to_s}
      end
      response["Content-type"] = "text/plain; charset=utf-8"
      response.body = status.to_json
    when 'unlock'
      id = request.query['id']
      @wn.unlock_time(dictionary, id, Thread.current[:auth_user])
      response["Content-type"] = "text/plain; charset=utf-8"
      response.body = {'action' => 'unlocked'}.to_json
    when 'recount'
      childs = @wn.recount_childs
      response.body = childs.size.to_s
      response.body
    when 'add_onto'
      source_xml = '/var/lib/deb-server/additionImportBasedOnWN16mapping_july_16_2007.xml'
      entry = ''
      IO.foreach(source_xml) {|line|
        if line =~ /<cdb_synset[ >\/]/
          #start new
          entry = line
        else
          entry += line
        end
        if line =~ /<\/cdb_synset>/ or line =~ /<cdb_synset [^<]*\/>/
          #parse, save
          if entry != ''
            #parse start
            doc = REXML::Document.new(entry)
            sy_id = doc.root.attributes['c_sy_id']
            begin
              orig = @wn.get(sy_id)
              origdoc = REXML::Document.new(orig.to_s)
              #$stderr.puts "**********************"
              #$stderr.puts entry
              #$stderr.puts "******"
              #$stderr.puts orig
              #$stderr.puts "******"
              origdoc.root.elements['wn_equivalence_relations'] = doc.root.elements['wn_equivalence_relations'] if doc.root.elements['wn_equivalence_relations'] != nil
              origdoc.root.elements['wn_domains'] = doc.root.elements['wn_domains'] if doc.root.elements['wn_domains'] != nil
              origdoc.elements.to_a("//ont_relation[@name='dwn10_pwn15_pwn20_mapping']").each{|el|
                origdoc.root.elements['sumo_relations'].delete_element el
              }
              origdoc.elements.to_a("//ont_relation[@name='dwn10_pwn16_pwn20_mapping']").each{|el|
                origdoc.root.elements['sumo_relations'].delete_element el
              }
              if origdoc.root.elements['sumo_relations'] == nil
                origdoc.root.add_element 'sumo_relations'
              end
              doc.elements.to_a("//ont_relation").each{|el|
                origdoc.root.elements['sumo_relations'].add_element el
              }
              @wn.update(sy_id, origdoc.to_s)
              $stderr.puts sy_id + ' done'
              response.body += sy_id + " done\n"
            rescue => e
              $stderr.puts e
              $stderr.puts sy_id + " not found\n"
              response.body += sy_id + " not found\n"
            end
            #parse end
            entry = ''
          end
        end
      }
      response.body

    when 'map'
      lu_id = request.query['lu_id'].to_s
      syn_id = request.query['syn_id'].to_s
      if lu_id == '' or syn_id == ''
        response.body = {'status'=>'error', 'error'=>'no LUID or SYNID'}.to_json
        response['Content-Type'] = 'application/json'
      else
        #add synonym to synset
        res = @wn_array[@wn.dict_prefix+'cdb_syn'].add_synonym(syn_id, lu_id) 
        #get LU form, just for sure
        form, seq_nr, pos = @wn_array[@wn.dict_prefix+'cdb_lu'].get_form_seq(lu_id) 
        #update synset preview
        @wn_array[@wn.dict_prefix+'cdb_lu'].update_synset_preview(@wn_array[@wn.dict_prefix+'cdb_syn'], lu_id, form, seq_nr)
        if res == true
          response.body = {'status'=>'ok'}.to_json
          response['Content-Type'] = 'application/json'
        else
          response.body = {'status'=>'error', 'error'=>res}.to_json
          response['Content-Type'] = 'application/json'
        end
      end

      
    else
      super(request, response)
    end
  end
  
  alias do_POST do_GET 

  def runQuery(dictionary, query, type, arg, dictcode, default='', expand='', tree_details = 0, display_only = '', reldics=[])
    if not @server.info.check_perm(Thread.current[:auth_user], @wn.dict_prefix+'cdb_syn')
      return nil
    end

    begin
    	resxml = dictionary.get(query)
    rescue
      return nil
    end
  	$stderr.puts resxml
    res = resxml.to_s.gsub('has_hyperonym', 'HAS_HYPERONYM')
  	case type
  	when 'xml'
      #res = @wn.get_rilr(res, @wn_array)
      res = @wn.strip_redundant(res, true)
      param = {'slovnik'=>dictcode.to_s}
      #res = dictionary.apply_transform( 'xml', res, [ [ 'slovnik', '"'+dictcode.to_s+'"'], [ 'servername', '"'+@servername.to_s+'"' ] ] )
      res = dictionary.apply_transform('xml', res, param )
  		return res
  	when 'html'
      transform = 'preview'
      transform += arg if arg.to_s != ''
      #res = @wn.follow_previewtext(res,  @wn_array, true)
      #res = @wn.get_rilr(res, @wn_array)
      #res = @wn.follow_redundant(res, @wn_array['cdb_id'], @wn_array['cdb_red']) 
      res = @wn.strip_redundant(res, true)
      #res = dictionary.apply_transform( transform, res, [ [ 'slovnik', '"'+dictcode.to_s+'"'], [ 'servername', '"'+@servername.to_s+'"' ], ['default', '"'+default.to_s+'"'] ] )
      param = {'dictcode'=>'"'+dictcode.to_s+'"'}
      param['default'] = default.to_s unless default.to_s == ''
      res = dictionary.apply_transform(transform, res, param)
  		return res
    when 'plain'
      #res = @wn.get_rilr(res, @wn_array)
      #res = @wn.follow_redundant(res, @wn_array['cdb_id'], @wn_array['cdb_red']) 
      res = @wn.strip_redundant(res, true)
      return res
  	when 'tree'
  		array = []
  		if arg == nil or arg == '' or arg == 'ILR'
    		arg = 'HAS_HYPERONYM'
      else
        arg = 'HAS_HYPONYM'
  		end
      #res = @wn.get_rilr(res, @wn_array)
  		tree = dictionary.get_tree(res, @wn_array, arg)
  		tree.each {|ar|
  			array << {'parent' => ar[0], 'id' => ar[1], 'val' => ar[2]}
  		}
      array.reverse!
  		return array
  	when 'editor'
      check = dictionary.check_lock(dictcode, query, Thread.current[:auth_user])
      res = @wn.strip_generated(res)
      hash = CobraVsMongoose.xml_to_hash(res)
      if hash['cdb_synset'] != nil and  hash['cdb_synset']['wn_internal_relations'] != nil and hash['cdb_synset']['wn_internal_relations']['relation'].is_a?(Array)
        hash['cdb_synset']['wn_internal_relations']['relation'].sort!{|x,y|
          (x['@relation_name'].to_s == 'HAS_HYPERONYM')? -1 : (x['@relation_name'].to_s<=>y['@relation_name'].to_s)
        }
      end
      begin
        if hash['cdb_synset']['kont_relations']['ont_relation'].is_a?(Hash)
          temp = hash['cdb_synset']['kont_relations']['ont_relation']
          hash['cdb_synset']['kont_relations']['ont_relation'] = [temp]
        end
      rescue
      end
      begin
        if hash['cdb_synset']['sumo_relations']['ont_relation'].is_a?(Hash)
          temp = hash['cdb_synset']['sumo_relations']['ont_relation']
          hash['cdb_synset']['sumo_relations']['ont_relation'] = [temp]
        end
      rescue
      end
      begin
        if hash['cdb_synset']['wn_domains']['dom_relation'].is_a?(Hash)
          temp = hash['cdb_synset']['wn_domains']['dom_relation']
          hash['cdb_synset']['wn_domains']['dom_relation'] = [temp]
        end
      rescue
      end
      begin
        if hash['cdb_synset']['vlis_domains']['dom_relation'].is_a?(Hash)
          temp = hash['cdb_synset']['vlis_domains']['dom_relation']
          hash['cdb_synset']['vlis_domains']['dom_relation'] = [temp]
        end
      rescue
      end
      begin
        if hash['cdb_synset']['wn_equivalence_relations']['relation'].is_a?(Hash)
          temp = hash['cdb_synset']['wn_equivalence_relations']['relation']
          hash['cdb_synset']['wn_equivalence_relations']['relation'] = [temp]
        end
      rescue
      end
      begin
        if hash['cdb_synset']['wn_internal_relations']['relation'].is_a?(Hash)
          temp = hash['cdb_synset']['wn_internal_relations']['relation']
          hash['cdb_synset']['wn_internal_relations']['relation'] = [temp]
        end
      rescue
      end
      begin
        if hash['cdb_synset']['synonyms']['synonym'].is_a?(Hash)
          temp = hash['cdb_synset']['synonyms']['synonym']
          hash['cdb_synset']['synonyms']['synonym'] = [temp]
        end
      rescue
      end
      return hash
  	end
  end

end
