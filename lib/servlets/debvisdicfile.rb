class DebvisdicFileHandler < WEBrick::HTTPServlet::FileHandler
  def service(req, res)
    if req.path.include?('/dictionaries/')
      unless File.file?(@root + req.path_info)
        servername = 'https://' + req['Host']
        res.set_redirect(WEBrick::HTTPStatus::SeeOther, servername + '/editor/dictionaries/wndefault.js')
      end
    end
    super(req, res)
  end
end
