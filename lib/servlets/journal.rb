require 'webrick'
require 'cobravsmongoose'
require 'template'
require 'servlets/admin'

class JournalServlet < WEBrick::HTTPServlet::AbstractServlet

  def initialize(server, journaldict, template_path)
    @journaldict = journaldict
    @template_path = template_path
  end

  def do_GET(request, response)
    res = ''
    response['Content-type'] = "text/html; charset=utf-8"
    #reload = (request.query['reload'].to_i > 0)? '<meta http-equiv="Refresh" content="' + request.query['reload'].to_s + '"/>' : ''
    #@journaldict.list_dictfiles(nil).each{|code,info|
    #  res += info + '<br/>'
    #}

    filt_limit = 20
    filt_start = 0
    filt_user = request.query['user'].to_s if request.query['user'].to_s != ''
    filt_dict = request.query['dict'].to_s if request.query['dict'].to_s != ''
    filt_key = request.query['key'].to_s if request.query['key'].to_s != ''
    filt_limit = request.query['limit'].to_i if request.query['limit'].to_s != ''
    filt_start = request.query['start'].to_i if request.query['start'].to_s != ''
    filt_date = request.query['date'].to_s if request.query['date'].to_s != ''

    date_match = /([0-9][0-9][0-9][0-9])(-[01][0-9])?(-[0-3][0-9])?/.match(filt_date)
    if date_match != nil
      filt_date_format = '%Y'
      filt_date_format = '%Y-%m' if date_match[2] != nil
      filt_date_format = '%Y-%m-%d' if date_match[3] != nil
    end
    
    i = 0
    entry_ar = []
    @journaldict.update_hash.sort{|x,y| y <=> x}.each{|time,ar|
      ar.each{|info|
        if (filt_user == nil or filt_user == info['user']) and (filt_dict == nil or filt_dict == info['dictionary']) and (filt_key == nil or filt_key == info['key']) and (filt_date == nil or (filt_date_format != nil and filt_date.to_s == Time.at(time).strftime(filt_date_format)))
          i += 1
          next if i <= filt_start
          entry_ar << {
            'cssclass' => 'row' + (i%2).to_s,
            'user' => info['user'],
            'time' => Time.at(time).strftime("%Y-%m-%d %H:%M:%S"),
            'dictionary' => info['dictionary'],
            'action' => info['action'],
            'thread' => info['thread'],
            'key' => info['key'],
            'i' => i,
            'xml' => CGI::escapeHTML(info['xmldoc'].to_s)
          }
        end
      }
      break if i >= filt_start + filt_limit
    }

    users = []
    keys = []
    dicts = []
    @journaldict.journal_hash.each{|time,ar|
      ar.each{|info|
        users << info['user']
        keys << info['key']
        dicts << info['dictionary']
      }
    }
    users = users.uniq.sort
    keys = keys.uniq.sort
    dicts = dicts.uniq.sort
    users.map!{|x| 
      if (filt_user != nil and x==filt_user)
        {'user'=>x, 'selected'=>'selected'}
      else
        {'user'=>x}
      end
    }
    keys.map!{|x| 
      if (filt_key != nil and x==filt_key)
        {'key'=>x, 'selected'=>'selected'}
      else
        {'key'=>x}
      end
    }
    dicts.map!{|x| 
      if (filt_dict != nil and x==filt_dict)
        {'dict'=>x, 'selected'=>'selected'}
      else
        {'dict'=>x}
      end
    }

    limits = ['10','20','30','50']
    limits.map!{|x|
      if (filt_limit != nil and x==filt_limit.to_s)
        {'limit'=>x, 'selected'=>'selected'}
      else
        {'limit'=>x}
      end
    }
    
    template_data = {
      'entries'=>entry_ar, 
      'users'=>users, 
      'keys'=>keys, 
      'dicts'=>dicts,
      'limits'=>limits,
      'filt_key'=>filt_key,
      'filt_user'=>filt_user,
      'filt_dict'=>filt_dict,
      'filt_limit'=>filt_limit,
      'filt_date'=>filt_date
    }
    template_data['filt_next'] = (filt_start + filt_limit).to_s
    template_data['filt_prev'] = (filt_start - filt_limit).to_s if filt_start - filt_limit >= 0
    
    response.body = HTMLTemplate.new(template_data).output(@template_path + '/journal.tmpl')
  end

end
