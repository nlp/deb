require 'template'
require 'cobravsmongoose'
#require 'unicode'
require 'json'

class DafnServlet < DictServlet

  def initialize( server, dict, array )
    super
    @servername = nil
  end

  def do_GET(request, response)
    @servername = get_servername(@server, request) if @servername == nil
    
    user = request.attributes[:auth_user].to_s
    #$stderr.puts @dict.get_perm(user)
    $stderr.puts @server.info.service_name
    if request.query['action'] == nil or request.query['action'].to_s == ''
      request.query['action'] = 'main'
    end
    if request.query['action'] == 'list' and request.query['search'].to_s == ''
      request.query['action'] = 'main'
    end

    case request.query['action']
    when 'main'
      response['Content-Type'] = 'text/html; charset=utf8'
      response.body = HTMLTemplate.new().output(@dict.template_path + '/main.tmpl')
    when 'list'
      entries = @dict.get_list(request.query['search'].to_s)
      entries.sort!{|a, b| a['name']<=>b['name']}
      tmpl_params = {'entries'=>entries, 'search'=>request.query['search'].to_s, 'count'=>entries.size}
      response.body = HTMLTemplate.new(tmpl_params).output(@dict.template_path + '/list.tmpl')
      response['Content-Type'] = 'text/html; charset=utf-8'
    when 'save'
      data_hash = JSON.parse(request.query['data'])
      $stderr.puts data_hash
      if (request.query['id'].to_s == '')
        id = data_hash['entry']['name']['$']
      else
        id = request.query['id'].to_s
      end
      begin
        xml = @dict.get(id).to_s
        orig_hash = CobraVsMongoose.xml_to_hash(xml)
        data_hash['entry']['metadata'] = orig_hash['entry']['metadata']
        raise 'nil' if orig_hash['entry']['metadata'].nil?
      rescue
        $stderr.puts 'rescue'
        data_hash['entry']['metadata'] = {}
        data_hash['entry']['metadata']['author'] = {'$'=>user}
        data_hash['entry']['metadata']['history'] = {'changes'=>[]}
        data_hash['entry']['metadata']['history']['changes'] << {'@user'=>user, '@time'=>Time.now.strftime("%Y-%m-%d %H:%M")}
      end
      data_hash['entry']['metadata']['history']['changes'] << {'@user'=>user, '@time'=>Time.now.strftime("%Y-%m-%d %H:%M")}
      xmldata = CobraVsMongoose.hash_to_xml(data_hash)
      $stderr.puts xmldata
      xmldata.gsub!('&lt;i&gt;', '<i>')
      xmldata.gsub!('&lt;/i&gt;', '</i>')
      xmldata.gsub!('&lt;bi&gt;', '<bi>')
      xmldata.gsub!('&lt;/bi&gt;', '</bi>')
      xmldata.gsub!('&lt;b&gt;', '<b>')
      xmldata.gsub!('&lt;/b&gt;', '</b>')
      xmldata.gsub!(/&lt;xref refurl=&quot;([0-9]*)&quot;&gt;/, '<xref refurl="\1">')
      xmldata.gsub!('&lt;/xref&gt;', '</xref>')
      xmldata.gsub!('&lt;xref&gt;', '<xref>')
      xmldata.gsub!('&lt;form&gt;', '<form>')
      xmldata.gsub!('&lt;/form&gt;', '</form>')
      xmldata.gsub!('&lt;forename&gt;', '<forename>')
      xmldata.gsub!('&lt;/forename&gt;', '</forename>')
      xmldata.gsub!('&lt;annotation&gt;', '<annotation>')
      xmldata.gsub!('&lt;/annotation&gt;', '</annotation>')
      xmldata.gsub!('&lt;regionalStats&gt;', '<regionalStats>')
      xmldata.gsub!('&lt;/regionalStats&gt;', '</regionalStats>')
      xmldata.gsub!('&lt;in&gt;', '<in>')
      xmldata.gsub!('&lt;/in&gt;', '</in>')
      xmldata.gsub!('&lt;ini&gt;', '<ini>')
      xmldata.gsub!('&lt;/ini&gt;', '</ini>')
      xmldata.gsub!('&lt;su&gt;', '<su>')
      xmldata.gsub!('&lt;/su&gt;', '</su>')
      xmldata.gsub!('&lt;sui&gt;', '<sui>')
      xmldata.gsub!('&lt;/sui&gt;', '</sui>')
      xmldata.gsub!('&lt;sc&gt;', '<sc>')
      xmldata.gsub!('&lt;/sc&gt;', '</sc>')
      xmldata.gsub!('&lt;fr&gt;', '<fr>')
      xmldata.gsub!('&lt;/fr&gt;', '</fr>')
      $stderr.puts xmldata

      @dict.update(id, xmldata)
      response.body = '{"success":true,"msg":"Saved"}'
      response['Content-Type'] = 'application/json; charset=utf-8'
    when 'get'
      if request.query['id'].to_s == ''
        response.body = '{"success":false, "msg":"No ID"}'
        response['Content-Type'] = 'application/json; charset=utf-8'
      else
        begin
          xml = @dict.get_for_edit(request.query['id'].to_s).to_s
          data_hash = CobraVsMongoose.xml_to_hash(xml)
          response.body = data_hash.to_json
          response['Content-Type'] = 'application/json; charset=utf-8'
        rescue => e
          $stderr.puts e.message
          $stderr.puts e.backtrace.join("\n")
          response.body = '{"success":false, "msg":"Error while loading: '+e.to_s+'"}'
          response['Content-Type'] = 'application/json; charset=utf-8'
        end
      end
    when 'preview'
      id = request.query['id'].to_s
      xml = @dict.get(id)
      response['Content-Type'] = 'text/html; charset=utf8'
      response.body = @dict.apply_transform('preview', xml.to_s)

    else
      super(request, response)
    end
  end
  
  alias do_POST do_GET
end
