require 'template'
require 'cobravsmongoose'
require 'json'
require 'tmpdir'
require 'uri'
require 'iconv'
require 'csv'
require 'cgi'
require 'uuid'

class CZJAdminServlet < DictServlet

  def initialize( server, dict, array )
    super
    @servername = nil
  end

  def checkadmin(user)
    return false if user == ''
    if @server[:Authenticator].userdb.check_permissions(user, 'admin', true)
      return true
    else
      return false
    end
  end

  def check_perm(user)
    return 'ro' if user == ''
    perm_info = @dict_array['czj'].get_perm(user, @dict_array['czj_user'])
    if perm_info.is_a?(Array)
      perm = perm_info.join(',')
    else
      perm = perm_info
    end
    return perm
  end

  def norm_name(name)
    name = name.sub('.flv','')
    name = name.sub('.mp4','')
    sense = ''
    name.sub!(/^[ABDKabdk][-_]/,'')
    var = ''
    var = 'A' if name.include?('_1_FLV_HQ')
    var = 'B' if name.include?('_2_FLV_HQ')
    var = 'C' if name.include?('_3_FLV_HQ')
    var = 'D' if name.include?('_4_FLV_HQ')
    var = 'E' if name.include?('_5_FLV_HQ')
    var = 'F' if name.include?('_6_FLV_HQ')
    name.sub!('_1_FLV_HQ','')
    name.sub!('_2_FLV_HQ','')
    name.sub!('_3_FLV_HQ','')
    name.sub!('_4_FLV_HQ','')
    name.sub!('_5_FLV_HQ','')
    name.sub!('_6_FLV_HQ','')
    name.gsub!('FLV_HQ','')
    if name =~ /_[1-9]$/
      var += name[/_([1-9])$/,1]
      name.sub!(/_[1-9]$/,'')
    end
    var = 'A' if name =~ /_I$/
    var = 'B' if name =~ /_II$/
    var = 'C' if name =~ /_III$/
    var = 'D' if name =~ /_IV$/
    name.sub!(/_I$/,'')
    name.sub!(/_II$/,'')
    name.sub!(/_III$/,'')
    name.sub!(/_IV$/,'')
    name.sub!('A-C1','A1')
    name.sub!('A-C-D','A1d')
    if name =~ /[A-Z]$/ and not name =~ /[A-Z][A-Z]$/
      var = name[-1,1]
      name = name[0..-2]
    end
    if name =~ /[A-Z][1-9]$/
      var = name[-2,1]
      sense = name[-1,1]
      name = name[0..-3]
    end
    if name =~ /[A-Z][1-9][a-z]$/
      var = name[-3,3]
      name = name[0..-4]
    end
    if name =~ /[0-9][a-z]$/
      var = (name[/([0-9])[a-z]$/,1].to_i+64).chr + (name[/[0-9]([a-z])$/,1][0]-96).to_s
      name = name[0..-3]
    end
    if name =~ /[a-z][0-9]$/
      var = (name[/([0-9])$/,1].to_i+64).chr
      name = name[0..-2]
    end
    var = 'A' if var == ''
    name.gsub!(/([a-z])([A-Z])/, '\1-\2')
    name.gsub!('_','-')
    name.gsub!('(','')
    name.gsub!(')','')
    name.downcase!
    return name+'=='+var, sense
  end

  def handle_upload(filedata, dir)
    if not filedata.nil? and not filedata.filename.nil? and filedata.filename != ''
      filepath = dir + '/' + filedata.filename
      $stderr.puts filepath
      f = File.new(filepath, 'w')
      f.syswrite(filedata)
      f.chmod(0664)
      f.close
      #zip?
      if filedata.filename[-4..-1] == '.zip'
        system('unzip ' + filepath + ' -d ' + dir)
        File.delete(filepath)
      end
    end
  end

  def import_run(request, logid)
      logname = '/var/log/deb-server/czjimport'+logid+'.log'
      logfile = File.open(logname, 'w')
      targetdict = request.query['targetdict'].to_s
      transdict = request.query['transdict'].to_s
      skupina_obor = {}
      skupina_obor['biologie'] = 28
      skupina_obor['informatika'] = 6
      skupina_obor['matematika'] = 27
      skupina_obor['podnikani'] = 14
      zalozitpreklad = false
      newcjlemma = []
      cjhesla = {}
      update = {}
      update['cs'] = []
      update['czj'] = []
      update['asl'] = []
      update['is'] = []
      update['en'] = []
      update['sj'] = []
      update['spj'] = []

      i = 0
      while i < request.query['count'].to_i
        request.query['v'+i.to_s+'_newentry'] = request.query['v'+i.to_s+'_newentry'].to_s.gsub('<','&lt;').gsub('"','')
        request.query['v'+i.to_s+'_label'] = request.query['v'+i.to_s+'_label'].to_s.gsub('<','&lt;').gsub('"','')
        request.query['v'+i.to_s+'_zdroj'] = request.query['v'+i.to_s+'_zdroj'].to_s.gsub('<','&lt;').gsub('"','')
        request.query['v'+i.to_s+'_autor'] = request.query['v'+i.to_s+'_autor'].to_s.gsub('<','&lt;').gsub('"','')
        request.query['v'+i.to_s+'_copy'] = request.query['v'+i.to_s+'_copy'].to_s.gsub('<','&lt;').gsub('"','')
        request.query['v'+i.to_s+'_tran'] = request.query['v'+i.to_s+'_tran'].to_s.gsub('<','&lt;').gsub('"','')
        request.query['v'+i.to_s+'_assignset'] = request.query['v'+i.to_s+'_assignset'].to_s.gsub('<','&lt;').gsub('"','')
        i += 1
      end

      if request.query['zalozitpreklad'].to_s == 'on'
        zalozitpreklad = true
        i = 0
        while i < request.query['count'].to_i
          tran = request.query['v'+i.to_s+'_tran'].to_s
          if cjhesla[tran].nil? and tran != ''and request.query['v'+i.to_s+'_assign'].to_s != 'ign' and (request.query['v'+i.to_s+'_mediaexist'].to_s == 'newlemma' or request.query['v'+i.to_s+'_newentry'].to_s != '')
            #zalozit nove CJ heslo
            cjid = @server.info.get_next_id(transdict).to_s
            xml = '<entry id="'+cjid.to_s+'">'
            xml += '<lemma id="'+cjid.to_s+'">'
            xml += '<completeness>0</completeness>'
            xml += '<pracskupina>'+request.query['v'+i.to_s+'_skup'].to_s+'</pracskupina>'
            xml += '<created_at>'+Time.now.strftime("%Y-%m-%d %H:%M:%S")+'</created_at>'
            xml += '<id>'+cjid.to_s+'</id>'
            xml += '<title>'+tran+'</title>'
            xml += '</lemma>'
            xml += '<meanings><meaning id="'+cjid.to_s+'-1" number="1">'
            xml += '<relation type="translation" target="'+targetdict+'" meaning_id="'+request.query['v'+i.to_s+'_newentry'].to_s+'-1"/>' if request.query['v'+i.to_s+'_newentry'].to_s != ''
            xml += '</meaning></meanings>'
            xml += '</entry>'
            $stderr.puts xml
            newcjlemma << transdict + '_' + cjid + '_' + URI.escape(tran)
            ##save
            @dict_array[transdict].update(cjid, xml)
            cjhesla[tran] = cjid
            update[transdict] << cjid
          end
          i += 1
        end
      end

      if request.query['count'].to_i > 0 and request.query['dir'].to_s != ''
        i = 0
        newlemma = []
        newdef = []
        newusage = []
        updlemma = []
        while i < request.query['count'].to_i
          $stderr.puts request.query['v'+i.to_s+'_file']
          $stderr.puts request.query['v'+i.to_s+'_fileexist'].to_s
          $stderr.puts request.query['v'+i.to_s+'_mediaexist'].to_s
          logfile.puts request.query['v'+i.to_s+'_file'].to_s
          label = request.query['v'+i.to_s+'_label'].to_s
          #ignorovat
          if request.query['v'+i.to_s+'_assign'].to_s == 'ign'
            $stderr.puts 'ignore '+i.to_s
            i += 1
            next
          end
          #neprepisovat soubor
          unless request.query['v'+i.to_s+'_fileexist'].to_s == 'ignore'
            FileUtils.cp(request.query['dir'].to_s+'/'+request.query['v'+i.to_s+'_file'].to_s, '/var/lib/deb-server/files/czj/media/video'+targetdict)
            #MP4
            command1 = '/var/lib/deb-server/files/czj/editor/mkthumb.sh '+request.query['v'+i.to_s+'_file'].to_s+' '+targetdict
            filepath = '/var/lib/deb-server/files/czj/media/video'+targetdict+'/'+request.query['v'+i.to_s+'_file'].to_s
            command2 = 'ffmpeg -y -i ' + filepath + ' -vcodec libvpx -acodec libvorbis ' + filepath + '.webm'
            command3 = 'qt-faststart '+filepath+' '+filepath+'_new; if [ -e '+filepath+'_new ]; then mv -f '+filepath+'_new '+filepath+'; fi;'
            Thread.new {
              $stderr.puts command1
              system(command1)
              $stderr.puts command2
              system(command2)
              $stderr.puts command3
              system(command3)
            }
          end
          #nevytvaret zaznam
          if request.query['v'+i.to_s+'_mediaexist'].to_s != 'ign'
            mediaid = ''
            entryfolders = []
            if request.query['v'+i.to_s+'_mediaexist'].to_s == 'overwrite' or @dict_array[targetdict+'_media'].xquery_to_hash('[media[location="'+request.query['v'+i.to_s+'_file']+'"]]').size > 0
              oldfile = '__'
              oldfile = request.query['v'+i.to_s+'_oldfile'] unless request.query['v'+i.to_s+'_oldfile'].to_s == ''
              @dict_array[targetdict+'_media'].xquery_to_hash('[media[location="'+request.query['v'+i.to_s+'_file']+'" or original_file_name="'+request.query['v'+i.to_s+'_file']+'" or location="'+oldfile+'"]]').each{|mi,mx|
                mediaid = mi
                mdoc = @dict.load_xml_string(mx)
                mdoc.find('/media/entry_folder').each{|ef|
                  entryfolders << ef.content.to_s
                }

                #aktualizovat pro bocni/celni video odkaz v heslech na novy nazev
                #if request.query['v'+i.to_s+'_type'].to_s == 'sign_front' or request.query['v'+i.to_s+'_type'].to_s == 'sign_side'
                #  @dict_array[targetdict].xquery_to_hash('[entry[media/file/label="'+request.query['v'+i.to_s+'_label'].to_s+'"]]').each{|li,lv|
                #    if request.query['v'+i.to_s+'_type'].to_s == 'sign_front'
                #      tag = 'video_front'
                #    else
                #      tag = 'video_side'
                #    end
                #    lvdoc = @dict.load_xml_string(lv)
                #    lvdoc.find('//'+tag).each{|vt| vt.remove!}
                #    lvdoc.find('/entry/lemma').first << XML::Node.new(tag, request.query['v'+i.to_s+'_file'].to_s)
                #    @dict_array[targetdict].update(li,lvdoc.to_s)
                #    updlemma << li + '_' + label
                #    update[targetdict] << li
                #  }
                #end
              }
            else
              mediaid = @server.info.get_next_id(targetdict+'_media').to_s
            end

            filexml = "<media id='#{mediaid}'>"
            if request.query['v'+i.to_s+'_label'].is_a?(Array) 
              nlabel = request.query['v'+i.to_s+'_label'][0].to_s
            else
              nlabel = request.query['v'+i.to_s+'_label'].to_s
            end
            entryfolders.each{|ef| filexml += '<entry_folder>'+ef+'</entry_folder>'}
            filexml += "<location>#{request.query['v'+i.to_s+'_file'].to_s}</location>
    <original_file_name>#{request.query['v'+i.to_s+'_file'].to_s}</original_file_name>
    <id_meta_copyright>#{request.query['v'+i.to_s+'_copy'].to_s}</id_meta_copyright>
    <id_meta_author>#{request.query['v'+i.to_s+'_autor'].to_s}</id_meta_author>
    <id_meta_source>#{request.query['v'+i.to_s+'_zdroj'].to_s}</id_meta_source>
    <type>#{request.query['v'+i.to_s+'_type'].to_s}</type>
    <label>#{nlabel}</label>
    <status>#{request.query['video_status'].to_s}</status>
    <created_at>#{Time.now.strftime("%Y-%m-%d %H:%M:%S")}</created_at>
    </media>"
            $stderr.puts filexml

            if request.query['v'+i.to_s+'_mediaexist'].to_s == 'newlemma'
              eid = @server.info.get_next_id(targetdict).to_s
              xml = '<entry id="'+eid.to_s+'">'
              xml += '<lemma id="'+eid.to_s+'">'
              xml += '<completeness>0</completeness>'
              xml += '<pracskupina>'+request.query['v'+i.to_s+'_skup'].to_s+'</pracskupina>'
              xml += '<created_at>'+Time.now.strftime("%Y-%m-%d %H:%M:%S")+'</created_at>'
              xml += '<video_front>'+request.query['v'+i.to_s+'_file'].to_s+'</video_front>' if request.query['v'+i.to_s+'_type'].to_s == 'sign_front'
              xml += '<video_side>'+request.query['v'+i.to_s+'_file'].to_s+'</video_side>' if request.query['v'+i.to_s+'_type'].to_s == 'sign_side'
              xml += '<id>'+eid.to_s+'</id>'
              xml += '</lemma>'
              xml += '<meanings><meaning id="'+eid.to_s+'-1" number="1"><category>'+skupina_obor[request.query['v'+i.to_s+'_skup'].to_s].to_s+'</category>'
              if request.query['v'+i.to_s+'_tran'].to_s != ''
                if cjhesla[request.query['v'+i.to_s+'_tran'].to_s].nil?
                  xml += '<relation type="translation" target="'+transdict+'" meaning_id="'+request.query['v'+i.to_s+'_tran'].to_s+'"/>'
                else
                  xml += '<relation type="translation" target="'+transdict+'" meaning_id="'+cjhesla[request.query['v'+i.to_s+'_tran'].to_s]+'-1"/>'
                end
              end
              xml += '</meaning></meanings>'
              xml += '</entry>'
              $stderr.puts xml
              newlemma << eid + '_' + label
              ##save
              @dict_array[targetdict].update(eid, xml)
              update[targetdict] << eid
            end

            if request.query['v'+i.to_s+'_mediaexist'].to_s == 'newdef'
              @dict_array[targetdict+'_media'].xquery_to_hash('[media[location="'+request.query['v'+i.to_s+'_file']+'" or original_file_name="'+request.query['v'+i.to_s+'_file']+'"]]').each{|mi,mx|
                @dict_array[targetdict].xquery_to_hash('[entry[.//file/@media_id="'+mi+'"]]').each{|eid,xml|
                  doc = @dict.load_xml_string(xml)
                  doc.root << XML::Node.new('meanings') if doc.find('//meanings').size == 0
                  maxm = 0
                  doc.find('//meaning').each{|me|
                    mn = me['id'].split('-')[1]
                    maxm = mn.to_i if mn.to_i > maxm
                  }
                  maxm += 1
                  nm = XML::Node.new('meaning')
                  nm['id'] = eid.to_s + '-' + maxm.to_s
                  nm['number'] = '1'
                  ntd = XML::Node.new('text')
                  nf = XML::Node.new('file')
                  nf['media_id'] = mi 
                  ntd << nf
                  nm << ntd
                  doc.find('//meanings').first << nm
                  $stderr.puts nm
                  newdef << eid + '_' + label
                  ##save
                  @dict_array[targetdict].update(eid, doc.to_s)
                  update[targetdict] << eid
                }
              }
            end
            if request.query['v'+i.to_s+'_mediaexist'].to_s == 'newusage'
              @dict_array[targetdict+'_media'].xquery_to_hash('[media[location="'+request.query['v'+i.to_s+'_file']+'" or original_file_name="'+request.query['v'+i.to_s+'_file']+'"]]').each{|mi,mx|
                @dict_array[targetdict].xquery_to_hash('[entry[.//file/@media_id="'+mi+'"]]').each{|eid,xml|
                  doc = @dict.load_xml_string(xml)
                  doc.find('//meaning[.//file/@media_id="'+mi+'"]').each{|me|
                    mu = me.find('usages/usage').size + 1
                    me << XML::Node.new('usages') if mu == 1
                    nu = XML::Node.new('usage')
                    nu['id'] = me['id'] + '-' + mu.to_s
                    ntd = XML::Node.new('text')
                    nf = XML::Node.new('file')
                    nf['media_id'] = mi 
                    ntd << nf
                    nu << ntd
                    me.find('usages').first << nu
                    $stderr.puts nu
                    newusage << eid + '_' + label
                  }
                  ##save
                  @dict_array[targetdict].update(eid, doc.to_s)
                  update[targetdict] << eid
                }
              }
            end

            if request.query['v'+i.to_s+'_mediaexist'].to_s == 'set' and request.query['v'+i.to_s+'_mediaset'].to_s != ''
              eids = request.query['v'+i.to_s+'_mediaset'].to_s 
              eids.split(',').each{|eid|
                enr = ''
                if eid.include?('-')
                  eid, enr = eid.strip.split('-')
                end
                if request.query['v'+i.to_s+'_sense'].to_s != '' and enr != ''
                  enr = request.query['v'+i.to_s+'_sense'].to_s
                end
                eid.strip!
                updlemma << eid + '_' + label 
                #update video
                vd = @dict.load_xml_string(filexml)
                vd.root << XML::Node.new('entry_folder', eid.to_s)
                filexml = vd.to_s
                $stderr.puts filexml
                #update entry
                exxml = @dict_array[targetdict].get(eid)
                if exxml != ''
                  exdoc = @dict.load_xml_string(exxml)
                  if request.query['v'+i.to_s+'_mediaset_gram'] == 'on' or request.query['v'+i.to_s+'_mediaset_styl'] == 'on' 
                    if request.query['v'+i.to_s+'_mediaset_gram'] == 'on'
                      exdoc.find('//lemma').first << XML::Node.new('grammar_note') if exdoc.find('//lemma/grammar_note').size == 0
                      exdoc.find('//lemma/grammar_note').first << XML::Node.new('variant', mediaid)
                    end
                    if request.query['v'+i.to_s+'_mediaset_styl'] == 'on'
                      exdoc.find('//lemma').first << XML::Node.new('style_note') if exdoc.find('//lemma/style_note').size == 0
                      exdoc.find('//lemma/style_note').first << XML::Node.new('variant', mediaid)
                    end
                  else
                    if request.query['v'+i.to_s+'_type'].to_s == 'sign_front'
                      exdoc.find('//lemma/video_front').each{|vf| vf.remove!}
                      exdoc.find('//lemma').first << XML::Node.new('video_front', request.query['v'+i.to_s+'_file'].to_s)
                    end
                    if request.query['v'+i.to_s+'_type'].to_s == 'sign_side'
                      exdoc.find('//lemma/video_side').each{|vf| vf.remove!}
                      exdoc.find('//lemma').first << XML::Node.new('video_side', request.query['v'+i.to_s+'_file'].to_s)
                    end
                    if request.query['v'+i.to_s+'_type'].to_s == 'sign_definition'
                      ntd = XML::Node.new('text')
                      nf = XML::Node.new('file')
                      nf['media_id'] = mediaid
                      ntd << nf
                      if enr == ''
                        if exdoc.find('//meaning').size == 0
                          nm = XML::Node.new('meaning')
                          nm['id'] = eid+'-1'
                          exdoc.find('//meanings').first << nm
                        end
                        exdoc.find('//meaning').first << ntd
                      else
                        if exdoc.find('//meaning[@number="'+enr+'"]').size == 0
                          mean_sense = XML::Node.new('meaning')
                          mean_sense['number'] = enr
                          mean_sense['id'] = eid+'-'+enr
                          exdoc.find('//meanings').first << mean_sense
                        else
                          mean_sense = exdoc.find('//meaning[@number="'+enr+'"]').first
                        end
                        mean_sense << ntd
                      end
                    end
                    if request.query['v'+i.to_s+'_type'].to_s == 'sign_usage_example'
                      nus = XML::Node.new('usages')
                      nu = XML::Node.new('usage')
                      nu['id'] = eid + '-1-1' 
                      ntd = XML::Node.new('text')
                      nf = XML::Node.new('file')
                      nf['media_id'] = mediaid
                      ntd << nf
                      nu << ntd
                      nus << nu
                      if enr == ''
                        if exdoc.find('//meaning').size == 0
                          nm = XML::Node.new('meaning')
                          nm['id'] = eid+'-1'
                          nm['number'] = '1'
                          exdoc.find('//meanings').first << nm
                        end
                        exdoc.find('//meaning').first << nus
                      else
                        if exdoc.find('//meaning[@number="'+enr+'"]').size == 0
                          mean_sense = XML::Node.new('meaning')
                          mean_sense['number'] = enr
                          mean_sense['id'] = eid+'-'+enr
                          exdoc.find('//meanings').first << mean_sense
                        else
                          mean_sense = exdoc.find('//meaning[@number="'+enr+'"]').first
                        end
                        mean_sense << nus
                      end

                    end
                  end
                  $stderr.puts exdoc.to_s
                  ##save
                  @dict_array[targetdict].update(eid, exdoc.to_s)
                  update[targetdict] << eid
                end
              }
            end

            if request.query['v'+i.to_s+'_assign'].to_s == '-'
              eid = @server.info.get_next_id(targetdict).to_s
              xml = '<entry id="'+eid.to_s+'">'
              xml += '<lemma id="'+eid.to_s+'">'
              xml += '<completeness>0</completeness>'
              xml += '<pracskupina>'+request.query['v'+i.to_s+'_skup'].to_s+'</pracskupina>'
              xml += '<created_at>'+Time.now.strftime("%Y-%m-%d %H:%M:%S")+'</created_at>'
              xml += '<video_front>'+request.query['v'+i.to_s+'_file'].to_s+'</video_front>' if request.query['v'+i.to_s+'_type'].to_s == 'sign_front'
              xml += '<video_side>'+request.query['v'+i.to_s+'_file'].to_s+'</video_side>' if request.query['v'+i.to_s+'_type'].to_s == 'sign_side'
              xml += '<id>'+eid.to_s+'</id>'
              xml += '</lemma>'
              xml += '<meanings><meaning id="'+eid.to_s+'-1" number="1"><category>'+skupina_obor[request.query['v'+i.to_s+'_skup'].to_s].to_s+'</category>'
              xml += '<text><file media_id="'+mediaid+'"/></text>' if request.query['v'+i.to_s+'_type'].to_s == 'sign_definition'
              xml += '<usages><usage id="'+eid.to_s+'-1-1"><text><file media_id="'+mediaid+'"/></text></usage></usages>' if request.query['v'+i.to_s+'_type'].to_s == 'sign_usage_example'
              if request.query['v'+i.to_s+'_tran'].to_s != ''
                if cjhesla[request.query['v'+i.to_s+'_tran'].to_s].nil?
                  xml += '<relation type="translation" target="'+transdict+'" meaning_id="'+request.query['v'+i.to_s+'_tran'].to_s+'"/>'
                else
                  xml += '<relation type="translation" target="'+transdict+'" meaning_id="'+cjhesla[request.query['v'+i.to_s+'_tran'].to_s]+'-1"/>'
                end
              end
              xml += '</meaning></meanings>'
              xml += '</entry>'
              $stderr.puts xml
              newlemma << eid + '_' + label
              vd = @dict.load_xml_string(filexml)
              vd.root << XML::Node.new('entry_folder', eid.to_s)
              filexml = vd.to_s
              $stderr.puts filexml
              ##save
              @dict_array[targetdict].update(eid, xml)
              update[targetdict] << eid
            end

            if request.query['v'+i.to_s+'_assign'].to_s != '' and request.query['v'+i.to_s+'_assign'].to_s != '-'
              eids = request.query['v'+i.to_s+'_assign'].to_s
              eids = request.query['v'+i.to_s+'_assignset'].to_s if request.query['v'+i.to_s+'_assign'].to_s == 'set'
              eids.split(',').each{|eid|
                enr = ''
                if eid.include?('-')
                  eid, enr = eid.strip.split('-')
                end
                if request.query['v'+i.to_s+'_sense'].to_s != '' and enr != ''
                  enr = request.query['v'+i.to_s+'_sense'].to_s
                end
                eid.strip!
                updlemma << eid + '_' + label 
                #update video
                vd = @dict.load_xml_string(filexml)
                vd.root << XML::Node.new('entry_folder', eid.to_s)
                filexml = vd.to_s
                $stderr.puts filexml
                #update entry
                exxml = @dict_array[targetdict].get(eid)
                if exxml.strip != ''
                  exdoc = @dict.load_xml_string(exxml)
                  $stderr.puts request.query['v'+i.to_s+'_assign_gram']
                  if request.query['v'+i.to_s+'_assign_gram'] == 'on' or request.query['v'+i.to_s+'_assign_styl'] == 'on' 
                    if request.query['v'+i.to_s+'_assign_gram'] == 'on'
                      exdoc.find('//lemma').first << XML::Node.new('grammar_note') if exdoc.find('//lemma/grammar_note').size == 0
                      exdoc.find('//lemma/grammar_note').first << XML::Node.new('variant', mediaid)
                    end
                    if request.query['v'+i.to_s+'_assign_styl'] == 'on'
                      exdoc.find('//lemma').first << XML::Node.new('style_note') if exdoc.find('//lemma/style_note').size == 0
                      exdoc.find('//lemma/style_note').first << XML::Node.new('variant', mediaid)
                    end
                  else
                    if request.query['v'+i.to_s+'_type'].to_s == 'sign_front'
                      exdoc.find('//lemma/video_front').each{|vf| vf.remove!}
                      exdoc.find('//lemma').first << XML::Node.new('video_front', request.query['v'+i.to_s+'_file'].to_s)
                    end
                    if request.query['v'+i.to_s+'_type'].to_s == 'sign_side'
                      exdoc.find('//lemma/video_side').each{|vf| vf.remove!}
                      exdoc.find('//lemma').first << XML::Node.new('video_side', request.query['v'+i.to_s+'_file'].to_s)
                    end
                    if request.query['v'+i.to_s+'_type'].to_s == 'sign_definition'
                      ntd = XML::Node.new('text')
                      nf = XML::Node.new('file')
                      nf['media_id'] = mediaid
                      ntd << nf
                      exdoc.find('//entry').first << XML::Node.new('meanings') if exdoc.find('//meanings').size == 0
                      if enr == ''
                        if exdoc.find('//meaning').size == 0
                          nm = XML::Node.new('meaning')
                          nm['id'] = eid+'-1'
                          nm['number'] = '1'
                          exdoc.find('//meanings').first << nm
                        end
                        exdoc.find('//meaning').first << ntd
                      else
                        if exdoc.find('//meaning[@number="'+enr+'"]').size == 0
                          mean_sense = XML::Node.new('meaning')
                          mean_sense['number'] = enr
                          mean_sense['id'] = eid+'-'+enr
                          exdoc.find('//meanings').first << mean_sense
                        else
                          mean_sense = exdoc.find('//meaning[@number="'+enr+'"]').first
                        end
                        mean_sense << ntd
                      end
                    end
                    if request.query['v'+i.to_s+'_type'].to_s == 'sign_usage_example'
                      nus = XML::Node.new('usages')
                      nu = XML::Node.new('usage')
                      nu['id'] = eid + '-1-1' 
                      ntd = XML::Node.new('text')
                      nf = XML::Node.new('file')
                      nf['media_id'] = mediaid
                      ntd << nf
                      nu << ntd
                      nus << nu
                      exdoc.find('//entry').first << XML::Node.new('meanings') if exdoc.find('//meanings').size == 0
                      if enr == ''
                        if exdoc.find('//meaning').size == 0
                          nm = XML::Node.new('meaning')
                          nm['id'] = eid+'-1'
                          nm['number'] = '1'
                          exdoc.find('//meanings').first << nm
                        end
                        exdoc.find('//meaning').first << nus
                      else
                        if exdoc.find('//meaning[@number="'+enr+'"]').size == 0
                          mean_sense = XML::Node.new('meaning')
                          mean_sense['id'] = eid+'-'+enr
                          mean_sense['number'] = enr
                          exdoc.find('//meanings').first << mean_sense
                        else
                          mean_sense = exdoc.find('//meaning[@number="'+enr+'"]').first
                        end
                        mean_sense << nus
                      end
                    end
                  end
                  $stderr.puts exdoc.to_s
                  ##save
                  @dict_array[targetdict].update(eid, exdoc.to_s)
                  update[targetdict] << eid
                end
              }
            end

            if request.query['v'+i.to_s+'_newentry'].to_s != '' and request.query['v'+i.to_s+'_assign'].to_s == ''
              eid = request.query['v'+i.to_s+'_newentry'].to_s
              exxml = @dict_array[targetdict].get(eid)
              if exxml == ''
                xml = '<entry id="'+eid.to_s+'">'
                xml += '<lemma id="'+eid.to_s+'">'
                xml += '<completeness>0</completeness>'
                xml += '<pracskupina>'+request.query['v'+i.to_s+'_skup'].to_s+'</pracskupina>'
                xml += '<created_at>'+Time.now.strftime("%Y-%m-%d %H:%M:%S")+'</created_at>'
                xml += '<video_front>'+request.query['v'+i.to_s+'_file'].to_s+'</video_front>' if request.query['v'+i.to_s+'_type'].to_s == 'sign_front'
                xml += '<video_side>'+request.query['v'+i.to_s+'_file'].to_s+'</video_side>' if request.query['v'+i.to_s+'_type'].to_s == 'sign_side'
                xml += '<id>'+eid.to_s+'</id>'
                xml += '</lemma>'
                xml += '<meanings><meaning number="1" id="'+eid.to_s+'-1"><category>'+skupina_obor[request.query['v'+i.to_s+'_skup'].to_s].to_s+'</category>'
                if request.query['v'+i.to_s+'_tran'].to_s != ''
                  if cjhesla[request.query['v'+i.to_s+'_tran'].to_s].nil?
                    xml += '<relation type="translation" target="'+transdict+'" meaning_id="'+request.query['v'+i.to_s+'_tran'].to_s+'"/>'
                  else
                    xml += '<relation type="translation" target="'+transdict+'" meaning_id="'+cjhesla[request.query['v'+i.to_s+'_tran'].to_s]+'-1"/>'
                  end
                end
                xml += '<text><file media_id="'+mediaid+'"/></text>' if request.query['v'+i.to_s+'_type'].to_s == 'sign_definition'
                xml += '<usages><usage id="'+eid.to_s+'-1-1"><text><file media_id="'+mediaid+'"/></text></usage></usages>' if request.query['v'+i.to_s+'_type'].to_s == 'sign_usage_example'
                xml += '</meaning></meanings>'
                xml += '</entry>'
                $stderr.puts xml
                newlemma << eid + '_' + label
                vd = @dict.load_xml_string(filexml)
                vd.root << XML::Node.new('entry_folder', eid.to_s)
                filexml = vd.to_s
                $stderr.puts filexml
                ##save
                @dict_array[targetdict].update(eid, xml)
                update[targetdict] << eid
              else
                exdoc = @dict.load_xml_string(exxml)
                if request.query['v'+i.to_s+'_type'].to_s == 'sign_front'
                  exdoc.find('//lemma').first << XML::Node.new('video_front', request.query['v'+i.to_s+'_file'].to_s)
                end
                if request.query['v'+i.to_s+'_type'].to_s == 'sign_side'
                  exdoc.find('//lemma').first << XML::Node.new('video_side', request.query['v'+i.to_s+'_file'].to_s)
                end
                if request.query['v'+i.to_s+'_type'].to_s == 'sign_definition'
                  ntd = XML::Node.new('text')
                  nf = XML::Node.new('file')
                  nf['media_id'] = mediaid
                  ntd << nf
                  exdoc.find('//meaning').first << ntd
                end
                if request.query['v'+i.to_s+'_type'].to_s == 'sign_usage_example'
                  nus = XML::Node.new('usages')
                  nu = XML::Node.new('usage')
                  nu['id'] = eid + '-1-1' 
                  ntd = XML::Node.new('text')
                  nf = XML::Node.new('file')
                  nf['media_id'] = mediaid
                  ntd << nf
                  nu << ntd
                  nus << nu
                  exdoc.find('//meaning').first << nus
                end
                $stderr.puts exdoc.to_s
                ##save
                @dict_array[targetdict].update(eid, exdoc.to_s)
                update[targetdict] << eid
              end
            end


            ##save file
            @dict_array[targetdict+'_media'].update(mediaid, filexml)

          end

          i += 1
        end
        #save log
        logfile.close
        logfile = File.open(logname, 'w')
        logfile.puts 'FINAL'
        logfile.puts 'newlemma==='+newlemma.join(':')
        logfile.puts 'updlemma==='+updlemma.join(':')
        logfile.puts 'newdef==='+newdef.join(':')
        logfile.puts 'newusage==='+newusage.join(':')
        logfile.puts 'newcjlemma==='+newcjlemma.join(':')
        logfile.close
        update.each{|code, list|
          open('/var/lib/deb-server/cacheczj/'+code+'.lst','a'){|f|
            list.each{|lid|
              f.puts lid
            }
          }
        }
      end
  end

  def do_GET(request, response)
    if request['Host'].to_s == 'dictio.info' or request['Host'].to_s == 'dictio.teiresias.muni.cz'
      $stderr.puts request
      newurl = 'www.dictio.info'+request.path+'?'+request.query_string
      if @server[:SSLEnable]
          newurl = 'https://'+newurl
      else
          newurl = 'http://'+newurl
      end
      response.set_redirect(WEBrick::HTTPStatus::MovedPermanently, newurl)
    end
    @servername = get_servername(@server, request) if @servername == nil
    dictcode = request.path[1..-1]
    
    user = request.attributes[:auth_user].to_s
    $stderr.puts user 
    $stderr.puts @server.info.service_name

    request.query['action'] = 'report' if user == '' and not ['report','reportcs','reportis','reporten','reportasl','reportspj','reportsj'].include?(request.query['action'].to_s)
    request.query['action'] = 'main' if request.query['action'].to_s == ''
    request.query['lang'] = 'cz' if request.query['lang'].to_s == ''

    case request.query['action']
    when 'main'
      tmpl_params = {}
      tmpl_params['admin'] = 1 if checkadmin(user)
      tmpl_params['ro'] = 1 if check_perm(user) == 'ro'
      tmpl_params['active_'+request.query['lang'].to_s] = 'lang-menu-active'
      tmpl_params['lang'] = request.query['lang'].to_s
      tmpl_params['user'] = user
      response.body = HTMLTemplate.new(tmpl_params).output(@dict.template_path + '/admin.tmpl')
      return response.body
    when 'changeprof'
      if request.query['new'] != nil and request.query['new'] != ''
        newcrypt = request.query['new'].crypt(WEBrick::Utils::random_string(2))
        xml = @dict.get('user'+user)
        doc = @dict.load_xml_string(xml.to_s)
        doc.find('/user/pass').first.content = newcrypt
        @dict.update('user'+user, doc.to_s)
        @server[:Authenticator].userdb.reload(true)
        update_htpasswd(['cpa'])

        response.set_redirect(WEBrick::HTTPStatus::SeeOther, @servername + '/admin?action=changeprof&set=1')
        response.body += newcrypt
      end
      if request.query['new_name'] != nil
        xml = @dict.get('user'+user)
        doc = @dict.load_xml_string(xml.to_s)
        doc.find('/user/name').first.content = request.query['new_name'].to_s
        doc.find('/user/email').first.content = request.query['new_email'].to_s
        @dict.update('user'+user, doc.to_s)
        response.set_redirect(WEBrick::HTTPStatus::SeeOther, @servername + '/admin?action=changeprof&set=1')
      end
      if request.query['new_rel'] != nil
        @dict.update('user'+user, doc.to_s)
        xml = @dict.get('user'+user)
        doc = @dict.load_xml_string(xml.to_s)
        doc.root << XML::Node.new('set_rel') if doc.find('/user/set_rel').first.nil?
        doc.find('/user/set_rel').first.content = request.query['new_rel'].to_s
        @dict.update('user'+user, doc.to_s)
        response.set_redirect(WEBrick::HTTPStatus::SeeOther, @servername + '/admin?action=changeprof&set=1')
      end
      xml = @dict.get('user'+user)
      doc = @dict.load_xml_string(xml.to_s)
      name = ''
      email = ''
      name = doc.find('/user/name').first.content.to_s unless doc.find('/user/name').first.nil?
      email = doc.find('/user/email').first.content.to_s unless doc.find('/user/email').first.nil?
      set_rel = 'all'
      set_rel = doc.find('/user/set_rel').first.content.to_s unless doc.find('/user/set_rel').first.nil?
      params = {'user'=>user, 'name'=>name, 'email'=>email}
      skupiny, copy, zdroj, autor = @dict_array['czj'].get_user_info(user, @dict_array['czj_user'])
      params['autor'] = autor
      params['copy'] = copy
      params['zdroj'] = zdroj
      params['skupiny'] = skupiny
      params['active_'+request.query['lang'].to_s] = 'lang-menu-active'
      params['lang'] = request.query['lang'].to_s
      params['rel_'+set_rel] = '1'
      if request.query['set'] == '1'
        params['set'] = 1
      end
      response['Content-Type'] = 'text/html; charset=utf-8'
      response.body = HTMLTemplate.new(params).output(@dict.template_path + '/changeprof.tmpl')
      return response.body

    when 'edituser'
      testperm = checkadmin(user)
      if testperm != true
        response['Content-Type'] = 'text/html; charset=utf-8'
        response.body = HTMLTemplate.new({'noaccess'=>1}).output(@dict.template_path + '/listusers.tmpl')
        return response.body
      end
      newuser = false
      login = ''
      
      response['Content-Type'] = 'text/html; charset=utf-8'      
      if request.query['login'] != nil and request.query['login'] != ''
        login =request.query['login']
        
        #new user - check if login exists
        if request.query['new'] != nil and request.query['new'] == '1'
          if @dict.get('user'+login).to_s != ''
            responsetext = 'login '+login+' již existuje'
            response.body = responsetext
            return response.body
          end
        end
        #if password is entered, crypt it
        #if new user, generate new password
        #else use the existing one
        if request.query['pass'] != nil and request.query['pass'].to_s != ''
          newpass = request.query['pass']
          pass = newpass.crypt(WEBrick::Utils::random_string(2))
        elsif request.query['new'] != nil and request.query['new'] == '1'
          newpass = WEBrick::Utils::random_string(8)
          pass = newpass.crypt(WEBrick::Utils::random_string(2))
          newuser = true
        else
          pass = @server[:Authenticator].userdb[login]
        end

        email = request.query['email'].to_s

        xml = '<user>'
        xml += '<login>'+login+'</login>'
        xml += '<name>'+request.query['name'].to_s+'</name>'
        xml += '<copy>'+request.query['copy'].to_s+'</copy>'
        xml += '<zdroj>'+request.query['zdroj'].to_s+'</zdroj>'
        xml += '<autor>'+request.query['autor'].to_s+'</autor>'
        xml += '<email>'+email+'</email>'
        xml += '<pass>'+pass+'</pass>'

        labelskup = {
          'editor_transkripce'=>'editor transkripce ČZJ',
          'editor_preklad'=>'editor překladu ČZJ',
          'editor_komparator'=>'editor komparátor ČZJ',
          'editor_formal'=>'editor form. popisu ČZJ',
          'editor_video'=>'editor videí ČZJ',
          'editor_cjformal'=>'editor form. popisu ČJ',
          'editor_cjlemma'=>'editor zákl. info. lemma ČJ',
          'revizor_transkripce'=>'revizor transkripce ČZJ',
          'revizor_lingvist'=>'revizor formální části ČZJ',
          'revizor_video'=>'revizor videí ČZJ',
          'revizor_lemmaczj'=>'revizor lemmata ČZJ',
          'revizor_cjlingvist'=>'revizor lingvista ČJ',
          'revizor_lemmacj'=>'revizor zákl. info. lemma ČJ',
          'revizor_preklad'=>'revizor překladů',
          'skupina_biologie'=>'terminologie biologie MU',
          'skupina_informatika'=>'terminologie informatika MU',
          'skupina_matematika'=>'terminologie matematika MU',
          'skupina_podnikani'=>'terminologie podnikání APPN, MU',
          'skupina_upol'=>'znaková zásoba ÚSS UP',
          'skupina_obecna'=>'obecná zásoba MU',
          'skupina_spec'=>'speciální pedagogika UHK, MU',
          'skupina_all'=>'bez omezení'
        }
        mailservs = []
        mailskup = []
        unless request.query['skupina'].nil?
          request.query['skupina'].each_data{|e|
            mailskup << labelskup['skupina_'+e.to_s]
            xml += '<skupina>'+e.to_s+'</skupina>' unless e.nil?
          }
        end
        xml += '<admin>true</admin>' if request.query['admin'] != nil and request.query['admin'] == 'on'
        xml += '<prava>'
        readonly = true
        unless request.query['editor'].nil?
          request.query['editor'].each_data{|e|
            mailservs << labelskup['editor_'+e.to_s]
            xml += '<editor perm="'+e.to_s+'"/>' unless e.nil?
          }
          readonly = false
        end
        unless request.query['revizor'].nil?
          request.query['revizor'].each_data{|e|
            mailservs << labelskup['revizor_'+e.to_s]
            xml += '<revizor perm="'+e.to_s+'"/>' unless e.nil?
          }
          readonly = false
        end
        xml += '</prava>'
        if readonly
          mailservs << 'jen pro čtení'
          xml += '<services><service code="czj"><dict code="czj" perm="r"/></service></services>'
        else
          xml += '<services><service code="czj"><dict code="czj" perm="w"/></service></services>'
        end
        xml += '</user>'
        $stderr.puts xml
        @dict.update('user'+login, xml)
        @server[:Authenticator].userdb.reload(true)

        #send email
        if newuser
          message = IO.read(@dict.template_path + '/mailnew.tmpl')
        elsif newpass !=nil
          message = IO.read(@dict.template_path + '/mailnewpass.tmpl')
        else
          message = IO.read(@dict.template_path + '/mailchange.tmpl')
        end
        message.gsub!('#{user}', login)
        message.gsub!('#{email}', email)
        message.gsub!('#{pass}', newpass) if newpass != nil
        message.gsub!('#{server}', @servername)
        message.gsub!('#{services}', mailservs.join(', '))
        message.gsub!('#{skupiny}', mailskup.join(', '))
        $stderr.puts message
        if email != ''
          mail_send(email, 'deb@aurora.fi.muni.cz', message, 'debucty@aurora.fi.muni.cz')
        end        
        
      end
      response.set_redirect(WEBrick::HTTPStatus::SeeOther, @servername + '/admin?action=users#' + login)

    when 'users'
      testperm = checkadmin(user)
      if testperm != true
        response['Content-Type'] = 'text/html; charset=utf-8'
        response.body = HTMLTemplate.new({'noaccess'=>1}).output(@dict.template_path + '/listusers.tmpl')
        return response.body
      end
      response['Content-Type'] = 'text/html; charset=utf-8'
      users = {}

      #user list
      user_names = []
      query = '/user[login!="nagios"]'

      @dict.xquery_to_array(query).sort.each{|xml|
        doc = @dict.load_xml_string(xml)
        login = doc.find('/user/login').first.content.to_s
        name = ''
        name = doc.find('/user/name').first.content.to_s unless doc.find('/user/name').first.nil?
        email = doc.find('/user/email').first.content.to_s unless doc.find('/user/email').first.nil?
        copy = doc.find('/user/copy').first.content.to_s unless doc.find('/user/copy').first.nil?
        autor = doc.find('/user/autor').first.content.to_s unless doc.find('/user/autor').first.nil?
        zdroj = doc.find('/user/zdroj').first.content.to_s unless doc.find('/user/zdroj').first.nil?
        admin = (doc.find('/user/admin').first != nil and doc.find('/user/admin').first.content == 'true')? true:false
        userhash = {'login'=>login, 'name'=>name, 'email'=>email, 'admin'=>admin, 'copy'=>copy, 'zdroj'=>zdroj, 'autor'=>autor}
        doc.find('/user/skupina').each{|e|
          userhash['skupina_'+e.content.to_s] = '1'
        }
        doc.find('/user/prava/editor').each{|e|
          userhash['editor_'+e['perm']] = '1'
        }
        doc.find('/user/prava/revizor').each{|e|
          userhash['revizor_'+e['perm']] = '1'
        }
        user_names << userhash
      }


      tmpl_params = {'users'=>user_names}
      tmpl_params['active_'+request.query['lang'].to_s] = 'lang-menu-active'
      tmpl_params['lang'] = request.query['lang'].to_s
      tmpl_params['deluser'] = request.query['delete'].to_s if request.query['delete'] != nil and request.query['delete'].to_s != '' 

      response.body = HTMLTemplate.new(tmpl_params).output(@dict.template_path + '/listusers.tmpl')

    when 'deluser'
      testperm = checkadmin(user)
      if testperm != true
        response['Content-Type'] = 'text/html; charset=utf-8'
        response.body = HTMLTemplate.new({'noaccess'=>1}).output(@dict.template_path + '/listusers.tmpl')
        return response.body
      end
      login = request.query['login'].to_s
      if login != ''
        begin
          xml = @dict.get('user'+login)
        rescue=>e
            response.body = "invalid login"
            return response.body
        end
        doc = @dict.load_xml_string(xml.to_s)

        @dict.delete('user'+login)
        @server[:Authenticator].userdb.reload(true)

        #send email
        message = IO.read(@dict.template_path + '/maildel.tmpl')
        message.gsub!('#{user}', login)
        message.gsub!('#{email}', email)
        $stderr.puts message
        if email != ''
          mail_send(email, 'deb@aurora.fi.muni.cz', message, 'debucty@aurora.fi.muni.cz')
        end

      end
      response.set_redirect(WEBrick::HTTPStatus::SeeOther, @servername + '/admin?action=users')

    when 'genpass'
      testperm = checkadmin(user)
      if testperm != true
        response['Content-Type'] = 'text/html; charset=utf-8'
        response.body = HTMLTemplate.new({'noaccess'=>1}).output(@dict.template_path + '/listusers.tmpl')
        return response.body
      end

      if request.query['login'] != nil and request.query['login'] != ''
        login = request.query['login']
        newpass = WEBrick::Utils::random_string(8)
        newcrypt = newpass.crypt(WEBrick::Utils::random_string(2))
        begin
          xml = @dict.get('user'+login)
        rescue =>e
            response.body = "invalid login"
            return response.body
        end
        doc = @dict.load_xml_string(xml.to_s)
        email = doc.find('/user/email').first.content.to_s
        doc.find('/user/pass').first.content = newcrypt
        @dict.update('user'+login, doc.to_s)
        @server[:Authenticator].userdb.reload(true)
        

        #send email
        message = IO.read(@dict.template_path + '/mailnewpass.tmpl')
        message.gsub!('#{user}', login)
        message.gsub!('#{email}', email)
        message.gsub!('#{pass}', newpass) if newpass != nil
        message.gsub!('#{server}', @servername)
        $stderr.puts message
        if email != ''
          mail_send(email, 'deb@aurora.fi.muni.cz', message, 'debucty@aurora.fi.muni.cz')
        end

        response.set_redirect(WEBrick::HTTPStatus::SeeOther, @servername + '/admin?action=users')
        response.body += newcrypt
      else
          response.body = "invalid login"
          return response.body
      end
    when 'newcs'
      response['Content-Type'] = 'text/html; charset=utf-8'
      tmpl = {}
      tmpl['newid'] = request.query['newid'].to_s if request.query['newid'].to_s != ''
      tmpl['lang'] = request.query['lang'].to_s
      tmpl['active_'+request.query['lang'].to_s] = 'lang-menu-active'
      response.body = HTMLTemplate.new(tmpl).output(@dict.template_path + '/newcs.tmpl')
      return response.body
    when 'newcs_save'
      if request.query['lemma'].to_s != ''
        newid = @server.info.get_next_id('cs')
        xml = "<entry id='#{newid}' lang='cs'><lemma><title>#{request.query['lemma'].to_s}</title><created_at>#{Time.now.strftime("%Y-%m-%d %H:%M:%S")}</created_at><id>#{newid}</id><completeness>0</completeness></lemma><meanings>"
        xml += "<meaning id='#{newid}-1' number='1'><relation type='translation' meaning_id='#{request.query['tran1'].to_s}'/></meaning>" 
        xml += "<meaning id='#{newid}-2' number='2'><relation type='translation' meaning_id='#{request.query['tran2'].to_s}'/></meaning>" if request.query['tran2'].to_s != ''
        xml += "<meaning id='#{newid}-3' number='3'><relation type='translation' meaning_id='#{request.query['tran3'].to_s}'/></meaning>" if request.query['tran3'].to_s != ''
        xml += "</meanings></entry>"
        @dict_array['cs'].update(newid, xml)
        response.set_redirect(WEBrick::HTTPStatus::SeeOther, @servername + '/admin?action=newcs&newid='+newid)
      else
        response.body = 'chybí lemma'
        return response.body
      end
    when 'report_video'
      if request.query['filter'].to_s == ''
        type_a = true
        type_b = true
        type_d = true
        type_k = true
      end
      type_a = true if request.query['type_a'].to_s == '1'
      type_b = true if request.query['type_b'].to_s == '1'
      type_d = true if request.query['type_d'].to_s == '1'
      type_k = true if request.query['type_k'].to_s == '1'
      author = CGI.unescape(request.query['author'].to_s)
      source = CGI.unescape(request.query['source'].to_s)
      status = request.query['status'].to_s
      status = 'hidden' if status == ""
      perm = @dict_array['czj'].get_perm(user, @dict_array['czj_user'])
      if (perm.is_a?(Array) and perm.include?('revizor_video')) or perm.to_s == 'admin'
        page = request.query['page'].to_i

        report, count = @dict_array['czj'].report_video(@dict_array['czj_media'], @dict_array['czj_koment'], page, type_a, type_b, type_d, type_k, request.query['def_skup'].to_s, author, source, status)
        $stderr.puts report
      else
        response.body = 'nemáte přístup'
        return response.body
      end
      tmpl = {'report'=>report, 'count'=>count, 'begin'=>(1+page*10).to_s, 'end'=>(10+page*10).to_s, 'next'=>(page+1).to_s}
      tmpl['prev'] = (page-1).to_s if page > 0
      tmpl['type_a'] = 1 if type_a
      tmpl['type_b'] = 1 if type_b
      tmpl['type_d'] = 1 if type_d
      tmpl['type_k'] = 1 if type_k
      tmpl['authors'] = @dict_array['czj_media'].get_video_author(author)
      tmpl['source'] = @dict_array['czj_media'].get_video_source(source)
      tmpl['def_skup_'+request.query['def_skup'].to_s] = 1
      tmpl['status_'+request.query['status'].to_s] = 1
      url_par = ['filter=true']
      url_par << 'type_a=1' if type_a
      url_par << 'type_b=1' if type_b
      url_par << 'type_d=1' if type_d
      url_par << 'type_k=1' if type_k
      url_par << 'def_skup='+request.query['def_skup'].to_s if request.query['def_skup'].to_s != ''
      url_par << 'author='+CGI.escape(request.query['author'].to_s)
      url_par << 'source='+CGI.escape(request.query['source'].to_s)
      tmpl['url_par'] = url_par.join('&')
      tmpl['lang'] = request.query['lang'].to_s
      tmpl['active_'+request.query['lang'].to_s] = 'lang-menu-active'
      tmpl['user'] = user
      response.body = HTMLTemplate.new(tmpl).output(@dict.template_path + '/reportvideo.tmpl')
      return response.body
    when 'import'
      perm = @dict_array['czj'].get_perm(user, @dict_array['czj_user'])
      targetdict = request.query['targetdict'].to_s
      targetdict = 'czj' if targetdict == ''
      transdict = request.query['transdict'].to_s
      transdict = 'cs' if transdict == ''
      if (perm.is_a?(Array) and (perm.include?('revizor_video') or perm.include?('admin'))) or perm.to_s == 'admin'
        $stderr.puts 'x'
      else
        response.body = 'nemáte přístup'
        return response.body
      end
      dir = request.query['dir'].to_s
      tmpl = {'dir'=>dir, 'videos'=>[]}
      ignored_files = []
      if dir != ''
        $stderr.puts dir
        newen = {}
        trans = {}
        trans_meta = {}
        file_meta = false
        count = 0
        if File.exist?(dir+'/meta.csv')
          fmeta = File.open(dir+'/meta.csv')
          fmeta.each{|lm|
            ma = lm.strip.split(/[,;]/)
            trans_meta[ma[0]] = ma[1] unless ma[1].nil?
          }
          fmeta.close
          file_meta = true
        end
        Dir[dir+'/*wmv'].each{|file|
          command = 'avconv -y -v quiet -i ' + file + ' -an ' + file.sub(/.wmv/i, '.flv')
          $stderr.puts command
          system(command)
          File.rename(file, file+'.converted')
        }
        Dir[dir+'/*{flv,mp4}'].each{|file|
          video = {'c'=>count}
          $stderr.puts file
          if file !~ /^[-\.A-Za-z0-9_\/]*$/
            ignored_files << file
            next
          end
          name =  file.sub(dir+'/','')
          video['name'] = name
          norm_name, norm_sense = norm_name(name)
          video['norm'] = norm_name
          video['type'+name[0,1].to_s] = 1
          nameext = '__'
          nameext = name.sub('.mp4','.flv') if name.include?('.mp4')
          if File.exist?('/var/lib/deb-server/files/czj/media/video'+targetdict+'/'+name) 
            video['fileexist'] = 1
          end
          if File.exist?('/var/lib/deb-server/files/czj/media/video'+targetdict+'/'+nameext) 
            video['fileexist'] = 1
            video['oldname'] = nameext
          end
          found = false
          video['mediaexist'] = []
          @dict_array[targetdict+'_media'].xquery_to_array('/media[location="'+name+'" or original_file_name="'+name+'" or location="'+nameext+'"]').each{|lx|
            md = @dict.load_xml_string(lx)
            l = md.find('//location').first.content.to_s
            vid = md.root['id']
            @dict_array[targetdict].xquery_to_hash('[entry[lemma/video_side="'+l+'" or lemma/video_front="'+l+'" or .//file/@media_id="'+vid+'"]]').each{|li,lv|
              xml = @dict.load_xml_string(@dict_array[targetdict].get_follow(targetdict, li))
              tr = []
              xml.find('//relation[@type="translation"]/title').each{|t|
                tr << t.content.to_s
              }
              video['mediaexist'] << {'lid'=>li, 'tran'=>tr.join(', ')}
              #video['tran'] = name[2..-5].sub(/[0-9]$/, '').sub(/[A-Z]$/, '').gsub('-', ' ').downcase
              video['tran'] = norm_name.split('==')[0].gsub('-', ' ').downcase
            }
            $stderr.puts video['mediaexist']
          }
          video['mediaexist'] = nil if video['mediaexist'].size == 0
          if video['mediaexist'].nil?
            if trans[norm_name].nil?
              mids = []
              medids = []
              main_location = nil
              @dict_array[targetdict+'_media'].query('for $x in collection("'+targetdict+'_media")/media where $x/label="'+norm_name+'"  return <f>{$x/location/text()};{data($x/@id)};{$x/original_file_name/text()};{$x/type/text()}</f>').each{|mid|
                mid, medid, orig, ft = mid.sub('<f>','').sub('</f>','').split(';')
                $stderr.puts mid, orig
                main_location = mid if (ft == 'sign_front' or ft == 'sign_side') 
                mids << '$v="'+mid+'"'
                medids << '$v/@media_id="'+medid+'"'
              }
              $stderr.puts 'main '+main_location.to_s
              entries = []
              top = nil
              got_selected = false
              if mids.size < 20 and medids.size < 20 and mids.size > 0 and medids.size > 0
                query = 'for $x in collection("'+targetdict+'")/entry where (some $v in $x//video_side|$x//video_front satisfies ('+mids.join(' or ')+')) or (some $v in $x//file satisfies ('+medids.join(' or ')+')) return data($x/@id)'
                $stderr.puts query
                @dict_array[targetdict].query(query).each{|id|
                  xml = @dict.load_xml_string(@dict_array[targetdict].get_follow(targetdict, id))
                  #$stderr.puts xml
                  tr = []
                  xml.find('//relation[@type="translation"]/title').each{|t|
                    tr << t.content.to_s
                  }
                  $stderr.puts tr
                  envid = []
                  labels = []
                  xml.find('//video_front|//video_side').each{|vv|
                    @dict_array[targetdict+'_media'].xquery_to_hash('[media[location="'+vv.content.to_s+'"]]').each{|vvid,vxml|
                      vdoc = @dict.load_xml_string(vxml)
                      if vdoc.find('//original_file_name').first.nil?
                        envid << vv.content.to_s
                      else
                        envid << vdoc.find('//original_file_name').first.content.to_s
                      end
                      labels << vdoc.find('//label').first.content.to_s unless vdoc.find('//label').first.nil?
                    }
                  }
                  xml.find('//file[@media_id]').each{|vv|
                    $stderr.puts vv
                    vxml = @dict_array[targetdict+'_media'].get(vv['media_id'])
                    if vxml != ''
                      vdoc = @dict.load_xml_string(vxml)
                      if vdoc.find('//original_file_name').first.nil?
                        envid << vv.content.to_s
                      else
                        envid << vdoc.find('//original_file_name').first.content.to_s
                      end
                      labels << vdoc.find('//label').first.content.to_s unless vdoc.find('//label').first.nil?
                    end
                  }
                  if not xml.find('//video_front|//video_side').first.nil? and not main_location.nil? and xml.find('//video_front|//video_side').first.content[1..-1] == main_location[1..-1]
                    got_selected = true
                    entries = [{'id'=>id, 'trans'=>tr.join(', '), 'selected'=>1, 'c'=>count, 'vids'=>envid.join('/')}]
                    found = true
                    break
                  elsif labels.include?(norm_name)
                    got_selected = true
                    entries = [{'id'=>id, 'trans'=>tr.join(', '), 'selected'=>1, 'c'=>count, 'vids'=>envid.join('/')}]
                    found = true
                    break
                  else
                    entries << {'id'=>id, 'trans'=>tr.join(', '),'c'=>count, 'vids'=>envid.join('/')}
                  end
                  found = true
                }
              end
              if found
                video['selectnew'] = 1 if got_selected == false
                video['entry'] = Marshal.load(Marshal.dump(entries))
                trans[norm_name] =  entries
                video['tran'] = norm_name.split('==')[0].gsub('-', ' ').downcase
              end
            else
              found = true
              video['entry'] = Marshal.load(Marshal.dump(trans[norm_name]))
              video['entry'].each{|ve| ve['c'] = count}
              video['tran'] = norm_name.split('==')[0].gsub('-', ' ').downcase
            end
            unless found
              if newen[norm_name].nil?
                newen[norm_name] = @server.info.get_next_id(targetdict)
                video['newentry'] = newen[norm_name]
                video['tran'] = norm_name.split('==')[0].gsub('-', ' ').downcase
              else
                video['newentry'] = newen[norm_name]
                video['tran'] = norm_name.split('==')[0].gsub('-', ' ').downcase
              end
            end
          end
          video['tran'] = trans_meta[video['name']] unless video['name'].nil?
          video['def_skup_' + request.query['def_skup'].to_s] = 1 if request.query['def_skup'].to_s != ''
          video['tran'] = name[2..-5].sub(/[0-9]$/, '').sub(/[A-Z]$/, '').gsub('-', ' ').downcase if video['tran'].to_s == ''
          video['sense'] = norm_sense
          
          tmpl['videos'] << video
          count += 1
        }
      end
      skupiny, copy, zdroj, autor = @dict_array['czj'].get_user_info(user, @dict_array['czj_user'])
      tmpl['videos'].sort!{|x,y| [x['norm'],x['name']]<=>[y['norm'],y['name']]}
      tmpl['meta_file'] = 1 if file_meta
      tmpl['count'] = count
      tmpl['targetdict'] = targetdict
      tmpl['targetdict_'+targetdict] = 1
      tmpl['transdict'] = transdict
      tmpl['transdict_'+transdict] = 1
      if request.query['def_zdroj'].nil?
        tmpl['def_zdroj'] = zdroj
        tmpl['def_copy'] = copy
        tmpl['def_autor'] = autor
      else
        tmpl['def_zdroj'] = request.query['def_zdroj'].to_s
        tmpl['def_copy'] = request.query['def_copy'].to_s
        tmpl['def_autor'] = request.query['def_autor'].to_s
      end
      tmpl['def_skup_'+skupiny] = 1
      tmpl['def_skup_' + request.query['def_skup'].to_s] = 1 if request.query['def_skup'].to_s != ''
      tmpl['active_'+request.query['lang'].to_s] = 'lang-menu-active'
      tmpl['lang'] = request.query['lang'].to_s
      tmpl['ignored'] = ignored_files.join(", ")
      response.body = HTMLTemplate.new(tmpl).output(@dict.template_path + '/importvideo.tmpl')
      return response.body
    when 'importupload'
      if request.query['dir'].to_s == ''
        dir = Dir::mktmpdir('czj','/tmp')
      else
        dir = request.query['dir'].to_s
      end
      $stderr.puts 'DIR '+dir
      handle_upload(request.query['data1'], dir)
      handle_upload(request.query['data2'], dir)
      handle_upload(request.query['data3'], dir)
      handle_upload(request.query['data4'], dir)
      handle_upload(request.query['data6'], dir)
      response.set_redirect(WEBrick::HTTPStatus::SeeOther, @servername + '/admin?action=import&dir='+dir)
    when 'import_run'
      if request.query['count'].to_i > 0 and request.query['dir'].to_s != ''
        logid = UUID.new.to_s[0..7]
        import_run(request, logid)
        #response.set_redirect(WEBrick::HTTPStatus::SeeOther, @servername + '/admin?action=impor_report&targetdict='+targetdict+'&newlemma='+newlemma.join(':')+'&updlemma='+updlemma.join(':')+'&newdef='+newdef.join(':')+'&newusage='+newusage.join(':')+'&newcjlemma='+newcjlemma.join(':'))
        response.set_redirect(WEBrick::HTTPStatus::SeeOther, @servername + '/admin?action=impor_report&targetdict='+request.query['targetdict'].to_s+'&logid='+logid)
      else
        response.body = 'bad argument'
        return response.body
      end
    when 'impor_report'
      targetdict = request.query['targetdict'].to_s
      logid = request.query['logid'].to_s
      logname = '/var/log/deb-server/czjimport'+logid+'.log'
      tmpl = {}
      if File.exist?(logname)
        logfile = File.new(logname, 'r')
        if logfile.gets.strip == 'FINAL'
          ids = []
          idsc = []
          logfile.each{|line|
            linea = line.strip.split('===')
            if linea[0].to_s == 'newlemma' and linea[1].to_s != ''
              tmpl['newlemma'] = []
              linea[1].to_s.split(':').sort{|a,b| a.split('_')[1] <=> b.split('_')[1]}.each{|id|
                iid, label = id.split('_')
                tmpl['newlemma'] << {'id'=>iid, 'label'=>label}
                ids << iid
              }
            end
            if linea[0].to_s == 'newdef' and linea[1].to_s != ''
              tmpl['newdef'] = []
              linea[1].to_s.split(':').sort{|a,b| a.split('_')[1] <=> b.split('_')[1]}.each{|id|
                iid, label = id.split('_')
                tmpl['newdef'] << {'id'=>iid, 'label'=>label}
                ids << iid
              }
            end
            if linea[0].to_s == 'newusage' and linea[1].to_s != ''
              tmpl['newusage'] = []
              linea[1].to_s.split(':').sort{|a,b| a.split('_')[1] <=> b.split('_')[1]}.each{|id|
                iid, label = id.split('_')
                tmpl['newusage'] << {'id'=>iid, 'label'=>label}
                ids << iid
              }
            end
            if linea[0].to_s == 'updlemma' and linea[1].to_s != ''
              tmpl['updlemma'] = []
              linea[1].to_s.split(':').sort{|a,b| a.split('_')[1] <=> b.split('_')[1]}.each{|id|
                iid, label = id.split('_')
                tmpl['updlemma'] << {'id'=>iid, 'label'=>label}
                ids << iid
              }
            end
            if linea[0].to_s == 'newcjlemma' and linea[1].to_s != ''
              tmpl['newcjlemma'] = []
              linea[1].to_s.split(':').sort{|a,b| a.split('_')[1] <=> b.split('_')[1]}.each{|id|
                itar, iid, label = id.split('_')
                tmpl['newcjlemma'] << {'id'=>iid, 'target'=>itar, 'label'=>label}
                idsc << iid
              }
            end
          }
        else
          tmpl['progress'] = IO.readlines(logname).last(20).join('<br/>')
        end
      end
      res = 'nove vytvorena lemmata = '+request.query['newlemma'].to_s
      res += "\ndoplnena lemmata = "+request.query['updlemma'].to_s
      res += "\ndoplnena definice k lemmatu = "+request.query['newdef'].to_s
      res += "\ndoplnen priklad k lemmatu = "+request.query['newusage'].to_s
      tmpl['targetdict'] = targetdict
      tmpl['targetedit'] = ''
      tmpl['targetedit'] = targetdict if targetdict != 'czj'
      tmpl['active_'+request.query['lang'].to_s] = 'lang-menu-active'
      tmpl['lang'] = request.query['lang'].to_s
      response.body = HTMLTemplate.new(tmpl).output(@dict.template_path + '/importresult.tmpl')
      return response.body
    when 'import_cs'
      perm = @dict_array['czj'].get_perm(user, @dict_array['czj_user'])
      if (perm.is_a?(Array) and perm.include?('revizor_video')) or perm.to_s == 'admin'
      else
        response.body = 'nemáte přístup'
        return response.body
      end
      response['Content-Type'] = 'text/html; charset=utf-8'
      response.body = HTMLTemplate.new().output(@dict.template_path + '/importcs.tmpl')
      return response.body
    when 'import_cs_run'
      if request.query['data'].nil?
        response.body = 'chybi soubor'
        return response
      else
        newentry = []
        updateentry = []
        ids = []

        if request.query['charset'] == 'win1250'
          csv = Iconv.conv('UTF-8', 'windows-1250', request.query['data'])
        else
          csv = request.query['data']
        end
        ar = CSV.parse(csv, ";")
        ar.each{|l|
          $stderr.puts l[0]
          next if l[0] == 'Heslo čj'
          search = @dict_array['cs'].xquery_to_hash('[entry[lemma/title="'+l[0]+'"]]')
          if search.length > 0
            #heslo existuje, pridat vyznam
            doc = @dict.load_xml_string(search.values.first)
            id = doc.root['id']
            mid = 0
            doc.find('//meanings/meaning').each{|m|
              mid = m['id'].to_s.sub(id.to_s+'-', '').to_i if m['id'].to_s.sub(id.to_s+'-', '').to_i > mid
            }
            mid += 1
            nmean = '<meaning oblast="'+request.query['def_oblast'].to_s+'" id="'+id.to_s+'-'+mid.to_s+'" number="'+mid.to_s+'" author="'+l[2].to_s.gsub('"', '&quot;')+'" copyright="'+l[3].to_s.gsub('"', '&quot;')+'">'
            nmean += '<text>'+l[1].to_s.gsub('<','&lt;')+'</text>'
            nmean += '<usages><usage id="'+id.to_s+'-'+mid.to_s+'-1" author="'+l[6].to_s.gsub('"', '&quot;')+'" copyright="'+l[7].to_s.gsub('"', '&quot;')+'"><text>'+l[5].to_s.gsub('<','&lt;')+'</text>'
            nmean += '</usage></usages>'
            if l[4].to_s != ''
              video = @dict_array['czj_media'].xquery_to_hash('[media[original_file_name="'+l[4].to_s+'"]]')
              vid = ''
              if video.length > 0
                video.each{|k,h|
                  vid = k
                }
              end
              if vid != ''
                link = @dict_array['czj'].xquery_to_array('/entry[meanings/meaning//file/@media_id="'+vid+'"]')
                if link.length > 0
                  mdoc = @dict.load_xml_string(link.first)
                  mdoc.find('/entry/meanings/meaning[.//file/@media_id="'+vid+'"]').each{|me|
                    nmean += '<relation type="translation" meaning_id="'+me['id']+'"/>'
                    nrel = XML::Node.new('relation')
                    nrel['type'] = 'translation'
                    nrel['meaning_id'] = id.to_s+'-'+mid.to_s
                    me << nrel
                  }
                  ids << mdoc.root['id']
                  @dict_array['czj'].update(mdoc.root['id'], mdoc.to_s)
                end
              end
            end
            nmean += '</meaning>'
            doc.find('//meanings').first << doc.import(@dict.load_xml_string(nmean).root)
            updateentry << id+'_'+l[0]
            @dict_array['cs'].update(id, doc.to_s)
          else
            #nove heslo
            id = @server.info.get_next_id('cs')
            nmean = '<entry id="'+id+'"><lemma><title>'+l[0].to_s+'</title><pracskupina>'+request.query['skup'].to_s+'</pracskupina><completeness>0</completeness></lemma>'
            nmean += '<meanings>'
            nmean += '<meaning oblast="'+request.query['def_oblast'].to_s+'" number="1" id="'+id.to_s+'-1" author="'+l[2].to_s.gsub('"', '&quot;')+'" copyright="'+l[3].to_s.gsub('"', '&quot;')+'">'
            nmean += '<text>'+l[1].to_s.gsub('<','&lt;')+'</text>'
            nmean += '<usages><usage id="'+id.to_s+'-1-1" author="'+l[6].to_s.gsub('"', '&quot;')+'" copyright="'+l[7].to_s.gsub('"', '&quot;')+'"><text>'+l[5].to_s.gsub('<','&lt;')+'</text>'
            nmean += '</usage></usages>'
            if l[4].to_s != ''
              video = @dict_array['czj_media'].xquery_to_hash('[media[original_file_name="'+l[4].to_s+'"]]')
              vid = ''
              if video.length > 0
                video.each{|k,h|
                  vid = k
                }
              end
              if vid != ''
                link = @dict_array['czj'].xquery_to_array('/entry[meanings/meaning//file/@media_id="'+vid+'"]')
                if link.length > 0
                  mdoc = @dict.load_xml_string(link.first)
                  mdoc.find('/entry/meanings/meaning[.//file/@media_id="'+vid+'"]').each{|me|
                    nmean += '<relation type="translation" meaning_id="'+me['id']+'"/>'
                    nrel = XML::Node.new('relation')
                    nrel['type'] = 'translation'
                    nrel['meaning_id'] = id.to_s+'-'+mid.to_s
                    me << nrel
                  }
                  ids << mdoc.root['id']
                  @dict_array['czj'].update(mdoc.root['id'], mdoc.to_s)
                end
              end
            end
            nmean += '</meaning>'
            nmean += '</meanings>'
            nmean += '</entry>'
            newentry << id + '_' + l[0]
            @dict_array['cs'].update(id, nmean)
          end
        }
        ids.uniq.each{|id|
          @dict_array['czj'].update(id, @dict_array['czj'].get_follow('czj', id, false, nil, 'admin'))
        }
        response.set_redirect(WEBrick::HTTPStatus::SeeOther, @servername + '/admin?action=import_cs_report&newentry='+URI.encode(newentry.join(':'))+'&updentry='+URI.encode(updateentry.join(':')))
      end
    when 'import_cs_report'
      tmpl = {}
      ids = []
      if request.query['newentry'].to_s != ''
        tmpl['newentry'] = []
        request.query['newentry'].to_s.split(':').each{|id|
          iid, label = id.split('_')
          tmpl['newentry'] << {'id'=>iid, 'label'=>label}
          ids << iid
        }
      end
      if request.query['updentry'].to_s != ''
        tmpl['updateentry'] = []
        request.query['updentry'].to_s.split(':').each{|id|
          iid, label = id.split('_')
          tmpl['updateentry'] << {'id'=>iid, 'label'=>label}
          ids << iid
        }
      end
      ids.uniq.each{|id|
        @dict_array['cs'].update(id, @dict_array['cs'].get_follow('cs', id, false, nil, 'admin'))
      }
      response.body = HTMLTemplate.new(tmpl).output(@dict.template_path + '/importcsresult.tmpl')
      return response.body
    when 'report','reportcs','reportis','reporten','reportasl','reportspj','reportsj'
      case request.query['action']
      when 'report'
        dict = 'czj'
      when 'reportcs'
        dict = 'cs'
      when 'reportis'
        dict = 'is'
      when 'reporten'
        dict = 'en'
      when 'reportasl'
        dict = 'asl'
      when 'reportsj'
        dict = 'sj'
      when 'reportspj'
        dict = 'spj'
      end
      pocet = 10
      pocet = 25 if dict == 'cs' or dict == 'en'
      page = request.query['page'].to_i
      tmpl = {'begin'=>(1+page*pocet).to_s, 'end'=>(pocet+page*pocet).to_s, 'next'=>(page+1).to_s}
      tmpl['ro'] = 1 if check_perm(user) == 'ro'
      tmpl['admin'] = 1 if checkadmin(user)
      tmpl['prev'] = (page-1).to_s if page > 0
      if user != ''
        skupiny, copy, zdroj, autor = @dict_array['czj'].get_user_info(user, @dict_array['czj_user']) 
      else
        skupiny = ''
        copy = ''
        zdroj = ''
        autor = ''
      end
      if request.query['filter'].to_s == 'true'
        sort = request.query['sort'].to_s
        sort = 'id' if sort == ''
        tmpl['sort_'+sort] = '1'
        tmpl['sort'] = sort
        tmpl['report'], tmpl['count'] = @dict_array['czj'].get_report(@dict_array[dict], dict, page, request.query, pocet, check_perm(user), skupiny, sort, user)
      end
      unless tmpl['report'].nil?
        tmpl['report'].each{|vh|
          vh['ro'] = 1 if tmpl['ro'] == 1
        }
      end
      tmpl['last'] = (tmpl['count'].to_i / pocet).floor.to_s
      ['bez_sw','bez_hns','nes_sw','nes_hns','rucne','vztahy','videa','noupdate','translation','vyznam','usage','videa2','artik','skup','def_skup','koment','komentbox','usagecs','vyznamcs','coll','usagevid','vyznamvid','pubtrans','completeness','completenessbox','autocomp','autocompbox','schvcelni','schvbocni','translationczj','translationis','translationasl','translationen','translationcs','pubtransczj','pubtransasl','pubtransis','pubtransen','pubtranscs','sldruh','slovni_druh','relpub','texttranslationen','translationsj','translationspj','pubtransspj','pubtranssj','koment_moje','typhesla','seltyphesla'].each{|v|
        tmpl['check_'+v+'-'+request.query[v].to_s] = 1
      }
      url_par = ['filter=true']
      request.query.each{|par,val|
        next if ['page','action','filter','sort'].include?par
        url_par << par+'='+val
      }
      tmpl['url_par'] = (url_par+['sort='+request.query['sort'].to_s]).join('&')
      tmpl['filtr'] = url_par.join('&')
      tmpl['lang'] = request.query['lang'].to_s
      tmpl['lang_'+request.query['lang'].to_s] = '1'
      tmpl['active_'+request.query['lang'].to_s] = 'lang-menu-active'
      tmpl['user'] = user
      response.body = HTMLTemplate.new(tmpl).output(@dict.template_path + '/'+request.query['action']+'.tmpl')
      return response.body
    when 'history'
      response.body = HTMLTemplate.new(tmpl).output(@dict.template_path + '/history.tmpl')
      return response.body
    when 'log_search'
      dict = ''
      dict = request.query['dict'].to_s unless request.query['dict'].to_s == ''
      user = ''
      user = request.query['user'].to_s unless request.query['user'].to_s == ''
      entry = ''
      entry = request.query['entry'].to_s unless request.query['entry'].to_s == ''
      search = '<\(update\|add\)[^>]*'
      if entry != '' and user != ''
        search += 'key=\''+entry+'\'[^>]*user=\''+user+'\''
      elsif entry != ''
        search += 'key=\''+entry+'\'[^>]*'
      elsif user != ''
        search += 'user=\''+user+'\''
      end
      search += '[^>]*>'
      res_ar = []
      if dict == ''
        command = 'grep -o "'+search+'" /var/lib/deb-server/db/*.journal'
        $stderr.puts command
        result = `#{command}`
        result.split("\n").each{|l|
          match = l.match(/\/var\/lib\/deb-server\/db\/([a-z]*)\.journal:<(add|update) key='([0-9]*)' timestamp='([0-9]*)' thread='[^']*' user='([^']*)'.*/)
          res_ar << {'entry'=>match[3], 'type'=>match[2], 'timestamp'=>match[4], 'user'=>match[5], 'dict'=>match[1]} unless match.nil?
        }
      else
        command = 'grep -o "'+search+'" /var/lib/deb-server/db/'+dict+'.journal'
        $stderr.puts command
        result = `#{command}`
        result.split("\n").each{|l|
          match = l.match(/<(add|update) key='([0-9]*)' timestamp='([0-9]*)' thread='[^']*' user='([^']*)'.*/)
          res_ar << {'entry'=>match[2], 'type'=>match[1], 'timestamp'=>match[3], 'user'=>match[4], 'dict'=>dict} unless match.nil?
        }
      end
      output = ''
      res_ar.sort{|x,y| y['timestamp']<=>x['timestamp']}.each{|ha|
        output += ha['user'] + ' : ' + ha['dict'] + ' : ' + ha['entry'] + ' : ' + Time.at(ha['timestamp'].to_i).to_s + "\n"
      }
      response.body = output

    else
      super(request, response)
    end
  end
  
  alias do_POST do_GET
end
