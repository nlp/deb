require 'webrick'

class GSlovServlet < WEBrick::HTTPServlet::AbstractServlet

  def initialize( server, slov )
    @slov = slov
    @server = server
  end

  def not_found( response, key, lang )
    response["Content-type"] = "text/html; charset=utf-8"
    response.body += %Q[<?xml version="1.0" encoding="utf-8"?>\n]
    response.body += "<html><body><br><br>"
    response.body += "<font color='red'>"
    if lang=='cs'
        response.body += "Slovo '<b>#{key}</b>' nenalezeno"
    else
        response.body += "Headword '<b>#{key}</b>' not found"
    end
    response.body += "</font>"
    response.body += "</body></html>"
  end

  def do_GET(request, response)
    $stderr.puts request.query['key'].edump('gslov key')
    user = request.attributes[:auth_user].to_s
    check = @server.info.check_perm(user, 'gslov')
    if not check
      response["Content-type"] = "text/plain; charset=iso-8859-2"
      response.body = 'Unauthorized'      
      return
    end
    key = request.query['key']
    lang = request.query['lang']
    limit = request.query['limit']
    dict_name = request.query['dict']
    searchall = false
    searchall = true if request.query['all'] == '1'
    tr = request.query['tr'].to_s

    if (limit == nil or limit.to_i < 1) and limit.to_i != -1
      limit = 5
    end
    
    response["Content-type"] = "text/html; charset=utf-8"
    response.body = %Q[<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>DebDict: gslov</title>
<style type="text/css">
      body {
      background: #ffffff;
      color: #000000;
      }
      .red {
      color: #ff0000;
      }
      .green {
      color: #007700;
      }
      .darkred {
      color: #770000;
      }
      .blue {
      color: #0000ff;
      }
      .darkblue {
      color: #000088;
      }
      .head {
      font-size: 150%;
      }
      .head1 {
      font-weight: bold;
      font-size: 170%;
      }
      /*PSJC styly*/
      .Heslo { font: bold 12pt sans-serif;color: #ff0000;  }
      .Char { font: italic 10pt serif;  color: #007700; }
      .Tvar {  }
      .Sep { font-weight: bold;  }
      .Pram { color: #000088; }
      .Vyzn { font-style: italic; color: #770000; }
      .Dokl {  }
      .Gram { color: #007700;  }
      .Vysl {color: #007700 }
      .Zav { font-weight: bold; color: navy; }
      .Homo { font-weight: bold;  }
      .Odkazovaci { font-style: italic; }
      .DokladDI { font-size: 9pt;  vertical-align: sub }
      .DokladHI { font-size: 9pt;  vertical-align: super }
      .VyznamDI { font: italic 9pt serif;  vertical-align: sub }
      .VyznamHI { font: italic 9pt serif;  vertical-align: super }
      .Podheslo { background: #FF6600 }
      .Odkaz { color: black;  }
      .Odkaz a {font: 12pt sans-serif;}
      .Odkaz a:hover {text-decoration: none}
      .Valence { font: italic 9pt serif; border: 1.5pt solid red;  vertical-align: middle }
      .Konekt { color: white; }
      .Slovnispojeni { text-decoration: underline }
      </style>
</head>
<body>
]
    names = {
      'ssjc' => 'Slovník spisovného jazyka českého',
      'psjc' => 'Příruční slovník jazyka českého',
      'ssc' => 'Slovník spisovné češtiny',
      'scs' => 'Slovník cizích slov',
      'scfis' => 'Slovník české frazeologie a idiomatiky: výrazy slovesné',
      'scfin' => 'Slovník české frazeologie a idiomatiky: výrazy neslovesné',
      'syno' => 'Slovník českých synonym',
      'diderot' => 'Encyklopedie Diderot',
      'cod11' => 'Concise Oxford English Dictionary, ed. 11',
      'ode' => 'Oxford Dictionary of English',
      'ote' => 'Oxford Thesaurus of English',
      'soed6' => 'Shorter Oxford English Dictionary, ed. 6'
    }

    @slov.sort.each { |num, slovnik|
      slovnik.each { |name,dict|
        $stderr.puts dict_name.to_s + ' ' + name
        if dict_name.to_s != '' and name != dict_name
          next
        end
        if not @server.info.check_perm(user, name)
          next
        end
        response.body += "<p><span class='head1'>" + name + "</span>"
        response.body += " <small>" + names[name] + "</small></p>"
        transform = (tr != '')? name + '-' + tr + '-gslov'  : name + '-gslov'
        seznam = dict.list_starts_with(key, name, limit.to_i, searchall)
        count = 0
        seznam.split(/\n/).each { |id|
          begin
            res = dict.get(URI.escape(id))
            freq = dict.get_freq(name, id)
            response.body += dict.apply_transform(transform, res.to_s, [ ['freq', '"'+freq+'"'] ])
            count += 1
          rescue => e
            $stderr.puts e
          end
        }
        if count == limit.to_i
          all = '0'
          all = '1' if searchall
          case lang
            when 'cs'
              response.body += '<p>Zobrazeno: ' + count.to_s
              response.body += ' <a href="/gslov?lang=' + lang + '&key=' + key + '&lang=' + lang + '&dict=' + name + '&limit=-1&all='+all+'">zobrazit vše</a></p>'
            when 'en'
              response.body += '<p>Results: ' + count.to_s
              response.body += ' <a href="/gslov?lang=' + lang + '&key=' + key + '&lang=' + lang + '&dict=' + name + '&limit=-1&all='+all+'">show all</a></p>'
          end
        end
        response.body += "<hr/>"
      }
    }
    response.body += "</body>\n</html>"
  end

  alias do_POST do_GET
end

