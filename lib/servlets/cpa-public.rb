require 'open-uri'
require 'json'
require 'cobravsmongoose'

class CPAPublicServlet < WEBrick::HTTPServlet::AbstractServlet
  def CPAPublicServlet.get_instance config, *options
    load __FILE__
    CPAPublicServlet.new config, *options
  end

  def initialize (server, slov, admin)
    @slov = slov
    @admin = admin
    @semeval_verbs = []#'continue', 'operate','crush','apprehend','appreciate','decline','undertake','adapt', 'afflict', 'ask', 'avert', 'begrudge', 'bludgeon', 'boo', 'breeze', 'teeter', 'totter', 'advise', 'ascertain', 'attain', 'avoid', 'belch', 'bluff', 'brag', 'sue', 'tense', 'wing']
  end
 
  def do_GET(request, response)
    user = request.attributes[:auth_user].to_s
    case request.query['action']
    #display verb list in rdf
    when 'rdfverblist'
      response["Content-type"] = "text/rdf"
      response.body = @slov.rdf_verblist()

    #display patterns for verb
    when 'patterns'
      entry_id = request.query['id']
      if entry_id != nil
        rdf = @slov.rdf_patterns(entry_id)
        percent = load_percent(entry_id)
        if percent != nil
          rdfdoc = REXML::Document.new(rdf.to_s)
          rdfdoc.root.elements.to_a('//RDF:Description').each{|el|
            pel = el.add_element 'd:perc'
            num = el.elements['d:num'].text.to_i
            if percent[num] != nil
              pel.text = percent[num]
            else
              pel.text = '<1%'
            end
          }
          sel = rdfdoc.root.add_element 'samplesize'
          sel.text = percent[0].to_s
          status, samplesize, semclass, aspclass, erlangen, comment, patterns,\
              difficulty, compilation_time, created_by, created_time,\
              last_edit_by, last_edit_time, bnc50_freq = @slov.get_verb_info(entry_id)
          seli = rdfdoc.root.add_element 'samplesize_i'
          seli.text = samplesize.to_s
          rdf = rdfdoc.to_s
        end
        response["Content-type"] = "text/html; charset=utf-8"
        response.body = @slov.apply_transform('public-verb', rdf, [ ['entry_id', entry_id] ])
      end

    when 'patterns2'
      entry_id = request.query['id']
      stat = @slov.get_status(entry_id)
      if stat != 'C' and  stat !='R'
        patterns = "not released yet"
      else
        patterns = @slov.get_bare_patterns(entry_id)
      end
      response["Content-type"] = "text/html; charset=utf-8"
      response.body = patterns

    when 'percents'
      entry_id = request.query['id']
      percents = load_percent(entry_id)
      response["Content-type"] = "text/plain; charset=utf-8"
      response.body = percents.join("\n") + "\n"
    
    when 'percents2'
      entry_id = request.query['id']
      percents = load_percent2(entry_id)
      response["Content-type"] = "text/plain; charset=utf-8"
      response.body = percents.join("\n") + "\n"

    when 'get_verb_info'
      entry_id = request.query['verb']
      response["Content-type"] = "text/plain; charset=utf-8"
      status, samplesize, semclass, aspclass, erlangen, comment, patterns,\
            difficulty, compilation_time, created_by, created_time,\
            last_edit_by, last_edit_time, bnc50_freq = @slov.get_verb_info(entry_id)
      response.body = samplesize.to_s + '::' + semclass.to_s + '::' +
            aspclass.to_s + '::' + erlangen.to_s + '::' + comment.to_s  + '::' +
            patterns.to_s + '::' + difficulty.to_s + '::' +
            compilation_time.to_s + '::' + bnc50_freq.to_s

    when 'get_semclasses'
      response["Content-type"] = "text/plain; charset=utf-8"
      response.body = @slov.get_distinct_values('ontology', 'term')

    when 'gdex_save'
      verb = request.query['verb']
      patnum = request.query['patnum']
      json_string = request.query['json_string']
      mode = request.query['mode']
      username = request.query['username']
      r = @slov.gdex_save(verb,patnum,json_string,username,mode)
      
    when 'gdex_get'
      verb = request.query['verb']
      patnum = request.query['patnum']
      response["Content-type"] = "application/json"
      response.body = @slov.gdex_get(verb,patnum)

    when 'pattern'
      entry_id = request.query['id']
      pat = request.query['pat']
      if entry_id != nil and pat.to_i > 0
        xml = @slov.get(entry_id)
        response["Content-type"] = "text/html; charset=utf-8"
        response.body = @slov.apply_transform('public-pat', xml, [ ['entry_id', entry_id], ['pat_id', pat] ])
      end

    when 'pattern2'
      key = request.query['id']      
      num = request.query['patid']
      response["Content-type"] = "application/json"
      if key.to_s != '' and num.to_i > 0
        string = @slov.get(key)
        doc = REXML::Document.new(string.to_s)
        pat = REXML::XPath.first(doc, "/pattern_set/pattern[@id='#{num}']")
        response.body = CobraVsMongoose.xml_to_hash(pat.to_s).to_json
      else
        response.body = "#{key} not found".to_json
      end

    when 'advstring'
      verb = request.query['verb']
      patid = request.query['patid']
      response["Content-type"] = "text/plain; charset=utf-8"
      response.body = @slov.make_adverbial_object(verb, patid)

   when 'pataux'
     response["Content-type"] = "application/json"
     verb = request.query['verb'].to_s
     patid = request.query['patid'].to_i
     if verb == '' or patid == 0
       response.status = 500
       response.body = {'error' => "Missing verb of pattern id"}
     else
       response.body = @slov.pattern_aux_info(verb, patid).to_json
     end

   when 'secondary_implicature'
      entry_id = request.query['entry_id'].to_s
      pattern_id = request.query['pattern_id']
      response["Content-type"] = "text/plain"
      response.body = @slov.get_secondary_implicature(entry_id, pattern_id)

    when 'pattern_string'
      entry_id = request.query['entry_id'].to_s
      pattern_id = request.query['pattern_id']
      response["Content-type"] = "text/plain"
      response.body = @slov.get_pattern_string(entry_id, pattern_id)

    when 'semclass_verbs'
      semclass = request.query['semclass']
      response["Content-type"] = "text/plain; charset=utf-8"
      response.body = @slov.semclass_used_verbs(semclass, false, @semeval_verbs)

    #get nouns from pop_ontology table according to constraints
    when 'get_nouns'
      data = request.query['data']
      constraints = request.query['constraints']
      response["Content-type"] = "text/plain; charset=utf-8"
      result = @slov.get_nouns(data, constraints)
      response.body = result

    when 'verbs'
      query = request.query['id'] or ''
      filter = request.query['filter']
      if filter == nil
          filter = 'C'
      end
      response["Content-type"] = "application/json"
      csva = @slov.get_public_verbs(query, filter).split("\n")
      resjson = []
      public_status = ['WIP','ready','complete','NYS']
      csva.each{|l|
        ca = l.split(';')
        next if @semeval_verbs.include? ca[0]
        next unless public_status.include? ca[5]
        ce = {
          'entry_id'=>ca[0],
          'patcount'=>ca[1],
          'bnc_freq'=>ca[2],
          'bnc50_freq'=>ca[3],
          'oec_freq'=>ca[4],
          'status'=>ca[5]
        }
        resjson << ce
      }
      response.body = resjson.to_json
      return response.body

    when 'db_stat'
      response["Content-type"] = "text/plain; charset=utf-8"
      response.body = @slov.db_stat()

    else
      #list verbs
      perm = @slov.get_perm(@admin, user)
      query = request.query['query'].to_s
      response["Content-type"] = "text/html; charset=utf-8"
      response.body = @slov.apply_transform('public-main', @slov.to_rdf(query, false, perm))
    end
  end

  def load_percent2(entry_id)
    status, samplesize, semclass, aspclass, erl, comm, patt, diff, compt, cby,\
            ctime, leby, letime, bnc50_freq = @slov.get_verb_info(entry_id)
    percent = nil
    if samplesize.to_s != ''
      if @slov.corpora == 'bnc50'
        url = 'http://www.fanuk.net/bonito/run.cgi/lngroupinfo?q=q%5Blempos%3D%22' + URI.escape(entry_id) + '-v%22%5D;corpname=bnc50;annotconc=' + URI.escape(entry_id) + '-v;format=text';
      else
        url = 'http://www.fanuk.net/bonito/run.cgi/lngroupinfo?q=q%5Blemma%3D%22' + URI.escape(entry_id) + '%22%5D;corpname=' + URI.escape(@slov.corpora) + ';annotconc=' + URI.escape(entry_id) + ';format=text';
      end
      url += '&q=r' + URI.escape(samplesize.to_s) if !samplesize.to_s.downcase.include? 'all'
      if samplesize.downcase.include? 'all'
        samplesize = 0
      else
        samplesize = samplesize.to_i
      end
      sums = []
      total = 0
      first = true
      open(url, :http_basic_authentication=>['guest', 'GG0we5t']).each{|line|
        next if first and line.strip  == ''
        if first and line.strip != entry_id and line.strip != entry_id+'-v'
          return nil
        end
        if first
          first = false
          next
        end
        la = line.split("\t")
        if la[0] =~ /[0-9]+(.[eaf])?/
          i = la[0].to_i
          sums[i] = 0 if sums[i] == nil
          sums[i] += la[1].to_i
          total += la[1].to_i
        end
        if la[0] == 'u' or la[0] == 'Not assigned'
          total += la[1].to_i
        end
        if samplesize > 0 and la[0] == 'x'
          samplesize -= la[1].to_i
        end
      }
      if samplesize == 0
        samplesize = total
        h = 'all'
      else
        h = samplesize.to_s
      end
      percent = [h]
      for i in (1..sums.size-1)
        perc = (((sums[i].to_f/samplesize.to_f)*10000).round)/100.00
        if perc < 0.01
          perc = '<0.01'
        else
          perc = perc.to_s
        end
        percent[i] = "#{i}\t#{perc}"
      end
      return percent
    end
    return ["no samplesize"]
  end

  def load_percent(entry_id)
    status, samplesize, semclass, aspclass, erlangen, comment, patterns,\
        difficulty, compilation_time, created_by, created_time, last_edit_by,\
        last_edit_time, bnc50_freq = @slov.get_verb_info(entry_id)
    samplesize = samplesize.sub("all=", "")
    percent = nil
    if samplesize.to_s != ''
      if @slov.corpora == 'bnc50'
        url = 'http://www.fanuk.net/bonito/run.cgi/lngroupinfo?q=q%5Blempos%3D%22' + URI.escape(entry_id) + '-v%22%5D;corpname=bnc50;annotconc=' + URI.escape(entry_id) + '-v;format=text';
      else
        url = 'http://www.fanuk.net/bonito/run.cgi/lngroupinfo?q=q%5Blemma%3D%22' + URI.escape(entry_id) + '%22%5D;corpname=' + URI.escape(@slov.corpora) + ';annotconc=' + URI.escape(entry_id) + ';format=text';
      end
      url += '&q=r' + URI.escape(samplesize.to_s) if samplesize.to_s.downcase != 'all'
      samplesize = samplesize.to_i
      sums = []
      total = 0
      first = true
      open(url, :http_basic_authentication=>['guest', 'GG0we5t']).each{|line|
        next if first and line.strip  == ''
        if first and line.strip != entry_id and line.strip != entry_id+'-v'
          return nil
        end
        if first
          first = false
          next
        end
        la = line.split("\t")
        if la[0] =~ /[0-9]+(.[eaf])?/
          i = la[0].to_i
          sums[i] = 0 if sums[i] == nil
          sums[i] += la[1].to_i
          total += la[1].to_i
        end
        if la[0] == 'u' or la[0] == 'Not assigned'
          total += la[1].to_i
        end
        if samplesize > 0 and la[0] == 'x'
          samplesize -= la[1].to_i
        end
      }
      if samplesize == 0
        samplesize = total
      end
      percent = [samplesize]
      for i in (1..sums.size-1)
        perc = (((sums[i].to_f/samplesize.to_f)*10000).round)/100.00
        if perc < 0.01
          percent[i] = '<0.01%'
        else
          percent[i] = perc.to_s + '%'
        end
      end
    end
    return percent
  end

  alias do_POST do_GET
end

