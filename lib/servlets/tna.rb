require 'net/http'
require 'uri'

class TNAServlet < DictServlet

#def TNAServlet.get_instance config, *options
#load __FILE__
#TNAServlet.new config, *options
#end

  def initialize( server, dict, array )
    super
    @servername = nil
  end

  def do_GET(request, response)
    dict_path = request.path[1..-1]
    case request.query['action']
    when 'search'
      name = request.query['name'].to_s
      result = '<html><head><title>Chancery Proceedings: '+name+'</title></head><body><h1>Chancery Proceedings: '+name+'</h1>'
      found = false
      @dict.xquery_to_hash('[record[//surname="'+name+'"]]').each{|k,x|
        found = true
        result += @dict.apply_transform('preview', x.to_s)
        $stderr.puts k
      }
      result += 'not found' unless found
      result += '</body></html>'
      response.body = result
      response['Content-Type'] = 'text/html; charset=utf-8'
    when 'rest'
      url = '/API/Search/false/false/1?numberOfResultsPerPage=150&departments=C,CHES,CP,CRES,DL,DURH,E,ECCL,EXT,FEC,HCA,IND,JUST,KB,LR,LS,PALA,PC,PEV,PL,PRO,PROB,PSO,QAB,REQ,SC,SP,STAC,WALE,WARD&query='+URI.escape(request.query['query'])
      $stderr.puts url
      req = Net::HTTP::Get.new(url)
      resp = Net::HTTP.start('discovery.nationalarchives.gov.uk', 80) {|http|
        http.request(req)
      }
      data = resp.body
      response.body = resp.body
      response.body = url
      return response.body
      data.gsub!('&lt;span class="highlight"&gt;', '<span class="highlight">')
      data.gsub!('&lt;/span&gt;', '</span>')
      data.gsub!('<a:string>', '<string>')
      data.gsub!('</a:string>', '</string>')
      data.gsub!('i:nil', 'nil')
      data.gsub!('&amp;amp;#', '&#')
      data.gsub!(' xmlns="http://schemas.datacontract.org/2004/07/NationalArchives.REST" xmlns:i="http://www.w3.org/2001/XMLSchema-instance"', '')
      data.gsub!(/<StartDate>[0-9][0-9]-[0-9][0-9]-([0-9][0-9][0-9][0-9])<\/StartDate>/, '\0<StartYear>\1</StartYear>')
      response['Content-Type'] = 'text/html; charset=utf-8'
      response.body = @dict.apply_transform('rest', data.to_s, [ ['query', '"'+request.query['query'].to_s+'"'] ])
    else
      super(request, response)
    end
  end
end
