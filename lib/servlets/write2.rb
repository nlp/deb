require 'cobravsmongoose'
require 'base64'
require 'mime/types'
require 'securerandom'

class WriteServlet < DictServlet
  def initialize( server, dict, array )
    super
    @servername = nil

  def add_jsonp(data, callback)
    return callback + "(" + data + ");"
  end

  end
  def do_GET(request, response)
    @servername = get_servername(@server, request) if @servername == nil
    request.query['action'] = 'list' if request.query['action'] == nil or request.query['action'] == ''
    user = request.attributes[:auth_user].to_s
    dict_path = request.path[1..-1]
    perm = @dict.get_perm(user)
    $stderr.puts request.query['action']
    case request.query['action']
    when 'list'
      info = {}
      info['status'] = 'OK'
      info['entries'] = @dict.find_entries(request.query['search'].to_s, perm)
      response.body = info.to_json
    when 'edit'
      if perm != 'm' and perm != 'w'
        response.body = 'not allowed to edit entries'
        return response.body
      end
      tmpl_param = {'dict_name'=>@dict.title, 'schema'=>@dict.schema, 'code'=>dict_path, 'entryid'=>request.query['id'].to_s}
      res = HTMLTemplate.new(tmpl_param).output(@dict.template_path + '/editor.tmpl')
      response['Content-Type'] = 'text/html; charset=utf-8'
      response.body = res
    when 'get'
      info = {}
      format = 'json'
      format = request.query['format'].to_s if request.query['format'].to_s != ''
      if perm == ''
        info['status'] = 'error'
        info['text'] = 'not allowed to view'
      elsif request.query['entry_id'].to_s == ''
        info['status'] = 'error'
        info['text'] = 'entry ID not specified'
      elsif request.query['format'].to_s == 'template' and request.query['xslt'].to_s == ''
        info['status'] = 'error'
        info['text'] = 'xslt not specified'
      else
        entry = @dict.get(request.query['entry_id'].to_s)
        if entry == ''
          info['status'] = 'error'
          info['text'] = 'entry ID not in database'
        else
          case request.query['format'].to_s 
          when 'json'
            begin
              info['status'] = 'OK'
              info['result'] = CobraVsMongoose.xml_to_hash(entry)
            rescue => e
              info['status'] = 'error'
              info['text'] = e
            end
          when 'template'
            begin
              info['status'] = 'OK'
              info['result'] = @dict.apply_transform(request.query['xslt'].to_s, entry)
            rescue => e
              info['status'] = 'error'
              info['text'] = e
            end
          else
            info['result'] = entry
            info['status'] = 'OK'
          end
        end
      end
      if format == 'json'
        response.body = info.to_json
      else
        if info['status'] == 'OK'
          response['Content-Type'] = 'text/xml; charset=utf-8'
          response.body = info['result']
        else
          response.body = info['text']
        end
      end
    when 'preview'
      info = {}
      if perm == ''
        info['status'] = 'error'
        info['text'] = 'not allowed to view'
      elsif request.query['entry_id'].to_s == '' and request.query['entry_xml'].to_s == ''
        info['status'] = 'error'
        info['text'] = 'entry ID or data not specified'
      elsif request.query['xslt'].to_s == ''
        info['status'] = 'error'
        info['text'] = 'xslt template not specified'
      else
        if request.query['entry_xml'].to_s != ''
          xmldata = request.query['entry_xml'].to_s
        else
          xmldata = @dict.get(request.query['entry_id'].to_s).to_s
        end
        if xmldata == ''
          info['status'] = 'error'
          info['text'] = 'no XML data'
        else
          info['result'] = @dict.apply_transform(request.query['xslt'].to_s, xmldata)
          info['status'] = 'OK'
        end
      end
      if request.query['format'].to_s == 'html'
        if info['status'] == 'OK'
          response['Content-Type'] = 'text/html; charset=utf-8'
          response.body = info['result']
        else
          response.body = info['text']
        end
      else
        response.body = info.to_json
      end
    when 'preview_multi'
      info = {}
      if perm == ''
        info['status'] = 'error'
        info['text'] = 'not allowed to view'
      elsif request.query['entry_id'].to_s == ''
        info['status'] = 'error'
        info['text'] = 'entry ID not specified'
      elsif request.query['xslt'].to_s == ''
        info['status'] = 'error'
        info['text'] = 'xslt template not specified'
      else
        info['result'] = @dict.apply_transform(request.query['xslt'].to_s, @dict.get_multi(request.query['entry_id'].to_s))
        info['status'] = 'OK'
      end
      response.body = info.to_json

    when 'hbpreview'
      htemplates = @server.info.get_dict_htemplates(dict_path, @server.info.xslt_path)
      hb = ''
      htemplate = htemplates.select{|h| h['code'] == request.query['hb'].to_s}
      unless htemplate[0].nil?
        hb = htemplate[0]['template'].to_s 
      end
      data = CobraVsMongoose.xml_to_hash(@dict.get(request.query['id'].to_s)).to_json
      tmpl_param = {'data'=>data, 'hb'=>hb}
      res = HTMLTemplate.new(tmpl_param).output(@dict.template_path + '/hbpreview.tmpl')
      response['Content-Type'] = 'text/html; charset=utf-8'
      response.body = res
    when 'save'
      $stderr.puts request.query['data']
      $stderr.puts request.query['id']
      info = {}
      if perm != 'm' and perm != 'w'
        info['text'] = 'not allowed to edit entries'
        info['status'] = 'error'
      elsif request.query['data'].to_s == ''
        info['text'] = 'no XML data'
        info['status'] = 'error'
      else
        if request.query['entry_id'].to_s == ''
          id = @server.info.get_next_id(dict_path).to_s
          doc = @dict.load_xml_string(request.query['data'].to_s)
          doc.root['id'] = id
          data = doc.to_s
        else
          id = request.query['entry_id'].to_s
          data = request.query['data'].to_s
        end
        data = @dict.add_meta(data, user, id)
        data = @dict.detect_mime(data, dict_path, @server.info.file_path)
        $stderr.puts id
        begin
          @dict.update(id, data)
          info['status'] = 'OK'
          info['entry_id'] = id
        rescue => e
          $stderr.puts e
          $stderr.puts e.backtrace
          info['text'] = e
          info['status'] = 'error'
        end
      end
      response.body = info.to_json
    when 'delete'
      info = {}
      if perm != 'm' and perm != 'w'
        info['text'] = 'not allowed to edit entries'
        info['status'] = 'error'
      elsif request.query['entry_id'].to_s == ''
        info['text'] = 'no ID specified'
        info['status'] = 'error'
      else
        @dict.delete(request.query['entry_id'].to_s)
        info['status'] = 'OK'
      end
      response.body = info.to_json
    when 'upload'
      info = {}
      if perm != 'm' and perm != 'w'
        info['text'] = 'not allowed to edit entries'
        info['status'] = 'error'
      else
        begin
          file_path = @server.info.file_path + '/' + dict_path 
          #filename = file_path + '/' + request.query['data'].filename
          extension = MIME::Types[request.query['type'].to_s].first.extensions.first
          filename = SecureRandom.hex + '.' + extension
          full_path = file_path + '/' + filename
          unless File.directory?(file_path)
            Dir.mkdir(file_path)
          end
          $stderr.puts full_path
          File.open(full_path, 'wb') do |file|
            file.write(Base64.decode64(request.query['data'].to_s))
          end
          info['status'] = 'OK'
          info['filename'] = filename
        rescue => e
          $stderr.puts e
          $stderr.puts e.backtrace
          info['text'] = e
          info['status'] = 'error'
        end
      end
      response.body = info.to_json
    end
    if request.query['callback'].to_s != ''
      response["Content-type"] = "text/plain"
      response.body = add_jsonp(response.body, request.query['callback'].to_s)
    end
  end
  alias do_POST do_GET
end
