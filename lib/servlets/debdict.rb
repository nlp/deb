class DebdictServlet < DictServlet
  def initialize( server, dict, array )
    super
    @server = server
  end
  
  def do_GET(request, response)
    dict = request.path[1..-1]
    user = request.attributes[:auth_user].to_s
    check = @server.info.check_perm(user, dict)
    if not check
      response["Content-type"] = "text/plain; charset=iso-8859-2"
      response.body = 'Unauthorized'      
      return
    end
    
    $stderr.puts request.query['action']
    case request.query['action']
    when 'getdoc'
      key       = URI.escape(request.query['id'])
      transform = request.query['tr']
      lang      = request.query['lang']
    
      begin
        res = @dict.get(key)
        freq = @dict.get_freq(dict, key)
        if transform
          response["Content-type"] = "text/html; charset=utf-8"
          res = @dict.apply_transform(transform, res.to_s, [ ['freq', '"'+freq+'"'] ]) if transform
        else
          response["Content-type"] = "text/xml; charset=utf-8"
        end
        response.body += %Q[<?xml version="1.0" encoding="utf-8"?>\n]
        response.body += res
        response.body
      rescue => e
        $stderr.puts e
        not_found( response, key, lang )
      end
      
    when 'store'
    when 'deldoc'
      response["Content-type"] = "text/plain; charset=iso-8859-2"
      response.body = 'Unsupported action'      
    ### klic zacina na, vrati seznam
    when 'list_starts_with'
      key       = request.query['id']
      transform = dict
      lang      = request.query['lang']
      searchall = false
      searchall = true if request.query['all'] == '1'
      if key != ''
        res = @dict.list_starts_with(key, transform, 0, searchall)
        response["Content-type"] = "text/plain; charset=utf-8"
        response.body += res
        response.body
      end    
    else
      super(request, response)
    end
  end
  
  alias do_POST do_GET
end
