require 'template'
require 'cobravsmongoose'
#require 'unicode'
require 'json'
require 'resolv'

module WEBrick
  class HTTPResponse
    def create_error_page
      @body = ''
      @body << <<-_end_of_html_
<html xmlns='http://www.w3.org/1999/xhtml'>
  <head>
    <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>
    <meta http-equiv="refresh" content="0; url=http://www.dictio.info/" />
    <title>dictio.info</title>
    <link type='text/css' rel='stylesheet' href='https://fonts.googleapis.com/css?family=Titillium+Web:400,600&subset=latin,latin-ext'/>
    <link type='text/css' rel='stylesheet' href='https://fonts.googleapis.com/css?family=Roboto&subset=latin,latin-ext'/>
    <link type='text/css' rel='stylesheet' href='https://fonts.googleapis.com/css?family=Roboto+Condensed'/>
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.2.0/css/font-awesome.min.css'/>
    <style>
.czj-title-inner {
background-color: #159c81;
width: 1170px;
font-size: 20px;
font-weight: bold;
color: white;
text-align: center;
margin-left: auto;
margin-right: auto;
}
.czj-title {
background-color: #159c81;
padding-top: 20px;
padding-bottom: 20px;
overflow: hidden;
}
.czj-container {
background-color: white;
margin: 0px;
}
.czj-info {
background-color: white;
color: black;
padding-top: 10px;
padding-bottom: 30px;
}
.czj-inner-wrapper {
width: 1170px;
margin-left: auto;
margin-right: auto;
text-align: center;
}
    </style>
  </head>
  <body>
    <div class='czj-container'>
      <div class='czj-title'>
        <div class='czj-title-inner'>
          <div style='font-size: 36px; margin-bottom: 6px;'><a href='http://www.dictio.info/czj?action=page'><img src='http://www.dictio.info/editor/img/book.png' height='28px; border: 0;'/></a>&nbsp;<a style='text-decoration: none; color: white;' href='http://www.dictio.info/czj?action=page'>DICTIO</a></div>
          </div>
        </div>
        <div class='cleaner'></div>
        
        <div class='czj-info'>
          <div style='width: 1170px; margin-left: auto; margin-right: auto;' class='czj-inner-wrapper'>
          <a style='color:blue;cursor:pointer;decoration:underline;' href="http://www.dictio.info">WWW.DICTIO.INFO <img title='Přihlásit' border='0' src='http://www.dictio.info/editor/img/login.png'/></a>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
      _end_of_html_
    end
  end
end


class CZJServlet < DictServlet

  def initialize( server, dict, array )
    super
    @servername = nil
    @upload_path = '/var/lib/deb-server/files/czj/media/'
  end

  def cache_entry(dictcode, entry_id, fast=false)
    newxml = @dict.get_follow(dictcode, entry_id, false, nil, 'admin')
    #cache, nejprve samotne heslo, potom zavislosti a znovu heslo
    $stderr.puts 'CACHE ' + entry_id
    $stderr.puts 'FAST ' if fast
    @dict.update(entry_id, newxml)
    if fast
      open('/var/lib/deb-server/cacheczj/'+dictcode+'.lst','a'){|f|
        f.puts entry_id
      }
      return
    end
    newdoc = @dict.load_xml_string(newxml)
    newdoc.root.find('//colloc|//revcolloc|//relation').each{|rel|
      if rel['lemma_id'].to_s != ''
        target = dictcode
        if rel['type'].to_s == 'translation'
          target = rel['target'].to_s
          target = (dictcode=='czj')?'cs':'czj' if target == ''
        end
        $stderr.puts 'CACHE ' + target + rel['lemma_id'].to_s
        @dict_array[target].update(rel['lemma_id'].to_s, @dict_array[target].get_follow(target, rel['lemma_id'].to_s, false, nil, 'admin'))
      end
    }
    if dictcode == 'czj'
      newdoc.root.find('/entry/media/file').each{|rel|
        @dict.xquery_to_hash('[entry[@id!="'+entry_id+'" and (lemma/grammar_note/variant="'+rel['id']+'" or lemma/style_note/variant="'+rel['id']+'")]]').each{|rid,rxml|
          @dict.update(rid, @dict.get_follow(dictcode, rid, false, nil, 'admin'))
        }
      }
    end
    $stderr.puts 'CACHE again ' + entry_id
    @dict.update(entry_id, @dict.get_follow(dictcode, entry_id, false, nil, 'admin'))
  end

  def checkadmin(user)
    return false if @server[:Authenticator].nil?
    if @server[:Authenticator].userdb.check_permissions(user, 'admin', true)
      return true
    else
      return false
    end
  end

  def norm_name(name)
    name = name.sub('.flv','')
    name = name.sub('.mp4','')
    sense = ''
    name.sub!(/^[ABDKabdk][-_]/,'')
    var = ''
    var = 'A' if name.include?('_1_FLV_HQ')
    var = 'B' if name.include?('_2_FLV_HQ')
    var = 'C' if name.include?('_3_FLV_HQ')
    var = 'D' if name.include?('_4_FLV_HQ')
    var = 'E' if name.include?('_5_FLV_HQ')
    var = 'F' if name.include?('_6_FLV_HQ')
    name.sub!('_1_FLV_HQ','')
    name.sub!('_2_FLV_HQ','')
    name.sub!('_3_FLV_HQ','')
    name.sub!('_4_FLV_HQ','')
    name.sub!('_5_FLV_HQ','')
    name.sub!('_6_FLV_HQ','')
    name.gsub!('FLV_HQ','')
    if name =~ /_[1-9]$/
      var += name[/_([1-9])$/,1]
      name.sub!(/_[1-9]$/,'')
    end
    var = 'A' if name =~ /_I$/
    var = 'B' if name =~ /_II$/
    var = 'C' if name =~ /_III$/
    var = 'D' if name =~ /_IV$/
    name.sub!(/_I$/,'')
    name.sub!(/_II$/,'')
    name.sub!(/_III$/,'')
    name.sub!(/_IV$/,'')
    name.sub!('A-C1','A1')
    name.sub!('A-C-D','A1d')
    if name =~ /[A-Z]$/ and not name =~ /[A-Z][A-Z]$/
      var = name[-1,1]
      name = name[0..-2]
    end
    if name =~ /[A-Z][1-9]$/
      var = name[-2,1]
      sense = name[-1,1]
      name = name[0..-3]
    end
    if name =~ /[A-Z][1-9][a-z]$/
      var = name[-3,3]
      name = name[0..-4]
    end
    if name =~ /[0-9][a-z]$/
      var = (name[/([0-9])[a-z]$/,1].to_i+64).chr + (name[/[0-9]([a-z])$/,1][0]-96).to_s
      name = name[0..-3]
    end
    if name =~ /[a-z][0-9]$/
      var = (name[/([0-9])$/,1].to_i+64).chr
      name = name[0..-2]
    end
    var = 'A' if var == ''
    name.gsub!(/([a-z])([A-Z])/, '\1-\2')
    name.gsub!('_','-')
    name.gsub!('(','')
    name.gsub!(')','')
    name.downcase!
    return name+'=='+var, sense
  end

  def save_uploaded_file(filedata, filename, metadata, entryid, dictcode)
    filename = filename.gsub(/[^\w^\.^_^-]/, '')
    $stderr.puts filename
    mediaid = nil
    @dict_array[dictcode+'_media'].xquery_to_hash('[media[location="'+filename+'"]]').each{|mid,mxml|
      mediaid = mid
    }
    mediaid = @server.info.get_next_id(dictcode+'_media').to_s if mediaid.nil?
    filepath = @upload_path + 'video'+dictcode+'/' + filename
    $stderr.puts filepath
    f = File.new(filepath, 'w')
    f.syswrite(filedata)
    f.chmod(0664)
    f.close
    if filename[-4..-1].downcase == '.wmv'
      system('avconv -y -v quiet -i ' + filepath + ' ' + filepath.sub(/.wmv/i, '.flv'))
      filepath.sub!(/.wmv/i, '.flv')
      filename.sub!(/.wmv/i, '.flv')
    end
    if filename[-4..-1].downcase == '.mp4'
      system('ffmpeg -y -i ' + filepath + ' -vcodec libvpx -acodec libvorbis ' + filepath + '.webm')
      system('qt-faststart '+filepath+' '+filepath+'_new; if [ -e '+filepath+'_new ]; then mv -f '+filepath+'_new '+filepath+'; fi;')
    end
    label = norm_name(filename)[0]
    filexml = "<media id='#{mediaid}'>"
    filexml += "<entry_folder>#{entryid}</entry_folder>" if entryid != ''
    filexml += "<location>#{filename}</location>
    <label>#{label}</label>
    <original_file_name>#{filename}</original_file_name>
    <id_meta_copyright>#{metadata['@id_meta_copyright']}</id_meta_copyright>
    <id_meta_author>#{metadata['@id_meta_author']}</id_meta_author>
    <id_meta_source>#{metadata['@id_meta_source']}</id_meta_source>
    <admin_comment>#{metadata['@admin_comment']}</admin_comment>
    <type>#{metadata['@type']}</type>
    <status>#{metadata['@status']}</status>
    <orient>#{metadata['@orient']}</orient>
    <created_at>#{Time.now.strftime("%Y-%m-%d %H:%M:%S")}</created_at>
    </media>"
    $stderr.puts filexml
    command = '/var/lib/deb-server/files/czj/editor/mkthumb.sh "'+filename+'" '+dictcode
    $stderr.puts command
    Thread.new {
      system(command)
    }
    @dict_array[dictcode+'_media'].update(mediaid, filexml)

    return mediaid, filename
  end

  def get_hostname(ip)
    begin
      name = Resolv.getname(ip)
    rescue
      name = ip.to_s
    end
    return name
  end

  def do_GET(request, response)

    if request['Host'].to_s == 'dictio.info' or request['Host'].to_s == 'dictio.teiresias.muni.cz'
      $stderr.puts request
      newurl = 'www.dictio.info'+request.path+'?'+request.query_string
      if @server[:SSLEnable]
          newurl = 'https://'+newurl
      else
          newurl = 'http://'+newurl
      end
      response.set_redirect(WEBrick::HTTPStatus::MovedPermanently, newurl)
    end

    user = request.attributes[:auth_user].to_s

    is_cookie = false
    request.cookies.each{|coo|
      if coo.name == 'dictio_pref'
        is_cookie = true 
        cookie_val = coo.value
            CGI.unescape(cookie_val.to_s).split(';').each{|cs|
              csa = cs.split('=')
              if csa[0] == 'default' and user != ''
                is_cookie = false
              end
              if csa[0] == 'format' and request.query['video'].to_s == ''
                request.query['video'] = csa[1]
              end
            }
      end
    }

    default_target = 'czj'
    default_dict = 'cs'
    default_type = 'translate'
    #load settings to cookie for user
    if user != '' and not is_cookie #@dict_array['czj'].loaded_cookie.include?(user)
      $stderr.puts 'LOAD SETTING'
      set = @server.info.get_settings(user, @server.info.service_name)
      $stderr.puts set
      @dict_array['czj'].loaded_cookie.push(user)
      request.query['lang'] = set['settings']['lang']['$'] if request.query['lang'].to_s == '' and not set['settings']['lang'].nil?
      cookie_set = []
      cookie_set = ['lang='+set['settings']['lang']['$']] unless set['settings']['lang'].nil?
      unless set['settings']['default_lang'].nil?
        cookie_set.push('default_lang='+set['settings']['default_lang']['$']) 
        default_dict = set['settings']['default_lang']['$']
      end
      unless set['settings']['format'].nil?
        cookie_set.push('format='+set['settings']['format']['$']) 
        request.query['video'] = set['settings']['format']['$']
      end
      unless set['settings']['dict_type'].nil?
        cookie_set.push('dict_type='+set['settings']['dict_type']['$']) 
        default_type = set['settings']['dict_type']['$']
      end
      unless set['settings']['props'].nil?
        set['settings']['props'].each{|hs|
          cookie_set.push(hs['@name'] + '=' + hs['@val'])
        }
      end
      cookie_val = CGI.escape(cookie_set.join(';'))
      cookie = WEBrick::Cookie.new('dictio_pref', cookie_val)
      response.cookies.unshift(cookie)
      is_cookie = true
    end

    #pokud neni cookie, nastavi se podle domeny a presmeruje
    if not is_cookie
      remotename = get_hostname(request.remote_ip)
      if remotename.end_with?('.cz')
        default_dict = 'cs'
        request.query['lang'] = 'cz' 
        lang = 'cz'
        default_target = 'czj'
        cookie_val = CGI.escape(['lang=cz','dict-czj=true','dict-cs=true','default_lang=cs','default=true'].join(';'))
        $stderr.puts 'DOMAIN CZ '+cookie_val
      elsif remotename.end_with?('.sk')
        default_dict = 'sj'
        request.query['lang'] = 'sk' 
        lang = 'sk'
        default_target = 'spj'
        cookie_val = CGI.escape(['lang=sk','dict-spj=true','dict-sj=true','default_lang=sj','default=true'].join(';'))
        $stderr.puts 'DOMAIN SK '+cookie_val
      else
        default_dict = 'en'
        request.query['lang'] = 'en' 
        lang = 'en'
        default_target = 'czj'
        cookie_val = CGI.escape(['lang=en','dict-czj=true','dict-asl=true','dict-is=true','dict-en=true','default_lang=en','default=true'].join(';'))
        $stderr.puts 'DOMAIN EN '+cookie_val
      end
      cookie = WEBrick::Cookie.new('dictio_pref', cookie_val)
      response.cookies.unshift(cookie)
      is_cookie = true
    end


    @servername = get_servername(@server, request) if @servername == nil
    dictcode = request.path[1..-1]
    
    is_public = false
    if user == ''
      user = 'read'
    end
    is_public = true if user == 'read'

    $stderr.puts @server.info.service_name

    request.query['action'] = 'page' if request.query['action'].to_s == ''
    request.query['lang'] = 'cz' if request.query['lang'].to_s == ''
    video_format = 'webm'
    video_format = 'mp4' if request.query['video'].to_s == 'mp4'

    template_conf = @dict.locale[request.query['lang'].to_s].clone
    template_conf = {} if template_conf.nil?
    template_conf['public'] = '1' if is_public
    template_conf['is_admin'] = 1 if checkadmin(user)
    template_conf['lang'] = request.query['lang'].to_s
    template_conf['video_format'] = video_format
    template_conf['video_'+video_format] = '1'
    template_conf['dictcode'] = dictcode
    template_conf['dictcode_'+dictcode] = '1'
    template_conf['page_action'] = request.query['action'].to_s
    template_conf['referer'] = request['referer']
    template_conf['query_string'] = request.query_string.to_s.gsub(/lang=(en|cz|sk)/,'').gsub(/video=(webm|mp4)/,'').chomp('&')
    template_conf['query_string_page'] = request.query_string.to_s.gsub(/page=[^&]*/,'').gsub(/&*$/, '')
    template_conf['active_'+request.query['lang'].to_s] = 'lang-menu-active'
    template_conf['loc-web-searchpreklad'] = template_conf['loc-web-searchpreklad-'+dictcode].to_s

    case request.query['action']
    when 'page'
      page = request.query['page'].to_s
      lang = request.query['lang'].to_s
      if page == ''
        default_type = 'translate' if default_type.to_s == ''
        default_dict = 'cs' if default_dict.to_s == ''
        if user == 'read'
          if is_cookie
            CGI.unescape(cookie_val.to_s).split(';').each{|cs|
              csa = cs.split('=')
              if csa[0] == 'default_lang'
                default_dict = csa[1]
              end
              if csa[0] == 'dict_type'
                default_type = csa[1]
              end
              if csa[0] == 'lang'
                lang = csa[1]
              end
            }
          end
        else
          set = @server.info.get_settings(user, @server.info.service_name)
          default_dict = set['settings']['default_lang']['$'] unless set['settings']['default_lang'].nil?
          default_type = set['settings']['dict_type']['$'] unless set['settings']['dict_type'].nil?
          lang = set['settings']['lang']['$'] unless set['settings']['lang'].nil?
        end
        if default_target.nil? or default_target.to_s == ''
          case lang
          when 'cz'
            default_target = (['cs','sj','en'].include?(default_dict)?'czj':'cs')
          when 'sk'
            default_target = (['cs','sj','en'].include?(default_dict)?'spj':'sj')
          when 'en'
            default_target = (['cs','sj','en'].include?(default_dict)?'asl':'en')
          end
        end
        default_dict = 'cs' if default_dict.to_s == ''
        default_type = 'translate' if default_type.to_s == ''
        default_target = 'czj' if default_target.to_s == ''
        newurl = @servername + '/' + default_dict + '?action='+default_type+'&lang='+lang+'&target='+default_target
        response.set_redirect(WEBrick::HTTPStatus::TemporaryRedirect, newurl)
      end
      #page = 'front' if page == ''
      page = 'frontis' if page == 'front' and dictcode == 'is'
      if page == 'about'
        @dict.get_counts.each{|k,v| template_conf[k] = v}
      end
      file = lang + '-' + page + '.tmpl'
      unless File.exist?(@dict.template_path + '/' + file)
        file = lang + '-front.tmpl'
      end
      $stderr.puts 'PAGE ' + file
      #nacist data ze souboru a vlozit do hlavni sablony
      filedata = IO.read(@dict.template_path + '/' + file)
      template = 'template-main.tmpl'
      templatedata = IO.read(@dict.template_path + '/' + template)
      templatedata.sub!('<!--INSERT_TEMPLATE_PAGE-->', filedata)

      template_conf['template_page'] = '1' 
      template_conf['page'] = page
      response.body = HTMLTemplate.new(template_conf).output_string(templatedata)
      response['Content-Type'] = 'text/html; charset=utf-8'

    when 'front'
      response.body = HTMLTemplate.new().output(@dict.template_path + '/uvod.tmpl')
      response['Content-Type'] = 'text/html; charset=utf-8'
    
    when 'logout'
      response.status = 401

    when 'getperm'
      response.body = @dict.get_perm(user, @dict_array['czj_user']).to_json
      response["Content-type"] = "text/html; charset=utf-8"

    when 'new_entry'
      if @dict.get_perm(user, @dict_array['czj_user']).is_a?(Array)
        perm = @dict.get_perm(user, @dict_array['czj_user']).join(',')
      else
        perm = @dict.get_perm(user, @dict_array['czj_user'])
      end
      skupiny, copy, zdroj, autor = @dict.get_user_info(user, @dict_array['czj_user'])
      set_rel = @dict.get_set_rel(user, @dict_array['czj_user'])
      newid = @server.info.get_next_id(dictcode).to_s
      data = {'newid'=>newid, 'user_skupina'=>skupiny, 'user_perm'=>perm, 'default_copy'=>copy, 'default_zdroj'=>zdroj, 'default_autor'=>autor, 'set_rel'=>set_rel}
      response['Content-Type'] = 'application/json; charset=utf8'
      response.body = data.to_json
    
    when 'getgram'
      key = URI.escape(request.query['id'])
      xml = @dict.get(key)
      if xml == ''
        return ''
      else
        hash = CobraVsMongoose.xml_to_hash(xml)
        if hash['entry']['lemma'] != nil and hash['entry']['lemma']['gram'] != nil
          response['Content-Type'] = 'application/json; charset=utf8'
          response.body = hash['entry']['lemma']['gram'].to_json
          return response.body
        else
          return ''
        end
      end

    when 'getdoc'
      key = URI.escape(request.query['id'])
      transform = request.query['tr']
    
      if @dict.get_perm(user, @dict_array['czj_user']).is_a?(Array)
        perm = @dict.get_perm(user, @dict_array['czj_user']).join(',')
      else
        perm = @dict.get_perm(user, @dict_array['czj_user'])
      end

      begin
        if request.query['plain'] == '1' or dictcode == 'czj_media'
          res = @dict.get(key)
        else
          #res = @dict.get_follow(dictcode, key, false, nil, perm)
          res = @dict.get(key)
        end
        if request.query['perm'] == '1' 
          skupiny, copy, zdroj, autor = @dict.get_user_info(user, @dict_array['czj_user'])
          set_rel = @dict.get_set_rel(user, @dict_array['czj_user'])
          res = res.gsub('<entry ', '<entry user_skupina="'+skupiny+'" user_perm="'+perm+'" default_copy="'+copy+'" default_zdroj="'+zdroj+'" default_autor="'+autor+'" set_rel="'+set_rel+'" ')
        end
        if transform
          lang = 'cz'
          lang = request.query['lang'].to_s if request.query['lang'].to_s != ''
          res = @dict.preprocess_cs(res.to_s) if dictcode == 'cs'
          res.gsub!('hidden', 'published') if res.include?('<completeness>2</completeness>') and perm == 'ro'
          response["Content-type"] = "text/html; charset=utf-8"
          skupiny, copy, zdroj, autor = @dict.get_user_info(user, @dict_array['czj_user'])
          res.gsub!('<entry ', '<entry user_skupina="'+skupiny+'" user_perm="'+perm+'" ')
          xsltparam = [ [ 'perm', '"'+perm+'"'], ['dictcode','"'+dictcode+'"'], ['lang', '"'+lang+'"'], ['video', '"'+video_format+'"'] ]
          xsltparam << ['public', '"true"'] if is_public
          xsltparam << ['empty', '"'+request.query['empty'].to_s+'"'] 
          res = @dict.apply_transform( transform, res.to_s, xsltparam )
        else
          response["Content-type"] = "text/xml; charset=utf-8"
        end
        response.body += res.to_s
        response.body
      rescue => e
        $stderr.puts e
        $stderr.puts e.backtrace
        not_found(response, key, 'cs')
      end
    when 'filelist'
      if request.query['entryid'].to_s != ''
        #list = @dict_array['czj_media'].get_file_list(request.query['entryid'])
        list = @dict.get_entry_files(request.query['entryid'], @dict_array[dictcode+'_media'])
        list = @dict.find_files(request.query['search'], @dict_array[dictcode+'_media'], request.query['type'].to_s) + list if request.query['search'].to_s != ''
        response['Content-Type'] = 'application/json; charset=utf8'
        response.body = list.to_json
      end

    when 'filefind'
      if request.query['search'].to_s != ''
        #list = @dict_array['czj_media'].get_file_list(request.query['entryid'])
        list = @dict.find_files(request.query['search'], @dict_array[dictcode+'_media'])
        response['Content-Type'] = 'application/json; charset=utf8'
        response.body = list.to_json
      end

    when 'relfindcs'
      target = 'cs'
      target = request.query['target'].to_s unless request.query['target'].to_s == ''
      if request.query['search'].to_s != ''
        list = @dict_array[target].find_rel_cs(request.query['search'])
        response['Content-Type'] = 'application/json; charset=utf8'
        response.body = list.to_json
      end
    when 'relfind'
      target = 'czj'
      target = request.query['target'].to_s unless request.query['target'].to_s == ''
      if request.query['search'].to_s != ''
        list = @dict_array[target].find_rel(request.query['search'], @dict_array[target+'_media'])
        response['Content-Type'] = 'application/json; charset=utf8'
        response.body = list.to_json
      end
    when 'linkfind'
      if request.query['search'].to_s != ''
        list = @dict_array['czj'].find_link(request.query['search'])
        response['Content-Type'] = 'application/json; charset=utf8'
        response.body = list.to_json
      end
    when 'linkcsfind'
      if request.query['search'].to_s != ''
        list = @dict_array['cs'].find_link_cs(request.query['search'])
        response['Content-Type'] = 'application/json; charset=utf8'
        response.body = list.to_json
      end


    when 'getfsw'
      fsw = @dict.get_fsw(request.query['sw'], @dict_array['czj_symbol'])
      response.body = fsw
    when 'fromfsw'
      sw = @dict.from_fsw(request.query['fsw'], @dict_array['czj_symbol'])
      response.body = sw

    when 'upload'
      $stderr.puts 'UPLOAD'
      filedata = nil
      filename = nil
      request.query['filebutton'].each_data{|f|
        if not f.nil? and not f.filename.nil? and f.filename != ''
          filedata = f
          filename = f.filename
          $stderr.puts 'FILE ' + f.filename
        end
      }
      $stderr.puts filename
      #$stderr.puts filedata
      $stderr.puts request.query['metadata']
      metadata = JSON.parse(request.query['metadata'])
      if not filedata.nil?
        mediaid, filename = save_uploaded_file(filedata, filename, JSON.parse(request.query['metadata']), request.query['entryid'], dictcode)
        #response['Content-Type'] = 'application/json; charset=utf8'
        response['Content-Type'] = 'text/html; charset=utf8'
        response.body = {'success'=>true, 'message'=>"Soubor nahrán: #{filename} (#{mediaid})"}.to_json
        $stderr.puts response.body
        return response.body
      end
      if filedata.nil? and metadata['@location'].to_s != ''
        @dict_array[dictcode+'_media'].attach_file(metadata['@location'].to_s, request.query['entryid'], metadata)
        response['Content-Type'] = 'text/html; charset=utf8'
        response.body = {'success'=>true, 'message'=>"Soubor připojen: #{metadata['@location']}"}.to_json
        return response.body
      end
      response['Content-Type'] = 'text/html; charset=utf8'
      response.body = {'success'=>false, 'message'=>"Chyba při uploadu"}.to_json
      return response.body
    when 'remove_file'
      if request.query['entry_id'].to_s != '' and request.query['media_id'].to_s != ''
        @dict_array[dictcode+'_media'].remove_file(request.query['media_id'].to_s, request.query['entry_id'].to_s)
        response['Content-Type'] = 'text/html; charset=utf8'
        response.body = {'success'=>true, 'message'=>"Soubor odebrán"}.to_json
        return response.body
      end
      response['Content-Type'] = 'text/html; charset=utf8'
      response.body = {'success'=>false, 'message'=>"Chybí parametry videa"}.to_json
      return response.body
    when 'update_video'
      $stderr.puts request.query['id']
      data_hash = JSON.parse(request.query['data'])
      #update video
      unless data_hash['update_video'].nil?
        data_hash['update_video'].each{|uv|
          @dict_array[dictcode+'_media'].update_media_info(uv)
        }
      end
      cache_entry(dictcode, request.query['id'])
      response.body = '{"success":true,"msg":"Uloženo"}'
      response['Content-Type'] = 'text/html; charset=utf-8'
      return response.body
    when 'save'
      $stderr.puts request.query['id']
      #$stderr.puts request.query['data']
      data_hash = JSON.parse(request.query['data'])
      #update video
      unless data_hash['entry']['update_video'].nil?
        data_hash['entry']['update_video'].each{|uv|
          @dict_array[dictcode+'_media'].update_media_info(uv)
        }
        data_hash['entry'].delete('update_video')
      end

      oldrels = {}
      oldmeans = []
      oldxml = @dict.get(request.query['id'].to_s)
      if oldxml != ''
        orig_hash = CobraVsMongoose.xml_to_hash(oldxml)
        unless orig_hash['entry']['meanings'].nil?
          if orig_hash['entry']['meanings'].is_a?(Array)
            $stderr.puts '1'
            orig_hash['entry']['meanings'].each{|m|
              oldmeans << m['meaning']['@id']
              oldrels[m['meaning']['@id']] = []
              if m['meaning']['relation'].is_a?(Array)
                m['meaning']['relation'].each{|r|
                  oldrels[m['meaning']['@id']] << r
                }
              end
              if m['meaning']['relation'].is_a?(Hash)
                oldrels[m['meaning']['@id']] = [m['meaning']['relation']]
              end
                unless m['usages'].nil?
                  if m['usages']['usage'].is_a?(Array)
                    m['usages']['usage'].each{|usg|
                      oldrels[usg['@id']] = []
                      if usg['relation'].is_a?(Array)
                        usg['relation'].each{|r|
                          oldrels[usg['@id']] << r
                        }
                      end
                      if usg['relation'].is_a?(Hash)
                        oldrels[usg['@id']] = [usg['relation']]
                      end
                    }
                  end
                  if m['usages']['usage'].is_a?(Hash)
                    usg = m['usages']['usage']
                      oldrels[usg['@id']] = []
                      if usg['relation'].is_a?(Array)
                        usg['relation'].each{|r|
                          oldrels[usg['@id']] << r
                        }
                      end
                      if usg['relation'].is_a?(Hash)
                        oldrels[usg['@id']] = [usg['relation']]
                      end
                  end
                end
            }
          end
          if orig_hash['entry']['meanings'].is_a?(Hash)
            $stderr.puts '2'
            $stderr.puts orig_hash['entry']['meanings'].inspect
            if orig_hash['entry']['meanings']['meaning'].is_a?(Array)
              orig_hash['entry']['meanings']['meaning'].each{|m|
                oldmeans << m['@id']
                oldrels[m['@id']] = []
                if m['relation'].is_a?(Array)
                  m['relation'].each{|r|
                    oldrels[m['@id']] << r
                  }
                end
                if m['relation'].is_a?(Hash)
                  oldrels[m['@id']] = [m['relation']]
                end
                unless m['usages'].nil?
                  $stderr.puts m['usages'].inspect
                  if m['usages']['usage'].is_a?(Array)
                    m['usages']['usage'].each{|usg|
                      oldrels[usg['@id']] = []
                      if usg['relation'].is_a?(Array)
                        usg['relation'].each{|r|
                          oldrels[usg['@id']] << r
                        }
                      end
                      if usg['relation'].is_a?(Hash)
                        oldrels[usg['@id']] = [usg['relation']]
                      end
                    }
                  end
                  if m['usages']['usage'].is_a?(Hash)
                    usg = m['usages']['usage']
                      oldrels[usg['@id']] = []
                      if usg['relation'].is_a?(Array)
                        usg['relation'].each{|r|
                          oldrels[usg['@id']] << r
                        }
                      end
                      if usg['relation'].is_a?(Hash)
                        oldrels[usg['@id']] = [usg['relation']]
                      end
                  end
                end
              }
            end
            if orig_hash['entry']['meanings']['meaning'].is_a?(Hash)
            $stderr.puts '3'
              m = orig_hash['entry']['meanings']['meaning']
              oldrels[m['@id']] = []
              oldmeans << m['@id']
              if m['relation'].is_a?(Array)
                m['relation'].each{|r|
                  oldrels[m['@id']] << r
                }
              end
              if m['relation'].is_a?(Hash)
                oldrels[m['@id']] = [m['relation']]
              end
                unless m['usages'].nil?
                  $stderr.puts m['usages'].inspect
                  if m['usages']['usage'].is_a?(Array)
                    m['usages']['usage'].each{|usg|
                      oldrels[usg['@id']] = []
                      if usg['relation'].is_a?(Array)
                        usg['relation'].each{|r|
                          oldrels[usg['@id']] << r
                        }
                      end
                      if usg['relation'].is_a?(Hash)
                        oldrels[usg['@id']] = [usg['relation']]
                      end
                    }
                  end
                  if m['usages']['usage'].is_a?(Hash)
                    usg = m['usages']['usage']
                      oldrels[usg['@id']] = []
                      if usg['relation'].is_a?(Array)
                        usg['relation'].each{|r|
                          oldrels[usg['@id']] << r
                        }
                      end
                      if usg['relation'].is_a?(Hash)
                        oldrels[usg['@id']] = [usg['relation']]
                      end
                  end
                end
            end
          end
          $stderr.puts oldrels.inspect
        end
      end

      #update entry
      to_cache = []
      $stderr.puts data_hash
      #smazane vyznamy
      oldmeans.each{|mi|
        if data_hash['entry']['meanings'].nil? or data_hash['entry']['meanings']['meaning'].select{|m| m['@id']==mi}.length == 0
          $stderr.puts 'REMOVED meaning'+mi
          oldrels[mi].each{|olr|
            $stderr.puts 'smazat relation '+olr['@meaning_id']
            if olr['@type'] == 'translation'
              target = olr['@target'].to_s
              target = (dictcode == 'czj')?'cs':'czj' if target == ''
            else
              target = dictcode
            end
            @dict_array[target].remove_relation(mi, olr['@meaning_id'], olr['@type'], dictcode)
          }
        end
      }
      data_hash['entry']['media'] = []
      unless data_hash['entry']['lemma']['video_front'].nil?
        $stderr.puts data_hash['entry']['lemma']['video_front']['$']
        videoinfoxml = @dict.get_video_info(data_hash['entry']['lemma']['video_front']['$'])
        if videoinfoxml != ''
          videoinfo = CobraVsMongoose.xml_to_hash(videoinfoxml)
          data_hash['entry']['media'] << {'file'=> videoinfo['media']}
        end
      end
      unless data_hash['entry']['lemma']['video_side'].nil?
        $stderr.puts data_hash['entry']['lemma']['video_side']['$']
        videoinfoxml = @dict.get_video_info(data_hash['entry']['lemma']['video_side']['$'])
        if videoinfoxml != ''
          videoinfo = CobraVsMongoose.xml_to_hash(videoinfoxml)
          data_hash['entry']['media'] << {'file'=> videoinfo['media']} 
        end
      end
      #add fsw
      unless data_hash['entry']['lemma']['sw'].nil?
        swnew = []
        data_hash['entry']['lemma']['sw'].each{|sw|
          sw['@fsw'] = @dict.get_fsw(sw['$'], @dict_array['czj_symbol']) if sw['@fsw'] == ''
          swnew << sw
        }
        data_hash['entry']['lemma']['sw'] = swnew
      end
      max_meaning = 0
      unless data_hash['entry']['meanings'].nil?
        data_hash['entry']['meanings']['meaning'].each{|hash|
          if hash['@id'] != ''
            mid = hash['@id'].split('-').last.to_i
            max_meaning = mid if mid > max_meaning
          end
        }
      end

      unless data_hash['entry']['lemma']['grammar_note'].nil?
        #pridat variant
        if data_hash['entry']['lemma']['grammar_note'][0] != nil and data_hash['entry']['lemma']['grammar_note'][0]['variant'] != nil and data_hash['entry']['lemma']['video_front'] != nil
          data_hash['entry']['lemma']['grammar_note'][0]['variant'].each{|var|
            $stderr.puts 'pridat variant '+var['$']
            to_cache += @dict.add_variant(@dict_array[dictcode+'_media'], data_hash['entry']['lemma']['video_front']['$'], var['$'], 'grammar_note')
            
          }
        end
      end
      unless data_hash['entry']['lemma']['style_note'].nil? 
        #pridat variant
        if data_hash['entry']['lemma']['style_note']['variant'] != nil and data_hash['entry']['lemma']['video_front'] != nil
          data_hash['entry']['lemma']['style_note']['variant'].each{|var|
            $stderr.puts 'pridat variant '+var['$']
            to_cache += @dict.add_variant(@dict_array[dictcode+'_media'], data_hash['entry']['lemma']['video_front']['$'], var['$'], 'style_note')
          }
        end
      end

      unless data_hash['entry']['meanings'].nil?
        data_hash['entry']['meanings']['meaning'].each{|hash|
          if hash['@id'] == ''
            max_meaning += 1
            hash['@id'] = request.query['id'].to_s + '-' + max_meaning.to_s
          end

          #kontrola relation, smazane
          unless oldrels[hash['@id']].nil?
            oldrels[hash['@id']].each{|olr|
              if hash['relation'].select{|r| r['@meaning_id']==olr['@meaning_id'] and r['@type']==olr['@type']}.length == 0
                $stderr.puts 'smazat relation '+olr['@meaning_id']
                if olr['@type'] == 'translation'
                  target = olr['@target'].to_s
                  target = (dictcode == 'czj')?'cs':'czj' if target == ''
                else
                  target = dictcode
                end
                @dict_array[target].remove_relation(hash['@id'], olr['@meaning_id'], olr['@type'], dictcode) #unless olr['@meaning_id'].include?('_us')
              end
            }
          end
          #pridat relation
          hash['relation'].each{|rel|
            $stderr.puts 'pridat relation '+rel['@meaning_id']
            if rel['@type'] == 'translation'
              target = rel['@target'].to_s
              target = (dictcode == 'czj')?'cs':'czj' if target == ''
            else
              target = dictcode
            end
            @dict_array[target].add_relation(hash['@id'], rel['@meaning_id'], rel['@type'], rel['@status'], dictcode) #unless rel['@meaning_id'].include?('_us')
            if rel['@meaning_id'].include?('_us') and rel['@status'] == 'published' and /^([0-9]*)-.*/.match(rel['@meaning_id']) != nil
              @dict_array[target].publish_relation(/^([0-9]*)-.*/.match(rel['@meaning_id'])[1].to_s, rel['@meaning_id'], hash['@id'], rel['@type'])
            end
          }
          unless hash['usages'].nil?
            hash['usages']['usage'].each{|usg|
              #smazane relation v prikladech
              unless oldrels[usg['@id']].nil?
                oldrels[usg['@id']].each{|olr|
                  if usg['relation'].nil? or usg['relation'].select{|r| r['@meaning_id']==olr['@meaning_id'] and r['@type']==olr['@type']}.length == 0
                    $stderr.puts 'smazat relation '+olr['@meaning_id']
                    if olr['@type'] == 'translation'
                      target = olr['@target'].to_s
                      target = (dictcode == 'czj')?'cs':'czj' if target == ''
                    else
                      target = dictcode
                    end
                    @dict_array[target].remove_relation(usg['@id'], olr['@meaning_id'], olr['@type'], dictcode) 
                  end
                }
              end

              unless usg['relation'].nil?
                #pridat relation v prikladech
                usg['relation'].each{|rel|
                  $stderr.puts 'pridat relation '+rel['@meaning_id']
                  if rel['@type'] == 'translation'
                    target = rel['@target'].to_s
                    target = (dictcode == 'czj')?'cs':'czj' if target == ''
                  else
                    target = dictcode
                  end
                  @dict_array[target].add_relation(usg['@id'], rel['@meaning_id'], rel['@type'], usg['status']['$'], dictcode) #unless rel['@meaning_id'].include?('_us')
                  if rel['@meaning_id'].include?('_us') and usg['status']['$'] == 'published' and /^([0-9]*)-.*/.match(rel['@meaning_id']) != nil
                    @dict_array[target].publish_relation(/^([0-9]*)-.*/.match(rel['@meaning_id'])[1].to_s, rel['@meaning_id'], usg['@id'], rel['@type'])
                  end
                }
              end
            }
          end
        }
      end

      $stderr.puts data_hash
      xml = CobraVsMongoose.hash_to_xml(data_hash)
      xml.gsub!(/&lt;file media_id=&quot;([0-9]*)&quot;\/&gt;/, '<file media_id="\1"/>')
      $stderr.puts xml
      @dict.update(request.query['id'], xml.to_s)
      fast = false
      fast = true if request.query['fast'] == 'true'
      cache_entry(dictcode, request.query['id'], fast)
      @dict.update(request.query['id'], @dict.get_follow(dictcode, request.query['id'], false, nil, 'admin'))
      response.body = '{"success":true,"msg":"Uloženo"}'
      response['Content-Type'] = 'text/html; charset=utf-8'
      return response.body

    when 'savecs'
      $stderr.puts request.query['id']
      #$stderr.puts request.query['data']
      data_hash = JSON.parse(request.query['data'])

      oldrels = {}
      oldmeans = []
      oldxml = @dict.get(request.query['id'].to_s)
      if oldxml != ''
        orig_hash = CobraVsMongoose.xml_to_hash(oldxml)
        unless orig_hash['entry']['meanings'].nil?
          if orig_hash['entry']['meanings'].is_a?(Array)
            orig_hash['entry']['meanings'].each{|m|
              oldmeans << m['meaning']['@id']
              oldrels[m['meaning']['@id']] = []
              if m['meaning']['relation'].is_a?(Array)
                m['meaning']['relation'].each{|r|
                  oldrels[m['meaning']['@id']] << r
                }
              end
              if m['meaning']['relation'].is_a?(Hash)
                oldrels[m['meaning']['@id']] = [m['meaning']['relation']]
              end
            }
          end
          if orig_hash['entry']['meanings'].is_a?(Hash)
            $stderr.puts orig_hash['entry']['meanings'].inspect
            if orig_hash['entry']['meanings']['meaning'].is_a?(Array)
              orig_hash['entry']['meanings']['meaning'].each{|m|
                oldmeans << m['@id']
                oldrels[m['@id']] = []
                if m['relation'].is_a?(Array)
                  m['relation'].each{|r|
                    oldrels[m['@id']] << r
                  }
                end
                if m['relation'].is_a?(Hash)
                  oldrels[m['@id']] = [m['relation']]
                end
                if m['usages'] != nil and m['usages'].is_a?(Hash)
                  if m['usages']['usage'].is_a?(Hash)
                    u = m['usages']['usage']
                    oldrels[u['@id']] = []
                    if u['relation'].is_a?(Array)
                      u['relation'].each{|r|
                        oldrels[u['@id']] << r
                      }
                    end
                    if u['relation'].is_a?(Hash)
                      oldrels[u['@id']] = [u['relation']]
                    end
                  end
                  if m['usages']['usage'].is_a?(Array)
                    m['usages']['usage'].each{|u|
                      oldrels[u['@id']] = []
                      if u['relation'].is_a?(Array)
                        u['relation'].each{|r|
                          oldrels[u['@id']] << r
                        }
                      end
                      if u['relation'].is_a?(Hash)
                        oldrels[u['@id']] = [u['relation']]
                      end
                    }
                  end
                end
              }
            end
            if orig_hash['entry']['meanings']['meaning'].is_a?(Hash)
              m = orig_hash['entry']['meanings']['meaning']
              oldmeans << m['@id']
              oldrels[m['@id']] = []
              if m['relation'].is_a?(Array)
                m['relation'].each{|r|
                  oldrels[m['@id']] << r
                }
              end
              if m['relation'].is_a?(Hash)
                oldrels[m['@id']] = [m['relation']]
              end
            end
          end
          $stderr.puts 'OLDRELS ' + oldrels.inspect
        end
        oldvars_g = []
        unless orig_hash['entry']['lemma']['grammar_note'].nil?
          if orig_hash['entry']['lemma']['grammar_note'].is_a?(Hash)
            if orig_hash['entry']['lemma']['grammar_note']['variant'].is_a?(Hash)
              oldvars_g << orig_hash['entry']['lemma']['grammar_note']['variant']['$']
            end
            if orig_hash['entry']['lemma']['grammar_note']['variant'].is_a?(Array)
              orig_hash['entry']['lemma']['grammar_note']['variant'].each{|var|
                oldvars_g << var['$']
              }
            end
          else
            unless orig_hash['entry']['lemma']['grammar_note'][0].nil?
              if orig_hash['entry']['lemma']['grammar_note'][0]['variant'].is_a?(Hash)
                oldvars_g << orig_hash['entry']['lemma']['grammar_note'][0]['variant']['$']
              end
              if orig_hash['entry']['lemma']['grammar_note'][0]['variant'].is_a?(Array)
                orig_hash['entry']['lemma']['grammar_note'][0]['variant'].each{|var|
                  oldvars_g << var['$']
                }
              end
            end
          end
        end
        oldvars_s = []
        unless orig_hash['entry']['lemma']['style_note'].nil?
            if orig_hash['entry']['lemma']['style_note']['variant'].is_a?(Hash)
              oldvars_s << orig_hash['entry']['lemma']['style_note']['variant']['$']
            end
            if orig_hash['entry']['lemma']['style_note']['variant'].is_a?(Array)
              orig_hash['entry']['lemma']['style_note']['variant'].each{|var|
                oldvars_s << var['$']
              }
            end
        end
        $stderr.puts oldvars_g.inspect
      end

      #update entry
      $stderr.puts data_hash
      #smazane vyznamy
      oldmeans.each{|mi|
        if data_hash['entry']['meanings'].nil? or data_hash['entry']['meanings']['meaning'].select{|m| m['@id']==mi}.length == 0
          $stderr.puts 'REMOVED meaning'+mi
          oldrels[mi].each{|olr|
            $stderr.puts 'smazat relation '+olr['@meaning_id']
            if olr['@type'] == 'translation'
              target = olr['@target'].to_s
              target = (dictcode == 'czj')?'cs':'czj' if target == ''
            else
              target = dictcode
            end
            @dict_array[target].remove_relation(mi, olr['@meaning_id'], olr['@type'], dictcode)
          }
        end
      }

      max_meaning = 0
      unless data_hash['entry']['meanings'].nil?
        data_hash['entry']['meanings']['meaning'].each{|hash|
          if hash['@id'] != ''
            mid = hash['@id'].split('-').last.to_i
            max_meaning = mid if mid > max_meaning
          end
        }

        data_hash['entry']['meanings']['meaning'].each{|hash|
          if hash['@id'] == ''
            max_meaning += 1
            hash['@id'] = request.query['id'].to_s + '-' + max_meaning.to_s
          end

          #kontrola relation, smazane
          unless oldrels[hash['@id']].nil?
            oldrels[hash['@id']].each{|olr|
              #$stderr.puts '----'
              #$stderr.puts olr
              #$stderr.puts hash['relation'].select{|r| r['@meaning_id']==olr['@meaning_id'] and r['@type']==olr['@type']}
              if hash['relation'].select{|r| r['@meaning_id']==olr['@meaning_id'] and r['@type']==olr['@type']}.length == 0
                $stderr.puts 'smazat relation '+olr['@meaning_id']
                if olr['@type'] == 'translation'
                  target = olr['@target'].to_s
                  target = (dictcode == 'czj')?'cs':'czj' if target == ''
                else
                  target = dictcode
                end
                @dict_array[target].remove_relation(hash['@id'], olr['@meaning_id'], olr['@type'], dictcode)
              end
            }
          end
          #pridat relation
          hash['relation'].each{|rel|
            $stderr.puts 'test relation '+rel['@meaning_id']
            if oldrels[hash['@id']].nil? or oldrels[hash['@id']].select{|r| r['@meaning_id']==rel['@meaning_id'] and r['@type']==rel['@type'] and r['@type']==rel['@type'] and r['@status']==rel['@status']}.length == 0
              $stderr.puts 'pridat relation '+rel['@meaning_id']
              if rel['@type'] == 'translation'
                target = rel['@target'].to_s
                target = (dictcode == 'czj')?'cs':'czj' if target == ''
              else
                target = dictcode
              end
              @dict_array[target].add_relation(hash['@id'], rel['@meaning_id'], rel['@type'], rel['@status'], dictcode)
            end
          }
          #priklady, odkazy na spojeni
          unless hash['usages'].nil?
            hash['usages']['usage'].each{|ushash|
              #kontrola relation, smazane
              unless oldrels[ushash['@id']].nil?
                oldrels[ushash['@id']].each{|olr|
                  if ushash['relation'] != nil and ushash['relation'].select{|r| r['@meaning_id']==olr['@meaning_id'] and r['@type']==olr['@type']}.length == 0
                    $stderr.puts 'smazat relation usage '+olr['@meaning_id']
                    if olr['@type'] == 'translation'
                      target = olr['@target'].to_s
                      target = (dictcode == 'czj')?'cs':'czj' if target == ''
                    else
                      target = dictcode
                    end
                    @dict_array[target].remove_relation(ushash['@id'], olr['@meaning_id'], olr['@type'], dictcode)
                  end
                }
              end
              #pridat relation
              unless ushash['relation'].nil?
                ushash['relation'].each{|rel|
                  $stderr.puts 'test relation usage '+rel['@meaning_id']
                  if oldrels[ushash['@id']].nil? or oldrels[ushash['@id']].select{|r| r['@meaning_id']==rel['@meaning_id'] and r['@type']==rel['@type'] and r['@type']==rel['@type'] and r['@status']==rel['@status']}.length == 0
                    $stderr.puts 'pridat relation usage '+rel['@meaning_id']
                    if rel['@type'] == 'translation'
                      target = rel['@target'].to_s
                      target = (dictcode == 'czj')?'cs':'czj' if target == ''
                    else
                      target = dictcode
                    end
                    @dict_array[target].add_relation(ushash['@id'], rel['@meaning_id'], rel['@type'], ushash['status']['$'], dictcode)
                  end
                }
              end

              #if ushash['@colloc_link'].to_s != ''
              #  usstatus = 'hidden'
              #  usstatus = ushash['status']['$'].to_s unless ushash['status'].nil?
              #  @dict_array['czj'].add_relation(ushash['@id'], ushash['@colloc_link'], 'translation', ushash['status']['$'], dictcode)
              #end
            }
          end
        }
      end
      #odebrat varianty
      unless oldvars_g.nil?
        if data_hash['entry']['lemma']['grammar_note'].nil? or data_hash['entry']['lemma']['grammar_note'][0].nil? or data_hash['entry']['lemma']['grammar_note'][0]['variant'].nil?
          oldvars_g.each{|var|
            $stderr.puts 'odebrat propojeni varianty z ' + var
            @dict.remove_variant_cs(request.query['id'], var, 'grammar_note')
          }
        else
          oldvars_g.each{|var|
            $stderr.puts 'check var ' + var
            if data_hash['entry']['lemma']['grammar_note'][0]['variant'].select{|r| r['$'] == var}.length == 0
              $stderr.puts 'odebrat propojeni varianty z ' + var 
              @dict.remove_variant_cs(request.query['id'], var, 'grammar_note')
            end
          }
        end
      end
      #pridat varianty
      unless data_hash['entry']['lemma']['grammar_note'].nil?
        if data_hash['entry']['lemma']['grammar_note'][0] != nil and data_hash['entry']['lemma']['grammar_note'][0]['variant'] != nil 
          data_hash['entry']['lemma']['grammar_note'][0]['variant'].each{|var|
            if var['$'] != '' and not oldvars_g.include?(var['$'])
              $stderr.puts 'pridat variantu do '+var['$']
              @dict.add_variant_cs(request.query['id'], var['$'], 'grammar_note')
            end
          }
        end
      end
      #odebrat varianty
      unless oldvars_s.nil?
        if data_hash['entry']['lemma']['style_note'].nil? or data_hash['entry']['lemma']['style_note']['variant'].nil?
          oldvars_s.each{|var|
            $stderr.puts 'odebrat propojeni varianty z ' + var
            @dict.remove_variant_cs(request.query['id'], var, 'style_note')
          }
        else
          oldvars_s.each{|var|
            $stderr.puts 'check var ' + var
            if data_hash['entry']['lemma']['style_note']['variant'].select{|r| r['$'] == var}.length == 0
              $stderr.puts 'odebrat propojeni varianty z ' + var 
              @dict.remove_variant_cs(request.query['id'], var, 'style_note')
            end
          }
        end
      end
      #pridat varianty
      unless data_hash['entry']['lemma']['style_note'].nil?
        if data_hash['entry']['lemma']['style_note']['variant'] != nil 
          data_hash['entry']['lemma']['style_note']['variant'].each{|var|
            if var['$'] != '' and not oldvars_s.include?(var['$'])
              $stderr.puts 'pridat variantu do '+var['$']
              @dict.add_variant_cs(request.query['id'], var['$'], 'style_note')
            end
          }
        end
      end

      $stderr.puts data_hash
      xml = CobraVsMongoose.hash_to_xml(data_hash)
      xml.gsub!(/&lt;file media_id=&quot;([0-9]*)&quot;\/&gt;/, '<file media_id="\1"/>')
      if oldxml != ''
        olddoc = @dict.load_xml_string(oldxml)
        newdoc = @dict.load_xml_string(xml)
        if olddoc.find('/entry/html').size > 0
          newdoc.root << olddoc.find('/entry/html').first.copy(true)
        end
        #if olddoc.find('/entry/lemma/gram').size > 0
        #  newdoc.find('/entry/lemma').first << olddoc.find('/entry/lemma/gram').first.copy(true)
        #end
        xml = newdoc.to_s
      end
      $stderr.puts xml
      @dict.update(request.query['id'], xml.to_s)
      #newxml = @dict.get_follow(dictcode, request.query['id'], false, nil, 'admin')
      fast = false
      fast = true if request.query['fast'] == 'true'
      cache_entry(dictcode, request.query['id'], fast)
      response.body = '{"success":true,"msg":"Uloženo"}'
      if request.query['shownext'].to_s == '1'
        gotid = false
        curid = request.query['id'].to_i
        while not gotid and (curid - request.query['id'].to_i < 10)
          curid += 1
          if @dict.get(curid.to_s) != ''
            gotid = true
          end
        end
        response.body = '{"success":true,"msg":"Uloženo","shownext":"'+curid.to_s+'"}' if gotid
      end
      response['Content-Type'] = 'text/html; charset=utf-8'
      return response.body

    when 'search', 'translate'
      if request.query['action'] == 'search' and request.query['getdoc'].to_s != '' and request.query['empty'] == 'true'
        newurl = @servername + '/' + dictcode + '?action=getdoc&id='+request.query['getdoc'].to_s + '&tr=preview&empty=true&lang='+request.query['lang'].to_s+'&video='+video_format
        response.set_redirect(WEBrick::HTTPStatus::TemporaryRedirect, newurl)
      end
      if @dict.get_perm(user, @dict_array['czj_user']).is_a?(Array)
        perm = @dict.get_perm(user, @dict_array['czj_user']).join(',')
      else
        perm = @dict.get_perm(user, @dict_array['czj_user'])
      end
      $stderr.puts perm
      file = 'template-' + request.query['action'].to_s + '-' + dictcode + '.tmpl'
      unless File.exist?(@dict.template_path + '/' + file)
        if ['cs','en','sj'].include?(dictcode)
          file = 'template-' + request.query['action'].to_s + '-write.tmpl'
        else
          file = 'template-' + request.query['action'].to_s + '-sign.tmpl'
        end
      end

      type = request.query['type'].to_s
      type = 'img' if type == ''
      search_term = request.query['search'].to_s.strip
      search_term = '' if search_term == '||j||'
      sort_order = 'misto'
      sort_order = request.query['sort_order'].to_s if request.query['sort_order'].to_s != ''
      list = []
      count = 0
      count = 1 if request.query['getdoc'].to_s != ''
      moreresults = false
      skupiny, copy, zdroj, autor = @dict.get_user_info(user, @dict_array['czj_user'])
      target = ''
      if dictcode == 'cs' or dictcode == 'en' or dictcode == 'sj'
        page_limit = 20
        deklin = false
        deklin = true if request.query['deklin'].to_s == 'on'
        diak = false
        diak = true if request.query['diak'].to_s == 'on'
        spojeni = false
        spojeni = true if request.query['spojeni'].to_s == 'on'
        searchcs_czj = false
        searchcs_czj = true if request.query['searchcs-czj'] == 'on'
        if request.query['action'] == 'translate'
          case request.query['lang']
          when 'cz'
            target = 'czj'
          when 'sk'
            target = 'spj'
          when 'en'
            target = 'asl'
          end
          target = request.query['target'].to_s if request.query['target'].to_s != ''
          if search_term != ''
            list, count, trancount = @dict.translate_cs(search_term, searchcs_czj, @dict_array['czj'], perm, request.query['page'].to_i, skupiny, is_public, deklin, target) 
          else
            extend_translation = false
            extend_translation = true if dictcode == 'en' or dictcode == 'sj' or target == 'en' or target == 'sj'
            list, count, trancount = @dict.translate_allcs(search_term, searchcs_czj, @dict_array['czj'], perm, request.query['page'].to_i, skupiny, is_public, deklin, target, extend_translation) 
            template_conf['default_search'] = '1'
          end
        else
          if request.query['getdoc'].to_s != ''
            list = []
            count = 1
          elsif search_term != ''
            list, count = @dict.search_cs(search_term, searchcs_czj, @dict_array['czj'], perm, request.query['page'].to_i, skupiny, is_public, deklin, diak, spojeni)
          else
            list, count = @dict.search_allcs(search_term, searchcs_czj, @dict_array['czj'], perm, request.query['page'].to_i, skupiny, is_public, deklin)
            template_conf['default_search'] = '1'
          end
        end
        if is_public
          list.each{|entry|
            entry['public'] = 2
            unless entry['spojeni'].nil?
              entry['spojeni'].each{|spoj|
                spoj['public'] = '1'
                unless spoj['translations'].nil?
                  spoj['translations'].each{|spojt|
                    spojt['public'] = '1'
                  }
                end
              }
            end
            unless entry['translations'].nil?
              entry['translations'].each{|spoj|
                spoj['public'] = '1'
              }
            end
          }
        end
      else
        page_limit = 5
        if request.query['action'] == 'translate'
          case request.query['lang']
          when 'cz'
            target = 'cs'
          when 'sk'
            target = 'sj'
          when 'en'
            target = 'en'
          end
          page_limit = 10
          target = request.query['target'].to_s if request.query['target'].to_s != ''
          if search_term != '' or request.query['slovni_druh'].to_s != ''
            list, count, moreresults, trancount = @dict.translate(search_term, type, perm, request.query['page'].to_i, skupiny, is_public, sort_order, target)
          else
            list, count, moreresults, trancount = @dict.translate_all(search_term, type, perm, request.query['page'].to_i, skupiny, is_public, sort_order, target)
            template_conf['default_search'] = '1'
          end
        else
          if request.query['getdoc'].to_s != ''
            list = []
            count = 1
            moreresults = 0
          elsif search_term != '' or request.query['slovni_druh'].to_s != ''
            list, count, moreresults = @dict.search(search_term, type, perm, request.query['page'].to_i, skupiny, is_public, sort_order, request.query['slovni_druh'].to_s) 
          else
            list, count, moreresults = @dict.search_all(search_term, type, perm, request.query['page'].to_i, skupiny, is_public, sort_order, request.query['slovni_druh'].to_s) 
            template_conf['default_search'] = '1'
          end
        end
      end
      list.each{|entry|
        entry['video_'+video_format] = '1'
        entry['video_format'] = video_format
      }
      $stderr.puts list
      @dict_array.each{|dc,da|
        template_conf['active_action_'+dc] = request.query['action'].to_s.dup.force_encoding('UTF-8')
      }
      template_conf['active_action_'+dictcode] = ((request.query['action']=='search')?'translate':'search') + '&search='+request.query['search'].to_s.dup.force_encoding('UTF-8')+'&type='+request.query['type'].to_s
      template_conf['template_'+request.query['action'].to_s] = '1'
      template_conf['list'] = list
      template_conf['count'] = count
      template_conf['search'+type] = request.query['search'].to_s.dup.force_encoding('UTF-8')
      template_conf['type'] = type
      template_conf['translation_count'] = trancount unless trancount.nil? or trancount == 0
      template_conf['dictcode'] = dictcode
      template_conf['dictcode_'+dictcode] = '1'
      template_conf['target'] = target
      template_conf['target_'+target] = '1'
      template_conf['is_editor'] = '1' if perm.include?('edit') or perm.include?('admin')
      template_conf['user'] = user
      template_conf['sort_order_'+sort_order] = '1'
      template_conf['slovni_druh-'+request.query['slovni_druh'].to_s] = '1'
      template_conf['searchcs_czj'] = '1' if request.query['searchcs-czj'].to_s == 'on'
      template_conf['deklin'] = '1' if request.query['deklin'].to_s == 'on'
      template_conf['diak'] = '1' if request.query['diak'].to_s == 'on'
      template_conf['spojeni'] = '1' if request.query['spojeni'].to_s == 'on'
      template_conf['page'] = request.query['page'].to_i + 1 
      template_conf['page_count'] = (count.to_f/page_limit).ceil if (count.to_f/page_limit).ceil > 1
      template_conf['next_page'] = request.query['page'].to_i + 1 unless ((request.query['page'].to_i+1)*page_limit).to_i >= count.to_i
      template_conf['prev_page'] = request.query['page'].to_i - 1 unless request.query['page'].to_i == 0
      template_conf['more_result'] = '1' if moreresults
      template_conf['search'] = URI.escape(search_term)
      template_conf['type'] = request.query['type'].to_s
      template_conf['type_'+request.query['type'].to_s] = '1'
      template_conf['active_dict_'+dictcode] = (dictcode + '-active').to_s.dup.force_encoding('UTF-8')
      $stderr.puts file
      #$stderr.puts tmpl_params['list']
      #nacist data ze souboru a vlozit do hlavni sablony
      filedata = IO.read(@dict.template_path + '/' + file)
      template = 'template-main.tmpl'
      templatedata = IO.read(@dict.template_path + '/' + template)
      templatedata.sub!('<!--INSERT_TEMPLATE_'+request.query['action'].to_s.upcase+'-->', filedata)

      response.body = HTMLTemplate.new(template_conf).output_string(templatedata)
      response['Content-Type'] = 'text/html; charset=utf-8'

    when 'get_comments'
      box = request.query['box'].to_s
      entry = request.query['entry'].to_s
      result = []
      if box != '' and entry != ''
        result = @dict_array['czj_koment'].get_comments(dictcode, user, entry, box)
      end
      response['Content-Type'] = 'application/json; charset=utf8'
      response.body = {'comments'=>result}.to_json

    when 'add_comment'
      box = request.query['box'].to_s
      entry = request.query['entry'].to_s
      text = request.query['text'].to_s
      if box != '' and entry != '' and text != ''
        @dict_array['czj_koment'].add_comment(dictcode, user, entry, box, text)
        open('/var/lib/deb-server/cacheczj/'+dictcode+'.lst','a'){|f|
          f.puts entry
        }
      end
      response.body = '{"success":true,"msg":"Uloženo"}'
      response['Content-Type'] = 'text/html; charset=utf-8'
      return response.body

    when 'del_comment'
      if request.query['id'].to_s != ''
        @dict_array['czj_koment'].del_comment(request.query['id'].to_s)
      end
      response.body = '{"success":true,"msg":"Uloženo"}'
      response['Content-Type'] = 'text/html; charset=utf-8'
      return response.body

    when 'publish_video'
      if request.query['id'].to_s != ''
        @dict_array[dictcode+'_media'].publish_video(request.query['id'].to_s)
      end
      response.body = '{"success":true,"msg":"ok"}'
      response['Content-Type'] = 'text/html; charset=utf-8'
      return response.body

    when 'publish_all_cs'
      if request.query['id'].to_s != '' 
        to_change = @dict.publish_all_cs(request.query['id'].to_s)
        cache_entry(dictcode, request.query['id'].to_s)
      end
      response.body = 'ok'
    when 'get_relation_info'
      rels = ''
      if request.query['meaning_id'].to_s != ''
        rels = @dict.get_relation_info(request.query['meaning_id'].to_s)
      end
      response.body = rels
    when 'get_relations'
      rels = ''
      if request.query['meaning_id'].to_s != ''
        rels = @dict.get_relations(request.query['meaning_id'].to_s, request.query['type'].to_s).to_json
      end
      response.body = rels
    when 'publish_relation'
      if request.query['id'].to_s != '' and request.query['meaning_id'].to_s != '' and request.query['rel_id'].to_s != '' and request.query['type'].to_s != ''
        to_change = @dict.publish_relation(request.query['id'].to_s, request.query['meaning_id'].to_s, request.query['rel_id'].to_s, request.query['type'].to_s, true)
        to_change.each{|info|
          @dict_array[info['target']].publish_relation(info['entry_id'], request.query['rel_id'].to_s, request.query['meaning_id'].to_s, request.query['type'].to_s, false)
        }
        cache_entry(dictcode, request.query['id'].to_s)
      end
      response.body = 'ok'
    when 'publish_usage'
      if request.query['id'].to_s != '' and request.query['meaning_id'].to_s != '' and request.query['usage_id'].to_s != ''
        @dict.publish_usage(request.query['id'].to_s, request.query['meaning_id'].to_s, request.query['usage_id'].to_s, @dict_array[dictcode+'_media'])
      end
      response.body = 'ok'
    when 'publish_meaning'
      if request.query['id'].to_s != '' and request.query['meaning_id'].to_s != ''
        @dict.publish_meaning(request.query['id'].to_s, request.query['meaning_id'].to_s, @dict_array[dictcode+'_media'])
      end
      response.body = 'ok'
    when 'publish'
      if request.query['id'].to_s != '' and request.query['type'].to_s != ''
        @dict.publish(request.query['id'].to_s, request.query['type'].to_s, @dict_array[dictcode+'_media'])
        cache_entry(dictcode, request.query['id'].to_s)
      end
      response.body = 'ok'

    when 'delete'
      $stderr.puts 'delete ' + request.query['id'].to_s
      if @dict.get_perm(user, @dict_array['czj_user']).is_a?(Array)
        perm = @dict.get_perm(user, @dict_array['czj_user']).join(',')
      else
        perm = @dict.get_perm(user, @dict_array['czj_user'])
      end
      if perm.include?('admin') or perm.include?('revizor') or perm.include?('edit')
        id = URI.escape(request.query['id'])
        lock = @server.info.who_locked(dictcode, id)
        if lock != false and lock != user
          response['Content-Type'] = 'text/plain; charset=utf8'
          response.body = 'this entry is locked by ' + lock.to_s
          return response.body
        end        
        if id.to_s != ''
          doc = @dict.load_xml_string(@dict.get(id))
          @dict.remove_all_relations(dictcode,id)
          doc.find('/entry/collocations/revcolloc').each{|rev|
            $stderr.puts 'remove colloc ' + rev['lemma_id']
            @dict.remove_colloc(id, rev['lemma_id'])
          }
          @dict.delete(id)
          doc.find('/entry/collocations/colloc').each{|rev|
            if rev['lemma_id'].to_s != ''
              @dict.update(rev['lemma_id'].to_s, @dict.get_follow(dictcode, rev['lemma_id'].to_s, false, nil, 'admin'))
            end
          }
          response['Content-Type'] = 'text/plain; charset=utf8'
          response.body = 'DELETED ' + id
          return response.body
        end
      else
        response['Content-Type'] = 'text/plain; charset=utf8'
        response.body = 'not authorized to delete entry'
        return response.body        
      end
    when 'update_all'
      @dict_array['czj'].update_all

    when 'update_allcs'
      @dict_array['cs'].update_allcs

    when 'update_entry'
      @dict.update(request.query['id'].to_s, @dict.get_follow(dictcode, request.query['id'].to_s, false, nil, 'admin'))

    when 'get_settings'
      $stderr.puts 'GET settings'
      if user != 'read'
        set = @server.info.get_settings(user, @server.info.service_name)
      else
        set = {}
      end
      response["Content-type"] = "application/json; charset=utf-8"
      response.body = set.to_json
    when 'save_settings'
      $stderr.puts 'save settings'
      if user != 'read'
        data = URI.unescape(request.query['data']).to_s
        data_hash = JSON.parse(data)
        @dict_array['czj'].loaded_cookie.delete(user)
        set = @server.info.save_settings(user, data_hash, @server.info.service_name)
      else
        data_hash = {}
      end
      response["Content-type"] = "application/json; charset=utf-8"
      response.body = data_hash.to_json


    else
      super(request, response)
    end
  end
  
  alias do_POST do_GET
end
