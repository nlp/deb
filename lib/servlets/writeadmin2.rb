require 'template'
require 'uri'
require 'tempfile'
require 'import'
require 'base64'

class WriteAdminServlet < DictServlet
  def initialize( server, dict, array )
    super
    @servername = nil
  end

  def add_jsonp(data, callback)
    return callback + "(" + data + ");"
  end

  def do_GET(request, response)
    @servername = get_servername(@server, request) if @servername == nil
    request.query['action'] = 'main' if request.query['action'] == nil or request.query['action'] == ''
    user = request.attributes[:auth_user].to_s
    dict_path = request.path[1..-1]

    case request.query['action']
    #when 'main'
    #  tmpl_param = @dict.get_user_dicts(user)
    #  tmpl_param['my_dict'] = 1 unless (tmpl_param['dict_m']+tmpl_param['dict_w']+tmpl_param['dict_r']).size == 0
    #  res = HTMLTemplate.new(tmpl_param).output(@dict.template_path + '/main.tmpl')
    #  response.body = res
    when 'dict_list'
      output = {'status'=>'OK'}.merge(@dict.get_user_dicts(user))
      response.body = output.to_json
    when 'dict_info'
      info = {}
      dictcode = request.query['code'].to_s 
      if dictcode == ''
        info['status'] = 'error'
        info['text'] = 'dictonary code not specified'
      elsif @dict.get_dict_info(dictcode).nil?
        info['status'] = 'error'
        info['text'] = 'dictonary code not existing: '+dictcode
      else
        info = @dict.get_dict_info(dictcode)
        info['status'] = 'OK'
        info['owner'] = request.query['code'][0,dictcode.index('_')]
        info['users'] = @dict.get_dict_users(dictcode)
        info['templates'] = @dict.get_dict_templates(dictcode, @server.info.xslt_path)
        info['htemplates'] = @dict.get_dict_htemplates(dictcode, @server.info.xslt_path)
        info['current_user'] = user
        info['current_perm'] = @dict.array[dictcode].get_perm(user)
      end
      response.body = info.to_json
    #when 'dict_edit'
    #  tmpl_param = {}
    #  if request.query['dict'].to_s != ''
    #    tmpl_param = @dict.get_dict_info(request.query['dict'].to_s)
    #    tmpl_param['edit'] = 1 
    #    tmpl_param['exist'] = 1 if request.query['exist'].to_s == '1'
    #    tmpl_param['owner'] = request.query['dict'][0,request.query['dict'].index('_')]
    #    tmpl_param['users'] = @dict.get_dict_users(request.query['dict']).to_json
    #    tmpl_param['templates'] = @dict.get_dict_templates(request.query['dict'], @server.info.xslt_path).to_json
    #    tmpl_param['htemplates'] = @dict.get_dict_htemplates(request.query['dict'], @server.info.xslt_path).to_json
    #  end
    #  res = HTMLTemplate.new(tmpl_param).output(@dict.template_path + '/dictedit.tmpl')
    #  response.body = res
    when 'dict_save'
      code = request.query['dict_code'].to_s
      code = user + '_' + code if request.query['edit'].to_s == ''
      name = request.query['dict_name'].gsub(/[<>&]/, '')
      $stderr.puts code
      $stderr.puts @dict.get('dict_'+code)
      info = {}
      if request.query['edit'].to_s != '1' and @dict.get('dict_'+code).to_s != ''
        info['status'] = 'error'
        info['text'] = 'dictionary code '+code+' is already used'
      elsif request.query['edit'].to_s == '1' and @dict.get_perm(user, code) != 'm'
        info['status'] = 'error'
        info['text'] = 'user '+user+' not allowed to edit setting of dictionary '+code
      else
        dictxml = '<dict><code>'+code+'</code><name>'+name+'</name><key>/entry/@id</key><root>entry</root><class>WriteDict</class><schema>'+request.query['setting'].to_s+'</schema></dict>'
        dictxml = @dict.copy_templates(code, dictxml)
        $stderr.puts dictxml
        @dict.update('dict'+code, dictxml)
        @dict.add_dict_to_service(code)
        @dict.add_user_to_dict(user, code, 'm')
        @dict.remount(@server, 'write')
        info['status'] = 'OK'
        info['dict_code'] = code
      end
      response.body = info.to_json
    when 'dict_del'
      dictcode = request.query['code'].to_s 
      info = {}
      if dictcode == ''
        info['status'] = 'error'
        info['text'] = 'dictonary code not specified'
      elsif @dict.get_dict_info(dictcode).nil?
        info['status'] = 'error'
        info['text'] = 'dictonary code not existing: '+dictcode
      elsif @dict.get_perm(user, dictcode) != 'm'
        info['status'] = 'error'
        info['text'] = 'user '+user+' not allowed to remove dictionary '+dictcode
      else
        @dict.delete('dict'+dictcode)
        @dict.remove_dict_from_service(dictcode)
        @dict.remove_all_users_from_dict(dictcode)
        @dict.remount(@server, 'write')
        info['status'] = 'OK'
        info['dict_code'] = dictcode
      end
      response.body = info.to_json
    when 'user_search'
      info = {'users'=>[], 'status'=>''}
      if request.query['search'].to_s.length > 2
        info['users'] = @dict.find_users(request.query['search'].to_s)
        info['status'] = 'OK'
      else
        info['status'] = 'error'
        info['text'] = 'search string too short, at least 3 letters'
      end
      response.body = info.to_json
    when 'user_all'
      info = {'users'=>[], 'status'=>''}
      info['users'] = @dict.find_users(nil)
      info['status'] = 'OK'
      response.body = info.to_json
    when 'user_add'
      info = {}
      if request.query['login'].to_s != '' and request.query['code'].to_s != ''
        perm = 'r'
        perm = request.query['perm'].to_s if request.query['perm'].to_s != ''
        if @dict.get('user'+request.query['login'].to_s) == ''
          info['status'] = 'error'
          info['text'] = 'user '+request.query['login'].to_s+' not existing'
        elsif @dict.get_perm(user, request.query['code'].to_s) != 'm'
          info['status'] = 'error'
          info['text'] = 'user '+user+' not allowed to manage dictionary '+request.query['code'].to_s
        else
          @dict.add_user_to_dict(request.query['login'].to_s, request.query['code'].to_s, perm)
          info['status'] = 'OK'
        end
      else
        info['status'] = 'error'
        info['text'] = 'login or dictionary code not specified'
      end
      response.body = info.to_json
    when 'user_remove'
      info = {}
      if request.query['login'].to_s != '' and request.query['code'].to_s != ''
        if @dict.get('user'+request.query['login'].to_s) == ''
          info['status'] = 'error'
          info['text'] = 'user '+request.query['login'].to_s+' not existing'
        elsif @dict.get_perm(user, request.query['code'].to_s) != 'm'
          info['status'] = 'error'
          info['text'] = 'user '+user+' not allowed to manage dictionary '+request.query['code'].to_s
        else
          @dict.remove_user_from_dict(request.query['login'].to_s, request.query['code'].to_s)
          info['status'] = 'OK'
        end
      else
        info['status'] = 'error'
        info['text'] = 'login or dictionary code not specified'
      end
      response.body = info.to_json
    when 'template_save'
      info = {}
      begin
        if @dict.get_perm(user, request.query['dict'].to_s) != 'm'
          info['status'] = 'error'
          info['text'] = 'user '+user+' not allowed to manage dictionary '+request.query['dict'].to_s
        else
          @dict.template_save(request.query['dict'], request.query['name'], request.query['code'], request.query['template'], request.query['default'].to_s, @server.info.xslt_path)
          @dict.remount(@server, 'write')
          info['status'] = 'OK'
        end
      rescue => e
        $stderr.puts e
        $stderr.puts e.backtrace
        info['status'] = 'error'
        info['text'] = e
      end
      response.body = info.to_json
    when 'template_remove'
      info = {}
      begin
        if @dict.get_perm(user, request.query['dict'].to_s) != 'm'
          info['status'] = 'error'
          info['text'] = 'user '+user+' not allowed to manage dictionary '+request.query['dict'].to_s
        else
          @dict.template_remove(request.query['dict'], request.query['code'], @server.info.xslt_path)
          @dict.remount(@server, 'write')
          info['status'] = 'OK'
        end
      rescue => e
        $stderr.puts e
        $stderr.puts e.backtrace
        info['status'] = 'error'
        info['text'] = e
      end
      response.body = info.to_json
    when 'htemplate_save'
      info = {}
      begin
        if @dict.get_perm(user, request.query['dict'].to_s) != 'm'
          info['status'] = 'error'
          info['text'] = 'user '+user+' not allowed to manage dictionary '+request.query['dict'].to_s
        else
          @dict.htemplate_save(request.query['dict'], request.query['name'], request.query['code'], request.query['template'], request.query['default'].to_s, @server.info.xslt_path)
          info['status'] = 'OK'
        end
      rescue => e
        $stderr.puts e
        $stderr.puts e.backtrace
        info['status'] = 'error'
        info['text'] = e
      end
      response.body = info.to_json
    when 'htemplate_remove'
      info = {}
      begin
        if @dict.get_perm(user, request.query['dict'].to_s) != 'm'
          info['status'] = 'error'
          info['text'] = 'user '+user+' not allowed to manage dictionary '+request.query['dict'].to_s
        else
          @dict.htemplate_remove(request.query['dict'], request.query['code'], @server.info.xslt_path)
          info['status'] = 'OK'
        end
      rescue => e
        $stderr.puts e
        $stderr.puts e.backtrace
        info['status'] = 'error'
        info['text'] = e
      end
      response.body = info.to_json
    when 'export'
      code = request.query['code'].to_s
      format = 'json'
      format = request.query['format'].to_s if request.query['format'].to_s != ''
      info = {}
      if code == ''
        info['status'] = 'error'
        info['text'] = 'no dictionary selected'
      elsif @dict.array[code].nil?
        info['status'] = 'error'
        info['text'] = 'incorrect dictionary code '+code
      else
        begin
          info['xml'] = @dict.array[code].get_all
          info['status'] = 'OK'
        rescue => e
          $stderr.puts e
          $stderr.puts e.backtrace
          info['status'] = 'error'
          info['text'] = e
        end
      end
      if format == 'json'
        response.body = info.to_json
      else
        if info['status'] == 'OK'
          response['Content-Type'] = 'text/xml; charset=utf-8'
          response.body = info['xml']
        else
          response.body = info['text']
        end
      end
    when 'import'
      info = {}
      if request.query['code'].to_s == ''
        info['status'] = 'error'
        info['text'] = 'no dictionary selected'
      elsif @dict.get_perm(user, request.query['code'].to_s) != 'm'
        info['status'] = 'error'
        info['text'] = 'user '+user+' not allowed to manage dictionary '+request.query['code'].to_s
      elsif request.query['data'].to_s == ''
        info['status'] = 'error'
        info['text'] = 'no file data'
      else
        temp = Tempfile.new("import_temp")
        temp.syswrite(Base64.decode64(request.query['data'].to_s))
        temp.close
        srcfile = temp.path
        code = request.query['code'].to_s
        root = 'entry'
        key = '/entry/@id'
        logfile = code + Time.now.strftime('%Y%m%d-%H%M') +'.log'
        logpath = '/var/log/deb-server/' + logfile
        delete = false
        delete = true if request.query['delete'].to_s == 'true'
        overwrite = false
        overwrite = true if request.query['overwrite'].to_s == 'true'
        $stderr.puts srcfile.to_s + ';' +  code.to_s + ';' +  logpath.to_s+ ';' +  delete.to_s + ';' +overwrite.to_s + ';' + key + ';' + root
        Thread.new{ import(code, srcfile, logpath, key, root, delete, overwrite) }      
        info['status'] = 'OK'
        info['logfile'] = logfile
      end
      response.body = info.to_json
    when 'showlog'
      logfile = request.query['log'].to_s
      result = %x[tail -n 20 /var/log/deb-server/#{logfile}]
      lastline = ''
      result.to_s.each{|line| lastline=line}
      response['Content-Type'] = 'text/html; charset=utf-8'
      response.body = '<html><head><meta http-equiv="Refresh" content="5"/></head><body><a href="/admin">admin</a><pre>'+result.to_s+'</pre>'
      response.body += '</body></html>'

    end
      if request.query['callback'].to_s != ''
        response["Content-type"] = "text/plain"
        response.body = add_jsonp(response.body, request.query['callback'].to_s)
      end
  end
  alias do_POST do_GET
end
