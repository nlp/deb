require 'template'
require 'cobravsmongoose'
#require 'unicode'
require 'json'

class EdnaServlet < DictServlet

  def initialize( server, dict, array )
    super
    @servername = nil
  end

  def do_GET(request, response)
    @servername = get_servername(@server, request) if @servername == nil

    if @dict.last_update + 60*60 < Time.new
      @dict.letters = @dict.get_letters
      @dict.count = @dict.get_count
    end

    user = request.attributes[:auth_user].to_s
    user = 'read' if user == ''
    perm = @dict.get_perm(user)
    #$stderr.puts @dict.get_perm(user)
    $stderr.puts @server.info.service_name
    if request.query['action'].nil? or request.query['action'].to_s == ''
      request.query['action'] = 'page'
    end
    $stderr.puts request.query['action']
    case request.query['action']
    when 'page'
      page = request.query['page'].to_s
      page = 'front' if page == ''
      file = page + '.tmpl'
      unless File.exist?(@dict.template_path + '/' + file)
        file = 'front.tmpl'
      end
      $stderr.puts 'PAGE ' + file
      #nacist data ze souboru a vlozit do hlavni sablony
      filedata = IO.read(@dict.template_path + '/' + file)
      templatedata = IO.read(@dict.template_path + '/template.tmpl')
      templatedata.sub!('<!--INSERT_TEMPLATE-->', filedata)

      template_conf = {'page'=>page, 'active_'+page=>'active'}
      template_conf['count'] = @dict.count if page == 'front'
      response.body = HTMLTemplate.new(template_conf).output_string(templatedata)
      response['Content-Type'] = 'text/html; charset=utf-8'
    when 'front'
      response.body = HTMLTemplate.new({'count'=>@dict.count}).output(@dict.template_path + '/o_slovniku.tmpl')
      response['Content-Type'] = 'text/html; charset=utf-8'
    when 'list'
      entries = @dict.get_list(request.query['search'].to_s, request.query['kat'].to_s, request.query['letter'].to_s, request.query['search_entry'].to_s, request.query['search_trans'].to_s, request.query['search_kol'].to_s, request.query['search_der'].to_s, request.query['edit'].to_s)
      allentr = []
      if (request.query['search'].to_s != '' or request.query['kat'].to_s != '' or request.query['letter'].to_s != '') and entries.size == 0
        tmpl_params = {}
        tmpl_params['search'] = request.query['search'].to_s if request.query['search'].to_s != ''
        tmpl_params['search_entry'] = '1' if request.query['search_entry'].to_s == 'on'
        tmpl_params['search_trans'] = '1' if request.query['search_trans'].to_s == 'on'
        tmpl_params['search_der'] = '1' if request.query['search_der'].to_s == 'on'
        tmpl_params['search_kol'] = '1' if request.query['search_kol'].to_s == 'on'
        tmpl_params['act_'+request.query['kat'].to_s] = 'active' if request.query['kat'].to_s != ''
        tmpl_params.merge!(@dict.letters['kats'])
        response['Content-Type'] = 'text/html; charset=utf-8'
        response.body = HTMLTemplate.new(tmpl_params).output(@dict.template_path + '/notfound.tmpl')
        return response
      end
      entries.sort!{|a, b| a['lemma']<=>b['lemma']}
      entries.each{|e|
        e['escapeid'] = URI.escape(e['id']).gsub("&apos;", '%27')
        allentr << e['escapeid']
      }
      tmpl_params = {'entries'=>entries, 'kat'=>request.query['kat'].to_s}
      tmpl_params['query_string'] = request.query_string.gsub('action=list','')
      if request.query['search'].to_s != ''
        tmpl_params['search'] = request.query['search'].to_s 
        tmpl_params['search_entry'] = '1' if request.query['search_entry'].to_s == 'on'
        tmpl_params['search_trans'] = '1' if request.query['search_trans'].to_s == 'on'
        tmpl_params['search_der'] = '1' if request.query['search_der'].to_s == 'on'
        tmpl_params['search_kol'] = '1' if request.query['search_kol'].to_s == 'on'
      else
        tmpl_params['search_entry'] = '1'
        tmpl_params['search_trans'] = '1'
        tmpl_params['search_der'] = '1' 
        tmpl_params['search_kol'] = '1'
      end
      tmpl_params.merge!(@dict.letters['kats'])
      if request.query['kat'].to_s == ''
        tmpl_params.merge!(@dict.letters['-'])
      else
        tmpl_params.merge!(@dict.letters[request.query['kat'].to_s])
      end
      tmpl_params['act_'+request.query['kat'].to_s] = 'active' if request.query['kat'].to_s != ''
      tmpl_params['allentries'] = allentr.join('!')
      if request.query['edit'].to_s == '1'
        response.body = HTMLTemplate.new(tmpl_params).output(@dict.template_path + '/list.tmpl')
      else
        response.body = HTMLTemplate.new(tmpl_params).output(@dict.template_path + '/seznam.tmpl')
      end
      response['Content-Type'] = 'text/html; charset=utf-8'
    when 'save'
      if perm != 'w' and perm != 's'
        response['Content-Type'] = 'text/plain; charset=utf8'
        response.body = 'not authorized to edit entry'
        return response.body
      end
      data_hash = JSON.parse(request.query['data'])
      $stderr.puts data_hash
      if (request.query['id'].to_s == '')
        id = data_hash['entry']['head']['lemma']['$'].to_s + '-' + data_hash['entry']['head']['gram']['$'].to_s
      else
        id = request.query['id'].to_s
      end
      id = URI.escape(id)
      data_hash['entry']['@id'] = id
      begin
        xml = @dict.get(id).to_s
        orig_hash = CobraVsMongoose.xml_to_hash(xml)
        data_hash['entry']['metadata'] = orig_hash['entry']['metadata']
      rescue
        data_hash['entry']['metadata'] = {}
        data_hash['entry']['metadata']['author'] = {'$'=>user}
        data_hash['entry']['metadata']['history'] = {'changes'=>[]}
        data_hash['entry']['metadata']['history']['changes'] << {'@user'=>user, '@time'=>Time.now.strftime("%Y-%m-%d %H:%M")}
      end
      data_hash['entry']['metadata']['history']['changes'] << {'@user'=>user, '@time'=>Time.now.strftime("%Y-%m-%d %H:%M")}
      $stderr.puts CobraVsMongoose.hash_to_xml(data_hash)
      @dict.update(id, CobraVsMongoose.hash_to_xml(data_hash))
      response.body = '{"success":true,"msg":"Uloženo", "id":"'+id+'"}'
      response['Content-Type'] = 'application/json; charset=utf-8'
    when 'get'
      if request.query['id'].to_s == ''
        response.body = '{"success":false, "msg":"Chybí ID"}'
        response['Content-Type'] = 'application/json; charset=utf-8'
      else
        begin
          xml = @dict.get(request.query['id'].to_s).to_s
          data_hash = CobraVsMongoose.xml_to_hash(xml)
          response.body = data_hash.to_json
          response['Content-Type'] = 'application/json; charset=utf-8'
        rescue => e
          $stderr.puts e.message
          $stderr.puts e.backtrace.join("\n")
          response.body = '{"success":false, "msg":"Chyba při načítání: '+e.to_s+'"}'
          response['Content-Type'] = 'application/json; charset=utf-8'
        end
      end
    when 'xml'
      id = request.query['id']
      if id.to_s != ''
        xml = @dict.get(id)
        response['Content-Type'] = 'text/xml; charset=utf8'
        response.body = xml.to_s
      end

    when 'delete'
      if perm != 'w' and perm != 's'
        response['Content-Type'] = 'text/plain; charset=utf8'
        response.body = 'not authorized to edit entry'
        return response.body
      end

      id = request.query['id']
      if id.to_s != ''
        #check lock
        lock = @server.info.who_locked('edna', id)
        if lock != false and lock != user
          response['Content-Type'] = 'text/plain; charset=utf8'
          response.body = 'this entry is locked by ' + lock.to_s
          return response.body
        else
          @server.info.lock_entry('edna', id, user)
        end
        xml = @dict.get(id)
        $stderr.puts xml.to_s
        response['Content-Type'] = 'text/html; charset=utf8'
        response.body = @dict.apply_transform('del', xml.to_s)
      else
        response.body = ''
      end
    when 'dodelete'
      if perm != 'w' and perm != 's'
        response['Content-Type'] = 'text/plain; charset=utf8'
        response.body = 'not authorized to edit entry'
        return response.body
      end

      id = request.query['id']
      id = URI.escape(id)
      $stderr.puts 'DELETE '+id
      if id.to_s != ''
        #check lock
        lock = @server.info.who_locked('edna', id)
        if lock != false and lock != user
          response['Content-Type'] = 'text/plain; charset=utf8'
          response.body = 'this entry is locked by ' + lock.to_s
          return response.body
        else
          @server.info.lock_entry('edna', id, user)
        end
        @dict.delete(id)
      end
      response.set_redirect(WEBrick::HTTPStatus::SeeOther, @servername + '/edna')
    when 'preview'
      id = request.query['id'].to_s
      if id != '' or data != ''
        if id != ''
          xml = @dict.get(id)
        else
          xml = data
        end
        xml = @dict.check_links(xml)
        response['Content-Type'] = 'text/html; charset=utf8'
        if request.query['plain'] == '1'
          response.body = @dict.apply_transform('preview-plain', xml.to_s).sub('<?xml version="1.0" encoding="UTF-8"?>', '')
        else
          response.body = @dict.apply_transform('preview', xml.to_s)
        end
      else
        response.body = ''
      end

    when 'multipublish'
      ids = request.query['ids'].to_s
      ids.split('!').each{|id|
        $stderr.puts id
        xml = @dict.publish(id)
      }
      response.set_redirect(WEBrick::HTTPStatus::SeeOther, @servername + '/edna?action=list&edit=1&search='+URI.escape(request.query['search'].to_s)+'&kat='+request.query['kat'].to_s)

    when 'multipreview'
      ids = request.query['ids'].to_s
      output = %Q[   <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<html>
<head>
<link rel="shortcut icon" href="data:image/x-icon;," type="image/x-icon"> 
<title>Etnologický slovník</title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<link href="/editor/edna-preview.css" rel="stylesheet" type="text/css"/>
</head>
<body class="preview">
]
      ids.split('!').each{|id|
        $stderr.puts id
        xml = @dict.get(id)
        xmlconv = @dict.apply_transform('multipreview', xml.to_s).sub('<html>','').sub('</html>','')
        output += xmlconv
      }
      response['Content-Type'] = 'text/html; charset=utf8'
      output += '</body></html>'
      response.body = output



    else
      super(request, response)
    end
  end
  
  alias do_POST do_GET
end
