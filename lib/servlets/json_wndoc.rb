class JSWndocServlet < WEBrick::HTTPServlet::AbstractServlet
  def initialize( server, wn )
    @wn = wn
    @server = server
  end

  def add_jsonp(data, callback)
    return callback + "(" + data + ");"
  end
  
  def do_GET(request, response)
    user = request.attributes[:auth_user].to_s
    request.query['action'] = 'init' if request.query['action'].to_s == ''

    case request.query['action']
    when 'getQueryList'
      res = ''
      response['Content-type'] = "text/plain; charset=utf-8"

      wordnets = []
      @wn.list_dicts.each{|code,name|
        wordnets << {'prava'=>'w', 'id'=>code, 'name'=>name}
      }
      res = wordnets.to_json
      $stderr.puts res
      response.body = res
    when 'connect'
      response['Content-type'] = "text/plain; charset=utf-8"
      user = request.attributes[:auth_user].to_s
      packages = @wn.list_packages(user)
      wns = {'dicts'=>packages, 'user'=>@server.info.get_user_info(user)}
			response['Content-type'] = "application/json"
      response.body = wns.to_json
    when 'init'
      response['Content-type'] = "text/plain; charset=utf-8"
      packages = @wn.list_packages(user, true)
      wns = {'slovniky'=>packages}
      response.body = wns.to_json
		when 'logout'
      wordnets = []
      user = request.attributes[:auth_user].to_s
      @wn.list_dicts.each{|code,name|
        @wn.unlock_user_dic(code, user)
      }
      response['Content-type'] = "text/plain; charset=utf-8"
      res = 'OK'.to_json
      $stderr.puts res
      response.body = res
    when 'release_locks'
      user = request.attributes[:auth_user].to_s
      @wn.list_dicts.each{|code,name|
        @wn.unlock_user_dic(code, user)
      }
      response["Content-type"] = "application/json; charset=utf-8"
      response.body = {'username'=>user, 'all_lock_released'=>'true'}.to_json
      if request.query['callback'].to_s != ''
        response["Content-type"] = "text/plain"
        response.body = add_jsonp(response.body, request.query['callback'].to_s)
      end
      response.body
    when 'showlocks'
      response["Content-type"] = "text/plain; charset=utf-8"
      if request.query['format'] == 'kyoto'
        out_a = []
        @wn.list_dicts.each{|code,name|
          locks = @wn.get_locks(code)
          locks.each{|hash|
            out_a << {'username'=>hash['login'], 'time'=>hash['time'], 'id'=>hash['entry'], 'dictionary'=>code}
          }
        }
        response["Content-type"] = "application/json; charset=utf-8"
        response.body = out_a.to_json
      else
        @wn.list_dicts.each{|code,name|
          locks = @wn.get_locks(code)
          locks.each{|hash|
            response.body += code + ';' + hash['login']+';'+hash['entry']+';'+hash['time']+"\n"
          }
        }
      end
      if request.query['callback'].to_s != ''
        response["Content-type"] = "text/plain"
        response.body = add_jsonp(response.body, request.query['callback'].to_s)
      end
      response.body

    when 'get_username'
      response.body = {'username'=>user}.to_json
      if request.query['callback'].to_s != ''
        response["Content-type"] = "text/plain"
        response.body = add_jsonp(response.body, request.query['callback'].to_s)
      end
      response.body

    when 'get_settings'
      $stderr.puts 'GET settings'
      set = @wn.get_settings(request.attributes[:auth_user].to_s, @wn.service_name)
      response["Content-type"] = "application/json; charset=utf-8"
  		response.body = set.to_json
    when 'save_settings'
      $stderr.puts 'save settings'
      data = URI.unescape(request.query['data']).to_s
      data_hash = JSON.parse(data)
      set = @wn.save_settings(request.attributes[:auth_user].to_s, data_hash, @wn.service_name)
      response["Content-type"] = "application/json; charset=utf-8"
  		response.body = data_hash.to_json
    end
  end
  alias do_POST do_GET
end
