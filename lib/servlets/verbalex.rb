require 'template'
require 'cobravsmongoose'
#require 'unicode'
require 'json'

class VerbalexServlet < DictServlet

  def initialize( server, dict, array )
    super
    @servername = nil
  end

  def do_GET(request, response)
    @servername = get_servername(@server, request) if @servername == nil
    
    user = request.attributes[:auth_user].to_s
    #$stderr.puts @dict.get_perm(user)
    $stderr.puts @server.info.service_name
    if request.query['action'] == nil or request.query['action'].to_s == ''
      request.query['action'] = 'list'
    end
    case request.query['action']
    when 'list'
      if request.query['search'].to_s != ''
        entries = @dict.get_verb_list(request.query['search'].to_s)
        entries.sort!{|a, b| a['lemma']<=>b['lemma']}
      else
        entries = {}
      end
      tmpl_params = {'entries'=>entries, 'search'=>request.query['search'].to_s}
      response.body = HTMLTemplate.new(tmpl_params).output(@dict.template_path + '/list.tmpl')
      response['Content-Type'] = 'text/html; charset=utf-8'
    when 'save'
      data_hash = JSON.parse(request.query['data'])
      if (request.query['id'].to_s == '')
        id = @server.info.get_next_id('verbalex').to_s
      else
        id = request.query['id'].to_s
      end
      data_hash['verb']['@id'] = id
      begin
        xml = @dict.get(id).to_s
        orig_hash = CobraVsMongoose.xml_to_hash(xml)
        data_hash['verb']['metadata'] = orig_hash['verb']['metadata']
      rescue
        data_hash['verb']['metadata'] = {}
        data_hash['verb']['metadata']['author'] = {'$'=>user}
        data_hash['verb']['metadata']['history'] = {'changes'=>[]}
        data_hash['verb']['metadata']['history']['changes'] << {'@user'=>user, '@time'=>Time.now.strftime("%Y-%m-%d %H:%M")}
      end
      data_hash['verb']['metadata']['history']['changes'] << {'@user'=>user, '@time'=>Time.now.strftime("%Y-%m-%d %H:%M")}
      $stderr.puts CobraVsMongoose.hash_to_xml(data_hash)
      @dict.update(id, CobraVsMongoose.hash_to_xml(data_hash))
      response.body = '{"success":true,"msg":"Uloženo"}'
      response['Content-Type'] = 'application/json; charset=utf-8'
    when 'get'
      if request.query['id'].to_s == ''
        response.body = '{"success":false, "msg":"Chybí ID"}'
        response['Content-Type'] = 'application/json; charset=utf-8'
      else
        begin
          xml = @dict.get(request.query['id'].to_s).to_s
          data_hash = CobraVsMongoose.xml_to_hash(xml)
          response.body = data_hash.to_json
          response['Content-Type'] = 'application/json; charset=utf-8'
        rescue => e
          $stderr.puts e.message
          $stderr.puts e.backtrace.join("\n")
          response.body = '{"success":false, "msg":"Chyba při načítání: '+e.to_s+'"}'
          response['Content-Type'] = 'application/json; charset=utf-8'
        end
      end
    when 'delete'
      perm = @dict.get_perm(user)
      if perm != 's'
        response['Content-Type'] = 'text/plain; charset=utf8'
        response.body = 'nemáte oprávnění editovat heslo'
        return response.body
      end
      
      id = request.query['id']
      if id.to_s != ''
        xml = @dict.get(id)
        $stderr.puts xml.to_s
        response['Content-Type'] = 'text/html; charset=utf8'
        response.body = @dict.apply_transform('del', xml.to_s)
      else
        response.body = ''
      end
      
    when 'dodelete'
      perm = @dict.get_perm(user)
      if  perm != 's'
        response['Content-Type'] = 'text/plain; charset=utf8'
        response.body = 'nemáte oprávnění editovat heslo'
        return response.body
      end
      
      id = request.query['id']
      if id.to_s != ''
        @dict.delete(id)
      end
      response.set_redirect(WEBrick::HTTPStatus::MovedPermanently, @servername + '/verbalex?action=list&')

    else
      super(request, response)
    end
  end
  
  alias do_POST do_GET
end
