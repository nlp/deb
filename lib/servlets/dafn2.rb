require 'template'
require 'cobravsmongoose'
#require 'unicode'
require 'json'

class Dafn2Servlet < DictServlet

  def initialize( server, dict, array )
    super
    @servername = nil
  end

  def do_GET(request, response)
    @servername = get_servername(@server, request) if @servername == nil
    dict_path = request.path[1..-1]
    
    user = request.attributes[:auth_user].to_s
    #$stderr.puts @dict.get_perm(user)
    $stderr.puts @server.info.service_name
    if request.query['action'] == nil or request.query['action'].to_s == ''
      request.query['action'] = 'main'
    end
    if request.query['action'] == 'list' and request.query['hw'].to_s == '' and request.query['search'].to_s == '' and request.query['contrib'].to_s == ''
      request.query['action'] = 'main'
    end

    case request.query['action']
    when 'main'
      tmpl_params = {
        'path'=>dict_path,
        'is_'+dict_path=>1
      }
      response['Content-Type'] = 'text/html; charset=utf8'
      response.body = HTMLTemplate.new(tmpl_params).output(@dict.template_path + '/main.tmpl')
    when 'list'
      request.query['hw'] = '' if request.query['hw'].to_s == 'surname...'
      request.query['search'] = '' if request.query['search'].to_s == 'search...'
      if request.query['hw'].to_s == '' and request.query['search'].to_s == '' and request.query['contrib'].to_s == ''
        tmpl_params = {
          'path'=>dict_path,
          'is_'+dict_path=>1
        }
        response.body = HTMLTemplate.new(tmpl_params).output(@dict.template_path + '/main.tmpl')
        response['Content-Type'] = 'text/html; charset=utf-8'
      else
        entries = @dict.get_list(request.query)
        entries.sort!{|a, b| a['name']<=>b['name']}
        allentr = []
        perm = @dict.get_perm(user)
        entries.each{|e|
          e['read'] = '1' if perm == 'r'
          e['escapeid'] = URI.escape(e['id']).gsub("&apos;", '%27')
          e['escapeid2'] = URI.escape(e['id']).gsub("&apos;", '\%27')
          allentr << e['escapeid']
        }
        tmpl_params = {
          'entries'=>entries, 
          'count'=>entries.size,
          'path' => dict_path,
          'is_'+dict_path=>1
        }
        tmpl_params['read'] = '1' if perm == 'r'
        if request.query['hw'].to_s != ''
          tmpl_params['form_hw'] = request.query['hw'] 
          tmpl_params['form_hw_set'] = 1
          tmpl_params['form_whhw_'+request.query['where_hw'].to_s] =  1
          tmpl_params['form_whhw'] = request.query['where_hw'].to_s
        end
        if request.query['search'].to_s != ''
          tmpl_params['form_search'] = request.query['search']
          tmpl_params['form_search_set'] = 1
          tmpl_params['form_whsearch_'+request.query['where_search'].to_s] =  1
          tmpl_params['form_whsearch'] = request.query['where_search'].to_s
        end
        if request.query['contrib'].to_s != ''
          tmpl_params['form_contrib'] = request.query['contrib'].to_s
          tmpl_params['form_contrib_'+request.query['contrib'].to_s] = 1
        end
        tmpl_params['form_approved'] = '1' if request.query['approved'] == 'true'
        tmpl_params['form_mmcheck'] = '1' if request.query['mmcheck'] == 'true'
        tmpl_params['allentries'] = allentr.join('!')
        tmpl_params['is_patrick'] = '1' if user == 'patrick' or user == 'deb'

        response.body = HTMLTemplate.new(tmpl_params).output(@dict.template_path + '/list.tmpl')
        response['Content-Type'] = 'text/html; charset=utf-8'
      end
    when 'multipreview'
      ids = request.query['ids'].to_s
output = %Q[   <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>DAFN2 entries</title>
  <style type="text/css">
  body {
background: #ffffff;
color: #000000;
  }
.red {
color: #ff0000;
}
.green {
color: #007700;
}
.darkred {
color: #770000;
}
.blue {
color: #0000ff;
}
.darkblue {
color: #000088;
}
.head {
  font-size: 150%;
}
.sans {
  font-family: sans-serif;
}
.caps {
  font-variant: small-caps;
}
.indent {
  margin-left: 1em;
}
.normal {
  font-weight: normal;
}
.comment_link, .musing_link {
text-decoration: underline;
color: blue;
}
</style>
        <script src='files/preview.js' type='text/javascript'> </script>
</head>
<body>
    <a class="comment_link" id="comment_link" onclick="show_requests()">show requests</a> / 
    <a class="musing_link" id="musing_link" onclick="show_musings()">show musings</a>
]
        ids.split('!').each{|id|
          xml = @dict.get(id)
          xmlconv = @dict.apply_transform('multipreview', xml.to_s).sub('<html>','').sub('</html>','')
          output += xmlconv
        }
        response['Content-Type'] = 'text/html; charset=utf8'
        output += '</body></html>'
        response.body = output

    when 'approveall'
      ids = request.query['ids'].to_s
      count = 0
      ids.split('!').each{|id|
        res = @dict.approve(id)
        count += res
      }
      response['Content-Type'] = 'text/html; charset=utf8'
      response.body = count.to_s + ' entries approved'


    when 'save'
      data_hash = JSON.parse(request.query['data'])
      $stderr.puts data_hash
      if (request.query['id'].to_s == '')
        id = data_hash['entry']['name']['$']
      else
        id = request.query['id'].to_s
      end
      begin
        xml = @dict.get(id).to_s
        orig_hash = CobraVsMongoose.xml_to_hash(xml)
        data_hash['entry']['metadata'] = orig_hash['entry']['metadata']
        raise 'nil' if orig_hash['entry']['metadata'].nil?
      rescue
        $stderr.puts 'rescue'
        data_hash['entry']['metadata'] = {}
        data_hash['entry']['metadata']['author'] = {'$'=>user}
        data_hash['entry']['metadata']['history'] = {'changes'=>[]}
        data_hash['entry']['metadata']['history']['changes'] << {'@user'=>user, '@time'=>Time.now.strftime("%Y-%m-%d %H:%M")}
      end
      data_hash['entry']['metadata']['history']['changes'] << {'@user'=>user, '@time'=>Time.now.strftime("%Y-%m-%d %H:%M")}
      xmldata = CobraVsMongoose.hash_to_xml(data_hash)
      $stderr.puts xmldata
      xmldata.gsub!('&amp;amp;amp;amp;lt;', '&lt;')
      xmldata.gsub!('&amp;amp;amp;lt;', '&lt;')
      xmldata.gsub!('&amp;amp;lt;', '&lt;')
      xmldata.gsub!('&amp;lt;', '&lt;')
      xmldata.gsub!('&amp;amp;amp;amp;gt;', '&gt;')
      xmldata.gsub!('&amp;amp;amp;gt;', '&gt;')
      xmldata.gsub!('&amp;amp;gt;', '&gt;')
      xmldata.gsub!('&amp;gt;', '&gt;')
      xmldata.gsub!('&lt;i&gt;', '<i>')
      xmldata.gsub!('&lt;/i&gt;', '</i>')
      xmldata.gsub!('&lt;bi&gt;', '<bi>')
      xmldata.gsub!('&lt;/bi&gt;', '</bi>')
      xmldata.gsub!('&lt;b&gt;', '<b>')
      xmldata.gsub!('&lt;/b&gt;', '</b>')
      xmldata.gsub!('&lt;xr&gt;', '<xr>')
      xmldata.gsub!('&lt;/xr&gt;', '</xr>')
      xmldata.gsub!('&lt;sn&gt;', '<sn>')
      xmldata.gsub!('&lt;/sn&gt;', '</sn>')
      xmldata.gsub!(/&lt;xref refurl=&quot;([0-9]*)&quot;&gt;/, '<xref refurl="\1">')
      xmldata.gsub!(/&lt;render img=&quot;([0-9]*)&quot;&gt;/, '<render img="\1">')
      xmldata.gsub!('&lt;/xref&gt;', '</xr>')
      xmldata.gsub!('&lt;xref&gt;', '<xr>')
      xmldata.gsub!('&lt;form&gt;', '<b>')
      xmldata.gsub!('&lt;/form&gt;', '</b>')
      xmldata.gsub!('&lt;forename&gt;', '<forename>')
      xmldata.gsub!('&lt;/forename&gt;', '</forename>')
      xmldata.gsub!('&lt;annotation&gt;', '<annotation>')
      xmldata.gsub!('&lt;/annotation&gt;', '</annotation>')
      xmldata.gsub!('&lt;regionalStats&gt;', '<regionalStats>')
      xmldata.gsub!('&lt;/regionalStats&gt;', '</regionalStats>')
      xmldata.gsub!('&lt;in&gt;', '<in>')
      xmldata.gsub!('&lt;/in&gt;', '</in>')
      xmldata.gsub!('&lt;ini&gt;', '<ini>')
      xmldata.gsub!('&lt;/ini&gt;', '</ini>')
      xmldata.gsub!('&lt;su&gt;', '<su>')
      xmldata.gsub!('&lt;/su&gt;', '</su>')
      xmldata.gsub!('&lt;sui&gt;', '<sui>')
      xmldata.gsub!('&lt;/sui&gt;', '</sui>')
      xmldata.gsub!('&lt;sc&gt;', '<sc>')
      xmldata.gsub!('&lt;/sc&gt;', '</sc>')
      xmldata.gsub!('&lt;fr&gt;', '<fr>')
      xmldata.gsub!('&lt;/fr&gt;', '</fr>')
      xmldata.gsub!('&lt;fn&gt;', '<fn>')
      xmldata.gsub!('&lt;/fn&gt;', '</fn>')
      xmldata.gsub!('&lt;prob&gt;', '<prob>')
      xmldata.gsub!('&lt;/prob&gt;', '</prob>')
      $stderr.puts xmldata

      @dict.update(id, xmldata)
      response.body = '{"success":true,"msg":"Saved"}'
      response['Content-Type'] = 'application/json; charset=utf-8'
    when 'getedit'
      response.body = @dict.get_for_edit(request.query['id'].to_s).to_s
      response['Content-Type'] = 'text/xml; charset=utf8'

    when 'get'
      if request.query['id'].to_s == ''
        response.body = '{"success":false, "msg":"No ID"}'
        response['Content-Type'] = 'application/json; charset=utf-8'
      else
        begin
          xml = @dict.get_for_edit(request.query['id'].to_s).to_s
          data_hash = CobraVsMongoose.xml_to_hash(xml)
          response.body = data_hash.to_json
          response['Content-Type'] = 'application/json; charset=utf-8'
        rescue => e
          $stderr.puts e.message
          $stderr.puts e.backtrace.join("\n")
          response.body = '{"success":false, "msg":"Error while loading: '+e.to_s+'"}'
          response['Content-Type'] = 'application/json; charset=utf-8'
        end
      end
    when 'preview'
      id = request.query['id'].to_s
      xml = @dict.get(id)
      response['Content-Type'] = 'text/html; charset=utf8'
      response.body = @dict.apply_transform('preview', xml.to_s)

    when 'cluster'
      id = request.query['id']
      if id.to_s != ''
        refs = @dict.get_cluster(id.to_s)
        locked = []
        refs.each{|rid|
          lock = @server.info.who_locked('dafn2', rid)
          if lock != false and lock != user
            locked << {'user'=>lock.to_s, 'entry'=>rid}
          end
        }
        if locked.size > 0
          response.body = ''
          locked.each{|h|
            response.body += 'entry ' + h['entry'] + ' is locked by  ' + h['user'] + "\n"
          }
          return response.body
        else
        $stderr.puts 'locking'
          refs.each{|rid|
          $stderr.puts rid
            @server.info.lock_entry('dafn2', rid, user)
          }
        end

        text = @dict.get_cluster_html(refs, dict_path, user)
      else
        text = @dict.get_cluster_html(['__new__'], dict_path, user)
      end

        case dict_path
        when 'dafn2'
          pathinfo = 'DAFN2 LIST'
        when 'dafn2quar'
          pathinfo = 'QUARANTINE LIST'
        when 'dafn2res'
          pathinfo = 'RESERVE LIST'
        end

        out = %Q[<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
  <html xmlns="http://www.w3c.org/1999/xhtml">
  <head>
        <base href="#{@basehref}"/>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <script type="text/javascript" src="files/surnames3.js?#{Time.now.to_i}"></script>
  <script type="text/javascript" src="files/global.js"></script>
  <script type="text/javascript" src="files/sm/subModal/submodalsource.js"></script>
  <script type="text/javascript" src="files/textedit.js"></script>
  <link rel="stylesheet" href="files/surned.css" type="text/css"/>
  <link rel="stylesheet" href="files/sm/subModal/submodal.css" type="text/css"/>
</head>
<body onload="if (event.target == document) edit_onload();" >
<div id="top_info" style="position:fixed;z-index:10;height:33px;width:100%;background:#90ee90;top:0;border-bottom: solid 1px black;">
<div id="loading_wait" style="display:none;position: fixed;left: 25%;top: 25%;z-index: 10;background-color: #eee;width: 400px;height: 100px;font-size: 110%;text-align: center; vertical-align: middle; line-height: 100px">loading, please wait...</div>
        <span id="pathinfo">#{pathinfo}</span>
<button onclick="alert(get_all())">test</button>
<button id="but_save" disabled="true" onclick="save_all(false)">Save</button>
<button id="but_save_back" disabled="true" onclick="save_all(true)">Save & close</button>
<button onclick="unlock(true);">Close</button>
<span id="info_loading" style=""><b><i>Loading...</i></b></span>
<span id="info_saving" style="display:none"><b><i>Saving...</i></b></span>
<span id="info_saved" style="display:none"><b><i>Saved...</i></b></span>
</div>
<div id="inside" style="position:relative;margin-top:38px;">
        ]
        out2 = '</div><script type="text/javascript">var footer_loaded = true;</script></body></html>'
        response['Content-Type'] = 'text/html; charset=utf8'
        response.body = out+text.to_s+out2


    when 'save_cluster'
      perm = @dict.get_perm(user)
      if perm == 'w'
        data = request.query['data']
        data.gsub!('&amp;amp;amp;amp;lt;', '&lt;')
        data.gsub!('&amp;amp;amp;lt;', '&lt;')
        data.gsub!('&amp;amp;lt;', '&lt;')
        data.gsub!('&amp;lt;', '&lt;')
        data.gsub!('&amp;amp;amp;amp;gt;', '&gt;')
        data.gsub!('&amp;amp;amp;gt;', '&gt;')
        data.gsub!('&amp;amp;gt;', '&gt;')
        data.gsub!('&amp;gt;', '&gt;')
        data.gsub!('&lt;i&gt;', '<i>')
        data.gsub!('&lt;/i&gt;', '</i>')
        data.gsub!('&lt;b&gt;', '<b>')
        data.gsub!('&lt;/b&gt;', '</b>')
        data.gsub!('&lt;bi&gt;', '<bi>')
        data.gsub!('&lt;/bi&gt;', '</bi>')
        data.gsub!('&lt;xr&gt;', '<xr>')
        data.gsub!('&lt;/xr&gt;', '</xr>')
        data.gsub!('&lt;sn&gt;', '<sn>')
        data.gsub!('&lt;/sn&gt;', '</sn>')
        data.gsub!(/&lt;xref refurl=&quot;([0-9]*)&quot;&gt;/, '<xref refurl="\1">')
        data.gsub!(/&lt;render img="([0-9]*)"\/&gt;/, '<render img="\1"/>')
        data.gsub!('&lt;/xref&gt;', '</xref>')
        data.gsub!('&lt;xref&gt;', '<xref>')
        data.gsub!('&lt;form&gt;', '<form>')
        data.gsub!('&lt;/form&gt;', '</form>')
        data.gsub!('&lt;probability&gt;', '<probability>')
        data.gsub!('&lt;/probability&gt;', '</probability>')
        data.gsub!('&lt;forename&gt;', '<forename>')
        data.gsub!('&lt;/forename&gt;', '</forename>')
        data.gsub!('&lt;annotation&gt;', '<annotation>')
        data.gsub!('&lt;/annotation&gt;', '</annotation>')
        data.gsub!('&lt;regionalStats&gt;', '<regionalStats>')
        data.gsub!('&lt;/regionalStats&gt;', '</regionalStats>')
        data.gsub!('&lt;in&gt;', '<in>')
        data.gsub!('&lt;/in&gt;', '</in>')
        data.gsub!('&lt;ini&gt;', '<ini>')
        data.gsub!('&lt;/ini&gt;', '</ini>')
        data.gsub!('&lt;su&gt;', '<su>')
        data.gsub!('&lt;/su&gt;', '</su>')
        data.gsub!('&lt;sui&gt;', '<sui>')
        data.gsub!('&lt;/sui&gt;', '</sui>')
        data.gsub!('&lt;sc&gt;', '<sc>')
        data.gsub!('&lt;/sc&gt;', '</sc>')
        data.gsub!('&lt;fr&gt;', '<fr>')
        data.gsub!('&lt;/fr&gt;', '</fr>')
        data.gsub!('&lt;fn&gt;', '<fn>')
        data.gsub!('&lt;/fn&gt;', '</fn>')
        data.gsub!('&lt;prob&gt;', '<prob>')
        data.gsub!('&lt;/prob&gt;', '</prob>')
        data.gsub!('&lt;lang&gt;', '<lang>')
        data.gsub!('&lt;/lang&gt;', '</lang>')
        #$stderr.puts data

        begin
          edoc = @dict.load_xml_string(data.to_s)
          edoc.find('/data/entry').each{|en|
            id = en['sortkey'].gsub('__', "'")
            id = URI.escape(id) unless id.include?("%")
            $stderr.puts en['sortkey'] + ' '+id
            edits = @dict.get_edits(id)
            $stderr.puts edits
            edits << {'author'=>user, 'time'=>Time.now.strftime('%Y-%m-%d %H:%M:%S')}

            doc = @dict.add_meta(en.to_s, edits)
            xml = doc.to_s.gsub('<?xml version="1.0" encoding="utf-8"?>','')
            xml.gsub!('<?xml version="1.0" encoding="UTF-8"?>','')
            $stderr.puts id
            $stderr.puts xml
            @dict.update(id, xml)
            @server.info.unlock_entry('dafn2', id, user) if request.query['unlock'].to_s == 'true'
          }
          response.body = 'saved cluster' 
        rescue => e 
          $stderr.puts e.message
          $stderr.puts e.backtrace.join("\n")
          response.body = e.to_s
        end
      else
        response.body = 'not authorized to save entry'
      end
      return response.body

    when 'unlock'
    when 'unlock_cluster'
      if request.query['unlock'].to_s != ''
        @server.info.unlock_entry('dafn2', request.query['unlock'].to_s, user)
      end
      response.body = 'unlocked ' + request.query['unlock'].to_s

    when 'xr_from'
      id = URI.escape(request.query['id'].to_s)
      xrar = @dict.get_xr_from(id)
      out = ''
      xrar.each{|name,eid|
        out += name + ':' + eid + "\n"
      }
      response.body = out
      response['Content-Type'] = 'text/plain; charset=utf-8'
    when 'xr_to'
      id = URI.escape(request.query['id'].to_s)
      xrar = @dict.get_xr_to(id)
      out = ''
      xrar.each{|name,eid|
        out += name + ':' + eid + "\n"
      }
      response.body = out
      response['Content-Type'] = 'text/plain; charset=utf-8'

    when 'check_hw'
      response['Content-Type'] = 'text/html; charset=utf8'
      id = URI.escape(request.query['check'].to_s)
      entry_id = URI.escape(request.query['entry_id'].to_s)
      $stderr.puts id
      $stderr.puts entry_id
      if id != ''
        xml = @dict.get(id)
        response.body = '-'
        if xml == ''
          response.body = 'ok'
        end
        return response.body
      end
      response.body = '-'

    when 'move_entry'
      fromdb = request.query['from'].to_s
      todb = request.query['to'].to_s
      entry_id = URI.escape(request.query['id'].to_s)
      force = false
      force = true if request.query['force'].to_s == 'true'
      if fromdb != '' and todb != '' and entry_id != ''
        $stderr.puts 'MOVE ENTRY '+entry_id+' from '+fromdb+' to '+todb
        result = @dict.move_entry(@dict_array[fromdb], @dict_array[todb], entry_id, force)
        response['Content-Type'] = 'text/html; charset=utf8'
        response.body = result
      else
        response['Content-Type'] = 'text/html; charset=utf8'
        response.body = 'Error: arguments not set'
      end
    when 'delete'
      perm = @dict.get_perm(user)
      if perm != 'w' and perm != 's'
        response['Content-Type'] = 'text/plain; charset=utf8'
        response.body = 'not authorized to edit entry'
        return response.body
      end
      
      id = request.query['id']
      search = request.query['hw'].to_s
      if id.to_s != ''
        #check lock
        lock = @server.info.who_locked('dafn2', id)
        if lock != false and lock != user
          response['Content-Type'] = 'text/plain; charset=utf8'
          response.body = 'this entry is locked by ' + lock.to_s
          return response.body
        else
          @server.info.lock_entry('dafn2', id, user)
        end
        xml = @dict.get(id)
        $stderr.puts xml.to_s
        response['Content-Type'] = 'text/html; charset=utf8'
        response.body = @dict.apply_transform('del', xml.to_s, [ ['hw', '"'+search.gsub("'","%27")+'"'], ['where_hw', '"'+request.query['where_hw'].to_s+'"'], ['path', '"'+dict_path+'"'] ])
      else
        response.body = ''
      end
      
    when 'dodelete'
      $stderr.puts 'DO DELETE'
      perm = @dict.get_perm(user)
      if perm != 'w' and perm != 's'
        response['Content-Type'] = 'text/plain; charset=utf8'
        response.body = 'not authorized to edit entry'
        return response.body
      end
      
      id = URI.escape(request.query['id'])
      $stderr.puts request.query['id'] + ' ' + id
      if id.to_s != ''
        #check lock
        lock = @server.info.who_locked('dafn2', id)
        if lock != false and lock != user
          response['Content-Type'] = 'text/plain; charset=utf8'
          response.body = 'this entry is locked by ' + lock.to_s
          return response.body
        else
          @server.info.lock_entry('dafn2', id, user)
        end
        @dict.delete(id)
      end
      response.set_redirect(WEBrick::HTTPStatus::SeeOther, @servername + '/' + dict_path + '?action=list&hw=' + request.query['hw'].to_s + '&where_hw=' + request.query['where_hw'].to_s)

    when 'replace'
      tmpl_params = {'path'=>dict_path}
      tmpl_params['not_all'] = 1 if request.query['not_all'].to_s == '1'
      if request.query['done'].to_s == '1'
        tmpl_params['count_e'] = request.query['count_e'].to_s
        tmpl_params['count_r'] = request.query['count_r'].to_s
        tmpl_params['done'] = 1
      end
      response['Content-Type'] = 'text/html; charset=utf8'
      response.body = HTMLTemplate.new(tmpl_params).output(@dict.template_path + '/replace.tmpl')
    when 'do_replace'
      find = request.query['find'].to_s
      replace = request.query['replace'].to_s
      where = request.query['where'].to_s
      if find != '' #and replace != ''
        count_e, count_r = @dict.replace(find, replace, where)
      else
        response.set_redirect(WEBrick::HTTPStatus::SeeOther, @servername + '/'+dict_path+'?action=replace&not_all=1')
      end
      response.set_redirect(WEBrick::HTTPStatus::SeeOther, @servername + '/'+dict_path+'?action=replace&done=1&count_e='+count_e.to_s+'&count_r='+count_r.to_s)
    else
      super(request, response)
    end
  end
  
  alias do_POST do_GET
end
