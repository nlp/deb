require 'webrick'
require 'cobravsmongoose'

class CdbIDServlet < JSWordnetServlet
  def do_GET(request, response)
    user = request.attributes[:auth_user].to_s
    dictionary = request.path[1..-1]
    case request.query['action']
    when 'queryList'
      if not @server.info.check_perm(Thread.current[:auth_user], @wn.dict_prefix+'cdb_id')
        response.body = [{'value' => '0', 'label'=> 'not authorized'}].to_json
        return response.body
      end
     
      query = URI.unescape(request.query['word']).to_s
      response["Content-type"] = "text/plain; charset=utf-8"
      
      if query == ''
        $stderr.puts "Query not set"
        response.body += "Query not set".to_json
      else
        cids = @wn.get_list(query, @wn_array)
        cid_ar = []
        cids.each {|id,info|
          if info['pos'][0,1] == 'A'
            pos = info['pos'][0,3]
          else
            pos = info['pos']
          end
          label = ''
          label += (info['lockstatus'] == user)? '[*] ':'[x] '  if info['lockstatus'] != false
          label += info['form'] + ':' + info['seq_nr']
          label += ', ' + pos if pos != ''
          label += ', LU=' + info['lu_id'] + ', SY=' + info['sy_id'] + ';'
          label += ' ' + info['score'] if info['score'] != ''
          label += ', ' + form_status_text(info['selected'], info['status'])
          
          cid_ar << {'value' => id, 'label' => label, 'form' => info['form'].to_s, 'seq_nr' => info['seq_nr'].to_i}
        }
        response.body += cid_ar.sort!{|x,y| (x['form']<=>y['form']).nonzero? || x['seq_nr']<=>y['seq_nr']}.to_json
      end
    when 'save'
      dict = request.path[1..-1]
      id = request.query['id']
      $stderr.puts "EDIT CID ID " + id

      check = @wn.check_lock(dict, id, Thread.current[:auth_user])
      if not(check == true)
        response['Content-Type'] = 'text/plain'
        response.body = check.to_json
        return
      end
      if not @server.info.check_perm(Thread.current[:auth_user], @wn.dict_prefix+'cdb_id', 'w')
        response["Content-type"] = "text/plain; charset=utf-8"
        response.body = {'action'=>'error', 'message'=>'not authorized'}.to_json
        return response.body
      end
      
      new = false
      if id == '_new_id_' or id == ''
        new = true
        id = @server.info.get_next_id(dict)
      end
      data = URI.unescape(request.query['data']).to_s
      data_hash = JSON.parse data
      $stderr.puts data
      begin
        #$stderr.puts data_hash
        if new
          luid = data_hash['cid_array'][0]['cid']['@c_lu_id']
          orig_cid_ar = get_cid_array_lu(luid, @wn)
        else
          orig_cid_ar = get_cid_array(id, @wn)
        end
        $stderr.puts orig_cid_ar.to_json
        data_hash['cid_array'].each{|cid_entry|
          cid_data = cid_entry['cid']
          if new and (cid_data['@cid_id'] == '' or cid_data['@cid_id'] == nil)
            cid_id = id
            cid_data['@cid_id'] = id
          else
            cid_id = cid_data['@cid_id']
          end
          $stderr.puts " Mapping, CID "+cid_id.to_s
          orig_ar = orig_cid_ar['cid_array'].select{|item| item['cid']['@cid_id'] == cid_id}
          orig_data = nil
          orig_data = orig_ar[0]['cid'] if orig_ar != nil and orig_ar[0] != nil
          change = false
          #deselected
          if (orig_data != nil  and orig_data['@selected'] == 'true') and cid_data['@selected'] != 'true'
            $stderr.puts 'deselected: cid=' + cid_id + ' lu=' + cid_data['@c_lu_id'] + ' sy=' + cid_data['@d_sy_id']
            #remove synonym from synset
            @wn_array[@wn.dict_prefix+'cdb_syn'].remove_synonym(cid_data['@d_sy_id'], cid_data['@c_lu_id']) if cid_data['@d_sy_id'] != '' and cid_data['@c_lu_id'] != ''
            #update synset preview
            @wn_array[@wn.dict_prefix+'cdb_lu'].update_synset_preview(@wn_array[@wn.dict_prefix+'cdb_syn'], cid_data['@c_lu_id'], cid_data['@form'], cid_data['@seq_nr'])
            cid_data['@status'] = 'manual'
            cid_data['@name'] = user
            change = true
          end
          #newly selected
          if (orig_data == nil or orig_data['@selected'] != 'true') and cid_data['@selected'] == 'true'
            $stderr.puts 'selected: cid=' + cid_id + ' lu=' + cid_data['@c_lu_id'] + ' sy=' + cid_data['@d_sy_id']
            #add synonym to synset
            @wn_array[@wn.dict_prefix+'cdb_syn'].add_synonym(cid_data['@d_sy_id'], cid_data['@c_lu_id']) if cid_data['@d_sy_id'] != '' and cid_data['@c_lu_id'] != ''
            #get LU form, just for sure
            cid_data['@form'], cid_data['@seq_nr'], cid_data['@pos'] = @wn_array[@wn.dict_prefix+'cdb_lu'].get_form_seq(cid_data['@c_lu_id']) if cid_data['@c_lu_id'] != ''
            #update synset preview
            @wn_array[@wn.dict_prefix+'cdb_lu'].update_synset_preview(@wn_array[@wn.dict_prefix+'cdb_syn'], cid_data['@c_lu_id'], cid_data['@form'], cid_data['@seq_nr'])
            cid_data['@status'] = 'manual'
            cid_data['@name'] = user
            change = true
          end
          
          if orig_data != nil and orig_data['@status'] != cid_data['@status']
            change = true
            cid_data['@name'] = user
          end

          #save data
          if change
            xml = CobraVsMongoose.hash_to_xml({'cid'=>cid_data})
            $stderr.puts xml
            @wn.update(cid_id, xml)
          end
        }

        #special case for new CID. CID present in original array, but not in new
        if new
          orig_ar = orig_cid_ar['cid_array'].select{|item| item['cid']['@selected'] == 'true'}
          orig_ar.each{|orig_data|
            new_ar = data_hash['cid_array'].select{|item| item['cid']['@cid_id'] == orig_data['cid']['@cid_id']}
            if (new_ar == nil or new_ar[0] == nil or new_ar[0]['cid']['@selected'] == 'true')
              $stderr.puts 'deselected: cid=' + orig_data['cid']['@cid_id'] + ' lu=' + orig_data['cid']['@c_lu_id'] + ' sy=' + orig_data['cid']['@d_sy_id']
              #remove synonym from synset
              @wn_array[@wn.dict_prefix+'cdb_syn'].remove_synonym(orig_data['cid']['@d_sy_id'], orig_data['cid']['@c_lu_id']) if orig_data['cid']['@d_sy_id'] != '' and orig_data['cid']['@c_lu_id'] != ''
              #update synset preview
              @wn_array[@wn.dict_prefix+'cdb_lu'].update_synset_preview(@wn_array[@wn.dict_prefix+'cdb_syn'], orig_data['cid']['@c_lu_id'], '', '')
              orig_data['cid']['@selected'] = 'false'
              orig_data['cid']['@status'] = 'manual'
              orig_data['cid']['@name'] = user
              xml = CobraVsMongoose.hash_to_xml(orig_data)
              $stderr.puts xml
              @wn.update(orig_data['cid']['@cid_id'], xml)
            end
          }
        end
        response['Content-Type'] = 'text/plain'
        response.body = {'action'=>'saved', 'reload'=>['cdbsyn1', 'cdbid1'], 'id'=>id}.to_json
      rescue => e
        response['Content-Type'] = 'text/plain'
        $stderr.puts e.to_s + "\n" + e.backtrace.join("\n")
        response.body = {'action'=>'error', 'message'=>"exception #{e.class.to_s}: #{e.message}"}.to_json
      end

    when 'delete'
      dict = request.path[1..-1]
      id = request.query['id']
      $stderr.puts "DELETE CID ID " + id
      check = @wn.check_lock(dict, id, Thread.current[:auth_user])
      if not(check == true)
        response['Content-Type'] = 'text/plain'
        response.body = check.to_json
        return
      end
      begin
        xml = @wn.get(id)
        doc = REXML::Document.new(xml.to_s)
        lu_id = doc.root.attributes['c_lu_id'].to_s
        sy_id = doc.root.attributes['d_sy_id'].to_s
        #remove synonym
        @wn_array[@wn.dict_prefix+'cdb_syn'].remove_synonym(sy_id, lu_id) if sy_id != '' and lu_id != ''
        @wn.delete(id)
        new = select_best_mapping(lu_id, @wn)
        @wn.select(new) if new != nil and new != ''
        $stderr.puts "Unlocking"
        @wn.unlock_time(dict, id, Thread.current[:auth_user])
        response['Content-Type'] = 'text/plain'
        response.body = {'action'=>'deleted', 'reload'=>['cdbsyn1', 'cdbid1']}.to_json
      rescue => e
        response['Content-Type'] = 'text/plain'
        $stderr.puts e
        response.body = {'action'=>'error', 'message'=>"exception #{e.class.to_s}: #{e.message}"}.to_json
      end
      
    when 'newid'
      dict = request.path[1..-1]
      newid = @server.info.get_next_id(dict)
      response["Content-type"] = "text/plain; charset=utf-8"
      response.body = newid.to_s
    when 'check'
      logfile = '/var/log/deb-server/cid_check-' + Time.now.strftime('%Y%m%d-%H%M') +'.log'
      log = File.new(logfile, 'w')

      @wn.container.each {|doc|
        match_id = /cid_id="([^"]*)"/.match(doc.to_s)
        if match_id != nil and match_id[1] != nil
          cid_id = match_id[1]
        end
        match_id = /c_lu_id="([^"]*)"/.match(doc.to_s)
        if match_id != nil and match_id[1] != nil
          lu_id = match_id[1]
        end
        match_id = /d_sy_id="([^"]*)"/.match(doc.to_s)
        if match_id != nil and match_id[1] != nil
          sy_id = match_id[1]
        end
        log.puts "CID=#{cid_id}, LU=#{lu_id}, SY=#{sy_id}"
        begin
          @wn_array[@wn.dict_prefix+'cdb_lu'].get(lu_id)
        rescue
          log.puts " LU not found: #{lu_id}"
        end
        begin
          @wn_array[@wn.dict_prefix+'cdb_syn'].get(sy_id)
        rescue
          log.puts " SY not found: #{sy_id}"
        end
      }
      log.close
      response["Content-type"] = "text/plain; charset=utf-8"
      response.body = "done"
    when 'lock'
      id = request.query['id']
      begin
    	  res = @wn.get(id).to_s
        check = @wn.check_lock(dictionary, id, Thread.current[:auth_user])
        if check == true
          time = Time.now.to_i
          hash = CobraVsMongoose.xml_to_hash(res)
          lu_id = hash['cid']['@c_lu_id'].to_s
          sy_id = hash['cid']['@d_sy_id'].to_s
          @wn.lock(dictionary, id, Thread.current[:auth_user], time) #lock cid
          @wn.lock(@wn.dict_prefix+'cdb_lu', lu_id, Thread.current[:auth_user], time) if lu_id != '' #lock lu
          @wn.lock(@wn.dict_prefix+'cdb_syn', sy_id, Thread.current[:auth_user], time) if sy_id != '' #lock syn
          status = {'action' => 'locked'}
        else
          status = check
        end
      rescue => e
        status = {'error' => e.to_s}
      end
      response["Content-type"] = "text/plain; charset=utf-8"
      response.body = status.to_json
    when 'unlock'
      id = request.query['id']
      @wn.unlock_time(dictionary, id, Thread.current[:auth_user])
      response["Content-type"] = "text/plain; charset=utf-8"
      response.body = {'action' => 'unlocked'}.to_json
    
    else
      super(request, response)
    end
  end
  
  alias do_POST do_GET 

  def runQuery(dictionary, query, type, arg, dictcode, default='', expand='', tree_details = 0, display_only = '', reldics =[])
    if not @server.info.check_perm(Thread.current[:auth_user], @wn.dict_prefix+'cdb_id')
      return nil
    end
    
    begin
    	resxml = dictionary.get(query)
    rescue
      return nil
    end
    res = resxml.to_s
  	$stderr.puts resxml
  	case type
    when 'plain'
      return res
  	when 'xml'
      #res = dictionary.apply_transform( 'xml', res, [ [ 'slovnik', '"'+dictcode.to_s+'"'], [ 'servername', '"'+@servername.to_s+'"' ] ] )
      param = {'slovnik'=>dictcode.to_s}
      res = dictionary.apply_transform( 'xml', res, param )
  		return res
  	when 'html'
      transform = 'preview'
      transform += arg if arg.to_s != ''
      #res = dictionary.apply_transform( transform, res, [ [ 'slovnik', '"'+dictcode.to_s+'"'], [ 'servername', '"'+@servername.to_s+'"' ], ['default', '"'+default.to_s+'"'] ] )
      param = {'slovnik'=>dictcode.to_s}
      param['default'] = default.to_s unless default.to_s == ''
      res = dictionary.apply_transform( transform, res, param)
  		return res
  	when 'tree'
  	when 'editor'
      check = dictionary.check_lock(dictcode, query, Thread.current[:auth_user])
      return get_cid_array(query, dictionary)
  	end
  end

  def get_cid_array(cid_id, dictionary)
      cidar = []
      begin
      	resxml = dictionary.get(cid_id)
        res = resxml.to_s
        doc = REXML::Document.new(res)
        id = REXML::XPath.first( doc, '/cid/@c_lu_id' ).to_s
	$stderr.puts "GET CID ARR = /cid[@c_lu_id='#{id}']"
        ar = dictionary.xquery_to_array("/cid[@c_lu_id='#{id}']")
        ar.each{|xml|
          cidar << CobraVsMongoose.xml_to_hash(xml.to_s)
        }
      rescue
      end
      cidar.sort!{|x,y| y['cid']['@score'].to_i <=> x['cid']['@score'].to_i}
      return {'cid_array'=>cidar}
  end
  
  def get_cid_array_lu(lu_id, dictionary)
      cidar = []
      begin
$stderr.puts "CID ARRAY LU=/cid[@c_lu_id='#{lu_id}']"
        ar = dictionary.xquery_to_array("/cid[@c_lu_id='#{lu_id}']")
        ar.each{|xml|
          cidar << CobraVsMongoose.xml_to_hash(xml.to_s)
        }
      rescue => e
        $stderr.puts e
      end
      return {'cid_array'=>cidar}
  end

  def select_best_mapping(lu_id, dictionary)
    alt = get_cid_array_lu(lu_id, dictionary)
    maps = {}
    alt['cid_array'].each{|xml|
      score = xml['cid']['@score'].to_f
      id = xml['cid']['@cid_id'].to_s
      $stderr.puts score.to_s  + ' ' +id
      maps[id] = score
    }
    if maps.size > 0
      selected = maps.shift[0]
    else
      selected = nil
    end
    $stderr.puts "SELECT NEW BEST" + selected.to_s
    return selected
  end

  def form_status_text(selected, status)
    if selected == 'true' and status == 'manual'
      short = 'MC'
    elsif selected == 'false' and status == 'manual'
      short = 'MR'
    elsif selected == 'true' and status == 'automatic'
      short = 'AC'
    elsif selected == 'false' and status == 'automatic'
      short = 'AR'
    elsif status == 'neutral'
      short = 'N'
    else
      short = status + '-' + ((selected == 'true')? 'C' : 'R')
    end
    return short
  end
end
