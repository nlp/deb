require 'template'
require 'cobravsmongoose'
#require 'unicode'
require 'json'
require 'stringio'
require 'fastercsv'
require 'open-uri'

class TecuServlet < DictServlet

  def initialize( server, dict, array )
    super
    @servername = nil
    @hypers = nil
    @import_path = '/var/lib/deb-server/files/tecu'
    @var_path = '/var/www/import'
  end

  def wsdl_message(params)
    res = '<result>'
    params.each{|k,v|
      res += '<'+k+'>'+v.to_s+'</'+k+'>'
    }
    res += '</result>'
    return res
  end

  def hash_to_wsdl(data)
    res = '<result-list>'
    data.each{|h|
      res += '<result-entry>'
      h.each{|k,v|
        res += '<'+k+'>'+v.to_s+'</'+k+'>'
      }
      res += '</result-entry>'
    }
    res += '</result-list>'
    return res
  end

  def array_to_wsdl(data)
    res = '<result-list>'
    data.each{|v|
      res += '<result-entry>'
      if v.is_a?(Array)
        res += array_to_wsdl(v)
      elsif v.is_a?(Hash)
        res += hash_to_wsdl(v)
      else
        res += v.to_s
      end
      res += '</result-entry>'
    }
    res += '</result-list>'
    return res
  end

  def do_GET(request, response)
    @servername = 'https://'+request.host if @servername == nil
    user = request.attributes[:auth_user].to_s

    case request.query['action']
    when 'search'
      if request.query['search'].to_s.length > 2
        res = @dict.search(request.query['search'].to_s.gsub("(",""), request.query['status'].to_s)
        if request.query['format'].to_s == 'html'
          response.body = @dict.apply_transform('search', hash_to_wsdl(res),  [ [ 'search', '"'+request.query['search'].to_s+'"'] ])
          response['Content-Type'] = 'text/html; charset=utf-8'
        elsif request.query['format'].to_s == 'wsdl'
          response.body = hash_to_wsdl(res)
          response['Content-Type'] = 'application/xml; charset=utf-8'
        else
          response.body = res.to_json
          response['Content-Type'] = 'application/json; charset=utf-8'
        end
      else
        if request.query['format'].to_s == 'wsdl'
          response.body = wsdl_message({'result'=>'error','message'=>"Nezadán text, délka 2"})
          response['Content-Type'] = 'application/xml; charset=utf-8'
        else
          response.body = '{"error":true, "msg":"Nezadán text, délka 2"}'
          response['Content-Type'] = 'application/json; charset=utf-8'
        end
      end
    when 'get_deleted'
        res = @dict.get_deleted
        if request.query['format'].to_s == 'html'
          response.body = @dict.apply_transform('search', hash_to_wsdl(res), [ [ 'deleted', '"true"'] ])
          response['Content-Type'] = 'text/html; charset=utf-8'
        elsif request.query['format'].to_s == 'wsdl'
          response.body = hash_to_wsdl(res)
          response['Content-Type'] = 'application/xml; charset=utf-8'
        else
          response.body = res.to_json
          response['Content-Type'] = 'application/json; charset=utf-8'
        end
    when 'get_subtree'
      if request.query['id'].to_s != ''
        res = @dict.get_subtree(request.query['id'].to_s, @hypers)
        if request.query['format'].to_s == 'wsdl'
          response.body = hash_to_wsdl(res)
          response['Content-Type'] = 'application/xml; charset=utf-8'
        else
          response.body = res.to_json
          response['Content-Type'] = 'application/json; charset=utf-8'
        end
      else
        if request.query['format'].to_s == 'wsdl'
          response.body = wsdl_message({'result'=>'error','message'=>"Nezadáno ID"})
          response['Content-Type'] = 'application/xml; charset=utf-8'
        else
          response.body = '{"error":true, "msg":"Nezadáno ID"}'
          response['Content-Type'] = 'application/json; charset=utf-8'
        end
      end
    when 'export_subtree'
      if request.query['id'].to_s != ''
        res = @dict.export_subtree(request.query['id'].to_s, @hypers)
        if request.query['format'].to_s == 'wsdl'
          response.body = array_to_wsdl(res)
          response['Content-Type'] = 'application/xml; charset=utf-8'
        elsif request.query['format'].to_s == 'json'
          response.body = res.to_json
          response['Content-Type'] = 'application/json; charset=utf-8'
        else
          csvstring = FasterCSV.generate({:col_sep => "\t",:force_quotes=>true}){|csv| 
            csv << ['id', 'status','termin','jazyk terminu','definice','obor','reference','hyperonyma','viz','jazyk viz','tez','jazyk tez']
            res.each{|r| csv << r}
          }
          response.body = csvstring
          response['Content-Type'] = 'application/octet-stream'
          response['Content-Disposition'] = 'attachment; filename=tecu_export_'+request.query['id'].to_s+'.csv'
        end
      else
        if request.query['format'].to_s == 'wsdl'
          response.body = wsdl_message({'result'=>'error','message'=>"Nezadáno ID"})
          response['Content-Type'] = 'application/xml; charset=utf-8'
        else
          response.body = '{"error":true, "msg":"Nezadáno ID"}'
          response['Content-Type'] = 'application/json; charset=utf-8'
        end
      end
    when 'export_tree'
        res = @dict.export_tree_struct(@hypers, '0', 0)
        response.body = res
        response['Content-Type'] = 'text/plain; charset=utf-8'
    when 'get_path'
      if request.query['id'].to_s != ''
        res = @dict.get_path(request.query['id'].to_s)
        if request.query['format'].to_s == 'wsdl'
          response.body = array_to_wsdl(res)
          response['Content-Type'] = 'application/xml; charset=utf-8'
        else
          response.body = res.to_json
          response['Content-Type'] = 'application/json; charset=utf-8'
        end
      else
        if request.query['format'].to_s == 'wsdl'
          response.body = wsdl_message({'result'=>'error','message'=>"Nezadáno ID"})
          response['Content-Type'] = 'application/xml; charset=utf-8'
        else
          response.body = '{"error":true, "msg":"Nezadáno ID"}'
          response['Content-Type'] = 'application/json; charset=utf-8'
        end
      end
    when 'get_top'
      res = @dict.get_top
      if request.query['format'].to_s == 'wsdl'
        response.body = hash_to_wsdl(res)
        response['Content-Type'] = 'application/xml; charset=utf-8'
      else
        response.body = res.to_json
        response['Content-Type'] = 'application/json; charset=utf-8'
      end
    when 'gettrans'
      term = request.query['term']
      lang = request.query['language'] or 'en'
      port = request.query['port'] or '8080'
      domain = request.query['domain'] or 'ske.fi.muni.cz'
      response['Content-Type'] = 'text/plain; charset=utf-8'
      output = %x(python #{@import_path}/transcand/get_candidates.py "#{term}" #{lang} #{domain} #{port})
      response.body = output
    when 'getdoc'
      key = URI.escape(request.query['id'])
      transform = request.query['tr'].to_s
      perm = @dict.get_perm(user)
      begin
        if request.query['type'] == 'del'
          res = @dict_array['tecudel'].get(key)
        else
          res = @dict.get_doc(key)
        end
        if res != ''
          if transform != ''
            response["Content-type"] = "text/html; charset=utf-8"
            res = @dict.apply_transform(transform, res.to_s, [ [ 'perm', '"'+perm+'"'], [ 'type', '"'+request.query['type'].to_s+'"'] ])
          elsif request.query['json'].to_s == '1' or request.query['format'].to_s == 'json'
            response['Content-Type'] = 'application/json; charset=utf-8'
            doc = @dict.load_xml_string(res.to_s)
            doc.find('/entry/defs/def').each{|de|
              dec = de.content.to_s
              de.content = dec
            }
            hash = CobraVsMongoose.xml_to_hash(doc.to_s)
            #normalizace JSON
=begin
            if hash['entry']['trans'] != nil and hash['entry']['trans']['tr'].is_a?(Hash)
              h = hash['entry']['trans']['tr']
              hash['entry']['trans']['tr'] = [h]
            end
            if hash['entry']['hyper'].is_a?(Hash)
              h = hash['entry']['hyper']
              hash['entry']['hyper'] = [h]
            end
            if hash['entry']['see'].is_a?(Hash)
              h = hash['entry']['see']
              hash['entry']['see'] = [h]
            end
            if hash['entry']['also'].is_a?(Hash)
              h = hash['entry']['also']
              hash['entry']['also'] = [h]
            end
            if hash['entry']['domains'] != nil and hash['entry']['domains']['dom'].is_a?(Hash)
              h = hash['entry']['domains']['dom']
              hash['entry']['domains']['dom'] = [h]
            end
            if hash['entry']['defs'] != nil and hash['entry']['defs']['def'].is_a?(Hash)
              h = hash['entry']['defs']['def']
              hash['entry']['defs']['def'] = [h]
            end
            if hash['entry']['refs'] != nil and hash['entry']['refs']['ref'].is_a?(Hash)
              h = hash['entry']['refs']['ref']
              hash['entry']['refs']['ref'] = [h]
            end
            if hash['entry']['sees'] != nil and hash['entry']['sees']['see'].is_a?(Hash)
              h = hash['entry']['sees']['see']
              hash['entry']['sees']['see'] = [h]
            end
            if hash['entry']['alsos'] != nil and hash['entry']['alsos']['also'].is_a?(Hash)
              h = hash['entry']['alsos']['also']
              hash['entry']['alsos']['also'] = [h]
            end
            if hash['entry']['terms'] != nil and hash['entry']['terms']['term'].is_a?(Hash)
              h = hash['entry']['terms']['term']
              hash['entry']['terms']['term'] = [h]
            end
=end
            res = hash.to_json
          else
            response["Content-type"] = "text/xml; charset=utf-8"
          end
          response.body = res
        end
      rescue => e
        $stderr.puts e
        $stderr.puts e.backtrace
        if request.query['format'].to_s == 'wsdl'
          response.body = wsdl_message({'result'=>'error','message'=>e})
          response['Content-Type'] = 'application/xml; charset=utf-8'
        elsif request.query['format'].to_s == 'json'
          response.body = '{"error":true, "msg":"'+e+'"}'
          response['Content-Type'] = 'application/json; charset=utf-8'
        else
          response.body = e
        end
      end
    when 'delete'
      if @dict.get_perm(user) != 'w'
        if request.query['format'].to_s == 'wsdl'
          response.body = wsdl_message({'result'=>'error','message'=>"nemáte oprávnění k editaci"})
          response['Content-Type'] = 'application/xml; charset=utf-8'
        else
          response.body = '{"error":true, "msg":"nemáte oprávnění k editaci"}'
          response['Content-Type'] = 'application/json; charset=utf-8'
        end
        return response
      end
      id = request.query['id'].to_s
      begin
        xml = @dict.get(id)
        @dict_array['tecudel'].update(id, xml)
        @dict.delete(id)
        if request.query['format'].to_s == 'wsdl'
          response.body = wsdl_message({'result'=>'deleted','id'=>id})
          response['Content-Type'] = 'application/xml; charset=utf-8'
        else
          response.body = '{"ok":true, "msg":"deleted", "id": "'+id+'"}'
          response['Content-Type'] = 'application/json; charset=utf-8'
        end
      rescue => e
        $stderr.puts e
        $stderr.puts e.backtrace
        if request.query['format'].to_s == 'wsdl'
          response.body = wsdl_message({'result'=>'error','message'=>e})
          response['Content-Type'] = 'application/xml; charset=utf-8'
        else
          response.body = '{"error":true, "msg":'+e+'}'
          response['Content-Type'] = 'application/json; charset=utf-8'
        end
      end
    when 'undelete'
      if @dict.get_perm(user) != 'w'
        if request.query['format'].to_s == 'wsdl'
          response.body = wsdl_message({'result'=>'error','message'=>"nemáte oprávnění k editaci"})
          response['Content-Type'] = 'application/xml; charset=utf-8'
        else
          response.body = '{"error":true, "msg":"nemáte oprávnění k editaci"}'
          response['Content-Type'] = 'application/json; charset=utf-8'
        end
        return response
      end
      id = request.query['id'].to_s
      begin
        xml = @dict_array['tecudel'].get(id)
        @dict_array['tecu'].update(id, xml)
        @dict_array['tecudel'].delete(id)
        if request.query['format'].to_s == 'wsdl'
          response.body = wsdl_message({'result'=>'undeleted','id'=>id})
          response['Content-Type'] = 'application/xml; charset=utf-8'
        else
          response.body = '{"ok":true, "msg":"undeleted", "id": "'+id+'"}'
          response['Content-Type'] = 'application/json; charset=utf-8'
        end
      rescue => e
        $stderr.puts e
        $stderr.puts e.backtrace
        if request.query['format'].to_s == 'wsdl'
          response.body = wsdl_message({'result'=>'error','message'=>e})
          response['Content-Type'] = 'application/xml; charset=utf-8'
        else
          response.body = '{"error":true, "msg":'+e+'}'
          response['Content-Type'] = 'application/json; charset=utf-8'
        end
      end

    when 'gethypcand'
        response['Content-Type'] = 'text/plain; charset=utf-8'
        fn = @var_path + '/hyp_cands.txt'
        if File.exist?(fn)
            response.body = File.open(fn).read
        else
            response.body = "FILE #{fn} DOES NOT EXIST"
        end

    when 'import'
        $stderr.puts 'UPLOAD'
        data_file = request.query['input_file']
        config_type = request.query['config_type']
        configuration = request.query['configuration']
        multif = request.query['number_of_files'].to_i
        input_file_path = '/tmp/tecu_import_input_file.txt'
        conf_file_path = '/tmp/tecu_import_config_file.py'
        out_file_path = '/tmp/tecu_import_output_file.xml'
        if config_type == 'other' and configuration.to_s == ''
            response.body = '{"error": true, "msg": "missing configuration"}'
            response['Content-Type'] = 'application/json; charset=utf-8'
            response.status = 500
            return response
        elsif config_type == 'other'
            cnf = File.open(conf_file_path, 'r')
            cnf.write(configuration.to_s)
            cnf.close
        elsif config_type == 'html'
            conf_file_path = @import_path + '/html_conf.py'
        elsif config_type == 'csv'
            conf_file_path = @import_path + '/csv_conf.py'
        else
            response.body = '{"error": true, "msg": "unknown configuration type"}'
            response['Content-Type'] = 'application/json; charset=utf-8'
            response.status = 500
            return response
        end
        if data_file.nil?
            response.body = '{"error": true, "msg": "missing input file"}'
            response['Content-Type'] = 'application/json; charset=utf-8'
            response.status = 500
            return response
        else
            inf = File.open(input_file_path, "wb")
            data_file.each_data{|d|
                inf.syswrite d
            }
            inf.close
        end
        if @dict.get_perm(user) != 'w'
            response.body = '{"error": true, "msg": "access error"}'
            response['Content-Type'] = 'application/json; charset=utf-8'
            response.status = 403
            return response
        else
            $stderr.puts "python import.py -i #{input_file_path} -c #{conf_file_path} > #{out_file_path}"
            File.open(input_file_path).delete()
            response.body = '{"msg": "ok"}'
        end

    when 'stats'
      if request.query['format'].to_s == 'wsdl'
        response.body = array_to_wsdl([@dict.get_stats])
        response['Content-Type'] = 'application/xml; charset=utf-8'
      else
        response.body = @dict.get_stats.to_json
        response['Content-Type'] = 'application/json; charset=utf-8'
      end
      return response
    
    when 'history'
      xml = @dict.get_history
      response["Content-type"] = "text/html; charset=utf-8"
      response.body = @dict.apply_transform('history', xml)

    when 'highlight'
      result = `/var/www/termcheck/run.cgi "#{request.query['text']}" "#{@servername}"`
      response.body = result
      response["Content-type"] = "text/html; charset=utf-8"

    when 'save'
      if @dict.get_perm(user) != 'w'
        if request.query['format'].to_s == 'wsdl'
          response.body = wsdl_message({'result'=>'error','message'=>"nemáte oprávnění k editaci"})
          response['Content-Type'] = 'application/xml; charset=utf-8'
        else
          response.body = '{"error":true, "msg":"nemáte oprávnění k editaci"}'
          response['Content-Type'] = 'application/json; charset=utf-8'
        end
        return response
      end
      id = request.query['id'].to_s
      if id == '' and request.query['data'].to_s != ''
        id = @server.info.get_next_id('tecu')
        $stderr.puts 'NEW ID '+id
      end
      if id != '' and request.query['data'].to_s != ''
        begin
          data_hash = JSON.parse(request.query['data'])
          data_hash['entry']['@id'] = id
          current_time = Time.now.to_i.to_s
          data_hash['entry']['@timestamp'] = current_time
          unless data_hash['entry']['hyper'].nil?
            data_hash['entry']['hyper'].each{|h|
              @dict.hypers << h['@id']
            }
          end
          terms = []
          terms_lang = data_hash['entry']['@lang']
          # remove multiple spaces and strip terms
          data_hash['entry']['terms']['term'].each{|x|
              x['$'] = x['$'].squeeze(' ').strip
              terms << x['$']
              next if x['@lang'] != 'cs'
              @dict.search(x['$']).each{|y|
                  if y['head'].squeeze(' ').strip == x['$'] and id.to_i != y['id'].to_i
                      #response.status = 500
                      #response['Content-Type'] = 'text/plain; charset=utf-8'
                      msg = "heslo #{y['head']} (#{y['id']}) již v tezauru existuje"
                      response.body = '{"error":true, "msg":"'+msg+'"}'
                      response['Content-Type'] = 'application/json; charset=utf-8'
                      return response
                  end
              }
          }
          # fail when adding an existing term
          # remove reflexive hyperonyms
          filtered_hypers = []
          data_hash['entry']['hyper'].each{|x|
              if x['@id'].to_i != id.to_i and not @dict.get_path(x['@id']).flatten.include?(x['@id'])
                  filtered_hypers << x
              end
          }
          data_hash['entry']['hyper'] = filtered_hypers
          # remove reflexive sees
          filtered_sees = []
          data_hash['entry']['sees']['see'].each{|x|
              if not terms.include?(x['$']) or x['@lang'] != terms_lang
                  filtered_sees << x
              end
          }
          data_hash['entry']['sees']['see'] = filtered_sees
          # remove reflexive alsos
          filtered_alsos = []
          data_hash['entry']['alsos']['also'].each{|x|
              if not terms.include?(x['$']) or x['@lang'] != terms_lang
                  filtered_alsos << x
              end
          }
          data_hash['entry']['alsos']['also'] = filtered_alsos
          oldxml = @dict.get(id)
          data_hash['entry']['notes'] = {}
          if oldxml != ''
            #prevzit historii hesla
            old_hash = CobraVsMongoose.xml_to_hash(oldxml)
            unless old_hash['entry']['notes'].nil?
              if old_hash['entry']['notes']['note'].is_a?(Array)
                data_hash['entry']['notes']['note'] = old_hash['entry']['notes']['note']
              end
              if old_hash['entry']['notes']['note'].is_a?(Hash)
                data_hash['entry']['notes']['note'] = [old_hash['entry']['notes']['note']]
              end
            end
          end
          data_hash['entry']['notes']['note'] = [] if data_hash['entry']['notes']['note'].nil?
          #pridat poznamku
          new_comment,new_xpath = data_hash['entry']['comment'].split(';xpath:')
          data_hash['entry']['notes']['note'] << {
            '@author' => user,
            '@time' => Time.now.strftime('%Y-%m-%d %H:%M:%S'),
            '@xpath' => new_xpath,
            '$' => new_comment,
            '@number' => (data_hash['entry']['notes']['note'].size+1)
          }
          data_hash['entry'].delete('comment')
          xml = CobraVsMongoose.hash_to_xml(data_hash)
          Thread.new{ @dict.highlight_def(id, xml, @servername, current_time) }
          @dict.update(id, xml)
          if request.query['format'].to_s == 'wsdl'
            response.body = wsdl_message({'result'=>'saved','id'=>id})
            response['Content-Type'] = 'application/xml; charset=utf-8'
          else
            response.body = '{"ok":true, "msg":"saved", "id": "'+id+'"}'
            response['Content-Type'] = 'application/json; charset=utf-8'
          end
        rescue => e
          $stderr.puts e
          $stderr.puts e.backtrace
          if request.query['format'].to_s == 'wsdl'
            response.body = wsdl_message({'result'=>'error','message'=>e})
            response['Content-Type'] = 'application/xml; charset=utf-8'
          else
            response.body = '{"error":true, "msg":'+e+'}'
            response['Content-Type'] = 'application/json; charset=utf-8'
          end
        end
      else
        if request.query['format'].to_s == 'wsdl'
          response.body = wsdl_message({'result'=>'error','message'=>"Nezadáno ID nebo data"})
          response['Content-Type'] = 'application/xml; charset=utf-8'
        else
          response.body = '{"error":true, "msg":"Nezadáno ID nebo data"}'
          response['Content-Type'] = 'application/json; charset=utf-8'
        end
      end
      return response
    end
  end
  
  alias do_POST do_GET
end
