require 'webrick'
require 'cobravsmongoose'

class SumoServlet < JSWordnetServlet
 #devel
#def SumoServlet.get_instance config, *options
#load __FILE__
#SumoServlet.new config, *options
#end

  def do_GET(request, response)
    case request.query['action']
    when 'queryList'
      query = URI.unescape(request.query['word']).to_s.strip
      response["Content-type"] = "text/plain; charset=utf-8"
      
      if query == ''
        $stderr.puts "Query not set"
        response.body += "Query not set".to_json
      else
        sumos = @wn.get_list(query, @wn_array)
        su_ar = []
        sumos.each {|id,info|
          label = info['form']
          su_ar << {'value' => id, 'label' => label}
        }
        response.body += su_ar.sort!{|x,y| x['label']<=>y['label']}.to_json
      end
    else
      super(request, response)
    end
  end
  
  alias do_POST do_GET 

  def runQuery(dictionary, query, type, arg, dictcode, default='', expand='', tree_details = 0, display_only='', reldics=[])
  	resxml = dictionary.get(query)
    res = resxml.to_s
  	$stderr.puts resxml
  	case type
    when 'plain'
      res = dictionary.get_rilr(res)
      return res
  	when 'xml'
      res = dictionary.get_rilr(res)
      res = dictionary.apply_transform( 'xml', res, [ [ 'slovnik', '"'+dictcode.to_s+'"'], [ 'servername', '"'+@servername.to_s+'"' ] ] )
  		return res
  	when 'html'
      res = dictionary.get_rilr(res)
      transform = 'preview'
      transform += arg if arg.to_s != ''
      res = dictionary.apply_transform( transform, res, [ [ 'slovnik', '"'+dictcode.to_s+'"'], [ 'servername', '"'+@servername.to_s+'"' ], ['default', '"'+default.to_s+'"'] ] )
  		return res
    when 'tree'
      array = []
      if arg == ''
        arg = 'ILR'
      end
      res = dictionary.get_rilr(res)
      tree = dictionary.get_tree(res, arg)
      tree.each {|ar|
        array << {'parent' => ar[0], 'id' => ar[1], 'val' => ar[2]}
      }
      array.reverse!
      return array
 
  	end
  end

end
