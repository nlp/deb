        <div class="czj-info-inner" style="width: 1100px; text-align: justify;">
<div class="czj-expin" style="padding-bottom: 0px; float: right; width: 400px;">
	<div style="width: 320px; font-size: 13px; text-align: left; " class="czj-expin-inner" >

<strong>Dictio financial resources:</strong>

<p>2017-2022: MU; <strong>Masaryk University 4.0 (MUNI 4.0)</strong></p>

<p><img src="/media/img/erasmus_plus.png"  align="right" />2015-2018: STU;<strong> UniAll - Accessibility of Higher Education for Students with Special Needs</strong> - Selected entries of <a href="http://www.dictio.info/admin?autocompbox=1&def_skup=aka&skup=ne&lang=en&action=report&filter=true">ČZJ</a>, <a href="http://www.dictio.info/admin?slovni_druh=subst&completenessbox=&autocompbox=1&def_skup=informatika&skup=ne&komentbox=&lang=en&action=reportspj&filter=true">SPJ</a> and <a href="http://www.dictio.info/admin?schvcelni=&schvbocni=&action=reportis&filter=true&slovni_druh=subst&completenessbox=&autocompbox=1&def_skup=aka&skup=ne&lang=en">IS</a> containing vocabulary in the field of higher education processed by team AKA and TUB and AKAI.</p>

<p><img src="/editor/img/nf.png" align="right" />2015 - 2016: MU; <strong>Norway Funds and EHP funds (CZ07 – EEA and Norway Scholarship Programme)
</strong><br />
Signs for dictionary of International signs.
</p>
<p>2012 - 2014: MU; <strong>Network of Expert Centres Providing Inclusion in Tertiary Education (ExpIn</strong>) <img src="/editor/img/expin.png" align="right" /><br />
Teams: "biology", "informatics", "special education", "UPOL"</p>

<p>2009 - 2012: MU; <strong>Universal Learning Design – Innovations in Interpreting and Communication Services (ComIn)</strong><br />
Teams: "biology", "UPOL"</p>                                                         

<p>2009 - 2012: ZČU; <strong>Language Handicap Elimination for Hearing Impaired Students via Automatic Language Processing</strong>
</p>

<p>2005 - 2007: MU; <strong>Integration Module of Tertiary Education for the Sensory Impaired (IMoTeSP)</strong><br />
Team "informatics"</p>

</div>
</div>


<h2>Aims and functions of the dictionary</h2>              
  <p> The Dictio is an online monolingual dictionary, which has been developed to provide support to university students communicating in sign language, sign language interpreters and translators, and sign language linguists. It is gradually growing to become a helpful tool for wide public use, as it enables to present standard as well as specialized lexis of both sign (visual-motoric) and spoken (audio-oral) languages in one and the same interface. <br />At present the Dictio provides information on the lexis of Czech Sign Language (CZJ), the Czech language, some lexis of Slovak Sign Language (SPJ), American Sign Language (ASL) and International Sign System (IS).    
  </p>  
  
  <h3>Parts of the Dictio</h3>
The Dictio contains two connected parts: the Translate and the Dictionary. <br /><strong>The Translate </strong>is primarily designed for searching and translations among contained languages. <br />
<strong>The Dictionary</strong> provides detailed informations about single entries (eg. grammatical information, explanations, examples of use)
  
  <h3>Published Entries</h3>
<ul><li>The Translate provides only the entries, that contains translations approved by interpreter or translator. </li>
<li>The Dictionary provides only the entries, that contains transcription (SignWritign), explanation and example of use.</li></ul>

<div class="czj-expin">
	<div style="width: 600px; font-size: 13px" class="czj-expin-inner">
<img src="/media/img/info.png" style="float: left"/>More entries that are provided at the Dictionary or the Translate is possible after login. Acces can be given to university students, interpreters, linguists etc.<br />
If you like to get access to not published entries, please send us email to address: spravci@dictio.info
</div></div>

<h3>Covered publications</h3>
  

  <strong>Czech Sign Language</strong>:       
  <ul>    
    <li>LANGER, J. a kol. Znaková zásoba českého znakového jazyka k rozšiřujícímu studiu surdopedie se zaměřením na znakový jazyk (1. a 2. díl). 2. doplněné vydání, Olomouc: Univerzita Palackého, 2005, 27 s. ISBN 80-244-1113-X     
    </li>    
    <li>LANGER, J. a kol. Znaková zásoba českého znakového jazyka k rozšiřujícímu studiu surdopedie se zaměřením na znakový jazyk (3. a 4. díl). 2. doplněné vydání, Olomouc: Univerzita Palackého, 2005, 31 s. ISBN 80-244-1114-8     
    </li>    
    <li>LANGER, J. a kol. Slovník vybraných pojmů znakového jazyka pro oblast dopravní výchovy[CD-ROM]. Praha: Fortuna, 2009, 24 s. ISBN 978-80-7373-077-2     
    </li>    
    <li>LANGER, J. a kol. Slovník pojmů znakového jazyka pro oblast tělesné výchovy a sportu [CD-ROM]. Praha: Fortuna, 2006, 32 s. ISBN 80-7168-986-6     
    </li>    
    <li>LANGER, J. a kol. Slovník pojmů znakového jazyka pro oblast vlastivědy [CD-ROM]. Praha: Fortuna, 2007, 17 s. ISBN 978-80-7373-019-2     
    </li>    
    <li>LANGER, J. a kol. Slovník vybraných pojmů znakového jazyka pro oblast biologie člověka a zdravovědy [CD-ROM]. Praha: Fortuna, 2008, 24. ISBN 978-80-7373-041-3.     
    </li>
    <li>FRITZ, Milan. Znaky pro základní kalendářní jednotky v českém znakovém jazyce. Bakalářská práce. Praha: Univerzita Karlova v Praze, 2014.</li>  
  </ul>  
  
  
  <p>This <strong>Czech dictionary </strong>integrates the lexis of significant Czech dictionaries, such as Dictionary of Standard Czech, Dictionary of Foreign Words, and Online Language Guide.           
    <ul>      
      <li>Martincová, O. a kol. Nová slova v češtině: slovník neologizmů. Praha: Academia, 1998.       
      </li>      
      <li>Martincová, O. a kol. Nová slova v češtině: slovník neologizmů 2. Praha: Academia, 2004.       
      </li>      
      <li>Kraus, J. a kol. Nový akademický slovník cizích slov. Praha: Academia, 2005.       
      </li>      
      <li>Filipec, J. a kol. Slovník spisovné češtiny pro školu a veřejnost. 4. vydání, obsahově totožné se 3. Praha: Academia, 2005.       
      </li>    
    </ul>  
  </p>        
  <p>      The data from these dictionaries is processed and structured in a way that allows the entries in sign language and in written language to be presented in the same manner.    
  </p>    <h3>The Dictionary Entry Content:</h3>
  
  <strong>Cech Sign Language</strong>  
  <ul>    
    <li>        Headword (lemma) in CZSL as a video from two different perspectives (front and side view)      
    </li>    
    <li>Grammar information (word class characteristics, morphological irregularities)      
    </li>    
    <li>Etymology of the sign      
    </li>    
    <li>Stylistics information (regional or other restrictions in usage, for specialized terms or other neologisms also the author of the proposal)      
    </li>    
    <li>Transcription into SignWriting or HamNoSys (enables search)      
    </li>    
    <li>Semantic field        
    </li>    
    <li>Description of meaning of the CZSL expression, in CZSL      
    </li>    
    <li>Example of use in an authentic CZSL utterance      
    </li>    
    <li>Semantically superordinate and subordinate, synonymic and antonymic expressions in CZSL      
    </li>    
    <li>Phraseological units with the given CZSL expression, with examples of use in an authentic CZSL utterance      
    </li>    
    <li>Translations into Czech     
    </li>  
  </ul>  <strong>Czech language</strong>:    
  <ul>    
    <li>Headword in Czech in the basic dictionary form (lemma)      
    </li>    
    <li>Origin of the word      
    </li>    
    <li>Czech grammar information (word class characteristics, morphological paradigm, verb valency, prepositional phrases)      
    </li>    
    <li>Czech stylistics information (distributoin area, expressive function, etc.)      
    </li>    
    <li>Semantic field       
    </li>    
    <li>Description of meaning of the Czech expression, in Czech      
    </li>    
    <li>Syntactical relations and phraseological units, with examples of use in an authentic Czech utterance      
    </li>    
    <li>Semantically superordinate and subordinate, synonymic and antonymic expressions      
    </li>    
    <li>Translation into Czech Sign Language     
    </li>  
  </ul>  
   <a name="skupiny"></a>
<h2>Work teams</h2>  
  <p>  The work teams invited to collaborate on the Czech Sign Language dictionary are all Czech teams dealing with Czech Sign Language, its teaching, translating and research – that is groups in charge of elicitation of spontaneously existing sign lexis as well as teams reaching suggestions for the terminological development of Czech Sign Language. For managing the dictionary, a board of experts – Czech Sign Language users, a board of experts on lexicography and a technical team are to be established.    
  </p>  

<h3>AKAI</h3>
<strong>2018 /IS/</strong>: 
Mgr. et Mgr. Tomáš Sklenák, Ondřej Klofáč

<h3>Teiresiás AKA</h3>
<strong>2018 /ČZJ/</strong>: 
Mgr. et Mgr. Tomáš Sklenák, Mgr. Lucie Štefková, Bc. Martin Kulda, Mgr. Vlastimil Chlumský, Jindřich Mikulík, Tomáš Zbavitel, Mirka Tylová, Bc. Marie Pangrácová, Bc. Dana Peňázová, Mgr. BcA. Pavel Kučera Ph.D., Mgr. Radka Stará

<h3>Teiresiás BIO</h3>
<strong>2009 - 2010 /ČZJ/</strong>: Bc. Dana Peňázová, Magda Jandusová, Nikola Novotná, Dagmar Kovaříková, Mgr. Pavel Kučera, Martin Zrnečko, Miroslava Kočková, Iveta Smutná, Lenka Richterová, Marie Basovníková, Markéta Spilková, Pavlína Spilková, Tomáš Pruša, Milan Urík, Pavel Hurník           <br /><br />

<!-- <h3>Teiresiás 3a (BIO2)</h3>-->
<strong>2011-2014 /ČZJ/</strong>: Mgr. Lucie Štefková, Marie Pangrácová, Milan Fritz, Bc. Magda Jandusová, Bc. Nikola Novotná, Bc. Dagmar Kovaříková, Zuzana Jakešová (do r.2013), Bc. Dana Peňázová, MUDr. Pavel Hurník, Mgr. Bc. Tomáš Pruša, MUDr. Milan Urík (do r. 2012), Bc. Marie Basovníková (do r. 2013), Bc. Martin Kulda, Hana Wiesnerová (od 2014)
 
 <h3>Teiresiás INF</h3>
  <strong>2005 - 2007 /ČZJ/</strong>: Bc. Tomáš Sklenák, Martin Kulda, František Tichý, Lukáš Michl, Radek Červinka, David Podsedník, Robert Zaoral, Marek Bednařík, Tomáš Wirth, Leoš Materna, Jana Lapčáková, Petr Čížek, ing. Aleš Kastner<br />
<!-- <h3>Teiresiás INF2</h3> -->
 <strong>2013 - 2014  /ČZJ/</strong>: Mgr. Tomáš Sklenák, František Tichý, Bc. Martin Kulda, Bc. Radek Červinka, Lubomír Hykl, Martin Ťulák, Jakub Peřina, René Ratajský, Bc. Marek Bednařík, Martin Paulík (2014)
 
  <h3>Teiresiás MNP</h3>
 <strong>2006 /ČZJ/</strong>: Bc. Tomáš Sklenák, Zlatuše Kurcová, Mgr. Lucie Kastnerová, Martin Kulda, BcA. Daniela Pořízková, Monika Zelinková, Michaela Camperová, Iveta Smutná, Bc. Dana Peňázová, Mgr. Pavel Kučera, Mgr. Radka Faltínová, Mgr. Marie Horáková, Mgr. Tomáš Horák, Anna Diblíková, René Ratajský, Anna Pangrácová
 
  <h3>Teiresiás OZZ</h3>
   <strong>2013 - now /ČZJ/</strong>: PaedDr. Roman Vojtechovský, Mgr. Jana Wagnerová, Ondřej Klofáč, Miroslava Tylová, Bc. Dagmar Máliková, Mgr. Hana Strachoňová, PhDr. Petr Peňáz<br />
             Viktor Sersen (2014), Radim Minárik (2014), Jan Borovanský (2014), Marie Mikulíková (2014) 
         
  
  <h3>Teireisás S1</h3>
<strong>2005 - 2007 /SignWriting/</strong>: Bc. Jan Fikejs, Mgr. Alexandr Zvonek, Mgr. Pavel Kučera, Bc. Dana Peňázová
<h3>Teiresiás S2</h3>
<strong>2008 - 2014 /SignWriting/</strong>: Bc. Jan Fikejs, Bc. Jitka Václavíková (2008-14, 2017-nyní), Bc. Jitka Tarabová (2008-2011), Bc. Žaneta Petraturová (2008-14), Mgr. Alena Sliacka (2011-2016), Anna Pangrácová (2012), Nikola Novotná (2012)<br />

                  

<h3>Teiresiás SPED</h3>
<strong>2014 /ČZJ/</strong>: Mgr. Lucie Štefková, Mgr. Bc. Tomáš Sklenák, Bc. Monika Zelinková, Bc. et Bc. Jiří Procházka, Vojtěch Pražák, Bc. Daniela Tesaříková, Mgr. Ivana Kupčíková, DiS.

<h3>Team working on Czech dictionary</h3>
<strong>2014 - now</strong>: Mgr. Hana Strachoňová, Mgr. Jana Zemanová, Ph.D (2014), Mgr. Kateřina Najbrtová (2014-15)

<h3>TUB</h3>
<strong>2018 /SPJ/</strong>: Mgr. Vanda Šinkovičová, Ing. Jozef Jančovič, Mgr. Radoslav Petrík, MgA. Michal Hefty, Mgr. Richard Schramm, PaedDr. Ondrej Feč, PaedDr. Bc. Roman Vojtechovský

<h3>TUB SOC</h3>
<strong>2018 /SPJ/</strong>: PaedDr. Bc. Roman Vojtechovský, Mgr. Vladimíra Beliková PhD, Mgr. Veronika Vojtechovská

<h3>UKB</h3>
<strong>2018 /SPJ/</strong>:
 prof. PaedDr. Darina Tarcsiová PhD., Mgr. Vladimíra Beliková PhD., Mgr. Veronika Vojtechovská, Mgr. Silvia Doményová, Mgr. Jozef Rigo, PaedDr. Bc. Roman Vojtechovský



</div>       