require 'webrick'
require 'webrick/httpauth/userdb'
require 'webrick/httpauth/basicauth'
require 'fileutils'

class Dbpasswd < WEBrick::HTTPAuth::Htpasswd
  def initialize(db, loadperms=false)
    @db = db
    @passwd = Hash.new
    @perms = Hash.new
    @mtime = Time.at(0)
    reload
    if loadperms
      load_permissions
    end
  end

  def reload(update=false)
    mtimefile = @db.db_path + '/' + @db.collection + '.mtime'
    FileUtils.touch(mtimefile) if not File.exist?(mtimefile) or update
    mtime = File::mtime(mtimefile)
    if @mtime < mtime
      $stderr.puts "*reloading userdb"
      pahash = {}
      @db.get_passwd.each{|line|
        user = line['user']
        pass = line['pass']
        pahash[user] = pass
      }
      @passwd = pahash
      load_permissions
      @mtime = mtime
    end
  end

  def load_permissions
    pehash = @db.get_perms
    @perms = pehash
  end

  def check_permissions(user, permission, value)
    #$stderr.puts 'perms:' + @perms.to_s
    if @perms[user][permission] == value #or @perms[user][permission] == 'w'
      return true
    else
      return false
    end
  end

  def [](user)
    return @passwd[user]
  end

  def get_perms
    return @perms
  end
end
