#########################################################
##
## univerzalni kod pro inicializaci HTTP serveru
##
##

require 'webrick'

class AuthHTTPServer < WEBrick::HTTPServer
  include WEBrick
  attr_accessor :dbenv
  attr_accessor :info
  
  def initialize(config={}, default=WEBrick::Config::HTTP)
    super
    @authenticator = config[:Authenticator]
  end

  def service(req, res)
    @authenticator.userdb.reload
    user = @authenticator.authenticate(req,res)
    req.attributes[:auth_user] = user
    Thread.current[:auth_user] = user
    super
  end

  def access_log(config, req, res)
    param = AccessLog::setup_params(config, req, res)
    if req.path != '/hello'
      @config[:AccessLog].each{|logger, fmt|
        logger << AccessLog::format(fmt+"\n", param)
      }
    end
  end

end

class DebHTTPServer < WEBrick::HTTPServer
  include WEBrick
  attr_accessor :dbenv
  attr_accessor :info
  
  def initialize(config={}, default=WEBrick::Config::HTTP)
    super
  end

  def access_log(config, req, res)
    param = AccessLog::setup_params(config, req, res)
    if req.path != '/hello'
      @config[:AccessLog].each{|logger, fmt|
        logger << AccessLog::format(fmt+"\n", param)
      }
    end
  end
end

def create_http_server( args2 = {} )
  a = {
    :auth       => nil,
    :htpasswd   => nil,
    :realm      => "deb2 server",
    :ssl        => nil,
  }.update( args2 )

  authenticator = nil
  
  if a[:auth] or a[:htpasswd]
    require 'base64'
    auth = WEBrick::HTTPAuth::BasicAuth
    auth = TediImapAuth if a[:tedi_imap]
  
    userdb = nil
    if a[:htpasswd]
      userdb = WEBrick::HTTPAuth::Htpasswd.new(a[:htpasswd])
    elsif a[:dbpasswd]
      userdb = Dbpasswd.new(a[:dbpasswd], a[:loadperms])
    else
      userdb = Hash.new
      userdb.extend(WEBrick::HTTPAuth::UserDB)
      userdb.set_passwd(a[:realm], "test", "test")
      userdb.auth_type = auth
    end
  
    authenticator = auth.new({
      :Realm            => a[:realm],
      :UserDB           => userdb,
      :NonceExpirePeriod=>60,
      :NonceExpireDelta =>5
    }) 
  
    serverclass = AuthHTTPServer
  else
    #serverclass = WEBrick::HTTPServer
    serverclass = DebHTTPServer
  end

  #override mimetypes
  system_mime_table = WEBrick::HTTPUtils::load_mime_types('/etc/mime.types')
  my_mime_table = system_mime_table.update(
      { "xpi" => "application/x-xpinstall" })

  server = nil
  if a[:ssl]
    require 'webrick/https'
    require 'openssl'

    pkey = cert = cert_name = nil
    begin
      data = File.open( (a[:ssl_cert_base]+".key") ).read(nil)
      pkey = OpenSSL::PKey::RSA.new(data)
      data = File.open( (a[:ssl_cert_base]+".crt") ).read(nil)
      cert = OpenSSL::X509::Certificate.new( data )
    rescue
      $stderr.puts "Switching to use self-signed certificate"
      cert_name = [ ["C","CZ"], ["O",a[:ssl_org]||"fi.muni.cz"], ["CN", a[:ssl_host]||"nlp"] ]
    end

    server = serverclass.new( 
      :Port            => a[:port],
      :SSLEnable       => true,
      :SSLOptions => OpenSSL::SSL::OP_NO_SSLv3,
      :SSLVerifyClient => ::OpenSSL::SSL::VERIFY_NONE,
      :SSLCertificate  => cert,
      :SSLPrivateKey   => pkey,
      :SSLCACertificateFile => a[:ssl_ca_cert],
      :Authenticator	      => authenticator,
      :MimeTypes      => my_mime_table,
      :Logger               => a[:logger],
      :AccessLog => [[a[:access_log], WEBrick::AccessLog::COMBINED_LOG_FORMAT],
                    [a[:logger],WEBrick::AccessLog::COMBINED_LOG_FORMAT ]]
    )
  else
    server = serverclass.new( 
      :Port 		            => a[:port],
      :Authenticator	      => authenticator,
      :MimeTypes      => my_mime_table,
      :Logger               => a[:logger],
      :AccessLog => [[a[:access_log], WEBrick::AccessLog::COMBINED_LOG_FORMAT],
                    [a[:logger],WEBrick::AccessLog::COMBINED_LOG_FORMAT ]]
    )
  end

  return server
end

