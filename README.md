# DEB Dictionary Editor and Browser

## Installation
* Install Ruby and following packages:
  * rubygems
  * libxml2-devel
  * libxslt-devel

* Install Sedna DB: from http://www.sedna.org/download.html get
      sedna-3.5.161-bin-linux-x64.sh (Run the application, and when asked for install directory, select "/opt")

* Once you have ruby and rubygems installed, install following packages:
      (with command: gem install _package_)
  * json
  * libxml-ruby
  * sedna
  * libxslt-ruby

* Download the repository and copy contents to /var/lib/deb-server/
* run the commands in attached debinstall.sh
* create ssl certificate for your domain and put it into
      /var/lib/deb-server/certs (host.crt for certificate and host.key for private key)

## Running

Run command: "/etc/init.d/debserver-common start admin_services"
to start admin interface at https://server:8000 (default login deb, password deb)

To start debvisdic: /etc/init.d/debserver-common start debvisdic2

