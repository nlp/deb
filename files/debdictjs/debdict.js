
var url_base = 'https://abulafia.fi.muni.cz:8005';
var searched_dict = '';

function init() {
  dojo.connect(dijit.byId('search_query'), "onKeyPress", function(evt) {
      if (evt.keyCode === dojo.keys.ENTER) run_search();
    }
  );

  dojo.connect(dijit.byId('selectEntry'), "onClick", function(evt) {
      show_entry(dojo.byId('selectEntry').value, searched_dict, 'cs');
    }
  );



  var url = url_base + "/hello?action=init";

  var xhrArgs = {
     url: url,
     handleAs: "text",
     load: function(data) {
       var dictAr = data.split(';');
       hide_dicts(dictAr)
     },
       error: function(error) {
         alert(error);
       }
  }
  var deferred = dojo.xhrGet(xhrArgs);
}


function selected_dict() {
  return dojo.byId('mainform').elements['dict_list'].value;
}

function show_entry(query, dict, lang) {
  url = url_base + '/' + dict + '?action=getdoc&tr='+dict+'&id=' + encodeURIComponent(query) +'&lang='+lang;
  //url = 'proxy.php?dict=' + dict + '&action=getdoc&tr='+dict+'&id=' + encodeURIComponent(query) +'&lang='+lang;
  dojo.byId('dataIframe').setAttribute('src', '');
  dojo.byId('dataIframe').setAttribute('src', url );

}

function clear_list(element) {
  if (element.hasChildNodes()) {
    while (element.childNodes.length >= 1) {
      element.removeChild(element.firstChild);
    }
  }

}

function get_list(query, dict, lang, searchall) {
  var url = url_base + "/" + dict + "?action=list_starts_with&id=" + encodeURIComponent(query) + "&lang=" + lang;
  //var url = "proxy.php?dict=" + dict + "&action=list_starts_with&id=" + encodeURIComponent(query) + "&lang=" + lang;
  if (searchall) url += "&all=1";

  var xhrArgs = {
     url: url,
     handleAs: "text",
     load: function(data) {
       var selectList = dojo.byId('selectEntry');
       if (data == '') {
         dojo.byId('dataIframe').setAttribute('src', '');
         clear_list(selectList);
       } else {
         clear_list(selectList);
         var a = data.split(/\r?\n/);
         for (var i = 0; i < a.length; i++) {
           if (a[i] != '') {
             if (a[i].indexOf('|') > -1) {
               var aa = a[i].split('|');
               label = aa[1];
               value = aa[0];
             } else {
               label = a[i];
               value = a[i];
             }
             selectList.appendChild(new Option(label,value));
           }
         }
         selectList.firstChild.selected=true;
         show_entry(selectList.firstChild.value, dict, 'cs');
       }
     },
       error: function(error) {
         alert(error);
       }
  }

  var deferred = dojo.xhrGet(xhrArgs);
}

function run_search() {
  if (dojo.byId('search_query').value != '') {
    dojo.byId('statustext').innerHTML = 'Waiting';
    searched_dict = selected_dict();
    clear_list(dojo.byId('selectEntry'));
    /*var dictionaries = ["ssc", "scs", "ssjc", "psjc", "syno", "scfis", "scfin", "diderot", "cia", "cod11", "ode", "ote", "slvdict", "crd", "wncz"];
      if (searched_dict == 'map') {
      alert('mapa')
      } else if (dojo.indexOf(dictionaries, searched_dict) > -1) {
      get_list(dojo.byId('search_query').value, selected_dict(),'cs');
      dojo.byId('statustext').innerHTML = 'Done';
      } else if (searched_dict == 'gslov') {
      url = 'proxy.php?dict=gslov&tr=&key=' + encodeURIComponent(dojo.byId('search_query').value);
      dojo.byId('dataIframe').setAttribute('src', '');
      dojo.byId('dataIframe').setAttribute('src', url );
      dojo.byId('statustext').innerHTML = 'Done';
      } else {
      alert('o')
      }*/

    if (searched_dict == 'gslov') {
      url = url_base + '/gslov?lang=cs&tr=&key=' + encodeURIComponent(dojo.byId('search_query').value);
      dojo.byId('dataIframe').setAttribute('src', '');
      dojo.byId('dataIframe').setAttribute('src', url );
      dojo.byId('statustext').innerHTML = 'Done';
    } else {
      get_list(dojo.byId('search_query').value, selected_dict(),'cs');
      dojo.byId('statustext').innerHTML = 'Done';
    }
  }
}

function hide_dicts(dictAr) {
  var opts = dijit.byId('dict_list').getOptions();
  for (var i = 0; i < opts.length; i++) {
    if (dojo.indexOf(dictAr, opts[i].value) == -1) {
      dijit.byId("dict_list").removeOption(opts[i].value);
    }
  }
}
