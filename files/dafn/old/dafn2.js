/* verbalex ext js */

Ext.require([
    'Ext.form.*',
]);

var status_store = Ext.create('Ext.data.Store', {
  fields: ['status', 'label'],
  data: [
    {'status':'a', 'label':'Active'},
    {'status':'s', 'label':'Suppressed'},
    {'status':'d', 'label':'Deleted'},
  ]
});
var origin_store = Ext.create('Ext.data.Store', {
  fields: ['origin', 'label'],
  data: [
  {'origin':'generic', 'label':'generic'},
  {'origin':'Chinese', 'label':'Chinese'},
  {'origin':'Czech', 'label':'Czech'},
  {'origin':'CzechSlovak', 'label':'CzechSlovak'},
  {'origin':'Dutch', 'label':'Dutch'},
  {'origin':'French', 'label':'French'},
  {'origin':'German', 'label':'German'},
  {'origin':'Hispanic', 'label':'Hispanic'},
  {'origin':'Hungarian', 'label':'Hungarian'},
  {'origin':'Indian', 'label':'Indian'},
  {'origin':'Irish', 'label':'Irish'},
  {'origin':'Italian', 'label':'Italian'},
  {'origin':'Japanese', 'label':'Japanese'},
  {'origin':'Korean', 'label':'Korean'},
  {'origin':'LatvianLithuanian', 'label':'LatvianLithuanian'},
  {'origin':'Muslim', 'label':'Muslim'},
  {'origin':'Norwegian', 'label':'Norwegian'},
  {'origin':'OtherEastAsian', 'label':'OtherEastAsian'},
  {'origin':'Polish', 'label':'Polish'},
  {'origin':'Scandinavian', 'label':'Scandinavian'},
  {'origin':'Scottish', 'label':'Scottish'},
  {'origin':'Swedish', 'label':'Swedish'},
  {'origin':'Ukranian', 'label':'Ukranian'},
  {'origin':'Vietnamese', 'label':'Vietnamese'},
  ]
});

function count_elements(search) {
  var count = 0;
  Ext.ComponentMgr.all.each(function (item) {
    if (item.substr(0,search.length) == search) {
      count++;
    }
  });

  return count;
}

function creat_hist() {
  var name = 'hist_'+Ext.id();

  var histset = Ext.create('Ext.form.FieldSet', {
    border: false,
    xtype: 'fieldset',
    id: name,
    class: 'hitsset',
    name: 'histset',
    layout: {
      type: 'hbox'
    },
    items: [{
        xtype: 'textarea',
        name: 'history',
            grow: 'true',
        width: 600,
        rows: 2,
        value: 'The Italian surname Adorno is also established in southern Germany through descendants of an Italian merchant who settled in Swabia in 1680, and in Brazil, where it was taken from Genoa in the 16th century.'
    },{
      xtype: 'textfield',
              fieldLabel: 'annot.',
              name: 'annot',
              value: 'EC, DK'
    },{
      xtype: 'splitter'
    },{
      xtype: 'button',
      text: '+ history',
      handler: function() {
        var histset = create_hist();
        Ext.getCmp('histcontainer').add(histset);
      }
    },{
      xtype: 'splitter'
    },{
      xtype: 'button',
      text: '- history',
      handler: function() {
        if (confirm('Delete?')) {
          Ext.getCmp(name).destroy();
        }
      }
    }]
  });

  return histset;
}

function create_sense1() {
  var name = 'sense_'+Ext.id();

  var senseset = Ext.create('Ext.form.FieldSet', {
    border: false,
    xtype: 'fieldset',
    id: name,
    class: 'senseset',
    name: 'senseset',
    layout: {
      type: 'hbox'
    },
    items: [{
        xtype: 'textarea',
        name: 'sense_def',
            grow: 'true',
        width: 600,
        rows: 2,
        value: 'Southern Italian: from the personal name <i>Adorno</i>, meaning ‘adorned’.'
    },{
      xtype: 'container',
      layout: {
        type: 'vbox'
      },
      items: [{
              fieldLabel: 'status',
              name: 'status',
              xtype: 'combobox',
              store: status_store,
              displayField: 'label',
              valueField: 'status',
              value: 'a'
      },{
      xtype: 'textfield',
              fieldLabel: 'annot.',
              name: 'annot',
              value: 'EC, DK'
      }]
    },{
      xtype: 'splitter'
    },{
      xtype: 'button',
      text: '+ sense',
      handler: function() {
        var senseset = create_sense();
        Ext.getCmp('sensecontainer').add(senseset);
      }
    },{
      xtype: 'splitter'
    },{
      xtype: 'button',
      text: '- sense',
      handler: function() {
        if (count_elements('sense_') == 1) {
          alert("can't delete last sense.");
        } else if (confirm('Delete?')) {
          Ext.getCmp(name).destroy();
        }
      }
    }]
  });

  return senseset;
}
function create_sense2() {
  var name = 'sense_'+Ext.id();

  var senseset = Ext.create('Ext.form.FieldSet', {
    border: false,
    xtype: 'fieldset',
    id: name,
    class: 'senseset',
    name: 'senseset',
    layout: {
      type: 'hbox'
    },
    items: [{
        xtype: 'textarea',
        name: 'sense_def',
            grow: 'true',
        width: 600,
        rows: 2,
        value: 'Italian: from Italian <i>adorno</i> denoting a type of hawk, presumably applied as a nickname for someone with hawklike features or a metonymic occupational name for someone who trained hawks.'
    },{
      xtype: 'container',
      layout: {
        type: 'vbox'
      },
      items: [{
              fieldLabel: 'status',
              name: 'status',
              xtype: 'combobox',
              store: status_store,
              displayField: 'label',
              valueField: 'status',
              value: 'a'
      },{
      xtype: 'textfield',
              fieldLabel: 'annot.',
              name: 'annot',
      }]
    },{
      xtype: 'splitter'
    },{
      xtype: 'button',
      text: '+ sense',
      handler: function() {
        var senseset = create_sense();
        Ext.getCmp('sensecontainer').add(senseset);
      }
    },{
      xtype: 'splitter'
    },{
      xtype: 'button',
      text: '- sense',
      handler: function() {
        if (count_elements('sense_') == 1) {
          alert("can't delete last sense.");
        } else if (confirm('Delete?')) {
          Ext.getCmp(name).destroy();
        }
      }
    }]
  });

  return senseset;
}
function create_sense() {
  var name = 'sense_'+Ext.id();

  var senseset = Ext.create('Ext.form.FieldSet', {
    border: false,
    xtype: 'fieldset',
    id: name,
    class: 'senseset',
    name: 'senseset',
    layout: {
      type: 'hbox'
    },
    items: [{
        xtype: 'textarea',
        name: 'sense_def',
            grow: 'true',
        width: 600,
        rows: 2
    },{
      xtype: 'container',
      layout: {
        type: 'vbox'
      },
      items: [{
              fieldLabel: 'status',
              name: 'status',
              xtype: 'combobox',
              store: status_store,
              displayField: 'label',
              valueField: 'status',
      },{
      xtype: 'textfield',
              fieldLabel: 'annot.',
              name: 'annot',
      }]
    },{
      xtype: 'splitter'
    },{
      xtype: 'button',
      text: '+ sense',
      handler: function() {
        var senseset = create_sense();
        Ext.getCmp('sensecontainer').add(senseset);
      }
    },{
      xtype: 'splitter'
    },{
      xtype: 'button',
      text: '- sense',
      handler: function() {
        if (count_elements('sense_') == 1) {
          alert("can't delete last sense.");
        } else if (confirm('Delete?')) {
          Ext.getCmp(name).destroy();
        }
      }
    }]
  });

  return senseset;
}

function create_foreanot() {
  var name = 'foreanot_'+Ext.id();
  var container = 'foreanot_cont_'+Ext.id();
  var forename1 = create_forename(container);

  var senseset = Ext.create('Ext.form.FieldSet', {
    border: false,
    xtype: 'fieldset',
    id: name,
    class: 'senseset',
    name: 'senseset',
    layout: {
      type: 'hbox'
    },
    items: [{
      xtype: 'textfield',
              fieldLabel: 'annot.',
              name: 'annot',
    },{
      xtype: 'container',
      id: container,
      items: [forename1]
    },{
      xtype: 'splitter'
    },{
      xtype: 'button',
      text: '+ forename set',
      handler: function() {
        var senseset = create_foreanot();
        Ext.getCmp('forecont').add(senseset);
      }
    },{
      xtype: 'splitter'
    },{
      xtype: 'button',
      text: '- forename set',
      handler: function() {
        if (confirm('Delete?')) {
          Ext.getCmp(name).destroy();
        }
      }
    }]
  });

  return senseset;
}
function create_forename(container, value, count) {
  var name = 'forename_'+Ext.id();

  var senseset = Ext.create('Ext.form.FieldSet', {
    border: false,
    xtype: 'fieldset',
    id: name,
    class: 'senseset',
    name: 'senseset',
    layout: {
      type: 'hbox'
    },
    items: [{
      xtype: 'textfield',
              name: 'forename',
              value: value
    },{
      xtype: 'textfield',
              name: 'count',
              value: count
    },{
      xtype: 'splitter'
    },{
      xtype: 'button',
      text: '+',
      handler: function() {
        var senseset = create_forename();
        Ext.getCmp(container).add(senseset);
      }
    },{
      xtype: 'splitter'
    },{
      xtype: 'button',
      text: '-',
      handler: function() {
        if (confirm('Delete?')) {
          Ext.getCmp(name).destroy();
        }
      }
    }]
  });

  return senseset;
}



function create_foreanot1() {
  var name = 'foreanot_'+Ext.id();
  var container = 'foreanot_cont_'+Ext.id();
  var forename1 = create_forename(container,'Jose','10');
  var forename2 = create_forename(container,'Juan','7');
  var forename3 = create_forename(container,'Luis','4');
  var forename4 = create_forename(container,'Carlos','3');
  var forename5 = create_forename(container,'Enrique','3');
  var forename6 = create_forename(container,'Miguel','3');
  var forename7 = create_forename(container,'Pedro','3');
  var forename8 = create_forename(container,'Blanca','2');
  var forename9 = create_forename(container,'Ernesto','2');
  var forename10 = create_forename(container,'Jesus','2');
  var forename11 = create_forename(container,'Ramon','2');
  var forename12 = create_forename(container,'Raul','2');


  var senseset = Ext.create('Ext.form.FieldSet', {
    border: false,
    xtype: 'fieldset',
    id: name,
    class: 'senseset',
    name: 'senseset',
    layout: {
      type: 'hbox'
    },
    items: [{
      xtype: 'textfield',
              fieldLabel: 'annot.',
              name: 'annot',
              value: 'spa:'
    },{
      xtype: 'container',
      id: container,
      items: [forename1, forename2, forename3, forename4, forename5, forename6, forename7, forename8, forename9, forename10, forename11, forename12]
    },{
      xtype: 'splitter'
    },{
      xtype: 'button',
      text: '+ forename set',
      handler: function() {
        var senseset = create_foreanot();
        Ext.getCmp('forecont').add(senseset);
      }
    },{
      xtype: 'splitter'
    },{
      xtype: 'button',
      text: '- forename set',
      handler: function() {
        if (confirm('Delete?')) {
          Ext.getCmp(name).destroy();
        }
      }
    }]
  });

  return senseset;
}
function create_foreanot2() {
  var name = 'foreanot_'+Ext.id();
  var container = 'foreanot_cont_'+Ext.id();
  var forename1 = create_forename(container,'Angelo','2');
  var forename2 = create_forename(container,'Antonio','2');
  var forename3 = create_forename(container,'Carmelo','2');
  var forename4 = create_forename(container,'Sal','2');
  var forename5 = create_forename(container,'Eliseo','');
  var forename6 = create_forename(container,'Giovanni','');
  var forename7 = create_forename(container,'Giuseppe','');
  var forename8 = create_forename(container,'Heriberto','');
  var forename9 = create_forename(container,'Leonardo','');
  var forename10 = create_forename(container,'Luigi','');
  var forename11 = create_forename(container,'Sebastiano','');


  var senseset = Ext.create('Ext.form.FieldSet', {
    border: false,
    xtype: 'fieldset',
    id: name,
    class: 'senseset',
    name: 'senseset',
    layout: {
      type: 'hbox'
    },
    items: [{
      xtype: 'textfield',
              fieldLabel: 'annot.',
              name: 'annot',
              value: 'ita:'
    },{
      xtype: 'container',
      id: container,
      items: [forename1,forename2,forename3,forename4,forename5,forename6,forename7,forename8,forename9,forename10,forename11]
    },{
      xtype: 'splitter'
    },{
      xtype: 'button',
      text: '+ forename set',
      handler: function() {
        var senseset = create_foreanot();
        Ext.getCmp('forecont').add(senseset);
      }
    },{
      xtype: 'splitter'
    },{
      xtype: 'button',
      text: '- forename set',
      handler: function() {
        if (confirm('Delete?')) {
          Ext.getCmp(name).destroy();
        }
      }
    }]
  });

  return senseset;
}

Ext.onReady(function(){

    //var entryid = '5';
    var entryid;

    var sense1 = create_sense1();
    var sense2 = create_sense2();
    var hist = creat_hist();
    var foreanot = create_foreanot1();
    var foreanot2 = create_foreanot2();

    var verbform = Ext.create('Ext.form.Panel', {
        url:'/dafn',
        frame: true,
        title: 'Surname Adorno',
        bodyStyle:'padding:5px 5px',
        /*width: 350,*/
        fieldDefaults: {
          msgTarget: 'side',
          labelWidth: 50
        },
        id: 'dafnform',
        defaultType: 'textfield',
        defaults: {
          /*anchor: '100%'*/
        },

        items: [{
          xtype: 'fieldset',
          title: 'Heading',
          layout: 'anchor',
          items: [{
            xtype: 'container',
            id: 'lemmaset',
            frame: 'true',
            defaultType: 'textfield',
            layout: {
              type: 'hbox'
            },
            items: [{
              fieldLabel: 'name',
              name: 'name',
              //allowBlank: false,
              //grow: true,
              id: 'name',
              value: 'Adorno'
            },{
              fieldLabel: 'count',
              name: 'count',
              value: '406'
            },{
              fieldLabel: 'status',
              name: 'status',
              xtype: 'combobox',
              store: status_store,
              displayField: 'label',
              valueField: 'status',
              editable: false,
              value: 'a'
            },{
              fieldLabel: 'origin',
              name: 'origin',
              xtype: 'combobox',
              store: origin_store,
              displayField: 'label',
              valueField: 'origin',
              editable: false,
              value: 'generic'
            }]
          }]
        },{
          xtype: 'fieldset',
          title: 'Sense',
          layout: 'anchor',
          id: 'sensefield',
          items: [{
            xtype: 'container',
            id: 'sensecontainer',
            items: [sense1,sense2]
          }]
        },{
          xtype: 'fieldset',
          title: 'History',
          id: 'histcontainer',
          items: [hist]
        },{
          xtype: 'fieldset',
          title: 'Forenames',
          id: 'forenames',
          items: [{
            xtype: 'container',
            id: 'foreset',
            frame: 'true',
            defaultType: 'textfield',
            layout: {
              type: 'hbox'
            },
            items: [{
              fieldLabel: 'reg. stat.',
              name: 'regstat',
              value: 'Spanish 37%; Italian 8%.'
            },{
              xtype: 'fieldset',
              id: 'forecont',
              items:[foreanot,foreanot2]
            }] 
          }]
        }],
  
        buttons: [{
          text: 'Save',
          formBind: true,
          handler: function() {
            /* save data */
            var form = this.up('form').getForm();
            var data = {"verb":{
              "lemmas": {"lemma":[]},
              "properties": {},
              "frames": {"frame":[]}
            }};


            //alert(JSON.stringify(data))
            /*if (form.isValid()) {
              form.submit({
                params: {
                  data: JSON.stringify(data),
                  action: 'save',
                  id: entryid,
                },
                waitMsg: 'Ukládám',
                success: function(form, action) {
                  Ext.Msg.alert('Úspěch', action.result.msg);
                }
              });
            }*/
          },
        },{
          text: 'Close',
          handler: function() {
            if (confirm('Opravdu zavřít?')) {
              window.close();
            }
          }
        }],
    });

    verbform.render(document.body);

    var params = Ext.Object.fromQueryString(window.location.search.substring(1));

    /* prefill lemma */
    if (params.id == null && params.lemma != null) {
      var lemmas = Ext.getCmp('verbform').query('component[class="lemmaset"]');
      lemmas[0].query('component[name="lemma"]')[0].setValue(params.lemma);
    }

});
