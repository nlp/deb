/* verbalex ext js */

Ext.require([
    'Ext.form.*',
]);

var grams = Ext.create('Ext.data.Store', {
  fields: ['gram', 'label'],
  data: [
    {'gram':'', 'label':'-'},
    {'gram':'m', 'label':'m'},
    {'gram':'f', 'label':'f'},
    {'gram':'n', 'label':'n'},
    {'gram':'v', 'label':'v'},
    {'gram':'adj', 'label':'adj'},
  ]
});
var langs = Ext.create('Ext.data.Store', {
  fields: ['lang', 'label'],
  data: [
    {'lang':'am', 'label':'AmE'},
    {'lang':'br', 'label':'BrE'},
    {'lang':'cze', 'label':'Cze'},
    {'lang':'un', 'label':'UN'},
    {'lang':'eng', 'label':'Eng'},
    {'lang':'archaic', 'label':'archaic'},
  ]
});
var sems = Ext.create('Ext.data.Store', {
  fields: ['sem', 'label'],
  data: [
    {'sem':'hyper', 'label':'HYPER'},
    {'sem':'hypo', 'label':'HYPO'},
    {'sem':'syn', 'label':'SYN'},
    {'sem':'ant', 'label':'ANT'},
  ]
});
var classes = Ext.create('Ext.data.Store', {
  fields: ['class', 'label'],
  data: [
    {'class': 'zakladni', 'label':'základní pojmy'},
    {'class': 'nehmotna', 'label':'nehmotná kultura'},
    {'class': 'hmotna', 'label':'hmotná kultura'},
    {'class': 'obycejova', 'label':'obyčejová tradice'},
    {'class': 'zemedelstvi', 'label':'zemědělství a obživa'},
  ]
});

var klas_hash = {
  'vytvar':'Výtvarná kultura',
  'odev':'Oděv a textil',
  'obchod':'Obchod',
  'doprava':'Doprava a komunikace',
  'stavit':'Stavitelství',
  'rukod':'Rukodělná výroba',
  'zemed':'Zemědělství a obživa',
  'zakladni':'Základní pojmy',
  'pramen':'Pramenné studium, výzkum a publikace',
  'zak_pojmy':'Základní pojmy z etnologie a antropologie',
  'geo_nazvy':'Geografické názvy',
  'nazvy_inst':'Názvy odborných institucí',
  'nehmotna':'Nehmotná kultura',
  'vyrocni_ob':'Výroční obřady a obyčeje',
  'ostatni_ob':'Ostatní obřady a obyčeje',
  'rodinne_ob':'Rodinné obřady a obyčeje',
  'obci':'Vztahy v obci',
  'rod_vztah':'Rodinné vztahy',
  'magie':'Náboženství, magie a léčitelství',
  'folklor':'Folklor',
  'f_slovesny':'Slovesný folklor',
  'f_hudebni':'Hudební folklor',
  'f_tanecni':'Taneční folklor',
  'lid_divadlo':'Dramatické projevy',
  'hmotna':'Hmotná kultura'
};


function save_doc(id) {
  var data = {"entry":{
    "head":{
      "lemma":{},
        "gram":{},
        "klas":{}
    },
      "trans":[],
      "vars":[],
      "ency":{
        "example":{},
        "sense":[]
      },
      "semlink":[],
      "kolok":[],
      "refer":[],
      "deriv":[],
      "cite":{}
  }};

  /* zahlavi */
  data.entry.head.lemma = {'$': Ext.getCmp('lemma').getValue()};
  data.entry.head.gram = {'$': Ext.getCmp('gram').getValue()};
  data.entry.head.klas = new Array();
  if (Ext.getCmp('publish').getValue() != '') {
    data.entry['@publish'] = 'true';
  }
  if (Ext.getCmp('klasif1').getValue() != '') {
    data.entry.head.klas.push({'$': Ext.getCmp('klasif1').getValue()});
  }
  if (Ext.getCmp('klasif2').getValue() != '') {
    data.entry.head.klas.push({'$': Ext.getCmp('klasif2').getValue()});
  }
  if (Ext.getCmp('klasif3').getValue() != '') {
    data.entry.head.klas.push({'$': Ext.getCmp('klasif3').getValue()});
  }
  if (id == '' || id == undefined) {
    id = Ext.getCmp('lemma').getValue()+'-'+Ext.getCmp('gram').getValue();
  }

  /* varianty */
  var varar = Ext.getCmp('varcontainer').query('component[class="varset"]');
  for (var i = 0; i < varar.length; i++) {
    if (varar[i].query('component[name="var"]')[0].getValue() != '') {
      data.entry.vars.push({'$': varar[i].query('component[name="var"]')[0].getValue()});
    }
  }

  /* preklady */
  var transar = Ext.getCmp('transcontainer').query('component[class="transset"]');
  for (var i = 0; i < transar.length; i++) {
    if (transar[i].query('component[name="translation"]')[0].getValue() != '') {
      data.entry.trans.push({'$': transar[i].query('component[name="translation"]')[0].getValue(), '@lang': transar[i].query('component[name="lang"]')[0].getValue()});
    }
  }

  /* enc. inf. vyznamy */
  if (Ext.getCmp('example').getValue() != '') {
    data.entry.ency.example = {'$': Ext.getCmp('example').getValue()};
  }
  var encar = Ext.getCmp('ednaform').query('component[class="encyset"]');
  for (var i = 0; i < encar.length; i++) {
    //if (encar[i].query('component[name="ency_def"]')[0].getValue() != '') {
      var newenc = {
        "def": {"$": encar[i].query('component[name="ency_def"]')[0].getValue(), '@num': encar[i].query('component[name="ency_nr"]')[0].getValue()},
        "example": {"$": encar[i].query('component[name="ency_ex"]')[0].getValue()},
        "cite": {"$": encar[i].query('component[name="cite"]')[0].getValue()},
        "semlink": [],
        "trans": [],
        "kolok": [],
        "refer": []
      }
      if (encar[i].query('component[name="ency_cz"]')[0].getValue()) {
        newenc.def['@cz'] = 'true';
      }
      if (encar[i].query('component[name="ency_eng"]')[0].getValue()) {
        newenc.def['@eng'] = 'true';
      }
      if (encar[i].query('component[name="ency_gb"]')[0].getValue()) {
        newenc.def['@gb'] = 'true';
      }
      if (encar[i].query('component[name="ency_us"]')[0].getValue()) {
        newenc.def['@us'] = 'true';
      }
      if (encar[i].query('component[name="ency_un"]')[0].getValue()) {
        newenc.def['@un'] = 'true';
      }
      /* vyznam semantika */
      var semar = encar[i].query('component[class="semset"]');
      for (var j = 0; j < semar.length; j++) {
        if (semar[j].query('component[name="semlink"]')[0].getValue() != '') {
          newenc.semlink.push({'$': semar[j].query('component[name="semlink"]')[0].getValue(), '@type': semar[j].query('component[name="semtype"]')[0].getValue()});
        }
      }
      /* vyznam preklady */
      var transar = encar[i].query('component[class="transset"]');
      for (var j = 0; j < transar.length; j++) {
        if (transar[j].query('component[name="translation"]')[0].getValue() != '') {
          newenc.trans.push({'$': transar[j].query('component[name="translation"]')[0].getValue(), '@lang': transar[j].query('component[name="lang"]')[0].getValue()});
        }
      }
      /* vyznam kolokace */
      var kolar = encar[i].query('component[class="kolset"]');
      for (var j = 0; j < kolar.length; j++) {
        if (kolar[j].query('component[name="kol"]')[0].getValue() != '') {
          var newkol = {
            "kolok": {'$': kolar[j].query('component[name="kol"]')[0].getValue()},
            "trans": {'$': kolar[j].query('component[name="koltran"]')[0].getValue()},
            "ref": {'$': kolar[j].query('component[name="kolref"]')[0].getValue()}
          };
          newenc.kolok.push(newkol);
        }
      }
      /* vyznam odkazy */
      var refar = encar[i].query('component[class="refset"]');
      for (var j = 0; j < refar.length; j++) {
        if (refar[j].query('component[name="refer"]')[0].getValue() != '') {
          newenc.refer.push({'$': refar[j].query('component[name="refer"]')[0].getValue()});
        }
      }

      data.entry.ency.sense.push(newenc);
    //}
  }

  /* semanticke */
  var semar = Ext.getCmp('semcontainer').query('component[class="semset"]');
  for (var i = 0; i < semar.length; i++) {
    if (semar[i].query('component[name="semlink"]')[0].getValue() != '') {
      data.entry.semlink.push({'$': semar[i].query('component[name="semlink"]')[0].getValue(), '@type': semar[i].query('component[name="semtype"]')[0].getValue()});
    }
  }

  /* kolokace */
  var kolar = Ext.getCmp('kolcontainer').query('component[class="kolset"]');
  for (var i = 0; i < kolar.length; i++) {
    if (kolar[i].query('component[name="kol"]')[0].getValue() != '') {
      var newkol = {
        "kolok": {'$': kolar[i].query('component[name="kol"]')[0].getValue()},
        "trans": {'$': kolar[i].query('component[name="koltran"]')[0].getValue()},
        "ref": {'$': kolar[i].query('component[name="kolref"]')[0].getValue()}
      };
      data.entry.kolok.push(newkol);
    }
  }

  /* odkazy */
  var refar = Ext.getCmp('refcontainer').query('component[class="refset"]');
  for (var i = 0; i < refar.length; i++) {
    if (refar[i].query('component[name="refer"]')[0].getValue() != '') {
      data.entry.refer.push({'$': refar[i].query('component[name="refer"]')[0].getValue()});
    }
  }

  /* derivace */
  var derar = Ext.getCmp('ednaform').query('component[class="derset"]');
  for (var i = 0; i < derar.length; i++) {
    if (derar[i].query('component[name="deriv"]')[0].getValue() != '') {
      var newder = {
        "deriv": {'$': derar[i].query('component[name="deriv"]')[0].getValue()},
        "full": {'$': derar[i].query('component[name="derivfull"]')[0].getValue()}
      };
      data.entry.deriv.push(newder);
    }
  }

  /*citace*/
  if (Ext.getCmp('cite').getValue() != '') {
    data.entry.cite = {'$': Ext.getCmp('cite').getValue()};
  }


  return data;
}

function count_elements(search) {
  var count = 0;
  Ext.ComponentMgr.all.each(function (item) {
    if (item.substr(0,search.length) == search) {
      count++;
    }
  });

  return count;
}

function create_var(parent_cont) {
  var name = 'var_'+Ext.id();

  var transset = Ext.create('Ext.form.FieldSet', {
    border: false,
    xtype: 'fieldset',
    id: name,
    class: 'varset',
    name: 'varset',
    layout: {
      type: 'hbox'
    },
    items: [{
      xtype: 'textfield',
      fieldLabel: 'varianta',
      name: 'var'
    },{
      xtype: 'splitter'
    },{
      xtype: 'button',
      //text: '+',
      icon: '/editor/add.png',
      handler: function() {
        var transset = create_var(parent_cont);
        parent_cont.add(transset);
      }
    },{
      xtype: 'splitter'
    },{
      xtype: 'button',
      //text: '-',
      icon: '/editor/delete.png',
      handler: function() {
        if (confirm('smazat?')) {
          Ext.getCmp(name).destroy();
        }
      }
    }]
  });

  return transset;
}

function create_trans(parent_cont) {
  var name = 'trans_'+Ext.id();

  var transset = Ext.create('Ext.form.FieldSet', {
    border: false,
    xtype: 'fieldset',
    id: name,
    class: 'transset',
    name: 'transset',
    layout: {
      type: 'hbox'
    },
    items: [{
      xtype: 'textfield',
      fieldLabel: 'překlad',
      name: 'translation'
    },{
      xtype: 'combobox',
      name: 'lang',
      store: langs,
      displayField: 'label',
      valueField: 'lang',
      editable: false
    },{
      xtype: 'splitter'
    },{
      xtype: 'button',
      //text: '+ překlad',
      icon: '/editor/add.png',
      handler: function() {
        var transset = create_trans(parent_cont);
        //Ext.getCmp('transcontainer').add(transset);
        parent_cont.add(transset);
      }
    },{
      xtype: 'splitter'
    },{
      xtype: 'button',
      //text: '- překlad',
      icon: '/editor/delete.png',
      handler: function() {
        if (count_elements('trans_') == 1) {
          alert('Nelze smazat poslední překlad.');
        } else if (confirm('smazat?')) {
          Ext.getCmp(name).destroy();
        }
      }
    }]
  });

  return transset;
}

function create_sem(parent_cont) {
  var name = 'sem_'+Ext.id();

  var semset = Ext.create('Ext.form.FieldSet', {
    border: false,
    xtype: 'fieldset',
    id: name,
    class: 'semset',
    name: 'semset',
    layout: {
      type: 'hbox'
    },
    items: [{
      xtype: 'textfield',
      name: 'semlink',
      fieldLabel: 'sémantika:'
    },{
      xtype: 'combobox',
      name: 'semtype',
      store: sems,
      displayField: 'label',
      valueField: 'sem',
      editable: false
    },{
      xtype: 'splitter'
    },{
      xtype: 'button',
      //text: '+ sem. info',
      icon: '/editor/add.png',
      handler: function() {
        var semset = create_sem(parent_cont);
        //Ext.getCmp('semcontainer').add(semset);
        parent_cont.add(semset);
      }
    },{
      xtype: 'splitter'
    },{
      xtype: 'button',
      //text: '- sem. info',
      icon: '/editor/delete.png',
      handler: function() {
        if (confirm('smazat?')) {
          Ext.getCmp(name).destroy();
        }
      }
    }]
  });

  return semset;
}

function create_der() {
  var name = 'der_'+Ext.id();

  var derset = Ext.create('Ext.form.FieldSet', {
    border: false,
    xtype: 'fieldset',
    id: name,
    class: 'derset',
    name: 'derset',
    layout: {
      type: 'hbox'
    },
    items: [{
      xtype: 'textfield',
      name: 'deriv',
      fieldLabel: 'derivace'
    },{
      xtype: 'textfield',
      name: 'derivfull',
      fieldLabel: 'český opis'
    },{
      xtype: 'splitter'
    },{
      xtype: 'button',
      //text: '+ deriv',
      icon: '/editor/add.png',
      handler: function() {
        var derset = create_der();
        Ext.getCmp('dercontainer').add(derset);
      }
    },{
      xtype: 'splitter'
    },{
      xtype: 'button',
      //text: '- deriv',
      icon: '/editor/delete.png',
      handler: function() {
        if (confirm('smazat?')) {
          Ext.getCmp(name).destroy();
        }
      }
    }]
  });

  return derset;
}

function create_ref(parent_cont) {
  var name = 'ref_'+Ext.id();

  var refset = Ext.create('Ext.form.FieldSet', {
    border: false,
    xtype: 'fieldset',
    id: name,
    class: 'refset',
    name: 'refset',
    layout: {
      type: 'hbox'
    },
    items: [{
      xtype: 'textfield',
      name: 'refer',
      fieldLabel: 'odkaz'
    },{
      xtype: 'splitter'
    },{
      xtype: 'button',
      //text: '+ odkaz',
      icon: '/editor/add.png',
      handler: function() {
        var refset = create_ref(parent_cont);
        parent_cont.add(refset);
      }
    },{
      xtype: 'splitter'
    },{
      xtype: 'button',
      //text: '- odkaz',
      icon: '/editor/delete.png',
      handler: function() {
        if (confirm('smazat?')) {
          Ext.getCmp(name).destroy();
        }
      }
    }]
  });

  return refset;
}

function create_kol(parent_cont) {
  var name = 'kol_'+Ext.id();

  var kolset = Ext.create('Ext.form.FieldSet', {
    border: false,
    xtype: 'fieldset',
    id: name,
    class: 'kolset',
    name: 'kolset',
    layout: {
      type: 'hbox'
    },
    items: [{
      xtype: 'textfield',
      name: 'kol',
      fieldLabel: 'kolokace'
    },{
      xtype: 'textfield',
      name: 'koltran',
      fieldLabel: 'překlad'
    },{
      xtype: 'textfield',
      name: 'kolref',
      fieldLabel: 'odkaz'
    },{
      xtype: 'splitter'
    },{
      xtype: 'button',
      //text: '+ kolokace',
      icon: '/editor/add.png',
      handler: function() {
        var kolset = create_kol(parent_cont);
        parent_cont.add(kolset);
      }
    },{
      xtype: 'splitter'
    },{
      xtype: 'button',
      //text: '- kolokace',
      icon: '/editor/delete.png',
      handler: function() {
        if (confirm('smazat?')) {
          Ext.getCmp(name).destroy();
        }
      }
    }]
  });

  return kolset;
}

function create_ency() {
  var name = 'ency_'+Ext.id();

  var encyset = Ext.create('Ext.form.FieldSet', {
    xtype: 'fieldset',
    id: name,
    class: 'encyset',
    name: 'encyset',
    layout: {
      type: 'hbox'
    },
    items: [{
      xtype: 'container',
      layout: {
        type: 'vbox'
      },
      items: [{
        xtype: 'textfield',
        fieldLabel: 'číslo',
        name: 'ency_nr',
        width: 100
      },{
        xtype: 'textarea',
        fieldLabel: 'definice',
        name: 'ency_def',
        width: 600,
        rows: 2
      },{
        xtype: 'textarea',
        fieldLabel: 'příklad',
        name: 'ency_ex',
        width: 600,
        rows: 2
      },{
        xtype: 'container',
        frame: true,
        name: 'transcontainer',
        items: []
      },{
        xtype: 'container',
        frame: true,
        name: 'semcontainer',
        items: []
      },{
        xtype: 'container',
        frame: true,
        name: 'kolcontainer',
        items: []
      },{
        xtype: 'container',
        frame: true,
        name: 'refcontainer',
        items: []
      },{
        fieldLabel: 'citace',
        xtype: 'textarea',
        width: 600,
        rows: 2,
        name: 'cite',
      }]
    },{
      xtype: 'checkbox',
      boxLabel: 'CZ',
      name: 'ency_cz'
    },{
      xtype: 'checkbox',
      boxLabel: 'UN',
      name: 'ency_un'
    },{
      xtype: 'checkbox',
      boxLabel: 'ENG',
      name: 'ency_eng'
    },{
      xtype: 'checkbox',
      boxLabel: 'GB',
      name: 'ency_gb'
    },{
      xtype: 'checkbox',
      boxLabel: 'US',
      name: 'ency_us'
    },{
      xtype: 'splitter'
    },{
      xtype: 'button',
      text: 'význam',
      icon: '/editor/add.png',
      handler: function() {
        var encyset = create_ency();
        Ext.getCmp('encycontainer').add(encyset);
      }
    },{
      xtype: 'splitter'
    },{
      xtype: 'button',
      text: 'význam',
      icon: '/editor/delete.png',
      handler: function() {
        if (count_elements('ency_') == 1) {
          alert('Nelze smazat poslední význam.');
        } else if (confirm('smazat?')) {
          Ext.getCmp(name).destroy();
        }
      }
    },{
      xtype: 'button',
      text: 'překlad',
      icon: '/editor/add.png',
      handler: function() {
        var sems = create_trans();
        Ext.getCmp(name).query('[name=transcontainer]')[0].add(sems);
      }
    },{
      xtype: 'button',
      text: 'sémantika',
      icon: '/editor/add.png',
      handler: function() {
        var sems = create_sem();
        Ext.getCmp(name).query('[name=semcontainer]')[0].add(sems);
      }
    },{
      xtype: 'button',
      text: 'kolokace',
      icon: '/editor/add.png',
      handler: function() {
        var sems = create_kol();
        Ext.getCmp(name).query('[name=kolcontainer]')[0].add(sems);
      }
    },{
      xtype: 'button',
      text: 'odkaz',
      icon: '/editor/add.png',
      handler: function() {
        var sems = create_ref();
        Ext.getCmp(name).query('[name=refcontainer]')[0].add(sems);
      }
    }]
  });

  var transcont = encyset.query('component[name="transcontainer"]')[0];
  var transset = create_trans(transcont);
  transcont.add(transset);
  var semcont = encyset.query('component[name="semcontainer"]')[0];
  var semset = create_sem(semcont);
  semcont.add(semset);
  var kolcont = encyset.query('component[name="kolcontainer"]')[0];
  var kolset = create_kol(kolcont);
  kolcont.add(kolset);
  var refcont = encyset.query('component[name="refcontainer"]')[0];
  var refset = create_ref(refcont);
  refcont.add(refset);

  return encyset;
}

function create_klas_button(hidden_id, button_id) {
  var button = Ext.create('Ext.button.Button', {
    text: 'klasifikace',
    xtype: 'button',
    id: button_id,
    menu: {
      xtype: 'menu',                          
      items: [{
        text: '-',
        value: '',
        handler: function() {
          Ext.getCmp(button_id).setText(this.text);
          Ext.getCmp(hidden_id).setValue('');
        },
        },{
        text: klas_hash['zakladni'],
        value: 'zakladni',
        handler: function() {
          Ext.getCmp(button_id).setText(this.text);
          Ext.getCmp(hidden_id).setValue(this.value);
        },
        menu: {
          xtype: 'menu',
          items: [{
            text: klas_hash['pramen'],
            value: 'pramen',
            handler: function() {
              Ext.getCmp(button_id).setText(this.text);
              Ext.getCmp(hidden_id).setValue(this.value);
            }
          },{
            text: klas_hash['zak_pojmy'],
            value: 'zak_pojmy',
            handler: function() {
              Ext.getCmp(button_id).setText(this.text);
              Ext.getCmp(hidden_id).setValue(this.value);
            }
          },{
            text: klas_hash['geo_nazvy'],
            value: 'geo_nazvy',
            handler: function() {
              Ext.getCmp(button_id).setText(this.text);
              Ext.getCmp(hidden_id).setValue(this.value);
            }
          },{
            text: klas_hash['nazvy_inst'],
            value: 'nazvy_inst',
            handler: function() {
              Ext.getCmp(button_id).setText(this.text);
              Ext.getCmp(hidden_id).setValue(this.value);
            }
          }]
        }
      },{
        text: klas_hash['nehmotna'],
        handler: function() {
          Ext.getCmp(button_id).setText(this.text);
          Ext.getCmp(hidden_id).setValue(this.value);
        },
        value: 'nehmotna',
        menu: {
          xtype: 'menu',
          items: [{
                text: klas_hash['vyrocni_ob'],
                value: 'vyrocni_ob',
                handler: function() {
                  Ext.getCmp(button_id).setText(this.text);
                  Ext.getCmp(hidden_id).setValue(this.value);
                }
              },{
                text: klas_hash['rodinne_ob'],
                value: 'rodinne_ob',
                handler: function() {
                  Ext.getCmp(button_id).setText(this.text);
                  Ext.getCmp(hidden_id).setValue(this.value);
                }
              },{
                text: klas_hash['ostatni_ob'],
                value: 'ostatni_ob',
                handler: function() {
                  Ext.getCmp(button_id).setText(this.text);
                  Ext.getCmp(hidden_id).setValue(this.value);
                }
              },{
              
                text: klas_hash['obci'],
                value: 'obci',
                handler: function() {
                  Ext.getCmp(button_id).setText(this.text);
                  Ext.getCmp(hidden_id).setValue(this.value);
                }
              },{
                text: klas_hash['rod_vztah'],
                value: 'rod_vztah',
                handler: function() {
                  Ext.getCmp(button_id).setText(this.text);
                  Ext.getCmp(hidden_id).setValue(this.value);
                }
              },{
                text: klas_hash['magie'],
                value: 'magie',
                handler: function() {
                  Ext.getCmp(button_id).setText(this.text);
                  Ext.getCmp(hidden_id).setValue(this.value);
                }
              },{
                text: klas_hash['f_slovesny'],
                value: 'f_slovesny',
                handler: function() {
                  Ext.getCmp(button_id).setText(this.text);
                  Ext.getCmp(hidden_id).setValue(this.value);
                }
              },{
                text: klas_hash['f_hudebni'],
                value: 'f_hudebni',
                handler: function() {
                  Ext.getCmp(button_id).setText(this.text);
                  Ext.getCmp(hidden_id).setValue(this.value);
                }
              },{
                text: klas_hash['f_tanecni'],
                value: 'f_tanecni',
                handler: function() {
                  Ext.getCmp(button_id).setText(this.text);
                  Ext.getCmp(hidden_id).setValue(this.value);
                }
              },{
                text: klas_hash['lid_divadlo'],
                value: 'lid_divadlo',
                handler: function() {
                  Ext.getCmp(button_id).setText(this.text);
                  Ext.getCmp(hidden_id).setValue(this.value);
                }
              }]
        }
      },{
        text: klas_hash['hmotna'],
        value: 'hmotna',
        handler: function() {
          Ext.getCmp(button_id).setText(this.text);
          Ext.getCmp(hidden_id).setValue(this.value);
        },
        menu: {
          xtype: 'menu',
          items: [{
            text: klas_hash['zemed'],
            value: 'zemed',
            handler: function() {
              Ext.getCmp(button_id).setText(this.text);
              Ext.getCmp(hidden_id).setValue(this.value);
            }
          },{
            text: klas_hash['rukod'],
            value: 'rukod',
            handler: function() {
              Ext.getCmp(button_id).setText(this.text);
              Ext.getCmp(hidden_id).setValue(this.value);
            }
          },{
            text: klas_hash['stavit'],
            value: 'stavit',
            handler: function() {
              Ext.getCmp(button_id).setText(this.text);
              Ext.getCmp(hidden_id).setValue(this.value);
            }
          },{
            text: klas_hash['doprava'],
            value: 'doprava',
            handler: function() {
              Ext.getCmp(button_id).setText(this.text);
              Ext.getCmp(hidden_id).setValue(this.value);
            }
          },{
            text: klas_hash['obchod'],
            value: 'obchod',
            handler: function() {
              Ext.getCmp(button_id).setText(this.text);
              Ext.getCmp(hidden_id).setValue(this.value);
            }
          },{
            text: klas_hash['odev'],
            value: 'odev',
            handler: function() {
              Ext.getCmp(button_id).setText(this.text);
              Ext.getCmp(hidden_id).setValue(this.value);
            }
          },{
            text: klas_hash['vytvar'],
            value: 'vytvar',
            handler: function() {
              Ext.getCmp(button_id).setText(this.text);
              Ext.getCmp(hidden_id).setValue(this.value);
            }
          }]
        }
      }]                          
    }
  });
  return button;
}

Ext.onReady(function(){

    //var entryid = '5';
    var entryid;

    var ency = create_ency();
    var der = create_der();
    var klasbut1 = create_klas_button('klasif1', 'klasbut1');
    var klasbut2 = create_klas_button('klasif2', 'klasbut2');
    var klasbut3 = create_klas_button('klasif3', 'klasbut3');

    var verbform = Ext.create('Ext.form.Panel', {
        url:'/edna',
        frame: true,
        title: 'Heslo X',
        bodyStyle:'padding:5px 5px',
        /*width: 350,*/
        fieldDefaults: {
          msgTarget: 'side',
          labelWidth: 70
        },
        id: 'ednaform',
        defaultType: 'textfield',
        defaults: {
          /*anchor: '100%'*/
        },

        items: [{
          xtype: 'fieldset',
          title: 'Záhlaví',
          layout: 'anchor',
          items: [{
            xtype: 'checkbox',
            id: 'publish',
            name: 'publish',
            boxLabel: 'zveřejnit'
          },{
            xtype: 'fieldset',
            id: 'lemmaset',
            frame: 'true',
            defaultType: 'textfield',
            layout: {
              type: 'hbox'
            },
            items: [{
              fieldLabel: 'lemma',
              name: 'lemma',
              allowBlank: false,
              grow: true,
              id: 'lemma',
              listeners: {
                'blur': function(f,e) {
                  if (entryid == undefined) {
                    var to_check = e.target.value;
                  }
                }
              }
            },{
              fieldLabel: 'gram. inf.',
              name: 'gram',
              id: 'gram',
              xtype: 'combobox',
              store: grams,
              displayField: 'label',
              valueField: 'gram',
              editable: false
            },{
              xtype: 'hiddenfield',
              id: 'klasif1',
              name: 'klasif1'
            },klasbut1
            ,{
              xtype: 'hiddenfield',
              id: 'klasif2',
              name: 'klasif2'
            },klasbut2
            ,{
              xtype: 'hiddenfield',
              id: 'klasif3',
              name: 'klasif3'
            },klasbut3
            ]
          },{
            xtype: 'container',
            frame: true,
            id: 'varcontainer',
            items: []
          },{
            xtype: 'container',
            frame: true,
            id: 'transcontainer',
            items: []
          }]
        },{
          xtype: 'fieldset',
          title: 'Encyklopedická informace',
          layout: 'anchor',
          id: 'encyinfcontainer',
          items: [{
            xtype: 'container',
            id: 'encycontainer',
            items: [ency]
          },{
            xtype: 'textarea',
            fieldLabel: 'Příkladová věta',
            id: 'example',
            name: 'example',
            width: 600,
            rows: 2,
          }]
        },{
          xtype: 'fieldset',
          title: 'Sémantická informace',
          id: 'semcontainer',
          items: [{
            xtype: 'button',
            text: 'sémantika',
            icon: '/editor/add.png',
            handler: function() {
              var sems = create_sem();
              Ext.getCmp('semcontainer').add(sems);
            }
          }]
        },{
          xtype: 'fieldset',
          title: 'Kolokace',
          id: 'kolcontainer',
          items: [{
            xtype: 'button',
            text: 'kolokace',
            icon: '/editor/add.png',
            handler: function() {
              var sems = create_kol();
              Ext.getCmp('kolcontainer').add(sems);
            }
          }]
        },{
          xtype: 'fieldset',
          title: 'Křížové odkazy',
          id: 'refcontainer',
          items: [{
            xtype: 'button',
            text: 'odkaz',
            icon: '/editor/add.png',
            handler: function() {
              var sems = create_ref();
              Ext.getCmp('refcontainer').add(sems);
            }
          }]
        },{
          xtype: 'fieldset',
          title: 'Derivace',
          id: 'dercontainer',
          items: [{
            xtype: 'button',
            text: 'derivace',
            icon: '/editor/add.png',
            handler: function() {
              var sems = create_der();
              Ext.getCmp('dercontainer').add(sems);
            }
          }, der]
        },{
          xtype: 'fieldset',
          title: 'Zdroje, citace',
          id: 'citecontainer',
          items: [{
            xtype: 'textarea',
            width: 600,
            rows: 2,
            name: 'cite',
            id: 'cite'
          }]
        }],
  
        buttons: [{
          text: 'Náhled',
          handler: function() {
            window.open('/edna?action=preview&id='+encodeURIComponent(encodeURIComponent(entryid)), '_blank');
          }
        },{
          text: 'test',
          handler: function() {
            alert(JSON.stringify(save_doc(entryid)));
          }
        },{
          text: 'Uložit',
          formBind: true,
          handler: function() {
            /* save data */
            var form = this.up('form').getForm();
            var data = save_doc(entryid);
            if (form.isValid()) {
              form.submit({
                params: {
                  data: JSON.stringify(data),
                  action: 'save',
                  id: entryid,
                },
                waitMsg: 'Ukládám',
                success: function(form, action) {
                  Ext.Msg.alert('Úspěch', action.result.msg);
                  Ext.Msg.hide();
                  if (params.id == undefined) {
                    window.location = '/editor/?id='+encodeURIComponent(action.result.id);
                  }
                }
              });
            }
          },
        },{
          text: 'Zavřít',
          handler: function() {
            if (confirm('Opravdu zavřít?')) {
              window.close();
            }
          }
        },{
          text: 'Uložit a zavřít',
          formBind: true,
          handler: function() {
            /* save data */
            var form = this.up('form').getForm();
            var data = save_doc(entryid);
            if (form.isValid()) {
              form.submit({
                params: {
                  data: JSON.stringify(data),
                  action: 'save',
                  id: entryid,
                },
                waitMsg: 'Ukládám',
                success: function(form, action) {
                  Ext.Msg.alert('Úspěch', action.result.msg);
                  Ext.Msg.hide();
                  window.close();
                }
              });
            }
          },

        }],
    });
    var transset = create_var(Ext.getCmp('varcontainer'));
    Ext.getCmp('varcontainer').add(transset);
    var transset = create_trans(Ext.getCmp('transcontainer'));
    Ext.getCmp('transcontainer').add(transset);
    var sem = create_sem(Ext.getCmp('semcontainer'));
    Ext.getCmp('semcontainer').add(sem);
    var kol = create_kol(Ext.getCmp('kolcontainer'));
    Ext.getCmp('kolcontainer').add(kol);
    var ref = create_ref(Ext.getCmp('refcontainer'));
    Ext.getCmp('refcontainer').add(ref);

    verbform.render(document.body);

    var params = Ext.Object.fromQueryString(window.location.search.substring(1));

    /* load data */
    if (params.id != null) {
      var loadMask = new Ext.LoadMask(Ext.getBody(), {msg:"Please wait..."});
      loadMask.show();
      entryid = decodeURIComponent(params.id);
      Ext.Ajax.request({
        url: '/edna',
        params: {
          action: 'get',
          id: params.id,
        },
        method: 'get',
        success: function(response) {
          /* fill form */
          var data = JSON.parse(response.responseText);
          //alert(response.responseText)

          /* zahlavi */
          Ext.getCmp('lemma').setValue(data.entry.head.lemma.$);
          Ext.getCmp('gram').setValue(data.entry.head.gram.$);
          Ext.getCmp('ednaform').setTitle('Heslo '+data.entry.head.lemma.$);
          if (data.entry['@publish'] == 'true') {
            Ext.getCmp('publish').setValue(true);
          }
        
          /* kategorie */
          if (data.entry.head.klas != undefined) {
            /* normalize to Array */
            if (data.entry.head.klas.constructor != Array) {
              var klnew = {'$': data.entry.head.klas.$};
              data.entry.head.klas = new Array(klnew);
            }
            for (var i = 0; i < data.entry.head.klas.length; i++) {
              Ext.getCmp('klasif'+(i+1)).setValue(data.entry.head.klas[i].$);
              Ext.getCmp('klasbut'+(i+1)).setText(klas_hash[data.entry.head.klas[i].$]);
            }
          }

          /* varianty */
          if (data.entry.vars != undefined) {
            /* normalize to Array */
            if (data.entry.vars.constructor != Array) {
              var varnew = {'$': data.entry.vars.$};
              data.entry.vars = new Array(varnew);
            }
            if (data.entry.vars.length > 0) {
              Ext.getCmp('varcontainer').query('component[class="varset"]')[0].destroy();
            }
            for (var i = 0; i < data.entry.vars.length; i++) {
              var tr = create_var(Ext.getCmp('varcontainer'));
              Ext.getCmp('varcontainer').add(tr);
              tr.query('component[name="var"]')[0].setValue(data.entry.vars[i].$);
            }
          }

          /* preklady */
          if (data.entry.trans != undefined) {
            /* normalize to Array */
            if (data.entry.trans.constructor != Array) {
              var trnew = {'$': data.entry.trans.$, '@lang': data.entry.trans['@lang']};
              data.entry.trans = new Array(trnew);
            }
            if (data.entry.trans.length > 0) {
              Ext.getCmp('transcontainer').query('component[class="transset"]')[0].destroy();
            }
            for (var i = 0; i < data.entry.trans.length; i++) {
              var tr = create_trans(Ext.getCmp('transcontainer'));
              Ext.getCmp('transcontainer').add(tr);
              tr.query('component[name="translation"]')[0].setValue(data.entry.trans[i].$);
              tr.query('component[name="lang"]')[0].setValue(data.entry.trans[i]['@lang']);
            }
          }

          /* enc. inf. */
          Ext.getCmp('example').setValue(data.entry.ency.example.$);
          /* normalize to Array */
          if (data.entry.ency.sense != undefined ) {
            if (data.entry.ency.sense.constructor != Array) {
              if (data.entry.ency.sense.cite == undefined) {
                data.entry.ency.sense.cite = {'$':''};
              }
              var senew = {
                'def': {'$': data.entry.ency.sense.def.$, '@cz': data.entry.ency.sense.def['@cz'], '@eng': data.entry.ency.sense.def['@eng'], '@gb': data.entry.ency.sense.def['@gb'], '@us': data.entry.ency.sense.def['@us'], '@num': data.entry.ency.sense.def['@num']},
                'example': {'$': data.entry.ency.sense.example.$},
                'cite': {'$': data.entry.ency.sense.cite.$},
                'trans': data.entry.ency.sense.trans,
                'semlink': data.entry.ency.sense.semlink,
                'kolok': data.entry.ency.sense.kolok,
                'refer': data.entry.ency.sense.refer,
              };
              data.entry.ency.sense = new Array(senew);
            }
            if (data.entry.ency.sense.length > 0) {
              Ext.getCmp('ednaform').query('component[class="encyset"]')[0].destroy();
            }
            for (var i = 0; i < data.entry.ency.sense.length; i++) {
              var sen = create_ency();
              Ext.getCmp('encycontainer').add(sen);
              sen.query('component[name="ency_def"]')[0].setValue(data.entry.ency.sense[i].def.$);
              sen.query('component[name="ency_ex"]')[0].setValue(data.entry.ency.sense[i].example.$);
              sen.query('component[name="ency_cz"]')[0].setValue(data.entry.ency.sense[i].def['@cz']);
              sen.query('component[name="ency_eng"]')[0].setValue(data.entry.ency.sense[i].def['@eng']);
              sen.query('component[name="ency_gb"]')[0].setValue(data.entry.ency.sense[i].def['@gb']);
              sen.query('component[name="ency_us"]')[0].setValue(data.entry.ency.sense[i].def['@us']);
              sen.query('component[name="ency_un"]')[0].setValue(data.entry.ency.sense[i].def['@un']);
              sen.query('component[name="ency_nr"]')[0].setValue(data.entry.ency.sense[i].def['@num']);
              if (data.entry.ency.sense[i].cite != undefined) {
                sen.query('component[name="cite"]')[0].setValue(data.entry.ency.sense[i].cite.$);
              }

              /* semanticke */
              if (data.entry.ency.sense[i].semlink != undefined) {
                /* normalize to Array */
                if (data.entry.ency.sense[i].semlink.constructor != Array) {
                  var semnew = {'$': data.entry.ency.sense[i].semlink.$, '@type': data.entry.ency.sense[i].semlink['@type']};
                  data.entry.ency.sense[i].semlink = new Array(semnew);
                }
                if (data.entry.ency.sense[i].semlink.length > 0) {
                  sen.query('component[name="semcontainer"]')[0].query('component[class="semset"]')[0].destroy();
                }
                for (var j = 0; j < data.entry.ency.sense[i].semlink.length; j++) {
                  var sem = create_sem(sen.query('component[name="semcontainer"]')[0]);
                  sen.query('component[name="semcontainer"]')[0].add(sem);
                  sem.query('component[name="semlink"]')[0].setValue(data.entry.ency.sense[i].semlink[j].$);
                  sem.query('component[name="semtype"]')[0].setValue(data.entry.ency.sense[i].semlink[j]['@type']);
                }
              }
              /* preklady */
              if (data.entry.ency.sense[i].trans != undefined) {
                /* normalize to Array */
                if (data.entry.ency.sense[i].trans.constructor != Array) {
                  var trnew = {'$': data.entry.ency.sense[i].trans.$, '@lang': data.entry.ency.sense[i].trans['@lang']};
                  data.entry.ency.sense[i].trans = new Array(trnew);
                }
                if (data.entry.ency.sense[i].trans.length > 0) {
                  sen.query('component[name="transcontainer"]')[0].query('component[class="transset"]')[0].destroy();
                }
                for (var j = 0; j < data.entry.ency.sense[i].trans.length; j++) {
                  var tr = create_trans(sen.query('component[name="transcontainer"]')[0]);
                  sen.query('component[name="transcontainer"]')[0].add(tr);
                  tr.query('component[name="translation"]')[0].setValue(data.entry.ency.sense[i].trans[j].$);
                  tr.query('component[name="lang"]')[0].setValue(data.entry.ency.sense[i].trans[j]['@lang']);
                }
              }
              /* kolokace */
              if (data.entry.ency.sense[i].kolok != undefined) {
                /* normalize to Array */
                if (data.entry.ency.sense[i].kolok.constructor != Array) {
                  var newkol = {
                    "kolok": {'$': data.entry.ency.sense[i].kolok.kolok.$},
                    "trans": {'$': data.entry.ency.sense[i].kolok.trans.$},
                    "ref": {'$': data.entry.ency.sense[i].kolok.ref.$}
                  };
                  data.entry.ency.sense[i].kolok = new Array(newkol);
                }
                if (data.entry.ency.sense[i].kolok.length > 0) {
                  sen.query('component[name="kolcontainer"]')[0].query('component[class="kolset"]')[0].destroy();
                }
                for (var j = 0; j < data.entry.ency.sense[i].kolok.length; j++) {
                  var kol = create_kol(sen.query('component[name="kolcontainer"]')[0]);
                  sen.query('component[name="kolcontainer"]')[0].add(kol);
                  kol.query('component[name="kol"]')[0].setValue(data.entry.ency.sense[i].kolok[j].kolok.$);
                  kol.query('component[name="koltran"]')[0].setValue(data.entry.ency.sense[i].kolok[j].trans.$);
                  kol.query('component[name="kolref"]')[0].setValue(data.entry.ency.sense[i].kolok[j].ref.$);
                }
              }
              /* odkazy */
              if (data.entry.ency.sense[i].refer != undefined) {
                /* normalize to Array */
                if (data.entry.ency.sense[i].refer.constructor != Array) {
                  var refnew = {'$': data.entry.ency.sense[i].refer.$};
                  data.entry.ency.sense[i].refer = new Array(refnew);
                }
                if (data.entry.ency.sense[i].refer.length > 0) {
                  sen.query('component[name="refcontainer"]')[0].query('component[class="refset"]')[0].destroy();
                }
                for (var j = 0; j < data.entry.ency.sense[i].refer.length; j++) {
                  var ref = create_ref(sen.query('component[name="refcontainer"]')[0]);
                  sen.query('component[name="refcontainer"]')[0].add(ref);
                  ref.query('component[name="refer"]')[0].setValue(data.entry.ency.sense[i].refer[j].$);
                }
              }

            }
          }

          /* semanticke */
          if (data.entry.semlink != undefined) {
            /* normalize to Array */
            if (data.entry.semlink.constructor != Array) {
              var semnew = {'$': data.entry.semlink.$, '@type': data.entry.semlink['@type']};
              data.entry.semlink = new Array(semnew);
            }
            if (data.entry.semlink.length > 0) {
              Ext.getCmp('semcontainer').query('component[class="semset"]')[0].destroy();
            }
            for (var i = 0; i < data.entry.semlink.length; i++) {
              var sem = create_sem();
              Ext.getCmp('semcontainer').add(sem);
              sem.query('component[name="semlink"]')[0].setValue(data.entry.semlink[i].$);
              sem.query('component[name="semtype"]')[0].setValue(data.entry.semlink[i]['@type']);
            }
          }

          /* kolokace */
          if (data.entry.kolok != undefined) {
            /* normalize to Array */
            if (data.entry.kolok.constructor != Array) {
              var newkol = {
                "kolok": {'$': data.entry.kolok.kolok.$},
                "trans": {'$': data.entry.kolok.trans.$},
                "ref": {'$': data.entry.kolok.ref.$}
              };
              data.entry.kolok = new Array(newkol);
            }
            if (data.entry.kolok.length > 0) {
              Ext.getCmp('kolcontainer').query('component[class="kolset"]')[0].destroy();
            }
            for (var i = 0; i < data.entry.kolok.length; i++) {
              var kol = create_kol(Ext.getCmp('kolcontainer'));
              Ext.getCmp('kolcontainer').add(kol);
              kol.query('component[name="kol"]')[0].setValue(data.entry.kolok[i].kolok.$);
              kol.query('component[name="koltran"]')[0].setValue(data.entry.kolok[i].trans.$);
              kol.query('component[name="kolref"]')[0].setValue(data.entry.kolok[i].ref.$);
            }
          }

          /* odkazy */
          if (data.entry.refer != undefined) {
            /* normalize to Array */
            if (data.entry.refer.constructor != Array) {
              var refnew = {'$': data.entry.refer.$};
              data.entry.refer = new Array(refnew);
            }
            if (data.entry.refer.length > 0) {
              Ext.getCmp('refcontainer').query('component[class="refset"]')[0].destroy();
            }
            for (var i = 0; i < data.entry.refer.length; i++) {
              var ref = create_ref(Ext.getCmp('refcontainer'));
              Ext.getCmp('refcontainer').add(ref);
              ref.query('component[name="refer"]')[0].setValue(data.entry.refer[i].$);
            }
          }

          /* derivace */
          if (data.entry.deriv != undefined) {
            /* normalize to Array */
            if (data.entry.deriv.constructor != Array) {
              var newder = {
                "deriv": {'$': data.entry.deriv.deriv.$},
                "full": {'$': data.entry.deriv.full.$}
              };
              data.entry.deriv = new Array(newder);
            }
            if (data.entry.deriv.length > 0) {
              Ext.getCmp('ednaform').query('component[class="derset"]')[0].destroy();
            }
            for (var i = 0; i < data.entry.deriv.length; i++) {
              var der = create_der();
              Ext.getCmp('dercontainer').add(der);
              der.query('component[name="deriv"]')[0].setValue(data.entry.deriv[i].deriv.$);
              der.query('component[name="derivfull"]')[0].setValue(data.entry.deriv[i].full.$);
            }
          }

          /*citace*/
          if (data.entry.cite != undefined) {
            Ext.getCmp('cite').setValue(data.entry.cite.$);
          }

          loadMask.hide();

        }
      });
    }

});
