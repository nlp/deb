﻿function show_musings() {
  var coms = document.getElementsByClassName('musing');
  if (coms.length == 0) {
    document.getElementById('musing_link').innerHTML = 'no musings';
  }
  for (var i=0; i < coms.length; i++) {
    if (coms[i].style.display == 'none') {
      coms[i].style.display = 'block';
      document.getElementById('musing_link').innerHTML = 'hide musings';
    } else {
      coms[i].style.display = 'none';
      document.getElementById('musing_link').innerHTML = 'show musings';
    }
  }
}
function show_requests() {
  var coms = document.getElementsByClassName('request');
  if (coms.length == 0) {
    document.getElementById('comment_link').innerHTML = 'no requests';
  }
  for (var i=0; i < coms.length; i++) {
    if (coms[i].style.display == 'none') {
      coms[i].style.display = 'block';
      document.getElementById('comment_link').innerHTML = 'hide requests';
    } else {
      coms[i].style.display = 'none';
      document.getElementById('comment_link').innerHTML = 'show requests';
    }
  }
}
