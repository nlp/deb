var complete_time;
var id = new Array();
var entries = new Array();
var complete_time = new Array();
var textareas = new Array();
var ta_i = 0;
var changed = false;
var back_list = false;
var back_cat = '';
var entry_id = '';
var search2 = '';
var search2_opt = '';
var open_windows = new Array();
var got_src_list = '';
var footer_loaded = false;
var new_added_sources = new Array();

function edit_onload() {

    document.getElementById('loading_wait').style.display = 'block';
  for (var j = 0; j < entries.length; j++) {
    update(entries[j], true);
    resize(document.getElementById(entries[j]+'_comment'));
    resize(document.getElementById(entries[j]+'_musings'));
    resize(document.getElementById(entries[j]+'_history'));
    resize(document.getElementById(entries[j]+'_forenames'));
  }
  window.onbeforeunload = edit_onunload;

  if (footer_loaded != true) {
    setTimeout('window.location = window.location;', 500);
  } else {
    document.getElementById('but_save').disabled = false;
    document.getElementById('but_save_back').disabled = false;
    document.getElementById('info_loading').style.display = 'none';
    document.getElementById('loading_wait').style.display = 'none';
  }
  document.title = unescape(gup('id')) + ' DAFN2';
}


function edit_onunload() {
  unlock();
  if (changed) {
    return "Entry "+entry_id+". There are unsaved changes."
  }
}

function close_window() {
  unlock();
  window.close();
}

function reload_main() {
  //if (window.opener && window.opener.is_list_window) window.opener.location.reload();
}

function unlock(close_window) {
  var url = url_base + '?action=unlock_cluster&unlock=' + encodeURIComponent(entries.join('|'));
  var r = new XMLHttpRequest();
  r.open('GET', url, true);
  r.onreadystatechange = function () {
    if (r.readyState == 4) {
      if (close_window) {
        window.close();
      }
    }
  }
  r.send('');
}

function lock() {
  for (var j = 0; j < entries.length; j++) {
    var url = url_base + '?action=lock&lock=' + encodeURIComponent(entries[j]);
    var r = new XMLHttpRequest();
    r.open('GET', url, true);
    r.send('');
  }
}


function get_all() {
  data = '<data>';
  for (var i = 0; i < entries.length; i++) {
    data += get_xml(entries[i]);
  }
  data += '</data>';
  return data;
}

function createElementText(doc, elementname, text) {
  var newEl = doc.createElement(elementname);
  newEl.appendChild(doc.createTextNode(text));
  return newEl;
}

function get_xml(entry_id) {
  var doc = document.implementation.createDocument("", "", null);
  var entryEl = doc.createElement('entry');
  doc.appendChild(entryEl);


  if (entry_id == '__new__' || entry_id == '') {
    entryEl.setAttribute('sortkey', document.getElementById(entry_id+'_name').value);
  } else {
    entryEl.setAttribute('sortkey', entry_id);
  }
  entryEl.setAttribute('status', document.getElementById(entry_id+'_status').value);
  if (document.getElementById(entry_id+'_approved').checked) {
    entryEl.setAttribute('approved', 'true');
  }
  if (document.getElementById(entry_id+'_mmcheck').checked) {
    entryEl.setAttribute('mmcheck', 'true');
  }

  entryEl.appendChild(createElementText(doc, 'name', document.getElementById(entry_id+'_name').value));
  entryEl.appendChild(createElementText(doc, 'count', document.getElementById(entry_id+'_count').value));
  entryEl.appendChild(createElementText(doc, 'freq_2000', document.getElementById(entry_id+'_freq_2000').value));
  entryEl.appendChild(createElementText(doc, 'freq_2010', document.getElementById(entry_id+'_freq_2010').value));
  var frEl = doc.createElement('freq_2007');
  frEl.appendChild(doc.createTextNode(document.getElementById(entry_id+'_freq_2007').value));
  //frEl.setAttribute('origin', document.getElementById(entry_id+'_freq_origin').value);
  //frEl.setAttribute('group', document.getElementById(entry_id+'_freq_group').value);
  entryEl.appendChild(frEl);
  entryEl.appendChild(createElementText(doc, 'comment', document.getElementById(entry_id+'_comment').value));
  entryEl.appendChild(createElementText(doc, 'musings', document.getElementById(entry_id+'_musings').value));
  entryEl.appendChild(createElementText(doc, 'editors', document.getElementById(entry_id+'_editors').value));
  entryEl.appendChild(createElementText(doc, 'history', document.getElementById(entry_id+'_history').value));
  entryEl.appendChild(createElementText(doc, 'forenames', document.getElementById(entry_id+'_forenames').value));
  entryEl.appendChild(createElementText(doc, 'annot', document.getElementById(entry_id+'_annot').value));

  defEl = doc.createElement('defblock');
  entryEl.appendChild(defEl);
  if (document.getElementById(entry_id+'_prelim').value!='') {
    defEl.appendChild(createElementText(doc, 'prelim', document.getElementById(entry_id+'_prelim').value));
  }

  data = getElementsByAttribute(document, 'tr', 'data', entry_id+'_citex');
  for (var i=0; i < data.length; i++) {
    var s_nr = getElementsByAttribute(data[i], 'input', 'data', entry_id+'_nr')[0].value;
    var s_stat = getElementsByAttribute(data[i], 'select', 'data', entry_id+'_status')[0].value;
    var senseel = doc.createElement('sense');
    senseel.setAttribute('number', s_nr);
    senseel.setAttribute('status', s_stat);
    defEl.appendChild(senseel);
    var editor = data[i].getElementsByTagName('textarea')[0];
    senseel.appendChild(doc.createTextNode(editor.value.trim()));
  }

  var entry = new XMLSerializer().serializeToString(doc);
  return entry;
}
function save_all(close_after_save, reload_after_save) {
  /* Check: sense numbers sequential, or no sense 1*/
  for (var j = 0; j < entries.length; j++) {
    var data = getElementsByAttribute(document, 'tr', 'data', entries[j]+'_citex');
    var snr_ar = new Array();
    var snr_max = 0;
    var snr_cnt = 0;
    /* load and count sense numbers */
    for (var i = 0; i < data.length; i++) {
      var elname = data[i].getAttribute('element');
      if (elname == 'sense') {
        var s_nr = getElementsByAttribute(data[i], 'input', 'data', entries[j]+'_nr')[0].value;
        if (s_nr > snr_max) snr_max = s_nr;
        if (snr_ar[s_nr] == null) snr_ar[s_nr] = 1;
        else snr_ar[s_nr] += 1;
        snr_cnt +=1 ;
      }
    }
    /* only one sense and not nr 1 */
    if (snr_cnt == 1 && snr_max != 1) {
      if (confirm('Entry '+ entries[j] +': No sense 1. Really save entry?')) {
      } else {
        return false;
      }
    } else {
      /* check if each is present only once */
      var is_seq = true;
      for (var s = 1; s <= snr_max; s++) {
        if (snr_ar[s] != 1) is_seq = false;
      }
      if (!is_seq) {
        if (confirm('Entry '+ entries[j] +': Sense numbers are not sequential. Really save entry?')) {
        } else {
          return false;
        }
      }
    }
  }

  var entry = get_all();
  
  var url = url_base + '?action=save_cluster&data=' + encodeURIComponent(entry);
  if (close_after_save) {
    url += '&unlock=true';
  }
  var r = new XMLHttpRequest();
  r.open('GET', url, true);
  r.timeout = 0;
  r.onreadystatechange = function () {
    if (r.readyState == 4) {
      if (r.responseText.substring(0, 6) == 'saved ') {
        entry_id = r.responseText.substring(6, r.responseText.length);
        changed = false;
        for (var j = 0; j < entries.length; j++) {
          update(entries[j], true);
        }
        document.getElementById('info_saving').style.display = 'none';
        document.getElementById('info_saved').style.display = 'inline';
        if (close_after_save) {
          setTimeout(close_window, 1000);
        }
        if (reload_after_save != undefined) {
          window.location = url_base + '?action=cluster&id=' + encodeURIComponent(reload_after_save);
        }
      } else {
        alert('Error: '+ r.responseText);
      }
    }
  }
  document.getElementById('info_saved').style.display = 'none';
  document.getElementById('info_saving').style.display = 'inline';
  r.send(null);
}


function get_array(idStart, tagName) {
  var res_ar = new Array();
  if (tagName == null || tagName == undefined) tagName = 'input';
  var ar = document.getElementsByTagName(tagName);
  for (var i = 0; i < ar.length; i++) {
    re = new RegExp('^'+idStart + '[0-9]+')
    if (re.test(ar[i].id)) {
      res_ar.push(ar[i]);
    }
  }
  return res_ar;
}


function remove(element) {
  document.getElementById(element).parentNode.removeChild( document.getElementById(element) );
  //update();
}
function remove_var(element) {
  document.getElementById(element).parentNode.removeChild( document.getElementById(element) );
  //update();
}

function update(entry_id, ignore) {
  if (ignore != true) changed = true;
  if (changed) {
        document.getElementById('info_saved').style.display = 'none';
    document.getElementById(entry_id+'_modified').style.display = 'inline';
  } else {
    document.getElementById(entry_id+'_modified').style.display = 'none';
  }
}

function add_data(after, type, in_sense, sense_pos, entry_id) {
  var label = new Array();
  label['cit'] = 'early bearers';
  label['ex'] = 'expl.';
  label['sense'] = 'sense';
  label['arch'] = 'archive EB';
  label['igi'] = 'IGI';
  label['med'] = 'MED';
  label['other_info'] = 'other info';
  label['ppkk'] = 'expl.';
  label['dafn'] = 'DAFN/DoS';
  var newid = ta_i++;

  var before = after.nextSibling;
  var rows = after.parentNode;

  if (type == 'sense') {
    //count senses
    countdata = getElementsByAttribute(document, 'input', 'data', entry_id+'_nr');
    if (countdata.length == 1) {
      getElementsByAttribute(document, 'input', 'data', entry_id+'_nr')[0].value = '1';
    }


    //create row
    var ntr = document.createElement('tr');
    ntr.id = entry_id+'_citexrow' + (newid + 1);
    ntr.setAttribute('editor', newid);
    ntr.setAttribute('data', entry_id+'_citex');
    ntr.setAttribute('element', 'sense');
    ntr.setAttribute('class', 'sense');
    rows.insertBefore(ntr, before);
    var th = document.createElement('th');
    th.appendChild(document.createTextNode(label[type]));
    ntr.appendChild(th);
    var td = document.createElement('td');
    ntr.appendChild(td);
    var tab = document.createElement('table');
    td.appendChild(tab);
    var tr2 = document.createElement('tr');
    tab.appendChild(tr2);
    var th2 = document.createElement('th');
    th2.appendChild(document.createTextNode('nr.'));
    tr2.appendChild(th2);
    var td2 = document.createElement('td');
    tr2.appendChild(td2);
    var inp = document.createElement('input');
    inp.setAttribute('type', 'text');
    inp.setAttribute('data', entry_id+'_nr');
    inp.setAttribute('class', 'sense_nr');
    inp.value = countdata.length + 1;
    td2.appendChild(inp);
    td2.appendChild(document.createTextNode('Status.'));
    var tsel = document.createElement('select');
    tsel.setAttribute('data', entry_id+'_status');
    var opt = document.createElement('option');
    opt.setAttribute('value', 'Active');
    opt.appendChild(document.createTextNode('Active'));
    tsel.appendChild(opt);
    var opt = document.createElement('option');
    opt.setAttribute('value', 'Suppressed');
    opt.appendChild(document.createTextNode('Suppressed'));
    tsel.appendChild(opt);
    var opt = document.createElement('option');
    opt.setAttribute('value', 'Deleted');
    opt.appendChild(document.createTextNode('Deleted'));
    tsel.appendChild(opt);
    var opt = document.createElement('option');
    opt.setAttribute('value', 'New');
    opt.appendChild(document.createTextNode('New'));
    tsel.appendChild(opt);
    td2.appendChild(tsel);

    var but3 = document.createElement('button')
    but3.appendChild(document.createTextNode('+ sense'));
    but3.setAttribute('onclick', "add_data(document.getElementById('"+entry_id+"_citexrow" + (newid + 1) + "'), 'sense', '', '', '"+entry_id+"')");
    td2.appendChild(but3);
    var but3 = document.createElement('button')
    but3.appendChild(document.createTextNode('remove sense'));
    but3.setAttribute('onclick', "remove_sense('"+entry_id+"_citexrow" + (newid + 1) + "', '" + entry_id + "')");
    td2.appendChild(but3);

    var tr3 = document.createElement('tr');
    tab.appendChild(tr3);
    tr3.appendChild(document.createElement('td'));
    var td3 = document.createElement('td');
    tr3.appendChild(td3);

    var ud = document.createElement('div');
    ud.setAttribute('class', 'textarea_updiv');
    td3.appendChild(ud);
    var nd = document.createElement('div');
    nd.setAttribute('class', 'textarea_div');
    ud.appendChild(nd);
    var nta = document.createElement('textarea');
    nta.id = entry_id + '_sense' + (newid+1) + '_text';
    nta.setAttribute('element', 'ppkk');
    nta.setAttribute('class', 'ppkk');
    nta.setAttribute('onchange', 'checkxmltext(this, "'+entry_id+'");update("'+entry_id+'");');
    nta.setAttribute('onkeyup', 'resize(this);');
    nd.appendChild(nta);

    var td4 = document.createElement('td');
    td4.setAttribute('valign', 'top');
    tr3.appendChild(td4);
    var sspan = document.createElement('span');
    sspan.setAttribute('style', 'display:block');
    var ssel = document.createElement('select');
    ssel.setAttribute('onfocus', "populate_link_select(this, document.getElementById('"+entry_id+'_sense' + (newid+1)+ "_text'))");
    ssel.setAttribute('onchange', 'open_link(this)');
    var sopt = document.createElement('option');
    sopt.text = 'open links';
    ssel.appendChild(sopt);
    sspan.appendChild(ssel);
    td4.appendChild(sspan);

    //init editor
    make_textedit(nta);


    var tre = document.createElement('tr');
    tre.setAttribute('class', 'dataend');
    tab.appendChild(tre);

  }

  return true;
}

function go_var(text) {
  //alert(url_base + '?action=edit&id=' + text.value)
  window.open(url_base + '?action=edit&id=' + text.value + '&cat=' + back_cat, 'surnames' + text.value);
  return true;
}

function open_entry(entry) {
  if (window.opener) var wa = window.opener.open_windows;
  else var wa = open_windows;

  if (!wa[entry] || wa[entry].closed) {
    wa[entry] = window.open(url_base + '?action=edit&id=' + entry , 'surnames_' + entry);
  } else {
    alert('This entry is already opened.');
  }
  return true;
}

function xr_from(entry_id) {
  var url = url_base + '?action=xr_from&id=' + encodeURIComponent(entry_id);
  var r = new XMLHttpRequest();
  r.open ('GET', url, true);
  r.onreadystatechange = function () {
    if (r.readyState == 4) {
      console.log(r.responseText);
      var out = '';
      var lar = r.responseText.split("\n");
      for (var i = 0; i < lar.length; i++) {
        if (lar[i] != '') {
          var ear = lar[i].split(':');
          if (ear[1] == '') {
            out += ear[0] + ' Not in<br/>';
          } else {
            out += '<a onclick="window.opener.open_cluster(\''+ear[1]+'\')" href="#">'+ear[0]+'</a><br/>';
          }
        }
      }
      if (out == '') {out = 'none';}
      document.getElementById(entry_id+'_xr_from_show').innerHTML = out;
    }
  }
  r.send('');
}

function xr_to(entry_id) {
  var url = url_base + '?action=xr_to&id=' + encodeURIComponent(entry_id);
  var r = new XMLHttpRequest();
  r.open ('GET', url, true);
  r.onreadystatechange = function () {
    if (r.readyState == 4) {
      console.log(r.responseText);
      var out = '';
      var lar = r.responseText.split("\n");
      for (var i = 0; i < lar.length; i++) {
        if (lar[i] != '') {
          var ear = lar[i].split(':');
          if (ear[1] == '') {
            out += ear[0] + ' Not in<br/>';
          } else {
            out += '<a onclick="window.opener.open_cluster(\''+ear[1]+'\')" href="#">'+ear[0]+'</a><br/>';
          }
        }
      }
      if (out == '') {out = 'none';}
      document.getElementById(entry_id+'_xr_to_show').innerHTML = out;
    }
  }
  r.send('');
}

function check_hw(text, entry_id) {
  var hw = text.value;
  if (hw == '') return true;
  var prev_hw = document.getElementById(entry_id+'_cur_hw').value;

  var url = url_base + '?action=check_hw&check=' + encodeURIComponent(hw) + '&entry_id='+prev_hw;
  var r = new XMLHttpRequest ();
  r.open ('GET', url, true);
  r.onreadystatechange = function () {
    if (r.readyState == 4) {
      if (r.responseText != 'ok') {
        var move = confirm('This entry already exists. Do you want to display the entry? Cancel - stay here and reedit the entry. OK - display existing entry.');
        if(move) {
          window.location = url_base + '?action=cluster&id=' + encodeURIComponent(text.value);
          return true;
        }
      } else {
        document.getElementById(entry_id+'_cur_hw').value = text.value;
        save_all(false, text.value);
        changed = false;
        //window.location = url_base + '?action=cluster&id=' + encodeURIComponent(text.value);
      }
      return true;
    }
  }
  r.send ('');
}


function time_now() {
  var time = new Date();
  var m = (time.getMonth()+1 < 10)? '0'+(time.getMonth()+1):(time.getMonth()+1);
  var d = (time.getDate() < 10)? '0'+time.getDate():time.getDate();
  var h = (time.getHours() < 10)? '0'+time.getHours():time.getHours();
  var mm = (time.getMinutes() < 10)? '0'+time.getMinutes():time.getMinutes();
  return time.getFullYear() + '-' + m + '-' + d + ' ' + h + ':' + mm;
}


function checkxmltext(textbox, entry_id) {
  var xmltext = '<text>'+textbox.value+'</text>';
  //var xml = new XML(xmltext);
  var dp = new DOMParser();
  var xml = dp.parseFromString(xmltext, "text/xml");
  if (xml.documentElement.nodeName == "parsererror" || xml.documentElement.getElementsByTagName("parsererror").length > 0) {
    if (textbox.getAttribute('element') == 'arch') {
      var type = 'EB';
    } else {
      var type= 'expl.';
    }
    alert('Please check the tags in text, entry '+entry_id+', '+type);
  } else {
    return true;
  }
}



function remove_sense(id, entry_id) {
  if (confirm('Really remove sense?')) {
    var element = document.getElementById(id);
    element.parentNode.removeChild(element);
    countdata = getElementsByAttribute(document, 'input', 'data', entry_id+'_nr');
    if (countdata.length == 0) {
      document.getElementById(entry_id+'_add_sense_box').style.visibility = 'visible';
    }
    return true;
  } else {
    return true;
  }
}


function populate_link_select(select, textbox) {
  var text = textbox.value;

  var mes = text.match(/<me>[^<]*<\/me>/g);
  var vars = text.match(/<var>[^<]*<\/var>/g);
  var xrs = text.match(/<xr>[^<]*<\/xr>/g);
  var names = new Array();
  if (mes != null) {
    for (var i=0; i < mes.length; i++) {
      var name = mes[i];
      var xref = /<me>([^<]*)<\/me>/.exec(name)[1];
      names.push(xref);
    }
  }
  if (vars != null) {
    for (var i=0; i < vars.length; i++) {
      var name = vars[i];
      var xref = /<var>([^<]*)<\/var>/.exec(name)[1];
      names.push(xref);
    }
  }
  if (xrs != null) {
    for (var i=0; i < xrs.length; i++) {
      var name = xrs[i];
      var xref = /<xr>([^<]*)<\/xr>/.exec(name)[1];
      names.push(xref);
    }
  }
  if (select.options.length > 1) {
    for (var i=select.options.length-1; i > 0; i--) {
      select.removeChild(select.options[i]);
    }
  }
  for (var i=0; i < names.length; i++) {
    var opt = document.createElement('option');
    opt.value = names[i];
    opt.text = names[i];
    select.appendChild(opt);
  }
}

function open_link(select) {
  var ind = select.selectedIndex;
  if (window.opener == null) {
    alert('Error opening entry. Entry not opened from entry manager?');
    return false;
  }
  if (ind > 0) {
    window.opener.open_cluster(select.options[ind].value);
  }
}

function show_chinese(elem) {
  var cnre = new RegExp('render img="([0-9]*)"');
  var cnar = cnre.exec(elem.value);
  if (cnar != null && cnar.length > 0) {
    for (var i = 1; i < cnar.length; i++) {
      console.log(cnar[i])
      var img = document.createElement('br');
      elem.parentNode.nextSibling.appendChild(img);
      elem.parentNode.nextSibling.appendChild(document.createTextNode(cnar[i]+'='));
      var img = document.createElement('img');
      img.setAttribute('src', '/files/dafnimages/gb_'+cnar[i]+'.JPG');
      elem.parentNode.nextSibling.appendChild(img);
    }
  }
}


function save_before_move() {
  if (changed) {
    alert('Please, save the changes before moving the entry');
    return false;
  } else {
    return true;
  }
}

function move_entry(from, to, entry_id) {
  entry_hw = document.getElementById(entry_id+'_name').value;
  if (to == 'dafn2') {
    var text = 'Really move entry '+entry_hw+' to main list?';
  }
  if (to == 'dafn2quar') {
    var text = 'Really move entry '+entry_hw+' to quarantine?';
  }
  if (to == 'dafn2res') {
    var text = 'Really move entry '+entry_hw+' to reserve?';
  }
  if (confirm(text)) {
    document.getElementById('info_move_entry_'+entry_id).style.display = 'inline';
    var url = url_base + '?action=move_entry&id='+entry_hw+'&from='+from+'&to='+to;
    var r = new XMLHttpRequest();
    r.open ('GET', url, true);
    r.onreadystatechange = function () {
      if (r.readyState == 4) {
        if (r.responseText == 'entry moved') {
          document.getElementById('info_move_entry_'+entry_id).style.display = 'none';
          var newurl = url_base_s + to + '?action=cluster&id='+encodeURIComponent(encodeURIComponent(entry_hw));
          alert('Entry moved. Taking you to moved entry now.');
          location.href = newurl;
        } else if (r.responseText == 'in target') {
          var msg = 'Error: entry exists in target database. Overwrite?';
          if (confirm(msg)) {
            move_entry_force(from,to,entry_id);
          }
        } else {
          document.getElementById('info_move_entry_'+entry_id).style.display = 'none';
          alert(r.responseText);
        }
        return true;
      }
    }
    r.send ('');
  }
}

function move_entry_force(from, to, entry_id) {
  document.getElementById('info_move_entry_'+entry_id).style.display = 'inline';
  var url = url_base + '?action=move_entry&force=true&id='+entry_hw+'&from='+from+'&to='+to;
  var r = new XMLHttpRequest();
  r.open ('GET', url, true);
  r.onreadystatechange = function () {
    if (r.readyState == 4) {
      if (r.responseText == 'entry moved') {
        document.getElementById('info_move_entry_'+entry_id).style.display = 'none';
        var newurl = url_base_s + to + '?action=cluster&id='+entry_hw;
        alert('Entry moved. Taking you to moved entry now.');
        location.href = newurl;
      } else {
        document.getElementById('info_move_entry_'+entry_id).style.display = 'none';
        alert(r.responseText);
      }
      return true;
    }
  }
  r.send ('');
}
/*
	Copyright Robert Nyman, http://www.robertnyman.com
	Free to use if this text is included
*/
function getElementsByAttribute(oElm, strTagName, strAttributeName, strAttributeValue){
	var arrElements = (strTagName == "*" && oElm.all)? oElm.all : oElm.getElementsByTagName(strTagName);
	var arrReturnElements = new Array();
	var oAttributeValue = (typeof strAttributeValue != "undefined")? new RegExp("(^|\\s)" + strAttributeValue + "(\\s|$)") : null;
	var oCurrent;
	var oAttribute;
	for(var i=0; i<arrElements.length; i++){
		oCurrent = arrElements[i];
		oAttribute = oCurrent.getAttribute && oCurrent.getAttribute(strAttributeName);
		if(typeof oAttribute == "string" && oAttribute.length > 0){
			///if(typeof strAttributeValue == "undefined" || (oAttributeValue && oAttributeValue.test(oAttribute))){
			if(typeof strAttributeValue == "undefined" || (oAttribute==strAttributeValue)){
				arrReturnElements.push(oCurrent);
			}
		}
	}
	return arrReturnElements;
}

String.prototype.trim = function () {
    return this.replace(/^\s*/, "").replace(/\s*$/, "");
}

function gup( name )
{
  name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
  var regexS = "[\\?&]"+name+"=([^&#]*)";
  var regex = new RegExp( regexS );
  var results = regex.exec( window.location.href );
  if( results == null )
    return "";
  else
    return results[1];
}

Array.prototype.contains = function(obj) {
  var i = this.length;
  while (i--) {
    if (this[i] === obj) {
      return true;
    }
  }
  return false;
}


