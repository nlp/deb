var columns_show = true;
var sortdone = false;
var search_row_count = 1;

function getParameterByName(name) {
  name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
  var regexS = "[\\?&]" + name + "=([^&#]*)";
  var regex = new RegExp(regexS);
  var results = regex.exec(window.location.search);
  if(results == null)
    return "";
  else
    return decodeURIComponent(results[1].replace(/\+/g, " "));
}

function onload_list() {
  //check multiple query
  if (getParameterByName('hw2') != '') {
    var row_count = 2;
    var go = true;
    while(go) {
      add_search_row(
        getParameterByName('not'+row_count), 
        getParameterByName('hw'+row_count),
        getParameterByName('where_hw'+row_count),
        getParameterByName('search'+row_count),
        getParameterByName('where_search'+row_count),
        getParameterByName('contrib'+row_count),
        getParameterByName('andor'+row_count),
        getParameterByName('approved'+row_count),
        getParameterByName('mmcheck'+row_count)
      );
      row_count += 1;
      if (getParameterByName('hw'+row_count) == '') {
        go = false;
      }
    }
  }

}

function st_wait_handler(e) {
  if (e.target.id == 'wait' && sortdone){
    //window.alert(e.target.id);
    e.target.innerHTML = 'd';
    //e.target.style.display = 'none';
  }
}

function del_search_row(id) {
  var el = document.getElementById(id);
  el.parentNode.removeChild(el);
}  

function switch_regex(textbox) {
  if (/[^a-zA-Z]/.test(textbox.value)) {
    var select = textbox.nextSibling;
    if (select.nodeType == 3) {
      select = textbox.nextSibling.nextSibling;
    }
    select.value = 'regexp';
  }
}

function add_search_row(not, hw, where_hw, search, where_search, contrib, andor, approved, mmcheck) {
  search_row_count += 1;
  var fragment = document.createDocumentFragment();

  var tr = document.createElement('tr');
  var td = document.createElement('td');

  var htmlstring = '';
  htmlstring += '<select id="andor'+search_row_count+'" name="andor'+search_row_count+'"><option value="and">AND</option><option value="or">OR</option></select>';
  htmlstring += '<label><input type="checkbox" id="not'+search_row_count+'" name="not'+search_row_count+'" value="true"/>NOT</label>';
  htmlstring += '<input type="text" name="hw'+search_row_count+'" value="surname..." class="fill" onfocus="box_clear(this, &apos;surname...&apos;);" onkeyup="switch_regex(this);" id="hw'+search_row_count+'"/>';
  htmlstring += '<select name="where_hw'+search_row_count+'" id="where_hw'+search_row_count+'"><option value="start" selected="selected">starts</option><option value="cont">contains</option><option value="end">ends</option><option value="regexp">RegExp</option></select>  || ';
  htmlstring += '<input style="width:20em;" type="text" name="search'+search_row_count+'" id="search'+search_row_count+'" value="search..." class="fill" onfocus="box_clear(this, &apos;search...&apos;);" />';
  htmlstring += '<select name="where_search'+search_row_count+'" id="where_search'+search_row_count+'"><option value="all">all</option><option value="expl">expl.</option><option value="prelim">preamble</option><option value="history">history</option><option value="fore">forenames</option><option value="editors">editors</option><option value="comment">requests</option><option value="musings">musings</option></select>  ||';
  htmlstring += 'contributor  <select name="contrib'+search_row_count+'" id="contrib'+search_row_count+'">    <option value="">-</option>    <option value="beider">axb</option>    <option value="cieslikowa">axc</option>    <option value="horace">cxc</option>    <option value="marcato">cxm</option>    <option value="dcole">djc</option>    <option value="brozovic">dxb</option>    <option value="kremer">dxk</option>    <option value="moldanova">dxm</option>    <option value="tomescu">dxt</option>    <option value="caffarelli">exc</option>    <option value="gentry">exg</option>    <option value="brady">fxb</option>    <option value="freeman">jxf</option>    <option value="kilpatrick">kak</option>    <option value="brouwer">lsb</option>    <option value="ryman">lxr</option>    <option value="picard">mxp</option>    <option value="veka">oxv</option>    <option value="taylor">pjt</option>    <option value="patrick">pwh</option>    <option value="durco">pxd</option>    <option value="swoboda">pxs</option>    <option value="marshall">rxm</option>    <option value="rcampbell">rjc</option>    <option value="lenarcic">sxl</option>  </select>';
  htmlstring += ' || <label><input name="approved'+search_row_count+'" id="approved'+search_row_count+'" value="true" type="checkbox">approved</label>';
  htmlstring += ' || <label><input name="mmcheck'+search_row_count+'" id="mmcheck'+search_row_count+'" value="true" type="checkbox">MM checked</label>';

  td.innerHTML = htmlstring;
  tr.appendChild(td);

  var td2 = document.createElement('td');
  td2.innerHTML = '<input type="button" value="-" onclick="del_search_row(\'search_row'+search_row_count+'\');"/>';
  tr.appendChild(td2);

  fragment.appendChild(tr);
  document.getElementById('search_param').appendChild(fragment);
  td.setAttribute('class', 'search_row');
  tr.setAttribute('id', 'search_row'+search_row_count);

  //set values
  if (andor != null) {
    document.getElementById('andor'+search_row_count).value = andor;
  }
  if (not != null && not == 'true') {
    document.getElementById('not'+search_row_count).checked = true;
  }
  if (hw != null) {
    document.getElementById('hw'+search_row_count).value = hw;
    if (hw != 'surname...') {
      document.getElementById('hw'+search_row_count).className = '';
    }
  }
  if (where_hw != null) {
    document.getElementById('where_hw'+search_row_count).value = where_hw;
  }
  if (search != null) {
    document.getElementById('search'+search_row_count).value = search;
    if (search != 'search...') {
      document.getElementById('search'+search_row_count).className = '';
    }
  }
  if (where_search != null) {
    document.getElementById('where_search'+search_row_count).value = where_search;
  }
  if (contrib != null) {
    document.getElementById('contrib'+search_row_count).value = contrib;
  }
  if (approved != null && approved == 'true') {
    document.getElementById('approved'+search_row_count).checked = true;
  }
  if (mmcheck != null && mmcheck == 'true') {
    document.getElementById('mmcheck'+search_row_count).checked = true;
  }

}
