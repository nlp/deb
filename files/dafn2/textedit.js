function make_textedit(textelem) {
  //alert(textelem)
  bd = document.createElement('div');
  bd.setAttribute('style', 'float:right');
  textelem.parentNode.parentNode.appendChild(bd);
  add_button('icon_italic.png', 'Italic', 'insert_i', textelem, bd);
  add_button('icon_bold.png', 'Bold', 'insert_b', textelem, bd);
  br2 = document.createElement('br');
  bd.appendChild(br2);
  add_button('icon_special.png', 'Special character', 'insert_char', textelem, bd);
  add_button('icon_xref.png', 'Cross-reference', 'insert_x', textelem, bd);
  add_button('icon_quote.png', 'Quotes', 'insert_quote', textelem, bd);
  resize(textelem);
}

function add_button(icon, title, action, textelem, buttonelem, add_arg) {
  var img = document.createElement('img');
  img.setAttribute('src', 'files/'+icon);
  img.setAttribute('title', title);
  img.setAttribute('class', 'button_img');
  if (add_arg != null) {
    img.setAttribute('onclick', action + "('" + textelem.id + "', '" + add_arg + "')");
  } else {
    img.setAttribute('onclick', action + "('" + textelem.id + "')");
  }
  buttonelem.appendChild(img);
  return true;
}

function insert_src(textid) {
  selsrc_object = document.getElementById(textid);
  showPopWin('surnames?action=sources', 650, 180, null);
}

function insert_char(textid) {
  selchar_object = document.getElementById(textid);
  showPopWin('files/charsel.html', 520, 180, null);
}

function insert_i(textid) {
  return insert_tag(textid, 'i');
}

function insert_b(textid) {
  return insert_tag(textid, 'b');
}

function insert_x(textid) {
  insert_tag(textid, 'xr');
  var entry_id = textid.match(/[^_]*/);
  checkxmltext(document.getElementById(textid), entry_id);
  return true;
}
function insert_v(textid) {
  insert_tag(textid, 'me');
  var entry_id = textid.match(/[^_]*/);
  checkxmltext(document.getElementById(textid), entry_id);
  return true;
}

function insert_tag(textid, tag) {
  var element = document.getElementById(textid);
  var starttag = '<' + tag + '>';
  var endtag = '</' + tag + '>';
  var startpos = element.selectionStart;
  var endpos = element.selectionEnd + starttag.length;
  string_insert(element, starttag, startpos);
  string_insert(element, endtag, endpos);
  if (tag == 'xr') {
    element.value = element.value.slice(0, startpos+starttag.length) + element.value.slice(startpos+starttag.length, endpos).replace(/<\/?[a-z]*>/g,'') + element.value.slice(endpos);
  }
  var newpos = endpos + endtag.length;
  element.focus();
  element.selectionStart = newpos;
  element.selectionEnd = newpos;
  return true;
}

function insert_quote(textid) {
  var element = document.getElementById(textid);
  var starttag = '‘';
  var endtag = '’';
  var startpos = element.selectionStart;
  var endpos = element.selectionEnd + starttag.length;
  string_insert(element, starttag, startpos);
  string_insert(element, endtag, endpos);
  if (tag == 'xr') {
    element.value = element.value.slice(0, startpos+starttag.length) + element.value.slice(startpos+starttag.length, endpos).replace(/<\/?[a-z]*>/g,'') + element.value.slice(endpos);
  }
  var newpos = endpos + endtag.length;
  element.focus();
  element.selectionStart = newpos;
  element.selectionEnd = newpos;
  return true;
}

function char_insert(character) {
  string_insert(selchar_object, character, selchar_object.selectionEnd);
}

function src_insert(text) {
  var startpos = selsrc_object.selectionStart;
  selsrc_object.value = selsrc_object.value.slice(0, selsrc_object.selectionStart) + selsrc_object.value.slice(selsrc_object.selectionEnd);
  string_insert(selsrc_object, text, startpos);
}

function string_insert(element, newtext, newpos) {
	element.value = element.value.slice(0,newpos) + newtext + element.value.slice(newpos);
}

function insert_source_quick(textid, what) {
  selsrc_object = document.getElementById(textid);
  switch(what) {
    case 'fiants':
      var text = 'FORENAME <i>SURNAME</i>, DATE in <src>Fiants</src> (PLACE);';
      break;
    case 'igi':
      var text = 'FN <i>SN</i>, YEAR in <src>IGI</src> (PLACE, COUNTY);';
      break;
  }
  return src_insert(text);
}


function resize(element) {
  while (element.offsetHeight < element.scrollHeight) {
    element.style.height = element.offsetHeight+20+'px';
    element.parentNode.parentNode.style.height = element.parentNode.parentNode.offsetHeight+20+'px';
  }
}

