var complete_time;
var id = new Array();
id['var'] = 1
id['ex'] = 1
id['cit'] = 1
id['sense'] = 1
var textareas = new Array();
var ta_i = 0;
var changed = false;
var back_list = false;
var back_cat = '';
var entry_id = '';
var search2 = '';
var search2_opt = '';
var open_windows = new Array();


function close_window() {
  unlock();
  window.close();
}

function reload_main() {
  if (window.opener && window.opener.is_list_window) window.opener.location.reload();
}

function unlock() {
  var url = url_base + '?action=unlock&unlock=' + encodeURIComponent(entry_id);
  var r = new XMLHttpRequest ();
  r.open('GET', url, true);
  r.send('');
}

function lock() {
  var url = url_base + '?action=lock&lock=' + encodeURIComponent(entry_id);
  var r = new XMLHttpRequest ();
  r.open('GET', url, true);
  r.send('');
}

function open_cat(cat) {
  if (search2 == '') {
    if (cat == null) {
      if (back_cat != '') {
        cat = back_cat;
      } else {
        cat = document.getElementById('hw').value[0];
      }
    }
    location.href = url_base + '?action=list&cat=' + cat + '&unlock=' + encodeURIComponent(entry_id) + '#' + encodeURIComponent(entry_id);
  } else {
    location.href = url_base + '?action=search&search=' + search2 + '&where=' + search2_opt + '&unlock=' + encodeURIComponent(entry_id) + '#' + encodeURIComponent(entry_id);
  }
  return true;
}



function open_entry(entry) {
  if (window.opener) var wa = window.opener.open_windows;
  else var wa = open_windows;

  if (!wa[entry] || wa[entry].closed) {
    wa[entry] = window.open(url_base + '?action=edit&id=' + entry , 'dafn2_' + entry);
  } else {
    alert('This entry is already opened.');
  }
  return true;
}

function open_cluster(entry) {
  if (window.opener && window.opener.open_windows) var wa = window.opener.open_windows;
  else var wa = open_windows;
//alert(url_base)
  if (!wa['clust'+entry] || wa['clust'+entry].closed) {
    wa['clust'+entry] = window.open(url_base + '?action=cluster&id=' + entry , 'dafn2clust_' + entry);
  } else {
    alert('This cluster is already opened.');
  }
  return true;
}


function time_now() {
  var time = new Date();
  var m = (time.getMonth()+1 < 10)? '0'+(time.getMonth()+1):(time.getMonth()+1);
  var d = (time.getDate() < 10)? '0'+time.getDate():time.getDate();
  var h = (time.getHours() < 10)? '0'+time.getHours():time.getHours();
  var mm = (time.getMinutes() < 10)? '0'+time.getMinutes():time.getMinutes();
  return time.getFullYear() + '-' + m + '-' + d + ' ' + h + ':' + mm;
}

function edit_sources(data) {
  var editor = data.getElementsByTagName('textarea')[0];
  if (editor.value.trim() != '') {
    var text = editor.value;
    var match = text.match(/<src>(.*?)<\/src>/g);
    if (match != null) {
      for (i = 0; i < match.length; i++) {
        match[i] = match[i].replace(/<\/?src>/g,'');
      }
      url = url_base + '?action=edit_source&orig=' + match.join('%7C');
      window.open(url, 'edit_source');
    }
    return true;
  }
  return true;
}

function change_sources_text(origtext, newtext) {
  data = getElementsByAttribute(document, 'tr', 'data', 'citex');
  for (var i=0; i < data.length; i++) {
    var elname = data[i].getAttribute('element');
    switch(elname) {
      case 'ex':
      case 'cit':
        var editor = data[i].getElementsByTagName('textarea')[0];
        var newtext = editor.value.replace('<src>'+origtext+'</src>', '<src>'+newtext+'</src>');
        editor.value = newtext;
        break;
      case 'sense':
        s_data = getElementsByAttribute(data[i], 'tr', 'data', 'sensecitex');
        for (var j=0; j<s_data.length; j++) {
          var s_elname = s_data[j].getAttribute('element');
          switch(s_elname) {
            case 'ex':
            case 'cit':
              var editor = s_data[j].getElementsByTagName('textarea')[0];
              var newtext = editor.value.replace('<src>'+origtext+'</src>', '<src>'+newtext+'</src>');
              editor.value = newtext;
              break;
          }
        }

    }
  }
}

/*
	Copyright Robert Nyman, http://www.robertnyman.com
	Free to use if this text is included
*/
function getElementsByAttribute(oElm, strTagName, strAttributeName, strAttributeValue){
	var arrElements = (strTagName == "*" && oElm.all)? oElm.all : oElm.getElementsByTagName(strTagName);
	var arrReturnElements = new Array();
	var oAttributeValue = (typeof strAttributeValue != "undefined")? new RegExp("(^|\\s)" + strAttributeValue + "(\\s|$)") : null;
	var oCurrent;
	var oAttribute;
	for(var i=0; i<arrElements.length; i++){
		oCurrent = arrElements[i];
		oAttribute = oCurrent.getAttribute && oCurrent.getAttribute(strAttributeName);
		if(typeof oAttribute == "string" && oAttribute.length > 0){
			if(typeof strAttributeValue == "undefined" || (oAttributeValue && oAttributeValue.test(oAttribute))){
				arrReturnElements.push(oCurrent);
			}
		}
	}
	return arrReturnElements;
}

String.prototype.trim = function () {
    return this.replace(/^\s*/, "").replace(/\s*$/, "");
}

function gup( name )
{
  name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
  var regexS = "[\\?&]"+name+"=([^&#]*)";
  var regex = new RegExp( regexS );
  var results = regex.exec( window.location.href );
  if( results == null )
    return "";
  else
    return results[1];
}


