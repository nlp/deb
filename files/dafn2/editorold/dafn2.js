/* verbalex ext js */

var entryid;

Ext.require([
    'Ext.form.*',
]);

var status_store = Ext.create('Ext.data.Store', {
  fields: ['status', 'label'],
  data: [
    {'status':'Active', 'label':'Active'},
    {'status':'Suppressed', 'label':'Suppressed'},
    {'status':'Deleted', 'label':'Deleted'},
    {'status':'New', 'label':'New'},
  ]
});
var origin_store = Ext.create('Ext.data.Store', {
  fields: ['origin', 'label'],
  data: [
  {'origin':'generic', 'label':'generic'},
  {'origin':'Chinese', 'label':'Chinese'},
  {'origin':'Czech', 'label':'Czech'},
  {'origin':'CzechSlovak', 'label':'CzechSlovak'},
  {'origin':'Dutch', 'label':'Dutch'},
  {'origin':'French', 'label':'French'},
  {'origin':'German', 'label':'German'},
  {'origin':'Hispanic', 'label':'Hispanic'},
  {'origin':'Hungarian', 'label':'Hungarian'},
  {'origin':'Indian', 'label':'Indian'},
  {'origin':'Irish', 'label':'Irish'},
  {'origin':'Italian', 'label':'Italian'},
  {'origin':'Japanese', 'label':'Japanese'},
  {'origin':'Korean', 'label':'Korean'},
  {'origin':'LatvianLithuanian', 'label':'LatvianLithuanian'},
  {'origin':'Muslim', 'label':'Muslim'},
  {'origin':'Norwegian', 'label':'Norwegian'},
  {'origin':'OtherEastAsian', 'label':'OtherEastAsian'},
  {'origin':'Polish', 'label':'Polish'},
  {'origin':'Scandinavian', 'label':'Scandinavian'},
  {'origin':'Scottish', 'label':'Scottish'},
  {'origin':'Swedish', 'label':'Swedish'},
  {'origin':'Ukranian', 'label':'Ukranian'},
  {'origin':'Vietnamese', 'label':'Vietnamese'},
  ]
});

function checkxmltext(textbox) {
  var xmltext = '<text>'+textbox.getValue()+'</text>';
  var dp = new DOMParser();
  var xml = dp.parseFromString(xmltext, "text/xml");
  if (xml.documentElement.nodeName == "parsererror" || xml.documentElement.getElementsByTagName("parsererror").length > 0) {
    alert('Please check the tags in text: ' + textbox.name);
    return false;
  } else {
    return true;
  }
}

function insert_tag(textid, tag) {
  var element = Ext.getDom(textid);
  var starttag = '<' + tag + '>';
  var endtag = '</' + tag + '>';
  var startpos = element.selectionStart;
  var endpos = element.selectionEnd + starttag.length;
  string_insert(element, starttag, startpos);
  string_insert(element, endtag, endpos);
  var newpos = endpos + endtag.length;
  element.focus();
  element.selectionStart = newpos;
  element.selectionEnd = newpos;
  return true;
}

function string_insert(element, newtext, newpos) {
  element.value = element.value.slice(0,newpos) + newtext + element.value.slice(newpos);
}



function count_elements(search) {
  var count = 0;
  Ext.ComponentMgr.all.each(function (item) {
    if (item.substr(0,search.length) == search) {
      count++;
    }
  });

  return count;
}

Ext.define('Dafn.form.field.RichText', {
  extend: 'Ext.container.Container',
  alias: 'widget.richtext',
  initComponent: function() {
    //alert(this.fieldLabel);
    var newta = {
      xtype: 'textarea',
      width: 600,
      rows: 2,
      grow: 'true',
      fieldLabel: this.fieldLabel
    };
    this.items = [newta,{
      xtype: 'container',
      layout: {
        type: 'vbox'
      },
      items: [{
        xtype: 'label',
        text: 'B',
      },{
        xtype: 'label',
        text: 'I',
      }]
    }];
    this.callParent(arguments);

  },
  layout: {
    type: 'hbox'
  },
}
);

function create_hist() {
  var name = 'hist_'+Ext.id();

  var histset = Ext.create('Ext.form.FieldSet', {
    border: false,
    xtype: 'fieldset',
    id: name,
    class: 'histset',
    name: 'histset',
    layout: {
      type: 'hbox'
    },
    items: [{
        xtype: 'textarea',
        name: 'history',
        grow: 'true',
        width: 600,
        rows: 2,
            listeners: {
              'blur': function(field) {
                checkxmltext(field);
              }
            },
    },{
      xtype: 'textfield',
              fieldLabel: 'annot.',
              name: 'annot',
              //value: 'EC, DK'*/
    },{
      xtype: 'splitter'
    },{
      xtype: 'button',
      text: '+ history',
      handler: function() {
        var histset = create_hist();
        Ext.getCmp('histcontainer').add(histset);
      }
    },{
      xtype: 'splitter'
    },{
      xtype: 'button',
      text: '- history',
      handler: function() {
        if (confirm('Delete?')) {
          Ext.getCmp(name).destroy();
        }
      }
    }]
  });

  return histset;
}

function create_sense() {
  var name = 'sense_'+Ext.id();

  var senseset = Ext.create('Ext.form.FieldSet', {
    border: false,
    xtype: 'fieldset',
    id: name,
    class: 'senseset',
    name: 'senseset',
    layout: {
      type: 'hbox'
    },
    items: [{
      xtype: 'textfield',
      name: 'number',
      fieldLabel: 'nr.',
      labelAlign: 'top',
      width: 20
    },{
        xtype: 'textarea',
        name: 'sense_def',
        grow: 'true',
        width: 600,
        rows: 2,
            listeners: {
              'blur': function(field) {
                checkxmltext(field);
              }
            }
    },{
      xtype: 'container',
      layout: {
        type: 'vbox'
      },
      items: [{
              fieldLabel: 'status',
              name: 'status',
              xtype: 'combobox',
              store: status_store,
              displayField: 'label',
              valueField: 'status',
      },{
      xtype: 'textfield',
              fieldLabel: 'annot.',
              name: 'annot',
      }]
    },{
      xtype: 'splitter'
    },{
      xtype: 'button',
      text: '+ sense',
      handler: function() {
        var senseset = create_sense();
        Ext.getCmp('sensecontainer').add(senseset);
      }
    },{
      xtype: 'splitter'
    },{
      xtype: 'button',
      text: '- sense',
      handler: function() {
        if (count_elements('sense_') == 1) {
          alert("can't delete last sense.");
        } else if (confirm('Delete?')) {
          Ext.getCmp(name).destroy();
        }
      }
    }]
  });

  return senseset;
}



        function save_data(closewindow) {
            /* save data */
            //var form = this.up('form').getForm();
            var form = Ext.getCmp('dafnform').getForm();
            var data = {"entry":{
              "name": {},
              "count": {},
              "defblock": {"sense":[]}
            }};

            /* heading */
            data.entry.name = {'$': Ext.getCmp('dafnform').query('component[name="name"]')[0].getValue()};
            data.entry.count = {'$': Ext.getCmp('dafnform').query('component[name="count"]')[0].getValue()};
            data.entry.freq_2007 = {'$': Ext.getCmp('dafnform').query('component[name="freq_2007"]')[0].getValue()};
            data.entry['@status'] = Ext.getCmp('dafnform').query('component[name="status"]')[0].getValue();
            if (Ext.getCmp('dafnform').query('component[name="sortkey"]')[0].getValue() == '') {
              data.entry['@sortkey'] = Ext.getCmp('dafnform').query('component[name="name"]')[0].getValue();
              entryid = Ext.getCmp('dafnform').query('component[name="name"]')[0].getValue();
            } else {
              data.entry['@sortkey'] = Ext.getCmp('dafnform').query('component[name="sortkey"]')[0].getValue();
            }

            /* forenames */
            if (Ext.getCmp('dafnform').query('component[name="forenames"]')[0].getValue() != '') {
              data.entry.forenames = {'$': Ext.getCmp('dafnform').query('component[name="forenames"]')[0].getValue()};
              if (checkxmltext(Ext.getCmp('dafnform').query('component[name="forenames"]')[0]) == false) return false;
            }

            /* history */
            var histar = Ext.getCmp('dafnform').query('component[class="histset"]');
            if (histar.length > 0) {
              data.entry.history = new Array();
            }
            for (var i = 0; i < histar.length; i++) {
              if (histar[i].query('component[name="history"]')[0].getValue() != '') {
                var newhist = {
                  '$': histar[i].query('component[name="history"]')[0].getValue(),
                  '@annotation': histar[i].query('component[name="annot"]')[0].getValue()
                };
                //data.entry.history.push({'$': histar[i].query('component[name="history"]')[0].getValue()});
                data.entry.history.push(newhist);
                if (checkxmltext(histar[i].query('component[name="history"]')[0]) == false) return false;
              }
            }

            /* sense */
            if (Ext.getCmp('dafnform').query('component[name="prelim"]')[0].getValue() != '') {
              data.entry.defblock.prelim = {'$': Ext.getCmp('dafnform').query('component[name="prelim"]')[0].getValue()};
            }
            var senar = Ext.getCmp('dafnform').query('component[class="senseset"]');
            for (var i = 0; i < senar.length; i++) {
              if (senar[i].query('component[name="sense_def"]')[0].getValue() != '') {
                if (checkxmltext(senar[i].query('component[name="sense_def"]')[0]) == false) return false;
                var newsen = {
                  '$': senar[i].query('component[name="sense_def"]')[0].getValue(),
                  '@status': senar[i].query('component[name="status"]')[0].getValue(),
                  '@number': senar[i].query('component[name="number"]')[0].getValue(),
                  '@annotation': senar[i].query('component[name="annot"]')[0].getValue(),
                };
                data.entry.defblock.sense.push(newsen);
              }
            }
            /* Requests musings  */
            if (Ext.getCmp('dafnform').query('component[name="requests"]')[0].getValue() != '') {
              data.entry.requests = {'$': Ext.getCmp('dafnform').query('component[name="requests"]')[0].getValue()};
              if (checkxmltext(Ext.getCmp('dafnform').query('component[name="requests"]')[0]) == false) return false;
            }
            if (Ext.getCmp('dafnform').query('component[name="musings"]')[0].getValue() != '') {
              data.entry.musings = {'$': Ext.getCmp('dafnform').query('component[name="musings"]')[0].getValue()};
              if (checkxmltext(Ext.getCmp('dafnform').query('component[name="musings"]')[0]) == false) return false;
            }

            alert(JSON.stringify(data))
            /*if (form.isValid()) {
              form.submit({
                params: {
                  data: JSON.stringify(data),
                  action: 'save',
                  id: entryid,
                },
                waitMsg: 'Saving...',
                success: function(form, action) {
                  if (closewindow) {
                    window.close();
                  } else {
                    Ext.Msg.alert('Saved', action.result.msg);
                  }
                }
              });
            }*/
        }


Ext.onReady(function(){

    //var entryid = '5';
    //var entryid;

    var sense1 = create_sense();
    //var sense2 = create_sense2();
    var hist = create_hist();

    var verbform = Ext.create('Ext.form.Panel', {
        url:'/dafn',
        frame: true,
        title: 'Surname ',
        bodyStyle:'padding:5px 5px',
        /*width: 350,*/
        fieldDefaults: {
          msgTarget: 'side',
          labelWidth: 50
        },
        id: 'dafnform',
        defaultType: 'textfield',
        defaults: {
          /*anchor: '100%'*/
        },

        items: [{
          xtype: 'fieldset',
          title: 'Heading',
          layout: 'anchor',
          items: [{
            xtype: 'container',
            id: 'headingset',
            frame: 'true',
            defaultType: 'textfield',
            layout: {
              type: 'hbox'
            },
            items: [{
              fieldLabel: 'name',
              name: 'name',
              //allowBlank: false,
              //grow: true,
              id: 'name',
              //value: 'Adorno'
            },{
              fieldLabel: 'count',
              name: 'count',
              //value: '406'
            },{
              fieldLabel: '2007 freq',
              name: 'freq_2007',
              //value: '406'
            },{
              fieldLabel: 'status',
              name: 'status',
              xtype: 'combobox',
              store: status_store,
              displayField: 'label',
              valueField: 'status',
              editable: false,
              //value: 'a'
            },{
              xtype: 'splitter'
            },{
              xtype: 'textfield',
              name: 'sortkey',
              readOnly: true,
              width: 50
            }]
          }]
        },{
          xtype: 'fieldset',
          title: 'Sense',
          layout: 'anchor',
          id: 'sensefield',
          items: [{
            xtype: 'container',
            id: 'sensecontainer',
            items: [{
              xtype: 'textfield',
              name: 'prelim',
              fieldLabel: 'prelim'
            },sense1]
          }]
        },{
          xtype: 'fieldset',
          title: 'History',
          id: 'histcontainer',
          items: [hist]
        },{
          xtype: 'fieldset',
          title: 'Forenames',
          id: 'forecontainer',
          items: [{
            xtype: 'textarea',
            width: 600,
            rows: 2,
            grow: 'true',
            name: 'forenames',
            listeners: {
              'blur': function(field) {
                checkxmltext(field);
              }
            }
          }]
        },{
          xtype: 'fieldset',
          title: 'Requests',
          id: 'requestcontainer',
          items: [{
            xtype: 'textarea',
            width: 600,
            rows: 2,
            grow: 'true',
            name: 'requests',
            fieldLabel: 'Requests'
          },{
            xtype: 'textarea',
            width: 600,
            rows: 2,
            grow: 'true',
            name: 'musings',
            id: 'musings',
            fieldLabel: 'Musings'
          },{
            xtype: 'label',
            text: 'B',
              listeners: {
                render: function(c){
                  c.getEl().on({
                  click: function(el){
                    insert_tag('musings-inputEl', 'b');
                  },
                  scope: c
                })
              }
            }
          },{
            xtype: 'richtext',
            fieldLabel: 'a'
          },{
            xtype: 'richtext',
            fieldLabel: 'z'
          }]
        }],
 
        buttons: [{
          text: 'Save&close',
          formBind: true,
          handler: function() {
            save_data(true);
          }
        },{
          text: 'Save',
          formBind: true,
          handler: function() {
            save_data();
          }
        },{
          text: 'Close',
          handler: function() {
            if (confirm('Really close?')) {
              window.close();
            }
          }
        }],
    });

    verbform.render(document.body);

    var params = Ext.Object.fromQueryString(window.location.search.substring(1));

    /* load data */
    if (params.id != null) {
      entryid = params.id;
      Ext.Ajax.request({
        url: '/dafn',
        params: {
          action: 'get',
          id: params.id,
        },
        method: 'get',
        success: function(response) {
          /* fill form */
          var data = JSON.parse(response.responseText);

          /* heading */
          Ext.getCmp('dafnform').setTitle('Surname '+data.entry.name.$);
          document.title = 'DAFN '+data.entry.name.$;

          Ext.getCmp('dafnform').query('component[name="name"]')[0].setValue(data.entry.name.$);
          if (data.entry.count != undefined) {
            Ext.getCmp('dafnform').query('component[name="count"]')[0].setValue(data.entry.count.$);
          }
          if (data.entry.freq_2007 != undefined) {
            Ext.getCmp('dafnform').query('component[name="freq_2007"]')[0].setValue(data.entry.freq_2007.$);
          }
          Ext.getCmp('dafnform').query('component[name="status"]')[0].setValue(data.entry['@status']);
          Ext.getCmp('dafnform').query('component[name="sortkey"]')[0].setValue(data.entry['@sortkey']);

          /* forenames */
          if (data.entry.forenames != undefined) {
            Ext.getCmp('dafnform').query('component[name="forenames"]')[0].setValue(data.entry.forenames.$);
          }

          /* history */
          if (data.entry.history != undefined) {
            /* normalize to Array */
            if (data.entry.history.constructor != Array) {
              var histnew = {'$': data.entry.history.$, '@annotation': data.entry.history['@annotation']};
              //var histnew = {'$': data.entry.history.$};
              data.entry.history = new Array(histnew);
            }
            /* delete existing */
            Ext.getCmp('dafnform').query('component[class="histset"]')[0].destroy();
            /* fill new */
            for (var i = 0; i < data.entry.history.length; i++) {
              var hist = create_hist();
              Ext.getCmp('histcontainer').add(hist);
              hist.query('component[name="history"]')[0].setValue(data.entry.history[i].$);
              hist.query('component[name="annot"]')[0].setValue(data.entry.history[i]['@annotation']);
            }
          }

          /* sense */
          if (data.entry.defblock != undefined) {
            if (data.entry.defblock.prelim != undefined) {
              Ext.getCmp('dafnform').query('component[name="prelim"]')[0].setValue(data.entry.defblock.prelim.$);
            }
            /* normalize to Array */
            if (data.entry.defblock.sense.constructor != Array) {
              //var senew = {'$': data.entry.defblock.sense.$, '@url': data.entry.defblock.sense['@url'], '@status': data.entry.defblock.sense['@status'], '@annotation': data.entry.defblock.sense['@annotation']};
              var senew = {'$': data.entry.defblock.sense.$, '@status': data.entry.defblock.sense['@status'], '@annotation': data.entry.defblock.sense['@annotation'], '@number': data.entry.defblock.sense['@number']};
              data.entry.defblock.sense = new Array(senew);
            }
            if (data.entry.defblock.sense.length > 0) {
              Ext.getCmp('dafnform').query('component[class="senseset"]')[0].destroy();
            }
            for (var i = 0; i < data.entry.defblock.sense.length; i++) {
              var sen = create_sense();
              Ext.getCmp('sensecontainer').add(sen);
              sen.query('component[name="sense_def"]')[0].setValue(data.entry.defblock.sense[i].$);
              sen.query('component[name="status"]')[0].setValue(data.entry.defblock.sense[i]['@status']);
              sen.query('component[name="number"]')[0].setValue(data.entry.defblock.sense[i]['@number']);
              sen.query('component[name="annot"]')[0].setValue(data.entry.defblock.sense[i]['@annotation']);
            }
          }

          /* requests musings */
          if (data.entry.requests != undefined) {
            Ext.getCmp('dafnform').query('component[name="requests"]')[0].setValue(data.entry.requests.$);
          }
          if (data.entry.musings != undefined) {
            Ext.getCmp('dafnform').query('component[name="musings"]')[0].setValue(data.entry.musings.$);
          }

        }
      });
    }

});
